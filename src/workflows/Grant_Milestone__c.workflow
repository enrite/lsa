<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Milestone_progress_update_changed</fullName>
        <description>Milestone progress update changed</description>
        <protected>false</protected>
        <recipients>
            <recipient>Milestone_review</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Grant_Templates/Progress_update_changed</template>
    </alerts>
    <rules>
        <fullName>Progress update changed</fullName>
        <actions>
            <name>Milestone_progress_update_changed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>(ISCHANGED(Progress_Update__c) || ISCHANGED( Attachment_Uploaded__c))  &amp;&amp;  LastModifiedBy.Profile.Name == &apos;LSA Grant Community&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
