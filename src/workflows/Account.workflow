<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <rules>
        <fullName>Account Created</fullName>
        <actions>
            <name>Complete_account_setup_Finance</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>Complete_account_setup_Finance</fullName>
        <assignedTo>finance.officer@enrite.com.au</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Complete account setup (Finance)</description>
        <dueDateOffset>7</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Complete account setup(Finance)</subject>
    </tasks>
</Workflow>
