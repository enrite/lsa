<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Activity_Code</fullName>
        <field>Activity_Code__c</field>
        <formula>IF( RCR_Funding_Detail__r.Activity_Code_Based_On_Client_Details__c, 
    IF(RCR_Funding_Detail__r.RCR_Service_Order__r.Client__r.Is_Indigenous__c,
        IF((IF(MONTH(RCR_Activity_Statement_Item__r.Activity_Date__c) &gt; MONTH(RCR_Funding_Detail__r.RCR_Service_Order__r.Client__r.Date_Of_Birth__c), 
            YEAR(RCR_Activity_Statement_Item__r.Activity_Date__c) - YEAR(RCR_Funding_Detail__r.RCR_Service_Order__r.Client__r.Date_Of_Birth__c), 
            IF(AND(MONTH(RCR_Activity_Statement_Item__r.Activity_Date__c) = MONTH(RCR_Funding_Detail__r.RCR_Service_Order__r.Client__r.Date_Of_Birth__c), 
                          DAY(RCR_Activity_Statement_Item__r.Activity_Date__c) &gt;= DAY(RCR_Funding_Detail__r.RCR_Service_Order__r.Client__r.Date_Of_Birth__c)), 
                YEAR(RCR_Activity_Statement_Item__r.Activity_Date__c) - YEAR(RCR_Funding_Detail__r.RCR_Service_Order__r.Client__r.Date_Of_Birth__c),
               (YEAR(RCR_Activity_Statement_Item__r.Activity_Date__c) - YEAR(RCR_Funding_Detail__r.RCR_Service_Order__r.Client__r.Date_Of_Birth__c)) - 1))) &gt;= 50, &apos;L103&apos;, &apos;L104&apos;), 
        IF((IF(MONTH(RCR_Activity_Statement_Item__r.Activity_Date__c) &gt; MONTH(RCR_Funding_Detail__r.RCR_Service_Order__r.Client__r.Date_Of_Birth__c), 
            YEAR(RCR_Activity_Statement_Item__r.Activity_Date__c) - YEAR(RCR_Funding_Detail__r.RCR_Service_Order__r.Client__r.Date_Of_Birth__c), 
            IF(AND(MONTH(RCR_Activity_Statement_Item__r.Activity_Date__c) = MONTH(RCR_Funding_Detail__r.RCR_Service_Order__r.Client__r.Date_Of_Birth__c), 
                          DAY(RCR_Activity_Statement_Item__r.Activity_Date__c) &gt;= DAY(RCR_Funding_Detail__r.RCR_Service_Order__r.Client__r.Date_Of_Birth__c)), 
                YEAR(RCR_Activity_Statement_Item__r.Activity_Date__c) - YEAR(RCR_Funding_Detail__r.RCR_Service_Order__r.Client__r.Date_Of_Birth__c),
               (YEAR(RCR_Activity_Statement_Item__r.Activity_Date__c) - YEAR(RCR_Funding_Detail__r.RCR_Service_Order__r.Client__r.Date_Of_Birth__c)) - 1))) &gt;= 65, &apos;L101&apos;, &apos;L102&apos;)),  
RCR_Funding_Detail__r.Activity_Code__c)</formula>
        <name>Set Activity Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Determine Activity Code</fullName>
        <actions>
            <name>Set_Activity_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Determine the Activity Code that applies at the time the record is created.</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
