<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Address</fullName>
        <field>Address__c</field>
        <formula>if( Address__c &lt;&gt; &apos;&apos;,Address__c, Client__r.Address__c)</formula>
        <name>Update Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Client__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Location_Type</fullName>
        <description>update the location type from the Location History</description>
        <field>Location_Type__c</field>
        <formula>text(Location_Type__c)</formula>
        <name>Update Location Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Client__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Postal_Address</fullName>
        <field>Postal_Address__c</field>
        <formula>IF (Postal_Address__c &lt;&gt; &apos;&apos;, Postal_Address__c , &apos;&apos;)</formula>
        <name>Update Postal Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Client__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Postal_Postcode</fullName>
        <field>Postal_PostCode__c</field>
        <formula>if( Postal_PostCode__c &lt;&gt; &apos;&apos; , Postal_PostCode__c ,  &apos;&apos; )</formula>
        <name>Update Postal Postcode</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Client__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Postal_State</fullName>
        <field>Postal_State__c</field>
        <formula>if( ispickval(Postal_State__c,&apos;&apos;),&apos;&apos;,TEXT(Postal_State__c))</formula>
        <name>Update Postal State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Client__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Postal_Suburb</fullName>
        <field>Postal_Suburb__c</field>
        <formula>if( Postal_Suburb__c &lt;&gt; &apos;&apos; , Postal_Suburb__c ,  &apos;&apos; )</formula>
        <name>Update Postal Suburb</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Client__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Postcode</fullName>
        <field>PostCode__c</field>
        <formula>if( Postcode__c &lt;&gt;&apos;&apos;,Postcode__c ,  Client__r.PostCode__c )</formula>
        <name>Update Postcode</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Client__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Suburb</fullName>
        <field>Suburb__c</field>
        <formula>if ( Suburb__c &lt;&gt; &apos;&apos;,Suburb__c , Client__r.Suburb__c )</formula>
        <name>Update Suburb</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Client__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Location History - Update Client Address</fullName>
        <actions>
            <name>Update_Address</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Location_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Postal_Address</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Postal_Postcode</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Postal_State</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Postal_Suburb</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Postcode</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Suburb</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Location_History__c.Update_Residential_Address__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>update the client address from the location history</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Location History - Update Current Facility</fullName>
        <actions>
            <name>Update_Location_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Location_History__c.Update_Current_Hospital_Facility__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>update the current facility on the client from the Location History Update</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
