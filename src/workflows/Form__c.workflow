<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>FIMFAM_Mobility_Total_Update</fullName>
        <field>FIMFAM_Mobility_Total__c</field>
        <formula>VALUE(BLANKVALUE(TEXT( Tx_Bed_Score__c ), &apos;0&apos;)) 
+ VALUE(BLANKVALUE(TEXT( Tx_Toilet_Score__c ), &apos;0&apos;)) 
+ VALUE(BLANKVALUE(TEXT( Tx_Bath_Score__c ), &apos;0&apos;)) 
+ VALUE(BLANKVALUE(TEXT( Car_Transfer_Score__c ), &apos;0&apos;)) 
+ VALUE(BLANKVALUE(TEXT( Walk_Wheelchair_Score__c ), &apos;0&apos;)) 
+ VALUE(BLANKVALUE(TEXT( Stairs_Score__c ), &apos;0&apos;)) 
+ VALUE(BLANKVALUE(TEXT( Community_Access_Score__c ), &apos;0&apos;))</formula>
        <name>FIMFAM Mobility Total Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Submitted_Flag</fullName>
        <field>Submitted__c</field>
        <literalValue>1</literalValue>
        <name>Set Submitted Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Time_of_Accident</fullName>
        <field>Time_of_accident__c</field>
        <formula>IF(NOT(ISPICKVAL(Time_of_accident_hours__c, &apos;&apos; )) &amp;&amp; NOT(ISPICKVAL(Time_of_accident_minutes__c, &apos;&apos; )), 
TEXT( Time_of_accident_hours__c ) + &apos;:&apos; 
+ TEXT( Time_of_accident_minutes__c ),
&apos;&apos;)</formula>
        <name>Set Time of Accident</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>FIMFAM Mobility Score Total</fullName>
        <actions>
            <name>FIMFAM_Mobility_Total_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>1=1</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Form has been submitted</fullName>
        <actions>
            <name>Set_Submitted_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Form__c.Status__c</field>
            <operation>equals</operation>
            <value>Submitted</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Time of Accident</fullName>
        <actions>
            <name>Set_Time_of_Accident</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
