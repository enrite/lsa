<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_to_App_Rules_Committee</fullName>
        <description>Email to App Rules Committee</description>
        <protected>false</protected>
        <recipients>
            <recipient>App_Rules_Committee</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>lifetime.support@sa.gov.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Form_Email_Templates/Application_Rules_Committee_Review_Required</template>
    </alerts>
    <alerts>
        <fullName>Email_to_Chief_Executive</fullName>
        <description>Email to Chief Executive</description>
        <protected>false</protected>
        <recipients>
            <recipient>Chief_Executive</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>lifetime.support@sa.gov.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Form_Email_Templates/Chief_Executive_Approval_Required</template>
    </alerts>
    <alerts>
        <fullName>Email_to_Manager_ASD</fullName>
        <description>Email to Manager ASD</description>
        <protected>false</protected>
        <recipients>
            <recipient>Manager_ASD</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>lifetime.support@sa.gov.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Form_Email_Templates/Manager_ASD_Approval_Required</template>
    </alerts>
    <alerts>
        <fullName>Email_to_Service_Planner</fullName>
        <description>Email to Service Planner</description>
        <protected>false</protected>
        <recipients>
            <field>Completed_By__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>lifetime.support@sa.gov.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Form_Email_Templates/Service_Planner_Assessment_Required</template>
    </alerts>
    <alerts>
        <fullName>Send_Guardianship_date_of_hearing_due_email</fullName>
        <description>Send Guardianship date of hearing due email</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_Service_Planner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>lifetime.support@sa.gov.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Form_Email_Templates/Guardianship_application_date_of_hearing_reached</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_Assigned_Service_Planner_Email</fullName>
        <field>Assigned_Service_Planner_Email__c</field>
        <formula>Form_Application_Assessment__r.Allocated_to__r.Email</formula>
        <name>Set Assigned Service Planner Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Date_Recommended_to_CE</fullName>
        <field>Date_Recommended_to_CE__c</field>
        <formula>TODAY()</formula>
        <name>Set Date Recommended to CE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_Date</fullName>
        <field>Status_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Status Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Application Assessment - App Rules Committee</fullName>
        <actions>
            <name>Email_to_App_Rules_Committee</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Form_Detail__c.Notify_Application_Rules_Comm__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Application Assessment - Chief Executive</fullName>
        <actions>
            <name>Email_to_Chief_Executive</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>(ISCHANGED(RecordTypeId) &amp;&amp; RecordType.Name == &apos;Chief Executive Application Assessment&apos;)
||
(ISCHANGED(Application_Rules_Comm_Chair_Concurs__c) &amp;&amp; Application_Rules_Comm_Chair_Concurs__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Application Assessment - Manager ASD</fullName>
        <actions>
            <name>Email_to_Manager_ASD</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Form_Detail__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Manager ASD Application Assessment</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Application Assessment - Service Planner</fullName>
        <actions>
            <name>Email_to_Service_Planner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Form_Detail__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Service Planner Application Assessment</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Guardianship date of hearing set</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Form_Detail__c.Guardianship_application_date_of_hearing__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Guardianship_date_of_hearing_due_email</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Form_Detail__c.Guardianship_application_date_of_hearing__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Set Assigned Service Planner Email</fullName>
        <actions>
            <name>Set_Assigned_Service_Planner_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Form_Detail__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Manager ASD Application Assessment,Chief Executive Application Assessment,Service Planner Application Assessment</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Date Recommended to CE</fullName>
        <actions>
            <name>Set_Date_Recommended_to_CE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Recommended_to_Chief_Executive_CE__c 
&amp;&amp; NOT(PRIORVALUE(Recommended_to_Chief_Executive_CE__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Task - Assessment Completion</fullName>
        <actions>
            <name>Assessment_Complete_Letter_s_Required</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Form_Detail__c.Status_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Task on assessment/lifetime assessment completion - to be set to the PSO</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Assessment_Complete_Letter_s_Required</fullName>
        <assignedTo>finance.officer@enrite.com.au</assignedTo>
        <assignedToType>user</assignedToType>
        <description>PSO send out a participant letter in all cases, and a letter to the person who submitted the application.</description>
        <dueDateOffset>7</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Form_Detail__c.Status_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Assessment Complete - Letter(s) Required - PSO to action</subject>
    </tasks>
</Workflow>
