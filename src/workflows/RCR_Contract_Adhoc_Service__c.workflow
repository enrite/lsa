<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Round_AdHoc_Amount</fullName>
        <field>Amount__c</field>
        <formula>ROUND(Amount__c, 2)</formula>
        <name>Round AdHoc Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Round_AdHoc_Rate</fullName>
        <field>Rate__c</field>
        <formula>ROUND(Rate__c, 2)</formula>
        <name>Round AdHoc Rate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Round AdHoc Cost Fields</fullName>
        <actions>
            <name>Round_AdHoc_Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Round_AdHoc_Rate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
