<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>LP_Approved_Invoice_failed_reconciliation</fullName>
        <description>LP Approved Invoice failed reconciliation</description>
        <protected>false</protected>
        <recipients>
            <recipient>SysAdmin</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>lifetime.support@sa.gov.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>LanternPay_Templates/LP_Invoice_failed_reconciliation</template>
    </alerts>
    <fieldUpdates>
        <fullName>AS_Add_To_Batch_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>RCR_Generate_RCTI</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>AS Add To Batch Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approved</fullName>
        <field>Reject__c</field>
        <literalValue>0</literalValue>
        <name>Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_RCTI_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>RCR_Waiting_for_RCTI_Creation</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to RCTI Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Schedule</fullName>
        <field>Schedule__c</field>
        <name>Clear Schedule</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>InvoiceRejected</fullName>
        <field>Reject__c</field>
        <literalValue>1</literalValue>
        <name>Invoice Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Invoice_Approved</fullName>
        <field>Reject__c</field>
        <literalValue>0</literalValue>
        <name>Invoice Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_30_Day_Tasks</fullName>
        <field>Create_30_Day_Tasks__c</field>
        <literalValue>1</literalValue>
        <name>Set 30 Day Tasks</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Owner_to_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>RCR_Manual_Approval_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Set Owner to Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Clear Schedule</fullName>
        <actions>
            <name>Clear_Schedule</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$RecordType.DeveloperName == &apos;Submitted&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email to Invoice 30 Days</fullName>
        <active>false</active>
        <formula>NOT(ISBLANK(Email_To_Invoice__c)) &amp;&amp; NOT(Create_30_Day_Tasks__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_30_Day_Tasks</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>RCR_Invoice__c.Date__c</offsetFromField>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>LP Approved Failed Reconciliation</fullName>
        <actions>
            <name>LP_Approved_Invoice_failed_reconciliation</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>RCR_Invoice__c.LP_Invoice_Key__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>RCR_Invoice__c.LP_Approved_Rejected_Items__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>RCR_Invoice__c.Status__c</field>
            <operation>equals</operation>
            <value>Reconciled - Errors</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Minimum Start Date</fullName>
        <active>false</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>RCR Send to Approval Required Queue</fullName>
        <actions>
            <name>Set_Owner_to_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>RCR_Invoice__c.Lines_Requiring_Approval__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>RCR_Invoice__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Submitted</value>
        </criteriaItems>
        <description>When an invoice has been submitted assign it to the approval queue if and items require manual approval</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RCR Send to RCTI Queue</fullName>
        <actions>
            <name>Assign_to_RCTI_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>RCR_Invoice__c.Lines_Requiring_Approval__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>RCR_Invoice__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Submitted</value>
        </criteriaItems>
        <criteriaItems>
            <field>RCR_Invoice__c.RCTI_Agreement__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>When an invoice has been submitted and all no items require approval then assign it to the RCTI queue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RCR Send to Waiting to be Added to Batch</fullName>
        <actions>
            <name>AS_Add_To_Batch_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND ((3 AND 4) OR 5) AND 6</booleanFilter>
        <criteriaItems>
            <field>RCR_Invoice__c.Lines_Requiring_Approval__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>RCR_Invoice__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Submitted</value>
        </criteriaItems>
        <criteriaItems>
            <field>RCR_Invoice__c.RCTI_Agreement__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>RCR_Invoice__c.RCTI_Generated__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>RCR_Invoice__c.RCTI_Agreement__c</field>
            <operation>equals</operation>
            <value>No</value>
        </criteriaItems>
        <criteriaItems>
            <field>RCR_Invoice__c.Total_Reconciled__c</field>
            <operation>notEqual</operation>
            <value>0</value>
        </criteriaItems>
        <description>When an invoice has been submitted and all no items require approval then assign it to the Waiting to be Added to Batch queue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
