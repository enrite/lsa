<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Assign_to_Manager_Service_Planning</fullName>
        <field>OwnerId</field>
        <lookupValue>admin@enrite.com.au.lsa</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Assign to Manager Service Planning</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Escalate_priority</fullName>
        <field>Priority</field>
        <literalValue>High</literalValue>
        <name>Escalate priority</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>PreviousValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Create_Reconciliation_Reminder_True</fullName>
        <field>Create_Reconciliation_Reminder__c</field>
        <literalValue>1</literalValue>
        <name>Set Create Reconciliation Reminder True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Invoice_Reconciliation_Escalation</fullName>
        <field>Escalate_Reconciliation_Reminder__c</field>
        <literalValue>1</literalValue>
        <name>Set Invoice Reconciliation Escalation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Task_Group_PSO</fullName>
        <description>Update to the task group</description>
        <field>Assigned_to_Group__c</field>
        <literalValue>PSO</literalValue>
        <name>Update Task Group to PSO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Task_Group_to_Finance</fullName>
        <description>Update the task group to finance</description>
        <field>Assigned_to_Group__c</field>
        <literalValue>Finance</literalValue>
        <name>Update Task Group to Finance</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Contract Review Escalation</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>equals</operation>
            <value>Contract Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>notEqual</operation>
            <value>Completed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Assign_to_Manager_Service_Planning</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Escalate_priority</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Task.ActivityDate</offsetFromField>
            <timeLength>-3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Invoice Reconciliation Task Created</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>notEqual</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Create_Reconciliation_Reminder__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Create_Reconciliation_Reminder_True</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Task.CreatedDate</offsetFromField>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Invoice Reconciliation Task Created Escalation</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>notEqual</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Escalate_Reconciliation_Reminder__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Invoice_Reconciliation_Escalation</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Task.CreatedDate</offsetFromField>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Task - update Finance</fullName>
        <actions>
            <name>Update_Task_Group_to_Finance</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Task.Description</field>
            <operation>contains</operation>
            <value>Finance</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>contains</operation>
            <value>Finance</value>
        </criteriaItems>
        <description>Update the task assigned to group to Finance if the comment contains Finance</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Task - update PSO</fullName>
        <actions>
            <name>Update_Task_Group_PSO</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Task.Description</field>
            <operation>contains</operation>
            <value>PSO</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>contains</operation>
            <value>PSO</value>
        </criteriaItems>
        <description>Update the task assigned to group to PSO if the comment contains PSO</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Task Creation</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Task.OwnerId</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
