<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Application_Created_Acknowledge</fullName>
        <description>Application Created Acknowledge</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>lifetime.support@sa.gov.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Grant_Templates/Application_Created</template>
    </alerts>
    <alerts>
        <fullName>Application_Created_Internal</fullName>
        <description>Application Created Internal</description>
        <protected>false</protected>
        <recipients>
            <recipient>Grant_Management</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>lifetime.support@sa.gov.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Grant_Templates/Application_Created</template>
    </alerts>
    <alerts>
        <fullName>Application_Submitted_For_Approval</fullName>
        <description>Application Submitted For Approval</description>
        <protected>false</protected>
        <recipients>
            <recipient>Grant_Management</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>lifetime.support@sa.gov.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Grant_Templates/Application_Created</template>
    </alerts>
    <alerts>
        <fullName>Grant_Awarded</fullName>
        <description>Grant Awarded</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>lifetime.support@sa.gov.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Grant_Templates/Grant_Awarded</template>
    </alerts>
    <alerts>
        <fullName>Grant_Awarded_Internal</fullName>
        <description>Grant Awarded Internal</description>
        <protected>false</protected>
        <recipients>
            <recipient>Milestone_review</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Grant_Templates/Grant_Awarded_Internal</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_Approval_Queue_Tier1</fullName>
        <field>Approval_Queue_Name__c</field>
        <formula>&apos;Tier_1&apos;</formula>
        <name>Set Approval Queue Tier1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Queue_Tier2</fullName>
        <field>Approval_Queue_Name__c</field>
        <formula>&apos;Tier_2&apos;</formula>
        <name>Set Approval Queue Tier2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Queue_Tier3</fullName>
        <field>Approval_Queue_Name__c</field>
        <formula>&apos;Tier_3&apos;</formula>
        <name>Set Approval Queue Tier3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_Approved</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Set Status Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_Assessment</fullName>
        <field>Status__c</field>
        <literalValue>Assessment</literalValue>
        <name>Set Status Assessment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_Rejected</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Set Status Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_Review</fullName>
        <field>Status__c</field>
        <literalValue>Review</literalValue>
        <name>Set Status Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Application Created</fullName>
        <actions>
            <name>Application_Created_Acknowledge</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Application_Created_Internal</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Grant_Application__c.Email__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
