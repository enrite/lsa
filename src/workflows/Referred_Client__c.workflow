<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Finance_about_client_accepted</fullName>
        <description>Email Finance about client accepted</description>
        <protected>false</protected>
        <recipients>
            <recipient>Finance</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>Program_Support_Officers</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>lifetime.support@sa.gov.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Form_Email_Templates/Email_to_Finance</template>
    </alerts>
    <alerts>
        <fullName>Email_newly_allocated_user</fullName>
        <description>Email newly allocated user</description>
        <protected>false</protected>
        <recipients>
            <field>Allocated_to__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>lifetime.support@sa.gov.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Form_Email_Templates/Client_Allocated_to_User</template>
    </alerts>
    <alerts>
        <fullName>Email_to_Broadcast_All</fullName>
        <ccEmails>engl0038@hotmail.com</ccEmails>
        <description>Email to Broadcast All</description>
        <protected>false</protected>
        <recipients>
            <recipient>Broadcast_All</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>lifetime.support@sa.gov.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Form_Email_Templates/Email_about_client_status_change</template>
    </alerts>
    <alerts>
        <fullName>Email_when_client_Not_Accepted</fullName>
        <description>Email when client Not Accepted</description>
        <protected>false</protected>
        <recipients>
            <recipient>ASD</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>Governance</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>lifetime.support@sa.gov.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Form_Email_Templates/Email_about_client_status_change</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_Client_Name</fullName>
        <field>Name</field>
        <formula>IF(ISBLANK(Family_Name__c), Name, TRIM(Given_Name__c + &apos; &apos; + Family_Name__c))</formula>
        <name>Set Client Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Client_Number</fullName>
        <field>CCMS_ID__c</field>
        <formula>Client_Number_Sequence__c</formula>
        <name>Set Client Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Date_VCR_Requested</fullName>
        <field>Date_VCR_Requested__c</field>
        <formula>TODAY()</formula>
        <name>Set Date VCR Requested</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Time_of_Accident</fullName>
        <field>Time_of_Accident__c</field>
        <formula>IF(NOT(ISPICKVAL(Time_of_accident_hours__c, &apos;&apos; )) &amp;&amp; NOT(ISPICKVAL(Time_of_accident_minutes__c, &apos;&apos; )), 
TEXT( Time_of_accident_hours__c ) + &apos;:&apos; 
+ TEXT( Time_of_accident_minutes__c ),
&apos;&apos;)</formula>
        <name>Set Time of Accident</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Allocated to changed</fullName>
        <actions>
            <name>Email_newly_allocated_user</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>NOT(ISBLANK( Allocated_to__c ))
&amp;&amp; (ISNEW() || ISCHANGED( Allocated_to__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Client Name</fullName>
        <actions>
            <name>Set_Client_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Client Number</fullName>
        <actions>
            <name>Set_Client_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Time of Accident</fullName>
        <actions>
            <name>Set_Time_of_Accident</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Status set to Accepted</fullName>
        <actions>
            <name>Email_Finance_about_client_accepted</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_to_Broadcast_All</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Referred_Client__c.Status__c</field>
            <operation>startsWith</operation>
            <value>Accepted</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Status set to Not Accepted</fullName>
        <actions>
            <name>Email_when_client_Not_Accepted</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Referred_Client__c.Status__c</field>
            <operation>equals</operation>
            <value>Not Accepted</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>VCR Requested</fullName>
        <actions>
            <name>Set_Date_VCR_Requested</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Referred_Client__c.VCR_Requested__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Referred_Client__c.Date_VCR_Requested__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
