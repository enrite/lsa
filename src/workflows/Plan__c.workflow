<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_about_plan_approved</fullName>
        <description>Email about plan approved</description>
        <protected>false</protected>
        <recipients>
            <field>Service_Planner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>lifetime.support@sa.gov.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Form_Email_Templates/Plan_Approved</template>
    </alerts>
    <alerts>
        <fullName>Email_about_plan_not_approved</fullName>
        <description>Email about plan not approved</description>
        <protected>false</protected>
        <recipients>
            <field>Service_Planner_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>Manager_ASD</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>lifetime.support@sa.gov.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Form_Email_Templates/Plan_Not_Approved</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_Approved_Estimated_Plan_Cost</fullName>
        <field>Approved_Estimated_Plan_Cost__c</field>
        <formula>Estimated_Plan_Costs__c</formula>
        <name>Set Approved Estimated Plan Cost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Service_Planner_Email</fullName>
        <field>Service_Planner_Email__c</field>
        <formula>Client__r.Allocated_to__r.Email</formula>
        <name>Set Service Planner Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Plan Approved</fullName>
        <actions>
            <name>Email_about_plan_approved</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Set_Approved_Estimated_Plan_Cost</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Plan__c.Plan_Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Plan Not Approved</fullName>
        <actions>
            <name>Email_about_plan_not_approved</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Plan__c.Plan_Status__c</field>
            <operation>equals</operation>
            <value>Not Approved</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Plan Updated</fullName>
        <actions>
            <name>Set_Service_Planner_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
