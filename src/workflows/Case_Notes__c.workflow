<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Alert_Comments_Update</fullName>
        <description>update the client Alert Comments if case note alert set</description>
        <field>AlertComments__c</field>
        <formula>if( Trigger_Client_Alert__c = True,   Note__c &amp; BR() &amp; &quot; Created By - &quot; &amp;  CreatedBy.FullName__c &amp; &quot; on &quot; &amp;  text(Day(Note_Date__c)) &amp; &quot;/&quot; &amp; text(Month(Note_Date__c)) &amp; &quot;/&quot; &amp; text(YEAR(Note_Date__c) )    ,&quot;&quot;)</formula>
        <name>Alert Comments Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Client__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Client_Alert_Clear</fullName>
        <field>AlertComments__c</field>
        <name>Client Alert - Clear</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <targetObject>Client__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Case Notes Clear Trigger Alert</fullName>
        <actions>
            <name>Client_Alert_Clear</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case_Notes__c.Trigger_Client_Alert__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Notes update Trigger Alert</fullName>
        <actions>
            <name>Alert_Comments_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case_Notes__c.Trigger_Client_Alert__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
