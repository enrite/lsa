<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Broadcast_All_about_new_form</fullName>
        <description>Email Broadcast All about new form</description>
        <protected>false</protected>
        <recipients>
            <recipient>Broadcast_All</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>lifetime.support@sa.gov.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Form_Email_Templates/Email_to_Broadcast_All_about_new_Form</template>
    </alerts>
    <alerts>
        <fullName>Email_PSOs_for_new_Notification</fullName>
        <description>Email PSOs for new Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>Program_Support_Officers</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>lifetime.support@sa.gov.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Form_Email_Templates/Email_about_new_Notification</template>
    </alerts>
    <rules>
        <fullName>Broadcast All changed</fullName>
        <actions>
            <name>Email_Broadcast_All_about_new_form</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Broadcast_All__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notification created</fullName>
        <actions>
            <name>Email_PSOs_for_new_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
