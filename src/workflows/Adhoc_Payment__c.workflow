<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>adhoc_payment_approval_planner_rejection</fullName>
        <description>adhoc payment - approval rejection</description>
        <protected>false</protected>
        <recipients>
            <recipient>Finance</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RCR_Email_Templates/Adhoc_Payment_Rejection</template>
    </alerts>
    <fieldUpdates>
        <fullName>Clear_Approval_Step</fullName>
        <field>Approval_Process_Step__c</field>
        <name>Clear Approval Step</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Service_Planner_Approved</fullName>
        <field>Service_Planner_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Service Planner Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Service_Planner_Not_Approved</fullName>
        <field>Service_Planner_Approved__c</field>
        <literalValue>0</literalValue>
        <name>Service Planner Not Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_30_Day_Tasks</fullName>
        <field>Create_30_Day_Tasks__c</field>
        <literalValue>1</literalValue>
        <name>Set 30 Day Tasks</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Step_Director_Services</fullName>
        <field>Approval_Process_Step__c</field>
        <formula>&apos;Director Services&apos;</formula>
        <name>Set Approval Step - Director Services</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Step_Final_Approval</fullName>
        <field>Approval_Process_Step__c</field>
        <formula>IF(Total_Amount__c &lt;= 5500, &apos;Final Approval&apos;, &apos;Director Approval&apos;)</formula>
        <name>Set Approval Step - Final Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Step_Service_Planner</fullName>
        <field>Approval_Process_Step__c</field>
        <formula>&apos;Service Planner&apos;</formula>
        <name>Set Approval Step - Service Planner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approved</fullName>
        <field>Approved__c</field>
        <literalValue>1</literalValue>
        <name>Set Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approver</fullName>
        <field>Approver__c</field>
        <formula>$User.Id</formula>
        <name>Set Approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Step_Final_Approval_Approved</fullName>
        <field>Approval_Process_Step__c</field>
        <formula>&apos;Final Approval Approved&apos;</formula>
        <name>Set Step Final Approval Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Submitter</fullName>
        <field>Submitter__c</field>
        <formula>$User.Id</formula>
        <name>Set Submitter</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Email to Invoice 30 Days</fullName>
        <active>false</active>
        <formula>NOT(ISBLANK(Email_To_Invoice__c)) &amp;&amp; NOT( Create_30_Day_Tasks__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_30_Day_Tasks</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Adhoc_Payment__c.Date_of_Transaction__c</offsetFromField>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <tasks>
        <fullName>Incomplete_Adhoc_Payment</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Incomplete Adhoc Payment</subject>
    </tasks>
</Workflow>
