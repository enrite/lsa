<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Funding_Details_Activity_Code</fullName>
        <field>Activity_Code__c</field>
        <formula>IF( Activity_Code_Based_On_Client_Details__c, &apos;&apos;, Activity_Code__c)</formula>
        <name>Set Funding Details Activity Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Set FD Activity Code</fullName>
        <actions>
            <name>Set_Funding_Details_Activity_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Activity_Code_Based_On_Client_Details__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
