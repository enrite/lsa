<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>email_approval</fullName>
        <description>email approval</description>
        <protected>false</protected>
        <recipients>
            <recipient>neil@lsa.gov</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Brokerage_Officer_Approval_Req</template>
    </alerts>
    <alerts>
        <fullName>rejected</fullName>
        <description>rejected</description>
        <protected>false</protected>
        <recipients>
            <recipient>neil@lsa.gov</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Brokerage_Officer_Approval_Req</template>
    </alerts>
    <fieldUpdates>
        <fullName>Clear_Error_Message</fullName>
        <field>Error__c</field>
        <name>Clear Error Message</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Reconciled_Service</fullName>
        <field>Reconciled_Service__c</field>
        <formula>IF(ISBLANK(RCR_Contract_Adhoc_Service__c),  RCR_Contract_Scheduled_Service__c ,  RCR_Contract_Adhoc_Service__c)</formula>
        <name>Set Reconciled Service</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_Submitted</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Submitted</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Scheduled_Service</fullName>
        <field>Reconciled_Service__c</field>
        <name>Set Scheduled Service</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_to_Rejected</fullName>
        <field>Override_Tolerance__c</field>
        <literalValue>Rejected</literalValue>
        <name>Set to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_to_Uppercase</fullName>
        <description>Set the Force Reconcile to upper case</description>
        <field>Force_Reconcile_To__c</field>
        <formula>UPPER( Force_Reconcile_To__c )</formula>
        <name>Set to Uppercase</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Override_Tolerance</fullName>
        <field>Override_Tolerance__c</field>
        <literalValue>Approved</literalValue>
        <name>Update Override Tolerance</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Force Reconcile to Upper</fullName>
        <actions>
            <name>Set_to_Uppercase</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Force Reconcile to Upper</description>
        <formula>ISCHANGED( Force_Reconcile_To__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Reconciled</fullName>
        <actions>
            <name>Set_Reconciled_Service</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>1=1</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
