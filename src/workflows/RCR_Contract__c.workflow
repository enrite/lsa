<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Cancellation_Notification</fullName>
        <description>Cancellation Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>RCRBrokerageOfficer</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RCR_Email_Templates/Cancellation_Notification</template>
    </alerts>
    <alerts>
        <fullName>New_IF_Service_Order_Host</fullName>
        <description>New Service Order for IF Host</description>
        <protected>false</protected>
        <recipients>
            <recipient>neil@lsa.gov</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>IF_Templates/New_IF_Service_Order_Host</template>
    </alerts>
    <alerts>
        <fullName>New_Request_Approved</fullName>
        <description>New Request Approved</description>
        <protected>false</protected>
        <recipients>
            <recipient>RCRBrokerageOfficer</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>RCR_Senior_Client_Information_Officer</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>RCR_Team_Leader_Manager</recipient>
            <type>role</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>RCR_Email_Templates/New_Request_Approved</template>
    </alerts>
    <alerts>
        <fullName>New_Service_Order_for_IF</fullName>
        <description>New Service Order for IF</description>
        <protected>false</protected>
        <recipients>
            <recipient>neil@lsa.gov</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>IF_Templates/New_IF_Service_Order</template>
    </alerts>
    <alerts>
        <fullName>Notify_Brokerage_of_rejection</fullName>
        <description>Notify Brokerage of rejection</description>
        <protected>false</protected>
        <recipients>
            <field>Brokerage_Officer__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>RCR_Email_Templates/Notify_Brokerage_of_rejection</template>
    </alerts>
    <alerts>
        <fullName>Notify_RCR_RFS_CRAU_Approval_queue_members</fullName>
        <ccEmails>DCSIDisabilityCSPPanel@dcsi.sa.gov.au</ccEmails>
        <description>Notify DCSI Disability CSP Panel of Approval Request</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>RCR_Email_Templates/New_Out_of_Session_Request_Approval</template>
    </alerts>
    <alerts>
        <fullName>Notify_Service_Coordinator_Review_Rollover</fullName>
        <description>Notify Service Coordinator Review Rollover</description>
        <protected>false</protected>
        <recipients>
            <field>Service_Coordinator__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>RCR_Email_Templates/Rollover_Ready_for_Coordinator_Review</template>
    </alerts>
    <alerts>
        <fullName>Notify_Service_Coordinator_of_Approval</fullName>
        <description>Notify Service Coordinator of Approval</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Service_Coordinator__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RCR_Email_Templates/New_Request_Allocation_Approved</template>
    </alerts>
    <alerts>
        <fullName>Notify_Service_Coordinator_of_Rejection</fullName>
        <description>Notify Service Coordinator of Rejection</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Service_Coordinator__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RCR_Email_Templates/New_Request_Allocation_Rejected</template>
    </alerts>
    <alerts>
        <fullName>RCR_Once_off_ending_update</fullName>
        <description>RCR Once off ending update</description>
        <protected>false</protected>
        <recipients>
            <field>Service_Coordinator__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RCR_Email_Templates/Once_off_ending_update</template>
    </alerts>
    <fieldUpdates>
        <fullName>Add_to_audit</fullName>
        <field>Audit__c</field>
        <formula>TEXT(TODAY()) + &apos; &apos; + $User.FullName__c + &apos;: &apos; + Minor_Changes__c + BR() + BR() + Audit__c</formula>
        <name>Add to audit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Allocation_Approved</fullName>
        <field>Status__c</field>
        <literalValue>Allocation - Approved</literalValue>
        <name>Allocation - Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Allocation_Pending_Approval</fullName>
        <field>Status__c</field>
        <literalValue>Allocation Pending Approval</literalValue>
        <name>Allocation Pending Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Amendment_Request_Approved</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Amendment Request Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Amendment_Request_Recalled_Rejected</fullName>
        <field>Status__c</field>
        <literalValue>New</literalValue>
        <name>Amendment Request Recalled/Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approve_Service_Order</fullName>
        <field>CSA_Status__c</field>
        <literalValue>Pending Acceptance</literalValue>
        <name>Approve Service Order</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approve_Service_Order_Recall</fullName>
        <field>CSA_Status_Recall__c</field>
        <literalValue>Pending Acceptance</literalValue>
        <name>Approve Service Order (Recall)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Ownership_to_15000_to_50000</fullName>
        <field>OwnerId</field>
        <lookupValue>CSA_Pending_Approvals_15000_to_50000</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Ownership to $15,000 to $50,000</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Ownership_to_50000_to_75000</fullName>
        <field>OwnerId</field>
        <lookupValue>CSA_Pending_Approvals_50000_to_75000</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Ownership to $50,000 to $75,000</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Ownership_to_Over_75000</fullName>
        <field>OwnerId</field>
        <lookupValue>CSA_Pending_Approvals_Over_75000</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Ownership to Over $75,000</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Ownership_to_Under_15000</fullName>
        <field>OwnerId</field>
        <lookupValue>CSA_Pending_Approvals_Under_15000</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Ownership to Under $15,000</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_Queue_for_Final_Costings</fullName>
        <description>Set the Owner to the Queue &quot;Requested Final Costings&quot;</description>
        <field>OwnerId</field>
        <lookupValue>Requested_Final_Costings</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Queue for Final Costings</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Approval_Date</fullName>
        <field>Date_Approved__c</field>
        <name>Clear Approval Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Approval_Date_Recall</fullName>
        <field>Date_Approved_Recall__c</field>
        <name>Clear Approval Date (Recall)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Approver_Name</fullName>
        <field>Approver_User_Name__c</field>
        <name>Clear Approver Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Approver_User_ID</fullName>
        <field>Approver_User_ID__c</field>
        <name>Clear Approver User ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Bulk_Approved_Flag</fullName>
        <field>CSA_Bulk_Approved__c</field>
        <literalValue>0</literalValue>
        <name>Clear Bulk Approved Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_CSA_Accepted_Date</fullName>
        <field>CSA_Acceptance_Date__c</field>
        <name>Clear CSA Accepted Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Cancellation_Approval_User</fullName>
        <field>Cancellation_Approver__c</field>
        <name>Clear Cancellation Approval User</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Requested_Cancellation_Date</fullName>
        <field>Requested_Cancellation_Date__c</field>
        <name>Clear Requested Cancellation Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_minor_changes</fullName>
        <field>Minor_Changes__c</field>
        <name>Clear minor changes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pending_Approval</fullName>
        <description>Set the request status to Pending Approval</description>
        <field>Status__c</field>
        <literalValue>Pending Approval</literalValue>
        <name>Pending Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall_Cancellation_Date</fullName>
        <field>RCR_Cancellation_Date__c</field>
        <formula>Cancellation_Date_Recall__c</formula>
        <name>Recall Cancellation Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reject_Service_Order</fullName>
        <field>CSA_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Reject Service Order</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reject_Service_Order_Recall</fullName>
        <field>CSA_Status_Recall__c</field>
        <literalValue>Rejected</literalValue>
        <name>Reject Service Order (Recall)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Request_Approved</fullName>
        <description>Set the status of the Service Order to Approved</description>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Request Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Request_Rejected</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Request Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rollover_Status_to_Approved</fullName>
        <field>Rollover_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Rollover Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rollover_Status_to_Pending</fullName>
        <field>Rollover_Status__c</field>
        <literalValue>Pending Coordinator Review</literalValue>
        <name>Rollover Status to Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rollover_Status_to_Pending_Approval</fullName>
        <field>Rollover_Status__c</field>
        <literalValue>Pending Approval</literalValue>
        <name>Rollover Status to Pending Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rollover_Status_to_Rejected</fullName>
        <field>Rollover_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Rollover Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Date</fullName>
        <description>The date that the Service Order was approved for release to the Service Providers for acceptance.</description>
        <field>Date_Approved__c</field>
        <formula>NOW()</formula>
        <name>Set Approval Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Date_Recall</fullName>
        <field>Date_Approved_Recall__c</field>
        <formula>NOW()</formula>
        <name>Set Approval Date (Recall)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Process</fullName>
        <field>Approval_process__c</field>
        <formula>&apos;Direct Approval&apos;</formula>
        <name>Set Approval Process(DEV only)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Process_2</fullName>
        <field>Approval_process__c</field>
        <formula>&apos;RFS Amendment Approval&apos;</formula>
        <name>Set Approval Process 2(DEV only)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Process_3</fullName>
        <field>Approval_process__c</field>
        <formula>&apos;RFS Rollover Approval&apos;</formula>
        <name>Set Approval Process 3(DEV only)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Process_4</fullName>
        <field>Approval_process__c</field>
        <formula>&apos;RFS Day Option Approval&apos;</formula>
        <name>Set Approval Process 4(DEV only)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Process_5</fullName>
        <field>Approval_process__c</field>
        <formula>&apos;RFS Cancellation Approval&apos;</formula>
        <name>Set Approval Process 5(DEV only)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Process_6</fullName>
        <field>Approval_process__c</field>
        <formula>&apos;Allocation - Emergency Respite&apos;</formula>
        <name>Set Approval Process 6</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Process_7</fullName>
        <field>Approval_process__c</field>
        <formula>&apos;Allocation Low Gap - New Request Apprv&apos;</formula>
        <name>Set Approval Process 7</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Process_8</fullName>
        <field>Approval_process__c</field>
        <formula>&apos;Allocation High Gap - New Request Apprv&apos;</formula>
        <name>Set Approval Process 8</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Process_9</fullName>
        <field>Approval_process__c</field>
        <formula>&apos;Service Request - Request Approval 04.1&apos;</formula>
        <name>Set Approval Process 9</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Process_LSA_1</fullName>
        <description>New Request with Plan Action</description>
        <field>Approval_process__c</field>
        <formula>&apos;New Request with Plan Action&apos;</formula>
        <name>Set Approval Process LSA 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Process_LSA_2</fullName>
        <description>Att Care &amp; Support &lt;= $5,500</description>
        <field>Approval_process__c</field>
        <formula>&apos;Att Care &amp; Support &lt;= $5,500&apos;</formula>
        <name>Set Approval Process LSA 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Process_LSA_3</fullName>
        <description>Att Care &amp; Support $5,500 - $55,000</description>
        <field>Approval_process__c</field>
        <formula>&apos;Att Care &amp; Support $5,500 - $55,000&apos;</formula>
        <name>Set Approval Process LSA 3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Process_LSA_4</fullName>
        <description>Att Care &amp; Support $55,000 - $110,000</description>
        <field>Approval_process__c</field>
        <formula>&apos;Att Care &amp; Support $55,000 - $110,000&apos;</formula>
        <name>Set Approval Process LSA 4</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Process_LSA_5</fullName>
        <description>Att Care &amp; Support $110,000 - $550,000</description>
        <field>Approval_process__c</field>
        <formula>&apos;Att Care &amp; Support $110,000 - $550,000&apos;</formula>
        <name>Set Approval Process LSA 5</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Process_LSA_6</fullName>
        <description>NOT Att Care &amp; Support &lt;= $5,500</description>
        <field>Approval_process__c</field>
        <formula>&apos;NOT Att Care &amp; Support &lt;= $5,500&apos;</formula>
        <name>Set Approval Process LSA 6</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Process_LSA_7</fullName>
        <description>NOT Att Care &amp; Support $5,500 - $11,000</description>
        <field>Approval_process__c</field>
        <formula>&apos;NOT Att Care &amp; Support $5,500 - $11,000&apos;</formula>
        <name>Set Approval Process LSA 7</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Process_LSA_8</fullName>
        <description>NOT Att Care &amp; Support $11,000 - $55,000</description>
        <field>Approval_process__c</field>
        <formula>&apos;NOT Att Care &amp; Support $11,000 - $55,000&apos;</formula>
        <name>Set Approval Process LSA 8</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Process_LSA_9</fullName>
        <description>NOT Att Care &amp; Support $55,000-$110,000</description>
        <field>Approval_process__c</field>
        <formula>&apos;NOT Att Care &amp; Support $55,000-$110,000&apos;</formula>
        <name>Set Approval Process LSA 9</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approved_Adhoc_Cost</fullName>
        <field>Approved_Adhoc_Service_Cost__c</field>
        <formula>Original_Adhoc_Service_Cost__c</formula>
        <name>Set Approved Adhoc Cost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approver_ID</fullName>
        <field>Approver_User_ID__c</field>
        <formula>$User.Id</formula>
        <name>Set Approver ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approver_ID_Recall</fullName>
        <field>Approver_User_ID_Recall__c</field>
        <formula>$User.Id</formula>
        <name>Set Approver ID (Recall)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approver_Name</fullName>
        <field>Approver_User_Name__c</field>
        <formula>$User.FirstName + &apos; &apos; + $User.LastName</formula>
        <name>Set Approver Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approver_Name_Recall</fullName>
        <field>Approver_User_Name_Recall__c</field>
        <formula>$User.FirstName + &apos; &apos; + $User.LastName</formula>
        <name>Set Approver Name (Recall)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CSA_Accepted_On</fullName>
        <description>set the CSA Accepted on to the time/date that the new request/amendment was approved</description>
        <field>CSA_Acceptance_Date__c</field>
        <formula>now()</formula>
        <name>Set CSA Accepted On</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CSA_Status_Accepted</fullName>
        <field>CSA_Status__c</field>
        <literalValue>Accepted</literalValue>
        <name>Set CSA Status Accepted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Cancellation_Approval_Date</fullName>
        <field>Cancellation_Approval_Date__c</field>
        <formula>NOW()</formula>
        <name>Set Cancellation Approval Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Cancellation_Approval_User</fullName>
        <field>Cancellation_Approver__c</field>
        <formula>$User.FullName__c</formula>
        <name>Set Cancellation Approval User</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Cancellation_Date_Recall</fullName>
        <field>Cancellation_Date_Recall__c</field>
        <formula>RCR_Cancellation_Date__c</formula>
        <name>Set Cancellation Date Recall</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Date_Allocation_Approved</fullName>
        <field>Allocation_Approved_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Date Allocation Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Process_032</fullName>
        <field>Approval_process__c</field>
        <formula>&apos;Service Request - Request Approval 03.2&apos;</formula>
        <name>Set Process 03.2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Process_Rollover_Top_Up</fullName>
        <field>Approval_process__c</field>
        <formula>&quot;Allocation - Rollover Top Up&quot;</formula>
        <name>Set Process Rollover Top Up</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Process_Service_Request_Direct</fullName>
        <field>Approval_process__c</field>
        <formula>&apos;Service Request - Direct Contract 01.0&apos;</formula>
        <name>Set Process Service Request Direct</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Process_Service_Request_Request_Ap</fullName>
        <field>Approval_process__c</field>
        <formula>&apos;Service Request - Request Approval 01.3&apos;</formula>
        <name>Set Process Service Request - Request Ap</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_RCR_Cancellation_Date</fullName>
        <description>Sets the RCR Cancellation Date when cancellation is approved.</description>
        <field>RCR_Cancellation_Date__c</field>
        <formula>Requested_Cancellation_Date__c</formula>
        <name>Set RCR Cancellation Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Recalled</fullName>
        <field>Approval_Request_Recalled__c</field>
        <literalValue>1</literalValue>
        <name>Set Recalled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Scheduled_Service_Cost</fullName>
        <field>Approved_Scheduled_Service_Cost__c</field>
        <formula>Original_Scheduled_Service_Cost__c</formula>
        <name>Set Scheduled Service Cost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Service_Order_Number</fullName>
        <field>Name</field>
        <formula>IF(NOT(ISBLANK(Client__r.IF_Number__c)), &quot;IF&quot; &amp; TEXT(Client__r.IF_Number__c), TEXT(VALUE(Sequence_Number__c)))</formula>
        <name>Set Service Order Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Service_Request_Approval_Recalled</fullName>
        <field>Service_Request_Approval_Recalled__c</field>
        <literalValue>1</literalValue>
        <name>Set Service Request Approval Recalled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_on_Submit</fullName>
        <field>Status_Recall__c</field>
        <formula>TEXT(Status__c)</formula>
        <name>Set Status on Submit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_New</fullName>
        <description>Set the Staus to new request</description>
        <field>Status__c</field>
        <literalValue>New</literalValue>
        <name>Set Status to New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Step_Allocation_Blank_Value</fullName>
        <field>Approval_Process_Step__c</field>
        <name>Set Step Allocation - Blank Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Step_Allocation_CRAU_Recommend</fullName>
        <field>Approval_Process_Step__c</field>
        <formula>&apos;Allocation - CRAU Recommendation&apos;</formula>
        <name>Set Step Allocation - CRAU Recommend</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Step_Allocation_Director_Approval</fullName>
        <field>Approval_Process_Step__c</field>
        <formula>&apos;Allocation - Director Approval&apos;</formula>
        <name>Set Step Allocation - Director Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Step_Allocation_Regional_Manager</fullName>
        <field>Approval_Process_Step__c</field>
        <formula>&apos;Allocation - Regional Manager Approval&apos;</formula>
        <name>Set Step Allocation - Regional Manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Step_Allocation_Service_Coord</fullName>
        <field>Approval_Process_Step__c</field>
        <formula>&apos;Coordinator Approval&apos;</formula>
        <name>Set Step Allocation - Service Coord.</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Step_Allocation_Team_Leader</fullName>
        <field>Approval_Process_Step__c</field>
        <formula>&apos;Allocation - Team Leader Approval&apos;</formula>
        <name>Set Step Allocation - Team Leader</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Submitted_For_Approval_User_ID</fullName>
        <field>Submited_For_Approval_User_ID__c</field>
        <formula>$User.Id</formula>
        <name>Set Submitted For Approval User ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Submitted_For_Approval_User_Name</fullName>
        <field>Submited_For_Approval_User__c</field>
        <formula>$User.FirstName + &apos; &apos; + $User.LastName</formula>
        <name>Set Submitted For Approval User Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Visible_To_Service_Providers</fullName>
        <field>Visible_To_Service_Providers__c</field>
        <literalValue>1</literalValue>
        <name>Set Visible To Service Providers</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CSA_Status_Pending_Approval</fullName>
        <field>CSA_Status__c</field>
        <literalValue>Pending Approval</literalValue>
        <name>Update CSA Status Pending Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Add Audit</fullName>
        <actions>
            <name>Add_to_audit</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_minor_changes</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Minor_Changes__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contract Review</fullName>
        <active>false</active>
        <criteriaItems>
            <field>RCR_Contract__c.End_Date__c</field>
            <operation>greaterThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Contract_Review</name>
                <type>Task</type>
            </actions>
            <offsetFromField>RCR_Contract__c.End_Date__c</offsetFromField>
            <timeLength>-14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Notify Coordinator Review Required</fullName>
        <actions>
            <name>Notify_Service_Coordinator_Review_Rollover</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>Record_Type_Name__c = &apos;Rollover&apos; &amp;&amp; 
ISPICKVAL(Rollover_Status__c, &apos;Pending Coordinator Review&apos;) &amp;&amp; 
ISCHANGED(Service_Coordinator__c) &amp;&amp; 
NOT(ISBLANK(Service_Coordinator__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>RCR Once Off Contract Update</fullName>
        <active>true</active>
        <description>Every &apos;Once Off&apos; contract informing the Servcie Coordinator that the Servcie Order will end within one month and they need to ensure that if applicable a new contract is in place for the continuation of service for the client.</description>
        <formula>NOT(ISBLANK(Once_Off_Min_End_Date__c)) 
&amp;&amp; Record_Type_Name__c = &quot;Service Order&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>RCR_Once_off_ending_update</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>RCR_Contract__c.Once_Off_Min_End_Date__c</offsetFromField>
            <timeLength>-56</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Request Approved</fullName>
        <actions>
            <name>New_Request_Approved</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>Record_Type_Name__c != &apos;Rollover&apos; &amp;&amp; ISPICKVAL(Status__c, &apos;Approved&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Contract_Review</fullName>
        <assignedTo>Service_Planners</assignedTo>
        <assignedToType>role</assignedToType>
        <description>Contract is due to expire in 2 weeks</description>
        <dueDateOffset>-3</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>RCR_Contract__c.End_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Contract Review</subject>
    </tasks>
</Workflow>
