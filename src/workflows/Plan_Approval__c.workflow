<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Chief_Executive_about_Plan_Approval</fullName>
        <description>Email Chief Executive about Plan Approval</description>
        <protected>false</protected>
        <recipients>
            <recipient>Chief_Executive</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>lifetime.support@sa.gov.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Form_Email_Templates/Chief_Executive_Plan_Approval_Required</template>
    </alerts>
    <alerts>
        <fullName>Email_Lead_Service_Planner_about_Plan_Approval</fullName>
        <description>Email Lead Service Planner about Plan Approval</description>
        <protected>false</protected>
        <recipients>
            <recipient>Lead_Service_Planner</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>lifetime.support@sa.gov.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Form_Email_Templates/Lead_Service_Planner_Plan_Approval_Required</template>
    </alerts>
    <alerts>
        <fullName>Email_Manager_ASD_about_Plan_Approval</fullName>
        <description>Email Manager ASD about Plan Approval</description>
        <protected>false</protected>
        <recipients>
            <recipient>Manager_ASD</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>lifetime.support@sa.gov.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Form_Email_Templates/Manager_ASD_Plan_Approval_Required</template>
    </alerts>
    <alerts>
        <fullName>Email_Service_Planners_about_Plan_Approval</fullName>
        <description>Email Service Planners about Plan Approval</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_Service_Planner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>lifetime.support@sa.gov.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Form_Email_Templates/Service_Planner_Plan_Approval_Required</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_Approved_Estimated_Plan_Cost</fullName>
        <field>Approved_Plan_Cost__c</field>
        <formula>Estimated_Plan_Cost__c</formula>
        <name>Set Approved Estimated Plan Cost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Assigned_Service_Planner_Email</fullName>
        <field>Assigned_Service_Planner_Email__c</field>
        <formula>Participant_Plan__r.Client__r.Allocated_to__r.Email</formula>
        <name>Set Assigned Service Planner Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Plan_Approval_Date_to_today</fullName>
        <field>Plan_Approval_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Plan Approval Date to today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Plan_Approval_Owner</fullName>
        <field>OwnerId</field>
        <lookupValue>neil@lsa.gov</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set Plan Approval Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Chief Executive Plan Approval</fullName>
        <actions>
            <name>Email_Chief_Executive_about_Plan_Approval</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Plan_Approval__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Chief Executive Approval</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Service Planner Plan Approval</fullName>
        <actions>
            <name>Email_Lead_Service_Planner_about_Plan_Approval</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Plan_Approval__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Lead Service Planner Recommendation</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Manager ASD Plan Approval</fullName>
        <actions>
            <name>Email_Manager_ASD_about_Plan_Approval</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Plan_Approval__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Manager ASD Recommendation</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Plan Approval Created</fullName>
        <actions>
            <name>Set_Assigned_Service_Planner_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Plan Approved</fullName>
        <actions>
            <name>Set_Approved_Estimated_Plan_Cost</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Plan_Approval_Date_to_today</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Plan_Approval__c.Chief_Executive_Decision__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Plan_Approval__c.Plan_Approval_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Service Planner Plan Approval</fullName>
        <actions>
            <name>Email_Service_Planners_about_Plan_Approval</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>RecordType.DeveloperName == &apos;Service_Planner_Recommendation&apos;
&amp;&amp; ISCHANGED(RecordTypeId) 
&amp;&amp; NOT(ISNEW())</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
