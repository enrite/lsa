<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <content>RCR_Scheduled_Service</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <content>RCR_Scheduled_Service</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <content>RCRContractScheduledServiceRedirect</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Adhoc_Service_Description__c</fullName>
        <description>Used as a reference data field to populate the Service Description for the Contract Scheduled Service</description>
        <externalId>false</externalId>
        <label>Adhoc Service Description</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Cancellation_Date__c</fullName>
        <description>The Cancellation date of the Scheduled Service.</description>
        <externalId>false</externalId>
        <formula>RCR_Contract__r.Cancellation_Date__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Cancellation Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Client__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <description>The client this scheduled service is for. Only required when a Service Order relates to multiple clients (eg block grant). If a Service Order is for a single client then this field will be empty and the client will be recorded at the Servcie Order level</description>
        <externalId>false</externalId>
        <inlineHelpText>The client this scheduled service is for. Only required when a Service Order relates to multiple clients (eg block grant). If a Service Order is for a single client then this field will be empty and the client will be recorded at the Servcie Order level</inlineHelpText>
        <label>Client</label>
        <referenceTo>Referred_Client__c</referenceTo>
        <relationshipLabel>RCR Contract Scheduled Services</relationshipLabel>
        <relationshipName>RCR_Contract_Scheduled_Services</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Comment__c</fullName>
        <description>Comments about the Scheduled Service.</description>
        <externalId>false</externalId>
        <label>Comment</label>
        <length>32768</length>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>End_Date__c</fullName>
        <description>The date that the scheduled service is scheduled to end. (Equal to the Original End Date or the Cancellation Date).</description>
        <externalId>false</externalId>
        <formula>IF( NOT( ISBLANK( Cancellation_Date__c ) ) , 
        IF( Cancellation_Date__c &lt;  Original_End_Date__c ,  Cancellation_Date__c ,  Original_End_Date__c ) ,  
Original_End_Date__c )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Actual End Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Exception_Count__c</fullName>
        <externalId>false</externalId>
        <label>Exception Count</label>
        <summaryForeignKey>RCR_Scheduled_Service_Exception__c.RCR_Contract_Scheduled_Service__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Expenses__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Expenses</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Flexibility__c</fullName>
        <description>Describes with what flexibility the Scheduled Service may be delivered.</description>
        <externalId>false</externalId>
        <label>Flexibility</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Flexible within a Period</fullName>
                    <default>false</default>
                    <label>Flexible within a Period</label>
                </value>
                <value>
                    <fullName>Flexible across a Period</fullName>
                    <default>false</default>
                    <label>Flexible across a Period</label>
                </value>
                <value>
                    <fullName>Flexible across Scheduled Service Date Range</fullName>
                    <default>false</default>
                    <label>Flexible across Scheduled Service Date Range</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Funding_Commitment__c</fullName>
        <externalId>false</externalId>
        <label>Funding Commitment</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Recurrent</fullName>
                    <default>false</default>
                    <label>Recurrent</label>
                </value>
                <value>
                    <fullName>Once Off</fullName>
                    <default>true</default>
                    <label>Once Off</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>GL_Category__c</fullName>
        <externalId>false</externalId>
        <label>GL Category</label>
        <length>100</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Internal_Comments__c</fullName>
        <description>Comments about the scheduled service that can only be seen by departmental users.</description>
        <externalId>false</externalId>
        <label>Internal Comments</label>
        <length>32768</length>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Negotiated_Rate_Public_Holidays__c</fullName>
        <description>The negotiated Public Holiday rate that applies for this Scheduled Service.</description>
        <externalId>false</externalId>
        <label>Negotiated Rate (Public Holidays)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Negotiated_Rate_Standard__c</fullName>
        <description>The negotiated standard rate that applies for this Scheduled Service.</description>
        <externalId>false</externalId>
        <label>Negotiated Rate (Standard)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Original_End_Date__c</fullName>
        <description>The date that the Scheduled Service was scheduled to end when the Scheduled Service was first created.</description>
        <externalId>false</externalId>
        <label>Original End Date</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Pre_Approved_Plan_Action__c</fullName>
        <externalId>false</externalId>
        <formula>RCR_Contract__r.Plan_Action__r.RecordType.DeveloperName == &apos;Preapproved_Products_and_Equipment&apos; || 
RCR_Contract__r.Plan_Action__r.RecordType.DeveloperName == &apos;Preapproved_Discharge_Service&apos;</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Pre-Approved Plan Action</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Pre_Cancellation_Cost__c</fullName>
        <description>The value of the Scheduled Service at the time that the Scheduled Service was cancelled.</description>
        <externalId>false</externalId>
        <label>Precancellation Cost</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Public_holidays_not_allowed__c</fullName>
        <externalId>false</externalId>
        <label>Exceptions</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>No, normal scheduled services are not contracted on Public Holidays</fullName>
                    <default>false</default>
                    <label>No, normal scheduled services are not contracted on Public Holidays</label>
                </value>
                <value>
                    <fullName>No, Public Holidays are excluded</fullName>
                    <default>false</default>
                    <label>No, Public Holidays are excluded</label>
                </value>
                <value>
                    <fullName>Yes, normal scheduled services continue and are paid at Public Holiday Rates</fullName>
                    <default>false</default>
                    <label>Yes, normal scheduled services continue and are paid at Public Holiday Rates</label>
                </value>
                <value>
                    <fullName>Special arrangements have been agreed for Public holidays or other days, replacing normal schedule services</fullName>
                    <default>false</default>
                    <label>Special arrangements have been agreed for Public holidays or other days, replacing normal schedule services</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Quantity_Used__c</fullName>
        <externalId>false</externalId>
        <label>Quantity Used</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>RCR_Contract__c</fullName>
        <description>The Service Order record that this Scheduled Service is related to.</description>
        <externalId>false</externalId>
        <label>Service Order</label>
        <referenceTo>RCR_Contract__c</referenceTo>
        <relationshipLabel>RCR Contract Scheduled Services</relationshipLabel>
        <relationshipName>RCR_Contract_Scheduled_Services</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>RCR_Program_Category_Service_Type__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>The RCR Program Category Service Type record to which the Scheduled Service record is related.</description>
        <externalId>false</externalId>
        <label>RCR Program Category Service Type</label>
        <referenceTo>RCR_Program_Category_Service_Type__c</referenceTo>
        <relationshipLabel>RCR Contract Scheduled Services</relationshipLabel>
        <relationshipName>RCR_Contract_Scheduled_Services</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>RCR_Service__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <description>The RCR Service record that the Scheduled Service record is related to.</description>
        <externalId>false</externalId>
        <label>Service</label>
        <referenceTo>RCR_Service__c</referenceTo>
        <relationshipLabel>RCR Contract Scheduled Services</relationshipLabel>
        <relationshipName>RCR_Contract_Scheduled_Services</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>RCR_Sub_Program__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>OBSOLETE</description>
        <externalId>false</externalId>
        <label>RCR Sub Program</label>
        <referenceTo>RCR_Sub_Program__c</referenceTo>
        <relationshipLabel>RCR Contract Scheduled Services</relationshipLabel>
        <relationshipName>RCR_Contract_Scheduled_Services</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Remaining__c</fullName>
        <description>The value of the Scheduled service that has not yet been invoiced.</description>
        <externalId>false</externalId>
        <formula>Total_Cost__c -  Total_Submitted__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Remaining</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Rollover_Comment__c</fullName>
        <externalId>false</externalId>
        <label>Rollover Comment</label>
        <length>10000</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Schedule_Service_ID__c</fullName>
        <externalId>true</externalId>
        <label>Schedule Service ID</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Scheduled_Service_ID__c</fullName>
        <externalId>false</externalId>
        <formula>HYPERLINK(&apos;/SPPortalScheduledService?id=&apos; + Id,  Name, &apos;_top&apos;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Scheduled Service ID</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Service_Code__c</fullName>
        <description>The RCR Service Name.</description>
        <externalId>false</externalId>
        <formula>RCR_Service__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Service Code</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Service_Description__c</fullName>
        <description>The description of the RCR Service that will be provided. (From the RCR Service record).</description>
        <externalId>false</externalId>
        <formula>BLANKVALUE(Adhoc_Service_Description__c , RCR_Service__r.Description__c)</formula>
        <label>Service Description</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Service_Level__c</fullName>
        <description>The Level of the RCR Service that will be provided. (From the RCR Service record).</description>
        <externalId>false</externalId>
        <formula>TEXT(RCR_Service__r.Level__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Service Level</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Service_Types_Desc__c</fullName>
        <description>The Service Types that can be supplied under this Scheduled Service. (Displayed with carriage returns).</description>
        <externalId>false</externalId>
        <formula>SUBSTITUTE(Service_Types__c, &apos;&lt;br/&gt;&apos;, &apos;&apos; )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Service Types</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Service_Types__c</fullName>
        <description>The Service Types that can be supplied under this Scheduled Service.</description>
        <externalId>false</externalId>
        <label>Service Types</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Source_Record__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Source Record</label>
        <referenceTo>RCR_Contract_Scheduled_Service__c</referenceTo>
        <relationshipLabel>RCR Contract Scheduled Services</relationshipLabel>
        <relationshipName>Amendments</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Standard_Service_Code_Description__c</fullName>
        <externalId>false</externalId>
        <formula>RCR_Service__r.Name + IF(ISBLANK(Standard_Service_Code__c), &apos;&apos;, &apos; - &apos; +  Standard_Service_Code__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Standard Service Code Description</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Standard_Service_Code__c</fullName>
        <externalId>false</externalId>
        <formula>RCR_Service__r.Standard_Service_Code__r.Code__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Standard Service Code</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Start_Date__c</fullName>
        <description>The date that the Scheduled Service is scheduled to start.</description>
        <externalId>false</externalId>
        <label>Start Date</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Total_Cost__c</fullName>
        <defaultValue>0</defaultValue>
        <description>The total value of this scheduled service.</description>
        <externalId>false</externalId>
        <label>Total Cost</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Total_Quantity__c</fullName>
        <description>The total number of activities scheduled for this Scheduled Service.</description>
        <externalId>false</externalId>
        <label>Total Quantity</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Total_Submitted__c</fullName>
        <description>The total value of the activities that have been submitted for payment for this Scheduled Service.</description>
        <externalId>false</externalId>
        <label>Total Submitted</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Use_Negotiated_Rates__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Indicates that special Negotiated Rates apply for this Scheduled Service.</description>
        <externalId>false</externalId>
        <inlineHelpText>Indicates that special Negotiated Rates apply for this Scheduled Service.</inlineHelpText>
        <label>Use Negotiated Rates</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>RCR Contract Scheduled Service</label>
    <nameField>
        <displayFormat>CSA{000000000}</displayFormat>
        <label>Scheduled Service Name</label>
        <trackHistory>true</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>RCR Contract Scheduled Services</pluralLabel>
    <searchLayouts>
        <lookupDialogsAdditionalFields>RCR_Service__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Service_Code__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Service_Level__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Service_Description__c</lookupDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>End_Date_Within_Contract_Period</fullName>
        <active>false</active>
        <description>The End Date must be between the Contract Start and End Dates.</description>
        <errorConditionFormula>OR(Original_End_Date__c &lt; RCR_Contract__r.Start_Date__c, Original_End_Date__c &gt; RCR_Contract__r.Original_End_Date__c )</errorConditionFormula>
        <errorMessage>The End Date must be within the Contracted Period.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Negotiated_Rates_Have_Not_Been_Provided</fullName>
        <active>true</active>
        <description>If the Use Negotiated Rates flag has been set then both Standard and Public Holiday rates must be provided.</description>
        <errorConditionFormula>Use_Negotiated_Rates__c &amp;&amp; (Negotiated_Rate_Standard__c &lt;= 0.0 || ISBLANK(Negotiated_Rate_Standard__c) || Negotiated_Rate_Public_Holidays__c &lt;= 0.0 || ISBLANK(Negotiated_Rate_Public_Holidays__c))</errorConditionFormula>
        <errorMessage>If Negotiated Rates are to be used for this Scheduled Service then both the negotiated Standard and Public Holiday Rates must be greater than zero.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Public_Holidays_Options_Mandatory</fullName>
        <active>true</active>
        <description>Check to see that an option has been selected for chosen</description>
        <errorConditionFormula>ISPICKVAL(Public_holidays_not_allowed__c, &quot;&quot;)</errorConditionFormula>
        <errorDisplayField>Public_holidays_not_allowed__c</errorDisplayField>
        <errorMessage>A Public holidays option must be selected</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Scheduled_Service_Locked</fullName>
        <active>false</active>
        <errorConditionFormula>RCR_Contract__r.RecordType.Name  = &apos;Rollover&apos; &amp;&amp;  (ISPICKVAL(RCR_Contract__r.Rollover_Status__c, &apos;Approved&apos;)  || ISPICKVAL(RCR_Contract__r.Rollover_Status__c, &apos;Bulk Approved&apos;))</errorConditionFormula>
        <errorMessage>You cannot update the Scheduled Service once the Rollover Request has been &apos;Approved&apos;.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Start_Date_Before_End_Date</fullName>
        <active>true</active>
        <description>The Contract Scheduled Service Start Date must be before the End Date.</description>
        <errorConditionFormula>Start_Date__c &gt;  Original_End_Date__c</errorConditionFormula>
        <errorDisplayField>Start_Date__c</errorDisplayField>
        <errorMessage>The Start Date Must be before the End Date</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Start_Date_Within_Contract_Period</fullName>
        <active>true</active>
        <errorConditionFormula>OR(Start_Date__c &lt;  RCR_Contract__r.Start_Date__c,  Start_Date__c &gt;  RCR_Contract__r.Original_End_Date__c )</errorConditionFormula>
        <errorMessage>The Start Date is not within the Contracted Period</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Updated_EndDate_After_Contract_EndDate</fullName>
        <active>false</active>
        <errorConditionFormula>NOT(ISNEW()) &amp;&amp; ISCHANGED(Original_End_Date__c) &amp;&amp; Original_End_Date__c &gt;  RCR_Contract__r.End_Date__c</errorConditionFormula>
        <errorMessage>The End Date for this service must not be later than the Actual End Date for the Service Order.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Updated_during_rollover</fullName>
        <active>true</active>
        <errorConditionFormula>(ISNEW() || LastModifiedDate &lt;&gt; PRIORVALUE(LastModifiedDate)) &amp;&amp;
 ISPICKVAL( RCR_Contract__r.Rollover_Status__c, &apos;Queued&apos;)</errorConditionFormula>
        <errorMessage>Scheduled Service cannot be altered while the Service Order is queued for rollover.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
    <webLinks>
        <fullName>New_RCR_Contract_Scheduled_Service</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <linkType>javascript</linkType>
        <masterLabel>New RCR Contract Scheduled Service</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <requireRowSelection>false</requireRowSelection>
        <url>if (&apos;{!RCR_Contract__c.Status__c}&apos; != &apos;New&apos; &amp;&amp; &apos;{!$Profile.Name}&apos; != &apos;System Administrator&apos;)
{
alert(&apos;Scheduled services for a Service Order can only be changed via an Amendment to the Service Order.&apos;);
}
else
{
top.location.href = &apos;/apex/RCR_Scheduled_Service?CF00NU0000003z1yq=LSA00148&amp;CF00NU0000003z1yq_lkid={!RCR_Contract__c.Id}&amp;retURL=/{!RCR_Contract__c.Id}&apos;;
}</url>
    </webLinks>
    <webLinks>
        <fullName>UsageStatusReport</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Usage Status Report</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <url>var screenHeight; 
var screenWidth; 

if(isIE) 
{ 
    screenHeight = screen.height; 
    screenWidth = screen.width; 
} 
else 
{ 
    screenHeight = window.innerHeight; 
    screenWidth = window.innerWidth; 
} 

var popupTop = (screenHeight - 800)/2; 
var popupLeft = (screenWidth - 1200)/2; 

window.open(&apos;{!$Site.Prefix}&apos; + &apos;/apex/RCRScheduledServiceCostBreakdown?css={!RCR_Contract_Scheduled_Service__c.Id}&apos;,&apos;RCRScheduledServiceCostBreakdown&apos;, &apos;titlebar=no,,resizable=yes,scrollbars=yes,top=&apos; + popupTop + &apos;px,left=&apos; + popupLeft + &apos;px&apos;);</url>
    </webLinks>
</CustomObject>
