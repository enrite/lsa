trigger trgParticipantPlanAfterUD on Plan__c (after update, after delete)
{
//    // update any service orders that are attached to the plan actions for this plan
//    if(Trigger.isDelete)
//    {
//        update
//        [
//                SELECT Id
//                FROM RCR_Contract__c
//                WHERE Plan_Action__r.Participant_Plan__c IN :Trigger.OldMap.keySet()
//        ];
//    }
    switch on Trigger.operationType
    {
        when AFTER_DELETE
        {
            update
            [
                    SELECT Id
                    FROM RCR_Contract__c
                    WHERE Plan_Action__r.Participant_Plan__c IN :Trigger.OldMap.keySet()
            ];
        }
        when AFTER_UPDATE
        {
            Set<ID> planIds = new Set<ID>();
                    for(Plan__c p : Trigger.new)
            {
                Plan__c oldValue = Trigger.oldMap.get(p.ID);
                if(p.Plan_End_Date__c != oldValue.Plan_End_Date__c)
                {
                    planIds.add(p.ID);
                }
            }
            if(!planIds.isEmpty())
            {
                delete [SELECT  ID
                        FROM    Task
                        WHERE   WhatID IN :planIds AND
                                Plan_End_Date_Task__c = true];
            }
        }
    }
}