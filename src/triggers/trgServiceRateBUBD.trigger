trigger trgServiceRateBUBD on RCR_Service_Rate__c (before delete) 
{
    Map<Id, List<RCR_Contract_Scheduled_Service__c>> scheduledServicesByService = new Map<Id, List<RCR_Contract_Scheduled_Service__c>>();    
    for(RCR_Contract_Scheduled_Service__c css : [SELECT Id,
                                                     Name,
                                                     Start_Date__c,
                                                     End_Date__c,
                                                     RCR_Service__c
                                              FROM   RCR_Contract_Scheduled_Service__c
                                              WHERE  RCR_Service__c IN (SELECT  RCR_Service__c
                                                                        FROM    RCR_Service_Rate__c
                                                                        WHERE   Id IN :Trigger.oldMap.keySet())])
    {
        if(!scheduledServicesByService.containsKey(css.RCR_Service__c))
        {
            scheduledServicesByService.put(css.RCR_Service__c, new List<RCR_Contract_Scheduled_Service__c>());
        }
        scheduledServicesByService.get(css.RCR_Service__c).add(css);
    }
                                                                                                                                            
    for(RCR_Service_Rate__c sr : Trigger.old)
    {
        if(scheduledServicesByService.containsKey(sr.RCR_Service__c))
        {
            for(RCR_Contract_Scheduled_Service__c rcss : scheduledServicesByService.get(sr.RCR_Service__c))
            {
                if(A2HCUtilities.dateRangesOverlap(rcss.Start_Date__c, rcss.End_Date__c, sr.Start_Date__c, sr.End_Date__c))
                {
                    sr.addError('You cannot delete the rate if it has been used by a Scheduled Service.');
                    break;
                }
            }
        }
    }
}