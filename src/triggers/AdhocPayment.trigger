trigger AdhocPayment on Adhoc_Payment__c(before insert, before update) 
{
    Set<Id> participantIds = new Set<Id>();
    for(Adhoc_Payment__c record : Trigger.New)
	{
        participantIds.add(record.Participant__c);
    }
    Map<ID, Referred_Client__c> clientsById = new Map<ID, Referred_Client__c>(
                                            [SELECT ID,
                                                    Allocated_To__c,
                                                    RecordType.DeveloperName,
                                                    Current_Status_Date__c,
                                                    Current_Status__c,
                                                    Current_Participant__c
                                            FROM    Referred_Client__c
                                            WHERE   ID IN :participantIds]);

	for(Adhoc_Payment__c record : Trigger.New)
	{
        Referred_Client__c client = clientsById.get(record.Participant__c);
        if(client != null)
        {
            record.Participant_Service_Planner__c = client.Allocated_To__c;
            if(client.RecordType.DeveloperName == 'LSA_Participant' && record.Date_of_Transaction__c != null)
            {
//                if((client.Current_Status__c == 'Lifetime participant' ||
//                        client.Current_Status__c == 'Accepted as Interim Participant' ||
//                        client.Current_Status__c == 'Interim Extended' ||
//                        client.Current_Status__c == 'Transitioning out'))
//                {
                if(client.Current_Participant__c)
                {
                    if(client.Current_Status_Date__c == null)
                    {
                        record.addError('Participant does not have a current status date');
                    }
//                    else if(record.Date_of_Transaction__c < client.Current_Status_Date__c)
//                    {
//                        record.addError('Date of Transaction must be on or after the date the participant was accepted into the scheme (' + client.Current_Status_Date__c.format() + ')');
//                    }
                }
                else
                {
                    record.addError('Participant has not been accepted into the scheme' + (client.Current_Status__c == null ? '' : (', their current status is ' + client.Current_Status__c)));
                }
            }
        }
		if(record.NonGST_Amount__c != null || record.NonGST_GL_Code__c != null || record.NonGST_Expense_Description__c != null)
        {
            if(record.NonGST_Amount__c == null)
            {
                record.NonGST_Amount__c.addError('You must enter a value');
            }
            if(record.NonGST_GL_Code__c == null)
            {
                record.NonGST_GL_Code__c.addError('You must enter a value');
            }
        }
        if(record.Amount__c != null || record.GL_Code__c != null || record.Expense_Description__c != null || record.GST__c != null)
        {
            if(record.Amount__c == null)
            {
                record.Amount__c.addError('You must enter a value');
            }
            if(record.GL_Code__c == null)
            {
                record.GL_Code__c.addError('You must enter a value');
            }
            if(record.GST__c == null)
            {
                record.GST__c.addError('You must enter a value');
            }
        }
	}
}