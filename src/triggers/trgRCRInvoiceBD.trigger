trigger trgRCRInvoiceBD on RCR_Invoice__c (before delete)
{
    for(RCR_Invoice__c inv : Trigger.old)
    {
        if(inv.Batch_Number__c != null)
        {
            inv.addError('Cannot delete an activity statement once it has been submitted for payment.');
        }
    }
}