trigger trgExternalExpenseLineItemAfterU on External_Expense_Line_Item__c (after update) 
{  
    // find the list of adhoc payments that have just become approved
    List<External_Expense_Line_Item__c> approved = new List<External_Expense_Line_Item__c>();
    for (External_Expense_Line_Item__c i : Trigger.New)
    {
        if (i.Approved__c
            && !Trigger.OldMap.get(i.Id).Approved__c
            && i.Participant__c != null)
        {           
            approved.add(i);           
        }            
    }

    if (approved.size() > 0)
    {
        // find some Ids
        Set<String> keys = new Set<String>();    
        Set<Id> expenseIds = new Set<Id>();
        Set<String> glCodes = new Set<String>();  
        for (External_Expense_Line_Item__c i : approved)
        {        
            keys.add(i.Account_Client_Date_Matching_Key__c);        
            expenseIds.add(i.External_Expense__c);     
            glCodes.add(i.GL_Code__c);
        } 
        
        // find the account Ids from the expense records    
        Set<Id> accountIds = new Set<Id>();
        Map<Id, External_Expense__c> expenses = new Map<Id, External_Expense__c> ([SELECT Id,
                                                                                                  Name,
                                                                                                  Account__c
                                                                                           FROM External_Expense__c 
                                                                                           WHERE Id IN :expenseIds]);
        
        for (External_Expense__c e : expenses.values())
        {
            accountIds.add(e.Account__c);
        }                                      
        
        // find service orders that match the keys
        Map<String, RCR_Contract__c> serviceOrders = new Map<String, RCR_Contract__c>();
        for (RCR_Contract__c c : [SELECT Id,
                                         Name,
                                         Account__c,
                                         Client__c,
                                         Start_Date__c,
                                         Original_End_Date__c,
                                         Account_Client_Date_Matching_Key__c
                                  FROM RCR_Contract__c
                                  WHERE RecordType.DeveloperName = 'Service_Order' 
                                  AND Account_Client_Date_Matching_Key__c IN :keys])
        {
            serviceOrders.put(c.Account_Client_Date_Matching_Key__c, c);
        }                                     
        
        // find the service types for the gl codes
        Map<String, RCR_Program_Category_Service_Type__c> serviceTypes = new Map<String, RCR_Program_Category_Service_Type__c>();
        for (RCR_Program_Category_Service_Type__c s : [SELECT Id,
                                                              Name,
                                                              RCR_Program_Category__r.RCR_Program__c,                                                          
                                                              Finance_Code_GG__c,
                                                              Finance_Code_NSAG__c,
                                                              RCR_Program_Category__r.Name    
                                                       FROM RCR_Program_Category_Service_Type__c 
                                                       WHERE Id IN :glCodes])
        {
            serviceTypes.put(s.Id, s);       
        }                                                       
    
        // find the adhoc expenses service records
        Map<String, RCR_Service__c> adhocServices = new Map<String, RCR_Service__c>();
        for (RCR_Service__c s : [SELECT Id,
                                        Account__c
                                 FROM RCR_Service__c
                                 WHERE Account__c IN :accountIds
                                 AND Name = 'Adhoc Expenses'])
        {
            adhocServices.put(s.Account__c, s);
        }                             
        
        // create adhoc expenses records as appropriate
        List<RCR_Service__c> newAdhocServices = new List<RCR_Service__c>();
        for (Id accountId : accountIds)
        {
            if (!adhocServices.containsKey(accountId))
            {
                RCR_Service__c s = new RCR_Service__c();
                s.Account__c = accountId;
                s.Name = 'Adhoc Expenses';
                newAdhocServices.add(s);
                
                adhocServices.put(accountId, s);
            }
        }
        
        // insert any new services
        if (newAdhocServices.size() > 0)
        {
            insert newAdhocServices;
        }       
    
        // loop through the new expense items again to process the service orders
        for (External_Expense_Line_Item__c i : approved)
        {
            // if a service order was found for the item then adjust its dates
            if (serviceOrders.containsKey(i.Account_Client_Date_Matching_Key__c))
            {
                if (serviceOrders.get(i.Account_Client_Date_Matching_Key__c).Start_Date__c > i.Date_of_Transaction__c)
                {
                    serviceOrders.get(i.Account_Client_Date_Matching_Key__c).Start_Date__c = i.Date_of_Transaction__c;                                    
                }
                
                if (serviceOrders.get(i.Account_Client_Date_Matching_Key__c).Original_End_Date__c < i.Date_of_Transaction__c)
                {
                    serviceOrders.get(i.Account_Client_Date_Matching_Key__c).Original_End_Date__c = i.Date_of_Transaction__c;                                    
                }                       
            }
            // if a service order was not found for this payment then create one
            else
            {
                RCR_Contract__c c = new RCR_Contract__c();            
                c.Account__c = expenses.get(i.External_Expense__c).Account__c;   
                c.Client__c = i.Participant__c;                                      
                c.Start_Date__c = i.Date_of_Transaction__c;
                c.Original_End_Date__c = i.Date_of_Transaction__c;                    
                c.RecordTypeId = A2HCUtilities.getRecordType('RCR_Contract__c', 'Service Order').Id;
                c.Status__c = 'Approved';     
                //c.Created_by_Adhoc_Payment__c = true;                   
                serviceOrders.put(i.Account_Client_Date_Matching_Key__c, c);                
            }                
        }
        
        // upsert the service orders
        upsert serviceOrders.values();
        
        // we'll need to make sure the service orders are populated with their new names
        Map<Id, RCR_Contract__c> soDetails = new Map<Id, RCR_Contract__c>();
        for (RCR_Contract__c so : serviceOrders.values())
        {    
            if (so.Name == null)
            {
                soDetails.put(so.Id, so);
            }
        }
            
        for (RCR_Contract__c so : [SELECT Id,
                                          Name
                                   FROM RCR_Contract__c
                                   WHERE Id IN :soDetails.keySet()])
        {        
            soDetails.get(so.Id).Name = so.Name;
        }                               
    
        // create an invoice batch for each expense
        Map<Id, RCR_Invoice_Batch__c> batches = new Map<Id, RCR_Invoice_Batch__c>();
        for (External_Expense__c e : expenses.values())
        {        
            batches.put(e.Id, new RCR_Invoice_Batch__c(Description__c = e.Name,
                                                       Sent_Date__c = Datetime.now(),
                                                       Approved_By__c = UserInfo.getUserId(),
                                                       Approved_Date__c = Datetime.now()));
        }
        insert batches.values();
    
        // loop through the new expense items again to create the scheduled services and activity statements
        List<External_Expense_Line_Item__c> updateExpenses = new List<External_Expense_Line_Item__c>();
        Map<String, RCR_Contract_Scheduled_Service__c> scheduledServices = new Map<String, RCR_Contract_Scheduled_Service__c>();
        Map<String, RCR_Invoice__c> activityStatements = new Map<String, RCR_Invoice__c>();
        for (External_Expense_Line_Item__c i : approved)
        {     
            // update the payments with their respective service orders
            updateExpenses.add(new External_Expense_Line_Item__c(Id = i.Id, RCR_Service_Order__c = serviceOrders.get(i.Account_Client_Date_Matching_Key__c).Id));
               
            RCR_Contract_Scheduled_Service__c css = new RCR_Contract_Scheduled_Service__c();
            css.RCR_Contract__c = serviceOrders.get(i.Account_Client_Date_Matching_Key__c).Id;
            css.RCR_Service__c = adhocServices.get(expenses.get(i.External_Expense__c).Account__c).Id;
            css.Start_Date__c = i.Date_of_Transaction__c;
            css.Original_End_Date__c = i.Date_of_Transaction__c;
            css.Total_Cost__c = i.Amount__c;
            css.Total_Submitted__c = i.Amount__c;
            css.Funding_Commitment__c = 'Once Off';  
            css.Expenses__c = true; 
            css.Public_holidays_not_allowed__c = APS_PicklistValues.RCRContractScheduledService_PublicHolidaysNotAllowed_UseExceptions;
            scheduledServices.put(i.Id, css);  
            
            RCR_Invoice__c a = new RCR_Invoice__c();        
            a.Account__c = expenses.get(i.External_Expense__c).Account__c;        
            a.Invoice_Number__c = i.Name;
            a.Start_Date__c = i.Date_of_Transaction__c;
            a.End_Date__c = i.Date_of_Transaction__c;
            a.Date__c = i.Date_of_Transaction__c;        
            a.Reject__c = false;
            a.Assigned_To__c = UserInfo.getUserId();   
            a.RecordTypeId = A2HCUtilities.getRecordType('RCR_Invoice__c', 'Created').Id;   
            a.RCR_Invoice_Batch__c = batches.get(i.External_Expense__c).Id;
            activityStatements.put(i.Id, a);              
        }
        
        // update the payments
        update updateExpenses;
        
        // insert the scheduled services
        insert scheduledServices.values();
        
        // insert the activity statements
        insert activityStatements.values();
    
        // loop through the new expenses again to create the contract service types and the activity statement items
        List<RCR_Contract_Service_Type__c> contractServiceTypes = new List<RCR_Contract_Service_Type__c>();    
        Map<String, RCR_Invoice_Item__c> asItems = new Map<String, RCR_Invoice_Item__c>();
        for (External_Expense_Line_Item__c i : approved)
        {   
            if (serviceTypes.containsKey(i.GL_Code__c))
            {     
                RCR_Contract_Service_Type__c cst = new RCR_Contract_Service_Type__c();
                cst.RCR_Contract_Scheduled_Service__c = scheduledServices.get(i.Id).Id; 
                cst.RCR_Program__c = serviceTypes.get(i.GL_Code__c).RCR_Program_Category__r.RCR_Program__c;
                cst.RCR_Program_Category_Service_Type__c = serviceTypes.get(i.GL_Code__c).Id;   
                contractServiceTypes.add(cst);
                
                scheduledServices.get(i.Id).Service_Types__c = serviceTypes.get(i.GL_Code__c).Name;
                scheduledServices.get(i.Id).GL_Category__c = serviceTypes.get(i.GL_Code__c).RCR_Program_Category__r.Name;
            }  
            
            RCR_Invoice_Item__c it = new RCR_Invoice_Item__c();
            it.RCR_Invoice__c = activityStatements.get(i.Id).Id;
            it.RCR_Contract_Scheduled_Service__c = scheduledServices.get(i.Id).Id;
            it.Reconciled_Service__c = i.Name;       
            it.Code__c = 'Adhoc Expenses';
            it.RCR_Service_Order__c = serviceOrders.get(i.Account_Client_Date_Matching_Key__c).Id;
            it.Service_Order_Number__c = serviceOrders.get(i.Account_Client_Date_Matching_Key__c).Name;
            it.Activity_Date__c = i.Date_of_Transaction__c;
            it.Comment__c = i.Expense_Description__c;
            it.GST__c = i.GST__c;
            it.Total_Item_GST__c = i.GST__c;        
            it.Total__c = i.Total_Amount__c;
            it.Quantity__c = 1.0;
            it.Rate__c = 1.00;  
            it.RecordTypeId = A2HCUtilities.getRecordType('RCR_Invoice_Item__c', 'Submitted').Id;       
            asItems.put(i.Id, it);
        }
        
        // insert the contract service types
        insert contractServiceTypes;
        
        // update the scheduled services again to ensure the funding details are created correctly
        //update scheduledServices.values();
        
        // insert the activity statement items
        insert asItems.values();
        
        // update the activity statements to submitted
        for (RCR_Invoice__c i : activityStatements.values())
        {
            i.RecordTypeId = A2HCUtilities.getRecordType('RCR_Invoice__c', 'Submitted').Id;       
        }
        update activityStatements.values();
        
        // update the scheduled services again to ensure the funding details are created correctly
        update scheduledServices.values();
        
        // find the funding details for the scheduled services
        Set<Id> scheduleServiceIds = new Set<Id>();
        for (RCR_Contract_Scheduled_Service__c css : scheduledServices.values())
        {
            scheduleServiceIds.add(css.Id);
        }
        
        Map<Id, RCR_Funding_Detail__c> fundingDetails = new Map<Id, RCR_Funding_Detail__c>();
        for (RCR_Funding_Detail__c fd : [SELECT Id,
                                                RCR_Contract_Scheduled_Service__c
                                         FROM RCR_Funding_Detail__c 
                                         WHERE RCR_Contract_Scheduled_Service__c IN :scheduleServiceIds])
        {
            fundingDetails.put(fd.RCR_Contract_Scheduled_Service__c, fd);
        }                                         
        
        // loop through the new expenses again to create the activity statement item funding records        
        List<RCR_Activity_Statement_Item_Funding__c> asiFundings = new List<RCR_Activity_Statement_Item_Funding__c>();
        for (External_Expense_Line_Item__c i : approved)
        { 
            RCR_Activity_Statement_Item_Funding__c asif = new RCR_Activity_Statement_Item_Funding__c();
            asif.RCR_Activity_Statement_Item__c = asItems.get(i.Id).Id;
            asif.RCR_Funding_Detail__c = fundingDetails.get(scheduledServices.get(i.Id).Id).Id;
            asif.RCR_Service_Order__c = serviceOrders.get(i.Account_Client_Date_Matching_Key__c).Id; 
            asif.Reconcile_Excess__c = 0.0; 
            asif.Reconcile_GST_Excess__c = 0.0;
            asif.Amount__c = i.Amount__c;
            asif.GST__c = i.GST__c;
            asiFundings.add(asif);
        }
        
        insert asiFundings;
    }
}