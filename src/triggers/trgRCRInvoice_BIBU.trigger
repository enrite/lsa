trigger trgRCRInvoice_BIBU on RCR_Invoice__c (before insert, before update, after update)
{
    if(TriggerByPass.RCRInvoice)
    {
        return;
    }
//    Set<SObjectField> fieldsCanUpdate = new  Set<SObjectField>{
//       RCR_Invoice__c.RCR_Invoice_Batch__c,
//            RCR_Invoice__c.RecordTypeId,
//            RCR_Invoice__c.Create_30_Day_Tasks__c
//    };
    Set<String> invoiceNumbers = new Set<String>();
    for(RCR_Invoice__c inv : Trigger.new)
    {
        invoiceNumbers.add(inv.Invoice_number__c);
    }
    Map<String, List<RCR_Invoice__c>> invoicesByInvoiceNumber = new Map<String, List<RCR_Invoice__c>>();
    if(Trigger.isBefore)
    {
        for(RCR_Invoice__c inv : [SELECT        ID,
                                                Invoice_Number__c,
                                                Account__c
                                    FROM        RCR_Invoice__c
                                    WHERE       Invoice_Number__c IN :invoiceNumbers AND
                                                LP_Invoice_Number__c = null])
        {
            if(!invoicesByInvoiceNumber.containsKey(inv.Invoice_Number__c))
            {
                invoicesByInvoiceNumber.put(inv.Invoice_Number__c, new List<RCR_Invoice__c>());
            }
            invoicesByInvoiceNumber.get(inv.Invoice_Number__c).add(inv);
        }
    }
    for(RCR_Invoice__c i : Trigger.new)
    {
        if(Trigger.isBefore)
        {
            if(Trigger.isUpdate)
            {
                RCR_Invoice__c oldValue = Trigger.oldMap.get(i.ID);
                if(i.LP_Invoice_Key__c != null)
                {
                    if(!UserUtilities.isAdminUser() &&
                            (i.Start_Date__c != oldValue.Start_Date__c ||
                                    i.End_Date__c != oldValue.End_Date__c ||
                                    i.Account__c != oldValue.Account__c ||
                                    i.LP_Invoice_Key__c != oldValue.LP_Invoice_Key__c))
                    {
                        i.addError('LanternPay statements cannot be modified');
                    }
                }
            }

            if(Trigger.isInsert && Userinfo.getUserType() == 'Standard')
            {
                i.Assigned_To__c = UserInfo.getUserId();
            }
            if(i.LP_Invoice_Number__c != null ||
                    !invoicesByInvoiceNumber.containsKey(i.Invoice_Number__c))
            {
                continue;
            }
            for(RCR_Invoice__c otherInv : invoicesByInvoiceNumber.get(i.Invoice_Number__c))
            {
                if(otherInv.Account__c == i.Account__c && otherInv.ID != i.ID)
                {
                    i.addError('Activity statement number ' + i.Invoice_Number__c + ' already exists for this account.');
                }
            }
        }
    }
}