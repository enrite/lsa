trigger trgIF_PlanItemProvider_BIBU on IF_Personal_Budget_Plan_Item_Provider__c (before insert, before update) 
{
    Map<ID, List<Account>> accMap = new Map<ID, List<Account>>();
    for(Account acc : [SELECT Id, Name FROM Account WHERE IsAcceptedPanelProvider__c = 'Yes'])
    {
        //Use Account__c as key so we can search by account later
        if(!accMap.containsKey(acc.Id))
        {
            accMap.put(acc.Id, new List<Account>());
        }
        accMap.get(acc.Id).add(acc);
    }
    
    // MAIN LOOP
    for (IF_Personal_Budget_Plan_Item_Provider__c pip: Trigger.new)
    {
        if(pip.Plan_Item_Service_Type__c == 'Personal Care Support Services')
        {
            //string accName = [SELECT Name FROM Account WHERE Id = :pip.Approved_Panel_Provider__c  LIMIT 1].Name;
            //System.debug('UserInfo.getUserId(): ' + UserInfo.getUserId());
            List<Account> accProv = accMap.get(pip.Approved_Panel_Provider__c);
            
            if(accProv != null)
            {
                pip.Name = accProv[0].Name;
            }
        }
    }
    // END MAIN LOOP
}