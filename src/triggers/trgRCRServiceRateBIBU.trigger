trigger trgRCRServiceRateBIBU on RCR_Service_Rate__c (before insert, before update) 
{
    Set<ID> serviceIds = new Set<ID>();
    for(RCR_Service_Rate__c sr : Trigger.new)
    {
        serviceIds.add(sr.RCR_Service__c);
    }
    
    Map<ID, List<RCR_Service_Rate__c>> ratesByServiceId = new Map<ID, List<RCR_Service_Rate__c>>();
    for(RCR_Service_Rate__c sr : [SELECT    ID,
                                            Start_Date__c,
                                            End_Date__c,
                                            RCR_Service__c
                                    FROM    RCR_Service_Rate__c
                                    WHERE   RCR_Service__c IN :serviceIds])
    {
        if(!ratesByServiceId.containsKey(sr.RCR_Service__c))
        {
            ratesByServiceId.put(sr.RCR_Service__c, new List<RCR_Service_Rate__c>());
        }
        ratesByServiceId.get(sr.RCR_Service__c).add(sr);
    } 
    
    Map<Id, List<RCR_Contract_Scheduled_Service__c>> scheduledServicesByService = new Map<Id, List<RCR_Contract_Scheduled_Service__c>>();
    if(Trigger.isUpdate)
    {
        for(RCR_Contract_Scheduled_Service__c css : [SELECT Id,
                                                         Name,
                                                         Start_Date__c,
                                                         End_Date__c,
                                                         RCR_Service__c
                                                  FROM   RCR_Contract_Scheduled_Service__c
                                                  WHERE  RCR_Service__c IN :serviceIds])
        {
            if(!scheduledServicesByService.containsKey(css.RCR_Service__c))
            {
                scheduledServicesByService.put(css.RCR_Service__c, new List<RCR_Contract_Scheduled_Service__c>());
            }
            scheduledServicesByService.get(css.RCR_Service__c).add(css);
        }
    }
    
    for(RCR_Service_Rate__c sr : Trigger.new)
    {
        if(ratesByServiceId.containsKey(sr.RCR_Service__c))
        {
            for(RCR_Service_Rate__c otherSr : ratesByServiceId.get(sr.RCR_Service__c))
            {
                if(otherSr.ID != sr.ID &&
                        A2HCUtilities.dateRangesOverlap(sr.Start_Date__c, sr.End_Date__c, otherSr.Start_Date__c, otherSr.End_Date__c))
                {
                    sr.addError('The Start and End Dates overlap another Rate for the same Service.');
                    break;
                }
            }
        }
        
        // If the Rate overlaps any Scheduled Services for this Service the Rate and Public Holiday Rate
        // cannot be changed.
        if(Trigger.isUpdate)
        {
            RCR_Service_Rate__c oldRate = Trigger.oldMap.get(sr.Id);   
            if(!sr.Recalculate_Service_Order_Value__c && (sr.Rate__c != oldRate.Rate__c || sr.Public_Holiday_Rate__c != oldRate.Public_Holiday_Rate__c))
            {                    
                if(scheduledServicesByService.containsKey(sr.RCR_Service__c))
                {
                    for(RCR_Contract_Scheduled_Service__c rcss : scheduledServicesByService.get(sr.RCR_Service__c))
                    {
                        if(A2HCUtilities.dateRangesOverlap(rcss.Start_Date__c, rcss.End_Date__c, sr.Start_Date__c, sr.End_Date__c))
                        {
                            sr.addError('You cannot change the rate if it has been used by a Scheduled Service.');
                            break;
                        }
                    }
                }
            } 
            Date minStartDate;
            Date maxEndDate;                
            if(scheduledServicesByService.containsKey(sr.RCR_Service__c))
            {
                // Get the Minimum Start Date and Maximum End Date of the associated Scheduled Services
                for(RCR_Contract_Scheduled_Service__c ss : scheduledServicesByService.get(sr.RCR_Service__c))
                {
                    if(ss.Start_Date__c != null && (minStartDate == null || minStartDate > ss.Start_Date__c))
                    {
                        minStartDate = ss.Start_Date__c;
                    }
                    if(ss.End_Date__c != null && (maxEndDate == null || maxEndDate < ss.End_Date__c))
                    {
                        maxEndDate = ss.End_Date__c;
                    }
                }
            }   
            // Check that the dates are valid
            if(!sr.Bulk_Update__c && (sr.Start_Date__c > oldRate.Start_Date__c && sr.Start_Date__c > minStartDate))
            {
                sr.addError('Cannot set the Start Date after the Minimum Start of any Scheduled Service that has used the Rate: ' + sr.Id + '.');
            }
    
            if(!sr.Bulk_Update__c && (sr.End_Date__c < oldRate.End_Date__c && sr.End_Date__c < maxEndDate))
            {
                sr.addError('Cannot set the End Date before the Maximum End Date of any Scheduled Service that has used the Rate: ' + sr.Id + '.');
            }
            sr.Bulk_Update__c = false;
        }
    }                                       
}