trigger trgLocationHistoryAfterIU on Location_History__c (after insert, after update) 
{
    Map<Id, Referred_Client__c> clients = new Map<Id, Referred_Client__c>();
    
    for (Location_History__c h : Trigger.New)
    {
        if (h.Update_Current_Hospital_Facility__c)
        {
            clients.put(h.Client__c, new Referred_Client__c(Id = h.Client__c, Account__c = h.Hospital_Facility__c));
        }
        if (h.Update_Residential_Address__c)
        {
            if(!clients.containsKey(h.Client__c))
            {
                clients.put(h.Client__c, new Referred_Client__c(Id = h.Client__c));
            }
            clients.get(h.Client__c).State__c = h.State__c;
        }
    }
    if (clients.size() > 0)
    {
        update clients.values();
    }
}