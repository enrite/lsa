trigger trgIFClientPortalRequest_AUAI on IFClientPortalRequest__c (after  update) {
  for(IFClientPortalRequest__c request : Trigger.new)
  {
      List<IFClientPortalRequest__c> requests = [Select Id, 
                                                        Actioned_By__c, 
                                                        (Select ActorId, 
                                                                Comments, 
                                                                StepStatus 
                                                         From ProcessSteps
                                                        where (StepStatus = 'Approved' or StepStatus = 'Rejected') ) 
                                                 From IFClientPortalRequest__c 
                                                 where id=: request.Id];

      if (requests.size()>0)
      {
         IFClientPortalRequest__c cs=requests[0];
         String commentsStr='';
         String approver='';
         
         if(cs.ProcessSteps.size() > 0 & (cs.Actioned_By__c == null || cs.Actioned_By__c == ''))
         {
            ProcessInstanceHistory  ps =  cs.ProcessSteps[0];
            commentsStr= ps.comments;
             
            User u = [Select FirstName, LastName from User where Id=: ps.ActorId];
           
            approver = u.FirstName + ' ' + u.LastName;
             
            cs.Response_Text__c = commentsStr;
            cs.Actioned_By__c = approver;
            update cs;
         }
      }
  }
}