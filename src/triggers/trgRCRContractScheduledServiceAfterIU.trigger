trigger trgRCRContractScheduledServiceAfterIU on RCR_Contract_Scheduled_Service__c (after insert, after update) 
{
    if(TriggerByPass.RCRContractScheduledService)
    {
        return;
    }
    List<RCR_Funding_Detail__c> fundingDetails = new List<RCR_Funding_Detail__c>();
    Set<Plan_Action__c> planActions = new Set<Plan_Action__c>();
    // loop through the new records to create new funding detail records
    
    for (RCR_Contract_Scheduled_Service__c ss : [SELECT Id,
                                                        RCR_Contract__c,
                                                        RCR_Contract__r.Account__r.Finance_Code__c,
                                                        Start_Date__c,
                                                        Total_Cost__c,
                                                        GL_Category__c,
                                                        RCR_Contract__r.Plan_Action__c,
                                                        RCR_Contract__r.Client__r.RecordType.Name,
                                                        RCR_Contract__r.RecordType.DeveloperName,
                                                        (SELECT Id,
                                                                RCR_Program_Category_Service_Type__c,
                                                                RCR_Program_Category_Service_Type__r.Name,
                                                                RCR_Program_Category_Service_Type__r.Finance_Code_GG__c,
                                                                RCR_Program_Category_Service_Type__r.Finance_Code_NSAG__c
                                                         FROM RCR_Contract_Service_Types__r
                                                         LIMIT 1),
                                                        (SELECT Id,
                                                                RCR_Service_Order__c,
                                                                RCR_Contract_Scheduled_Service__c,
                                                                Start_Date__c,
                                                                Activity_Code__c,
                                                                Cost_Centre__c,
                                                                Maximum_Amount__c
                                                         FROM RCR_Funding_Details__r)       
                                                 FROM RCR_Contract_Scheduled_Service__c
                                                 WHERE Id IN :Trigger.NewMap.keySet()])
    {
        if(ss.RCR_Contract__r.Plan_Action__c != null)
        {
            if(Trigger.isInsert)
            {
                planActions.add(new Plan_Action__c(ID = ss.RCR_Contract__r.Plan_Action__c, GL_Category__c = null));
            }
            else if(Trigger.isUpdate)
            {
                RCR_Contract_Scheduled_Service__c oldValue = Trigger.oldMap.get(ss.ID);
                if(ss.Total_Cost__c != oldValue.Total_Cost__c || 
                        ss.GL_Category__c != oldValue.GL_Category__c)
                {
                    planActions.add(new Plan_Action__c(ID = ss.RCR_Contract__r.Plan_Action__c, GL_Category__c = null));
                }
            }
        }

        RCR_Funding_Detail__c fd = new RCR_Funding_Detail__c();
        fd.RCR_Service_Order__c = ss.RCR_Contract__c;
        fd.RCR_Contract_Scheduled_Service__c = ss.Id;
    
        // if a funding details record already exists against the scheduled service then grab that
        if (ss.RCR_Funding_Details__r.size() > 0)
        {
            fd = ss.RCR_Funding_Details__r[0];
        }                                        
        
        fd.Start_Date__c = ss.Start_Date__c;

        for (RCR_Contract_Service_Type__c cst : ss.RCR_Contract_Service_Types__r)
        { 
            fd.Service_Type_Name__c = cst.RCR_Program_Category_Service_Type__r.Name;
        
            if (ss.RCR_Contract__r.Account__r.Finance_Code__c == 'GG')
            {
                fd.Activity_Code__c = cst.RCR_Program_Category_Service_Type__r.Finance_Code_GG__c;
            }        
            else if (ss.RCR_Contract__r.Account__r.Finance_Code__c == 'NSAG')
            { 
                fd.Activity_Code__c = cst.RCR_Program_Category_Service_Type__r.Finance_Code_NSAG__c;    
            }
        }

        if(ss.RCR_Contract__r.Client__r.RecordType.Name == APSRecordTypes.Client_LSAParticipant)
        {
            fd.Cost_Centre__c = '339710199999';
        }
        else
        {
            //Managed Client
            fd.Cost_Centre__c = '339710299999';
        }

        fd.Maximum_Amount__c = ss.Total_Cost__c;
        
        fundingDetails.add(fd);
    }
            
    // insert the new funding details records
    upsert fundingDetails;
    TriggerBypass.PlanAction = true;     
    update new List<Plan_Action__c>(planActions);       
}