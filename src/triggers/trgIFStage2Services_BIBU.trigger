trigger trgIFStage2Services_BIBU on IF_Stage_2_Services__c(before insert, before update) 
{
    Profile p = [SELECT Name FROM Profile WHERE Id = :Userinfo.getProfileId()];
    Id accId = [SELECT Account_ID__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1].Account_ID__c;
    
    for (IF_Stage_2_Services__c serv: Trigger.new)
    {
        if(serv.Account__c == null && p.Name == 'RCR Portal') //Catches the lack of Account__c from the clone on portal
        {
            serv.Account__c = accId;
        }
        
        if(serv.Account_ID__c == null) //Catches lack of Account_Id__c from clone for internal user
        {
            String accountId = serv.Account__c;
            serv.Account_ID__c = accountId.substring(0,15);
        }
    }
}