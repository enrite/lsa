trigger trgRCRContract_BD on RCR_Contract__c (before delete) 
{
    ServiceOrderTrigger triggerClass = new ServiceOrderTrigger(null, Trigger.oldMap);
    triggerClass.beforeDelete();
}