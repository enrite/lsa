trigger trgIFPlanItemProvider_BD on IF_Personal_Budget_Plan_Item_Provider__c (before delete) 
{
    RecordType roPlanItemProvider = A2HCUtilities.getRecordType('IF_Personal_Budget_Plan_Item_Provider__c', 'IF Plan Item Provider ReadOnly');
    
    for(IF_Personal_Budget_Plan_Item_Provider__c pip: trigger.old)
    {
        if(pip.RecordTypeId == roPlanItemProvider.Id )
        {
            pip.AddError('This plan has been recommeded to a Team Leader so this Plan Item Provider cannot be deleted.');
        }
    }
}