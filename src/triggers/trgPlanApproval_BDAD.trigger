trigger trgPlanApproval_BDAD on Plan_Approval__c (before delete, after delete)
{
    if(Trigger.isAfter)
    {
        List<Plan_Action__c> planActions = [SELECT ID
                                            FROM    Plan_Action__c
                                            WHERE   Plan_Approval__c IN :Trigger.oldMap.keySet()];
        for(Plan_Action__c pa : planActions)
        {
            pa.Approved__c = false;
            pa.Ready_to_Approve__c = false;
        }
        TriggerBypass.PlanAction = true;
        update planActions;
        TriggerBypass.PlanAction = false;
    }
}