trigger trgReferredClientAfterIU on Referred_Client__c (after insert, after update) 
{
    if(TriggerBypass.Client)
    {
        return;
    }

    private List<Task> tasks = new List<Task>();
    private Set<ID> clientIDsForLanternPay = new Set<ID>();
    private RecordType lSAParticipantRecordType = A2HCUtilities.getRecordType('Referred_Client__c', APSRecordTypes.Client_LSAParticipant );
    private Map<ID, ID> previousPlannerIDByClientID = new Map<ID, ID>();
    List<Task> baseTasks = new List<Task>();
    Map<Id,List<Task>> existingTasks = new Map<Id,List<Task>>();

    private User LSAAdminUser
    {
        get
        {
            if(LSAAdminUser == null)
            {
                List<User> adminUsers = [SELECT ID,
                                                Name
                                        FROM    User
                                        WHERE   IsActive = true AND
                                                ID IN (SELECT       UserOrGroupId
                                                            FROM    GroupMember
                                                            WHERE   Group.Name = 'SysAdmin')];
                for(User u : adminUsers)
                {
                    if(u.Name == 'LSA Admin')
                    {
                        LSAAdminUser = u;
                        break;
                    }
                }
                if(LSAAdminUser == null)
                {
                    LSAAdminUser = adminUsers[0];
                }
            }
            return LSAAdminUser;
        }
        set;
    }

    if(Trigger.isUpdate)
    {
        Boolean  vcrReceivedDateUpdate = false;
        for(Referred_Client__c c : Trigger.new)
        {
            if(c.Date_VCR_Received__c != null && Trigger.OldMap.get(c.Id).Date_VCR_Received__c==null)
            {
                vcrReceivedDateUpdate = true;
                break;
            }
        }

        if(vcrReceivedDateUpdate)
        {
            for(Task t : [SELECT ID, Status, WhatId from Task 
                                  where (Subject like '%VCR Report%' or Subject like '%Vehicle Collision Report%') 
                                  and WhatId in :Trigger.NewMap.keyset()])
            {
                if(!existingTasks.containsKey(t.WhatId))
                {
                    existingTasks.put(t.WhatId, new List<Task>());
                }
                existingTasks.get(t.WhatId).add(t);
            }
        }
    }

    for (Referred_Client__c c : Trigger.New)
    {
        Referred_Client__c oldValue = Trigger.isInsert ? null : (Referred_Client__c)Trigger.OldMap.get(c.Id);
        String oldStatus = oldValue == null ? null : oldValue.Current_Status__c;
        if(c.Current_Status__c != oldStatus &&
                c.LP_Ready_to_Integrate__c)
        {
            clientIDsForLanternPay.add(c.ID);
        }
        if(Trigger.isUpdate)
        {
            if(oldValue.Date_VCR_Received__c == null && c.Date_VCR_Received__c != null)
            {
                for(Task t : existingTasks.get(c.Id))
                {
                    t.Status = 'Completed';
                    tasks.add(t);
                }
                continue;
            }
            if(c.Allocated_to__c != oldValue.Allocated_to__c &&
                    c.Allocated_to__c != null)
            {
                previousPlannerIDByClientID.put(c.ID, oldValue.Allocated_to__c);
            }
            if(c.Date_Outcome_Discussion__c != null &&
                    c.Date_Outcome_Discussion__c != oldValue.Date_Outcome_Discussion__c)
            {
                createOutcomeTasks(c, oldValue, tasks);
            }
        }
        // for a new record if VCR is not requested then create a task for that
        if (Trigger.isInsert && !c.VCR_Requested__c && c.recordtypeid == lSAParticipantRecordType.Id)
        {
            // create the base task
            Task baseTask = new Task();
            baseTask.Subject = 'Request Vehicle Collision Report from SAPOL for ' + c.Given_Name__c + ' ' + c.Family_Name__c + ' ' + (c.Date_of_Birth__c != null ? c.Date_of_Birth__c.format() : '');
            baseTask.LSA_Task_Type__c = 'Request Vehicle Collision Report';
            baseTask.ActivityDate = Date.today();
            baseTask.Status = 'Not Started';
            baseTask.Priority = 'Normal';
            baseTask.WhatId = c.Id;
            baseTask.Assigned_to_Group__c = 'PSO';
            
            baseTasks.add(baseTask);                          
        }
    
        // has the vcr requested field change to true
        if ((Trigger.isInsert || !oldValue.VCR_Requested__c && c.recordtypeid == lSAParticipantRecordType.Id)
            && c.VCR_Requested__c
            && c.Date_VCR_Requested__c != null)
        {
            Date taskDate = Date.today().addDays(7);
        
            // create the base task
            Task baseTask = new Task();
            baseTask.Subject = 'Follow up VCR Report from SAPOL for ' + c.Given_Name__c + ' ' + c.Family_Name__c + ' ' + (c.Date_of_Birth__c != null ? c.Date_of_Birth__c.format() : '');
            baseTask.LSA_Task_Type__c = 'Follow up VCR Report';
            baseTask.Description = 'Requested ' + Date.today().format();
            baseTask.ActivityDate = taskDate;
            baseTask.ReminderDateTime = Datetime.newInstance(taskDate.year(), taskDate.month(), taskDate.day(), 9, 0, 0);
            baseTask.IsReminderSet = true;
            baseTask.Status = 'Not Started';
            baseTask.Priority = 'Normal';
            baseTask.WhatId = c.Id;
            baseTask.Assigned_to_Group__c = 'PSO';
            
            baseTasks.add(baseTask);                        
        }                    
    }


    if (baseTasks.size() > 0)
    {
        // find all users in the program support officer group
        List<GroupMember> groupMembers = [SELECT Id,
                                                 UserOrGroupId
                                          FROM GroupMember
                                          WHERE Group.Name = 'Program Support Officers'];
    
        for (Task bt : baseTasks)
        {
            /// if this is set it will be assigned to a default user
            if(String.isNotBlank(bt.Assigned_to_Group__c))
            {
                tasks.add(bt);
                continue;
            }
            // create a task for each user of the group
            for (GroupMember gm : groupMembers)
            {
                // only add a task for a user
                if (String.valueOf(gm.UserOrGroupId).left(3) == '005')
                {                
                    Task t = bt.clone(false, true);
                    t.OwnerId = gm.UserOrGroupId;
                    
                    tasks.add(t);
                }
            }
        }
    }

    List<Messaging.SingleEmailMessage> msgs = new List<Messaging.SingleEmailMessage>();
    if(!previousPlannerIDByClientID.isEmpty())
    {
        EmailTemplate et = [SELECT ID
                            FROM    EmailTemplate
                            WHERE DeveloperName = 'Planner_Changed'];

        List<User> grpMembers = [SELECT   Id
                                FROM    User
                                WHERE   IsActive = true AND
                                        ID IN (SELECT UserOrGroupId
                                            FROM    GroupMember
                                            WHERE   Group.DeveloperName = 'Program_Support_Officers')];

        for(Referred_Client__c client : [SELECT ID,
                                                Allocated_to__c,
                                                (SELECT ID,
                                                        OwnerId
                                                FROM    Tasks
                                                WHERE   IsClosed = false)
                                            FROM Referred_Client__c
                                            WHERE ID IN :previousPlannerIDByClientID.keySet()])
        {
            ID previousPlannerID = previousPlannerIDByClientID.get(client.ID);
            for(Task t : client.Tasks)
            {
                if(t.OwnerId == previousPlannerID)
                {
                    t.OwnerId = client.Allocated_to__c;
                    tasks.add(t);
                }
            }
            for(User grpMember : grpMembers)
            {
                Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
                msg.setTemplateId(et.ID);
                msg.setWhatId(client.ID);
                msg.setTargetObjectId(grpMember.ID);
                msg.setTreatTargetObjectAsRecipient(true);
                msg.setSaveAsActivity(false);
                msgs.add(msg);
            }
        }
    }
    if(!tasks.isEmpty())
    {
        upsert tasks;
    }
    if(!clientIDsForLanternPay.isEmpty())
    {
        LPRESTInterface.addMember(clientIDsForLanternPay);
    }
    if(!msgs.isEmpty())
    {
        Messaging.sendEmail(msgs);
    }

    private void createOutcomeTasks(Referred_Client__c client, Referred_Client__c oldValue, List<Task> tasks)
    {
system.debug(client.Current_Status__c);
        switch on client.Current_Status__c
        {
            when 'Not lifetime participant'
            {
                tasks.add(newTask(client, Date.today().addYears(1), 'Stop portal access for ' + client.Name,  LSAAdminUser.ID));

                Task psoTask = newTask(client, Date.today().addMonths(9), 'Contact ' + client.Name + ' 9 months after status change', UserInfo.getUserId());
                psoTask.Assigned_to_Group__c = 'PSO';
                tasks.add(psoTask);
            }
            when 'Lifetime assessed, transitioning out'
            {
                tasks.add(newTask(client, Date.today(), 'Send transitioning out letter and info pack', client.Allocated_to__c));
                tasks.add(newTask(client, Date.today(), 'Referral to other services', client.Allocated_to__c));
                tasks.add(newTask(client, Date.today(), 'Transfer of equipment items', client.Allocated_to__c));
                tasks.add(newTask(client, Date.today(), 'Communication to treating service providers', client.Allocated_to__c));
                tasks.add(newTask(client, Date.today(), 'Discuss participants use of services approved in current MyPlan', client.Allocated_to__c));
            }
        }
    }

    private Task newTask(Referred_Client__c client, Date dueDate, String subject, ID ownerID)
    {
        Task t = new Task();
        t.WhatId = client.ID;
        t.ActivityDate = dueDate;
        t.Subject = subject;
        t.OwnerId = ownerId;
        t.Status = 'Not Started';
        t.Priority = 'Normal';
        return t;
    }
}