trigger trgIF_PersonalBudgetPlan_BIBU on IF_Personal_Budget_Plan__c (before insert, before update) 
{

    RecordType draft = A2HCUtilities.getRecordType('IF_Personal_Budget_Plan__c', 'Draft');
    RecordType apr = A2HCUtilities.getRecordType('IF_Personal_Budget_Plan__c', 'IF Plan Approved');
    RecordType fac = A2HCUtilities.getRecordType('IF_Personal_Budget_Plan__c', 'Review by Facilitator');
    RecordType tl = A2HCUtilities.getRecordType('IF_Personal_Budget_Plan__c', 'Review by Team Leader');
    RecordType mgr = A2HCUtilities.getRecordType('IF_Personal_Budget_Plan__c', 'Review by Manager');
    RecordType dir = A2HCUtilities.getRecordType('IF_Personal_Budget_Plan__c', 'Review by Director');
    
    RecordType roPlanItem = A2HCUtilities.getRecordType('IF_Personal_Budget_Plan_Items__c', 'IF Plan Item ReadOnly');
    RecordType editPlanItem = A2HCUtilities.getRecordType('IF_Personal_Budget_Plan_Items__c', 'IF Plan Item Edit');
    
    RecordType roPlanItemProvider = A2HCUtilities.getRecordType('IF_Personal_Budget_Plan_Item_Provider__c', 'IF Plan Item Provider ReadOnly');
    RecordType editPlanItemProvider = A2HCUtilities.getRecordType('IF_Personal_Budget_Plan_Item_Provider__c', 'IF Plan Item Provider Edit');

    Set<ID> planClientIDs = new Set<ID>();
    Set<ID> planIDs = new Set<ID>();
    Set<ID> servOrderIDs = new Set<ID>();
    for (IF_Personal_Budget_Plan__c myplan: Trigger.new)
    { 
        planClientIDs.add(myplan.Client__c);
        planIDs.add(myplan.Id);
        servOrderIDs.add(myplan.IF_RCR_Service_Order__c);
    }
    
    Map<ID, Referred_Client__c> clientWithPlanMap = new Map<ID, Referred_Client__c>();
    for(Referred_Client__c cl: [SELECT name, (SELECT Id, Financial_Year__c,Client__c,Period_Start__c , Period_End__c   FROM IF_Personal_Support_Plans__r ORDER BY Financial_Year__c) FROM Referred_Client__c
                                            WHERE Id IN :planClientIDs AND Plan_Count__c > 0 ORDER BY name])
    { 
        clientWithPlanMap.put(cl.Id, cl);
    }
    List<IF_Personal_Budget_Plan__c> clientPlans = new List<IF_Personal_Budget_Plan__c>();

    Map<ID, IF_Personal_Budget_Plan__c> facPlanMap = new Map<ID, IF_Personal_Budget_Plan__c>();
    for(IF_Personal_Budget_Plan__c plan : [SELECT Id, Name, RecordTypeId , (SELECT Id, Providers__c FROM IF_Person_Support_Plan_Items__r) 
                                                FROM IF_Personal_Budget_Plan__c WHERE Id IN :planIDs AND RecordTypeId = :fac.Id])
    {
        facPlanMap.put(plan.Id, plan);   
    }
    List<IF_Personal_Budget_Plan_Items__c> planItems = new List<IF_Personal_Budget_Plan_Items__c>();
    
    // Get Service Orders for Client
    Map<ID, RCR_Contract__c> serviceOrdersforClientMap = new Map<ID, RCR_Contract__c>();
    for(RCR_Contract__c clientServOrd: [SELECT Id, Name, Total_Service_Order_Cost__c 
                                                FROM RCR_Contract__c WHERE Id IN :servOrderIDs ])
    {
        serviceOrdersforClientMap.put(clientServOrd.Id, clientServOrd);   
    }                           
    
    String totalCostNotEqualApprovedBudgetMsg = '';
    
    //used when setting PlanItems to back to Edit RecordType
    Map<ID, IF_Personal_Budget_Plan__c> rejectPlanMap = new Map<ID, IF_Personal_Budget_Plan__c>();
    for(IF_Personal_Budget_Plan__c planRej : [SELECT Id, Name, RecordTypeId , (SELECT Id, Providers__c FROM IF_Person_Support_Plan_Items__r) 
                                                FROM IF_Personal_Budget_Plan__c WHERE Id IN :planIDs AND RecordTypeId != :fac.Id AND Plan_Status__c != :APS_PicklistValues.IF_PlanStatus_Approved ])
    {
        rejectPlanMap.put(planRej.Id, planRej);   
    }
    
    List<IF_Personal_Budget_Plan_Items__c> rejectedPlanItems = new List<IF_Personal_Budget_Plan_Items__c>();
    List<IF_Personal_Budget_Plan_Items__c> planItemsToEdit = new List<IF_Personal_Budget_Plan_Items__c>();
    List<IF_Personal_Budget_Plan_Items__c> approvedPlanItems = new List<IF_Personal_Budget_Plan_Items__c>();
    List<IF_Personal_Budget_Plan_Items__c> planItemsToUpdate = new List<IF_Personal_Budget_Plan_Items__c>();
    
    //used when setting Plan Item Providers to read only RecordType
    Map<ID, IF_Personal_Budget_Plan_Items__c> readOnlyPlanItemMap = new Map<ID, IF_Personal_Budget_Plan_Items__c>();
    for(IF_Personal_Budget_Plan_Items__c rOnlyPlanItem : [SELECT Id, Name, RecordTypeId , (SELECT Id FROM IF_Personal_Budget_Plan_Item_Providers__r) FROM IF_Personal_Budget_Plan_Items__c  WHERE Personal_Budget_Plan__r.Id IN :planIDs])
    {
        readOnlyPlanItemMap.put(rOnlyPlanItem.Id, rOnlyPlanItem);   
    }
    List<IF_Personal_Budget_Plan_Item_Provider__c> readOnlyPlanItemProv = new List<IF_Personal_Budget_Plan_Item_Provider__c>();
    List<IF_Personal_Budget_Plan_Item_Provider__c> planItemProvsToUpdate = new List<IF_Personal_Budget_Plan_Item_Provider__c>();
    List<IF_Personal_Budget_Plan_Item_Provider__c> editPlanItemProv = new List<IF_Personal_Budget_Plan_Item_Provider__c>();
    List<IF_Personal_Budget_Plan_Item_Provider__c> editPlanItemProvsToUpdate = new List<IF_Personal_Budget_Plan_Item_Provider__c>();
    
    Boolean isPlanRecommended = false;
    Boolean isPlanRejected = false;
    
    // MAIN LOOP
    for (IF_Personal_Budget_Plan__c myplan: Trigger.new)
    {    
        
        if(Trigger.isInsert)
        {
            myplan.IF_Confirmed_Option__c = myplan.Confirmed_Option__c;
        }
        
        //Get Approved Personal Budget from Service Order
        if(myplan.IF_RCR_Service_Order__c != null)
        {
             if (serviceOrdersforClientMap.containsKey(myplan.IF_RCR_Service_Order__c))
             {
                 RCR_Contract__c servOrderActual = serviceOrdersforClientMap.get(myplan.IF_RCR_Service_Order__c);
                 myplan.Approved_Personal_Budget__c = servOrderActual.Total_Service_Order_Cost__c;
             }
        }
        else
        {
            myplan.Approved_Personal_Budget__c = 0;
        }
        
        //Check if Total Budget == Approved Personal Budget
        if(myplan.Total_Budget__c != myplan.Approved_Personal_Budget__c )
        {
            totalCostNotEqualApprovedBudgetMsg = 'The Approved Personal Budget has changed. Please reject this plan to allow Facilitator to amend.';
            //System.debug('totalCostNotEqualApprovedBudgetMsg : ' + totalCostNotEqualApprovedBudgetMsg );
            
            //'You cannot recommend this plan as either there are no Plan Items or the Total Cost of the Plan Items does not equal the Approved Personal Budget.';
        }
        
        if(Trigger.isUpdate && (myplan.IF_Confirmed_Option__c == APS_PicklistValues.IF_Client_AdminType_Agency || myplan.IF_Confirmed_Option__c == APS_PicklistValues.IF_Client_AdminType_AgencyService))
        {
            if(myplan.Host__c == null || myplan.Host_Contact_Details__c == null)
            {
                 myplan.AddError('You must complete all Host details.');
            }
        }
        
        //the check of the previous plan status is to stop the rule being checked for Amendments
        if(Trigger.isUpdate && (myplan.IF_Confirmed_Option__c == APS_PicklistValues.IF_Client_AdminType_HelpDirect ) && trigger.oldMap.get(myplan.Id).Plan_Status__c != APS_PicklistValues.IF_PlanStatus_Approved )
        {
            if(myplan.Person_Acting_Type__c == null || myplan.Person_Acting_Relationship_to_Client__c== null || myplan.Person_Acting_Address__c== null 
                || myplan.Person_Acting_Name__c== null || myplan.IF_Person_Acting_First_Name__c == null || myplan.IF_Person_Acting_Title__c == null)
            {
                 myplan.AddError('You must complete all Person Acting Details.');
            }
        }
        //the check of the previous plan status is to stop the rule being checked for Amendments
        if(Trigger.isUpdate && (myplan.IF_Confirmed_Option__c == APS_PicklistValues.IF_Client_AdminType_Direct ) && trigger.oldMap.get(myplan.Id).Plan_Status__c != APS_PicklistValues.IF_PlanStatus_Approved )
        {
            if(myplan.Person_Acting_Type__c != null || myplan.Person_Acting_Relationship_to_Client__c != null || myplan.Person_Acting_Address__c != null 
                || myplan.Person_Acting_Name__c != null || myplan.IF_Person_Acting_First_Name__c != null || myplan.IF_Person_Acting_Title__c != null)
            {
                 myplan.AddError('This client is not permiited to have any Person Acting details.');
            }
        }
        
        //Client can only have plan per Fin Yr
        if(clientWithPlanMap.containsKey(myplan.Client__c))
        {
            Referred_Client__c clientActual = clientWithPlanMap.get(myplan.Client__c);
            clientPlans.addAll(clientActual.IF_Personal_Support_Plans__r);
            
            if(clientPlans.size() > 0)
            {
                Boolean addError = false;
                
                for (IF_Personal_Budget_Plan__c p : clientPlans) 
                {
                    /*if(p.Financial_Year__c == myplan.Financial_Year__c && p.Id != myplan.Id && p.Client__c == myplan.Client__c)
                    {
                       myplan.AddError('There is already a plan for this financial year. You can only have one plan per financial year.');
                    }*/
                    
                    if(p.Period_Start__c <= myPlan.Period_End__c && myPlan.Period_Start__c  <= p.Period_End__c  )
                    {
                        addError = true;
                        if(Trigger.isUpdate && (p.Id == myPlan.Id || p.Client__c != myPlan.Client__c))
                        {
                            addError = false;
                        }
                        
                    }
                    if(addError)
                    {
                         myplan.AddError('Plan Start/end dates for this plan overlap with another plan.');
                    }
                }
            }
        }
        
        //If Facilitaor recommended set Status to 
        if (myplan.Facilitator_Recommended__c == APS_PicklistValues.IF_FacilitatorRecomended_Recommended && trigger.oldMap.get(myplan.Id).Facilitator_Recommended__c != APS_PicklistValues.IF_FacilitatorRecomended_Recommended)
        {
            isPlanRecommended  = true;
            //Check Plan Items for Providers, ie All items must have at leats one provider
            if(facPlanMap.containsKey(myplan.Id))
            {
                IF_Personal_Budget_Plan__c planActual = facPlanMap.get(myplan.Id);
                planItems.addAll(planActual.IF_Person_Support_Plan_Items__r);
                
                if(planItems.size() > 0)
                {
                    Boolean hasProvider = true;
                    for (IF_Personal_Budget_Plan_Items__c pi : planItems) 
                    {
                        if(pi.Providers__c == 0)
                        {
                            hasProvider = false;
                        }
                    }
                    
                    if (!hasProvider)
                    {
                         myplan.AddError('All plan items must have at least one provider before a plan can be recommended.');
                         isPlanRecommended  = false;
                    }
                }
            }
             
            If(myplan.Total_Budget__c <= 0)
            {
                 myplan.AddError('Plan must have at least one Plan Item.');
                 isPlanRecommended  = false;
            }
            
            //Check if Total Budget == Approved Personal Budget
            if(totalCostNotEqualApprovedBudgetMsg != '')
            {
                myplan.AddError('Approved Personal Budget does not equal Plan total.');
                isPlanRecommended  = false;
            }
            if(!myplan.RFS_Approved__c)
            {
                myplan.AddError('You cannot recommend a plan that has not had an RFS approved.');
                isPlanRecommended  = false;
            }
            
            myplan.Facilitator_Approval_Date__c = Date.Today();
            myplan.Plan_Status__c = APS_PicklistValues.IF_PlanStatus_RevbyTeamLead ;
            
            myplan.TeamLeader_Decision__c = '';
            //myplan.IF_Team_Leader_Name__c = '';
            //myplan.IF_Manager_Name__c = '';
            //myplan.IF_Director_Name__c = '';
            myplan.IF_Team_Leader__c = null;
            myplan.IF_Manager__c = null;
            myplan.IF_Director__c = null;
            myplan.RecordTypeId = tl.Id;
            myplan.IF_Remind_Team_Leader__c = 1;
            myplan.Due_Date_Team_Leader__c = Date.Today();
        }
        
        //Team Leader Decisions....
        if(Trigger.isUpdate && trigger.oldMap.get(myplan.Id).TeamLeader_Decision__c != myplan.TeamLeader_Decision__c )
        {
             myplan.TL_Date__c = Date.Today();
        }
        
        if(myplan.TeamLeader_Decision__c == APS_PicklistValues.IF_TeamLeaderDecision_Approved )
        {
             if(myplan.Total_Budget__c != myplan.Previous_Approved_Plan_Amount__c)
             {
                 myplan.AddError('The Total Budget for the plan has changed, so you cannnot approve it. It must be refered to a Manager.');
             }
             else
             {
                 myplan.Plan_Status__c = APS_PicklistValues.IF_PlanStatus_Approved;
                 myplan.Visible_to_Client__c = true;
                 myplan.Manager_Approval__c = null;
                 myplan.Dir_Approval__c = null;
                 myplan.RecordTypeId = apr.Id;
                 myplan.IF_Remind_Team_Leader__c = 0;
             }
            
        }
        if (Trigger.isUpdate &&(trigger.oldMap.get(myplan.Id).TeamLeader_Decision__c != APS_PicklistValues.IF_TeamLeaderDecision_Recommended && trigger.oldMap.get(myplan.Id).TeamLeader_Decision__c != APS_PicklistValues.IF_TeamLeaderDecision_Referred)
           && ((myplan.TeamLeader_Decision__c == APS_PicklistValues.IF_TeamLeaderDecision_Referred) 
           || (myplan.TeamLeader_Decision__c == APS_PicklistValues.IF_TeamLeaderDecision_Recommended)) )
        {
             //Check if Total Budget == Approved Personal Budget
            if(totalCostNotEqualApprovedBudgetMsg != '')
            {
                myplan.AddError(totalCostNotEqualApprovedBudgetMsg);
            }
            
            myplan.Plan_Status__c = APS_PicklistValues.IF_PlanStatus_RevbyManger;
            myplan.Manager_Approval__c = '';
            //myplan.IF_Manager_Name__c = '';
            //myplan.IF_Director_Name__c = '';
            myplan.IF_Manager__c = null;
            myplan.IF_Director__c = null;
            myplan.RecordTypeId = mgr.Id;
            myplan.IF_Remind_Manager__c = 1;
            myplan.Due_Date_Manager__c = Date.Today();
            myplan.IF_Remind_Team_Leader__c = 0;
        }
        
        if(myplan.TeamLeader_Decision__c == APS_PicklistValues.IF_TeamLeaderDecision_Rejected && trigger.oldMap.get(myplan.Id).TeamLeader_Decision__c != APS_PicklistValues.IF_TeamLeaderDecision_Rejected )
        {
            myplan.Facilitator_Recommended__c  = '';
            myplan.Plan_Status__c = APS_PicklistValues.IF_PlanStatus_NotApproved ;
            myplan.RecordTypeId = fac.Id;
            myplan.IF_Remind_Team_Leader__c = 0;
            isPlanRejected = true;
        }
        
        //Manager Decision....
        if(Trigger.isUpdate && (trigger.oldMap.get(myplan.Id).Manager_Approval__c != myplan.Manager_Approval__c) )
        {
             myplan.Manager_Date__c = Date.Today();
        }
        if (myplan.Manager_Approval__c == APS_PicklistValues.IF_ManagerApproval_Referred && trigger.oldMap.get(myplan.Id).Manager_Approval__c != APS_PicklistValues.IF_ManagerApproval_Referred )
        {
             //Check if Total Budget == Approved Personal Budget
            if(totalCostNotEqualApprovedBudgetMsg != '')
            {
                myplan.AddError(totalCostNotEqualApprovedBudgetMsg );
            }

            myplan.Plan_Status__c = APS_PicklistValues.IF_PlanStatus_RevbyDirector ;
            myplan.Dir_Approval__c = '';
            //myplan.IF_Director_Name__c = '';
            myplan.IF_Director__c = null;
            myplan.RecordTypeId = dir.Id;
            myplan.IF_Remind_Manager__c = 0;
        }
        
        if(myplan.Manager_Approval__c == APS_PicklistValues.IF_ManagerApproval_Approved && trigger.oldMap.get(myplan.Id).Manager_Approval__c != APS_PicklistValues.IF_ManagerApproval_Approved )
        {
             //Check if Total Budget == Approved Personal Budget
            if(totalCostNotEqualApprovedBudgetMsg != '')
            {
                myplan.AddError(totalCostNotEqualApprovedBudgetMsg);
            }
            
            myplan.Plan_Status__c = APS_PicklistValues.IF_PlanStatus_Approved;
            myplan.Dir_Approval__c = null;
            myplan.RecordTypeId = apr.Id;
            myplan.Previous_Approved_Plan_Amount__c = myplan.Total_Budget__c;
            myplan.Visible_to_Client__c = true;
            myplan.IF_Remind_Manager__c = 0;
        }
        
        if(myplan.Manager_Approval__c  == APS_PicklistValues.IF_ManagerApproval_Rejected && trigger.oldMap.get(myplan.Id).Manager_Approval__c != APS_PicklistValues.IF_ManagerApproval_Rejected )
        {
            myplan.Facilitator_Recommended__c  = '';
            myplan.Plan_Status__c = APS_PicklistValues.IF_PlanStatus_NotApproved ;
            myplan.RecordTypeId = fac.Id;
            isPlanRejected = true;
            myplan.IF_Remind_Manager__c = 0;
        }
        
        //Director decisions.....
        if(Trigger.isUpdate && trigger.oldMap.get(myplan.Id).Dir_Approval__c != myplan.Dir_Approval__c )
        {
             myplan.Director_Date__c = Date.Today();
        }
        if (myplan.Dir_Approval__c == APS_PicklistValues.IF_DirectorApproval_Referred && trigger.oldMap.get(myplan.Id).Dir_Approval__c != APS_PicklistValues.IF_DirectorApproval_Referred)
        {
            if(totalCostNotEqualApprovedBudgetMsg != '')
            {
                myplan.AddError(totalCostNotEqualApprovedBudgetMsg);
            }
            
            myplan.Plan_Status__c = APS_PicklistValues.IF_PlanStatus_RevbyEnPanel ;
        }
        
        if(myplan.Dir_Approval__c == APS_PicklistValues.IF_DirectorApproval_Approved && trigger.oldMap.get(myplan.Id).Dir_Approval__c != APS_PicklistValues.IF_DirectorApproval_Approved )
        {
            if(totalCostNotEqualApprovedBudgetMsg != '')
            {
                myplan.AddError(totalCostNotEqualApprovedBudgetMsg);
            }
            
            myplan.Plan_Status__c = APS_PicklistValues.IF_PlanStatus_Approved;
            myplan.RecordTypeId = apr.Id;
            myplan.Visible_to_Client__c = true;
            myplan.Previous_Approved_Plan_Amount__c = myplan.Total_Budget__c;
        }
        
        if(myplan.Dir_Approval__c == APS_PicklistValues.IF_DirectorApproval_Rejected && trigger.oldMap.get(myplan.Id).Dir_Approval__c != APS_PicklistValues.IF_DirectorApproval_Rejected )
        {
            myplan.Facilitator_Recommended__c  = '';
            myplan.Plan_Status__c = APS_PicklistValues.IF_PlanStatus_NotApproved ;
            myplan.RecordTypeId = fac.Id;
            isPlanRejected = true;
        }
        
        //Cancelled Plan
        if(myplan.Cancel_Plan__c == true)
        {
            myplan.Plan_Status__c = APS_PicklistValues.IF_PlanStatus_Cancelled ;
        }
        if (Trigger.isUpdate)
        {
            if(myplan.Cancel_Plan__c == false && trigger.oldMap.get(myplan.Id).Cancel_Plan__c == true )
            {
               myplan.Plan_Status__c = APS_PicklistValues.IF_PlanStatus_RevbyFac;  
            }
        }
        
        if(isPlanRecommended)
        {
            //get plan items and set their recordtypes to ReadOnly
            if(facPlanMap.containsKey(myplan.Id))
            {
                IF_Personal_Budget_Plan__c aprPlanActual = facPlanMap.get(myplan.Id);
                approvedPlanItems.addAll(aprPlanActual.IF_Person_Support_Plan_Items__r);
                
                if(approvedPlanItems.size() > 0)
                {
                    for (IF_Personal_Budget_Plan_Items__c pi : approvedPlanItems) 
                    {
                        planItemsToUpdate.add(pi);
                        // Now get list of Plan Item Providers to update their Record Type to ReadOnly
                        if(readOnlyPlanItemMap.containsKey(pi.Id))
                        {
                            IF_Personal_Budget_Plan_Items__c aprPlanItemActual = readOnlyPlanItemMap.get(pi.Id);
                            readOnlyPlanItemProv.addAll(aprPlanItemActual.IF_Personal_Budget_Plan_Item_Providers__r);
                           
                            if(readOnlyPlanItemProv.size() > 0)
                            {
                               planItemProvsToUpdate.addAll(readOnlyPlanItemProv);
                            }
                            
                            readOnlyPlanItemProv.clear();
                        }
                    }
                    
                }
            } 
        }
        
        if(isPlanRejected)
        {
            //get plan items and set their recordtypes to ReadOnly
            if(rejectPlanMap.containsKey(myplan.Id))
            {
                IF_Personal_Budget_Plan__c rejPlanActual = rejectPlanMap.get(myplan.Id);
                rejectedPlanItems.addAll(rejPlanActual.IF_Person_Support_Plan_Items__r);
                
                if(rejectedPlanItems.size() > 0)
                {
                    //planItemsToEdit.addAll(rejectedPlanItems);
                    for (IF_Personal_Budget_Plan_Items__c pi : rejectedPlanItems) 
                    {
                        planItemsToEdit.add(pi);
                        // Now get list of Plan Item Providers to update their Record Type to Edit
                        if(readOnlyPlanItemMap.containsKey(pi.Id))
                        {
                            IF_Personal_Budget_Plan_Items__c roPlanItemActual = readOnlyPlanItemMap.get(pi.Id);
                            editPlanItemProv.addAll(roPlanItemActual.IF_Personal_Budget_Plan_Item_Providers__r);
                           
                            if(editPlanItemProv.size() > 0)
                            {
                               editPlanItemProvsToUpdate.addAll(editPlanItemProv);
                            }
                            
                            editPlanItemProv.clear();
                        }
                    }
                   
                }
            }
            
        }
        
        isPlanRecommended = false;
        isPlanRejected = false;
        totalCostNotEqualApprovedBudgetMsg = '';
    }
    // END MAIN LOOP
    
    //update the recordType on the list of approved PlanItems;
    for (IF_Personal_Budget_Plan_Items__c pi: planItemsToUpdate) 
    { 
        pi.RecordTypeId = roPlanItem.Id;
    }
    UPDATE planItemsToUpdate;
    
    //update the recordType on the list of approved PlanItemProviders;
    for (IF_Personal_Budget_Plan_Item_Provider__c pip: planItemProvsToUpdate) 
    { 
        pip.RecordTypeId = roPlanItemProvider.Id;  
    }
    UPDATE planItemProvsToUpdate;
    
    //update the recordType on the list of rejected PlanItems;
    for (IF_Personal_Budget_Plan_Items__c pi: planItemsToEdit) 
    { 
        pi.RecordTypeId = editPlanItem.Id;
    }
    UPDATE planItemsToEdit;
    
    //update the recordType on the list of rejected PlanItemProviders;
    for (IF_Personal_Budget_Plan_Item_Provider__c pip: editPlanItemProvsToUpdate) 
    { 
        pip.RecordTypeId = editPlanItemProvider.Id;  
    }
    UPDATE editPlanItemProvsToUpdate;
}