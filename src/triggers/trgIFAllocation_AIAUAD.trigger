trigger trgIFAllocation_AIAUAD on IF_Personal_Budget__c (after insert, after update, after delete, after undelete) 
{
    TriggerBypass__c bypass = TriggerBypass__c.getInstance();
    if(bypass != null && bypass.ByPass__c)
    {
        return;
    }
    if(System.isBatch() || TriggerBypass.IFAllocation)
    {
        return;
    }
    Set<Id> serviceOrderIDs = new Set<Id>();
    Map<Id, List<IF_Personal_Budget__c>> allocationsByServiceOrderID = new Map<Id, List<IF_Personal_Budget__c>>();
    List<RCR_Contract__c> serviceOrdersToUpdate = new List<RCR_Contract__c>();
    Map<ID, Financial_Year__c> financialYearsByID = new Map<ID, Financial_Year__c>(
                                                    [SELECT ID,
                                                            Start_Date__c,
                                                            End_Date__c
                                                   FROM    Financial_Year__c]);   
                                                   
    List<IF_Personal_Budget__c> allocations = new List<IF_Personal_Budget__c>();
    if(Trigger.isInsert || Trigger.isUpdate || Trigger.isUnDelete)
    { 
        allocations = Trigger.new;
    }
    else if(Trigger.isDelete)
    {
        allocations = Trigger.old;
    }
    for(IF_Personal_Budget__c alloc : allocations)
    {
        if(Trigger.isDelete)
        {
            checkAllocation(alloc, financialYearsByID);
            isStatusValid(alloc, null);
        }
        else
        {
            if(alloc.Approval_Criteria__c != APS_PicklistValues.IFAllocation_ApprovalCriteria_AutomaticApprove)
            {
                checkAllocation(alloc, financialYearsByID);
            }
            if(!Trigger.isInsert && !Trigger.isUnDelete)
            {
                isStatusValid(alloc, Trigger.oldMap.get(alloc.ID));
            }
        }
        if(alloc.RCR_Service_Order__c != null)
        {   
            serviceOrderIDs.add(alloc.RCR_Service_Order__c);
        }
    }
    // Get all allocations for related Service Orders
    for(IF_Personal_Budget__c alloc : getAllocations())
    {
        if(!allocationsByServiceOrderID.containsKey(alloc.RCR_Service_Order__c))
        {
            allocationsByServiceOrderID.put(alloc.RCR_Service_Order__c, new List<IF_Personal_Budget__c>());
        }
        allocationsByServiceOrderID.get(alloc.RCR_Service_Order__c).add(alloc);
    }
    for(ID soID : allocationsByServiceOrderID.keySet())
    {
        decimal totApproved = 0.0;
        decimal totNotApproved = 0.0;
        decimal totNotApprovedOnceOff = 0.0;
        decimal totRecurrent = 0.0;
            for(IF_Personal_Budget__c otherAlloc : allocationsByServiceOrderID.get(soID))
            {
                if(otherAlloc.IsDeleted)
                {
                    continue;
                }
                decimal amt = otherAlloc.Amount__c == null ? 0.0 : otherAlloc.Amount__c; 
                if(otherAlloc.Status__c == APS_PicklistValues.IFAllocation_Status_Approved)
                {
                    totApproved += amt;
                    if(otherAlloc.Allocation_Type__c == APS_PicklistValues.IFAllocation_Type_Recurrent)
                    {
                        totRecurrent += amt;
                    }
                }
                else
                {
                    totNotApproved += amt;
                    if(otherAlloc.Allocation_Type__c == APS_PicklistValues.IFAllocation_Type_OnceOff)
                    {
                        totNotApprovedOnceOff += amt;
                    }
                }
            }
            RCR_Contract__c so = new RCR_Contract__c(Id = soID);
            so.Allocation_Total_Approved__c = totApproved;
            so.Allocation_Total_Not_Approved__c = totNotApproved;
            so.Allocation_Total_Not_Approved_Once_Off__c = totNotApprovedOnceOff;
            so.Allocation_Total_Recurrent__c = totRecurrent;        
            serviceOrdersToUpdate.add(so);
    }
    TriggerBypass.RCRServiceOrder = true;
    update serviceOrdersToUpdate;
    
    private List<IF_Personal_Budget__c> getAllocations()
    {
        return [SELECT  Amount__c,
                        Status__c,
                        Allocation_Type__c,
                        RCR_Service_Order__c,
                        IsDeleted
                FROM    IF_Personal_Budget__c i
                WHERE   i.RCR_Service_Order__c IN :serviceOrderIDs AND
                        Status__c != :APS_PicklistValues.IFAllocation_Status_Rejected
                ALL ROWS];
    }
    
    private void checkAllocation(IF_Personal_Budget__c alloc, Map<ID, Financial_Year__c> pFinancialYearsByID)
    {       
        Financial_Year__c finYear = pFinancialYearsByID.get(alloc.Financial_Year__c);
        String errMsg = ServiceOrderFuture.checkServiceOrderAllocation(new RCR_Contract__c(Client__c = alloc.Client__c,
                                                                                        Start_Date__c = finYear.Start_Date__c,
                                                                                        Original_End_Date__c = finYear.End_Date__c),
                                                                        (Trigger.isDelete ? null : alloc));
        if(errMsg != null)
        {
            alloc.addError(errMsg);
        }
    }
    
    private void isStatusValid(IF_Personal_Budget__c alloc, IF_Personal_Budget__c oldValue)
    {   
        // bypass validation when updated from approval process 
        if(oldValue != null &&
                (alloc.Status__c != oldValue.Status__c ||
                alloc.Date_Allocated__c != oldValue.Date_Allocated__c ||
                alloc.RCR_Service_Order__c != oldValue.RCR_Service_Order__c))
        {
            return;
        }
        if(alloc.Status__c == APS_PicklistValues.IFAllocation_Status_New)
        {
            return;
        }
        if(alloc.Status__c == APS_PicklistValues.IFAllocation_Status_PendingApproval)
        {
            if(!UserUtilities.isUserInRole('RCR_IARA', UserInfo.getUserID(), false))
            {
                alloc.addError('Allocations cannot be updated or deleted when it has status of ' + alloc.Status__c + '.');
            }
        }
        else
        {
            alloc.addError('Allocations cannot be updated or deleted when it has a status other than New.');
        }
    }
}