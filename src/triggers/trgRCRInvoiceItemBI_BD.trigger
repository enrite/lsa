trigger trgRCRInvoiceItemBI_BD on RCR_Invoice_Item__c (before insert)
{
    if(TriggerByPass.RCRInvoiceItem)
    {
        return;
    }
    Set<Id> invoiceIDs = new Set<Id>();
    Boolean bulkUpload = false;
    for (RCR_Invoice_Item__c item : Trigger.new)
    {
        if(!bulkUpload && item.Bulk_Upload__c)
        {
            bulkUpload = true;
            break;
        }
        invoiceIDs.add(item.RCR_Invoice__c);
    }
    Map<ID, RCR_Invoice__c> invoicesByID;

    if(!bulkUpload)
    {
         invoicesByID = new Map<ID, RCR_Invoice__c>(
                        [SELECT ID,
                                RecordTypeID
                        FROM    RCR_Invoice__c
                        WHERE   ID IN :invoiceIDs]);
    }
    for (RCR_Invoice_Item__c item : Trigger.new)
    {
        if(!bulkUpload)
        {
            if(invoicesByID.get(item.RCR_Invoice__c).RecordTypeID ==
                        A2HCUtilities.getRecordType('RCR_Invoice__c', 'Submitted').ID)
            {
                item.addError('You cannot create an activity statement item for an activity statement that has been submitted.');
            }
        }
        item.Bulk_Upload__c = false;
    }
}