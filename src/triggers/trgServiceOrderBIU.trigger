trigger trgServiceOrderBIU on RCR_Contract__c (before insert, before update)
{    
    // this code replaces the Set Default Fields workflow
    if (Trigger.isInsert || Trigger.isUpdate || !TriggerBypass.RCRServiceOrder)
    {
        RCRServiceOrderFormulaReplacement.replace(Trigger.New, Trigger.OldMap);
    }

    ServiceOrderTrigger triggerClass;
    if(Trigger.isUpdate)
    {
        triggerClass = new ServiceOrderTrigger(Trigger.newMap,Trigger.oldMap);   
    }   
    else
    {
        triggerClass = new ServiceOrderTrigger(Trigger.newMap);
    }
    triggerClass.beforeInsertUpdate();
}