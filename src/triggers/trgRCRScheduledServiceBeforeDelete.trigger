trigger trgRCRScheduledServiceBeforeDelete on RCR_Contract_Scheduled_Service__c (before delete)
{
    Set<ID> sourceCssIds = new Set<ID>();
    for(RCR_Contract_Scheduled_Service__c css : [SELECT Source_Record__c
                                                FROM    RCR_Contract_Scheduled_Service__c
                                                WHERE   RCR_Contract__r.RecordType.DeveloperName = 'Amendment' AND
                                                        Source_Record__c != null AND
                                                        ID IN :Trigger.oldMap.keySet()])
    {
        sourceCssIds.add(css.Source_Record__c);
    }
    AggregateResult[] results = [SELECT     COUNT(ID) itemCount,
                                            RCR_Contract_Scheduled_Service__c
                                    FROM    RCR_Invoice_Item__c
                                    WHERE   RCR_Contract_Scheduled_Service__c IN :Trigger.oldMap.keySet() OR
                                            RCR_Contract_Scheduled_Service__c IN :sourceCssIds
                                    GROUP BY RCR_Contract_Scheduled_Service__c];

    for (RCR_Contract_Scheduled_Service__c ss : Trigger.old)
    {
        if(ss.Pre_Cancellation_Cost__c != null)
        {
            ss.addError('This contract has been cancelled, deleting this scheduled service will affect the total cost of the service order.');
            break;
        }
        Integer cnt = 0;
        for(AggregateResult result : results)
        {
            ID cssID = (ID)(result.get('RCR_Contract_Scheduled_Service__c'));
            if(ss.ID ==  cssID || ss.Source_Record__c == cssID)
            {
                cnt = (Integer)(result.get('itemCount'));
                break;
            }
        }
        if(cnt > 0)
        {
            if(sourceCssIds.contains(ss.Source_Record__c))
            {
                ss.addError('This scheduled service cannot be deleted as the original scheduled service for this amendment has associated activity statement items');
            }
            else
            {
                ss.addError('A scheduled service may not be deleted with associated activity statement items.');
            }
        }
    }
    delete [SELECT  ID
            FROM    Schedule__c
            WHERE   Name IN :Trigger.oldMap.keySet()];

    delete [SELECT  ID
            FROM    RCR_Contract_Service_Type__c
            WHERE   RCR_Contract_Scheduled_Service__c IN :Trigger.oldMap.keySet()];
}