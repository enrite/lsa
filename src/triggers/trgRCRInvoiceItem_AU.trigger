trigger trgRCRInvoiceItem_AU on RCR_Invoice_Item__c (after update)
{
    List<Approval.ProcessSubmitRequest> requests = new List<Approval.ProcessSubmitRequest>();
    Set<Id> serviceOrderIds = new Set<Id>();
    Map<ID, RCR_Invoice_Item__c> newValues = new Map<ID, RCR_Invoice_Item__c>();
                                                 
    if(!System.isBatch())
    {
        newValues = new Map<ID, RCR_Invoice_Item__c>(
                                                [SELECT  ID,
                                                         Status__c
                                                 FROM   RCR_Invoice_Item__c
                                                 WHERE ID IN :Trigger.newMap.keySet()]);
    }
    // fire approval process only if record type has changed to Submitted
    // needs to be in a trigger as portal users have no access to approval processes
    for(Integer i = 0; i < Trigger.new.size(); i++)
    {
        if(!System.isBatch())
        {
            if(Trigger.new[i].RecordTypeId == A2HCUtilities.getRecordType('RCR_Invoice_Item__c', 'Submitted').ID &&
                    Trigger.new[i].RecordTypeId != Trigger.old[i].RecordTypeId)
            {
                Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
                request.setObjectId(Trigger.new[i].Id);
                requests.add(request);
            }
            // only fire after successful approval process
            // also ignored if part of batch process, these will handle this explicitly
            if(Trigger.old[i].Status__c != APS_PicklistValues.RCRInvoiceItem_Status_Approved &&
                    newValues.get(Trigger.new[i].Id).Status__c == APS_PicklistValues.RCRInvoiceItem_Status_Approved)
            {
                serviceOrderIds.add(Trigger.new[i].RCR_Service_Order__c);
            }
        }
    }
    if(requests.size() > 0)
    {
        Approval.process(requests);
    }
    if(serviceOrderIds.size() > 0 && !Test.isRunningTest())
    {
        RCRReconcileInvoice.setServiceTotals(serviceOrderIds, true);
    }
}