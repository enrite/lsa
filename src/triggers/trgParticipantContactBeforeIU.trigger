trigger trgParticipantContactBeforeIU on Participant_Contact__c (before insert, before update) 
{
    // get the Ids of the parent records
    Set<Id> accountIds = new Set<Id>();
    Set<Id> planIds = new Set<Id>();
    for (Participant_Contact__c pc : Trigger.New)
    {
        accountIds.add(pc.Health_Facility__c);
        planIds.add(pc.Participant_Plan__c);                
    }
    
    // get the account records
    Map<Id, Account> accounts = new Map<Id, Account> ([SELECT Id,
                                                              ShippingStreet,
                                                              ShippingCity,
                                                              ShippingState,
                                                              ShippingPostalCode
                                                       FROM Account
                                                       WHERE Id IN :accountIds]);
    
    // get the plan records
    Map<Id, Plan__c> plans = new Map<Id, Plan__c> ([SELECT Id,
                                                           Client__c
                                                    FROM Plan__c
                                                    WHERE Id IN :planIds]);
    
    // get the participant contact record types
    Map<Id, RecordType> recordTypes = new Map<Id, RecordType> ([SELECT Id,
                                                                       Name,
                                                                       DeveloperName
                                                                FROM RecordType
                                                                WHERE sObjectType = 'Participant_Contact__c']);
                                                                            
    // loop through the participant contacts and assign the participant where necessary
    for (Participant_Contact__c pc : Trigger.New)
    {
        // if this is a practitioner then populate the address fields from the account
        if (pc.Health_Facility__c != null
            && (recordTypes.get(pc.RecordTypeId).DeveloperName == 'Consulting_Practitioner' || recordTypes.get(pc.RecordTypeId).DeveloperName == 'Treating_Practitioners'))
        {
            pc.Street__c = accounts.get(pc.Health_Facility__c).ShippingStreet;
            pc.Suburb__c = accounts.get(pc.Health_Facility__c).ShippingCity;
            pc.State__c = accounts.get(pc.Health_Facility__c).ShippingState;
            pc.Postcode__c = accounts.get(pc.Health_Facility__c).ShippingPostalCode;
        }
    
        // if we have a plan but no participant then assign the participant from the plan
        if (pc.Participant__c == null)
        {
            if (pc.Participant_Plan__c != null)
            {
                pc.Participant__c = plans.get(pc.Participant_Plan__c).Client__c;        
            }                      
        }
    }                                                          
}