trigger trgRCRServiceBIU on RCR_Service__c (before insert, before update)
{
    Set<ID> accountIDs = new Set<ID>();
    Map<String, List<RCR_Service__c>> servicesByAccount = new Map<String, List<RCR_Service__c>>();
    for(RCR_Service__c s : Trigger.new)
    {
        accountIDs.add(s.Account__c);
        servicesByAccount.put(s.Account__c, new List<RCR_Service__c>());
    }

    for(RCR_Service__c s : [SELECT      ID,
                                        Name,
                                        Level__c,
                                        Account__c
                                FROM    RCR_Service__c
                                WHERE   Account__c IN :accountIDs])
    {
        servicesByAccount.get(s.Account__c).add(s);
    }

    for(RCR_Service__c newService : Trigger.new)
    {
        for(RCR_Service__c s : servicesByAccount.get(newService.Account__c))
        {
            if(s.Id != newService.Id && newService.Name == s.Name && newService.Level__c == s.Level__c)
            {
                newService.addError('A Service with this Code and Level already exists for this Account.');
                break;
            }
        }
    }
}