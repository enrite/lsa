trigger trgPlanApprovalAfterIU on Plan_Approval__c(after update) 
{
    if(TriggerByPass.PlanApproval)
    {
        return;
    }
    TriggerByPass.PlanApprovalRecursion++;
    Map<String, RecordTypeInfo> paRtInfoByName = Schema.SObjectType.Plan_Approval__c.getRecordTypeInfosByName();
    Map<ID, RecordTypeInfo> paRtInfoById = Schema.SObjectType.Plan_Approval__c.getRecordTypeInfosById();
    Map<ID, Plan__c> plans = new Map<ID, Plan__c>();

    for (Plan_Approval__c pa : Trigger.New)
    {
        if(pa.Participant_Plan__c != null)
        {
            plans.put(pa.Participant_Plan__c, new Plan__c());
        }
    }

    List<Task> tasksToAdd = new List<Task>();
    List<Task> tasks = new List<Task>();
    List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
    for (Plan_Approval__c pa : Trigger.New)
    {
        if(plans.containsKey(pa.Participant_Plan__c))
        {
            plans.get(pa.Participant_Plan__c).Approval_Status__c = paRtInfoByID.get(pa.RecordTypeId).getName();
        }
        Plan_Approval__c oldPA = Trigger.oldMap.get(pa.ID);
        // NT 23/9 - cater for other levels of approval (i.e. not only CE Approval)
        if((TriggerByPass.PlanApprovalRecursion <= 1 &&              
                pa.RecordTypeId == paRtInfoByName.get(APSRecordTypes.PlanApproval_ChiefExecutiveApproval).getRecordTypeId() &&
                oldPA.Chief_Executive_Decision__c != APS_PicklistValues.PlanApproval_CEDecision_Approved && 
                pa.Chief_Executive_Decision__c == APS_PicklistValues.PlanApproval_CEDecision_Approved &&
                pa.Plan_Update_Required__c == APS_PicklistValues.PlanApproval_PlanUpdateRequired_Yes) ||
                (TriggerByPass.PlanApprovalRecursion <= 1 &&
                pa.Plan_Approval_Date__c != null &&
                oldPA.Plan_Approval_Date__c == null &&
                pa.Plan_Update_Required__c == APS_PicklistValues.PlanApproval_PlanUpdateRequired_Yes)               
                )
        {
            if(pa.Participant_Portal_Contact_ID__c != null)
            {
                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                email.setTargetObjectId(pa.Participant_Portal_Contact_ID__c); 
                email.setWhatId(pa.Participant_Plan__c); 
                email.setSaveAsActivity(true);
                emails.add(email);
            }
            else
            {
                Task newT = new Task();
                newT.Status = 'Not Started';
                newT.Subject = 'Print and Send a new copy of Plan';
                newT.Priority = 'Normal';
                newT.ActivityDate = Date.today();
                newT.WhatId = pa.Participant_Plan__c;
                newT.Assigned_to_Group__c = 'PSO';
                        
                tasksToAdd.add(newT);
            }
        }
    }

    if(!tasksToAdd.isEmpty())
    {
        List<GroupMember> groupMembers = [SELECT Id,
                                                 UserOrGroupId
                                        FROM    GroupMember
                                        WHERE   Group.Name = 'Program Support Officers'];
        for (Task bt : tasksToAdd)
        {
            if (String.isNotBlank(bt.Assigned_to_Group__c))
            {
                tasks.add(bt);
                continue;
            }
            for (GroupMember gm : groupMembers)
            {
                if (String.valueOf(gm.UserOrGroupId).left(3) == '005')
                {
                    Task t = bt.clone(false, true);
                    t.OwnerId = gm.UserOrGroupId;
                    tasks.add(t);
                }
            }
        }
    }
    if(tasks.size() > 0)
    {
        UPSERT tasks;
    }

    for(ID planID : plans.keySet())
    {
        Plan__c plan = plans.get(planID);
        if(plan.Approval_Status__c == null)
        {
            plans.remove(planID);
        }
        else
        {
            plan.ID = planID;
        }
    }
    update plans.values();

    if(!emails.isEmpty())
    {
        EmailTemplate template = [SELECT    Id
                                  FROM      EmailTemplate 
                                  WHERE     DeveloperName = 'New_Plan_Approved_Portal'];
        OrgWideEmailAddress owea = [SELECT  ID 
                                    FROM    OrgWideEmailAddress 
                                    WHERE   DisplayName = 'Lifetime Support Authority'];
        for(Messaging.SingleEmailMessage email : emails)
        {
            email.setTemplateID(template.Id);
            email.setOrgWideEmailAddressId(owea.ID);
        }
        Messaging.SendEmailResult [] results;
        Messaging.sendEmail(emails);
    }
}