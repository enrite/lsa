trigger trgServiceQuery_BIBU on Service_Query__c (before insert, before update)
{
    Set<ID> invoiceIds = new Set<ID>();
    Set<ID> serviceOrderIds = new Set<ID>();
    for(Service_Query__c sq : Trigger.new)
    {
        invoiceIds.add(sq.RCR_Invoice__c);
        serviceOrderIds.add(sq.RCR_Service_Order__c);
    }
    Map<ID, RCR_Invoice__c> invoicesByID = new Map<ID, RCR_Invoice__c>(
                                [SELECT ID,
                                        Account__c
                                FROM    RCR_Invoice__c
                                WHERE   ID In :invoiceIds]);
    Map<ID, RCR_Contract__c> serviceOrdersByID = new Map<ID, RCR_Contract__c>(
                                [SELECT ID,
                                        Account__c,
                                        RCR_Office_Team__r.RCR_Office__r.RCR_Region__c 
                                FROM    RCR_Contract__c
                                WHERE   ID In :serviceOrderIds]);

    for(Service_Query__c sq : Trigger.new)
    {
        String accountID = null;
        if(invoicesByID.containsKey(sq.RCR_Invoice__c))
        {
            accountID = invoicesByID.get(sq.RCR_Invoice__c).Account__c;
        }
        else if(serviceOrdersByID.containsKey(sq.RCR_Service_Order__c))
        {
            RCR_Contract__c so =  serviceOrdersByID.get(sq.RCR_Service_Order__c);
            accountID = so.Account__c;
            sq.Region__c = so.RCR_Office_Team__r.RCR_Office__r.RCR_Region__c ; 
        }
        sq.Service_Provider__c = accountID;
    }
}