trigger trgParticipantPlanAfterI on Plan__c (after insert) 
{
    // find the record types
    Map<Id, RecordType> recordTypes = new Map<Id, RecordType> ([SELECT Id,
                                                                       DeveloperName
                                                                FROM RecordType
                                                                WHERE sObjectType = 'Plan__c']);
    
    // find the client Ids
    Set<Id> clientIds = new Set<Id>();
    for (Plan__c p : Trigger.New)
    {
        clientIds.add(p.Client__c);
    }
           
    // find the client records
    Map<Id, Referred_Client__c> clients = new Map<Id, Referred_Client__c> ([SELECT Id,
                                                                                   Allocated_to__c,
                                                                                   Injury__c
                                                                            FROM Referred_Client__c
                                                                            WHERE Id IN :clientIds]);
                                                                
    List<Task> tasks = new List<Task>();
        
    for (Plan__c p : Trigger.New)
    {
        // create tasks for new discharge plans
        if (recordTypes.get(p.RecordTypeId).DeveloperName == 'Discharge_Plan')
        {
            if (clients.get(p.Client__c).Injury__c != null
                && clients.get(p.Client__c).Injury__c.toLowerCase().contains('brain injury'))
            {        
                Date dueDate = Date.today().addDays(14);
            
                Task t = new Task();
                t.OwnerId = clients.get(p.Client__c).Allocated_to__c;
                t.Subject = 'Discharge planning - CANS required';
                t.WhatId = p.Client__c;
                t.Status = 'Not Started';
                t.Priority = 'Normal';
                t.ActivityDate = dueDate;
                t.ReminderDatetime = Datetime.newInstance(dueDate.year(), dueDate.month(), dueDate.day(), 9, 0, 0);
                t.isReminderSet = true;                
             
                Tasks.add(t);    
            }                        
        }    
    }
                                                                            
    insert tasks;
}