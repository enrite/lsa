trigger trgRCRInvoiceItemBU on RCR_Invoice_Item__c (before update, before delete)
{
    Set<ID> invoiceIds = new Set<ID>();
    for(RCR_Invoice_Item__c i : (Trigger.isUpdate ? Trigger.new : Trigger.old))
    {
        invoiceIds.add(i.RCR_Invoice__c);
    }

    Map<ID, RCR_Invoice__c> invoicesByID = new Map<ID, RCR_Invoice__c>(
                        [SELECT ID,
                                RecordType.Name,
                                Status__c
                        FROM    RCR_Invoice__c
                        WHERE   ID IN :invoiceIds]);

    Map<ID, ProcessInstanceStep> rejectedCommentsByItemID = new Map<ID, ProcessInstanceStep>();
    if(Trigger.isUpdate)
    {
        for(ProcessInstanceStep pis : [SELECT   ProcessInstance.TargetObjectId,
                                                Comments
                                        FROM    ProcessInstanceStep p
                                        WHERE   StepStatus = 'Rejected' AND
                                                Comments != null AND
                                                ProcessInstance.TargetObjectId IN :Trigger.newMap.keySet()
                                        ORDER BY SystemModstamp DESC])
        {
            // only add the latest record, ignore any previous rejections
            if(!rejectedCommentsByItemID.containsKey(pis.ProcessInstance.TargetObjectId))
            {
                rejectedCommentsByItemID.put(pis.ProcessInstance.TargetObjectId, pis);
            }
        }
    }

    for(Integer i = 0; i < Trigger.old.size(); i++)
    {
        RCR_Invoice_Item__c oldItem = Trigger.old[i];
        if(Trigger.isUpdate)
        {
            RCR_Invoice_Item__c newItem = Trigger.new[i];
            if(newItem.LP_Claim_Key__c != null)
            {
                if(!UserUtilities.isAdminUser() &&
                        (newItem.Code__c != oldItem.Code__c ||
                        newItem.Activity_Date__c != oldItem.Activity_Date__c ||
                        newItem.Quantity__c != oldItem.Quantity__c ||
                        newItem.Rate__c != oldItem.Rate__c ||
                        newItem.GST__c != oldItem.GST__c ||
                        newItem.Total_Item_GST__c != oldItem.Total_Item_GST__c ||
                        newItem.Total__c != oldItem.Total__c))
                {
                    newItem.addError('LanternPay statement items cannot be modified');
                }
            }
            else if(newItem.Code__c != oldItem.Code__c ||
                    newItem.Activity_Date__c != oldItem.Activity_Date__c ||
                    newItem.Quantity__c != oldItem.Quantity__c ||
                    newItem.Rate__c != oldItem.Rate__c ||
                    newItem.GST__c != oldItem.GST__c ||
                    newItem.Total_Item_GST__c != oldItem.Total_Item_GST__c ||
                    newItem.Total__c != oldItem.Total__c ||
                    (newItem.Service_Order_Number__c != oldItem.Service_Order_Number__c &&
                            oldItem.Service_Order_Number__c != null) ||
                    newItem.Force_Reconcile_To__c != oldItem.Force_Reconcile_To__c)
            {
                newItem.RCR_Contract_Adhoc_Service__c = null;
                newItem.RCR_Contract_Scheduled_Service__c = null;
                newItem.Reconciled_Service__c = null;
                newItem.Error__c = null;
                checkRecordType(invoicesByID.get(newItem.RCR_Invoice__c), newItem);
            }
            // if approval process has been rejected and approver has entered a comment
            // replace error with the comment
            if(newItem.Override_Tolerance__c == 'Rejected' &&
                    newItem.Override_Tolerance__c != oldItem.Override_Tolerance__c &&
                    rejectedCommentsByItemID.containsKey(newItem.ID))
            {
                String comment = rejectedCommentsByItemID.get(newItem.ID).Comments;
                newItem.Error__c = comment.length() > 255 ? comment.substring(0, 255) : comment;
            }
        }
        else
        {
            checkRecordType(invoicesByID.get(oldItem.RCR_Invoice__c), oldItem);
        }
    }

    private void checkRecordType(RCR_Invoice__c invoice, RCR_Invoice_Item__c triggeringItem)
    {
        if(invoice.RecordType.Name == A2HCUtilities.getRecordType('RCR_Invoice__c', 'Submitted').Name)
        {
            triggeringItem.addError('Statement items cannot be modified after an activity statement has been submitted.');
        }
        else if(invoice.RecordType.Name == A2HCUtilities.getRecordType('RCR_Invoice__c', 'Processing').Name ||
                invoice.Status__c == 'Reconciliation In Progress')
        {
            triggeringItem.addError('Statement items cannot be modified after an activity statement has been scheduled for reconciliation.');
        }
    }
}