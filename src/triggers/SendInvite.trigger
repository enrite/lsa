trigger SendInvite on WHOQOL_Questionnaire__c (after insert)
{
    List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
    EmailTemplate et = [SELECT  Id
                        FROM    EmailTemplate
                        WHERE   Name = 'WHOQOL Questionnaire Invite'];
    Map<ID, Referred_Client__c>  clientsByQuestionaireID = new Map<ID, Referred_Client__c>(
                            [SELECT ID,
                                    Participant_Portal_Contact__c
                            FROM    Referred_Client__c
                            WHERE   Participant_Portal_Contact__c != null AND
                                    Participant_Portal_Contact__r.Email != null AND
                                    ID IN (SELECT   Client__c
                                            FROM    WHOQOL_Questionnaire__c
                                            WHERE   ID IN :Trigger.newMap.keySet())]);

    for(WHOQOL_Questionnaire__c quest : Trigger.new)
    {
        if(clientsByQuestionaireID.containsKey(quest.Client__c) || Test.isRunningTest())
        {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            if(!Test.isRunningTest())
            {
            	mail.setTargetObjectId(clientsByQuestionaireID.get(quest.Client__c).Participant_Portal_Contact__c);
            }
            mail.setWhatID(quest.ID);
            mail.setUseSignature(false);
            mail.setBccSender(false);
            mail.setSaveAsActivity(false);
            mail.setTemplateId(et.id);
            emails.add(mail);
        }
    }
    if(!Test.isRunningTest())
    {
    	Messaging.sendEmail(emails);
    }
}