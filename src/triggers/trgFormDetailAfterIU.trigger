trigger trgFormDetailAfterIU on Form_Detail__c (after insert, after update) 
{    
    // find the related Ids
    Set<Id> formIds = new Set<Id>();
    Set<Id> clientIds = new Set<Id>();
    for (Form_Detail__c fd : Trigger.New)
    {      
        formIds.add(fd.Form_Application_Assessment__c); 
        clientIds.add(fd.Client_Id__c);          
    }
    
    // find the client records
    Map<Id, Referred_Client__c> clients = new Map<Id, Referred_Client__c> ([SELECT Id,
                                                                                   Status__c,
                                                                                   Status_Reason__c,
                                                                                   Status_Date__c,
                                                                                   Note_for_Not_Accepted_Letter__c,
                                                                                   Allocated_to__c
                                                                            FROM Referred_Client__c
                                                                            WHERE Id IN :clientIds]);
                                                                            
    // find the form records
    Map<Id, Form__c> forms = new Map<Id, Form__c> ([SELECT Id,
                                                           Hospital_Facility__c,
                                                           Status__c
                                                    FROM Form__c
                                                    WHERE Id IN :formIds]);                                                                            
    
    // get the form detail record types
    Map<String, RecordType> recordTypes = new Map<String, RecordType>();
    for (RecordType rt : [SELECT Id,
                                 Name,
                                 DeveloperName,
                                 sObjectType
                          FROM RecordType
                          WHERE sObjectType IN ('Form_Detail__c', 'RCR_Contract__c')])
    {
        recordTypes.put(rt.sObjectType + ':' + rt.DeveloperName, rt);
    }                                                    
    
    // find the end of the current financial year
    Date endFinancialYear = Date.newInstance(Date.today().year(), 6, 30);
    if (Date.today().month() >= 7)
    {
        endFinancialYear = endFinancialYear.addYears(1);
    }
            
    Map<Id, Referred_Client__c> clientsToUpdate = new Map<Id, Referred_Client__c>();
    List<Form__c> formsToUpdate = new List<Form__c>();   
    List<RCR_Contract__c> newServiceOrders = new List<RCR_Contract__c>();
    List<Form_Detail__c> newFormDetails = new List<Form_Detail__c>();
    List<Task> newTasks = new List<Task>();     
            
    for (Form_Detail__c fd : Trigger.New)
    {                
        // if this is a status update then update the client
        if (fd.Status__c != null
            && (Trigger.OldMap == null || fd.Status__c != Trigger.OldMap.get(fd.Id).Status__c)
            && clients.containsKey(fd.Client_Id__c))
        {   
            // translate the new status if required
            String status = fd.Status__c;
            if (status == 'Referred back to Manager ASD')
            {
                status = 'Application referred back to Manager ASD';
            }
            else if (status == 'Referred back to Service Planner')
            {
                status = 'Application referred back to Service Planner';
            }
            
            if(fd.RecordTypeId != recordTypes.get('Form_Detail__c:Lifetime_Assessment_CE_Approval').Id 
                && fd.RecordTypeId != recordTypes.get('Form_Detail__c:Lifetime_Assessment_Start').Id
                    && fd.RecordTypeId != recordTypes.get('Form_Detail__c:Lifetime_Assessment_Complete').Id)
            {        
                // update the client
                clients.get(fd.Client_Id__c).Status__c = status;
                clients.get(fd.Client_Id__c).Status_Date__c = fd.Status_Date__c;
                clients.get(fd.Client_Id__c).Status_Reason__c = fd.Status_Reason__c;
            }
            // update the client
            clients.get(fd.Client_Id__c).Current_Status__c = status;
            clients.get(fd.Client_Id__c).Current_Status_Comment__c = fd.Status_Reason__c;
            clients.get(fd.Client_Id__c).Current_Status_Date__c = fd.Status_Date__c;
            clients.get(fd.Client_Id__c).Note_for_Not_Accepted_Letter__c = fd.Note_for_Not_Accepted_Letter__c;
            
                        
            clientsToUpdate.put(fd.Client_Id__c, clients.get(fd.Client_Id__c));
           
            // if status is anything other than not accepted then update the form and create a service order
            //if (fd.Status__c != null && fd.Status__c.startsWith('Accepted'))
            if (fd.Status__c != null && FormDetailEditExtension.AcceptedStatuses.contains(fd.Status__c))
            {
                // update the form status if this is a CE assessment
                if (forms.get(fd.Form_Application_Assessment__c).Status__c != 'CE Assessment Complete')
                {
                    forms.get(fd.Form_Application_Assessment__c).Status__c = 'CE Assessment Complete';
                    formsToUpdate.add(forms.get(fd.Form_Application_Assessment__c));
                }

                if (fd.Status__c == 'Accepted as Interim Participant')
                {
                    Task t = new Task();
                    t.OwnerId = clients.get(fd.Client_Id__c).Allocated_to__c;
                    t.WhatId = fd.Client_Id__c;
                    t.Subject = '90 days out from 2 years as an interim participant';
                    t.ActivityDate = Date.today().addYears(2).addDays(-90);
                    t.Description = 'A review of interim participation is due within 90 days - Please send correspondence informing the participant of the review, and commence gathering review materials';
                    
                    newTasks.add(t); 
                }
                     
                //BUG-00264                                
                /*RCR_Contract__c so = new RCR_Contract__c();
                so.RecordTypeId = recordTypes.get('RCR_Contract__c:New_Request').Id;
                so.Client__c = clients.get(fd.Client_Id__c).Id;
                so.Account__c = forms.get(fd.Form_Application_Assessment__c).Hospital_Facility__c;
                so.Start_Date__c = fd.Status_Date__c;
                so.Original_End_Date__c = endFinancialYear;
                so.Level__c = '1';
                so.Type__c = 'Direct Contract';
                so.Funding_Source__c = 'Direct Contract';
                so.New_Request_Type__c = 'New Service';
                //so.Service_Coordinator__c = UserInfo.getUserId();                
                so.Service_to_be_provided__c = 'Hospital Inpatient Stay';
                
                newServiceOrders.add(so); */
                
                //BUG-00262
                // if the status is Accepted as Interim Participant then a task needs to be created 6 months away
                /*if (fd.Status__c == 'Accepted as Interim Participant')
                {
                    Task t = new Task();
                    t.OwnerId = clients.get(fd.Client_Id__c).Allocated_to__c;
                    t.WhatId = fd.Client_Id__c;
                    t.Subject = '6 month review of Interim Participant';
                    t.ActivityDate = Date.today().addMonths(6);
                    t.Description = '6 month review of interim participant is required';  
                    Date fiveMonths = Date.today().addMonths(5);
                    t.ReminderDatetime = Datetime.newInstance(fiveMonths.year(), fiveMonths.month(), fiveMonths.day(), 9, 0, 0);           
                    t.IsReminderSet = true;
                    
                    newTasks.add(t);
                }*/
            }            
        }                
    }   
    
    update clientsToUpdate.values();
    update formsToUpdate;
    //insert newServiceOrders; 
    insert newFormDetails;                                                                       
    insert newTasks;
}