trigger trgIFAllocation_BU on IF_Personal_Budget__c (before update) 
{
    for(IF_Personal_Budget__c alloc : Trigger.new)
    {
        IF_Personal_Budget__c oldValue = Trigger.oldMap.get(alloc.ID);
        if(oldValue.Status__c == APS_PicklistValues.IFAllocation_Status_PendingApproval &&
            alloc.Status__c != APS_PicklistValues.IFAllocation_Status_PendingApproval)
        {
            alloc.OwnerId = alloc.CreatedById;
        }
    }
}