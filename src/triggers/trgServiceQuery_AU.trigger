trigger trgServiceQuery_AU on Service_Query__c (after update)
{
    Map<ID, User> internalUsersByID = new Map<ID, User>(
            [SELECT     ID,
                        Email
            FROM        User
            WHERE       ID IN (SELECT   Assigned_To__c
                                FROM    Service_Query__c
                                WHERE   ID IN :Trigger.newMap.keySet())]);
    for(Integer i = 0; i < Trigger.new.size(); i++)
    {
        List<Service_Query__c> updatedQueries = new List<Service_Query__c>();
        List<Service_Query__c>  internalQueries = new List<Service_Query__c>();
        Service_Query__c prev = Trigger.old[i];
        Service_Query__c curr = Trigger.new[i];

        if(curr.Status__c != prev.Status__c ||
                curr.Service_Provider__c != prev.Service_Provider__c ||
                curr.Resolution__c != prev.Resolution__c ||
                curr.RCR_Service_Order__c != prev.RCR_Service_Order__c ||
                curr.RCR_Invoice__c != prev.RCR_Invoice__c ||
                curr.Phone__c != prev.Phone__c ||
                curr.Email__c != prev.Email__c ||
                curr.Description__c != prev.Description__c ||
                curr.Contact_Name__c != prev.Contact_Name__c ||
                curr.Closed_Date__c != prev.Closed_Date__c)
        {
            updatedQueries.add(curr);
        }
        else if(curr.Internal_Notes__c != prev.Internal_Notes__c &&
                curr.LastModifiedByID != curr.Assigned_To__c &&
                curr.Assigned_To__c != null)
        {
            internalQueries.add(curr);
        }
        EmailManager mgr = new EmailManager();
        mgr.sendUpdatedServiceQueryEmails(updatedQueries, internalUsersByID);
        mgr.sendInternalServiceQueryEmails(internalQueries);
    }
}