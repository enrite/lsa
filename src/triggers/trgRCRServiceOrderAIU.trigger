trigger trgRCRServiceOrderAIU on RCR_Contract__c (after update) 
{
    if(TriggerBypass.RCRServiceOrder)
    {
        return;
    }
    if (Trigger.isUpdate)
    {
        ServiceOrderTrigger triggerClass = new ServiceOrderTrigger(Trigger.newMap,Trigger.oldMap);   
        triggerClass.afterUpdate();
    }
            
    // update any plans attached the parent clients
    Map<Id, Referred_Client__c> clients = new Map<Id, Referred_Client__c>();
//    Set<ID> planActionIds = new Set<ID>();
    for (RCR_Contract__c so : Trigger.New)
    {                    
        // update the plan if the service order has changed
        RCR_Contract__c oldValue = Trigger.OldMap.get(so.Id);
        if (so != oldValue)
        {
            clients.put(so.Client__c, new Referred_Client__c(Id = so.Client__c));
//            if(so.RecordTypeId == RCR_Contract__c.SObjectType.getDescribe().getRecordTypeInfosByName().get('Service Order').getRecordTypeId() &&
//                    so.Total_Service_Order_Cost__c != oldValue.Total_Service_Order_Cost__c &&
//                    so.Plan_Action__c != null)
//            {
//                planActionIds.add(so.Plan_Action__c);
//            }
        }
    }    
    
    if (clients.values().size() > 0)
    {                    
        update clients.values();
    }

//    if(!planActionIds.isEmpty())
//    {
//        ServiceOrderFuture.updatePlanActionTotalServiceOrderCost(planActionIds);
//    }
}