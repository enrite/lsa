trigger trgIFStage2Clients_BIBU on IF_Stage_2_Client_Identification__c (before insert, before update) 
{
    //Profile p = [SELECT Name FROM Profile WHERE Id = :Userinfo.getProfileId()];
       
    for (IF_Stage_2_Client_Identification__c cl: Trigger.new)
    {
        /*if(cl.Account__c == null && p.Name == 'RCR Portal') //Catches the lack of Account__c from the clone on portal
        {
            Id accId = [SELECT Account_ID__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1].Account_ID__c;
            cl.Account__c = accId;
        }*/
        
        if(cl.Account_ID__c == null) //Catches lack of Account_Id__c from clone for internal user
        {
            String accId = cl.Account2__c;
            cl.Account_ID__c = accId.substring(0,15);
        }
    }
}