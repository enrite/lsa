trigger trgRCRServiceOrderBeforeUpdate on RCR_Contract__c (before update) 
{               
    for(RCR_Contract__c contract : Trigger.new)
    {
        /*
        RCR_Contract__c oldValue = Trigger.oldMap.get(contract.Id);

        // If the Service Order was submitted for approval then recalled
        // return the values of the following fields back to what they were 
        // before submission.
        if(contract.Approval_Request_Recalled__c)
        {
            contract.CSA_Status__c = contract.CSA_Status_Recall__c; 
            contract.Approver_User_ID__c = contract.Approver_User_ID_Recall__c;
            contract.Approver_User_Name__c = contract.Approver_User_Name_Recall__c;   
            contract.CSA_Acceptance_Date__c = contract.CSA_Accepted_On_Recall__c;
            contract.Date_Approved__c = contract.Date_Approved_Recall__c;
            contract.Approval_Request_Recalled__c = false;                  
        }
        
        // If the Service Order has just been Accepted or Rejected by the Service Provider set the ownership of the record back to the Submitter.
        if((oldValue.CSA_Status__c == APS_PicklistValues.RCRContractCSAStatus_Pending_Acceptance || oldValue.CSA_Status__c == APS_PicklistValues.RCRContractCSAStatus_Pending_Approval) && 
           (contract.CSA_Status__c == APS_PicklistValues.RCRContractCSAStatus_Accepted || contract.CSA_Status__c == APS_PicklistValues.RCRContractCSAStatus_Rejected))
        {
            if(contract.Submited_For_Approval_User_ID__c != null)
            {
                contract.OwnerId = contract.Submited_For_Approval_User_ID__c;
            }
        }

        if(oldValue.Account__c != contract.Account__c)
        {
            //Get TrafficLight of new Service Provider based on Requets start and end dates
            RCR_Traffic_Light__c[] tl = [SELECT ID
                                        FROM RCR_Traffic_Light__c
                                        WHERE Service_Provider__c = :contract.Account__c
                                        AND (Date_Effective_from__c >= :contract.Start_Date__c AND Date_Effective_from__c < :contract.Original_End_Date__c)
                                        AND (Date_Effective_to__c > :contract.Start_Date__c AND Date_Effective_to__c > :contract.Original_End_Date__c)];
                                        
            if(tl.size() == 0)
            {
                contract.addError('The service provider selected does not have an associated Traffic Light.');
                break;
            }
        }
        */
    }
}