trigger trgGrantApplication on Grant_Application__c (before insert, before update, before delete, after insert, after update, after delete)
{
    private Map<String, Set<ID>> UserIdsByTierQueue
    {
        get
        {
            if(UserIdsByTierQueue == null)
            {
                UserIdsByTierQueue = new Map<String, Set<ID>>();
                for(GroupMember gm : [
                        SELECT UserOrGroupId,
                                Group.DeveloperName
                        FROM GroupMember
                        WHERE Group.DeveloperName IN ('Tier_1', 'Tier_2', 'Tier_3') AND
                        Group.Type = 'Queue' AND
                        UserOrGroupId IN (
                                SELECT ID
                                FROM User
                                WHERE IsActive = true
                        )
                ])
                {
                    if(!UserIdsByTierQueue.containsKey(gm.Group.DeveloperName))
                    {
                        UserIdsByTierQueue.put(gm.Group.DeveloperName, new Set<ID>());
                    }
                    UserIdsByTierQueue.get(gm.Group.DeveloperName).add(gm.UserOrGroupId);
                }
            }
            return UserIdsByTierQueue;
        }
        set;
    }

    if(Trigger.isUpdate)
    {
        Set<ID> approvalApplicationIds = new Set<ID>();
        if(Trigger.isBefore)
        {
            Set<ID> contactIds = new Set<ID>();
            for(Grant_Application__c ga : Trigger.new)
            {
                contactIds.add(ga.Contact__c);
            }
            Map<ID, Contact> contactsById = new Map<ID, Contact>([SELECT    ID,
                                                                            AccountID
                                                                    FROM    Contact
                                                                    WHERE   ID IN :contactIds]);
            for(Grant_Application__c ga : Trigger.new)
            {
                Contact c = contactsById.get(ga.Contact__c);
                ga.Account__c = c == null ? null : c.AccountID;
            }
        }
        if(Trigger.isAfter)
        {
            List<Task> newTasks = new List<Task>();
            for(Grant_Application__c ga : Trigger.new)
            {
                Grant_Application__c oldValue = Trigger.oldMap.get(ga.ID);
                if(ga.Status__c == 'Approved' && oldValue.Status__c != 'Approved')
                {
                    if(ga.Due_Date_For_Acceptance__c == null &&
                            (ga.Date_Approved__c == null ||
                            ga.Amount_Approved__c == null ||
                            ga.Milestones__c == 0))
                    {
                        ga.addError('Date Approved, Amount Approved and Milestones are required before a grant application can be approved');
                    }
                }
                if(ga.Status__c == 'Assessment' && oldValue.Status__c != 'Assessment')
                {
                    Set<ID> userIds = UserIdsByTierQueue.get(ga.Approval_Queue_Name__c);
                    if(userIds != null)
                    {
                        for(Id userId : userIds)
                        {
                            Task t = new Task();
                            t.WhatId = ga.ID;
                            t.ActivityDate = Date.today();
                            t.OwnerId = userId;
                            t.Subject = 'Grant application requires approval';
                            t.Description = 'A grant application requires your approval';
                            newTasks.add(t);
                        }
                    }
                    approvalApplicationIds.add(ga.ID);
                }
            }
            insert newTasks;
        }
        if(!approvalApplicationIds.isEmpty())
        {
            GrantExtension.sendApprovalEmails(approvalApplicationIds);
        }
    }
}