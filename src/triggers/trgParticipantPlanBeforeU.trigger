trigger trgParticipantPlanBeforeU on Plan__c (before update) 
{
//    Map<String, RecordType> recordTypes = new Map<String, RecordType>();
//    for (RecordType rt : [SELECT Id,
//            Name,
//            DeveloperName
//    FROM RecordType
//    WHERE sObjectType IN ('Plan__c','Form__c')])
//    {
//        recordTypes.put(rt.DeveloperName, rt);
//    }

    Set<Id> clientIds = new Set<Id>();
    for (Plan__c p : Trigger.New)
    {
        clientIds.add(p.Client_ID__c);
    }


    Map<Id,List<Form__c>> fimFamMap = new Map<Id, List<Form__c>>();
    List<Form__c> clientFormList = new List<Form__c>();
    Id prevClientId;
    Integer clientCount = 0;
    for( Form__c f : [SELECT Id, Name, Client__c, Date_of_assessment__c
                        FROM Form__c
                        WHERE   RecordType.DeveloperName = 'FIM_FAM_Score_Sheet'
                            AND     Client__c in :clientIds
                            AND     (Status__c = 'Submitted')
                        ORDER BY Date_of_assessment__c DESC])
    {
        if(prevClientId == null)
        {
            prevClientId = f.Client__c;
            clientCount = 1;
        }

        if(f.Client__c == prevClientId)
        {
            //add app to List
            clientFormList.add(f);
        }
        else
        {
            fimFamMap.put(prevClientId, clientFormList);
            clientFormList = new List<Form__c>();
            clientFormList.add(f);
            prevClientId = f.Client__c;
            clientCount++;
        }

    }
    if(clientCount == 1 && !clientFormList.isEmpty())
    {
        fimFamMap.put(prevClientId, clientFormList);
    }
    
    // loop through the applicable service order costs for the plans and assign
    for (AggregateResult ar : [SELECT Plan_Action__r.Participant_Plan__c planId,
                                      SUM(Total_Service_Order_Cost__c) cost
                               FROM RCR_Contract__c
                               WHERE RecordType.Name IN ('Service Order','New Request', 'Amendment')
                               AND Plan_Action__r.Participant_Plan__c IN :Trigger.NewMap.keySet()
                               GROUP BY Plan_Action__r.Participant_Plan__c])
    {
        Id planId = (Id)ar.get('planId');
            
        Trigger.NewMap.get(planId).Total_Request_Cost__c = (Decimal)ar.get('cost'); 
    }
    ID myPlanRecordTypeId = Plan__c.SObjectType.getDescribe().getRecordTypeInfosByDeveloperName().get('MyPlan').getRecordTypeId();
    for (Plan__c p : Trigger.New)
    {
        if (p.RecordTypeId == myPlanRecordTypeId)
        {
            if(fimFamMap.containsKey(p.Client_ID__c))
            {
                List<Form__c> clientFIMFAMList = new List<Form__c>(fimFamMap.get(p.Client_ID__c));
                Form__c latestFIMFAM;
                if(p.Plan_End_Date__c != null)
                {
                    latestFIMFAM = clientFIMFAMList[0];
                }
                else
                {
                    for(Form__c f : clientFIMFAMList)
                    {
                        if( f.Date_of_assessment__c <= p.Plan_End_Date__c)
                        {
                            latestFIMFAM = f;
                        }
                    }
                }
                p.Latest_FIM_FAM__c = latestFIMFAM == null ? p.Latest_FIM_FAM__c : latestFIMFAM.Id;
            }

        }
    }
}