trigger trgRCRAdHocServiceBeforeDelete on RCR_Contract_Adhoc_Service__c (before delete)
{
    AggregateResult[] results = [SELECT     COUNT(ID) itemCount,
                                            RCR_Contract_Adhoc_Service__c
                                    FROM    RCR_Invoice_Item__c
                                    WHERE   RCR_Contract_Adhoc_Service__c IN :Trigger.oldMap.keySet()
                                    GROUP BY RCR_Contract_Adhoc_Service__c];
    
    for (RCR_Contract_Adhoc_Service__c srvc : Trigger.old)
    {
        if(srvc.Pre_Cancellation_Amount__c != null)
        {
            srvc.addError('This contract has been cancelled, deleting this adhoc service will affect the total cost of the service order.');
            break;
        }       
        Integer cnt = 0;
        for(AggregateResult result : results)
        {
            if(srvc.ID == (String)(result.get('RCR_Contract_Adhoc_Service__c')))
            {
                cnt = (Integer)(result.get('itemCount'));
                break;
            }
        }
        if(cnt > 0)
        {
            srvc.addError('There are ' + cnt.format() + ' activity statement items reconciled against this AdHoc Service.');
            srvc.addError('An AdHoc Service may not be deleted with associated activity statement items.');
        }
    }

    delete [SELECT  ID
            FROM    RCR_Contract_Service_Type__c
            WHERE   RCR_Contract_Adhoc_Service__c IN :Trigger.oldMap.keySet()];
}