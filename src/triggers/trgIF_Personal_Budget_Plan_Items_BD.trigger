trigger trgIF_Personal_Budget_Plan_Items_BD on IF_Personal_Budget_Plan_Items__c (before delete) 
{
     RecordType roPlanItem = A2HCUtilities.getRecordType('IF_Personal_Budget_Plan_Items__c', 'IF Plan Item ReadOnly');
    
    for(IF_Personal_Budget_Plan_Items__c pi : trigger.old)
    {
        if(pi.RecordTypeId == roPlanItem.Id )
        {
            pi.AddError('This plan has been recommeded to a Team Leader so this Plan Item cannot be deleted.');
        }
    }
}