trigger trgAttachmentBeforeUD on Attachment (before update, before delete) 
{
    // only check is user is not a sys admin
    for (Profile p : [SELECT Id
                      FROM Profile
                      WHERE Id = :UserInfo.getProfileId()
                      AND Name != 'System Administrator'])
    {
        for (Attachment a : Trigger.Old)
        {
            // check if the attachment was a signature and if so don't allow updating/deleting
            if (a.Description != null && a.Description.startsWith('SIGNATURE:'))
            {
                if (Trigger.NewMap != null)
                {
                    Trigger.NewMap.get(a.Id).addError('You do not have permission to modify signatures');    
                }
                else
                {
                    a.addError('You do not have permission to delete signatures');
                }                
            }                   
        }
    }
}