trigger trgAdhocPaymentAfterU on Adhoc_Payment__c (after insert, after update, before insert, before update)
{
    // find the list of adhoc payments that have just become approved
    List<Adhoc_Payment__c> approved = new List<Adhoc_Payment__c>();
//    Set<ID> create30TasksIds = new Set<ID>();

    for(Adhoc_Payment__c p : Trigger.New)
    {
        if(Trigger.isUpdate)
        {
            Adhoc_Payment__c oldValue = Trigger.OldMap.get(p.Id);
            if(Trigger.isAfter)
            {
                if(p.Approval_Process_Step__c == 'Final Approval' &&
                        oldValue.Approval_Process_Step__c == 'Service Planner' &&
                        p.Final_Approver__c == null)
                {
                    p.addError('Manager approver is required before submitting the adhoc payment for approval.');
                }
                if(p.Approved__c
                        && !oldValue.Approved__c)
                {
                    if(p.Submitter__c == p.Approver__c)
                    {
                        p.addError('The submitter cannot approve the record. Please assign the approval request to a different user.');
                    }
                    else
                    {
                        approved.add(p);
                    }
                }
            }
//            if(p.Create_30_Day_Tasks__c && !oldValue.Create_30_Day_Tasks__c) {
//                create30TasksIds.add(p.ID);
//            }
        }
    }

//    EmailToInvoiceExtension.create30DayTasks(create30TasksIds);

    if(approved.size() > 0)
    {
        // find the list of clients and dates
        Set<String> keys = new Set<String>();
        Set<Id> accountIds = new Set<Id>();
        Set<String> glCodes = new Set<String>();
        for(Adhoc_Payment__c p : approved) {
            keys.add(p.Account_Client_Date_Matching_Key__c);
            accountIds.add(p.Account__c);
            if(p.GL_Code__c != null) glCodes.add(p.GL_Code__c);
            if(p.NonGST_GL_Code__c != null) glCodes.add(p.NonGST_GL_Code__c);
        }

        // find service orders that match the keys
        Map<String, RCR_Contract__c> serviceOrders = new Map<String, RCR_Contract__c>();
        for(RCR_Contract__c c : [
                SELECT Id,
                        Name,
                        Account__c,
                        Client__c,
                        Start_Date__c,
                        Original_End_Date__c,
                        Account_Client_Date_Matching_Key__c
                FROM RCR_Contract__c
                WHERE RecordType.DeveloperName = 'Service_Order'
                AND Plan_Action__c = null
                AND Account_Client_Date_Matching_Key__c IN :keys
        ]) {
            serviceOrders.put(c.Account_Client_Date_Matching_Key__c, c);
        }
        // remove any service orders with unapproved amendments
        Set<ID> serviceOrdersWithAmendmentIds = new Set<ID>();
        for(RCR_Contract__c amendment : [
                SELECT Source_Record__c
                FROM RCR_Contract__c
                WHERE RecordType.DeveloperName = 'Amendment'
                AND Status__c = 'New'
                AND Source_Record__c IN :serviceOrders.values()
        ]) {
            serviceOrdersWithAmendmentIds.add(amendment.Source_Record__c);
        }
        for(RCR_Contract__c so : serviceOrders.values()) {
            if(serviceOrdersWithAmendmentIds.contains(so.ID)) {
                serviceOrders.remove(so.Account_Client_Date_Matching_Key__c);
            }
        }

        // find the service types for the gl codes
        Map<String, RCR_Program_Category_Service_Type__c> serviceTypes = new Map<String, RCR_Program_Category_Service_Type__c>();
        for(RCR_Program_Category_Service_Type__c s : [
                SELECT Id,
                        Name,
                        RCR_Program_Category__r.RCR_Program__c,
                        RCR_Program_Category__r.Name,
                        Finance_Code_GG__c,
                        Finance_Code_NSAG__c
                FROM RCR_Program_Category_Service_Type__c
                WHERE Id IN :glCodes
        ]) {
            serviceTypes.put(s.Id, s);
        }

        // find the adhoc expenses service records
        Map<String, RCR_Service__c> adhocServices = new Map<String, RCR_Service__c>();
        for(RCR_Service__c s : [
                SELECT Id,
                        Account__c
                FROM RCR_Service__c
                WHERE Account__c IN :accountIds
                AND Name = 'Adhoc Expenses'
        ]) {
            adhocServices.put(s.Account__c, s);
        }

        // create adhoc expenses records as appropriate
        List<RCR_Service__c> newAdhocServices = new List<RCR_Service__c>();
        for(Id accountId : accountIds) {
            if(!adhocServices.containsKey(accountId)) {
                RCR_Service__c s = new RCR_Service__c();
                s.Account__c = accountId;
                s.Name = 'Adhoc Expenses';
                newAdhocServices.add(s);

                adhocServices.put(accountId, s);
            }
        }

        // insert any new services
        if(newAdhocServices.size() > 0) {
            insert newAdhocServices;
        }

        // loop through the new payments again to process the service orders
        for(Adhoc_Payment__c p : approved) {
            // if a service order was found for the payment then adjust its dates
            if(serviceOrders.containsKey(p.Account_Client_Date_Matching_Key__c)) {
                if(serviceOrders.get(p.Account_Client_Date_Matching_Key__c).Start_Date__c > p.Date_of_Transaction__c) {
                    serviceOrders.get(p.Account_Client_Date_Matching_Key__c).Start_Date__c = p.Date_of_Transaction__c;
                }

                if(serviceOrders.get(p.Account_Client_Date_Matching_Key__c).Original_End_Date__c < p.Date_of_Transaction__c) {
                    serviceOrders.get(p.Account_Client_Date_Matching_Key__c).Original_End_Date__c = p.Date_of_Transaction__c;
                }
            }
            // if a service order was not found for this payment then create one
            else {
                RCR_Contract__c c = new RCR_Contract__c();
                c.Account__c = p.Account__c;
                c.Client__c = p.Participant__c;
                c.Start_Date__c = p.Date_of_Transaction__c;
                c.Original_End_Date__c = p.Date_of_Transaction__c;
                c.RecordTypeId = A2HCUtilities.getRecordType('RCR_Contract__c', 'Service Order').Id;
                c.Status__c = 'Approved';
                c.Created_by_Adhoc_Payment__c = true;
                serviceOrders.put(p.Account_Client_Date_Matching_Key__c, c);
            }
        }

        // no need to fire triggers as no need for validations
        TriggerBypass.RCRServiceOrder = true;
        TriggerByPass.RCRInvoice = true;
        TriggerByPass.RCRInvoiceItem = true;

        // upsert the service orders
        upsert serviceOrders.values();

        // we'll need to make sure the service orders are populated with their new names
        Map<Id, RCR_Contract__c> soDetails = new Map<Id, RCR_Contract__c>();
        for(RCR_Contract__c so : serviceOrders.values()) {
            if(so.Name == null) {
                soDetails.put(so.Id, so);
            }
        }

        for(RCR_Contract__c so : [
                SELECT Id,
                        Name
                FROM RCR_Contract__c
                WHERE Id IN :soDetails.keySet()
        ]) {
            soDetails.get(so.Id).Name = so.Name;
        }

        // loop through the new payments again to create the scheduled services and activity statements
        List<Adhoc_Payment__c> updatePayments = new List<Adhoc_Payment__c>();
        Map<String, RCR_Contract_Scheduled_Service__c> scheduledServices = new Map<String, RCR_Contract_Scheduled_Service__c>();
        Map<String, RCR_Invoice__c> activityStatements = new Map<String, RCR_Invoice__c>();
        for(Adhoc_Payment__c p : approved) {
            // update the payments with their respective service orders
            updatePayments.add(new Adhoc_Payment__c(Id = p.Id, RCR_Service_Order__c = serviceOrders.get(p.Account_Client_Date_Matching_Key__c).Id));

            if(p.Amount__c != null) {
                RCR_Contract_Scheduled_Service__c css = new RCR_Contract_Scheduled_Service__c();
                css.Adhoc_Service_Description__c = 'Adhoc Payment with GST';
                css.RCR_Contract__c = serviceOrders.get(p.Account_Client_Date_Matching_Key__c).Id;
                css.RCR_Service__c = adhocServices.get(p.Account__c).Id;
                css.Start_Date__c = p.Date_of_Transaction__c;
                css.Original_End_Date__c = p.Date_of_Transaction__c;
                css.Total_Cost__c = p.Amount__c;
                css.Funding_Commitment__c = 'Once Off';
                css.Expenses__c = true;
                css.Public_holidays_not_allowed__c = APS_PicklistValues.RCRContractScheduledService_PublicHolidaysNotAllowed_UseExceptions;
                css.Use_Negotiated_Rates__c = true;
                css.Negotiated_Rate_Standard__c = p.Amount__c;
                css.Negotiated_Rate_Public_Holidays__c = p.Amount__c;
                scheduledServices.put(p.Id, css);
            }

            if(p.NonGST_Amount__c != null) {
                RCR_Contract_Scheduled_Service__c css = new RCR_Contract_Scheduled_Service__c();
                css.Adhoc_Service_Description__c = 'Adhoc Payment with no GST';
                css.RCR_Contract__c = serviceOrders.get(p.Account_Client_Date_Matching_Key__c).Id;
                css.RCR_Service__c = adhocServices.get(p.Account__c).Id;
                css.Start_Date__c = p.Date_of_Transaction__c;
                css.Original_End_Date__c = p.Date_of_Transaction__c;
                css.Total_Cost__c = p.NonGST_Amount__c;
                css.Funding_Commitment__c = 'Once Off';
                css.Expenses__c = true;
                css.Public_holidays_not_allowed__c = APS_PicklistValues.RCRContractScheduledService_PublicHolidaysNotAllowed_UseExceptions;
                css.Use_Negotiated_Rates__c = true;
                css.Negotiated_Rate_Standard__c = p.NonGST_Amount__c;
                css.Negotiated_Rate_Public_Holidays__c = p.NonGST_Amount__c;
                scheduledServices.put(p.Id + 'NonGST', css);
            }

            RCR_Invoice__c i = new RCR_Invoice__c();
            i.Account__c = p.Account__c;
            i.Invoice_Number__c = p.Name.left(12);
            i.Start_Date__c = p.Date_of_Transaction__c;
            i.End_Date__c = p.Date_of_Transaction__c;
            i.Date__c = p.Date_of_Transaction__c;
            i.Reject__c = false;
            i.Assigned_To__c = UserInfo.getUserId();
            i.RecordTypeId = A2HCUtilities.getRecordType('RCR_Invoice__c', 'Created').Id;
            activityStatements.put(p.Id, i);
        }

        // update the payments
        update updatePayments;

        // insert the scheduled services
        insert scheduledServices.values();

        // insert the activity statements
        insert activityStatements.values();

        List<Schedule__c> schedules = new List<Schedule__c>();
        for(RCR_Contract_Scheduled_Service__c css : scheduledServices.values()) {
            Schedule__c schedule = new Schedule__c();
            schedule.Name = css.ID;
            schedule.Start_Date__c = css.Start_Date__c;
            schedule.End_Date__c = css.End_Date__c;
            schedule.Number_of_weeks_in_period__c = APS_PicklistValues.Schedule_WeeksInPeriod_ServiceDateRange;
            schedule.Schedule_Recurrence_Pattern__c = 'Periodically';
            schedules.add(schedule);
        }
        insert schedules;
        List<Schedule_Period__c> schedulePeriods = new List<Schedule_Period__c>();
        for(Schedule__c schedule : schedules) {
            Schedule_Period__c schPeriod = new Schedule_Period__c(Schedule__c = schedule.ID);
            schPeriod.Sunday__c = 1;
            schPeriod.Monday__c = 0;
            schPeriod.Tuesday__c = 0;
            schPeriod.Wednesday__c = 0;
            schPeriod.Thursday__c = 0;
            schPeriod.Friday__c = 0;
            schPeriod.Saturday__c = 0;
            schPeriod.Week_Number__c = 1;
            schedulePeriods.add(schPeriod);
        }
        insert schedulePeriods;

        // loop through the new payments again to create the contract service types and the activity statement items
        List<RCR_Contract_Service_Type__c> contractServiceTypes = new List<RCR_Contract_Service_Type__c>();
        List<RCR_Invoice_Item__c> asItems = new List<RCR_Invoice_Item__c>();
        for(Adhoc_Payment__c p : approved) {
            if(serviceTypes.containsKey(p.GL_Code__c)) {
                RCR_Contract_Service_Type__c cst = new RCR_Contract_Service_Type__c();
                cst.RCR_Contract_Scheduled_Service__c = scheduledServices.get(p.Id).Id;
                cst.RCR_Program__c = serviceTypes.get(p.GL_Code__c).RCR_Program_Category__r.RCR_Program__c;
                cst.RCR_Program_Category_Service_Type__c = serviceTypes.get(p.GL_Code__c).Id;
                contractServiceTypes.add(cst);

                scheduledServices.get(p.Id).Service_Types__c = serviceTypes.get(p.GL_Code__c).Name;
                scheduledServices.get(p.Id).GL_Category__c = serviceTypes.get(p.GL_Code__c).RCR_Program_Category__r.Name;
            }

            if(serviceTypes.containsKey(p.NonGST_GL_Code__c)) {
                RCR_Contract_Service_Type__c cst = new RCR_Contract_Service_Type__c();
                cst.RCR_Contract_Scheduled_Service__c = scheduledServices.get(p.Id + 'NonGST').Id;
                cst.RCR_Program__c = serviceTypes.get(p.NonGST_GL_Code__c).RCR_Program_Category__r.RCR_Program__c;
                cst.RCR_Program_Category_Service_Type__c = serviceTypes.get(p.NonGST_GL_Code__c).Id;
                contractServiceTypes.add(cst);

                scheduledServices.get(p.Id + 'NonGST').Service_Types__c = serviceTypes.get(p.NonGST_GL_Code__c).Name;
                scheduledServices.get(p.Id + 'NonGST').GL_Category__c = serviceTypes.get(p.NonGST_GL_Code__c).RCR_Program_Category__r.Name;
            }

            if(p.Amount__c != null) {
                RCR_Invoice_Item__c it = new RCR_Invoice_Item__c();
                it.RCR_Invoice__c = activityStatements.get(p.Id).Id;
                it.RCR_Contract_Scheduled_Service__c = scheduledServices.get(p.Id).Id;
                it.Reconciled_Service__c = p.Name.left(20);
                it.Service_Order_Number__c = serviceOrders.get(p.Account_Client_Date_Matching_Key__c).Name;
                it.Code__c = 'Adhoc Expenses';
                it.RCR_Service_Order__c = serviceOrders.get(p.Account_Client_Date_Matching_Key__c).Id;
                it.Service_Order_Number__c = serviceOrders.get(p.Account_Client_Date_Matching_Key__c).Name;
                it.Activity_Date__c = p.Date_of_Transaction__c;
                it.GST__c = p.GST__c;
                it.Total_Item_GST__c = p.GST__c;
                it.Total__c = (p.Amount__c != null ? p.Amount__c : 0) + (p.GST__c != null ? p.GST__c : 0);
                it.Quantity__c = 1.0;
                it.Rate__c = 1.00;
                it.RecordTypeId = A2HCUtilities.getRecordType('RCR_Invoice_Item__c', 'Submitted').Id;
                asItems.add(it);
            }

            if(p.NonGST_Amount__c != null) {
                RCR_Invoice_Item__c it = new RCR_Invoice_Item__c();
                it.RCR_Invoice__c = activityStatements.get(p.Id).Id;
                it.RCR_Contract_Scheduled_Service__c = scheduledServices.get(p.Id + 'NonGST').Id;
                it.Reconciled_Service__c = p.Name.left(20);
                it.Service_Order_Number__c = serviceOrders.get(p.Account_Client_Date_Matching_Key__c).Name;
                it.Code__c = 'Adhoc Expenses';
                it.RCR_Service_Order__c = serviceOrders.get(p.Account_Client_Date_Matching_Key__c).Id;
                it.Service_Order_Number__c = serviceOrders.get(p.Account_Client_Date_Matching_Key__c).Name;
                it.Activity_Date__c = p.Date_of_Transaction__c;
                it.GST__c = 0;
                it.Total_Item_GST__c = 0;
                it.Total__c = p.NonGST_Amount__c;
                it.Quantity__c = 1.0;
                it.Rate__c = 1.00;
                it.RecordTypeId = A2HCUtilities.getRecordType('RCR_Invoice_Item__c', 'Submitted').Id;
                asItems.add(it);
            }
        }

        // insert the contract service types
        insert contractServiceTypes;

        // update the scheduled services again to ensure the funding details are created correctly
        update scheduledServices.values();

        // insert the activity statement items
        insert asItems;

        // update the activity statements to submitted
        for(RCR_Invoice__c i : activityStatements.values()) {
            i.RecordTypeId = A2HCUtilities.getRecordType('RCR_Invoice__c', 'Submitted').Id;
        }
        update activityStatements.values();

        Map<ID, List<Attachment>> attachmentsByAdhocId = new Map<ID, List<Attachment>>();
        List<Attachment> clonedAttachments = new List<Attachment>();
        Set<String> downloadableFileTypes = new Set<String>(Email2Invoice_File_Types__c.getOrgDefaults().File_Extensions__c.split(';'));
        for(Attachment a : [SELECT ID,
                                    Name,
                                    ContentType,
                                    Body,
                                    Description,
                                    ParentId,
                                    IsPrivate
                             FROM   Attachment
                            WHERE   ParentId IN :approved])
        {
            if(!attachmentsByAdhocId.containsKey(a.ParentId))
            {
                attachmentsByAdhocId.put(a.ParentId, new List<Attachment>());
            }
            attachmentsByAdhocId.get(a.ParentId).add(a);
        }
        if(!attachmentsByAdhocId.isEmpty() && !activityStatements.isEmpty())
        {
            for(ID adhocPaymentId : activityStatements.keySet())
            {
                for(Attachment a : attachmentsByAdhocId.get(adhocPaymentId))
                {
                    if(downloadableFileTypes.contains(a.Name.substringAfterLast('.')))
                    {
                        Attachment clonedAttachment = a.clone(false, true);
                        clonedAttachment.ParentId = activityStatements.get(adhocPaymentId).ID;
                        clonedAttachments.add(clonedAttachment);
                    }
                }
            }
            insert clonedAttachments;
        }
    }
}