trigger trgFormDetailBeforeI on Form_Detail__c (before insert, before update)
{
    // find the form Ids
    Set<Id> formIds = new Set<Id>();
    for (Form_Detail__c fd : Trigger.New)
    {
        formIds.add(fd.Form_Motor_Vehicle__c);
        formIds.add(fd.Form_Witness__c);
        formIds.add(fd.Form_Application_Assessment__c);
    }
    
    // find the form records
    Map<Id, Form__c> forms;
    if(Trigger.isInsert)
    {
        forms = new Map<Id, Form__c>([SELECT Id,
                                            OwnerId
                                    FROM    Form__c
                                    WHERE   Id IN :formIds AND
                                            Owner.IsActive = true]);
    }

    Map<ID, User> formDetailOwners;
    if(Trigger.isUpdate)
    {
        formDetailOwners = new Map<ID, User>([SELECT ID,
                                                    IsActive
                                            FROM    User
                                            WHERE   IsActive = true AND
                                                    ID IN (SELECT    OwnerId
                                                            FROM    Form_Detail__c
                                                            WHERE   ID IN :Trigger.new)]);
    }
    // loop through the form details and set the owner to the owner of the parent form
    for (Form_Detail__c fd : Trigger.New)
    {
        if(Trigger.isInsert)
        {
            if(forms.containsKey(fd.Form_Motor_Vehicle__c))
            {
                fd.OwnerId = forms.get(fd.Form_Motor_Vehicle__c).OwnerId;
            }
            else if(forms.containsKey(fd.Form_Witness__c))
            {
                fd.OwnerId = forms.get(fd.Form_Witness__c).OwnerId;
            }
            else if(forms.containsKey(fd.Form_Application_Assessment__c))
            {
                fd.OwnerId = forms.get(fd.Form_Application_Assessment__c).OwnerId;
            }
        }
        else
        {
            if(!formDetailOwners.containsKey(fd.OwnerId))
            {
                fd.OwnerId = UserUtilities.ManagerASDUser.ID;
            }
        }
    }
}