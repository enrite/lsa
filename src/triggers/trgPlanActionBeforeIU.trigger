trigger trgPlanActionBeforeIU on Plan_Action__c (before insert, before update, before delete)
{    
    if(TriggerBypass.PlanAction)
    {
        return;
    }
    Map<Id,Decimal> serviceOrderTotalCosts = new Map<Id,Decimal>();
    Set<Id> planIds = new Set<Id>();

    private final String SERVICEPLANNERRECOMMENDATION = 'Service_Planner_Recommendation';
    private final String SUPERSEDEDPLANAPPROVAL = 'Superseded_Plan_Approval';
    private final String PREAPPROVEDDISCHARGESERVICE = 'Preapproved_Discharge_Service';
    private final String PREAPPROVEDPRODUCTSANDEQUIPMENT = 'Preapproved_Products_and_Equipment';
    private final String PREAPPROVEDDESEQUIPMENT = 'Pre-Approved DES Equipment';

    private Map<String, List<Pre_Approved_Item__c>> PreApprovedItemsByName
    {
        get
        {
            if(PreApprovedItemsByName == null)
            {
                PreApprovedItemsByName = new Map<String, List<Pre_Approved_Item__c>>();
                for(Pre_Approved_Item__c pai : [SELECT ID,
                                                        Name,
                                                        Active_From_Date__c,
                                                        Pre_Approved_Amount__c,
                                                        MyPlan_Pre_Approved_Amount__c,
                                                        Pre_Approved_Provider__c
                                                FROM    Pre_Approved_Item__c
                                                ORDER BY Name,
                                                        Active_From_Date__c DESC])
                {
                    if(!PreApprovedItemsByName.containsKey(pai.Name))
                    {
                        PreApprovedItemsByName.put(pai.Name, new List<Pre_Approved_Item__c>());
                    }
                    PreApprovedItemsByName.get(pai.Name).add(pai);
                }
            }
            return PreApprovedItemsByName;
        }
        set;
    }

    if(Trigger.isDelete)
    {
        User currUser = UserUtilities.getUser(UserInfo.getUserId());
        for(Plan_Action__c pa : Trigger.old)
        {
            if((pa.Approved__c || pa.Plan_Approval__c != null) && currUser.Profile.Name != 'System Administrator')
            {
                pa.addError('Approved Plan Actions can only be deleted by System Administrators.');
            }
        }
    }
    else
    {
        for (Plan_Action__c pa : Trigger.New)
        {
            planIds.add(pa.Participant_Plan__c);
        }

        if(Trigger.isUpdate)
        {
            for(RCR_Contract__c so: [SELECT Total_Service_Order_Cost__c,
                                            Plan_Action__c
                                    FROM    RCR_Contract__c
                                    WHERE   Plan_Action__c IN :Trigger.newMap.keySet()])
            {
                if(!serviceOrderTotalCosts.containsKey(so.Plan_Action__c))
                {
                    serviceOrderTotalCosts.put(so.Plan_Action__c, 0.0);
                }
                serviceOrderTotalCosts.put(so.Plan_Action__c, (serviceOrderTotalCosts.get(so.Plan_Action__c) + so.Total_Service_Order_Cost__c));
            }
        }

        // find the plan records
        Map<Id, Plan__c> planMap = new Map<Id, Plan__c> ([
                SELECT Id,
                        Client__c,
                        Do_not_create_Amendment__c,
                        Plan_Start_Date__c,
                        Plan_End_Date__c,
                        RecordType.DeveloperName,
                (
                        SELECT Id,
                                Pre_Defined_Services__c
                        FROM Plan_Actions__r
                        WHERE RecordType.DeveloperName IN (:PREAPPROVEDDISCHARGESERVICE, :PREAPPROVEDPRODUCTSANDEQUIPMENT) AND
                        Pre_Defined_Services__c != :PREAPPROVEDDESEQUIPMENT AND
                        Participant_Plan__r.RecordType.DeveloperName != 'MyPlan'
                )
                FROM Plan__c
                WHERE Id IN :planIds
        ]);

        Map<String, ID> planApprovalRtByDeveloperName = Utils.GetRecordTypeIdsByDeveloperName(Plan_Approval__c.SObjectType, true);
        Map<String, ID> planActionRtByDeveloperName = Utils.GetRecordTypeIdsByDeveloperName(Plan_Action__c.SObjectType, true);

        for(Plan_Action__c pa : Trigger.New)
        {
            if(pa.RecordTypeId == planActionRtByDeveloperName.get(PREAPPROVEDDISCHARGESERVICE) ||
                    pa.RecordTypeId == planActionRtByDeveloperName.get(PREAPPROVEDPRODUCTSANDEQUIPMENT))
            {
                if(!UserUtilities.userHasRole('Service_Planners', UserInfo.getUserId()) && !UserUtilities.userHasRole('Lead_Service_Planner', UserInfo.getUserId()))
                {
                    Boolean error = Trigger.isInsert;
                    if(!error)
                    {
                        Boolean currentRecentlyUpdated = pa.Recently_Updated__c;
                        Plan_Action__c oldValue = Trigger.OldMap.get(pa.Id);
                        pa.Recently_Updated__c = oldValue.Recently_Updated__c;
                        error = pa != oldValue;
                        pa.Recently_Updated__c = currentRecentlyUpdated;
                    }
                    if(error)
                    {
                        pa.addError('Preapproved Plan Actions can only be created/modified by Service Planners or their superiors');
                    }
                }

                Plan__c plan = planMap.get(pa.Participant_Plan__c);
                Boolean dESEquipmentProviderFound = false;
                if(PreApprovedItemsByName.containsKey(pa.Pre_Defined_Services__c))
                {
                    // ordered by from date DESC, find the first record that has start date <= plan start date
                    for(Pre_Approved_Item__c pai : PreApprovedItemsByName.get(pa.Pre_Defined_Services__c))
                    {
                        if(pai.Active_From_Date__c <= plan.Plan_Start_Date__c)
                        {
                            Decimal preApprovedAmount = null;
                            if(plan.RecordType.DeveloperName == 'MyPlan')
                            {
                                preApprovedAmount = pai.MyPlan_Pre_Approved_Amount__c;
                            }
                            else if(plan.RecordType.DeveloperName == 'Discharge_Plan')
                            {
                                preApprovedAmount = pai.Pre_Approved_Amount__c;
                            }

                            if(pa.Pre_Defined_Services__c != PREAPPROVEDDESEQUIPMENT)
                            {
                                if(pa.Estimated_Cost__c > preApprovedAmount)
                                {
                                    pa.Estimated_Cost__c.addError(pa.Pre_Defined_Services__c + ' cost must not be greater than $' + String.valueOf(preApprovedAmount));
                                }
                                break;
                            }
                            else
                            {
                                if(pa.Provider__c == null)
                                {
                                    pa.Provider__c.addError('Provider is required for Pre-Approved DES Equipment');
                                }
                                if(pai.Pre_Approved_Provider__c == pa.Provider__c)
                                {
                                    dESEquipmentProviderFound = true;
                                    if(pa.Estimated_Cost__c > preApprovedAmount)
                                    {
                                        pa.Estimated_Cost__c.addError(PREAPPROVEDDESEQUIPMENT + ' cost must not be greater than $' + String.valueOf(preApprovedAmount) + ' for this provider');
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
                if(pa.Estimated_Cost__c == null)
                {
                    pa.addError('No Preapproved item with set costs available.');
                }
                if(pa.Pre_Defined_Services__c == PREAPPROVEDDESEQUIPMENT && !dESEquipmentProviderFound)
                {
                    pa.addError('No Preapproved item found for this provider.');
                }
                for(Plan_Action__c otherPa : plan.Plan_Actions__r)
                {
                    if(pa.ID != otherPa.ID && pa.Pre_Defined_Services__c == otherPa.Pre_Defined_Services__c)
                    {
                        pa.addError('An instance of ' + pa.Pre_Defined_Services__c + ' is already defined for this plan.');
                    }
                }
            }
            else
            {
                if(planMap.get(pa.Participant_Plan__c).Do_not_create_Amendment__c)
                {
                    continue;
                }
                // if this is an insert or the record has changed then we need a service planner recommendation plan approval
                if(Trigger.isUpdate)
                {
                    Plan_Action__c oldValue = Trigger.OldMap.get(pa.Id);
                    if(pa != oldValue &&
                            pa.Approved__c &&
                            pa.Approved__c == oldValue.Approved__c &&
                            pa.Used_Contingency__c == oldValue.Used_Contingency__c)
                    {
                        if(pa.Amendment_Details__c == null)
                        {
                            pa.Amendment_Details__c.addError('You must provide the details of the amendment');
                        }
                    }
                }
            }
        }

        // loop through the new plan actions and assign values to fields
        for(Plan_Action__c pa : Trigger.New)
        {
            if(pa.RecordTypeId == planActionRtByDeveloperName.get(PREAPPROVEDDISCHARGESERVICE) ||
                    pa.RecordTypeId == planActionRtByDeveloperName.get(PREAPPROVEDPRODUCTSANDEQUIPMENT))
            {
                pa.Total_Service_Order_Cost__c = pa.Estimated_Cost__c;
                pa.Approved__c = true;
            }
            else
            {
                // if this is an insert or the record has changed then assign a new plan approval
                if(Trigger.isInsert ||
                        (pa != Trigger.OldMap.get(pa.Id) &&
                                pa.Approved__c == true &&
                                pa.Approved__c == Trigger.OldMap.get(pa.Id).Approved__c &&
                                pa.Used_Contingency__c == Trigger.OldMap.get(pa.Id).Used_Contingency__c &&
                                !planMap.get(pa.Participant_Plan__c).Do_not_create_Amendment__c))
                {
                    pa.Approved__c = false;
                    pa.Plan_Approval__c = null;
                    // only change this when not already changed
                    if(Trigger.isUpdate && Trigger.OldMap.get(pa.Id).Ready_to_Approve__c)
                    {
                        pa.Ready_to_Approve__c = false;
                    }
                }
                // if record has become unapproved then make sure we store the previously approved estimated cost
                if(Trigger.isUpdate
                        && Trigger.OldMap.get(pa.Id).Approved__c
                        && !pa.Approved__c)
                {
                    pa.Previously_Approved_Cost__c = Trigger.OldMap.get(pa.Id).Estimated_Cost__c;
                }
                pa.Total_Service_Order_Cost__c = serviceOrderTotalCosts.get(pa.Id);
            }

            // assign the parent client to the client lookup
            pa.Client__c = planMap.get(pa.Participant_Plan__c).Client__c;
        }
    }
}