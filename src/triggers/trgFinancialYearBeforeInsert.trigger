trigger trgFinancialYearBeforeInsert on Financial_Year__c (before insert) 
{
    List<Financial_Year__c> fys = [SELECT Start_Year__c
                                   FROM Financial_Year__c];

    for (Financial_Year__c newFy : Trigger.New)
    {
        try
        {
            if (newFy.Start_Year__c.length() < 4)
            {
                newFy.addError('The Start Year must be a 4 digit number');
                continue;
            }
                               
            for (Financial_Year__c fy : fys)
            {
                if (fy.Start_Year__c == newFy.Start_Year__c)
                {
                    newFy.addError('This financial year already exists');
                    break;    
                }
            }  
        
            newFy.Name = Integer.valueOf(newFy.Start_Year__c) + ' - ' + (Integer.valueOf(newFy.Start_Year__c) + 1);
        }
        catch (Exception e)
        {
            newFy.addError(e);
        }
    }
}