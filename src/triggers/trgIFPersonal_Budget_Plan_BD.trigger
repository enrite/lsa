trigger trgIFPersonal_Budget_Plan_BD on IF_Personal_Budget_Plan__c (before delete) 
{
    RecordType fac = A2HCUtilities.getRecordType('IF_Personal_Budget_Plan__c', 'Review by Facilitator');
    
    for(IF_Personal_Budget_Plan__c plan: trigger.old)
    {
        if(plan.RecordTypeId != fac.Id )
        {
            plan.AddError('This plan has been recommeded to a Team Leader so it cannot be deleted.');
        }
    }
}