trigger trgRCRServiceOrderAD on RCR_Contract__c (after delete) {
	if (Trigger.isDelete)
    {
        ServiceOrderTrigger triggerClass = new ServiceOrderTrigger(Trigger.newMap,Trigger.oldMap);   
        triggerClass.afterDelete();
    }
}