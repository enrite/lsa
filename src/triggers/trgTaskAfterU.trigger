trigger trgTaskAfterU on Task (after insert, after update, after delete) 
{
    List<User> FinanceUsers
    {
        get 
        {
            if(FinanceUsers == null)
            {
                FinanceUsers = [SELECT ID 
                                FROM    User 
                                WHERE   User.IsActive = true AND 
                                        ID IN (SELECT UserOrGroupId
                                                FROM    GroupMember
                                                WHERE   Group.DeveloperName = 'Finance')];
            }
            return FinanceUsers;
        }
        set;
    } 

    List<User> SeniorAccountantUsers
    {
        get 
        {
            if(SeniorAccountantUsers == null)
            {
                SeniorAccountantUsers = [SELECT ID 
                                FROM    User 
                                WHERE   User.IsActive = true AND 
                                        ID IN (SELECT UserOrGroupId
                                                FROM    GroupMember
                                                WHERE   Group.DeveloperName = 'Senior_Accountant')];
            }
            return SeniorAccountantUsers;
        }
        set;
    } 

    // find any requests for the tasks
    Set<Id> whatIds = new Set<Id>();
    Map<ID, Adhoc_Payment__c> paymentsByID = new  Map<ID, Adhoc_Payment__c>();
    Map<ID, RCR_Invoice__c> invoicesByID = new  Map<ID, RCR_Invoice__c>();
    List<Task> newTasks = new List<Task>();
    for (Task t : Trigger.isDelete ? Trigger.Old : Trigger.New)
    {
        if(t.WhatId == null)
        {
            continue;
        }
        whatIds.add(t.WhatId);
        if(t.WhatId.getSobjectType() == Schema.Adhoc_Payment__c.SObjectType && 
            !paymentsByID.containsKey(t.WhatId))
        {
            paymentsByID.put(t.whatID, new Adhoc_Payment__c(ID = t.whatID, Number_of_open_tasks__c = 0));
        }
        else if(t.WhatId.getSobjectType() == Schema.RCR_Invoice__c.SObjectType && 
            !invoicesByID.containsKey(t.WhatId))
        {
            invoicesByID.put(t.whatID, new RCR_Invoice__c(ID = t.whatID, Number_of_open_tasks__c = 0));
        }
    }
    if(!paymentsByID.isEmpty() || !invoicesByID.isEmpty())
    {
        AggregateResult[] results = [SELECT COUNT(ID) cnt,
                                           WhatID 
                                    FROM   Task 
                                    WHERE  Status != 'Completed' AND 
                                            (WhatID IN :paymentsByID.keySet() OR 
                                            WhatID IN :invoicesByID.keySet())
                                    GROUP BY WhatID];
        for(AggregateResult result : results)
        {
            ID whatID = ID.valueOf((String)result.get('WhatID'));
            Integer count = ((Decimal)result.get('cnt')).intValue();
            if(paymentsByID.containsKey(whatId))
            {   
                paymentsByID.get(whatID).Number_of_open_tasks__c += count;
            }
            else if(invoicesByID.containsKey(whatId))
            {
                invoicesByID.get(whatID).Number_of_open_tasks__c += count;
            }
        }
        update paymentsByID.values();
        update invoicesByID.values();
    }
    if(Trigger.isUpdate)
    {
        Map<Id, Request__c> requests = new Map<Id, Request__c> ([SELECT Id,
                                                                        Status__c,
                                                                        Response__c
                                                                 FROM Request__c
                                                                 WHERE Id IN :whatIds]);
                                                             
        Map<Id, Referred_Client__c> clients = new Map<Id, Referred_Client__c>();
        List<Request__c> newRequests = new List<Request__c>();
    
        for (Task t : Trigger.New)
        {
            Task oldValue = Trigger.OldMap.get(t.Id);
            // has the status has changed to completed and is the type VCR requested
            if (t.LSA_Task_Type__c == 'Request Vehicle Collision Report'
                && t.Status == 'Completed'
                && oldValue.Status != 'Completed' 
                && t.WhatId != null
                && t.WhatId.getSobjectType() == Schema.Referred_Client__c.SObjectType)
            {
                // update vcr requested on the client
                clients.put(t.WhatId, new Referred_Client__c(Id = t.WhatId, VCR_Requested__c = true));
            }
        
            // is there a request and does it need to be updated?
            if (requests.containsKey(t.WhatId))
            {
                Request__c r = requests.get(t.WhatId);
            
                if (r.Status__c != t.Status
                    || r.Response__c != t.Description)
                {
                    r.Status__c = t.Status;
                    r.Response__c = t.Description;
                    newRequests.add(r);
                }
            }  
            if(t.Create_Reconciliation_Reminder__c && !oldValue.Create_Reconciliation_Reminder__c)  
            {
                for(User u : FinanceUsers)
                {
                    Task newTask = new Task();
                    newTask.Status = 'Not Started';
                    newTask.WhatId = t.WhatId;
                    newTask.Subject = 'Reminder for Invoice failed reconciliation task';
                    newTask.Priority = 'Normal';
                    newTask.ActivityDate = Date.today();
                    newTasks.add(newTask);
                }
            }      
            if(t.Escalate_Reconciliation_Reminder__c && !oldValue.Escalate_Reconciliation_Reminder__c)  
            {
                for(User u : SeniorAccountantUsers)
                {
                    Task newTask = new Task();
                    newTask.Status = 'Not Started';
                    newTask.WhatId = t.WhatId;
                    newTask.Subject = 'Escalation for Invoice failed reconciliation task';
                    newTask.Priority = 'High';
                    newTask.ActivityDate = Date.today();
                    newTasks.add(newTask);
                }
            }           
        }

        if (clients.size() > 0)
        {
            update clients.values();
        }
        if (newRequests.size() > 0)
        {
            update newRequests;
        }
        if(newTasks.size() > 0)
        {
            insert newTasks;
        }
    }
}