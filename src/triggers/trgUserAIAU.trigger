trigger trgUserAIAU on User (after insert, after update)  
{ 
    /*ID userRtId = User_Audit__c.sObjectType.getDescribe().getRecordTypeInfosByName().get('User').getRecordTypeId();
    List<User_Audit__c> audits = new List<User_Audit__c>();
    UserAuditField__c auditFields = UserAuditField__c.getOrgDefaults();
    Map<String, Schema.SObjectField> userFields = User.sObjectType.getDescribe().fields.getMap();
    List<String> fields = auditFields.Field_Names__c == null ? new List<String>() : auditFields.Field_Names__c.split(';');
    for(User newValue : Trigger.new)
    {
        User oldValue;
        if(fields != null)
        {
            for(String field : fields)
            {
                if(Trigger.isUpdate)
                {
                    oldValue = Trigger.oldMap.get(newValue.ID);
                }
                if(Trigger.isInsert || newValue.get(field) != oldValue.get(field))
                {
                    User_Audit__c audit = new User_Audit__c(User__c = newValue.ID, RecordTypeId = userRtId);
                    audit.Field__c = userFields.get(field).getDescribe().getLabel();
                    audit.Changed_By__c = UserInfo.getUserId();
                    audit.Updated__c = DateTime.now();
                    audit.Updated_On__c = DateTime.now().formatLong();
                    audit.New_Value__c = getFieldValue(newValue, field);
                    audit.Old_Value__c = Trigger.isInsert ? '' : getFieldValue(oldValue, field);
                    audits.add(audit);
                }
            }
        }
    }
    if(!audits.isEmpty())
    {
        System.enqueueJob(new UserAuditQueuable(audits));
    }

    private String getFieldValue(User record, String fieldName)
    {
        if(record.get(fieldName) == null)
        {
            return '';
        }
        return String.valueOf(record.get(fieldName));
    }*/
}