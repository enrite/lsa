trigger trgRequestAfterIU on Request__c (after insert, after update) 
{        
    Map<Id, Task> existingTasks = new Map<Id, Task>();
    List<Task> newTasks = new List<Task>();
    List<Task> updateTasks = new List<Task>();
    Id taskRecordTypeId = Schema.Sobjecttype.Task.getRecordTypeInfosByName().get('Participant Portal').RecordTypeId; 

    // find the existing tasks
    for (Task t : [SELECT Id,
                          WhatId,
                          Status,
                          Description
                   FROM Task
                   WHERE WhatId IN :Trigger.NewMap.keySet()])
    {
        existingTasks.put(t.WhatId, t);    
    }                   
  
    // find the client records
    Set<Id> clientIds = new Set<Id>();
    for (Request__c r : Trigger.New)
    {
        clientIds.add(r.Client__c);    
    }
    
    Map<Id, Referred_Client__c> clients = new Map<Id, Referred_Client__c> ([SELECT Id,
                                                                                   Name,
                                                                                   Allocated_to__c
                                                                            FROM Referred_Client__c
                                                                            WHERE Id IN :clientIds]);
            
    // loop through the requests and create or update tasks as necessary
    for (Request__c r : Trigger.New)
    {
        if (existingTasks.containsKey(r.Id))
        {
            Task t = existingTasks.get(r.Id);   
            
            if (t.Status != r.Status__c
                || t.Description != r.Response__c)
            {
                t.Status = r.Status__c;
                t.Description = r.Response__c; 
                updateTasks.add(t);                                
            }
        }
        else if (Trigger.isInsert)
        {
            Task t = new Task();
            t.WhatId = r.Id;
            t.OwnerId = clients.get(r.Client__c).Allocated_to__c;
            t.ActivityDate = r.CreatedDate.date().addDays(7);
            t.Status = r.Status__c;      
            t.RecordTypeId = taskRecordTypeId;
            t.Participant_Request_Details__c = (r.Request_Details__c.length()>255)?r.Request_Details__c.substring(0,252)+'...':r.Request_Details__c;      
            t.Subject = 'My Request from Participant Portal for ' + clients.get(r.Client__c).Name + ' Received from ' + UserInfo.getName();
            newTasks.add(t);
        }
        
        // if the status has become completed then submit to the approval process to lock record 
        if ((r.Status__c == 'Completed' || r.Status__c == 'Cancelled')
            && (Trigger.isInsert || r.Status__c != Trigger.OldMap.get(r.Id).Status__c))
        {
            Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();            
            req.setObjectId(r.id);                                              
            req.setProcessDefinitionNameOrId('Lock_Request');                                 
            Approval.process(req); 
        }
    }
  
    if (newTasks.size() > 0)
    {
        Database.DMLOptions dmlo = new Database.DMLOptions(); 
        dmlo.EmailHeader.triggerUserEmail = true; 
        database.insert(newTasks, dmlo);   
    }
    
    if (updateTasks.size() > 0)
    {
        update updateTasks;
    }
}