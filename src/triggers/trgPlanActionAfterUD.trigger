trigger trgPlanActionAfterUD on Plan_Action__c (after insert, after update, after delete) 
{
    if(TriggerBypass.PlanAction)
    {
        return;
    }

//    List<User> TaskUsers
//    {
//        get
//        {
//            if(TaskUsers == null)
//            {
//                TaskUsers = UserUtilities.getUsersInRoles(new List<String>{'Manager ASD','Lead Service Planner'});
//            }
//            return TaskUsers;
//        }
//        set;
//    }

    Set<Id> changedPlanActions = new Set<Id>();    
    Map<Id, Plan_Approval__c> planApprovals = new Map<Id, Plan_Approval__c>();

    List<Task> conflictTasks = new List<Task>();
    Set<ID> preApprovedPlanActionIds = new Set<ID>();
    // update any service orders and plan approvals that are attached to the plan actions for this plan
    if(Trigger.isUpdate || Trigger.isInsert)
    {
        Map<String, Id> recordTypesByDevName = Utils.GetRecordTypeIdsByDeveloperName(Plan_Action__c.SObjectType, true);
        Map<Id, Plan_Action__c> clients = new Map<Id, Plan_Action__c>([SELECT   ID,
                                                                                Client__r.Name
                                                                        FROM    Plan_Action__c
                                                                        WHERE   ID IN :Trigger.newMap.keySet()]);
        for (Plan_Action__c p : Trigger.New)
        {
            if(p.RecordTypeId == recordTypesByDevName.get('Preapproved_Discharge_Service') ||
                    p.RecordTypeId == recordTypesByDevName.get('Preapproved_Products_and_Equipment'))
            {
                if(Trigger.isInsert || p != Trigger.oldMap.get(p.ID))
                {
                    preApprovedPlanActionIds.add(p.ID);
                }
            }
            if(Trigger.isUpdate)
            {
                if (p != Trigger.OldMap.get(p.Id))
                {
                    changedPlanActions.add(p.Id);
                
                    // only update the plan approval if the amendment details have changed
                    if(p.Plan_Approval__c != null && p.Amendment_Details__c != Trigger.OldMap.get(p.Id).Amendment_Details__c)
                    {
                        planApprovals.put(p.Plan_Approval__c, new Plan_Approval__c(Id = p.Plan_Approval__c));
                    }
                }
            }
            
            if(p.Service_Planner_Conflict_of_Interest__c && 
                (Trigger.isInsert ||
                p.Service_Planner_Conflict_of_Interest__c != Trigger.OldMap.get(p.Id).Service_Planner_Conflict_of_Interest__c))
            {
                Task task  = new Task(Status='Not Started',
                        Subject = 'Potential Conflict of Interest - ' + clients.get(p.id).Client__r.Name,
                        WhatId=p.Id,
                        OwnerId = p.Allocated_To_Manager_ID__c);
                conflictTasks.add(task);
//                for(User u : TaskUsers)
//                {
//                    Task task  = new Task(Status='Not Started',
//                                            Subject = 'Potential Conflict of Interest - ' + clients.get(p.id).Client__r.Name,
//                                            WhatId=p.Id,
//                                            OwnerId = u.Id);
//                    conflictTasks.add(task);
//                }
            }
        }
    }
    else if (Trigger.isDelete)
    {
        changedPlanActions = Trigger.OldMap.keySet();
        
        for (Plan_Action__c p : Trigger.Old)
        {
            if(p.Plan_Approval__c != null)
            {
                planApprovals.put(p.Plan_Approval__c, new Plan_Approval__c(Id = p.Plan_Approval__c));
            }
        }
    }
//    if(!Trigger.isInsert)
//    {
//    system.debug(changedPlanActions);
//        // find the service order records
//        List<RCR_Contract__c> serviceOrders = new List<RCR_Contract__c>();
//        for (RCR_Contract__c so: [SELECT Id,
//                                         RecordType.Name,
//                                         Plan_Action__c,
//                                         Plan_Action_Approved__c,
//                                         Plan_Action_Estimated_Cost__c,
//                                         Plan_Action_Estimated_Cost_Update__c,
//                                         Plan_Action_Total_Service_Order_Cost__c
//                                  FROM RCR_Contract__c
//                                  WHERE Plan_Action__c IN :changedPlanActions
//                                  AND RecordType.Name IN ('New Request', 'Amendment')])
//        {
//            // put a new random value in the update field to force an update
//            if(so.Plan_Action_Estimated_Cost__c != Trigger.OldMap.get(so.Plan_Action__c).Estimated_Cost__c)
//            {
//                so.Plan_Action_Estimated_Cost_Update__c = String.valueOf(Math.random());
//                serviceOrders.add(so);
//            }
//            if((so.Plan_Action_Total_Service_Order_Cost__c != Trigger.NewMap.get(so.Plan_Action__c).Total_Service_Order_Cost__c) ||
//                (so.Plan_Action_Approved__c != Trigger.NewMap.get(so.Plan_Action__c).Approved__c))
//            {
//                ServiceOrderFuture.updatePlanActionTotalServiceOrderCost(so.Id,
//                                                      Trigger.NewMap.get(so.Plan_Action__c).Total_Service_Order_Cost__c,
//                                                      Trigger.NewMap.get(so.Plan_Action__c).Approved__c);
//            }
//        }
//
//        update serviceOrders;
//        update planApprovals.values();
//    }
    insert conflictTasks;
    if(!System.isBatch() && !preApprovedPlanActionIds.isEmpty())
    {
        PlanActionFuture.unsetLatestUpdate(preApprovedPlanActionIds);
    }
}