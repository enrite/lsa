trigger trgRequestBeforeI on Request__c (before insert) 
{
    // find the user record
    User u = [SELECT Id,
                     ContactId,
                     UserType,
                     Profile.Name
              FROM User
              WHERE Id = :UserInfo.getUserId()];

    // only continue if the current user is a portal user
    if (u.UserType == 'CspLitePortal' && u.Profile.Name != 'LSA SP Community')
    {
        // find the portal client
        for (Referred_Client__c c : [SELECT Id    
                                     FROM Referred_Client__c                                        
                                     WHERE Participant_Portal_Contact__c = :u.ContactId])
        {
            for (Request__c r : Trigger.New)
            {
                r.Client__c = c.Id;
            }   
        }                                                 
    }
}