trigger trgRCRGroupAgreement_AU on RCR_Group_Agreement__c (before update) 
{
    AggregateResult[] results = [SELECT SUM(Total_Service_Order_Cost__c) total,
                                        SUM(Orig_Cost__c) origCost,
                                        Status__c,   
                                        RCR_Group_Agreement__c,
                                        RecordType.Name  rtName
                                FROM    RCR_Contract__c
                                WHERE   RCR_Group_Agreement__c IN :Trigger.new
                                GROUP BY Status__c,
                                        RCR_Group_Agreement__c,
                                        RecordType.Name];
    for(RCR_Group_Agreement__c agr : Trigger.new)
    {
        decimal soAmount = 0.0;
        decimal reqAmount = 0.0;
        for(AggregateResult result : results)
        {
            if((ID)result.get('RCR_Group_Agreement__c') == agr.ID)
            {
                String status = (String)result.get('Status__c');
                String rtName = (String)result.get('rtName');
                decimal total = (decimal)result.get('total');
                if(rtName == APSRecordTypes.RCRServiceOrder_ServiceOrder ||
                        rtName == APSRecordTypes.RCRServiceOrder_ServiceOrderCBMS)
                {
                    soAmount += total;
                }
                else if(status != APS_PicklistValues.RCRContract_Status_Approved &&
                            status != APS_PicklistValues.RCRContract_Status_Rejected)
                {
                    if(rtName == APSRecordTypes.RCRServiceOrder_Amendment)
                    {
                        decimal origCost = (decimal)result.get('origCost');
                        reqAmount += total - (origCost == null ? total : origCost);
                    }
                    else
                    {
                        reqAmount += total;
                    }
                }
            }
        }
        agr.Total_Value_of_Service_Orders__c = soAmount;
        agr.Unapproved_New_Requests__c = reqAmount;
    }
}