trigger trgTask_BIBU on Task(before insert, before update) 
{
	for(Task t : Trigger.new)
	{      
        if(t.WhatId != null)  
        {
            if(t.WhatId.getSobjectType() == Schema.Adhoc_Payment__c.SObjectType) 
            {
                t.Adhoc_Payment__c = t.WhatId;
            }
            else if(t.WhatId.getSobjectType() == Schema.RCR_Invoice__c.SObjectType)
            {
                t.RCR_Activity_Statement__c = t.WhatId;
            }
        }
		if(Trigger.isInsert)
		{
			if(String.isNotBlank(t.Assigned_To_Group__c))
			{
				t.OwnerId = Test.isRunningTest() ? UserInfo.getUserId() : Default_Task_User__c.getInstance().User_Id__c;
                if(t.WhatID == null)
                {
                    t.WhatID = Test.isRunningTest() ? null : Default_Task_User__c.getInstance().Related_To_Account_ID__c;
                }
			}
		}
	}
}