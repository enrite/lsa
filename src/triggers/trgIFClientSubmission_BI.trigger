trigger trgIFClientSubmission_BI on IF_Client_Submission__c (before insert) 
{
    Set<String> firstNames = new Set<String>();
    Set<String> lastNames = new Set<String>();
    for(IF_Client_Submission__c cs : Trigger.new)
    {
        firstNames.add(cs.First_Name__c);
        lastNames.add(cs.Last_Name__c);
    }
    AggregateResult[] results = [SELECT COUNT(ID)  cnt,
                                        First_Name__c,
                                        Last_Name__c
                                FROM    IF_Client_Submission__c
                                WHERE   First_Name__c IN :firstNames AND
                                        Last_Name__c IN :lastNames AND
                                        DAY_ONLY(Response_Date__c) = :Date.today()
                                GROUP BY First_Name__c,
                                        Last_Name__c
                                HAVING  COUNT(ID) > 2];
                                            
    for(IF_Client_Submission__c cs : Trigger.new)
    {
        for(AggregateResult result : results)
        {
            if((String)result.get('First_Name__c') == cs.First_Name__c &&
                    (String)result.get('Last_Name__c') == cs.Last_Name__c)
            {
                cs.addError('A maximum of 3 submissions per person is allowed per day.');
            }
        }
    }                                   
                                
}