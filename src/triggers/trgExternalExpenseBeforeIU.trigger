trigger trgExternalExpenseBeforeIU on External_Expense__c (before insert, before update) 
{
    // find the unique keys
    Set<String> keys = new Set<String>();
    for (External_Expense__c e : Trigger.New)
    {
        if(!e.File_Upload__c)
        {
            keys.add(e.Unique_Key__c);    
        }
    }
    
    // find the other external expenses for those keys
    for (External_Expense__c other : [SELECT Id,
                                                 Unique_Key__c
                                          FROM External_Expense__c 
                                          WHERE Unique_Key__c IN :keys])
    {
        for (External_Expense__c e : Trigger.New)
        {
            if(other.Unique_Key__c == e.Unique_Key__c
                && other.Id != e.Id)
            {
                e.addError('An RCR External Expense record already exists for this Account, Month and Year.');
            }    
        }
    }                                              
}