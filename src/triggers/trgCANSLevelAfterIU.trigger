trigger trgCANSLevelAfterIU on CANS_Level__c (after insert, after update) 
{
    // find the client Ids
    Set<Id> clientIds = new Set<Id>();
    for (CANS_Level__c cl : Trigger.New)
    {
        clientIds.add(cl.Client__c);    
    }

    // get the client records, sub querying the latest child CANS level
    List<Referred_Client__c> clients = [SELECT Id,
                                               Current_CANS_Level__c,
                                               (SELECT Id,
                                                       CANS_Level__c
                                                FROM CANS_Levels__r
                                                ORDER BY Date_of_Assessment__c DESC
                                                LIMIT 1)
                                        FROM Referred_Client__c
                                        WHERE Id IN :clientIds];
                                        
    // loop through the clients and set their current CANS level to the latest one
    for (Referred_Client__c c : clients)
    {
        for (CANS_Level__c cl : c.CANS_Levels__r)
        {
            c.Current_CANS_Level__c = cl.CANS_Level__c;
        }
    }                                       
    
    update clients;
}