trigger trgExternalExpenseAfterU on External_Expense__c (after update) 
{
    // find the list of external expenses that have just become approved
    List<Id> approvedIds = new List<Id>();
    for (External_Expense__c e : Trigger.New)
    {
        if (e.Approved__c
            && !Trigger.OldMap.get(e.Id).Approved__c)
        {           
            approvedIds.add(e.Id);           
        }            
    }

    if (approvedIds.size() > 0)
    {
        List<External_Expense_Line_Item__c> items = [SELECT Id,
                                                            Approved__c
                                                     FROM External_Expense_Line_Item__c
                                                     WHERE External_Expense__c IN :approvedIds];
                                                     
        for (External_Expense_Line_Item__c i : items)
        {
            i.Approved__c = true;
        }                        
        
        update items;                                                                                  
    }
}