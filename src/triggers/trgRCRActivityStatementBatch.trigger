trigger trgRCRActivityStatementBatch on RCR_Invoice_Batch__c (after update, before update)
{
    // upon approval:
    if (Trigger.New.size() == 1)
    {   
        if (Trigger.New[0].Approved_by_Approval_Process__c
            && !Trigger.Old[0].Approved_by_Approval_Process__c)
        {
            RCR_Invoice_Batch__c invBatch = Trigger.New[0];
            if(Trigger.isBefore)
            {
                invBatch.Approved_By__c = UserInfo.getUserId();
                invBatch.Approved_Date__c = Datetime.now();
            }
            if(Trigger.isAfter)
            {
                RCRInvoiceBatchController ctr = new RCRInvoiceBatchController(Trigger.New[0].Id);
                Boolean isLocked = Approval.isLocked(Trigger.New[0].Id);
                if(isLocked)
                {
                    Approval.unlock(Trigger.New[0].Id);
                }
                ctr.processPayment();
                RCRInvoiceWebService.updateBatch(Trigger.New[0].Id, 1, Trigger.New[0].Name + '.txt', null);
                if(isLocked)
                {
                    Approval.lock(Trigger.New[0].Id);
                }
            }
        }
    }
}