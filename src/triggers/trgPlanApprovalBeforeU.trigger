trigger trgPlanApprovalBeforeU on Plan_Approval__c (before update)
{
//    // find the plan approval record types
//    Map<String, RecordType> recordTypes = new Map<String, RecordType>();
//    for (RecordType rt : [SELECT    Id,
//                                    Name,
//                                    DeveloperName
//                            FROM RecordType
//                            WHERE sObjectType = 'Plan_Approval__c'])
//    {
//        recordTypes.put(rt.DeveloperName, rt);
//    }
//
//    String userRole = UserUtilities.getUser(UserInfo.getUserId()).UserRole.Name;
//    Plan_Approval_Delegation__c approvalDelegation = null;
//    for(Plan_Approval_Delegation__c pad : [SELECT   Id,
//                                                    Name,
//                                                    Limit__c,
//                                                    Role__c
//                                            FROM    Plan_Approval_Delegation__c
//                                            WHERE   Role__c = :userRole AND
//                                                    Current__c = true
//                                            LIMIT 1])
//    {
//        approvalDelegation = pad;
//    }
//
//    Map<Id, Decimal> planCostByApprovalID = new Map<Id, Decimal>();
    // set the amendment details field if any are found on the child plan actions
//    for (Plan_Approval__c pa : [SELECT Id,
//                                        (SELECT Id,
//                                                Service_Planner_Conflict_of_Interest__c,
//                                                MSD_Conflict_Process_Complete__c,
//                                                Name,
//                                                LastModifiedDate,
//                                                Amendment_Details__c
//                                        FROM Plan_Actions__r),
//                                        Estimated_Plan_Cost__c,
//                                        RecordTypeId,
//                                        Service_Planner_Complete__c
//                                FROM    Plan_Approval__c
//                                WHERE   Id IN :Trigger.NewMap.keySet()])
//    {
//        planCostByApprovalID.put(pa.Id, pa.Estimated_Plan_Cost__c);
//        Trigger.NewMap.get(pa.Id).Amendment_Details__c = '';
//
//        Boolean hasConflict = false;
//        for (Plan_Action__c act : pa.Plan_Actions__r)
//        {
//            if (act.Amendment_Details__c != null)
//            {
//                Trigger.NewMap.get(pa.Id).Amendment_Details__c += act.Amendment_Details__c + '\n\n';
//            }
//
//            if (pa.RecordTypeId == recordTypes.get('Service_Planner_Recommendation').Id
//                    && Trigger.newMap.get(pa.Id).Service_Planner_Complete__c
//                    && !Trigger.oldMap.get(pa.Id).Service_Planner_Complete__c
//                    && !hasConflict)
//            {
//                if(act.Service_Planner_Conflict_of_Interest__c && !act.MSD_Conflict_Process_Complete__c)
//                {
//                    Trigger.NewMap.get(pa.Id).addError('The Plan cannot be recommended until all Potential Conflicts of Interest have been completed.');
//                    hasConflict = true;
//                }
//            }
//        }
//    }
//
//    Set<Id> approvedPlanIds = new Set<Id>();
//    Set<Id> unapprovedPlanIds = new Set<Id>();
//    Set<Id> approvedPlanApprovalIds = new Set<Id>();
//    Set<Id> delegateApprovedPlanApprovalIds = new Set<Id>();
    // take actions based on the new field values
//    for (Plan_Approval__c pa : Trigger.New)
//    {
//        if(Trigger.oldMap.get(pa.ID).Service_Planner_Complete__c != pa.Service_Planner_Complete__c &&
//                pa.Service_Planner_Complete__c &&
//                string.isEmpty(pa.Plan_Update_Required__c) )
//        {
//            pa.AddError('You must indicate if a Plan Update is Required.');
//        }
//        if(approvalDelegation != null && planCostByApprovalID.get(pa.ID) <= approvalDelegation.Limit__c)
//        {
//            delegatedApproval(pa, approvalDelegation, userRole);
//        }
//        else
//        {
//            undelegatedApproval(pa);
//        }
//    }
    // update any plans that require it
//    List<Plan__c> plansToUpdate = new List<Plan__c>();
//    for(Id planID : approvedPlanIds)
//    {
//        plansToUpdate.add(new Plan__c(Id = planID, Plan_Status__c = 'Approved'));
//    }
//    for(Id planID : unapprovedPlanIds)
//    {
//        plansToUpdate.add(new Plan__c(Id = planID, Plan_Status__c = 'Not Approved'));
//    }
//    update plansToUpdate;
//    // update any plan actions linked to approved plan approvals
//    List<Plan_Action__c> planActions = [SELECT Id
//                                        FROM    Plan_Action__c
//                                        WHERE   Plan_Approval__c IN :approvedPlanApprovalIds AND
//                                                Plan_Approval__c != null AND
//                                                Approved__c = false AND
//                                                Ready_to_Approve__c = true];
//    for (Plan_Action__c pa : planActions)
//    {
//        pa.Approved__c = true;
//    }
//    //TriggerBypass.PlanAction = true;
//    update planActions;

//    approvedPlanApprovalIds.addAll(delegateApprovedPlanApprovalIds);
//    if(!approvedPlanApprovalIds.isEmpty())
//    {
//        PlanActionFuture.createApprovalAudits(approvedPlanApprovalIds);
//    }

//    private void delegatedApproval(Plan_Approval__c pa, Plan_Approval_Delegation__c pad, String currentUserRole)
//    {
//        // service planner complete
//        if(pa.RecordTypeId == recordTypes.get('Service_Planner_Recommendation').Id)
//        {
//            if(pa.Service_Planner_Complete__c)
//            {
//                // only do final approval if user is a service planner, if other higher role is stepping in continue as normal
//                if(currentUserRole == 'Service Planners')
//                {
//                    setDelegatedApprovalFields(pa, 'Service_Planner_Approval', pad);
//                }
//                else
//                {
//                    undelegatedApproval(pa);
//                }
//            }
//        }
//        else if (pa.RecordTypeId == recordTypes.get('Manager_ASD_Recommendation').Id)
//        {
//            if(pa.Manager_ASD_Complete__c == 'Approved')
//            {
//                setDelegatedApprovalFields(pa, 'Manager_Service_Planning_Approval', pad);
//            }
//            else
//            {
//                undelegatedApproval(pa);
//            }
//        }
//        else
//        {
//            undelegatedApproval(pa);
//        }
//    }

//    private void setDelegatedApprovalFields(Plan_Approval__c pa, String recordTypeDevName, Plan_Approval_Delegation__c pad)
//    {
//        approvedPlanApprovalIds.add(pa.ID);
//        approvedPlanIds.add(pa.Participant_Plan__c);
//        pa.RecordTypeId = recordTypes.get(recordTypeDevName).Id;
//        pa.Plan_Approval_Date__c = Date.today();
//        pa.Chief_Executive_Comments__c = 'N/A approved at the ' + (pad.Role__c == 'Manager ASD' ? 'Senior Delegate' : pad.Role__c)  + ' level';
//    }
//
//    private void undelegatedApproval(Plan_Approval__c pa)
//    {
//        // service planner complete
//        if (pa.RecordTypeId == recordTypes.get('Service_Planner_Recommendation').Id)
//        {
//            if(pa.Service_Planner_Complete__c)
//            {
//                pa.RecordTypeId = recordTypes.get('Manager_ASD_Recommendation').Id;
//                pa.Lead_Service_Planner_Complete__c = null;
//                //pa.Lead_Service_Planner_Recommendation__c = 'This level of approval is not currently being used';
//            }
//        }
//        // lead service planner complete
//        else if (pa.RecordTypeId == recordTypes.get('Lead_Service_Planner_Recommendation').Id)
//        {
//            if (pa.Lead_Service_Planner_Complete__c == 'Referred back to Service Planner')
//            {
//                pa.RecordTypeId = recordTypes.get('Service_Planner_Recommendation').Id;
//                pa.Service_Planner_Complete__c = false;
//            }
//            else if (pa.Lead_Service_Planner_Complete__c == 'Recommended to Manager ASD')
//            {
//                pa.RecordTypeId = recordTypes.get('Manager_ASD_Recommendation').Id;
//                pa.Manager_ASD_Complete__c = null;
//            }
//        }
//        // manager ASD complete
//        else if (pa.RecordTypeId == recordTypes.get('Manager_ASD_Recommendation').Id)
//        {
//            if (pa.Manager_ASD_Complete__c == 'Referred back to Service Planner')
//            {
//                pa.RecordTypeId = recordTypes.get('Service_Planner_Recommendation').Id;
//                pa.Service_Planner_Complete__c = false;
//            }
//            else if (pa.Manager_ASD_Complete__c == 'Referred back to Lead Service Planner')
//            {
//                pa.RecordTypeId = recordTypes.get('Lead_Service_Planner_Recommendation').Id;
//                pa.Lead_Service_Planner_Complete__c = null;
//            }
//            else if (pa.Manager_ASD_Complete__c == 'Recommended to Chief Executive')
//            {
//                pa.RecordTypeId = recordTypes.get('Chief_Executive_Approval').Id;
//                pa.Chief_Executive_Decision__c = null;
//            }
//            else if(pa.Manager_ASD_Complete__c == 'Approved')
//            {
//                pa.addError('Estimated plan cost exceeds your delegation level');
//            }
//        }
//        // chief executive decision
//        else if (pa.RecordTypeId == recordTypes.get('Chief_Executive_Approval').Id)
//        {
//            if (pa.Chief_Executive_Decision__c == 'Referred back to Service Planner')
//            {
//                pa.RecordTypeId = recordTypes.get('Service_Planner_Recommendation').Id;
//                pa.Service_Planner_Complete__c = false;
//            }
//            else if (pa.Chief_Executive_Decision__c == 'Referred back to Lead Service Planner')
//            {
//                pa.RecordTypeId = recordTypes.get('Lead_Service_Planner_Recommendation').Id;
//                pa.Lead_Service_Planner_Complete__c = null;
//            }
//            else if (pa.Chief_Executive_Decision__c == 'Referred back to Senior Delegate')
//            {
//                pa.RecordTypeId = recordTypes.get('Manager_ASD_Recommendation').Id;
//                pa.Manager_ASD_Complete__c = null;
//            }
//            else if (pa.Chief_Executive_Decision__c == 'Not Approved')
//            {
//                // plans.add(new Plan__c(Id = pa.Participant_Plan__c, Plan_Status__c = 'Not Approved'));
//                unapprovedPlanIds.add(pa.Participant_Plan__c);
//            }
//            else if (pa.Chief_Executive_Decision__c == 'Approved' &&
//                    pa.Chief_Executive_Decision__c != Trigger.oldMap.get(pa.ID).Chief_Executive_Decision__c)
//            {
//                approvedPlanIds.add(pa.Participant_Plan__c);
//                approvedPlanApprovalIds.add(pa.Id);
//            }
//        }
//    }
}