<apex:page standardcontroller="Referred_Client__c" extensions="RCRClientSummary" sidebar="true" showHeader="true" standardStylesheets="true">

<apex:variable value="{!Referred_Client__c}" var="Client"/>
<apex:includeScript value="{!URLFOR($Resource.jQuery)}" />   
<style>
    .hide {
        display:none;
    }
    .show {
        width:100%;
        border-collapse:collapse;
    }
    .plus {
        background-image:URL({!URLFOR($Resource.RCRImages, 'img/plus.png')});
        background-repeat:no-repeat;
    }
    .minus {
        background-image:URL({!URLFOR($Resource.RCRImages, 'img/minus.png')});
        background-repeat:no-repeat;
    }
    .myTableInner {
        border:0px solid #e0e3e5 !important;
        border-collapse:collapse;
        font-family: Arial,Helvetica,sans-serif
        background-color: white;
        font-size: .9em important!;  
        }
    .myTableInner>tbody>tr {
        
        border-left: 0px;
        border-right: 0px;
        border-left-width:0px;
        border-right-width: 0px;
    }
    .myTableInner>tbody>tr>th {
        background-color:#f2f3f3;
        font-size: .9em;
    }
    .myTableInner th.right {
        text-align:right;
    }
    .myTableInner th.center {
        text-align:center;
    }
    .myTableInner td.right, .myTable>tbody>tr>td.right {
        text-align:right;
    }
    .myTableInner td.center {
        text-align:center;
    }
    .hdrTable td.right
    {
        padding-top:5px;
    }
    .tableHeaderColor {
        background-color:#E3F3FF !important;
        height:30px !important;
    }
    .evenRowColour {
        background-color:white;
    }
    .myTableInner td:last-child {
        border-right: solid 1px #ececec !important;
    }
    
    .myTableInner td:first-child {
        border-left: solid 1px #ececec !important;
    }
    
    .myTableInner td {
        border-left: 0px !important;
        border-right: 0px !important;
    }
    
    .secondaryPalette {
        background-color:transparent !important;
    }
    
    .bPageBlock {
        border: solid 0px transparent !important;
    }

    td.data2Col
    {
        border:0px solid transparent !important;
    }
</style>
<script>
    function toggleItems(tableRow, size)
        {
            if(size > 0)
            { 
                if(tableRow)
                { 
                    var td = $(tableRow).find('td:first');
                    td.attr('class', td.attr('class') == 'plus'?'minus':'plus');
                                       
                    $(tableRow).next().toggle(); 
                }
            }
        }
</script>
<apex:form onkeypress="return onEnter(event);" >
    <apex:stylesheet value="{!URLFOR($Resource.A2HCStyle)}"/>
    
    <div style="padding-top: 15px;">
        <c:RCRPageHeader recordType="Client Summary Requests" recordName="{!Client.Name}" contextName="Service Summary" contextLink="apex/RCRClientSummary?id={!Client.id}" displayContext="true" />
    </div> 

    <apex:pageBlock >
        <apex:outputPanel id="requestsOrders" >
        <apex:pageBlockSection title="Open Requests" columns="1">   
            <apex:outputPanel >        
             <table class="myTableInner list" id="outerTable" width="100%">
                <tr >
                    <th width="3%">&nbsp;</th>
                    <th width="10%">Service Order Number</th>
                    <!--<th width="16%">id</th>-->
                    <th width="10%" class="left">Record Type</th>
                    <th width="15%" class="left">Service Provider</th>
                    <th width="15%" class="left">Funding Source</th>                    
                    <th width="10%" class="left">Start Date</th>
                    <th width="10%" class="left">Actual End Date</th>                  
                    <th width="12%" class="left">Total Cost</th>                   
                    <th width="6%" class="left">Plan</th>
                    <th width="6%" class="left">Plan Action</th>
                    <th width="5%" class="left">Plan Approved</th>
                </tr>
            
             <apex:repeat value="{!CurrentSORequests}" var="all" >                
                <tr onclick="toggleItems(this, {!all.RCR_Contract_Scheduled_Services__r.size});">
                    <td class="{!IF(all.RCR_Contract_Scheduled_Services__r.size > 0, 'plus', '')}"></td>
                    <td><apex:outputLink value="/{!all.id}">                
                          {!all.Name}</apex:outputLink></td>
                    <!--<td  ><apex:outputField value="{!all.id}" /></td>-->
                    <td  ><apex:outputField value="{!all.RecordType.Name}" /></td>
                    <td  ><apex:outputField value="{!all.Account__c}" /></td>
                    <td  ><apex:outputField value="{!all.Funding_Source__c}" /></td>
                    <td  ><apex:outputField value="{!all.Start_Date__c}" /></td>
                    <td  ><apex:outputField value="{!all.End_Date__c}" /></td>                    
                    <td  ><apex:outputField value="{!all.Total_Service_Order_Cost__c}" /></td>                    
                    <td  ><apex:outputField value="{!all.Plan_Action__r.Participant_Plan__c}" /></td>
                    <td  ><apex:outputField value="{!all.Plan_Action__c}" /></td>
                    <td  ><apex:outputField value="{!all.Plan_Action__r.Approved__c}" /></td>
                </tr> 
                <tr class="hide">
                    <td colspan="11">
                        <apex:pageBlockTable value="{!all.RCR_Contract_Scheduled_Services__r}" var="css" >                            
                            <apex:column headerValue="Related Items">
                                <apex:outputLink value="/{!css.id}" >{!css.Name}</apex:outputLink>
                            </apex:column>                                                                                    
                            <apex:column value="{!css.Service_Code__c}" headerValue="Service Code" headerClass="leftAlign" />
                            <apex:column value="{!css.Service_Description__c}" headerValue="Service Description" headerClass="leftAlign" />
                            <apex:column value="{!css.Service_Types__c}" headerValue="GL Code" headerClass="leftAlign" />
                            <apex:column value="{!css.Start_Date__c}" headerValue="Start Date" headerClass="leftAlign" />
                            <apex:column value="{!css.End_Date__c}" headerValue="Actual End Date" headerClass="leftAlign" />
                            <apex:column value="{!css.Total_Cost__c}" headerValue="Total Cost" headerClass="leftAlign" />
                            <apex:column value="{!css.Total_Submitted__c}" headerValue="Total Submitted" headerClass="leftAlign" />
                            <apex:column value="{!css.Remaining__c}" headerValue="Remaining" headerClass="leftAlign" />
                        </apex:pageBlockTable>
                    </td>
                 </tr>
              </apex:repeat>
             </table>                     
            </apex:outputPanel>
        <!--
        <apex:outputPanel id="requestsOrders">  
        <apex:pageBlockSection title="Requests" columns="1">
        <apex:pageBlockTable value="{!CurrentSORequests}" var="allSO" >                 
              <apex:column headerValue="Service Order Number" headerClass="leftAlign">
                  <apex:repeat value="{!allSO}"  var="all" >
                  <apex:outputLink value="/{!all.id}">                
                      {!all.Name}
                  </apex:outputLink>
                  </apex:repeat>
              </apex:column>
              <apex:column headerValue="Record Type" headerClass="leftAlign">
                  <apex:repeat value="{!allSO}"  var="all" >
                      {!all.RecordType.Name}
                  </apex:repeat>
              </apex:column>
              <apex:column headerValue="Service Provider" headerClass="leftAlign">
                  <apex:repeat value="{!allSO}"  var="all" >
                  <apex:outputLink value="/{!all.Account__c}">  
                      {!all.Account__r.Name}
                  </apex:outputLink>
                  </apex:repeat>
              </apex:column>
              <apex:column headerValue="Funding Source" headerClass="leftAlign">
                  <apex:repeat value="{!allSO}"  var="all" >
                      {!all.Funding_Source__c}
                  </apex:repeat>
              </apex:column>
              <apex:column headerValue="Start Date" headerClass="leftAlign">
                  <apex:repeat value="{!allSO}"  var="all" >
                    <apex:outputText value="{0, date, dd/MM/yyyy}"> 
                        <apex:param value="{!all.Start_Date__c}" />
                    </apex:outputText>
                  </apex:repeat>
              </apex:column>
              <apex:column headerValue="Actual End Date" headerClass="leftAlign">
                  <apex:repeat value="{!allSO}"  var="all" >
                  <apex:outputText value="{0, date, dd/MM/yyyy}"> 
                        <apex:param value="{!all.End_Date__c}" />
                    </apex:outputText>
                  </apex:repeat>
              </apex:column>
              <apex:column headerValue="Total Service Order Cost" headerClass="leftAlign">
                  <apex:repeat value="{!allSO}"  var="all" >
                      {!all.Total_Service_Order_Cost__c}
                  </apex:repeat>
              </apex:column>
        </apex:pageBlockTable>-->
        <c:GridPaging2 grdController="{!GridPagerController}" rerenderPanels="requestsOrders"  /> 
        </apex:pageBlockSection>
        </apex:outputPanel>
    </apex:pageBlock>
    </apex:form>
</apex:page>