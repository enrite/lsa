<apex:page standardcontroller="Participant_Contact__c" renderAs="pdf" applyBodyTag="false" applyHtmlTag="false" showHeader="false">   
    <apex:variable value="{!Participant_Contact__c}" var="c" />
    <apex:composition template="LetterTemplatePDF">    
        <apex:define name="body">                 
            <span style="font-size: 12pt;">                        
                {!IF(ISBLANK(c.Title__c), '', c.Title__c + ' ')}{!c.First_Name__c} {!c.Surname__c}<br />
                {!c.Street__c}<br />
                {!c.Suburb__c} {!c.State__c} {!c.Postcode__c}<br />
                <br />
            </span>                     
            Dear {!IF(ISBLANK(c.Title__c), c.First_Name__c, c.Title__c + ' ' + c.Surname__c)}<br />
            <br />                        
            <apex:outputPanel layout="block" style="text-align: justify;" rendered="{!c.Relationship_to_Participant__c == 'Parent'}">I am writing to you to confirm that {!CASE(c.Participant__r.Sex__c, 'Male', 'your son ', 'Female', 'your daughter ', '')}{!c.Participant__r.Name} has been accepted as an interim participant to the Lifetime Support Scheme (LSS).</apex:outputPanel>
            <apex:outputPanel layout="block" style="text-align: justify;" rendered="{!c.Relationship_to_Participant__c != 'Parent'}">I am writing to you as the legal guardian for {!c.Participant__r.Name} to confirm {!CASE(c.Participant__r.Sex__c, 'Male', 'he', 'Female', 'she', c.Participant__r.Given_Name__c)} has been accepted as an interim participant to the Lifetime Support Scheme (LSS).</apex:outputPanel>
            <br />
            <div style="text-align: justify;">The assessment of {!c.Participant__r.Given_Name__c}'<!--'-->{!IF(RIGHT(c.Participant__r.Given_Name__c, 1) != 's', 's', '')} application has determined that {!CASE(c.Participant__r.Sex__c, 'Male', 'his', 'Female', 'her', c.Participant__r.Given_Name__c + "'" + IF(RIGHT(c.Participant__r.Given_Name__c, 1) == 's', '', 's'))} motor vehicle injury meets the eligibility criteria for the Scheme.</div>
            <br />
            <div style="text-align: justify;">This means that the Lifetime Support Authority (LSA) will pay for {!c.Participant__r.Given_Name__c}'<!--'-->{!IF(RIGHT(c.Participant__r.Given_Name__c, 1) != 's', 's', '')} necessary and reasonable treatment, care and support whilst {!IF(c.Participant__r.Sex__c == 'Male', 'he', 'she')} is a participant in the Scheme.</div>
            <br />
            <div style="text-align: justify;">{!c.Participant__r.Allocated_to__r.Name} is the service planner for {!c.Participant__r.Given_Name__c} and will contact you soon to explain the next steps, and to plan for {!c.Participant__r.Given_Name__c}'<!--'-->{!IF(RIGHT(c.Participant__r.Given_Name__c, 1) != 's', 's', '')} treatment, care and support.</div>
            <br />
            <div style="text-align: justify;">In the meantime, you can contact {!c.Participant__r.Allocated_to__r.Name} with any questions on 1300 880 849 or via email at {!c.Participant__r.Allocated_to__r.Email}.</div>
            <br />
            <div style="text-align: justify;">The information sheets included with this letter explain how the Scheme works and you can also find information on our website www.lifetimesupport.sa.gov.au.</div>
            <br />
            <div style="text-align: justify;">I'<!--'-->d like to explain what it means to be an interim participant in the Scheme. The injuries that meet the criteria for acceptance into the Scheme can take time to stabilise. It is sometimes not clear what the impact of the injuries will be in the long term.</div>
            <br />
            <div style="text-align: justify;">We initially accept people into the Scheme as interim participants to that treatment, care and support can begin as soon as possible.</div>
            <br />
            <div style="text-align: justify;">{!c.Participant__r.Given_Name__c} can remain an interim participant for up to three years if {!CASE(c.Participant__r.Sex__c, 'Male', 'his', 'Female', 'her', c.Participant__r.Given_Name__c + "'" + IF(RIGHT(c.Participant__r.Given_Name__c, 1) == 's', '', 's'))} motor vehicle injury continues to meet the eligibility criteria. During this time, the LSA will also decide whether {!CASE(c.Participant__r.Sex__c, 'Male', 'he', 'Female', 'she', c.Participant__r.Given_Name__c)} meets the criteria for lifetime participation in the Scheme.</div>
            <br />
            <div style="text-align: justify; page-break-after: always;">You can ask us to make a decision about {!c.Participant__r.Given_Name__c}'<!--'-->{!IF(RIGHT(c.Participant__r.Given_Name__c, 1) != 's', 's', '')} eligibility for lifetime participation at any time. If there is certainty about the long term effect of {!CASE(c.Participant__r.Sex__c, 'Male', 'his', 'Female', 'her', c.Participant__r.Given_Name__c + "'" + IF(RIGHT(c.Participant__r.Given_Name__c, 1) == 's', '', 's'))} injury, we can make a decision about making {!CASE(c.Participant__r.Sex__c, 'Male', 'him', 'Female', 'her', c.Participant__r.Given_Name__c)} a lifetime participant.</div>
            <br />
            <div style="text-align: justify;">Please note that {!c.Participant__r.Given_Name__c} has been accepted as an interim participant into the Scheme from {!DAY(c.Participant__r.Status_Date__c)}/{!MONTH(c.Participant__r.Status_Date__c)}/{!YEAR(c.Participant__r.Status_Date__c)} under section 26(3) of the <span style="font-family: Sans-Serif;"><i>Motor Vehicle Accidents (Lifetime Support Scheme) Act 2013</i></span>.</div>
            <br />            
            Yours sincerely<br />
            <c:CEOSignature /><br />
            Tamara Tomuc<br />
            <span style="font-family: Sans-Serif; font-weight: bold;">
            Chief Executive<br />
            LIFETIME SUPPORT AUTHORITY<br />
            </span>   
            <br />        
            {!DAY(TODAY())}/{!MONTH(TODAY())}/{!YEAR(TODAY())}          
        </apex:define>                                     
    </apex:composition>                                        
</apex:page>