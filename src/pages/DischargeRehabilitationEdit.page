<apex:page standardController="Form__c" extensions="FormEditExtension" title="{!RecordTypeLabel}">
    <apex:variable value="{!Form__c}" var="f" />
    <apex:sectionHeader subtitle="{!RecordTypeLabel}" title="{!IF(ISBLANK(f.Id), 'New Form', f.Name)}" />    
    <apex:pageMessages />    
    <c:FormStyles />    
    <apex:form >
    <apex:pageBlock title="Form Edit" mode="edit">
        <apex:pageBlockButtons >
            <apex:commandButton value="Save" action="{!SaveOverride}" onclick="processSignature(sig);" />
            <apex:commandButton value="Cancel" action="{!Cancel}" />
        </apex:pageBlockButtons>
                        
        <apex:pageBlockSection title="Personal details of the injured person" />
        <div class="fieldDiv">
            <table class="fields">
                <tr><td class="firstCol" /><td class="secondCol" /></tr>
                <tr>
                    <th>Title</th>
                    <th>First name(s)</th>
                    <th>Surname/family name</th>
                </tr>
                <tr>
                    <td><apex:inputField value="{!f.Title__c}" /></td>
                    <td><apex:inputField value="{!f.First_Name__c}" required="true" /></td>
                    <td><apex:inputField value="{!f.Surname__c}" required="true" /></td>
                </tr>                
            </table>
            <br/>
            <table class="fields">
                <tr><td class="firstCol" /><td class="secondCol" /></tr>
                <tr>
                    <th>Street</th>
                    <th>Suburb</th>
                    <th>State</th>
                    <th>Postcode</th>
                </tr>
                <tr>
                    <td><apex:inputField value="{!f.Street__c}" /></td>
                    <td><apex:inputField value="{!f.Suburb__c}" /></td>
                    <td><apex:inputField value="{!f.State__c}" /></td>
                    <td><apex:inputField value="{!f.Postcode__c}" /></td>
                </tr>                
            </table>
            <br/>               
            <table class="fields">
                <tr><td class="firstCol" /><td class="secondCol" /></tr>
                <tr>
                    <th>Home Phone Number</th>
                    <th>Mobile Phone Number</th>
                    <th>Date of Birth</th>
                </tr>
                <tr>
                    <td><apex:inputField value="{!f.Home_Phone__c}" /></td>
                    <td><apex:inputField value="{!f.Mobile_Phone__c}" /></td>                    
                    <td><apex:inputField value="{!f.Date_of_Birth__c}" required="true" /></td>
                </tr>                
            </table>            
            <br/>
            <table class="fields">
                <tr><td class="firstCol" /><td class="secondCol" /></tr>
                <tr>
                    <th>Date of Accident</th>
                    <th>Is an interpreter required?</th>
                    <th>Interpreter Language</th>                    
                </tr>
                <tr>
                    <td><apex:inputField value="{!f.Date_of_accident__c}" /></td>
                    <td><apex:inputField value="{!f.Is_an_Interpreter_Required__c}" /></td>
                    <td><apex:inputField value="{!f.Interpreter_Language__c}" /></td>                    
                </tr>                
            </table>            
        </div>       
        
        <apex:pageBlockSection title="Personal details of the family member / parent / guardian" />    
        <div class="fieldDiv">
            <table class="fields">
                <tr><td class="firstCol" /><td class="secondCol" /></tr>
                <tr>
                    <th>Title</th>
                    <th>First name(s)</th>
                    <th>Surname/family name</th>
                </tr>
                <tr>
                    <td><apex:inputField value="{!f.Contact_Title__c}" /></td>
                    <td><apex:inputField value="{!f.Contact_First_Name__c}" /></td>
                    <td><apex:inputField value="{!f.Contact_Surname__c}" /></td>
                </tr>                
            </table>
            <br/>    
            <table class="fields">
                <tr><td class="firstCol" /><td class="secondCol" /></tr>
                <tr>
                    <th>Street</th>
                    <th>Suburb</th>
                    <th>State</th>
                    <th>Postcode</th>
                </tr>
                <tr>
                    <td><apex:inputField value="{!f.Contact_Street__c}" /></td>
                    <td><apex:inputField value="{!f.Contact_Suburb__c}" /></td>
                    <td><apex:inputField value="{!f.Contact_State__c}" /></td>
                    <td><apex:inputField value="{!f.Contact_Postcode__c}" /></td>
                </tr>                
            </table>
            <br/>
            <table class="fields">
                <tr><td class="firstCol" /><td class="secondCol" /></tr>
                <tr>
                    <th>Relationship to injured person</th>
                    <th>Email address</th>
                </tr>
                <tr>
                    <td><apex:inputField value="{!f.Relationship_to_Injured_Person__c}" /></td>
                    <td><apex:inputField value="{!f.Contact_Email__c}" /></td>
                </tr>                
            </table>
            <br/>            
            <table class="fields">
                <tr><td class="firstCol" /><td class="secondCol" /></tr>
                <tr>                    
                    <th>Mobile Phone Number</th>
                    <th>Home Phone Number</th>
                    <th>Work Phone Number</th>
                </tr>
                <tr>                    
                    <td><apex:inputField value="{!f.Contact_Mobile_Phone__c}" /></td>
                    <td><apex:inputField value="{!f.Contact_Home_Phone__c}" /></td>
                    <td><apex:inputField value="{!f.Contact_Work_Phone__c}" /></td>
                </tr>                
            </table>
            <br/>
            <table class="fields">
                <tr><td class="firstCol" /><td class="secondCol" /></tr>
                <tr>
                    <th>Is an interpreter required?</th>
                    <th>Interpreter Language</th>                    
                </tr>
                <tr>
                    <td><apex:inputField value="{!f.Is_an_Interpreter_Required_for_Contact__c}" /></td>
                    <td><apex:inputField value="{!f.Contact_Interpreter_Language__c}" /></td>                   
                </tr>                
            </table>            
        </div>

        <apex:pageBlockSection title="Discharge Details" />    
        <div class="fieldDiv">
            <apex:inputField value="{!f.Participant_Guardian_Involved_in_Plan__c}" />&nbsp;Participant/Guardian Involved in Plan
        </div>
        
        <apex:pageBlockSection title="Form Completed By" />    
        <div class="fieldDiv">            
            <table class="fields">
                <tr>
                    <th>Title</th>
                    <th>Full Name</th>
                    <th>Qualification</th>
                    <th>Organisation</th>
                </tr>
                <tr>
                    <td><apex:inputField value="{!f.Completed_by_Title__c}" /></td>
                    <td><apex:inputField value="{!f.Completed_by_Name__c}" /></td>
                    <td><apex:inputField value="{!f.Completed_by_Qualification__c}" /></td>
                    <td><apex:inputField value="{!f.Completed_by_Organisation__c}" /></td>
                </tr>                
            </table>
            <br />
            <table class="fields">
                <tr>
                    <th>Days/Hours Available</th>
                    <th>Phone</th>
                    <th>Email</th>
                </tr>
                <tr>
                    <td><apex:inputField value="{!f.Completed_by_Days_Hours_Available__c}" /></td>
                    <td><apex:inputField value="{!f.Completed_by_Phone__c}" /></td>
                    <td><apex:inputField value="{!f.Completed_by_Email__c}" /></td>
                </tr>                
            </table>            
        </div>
        
        <apex:pageBlockSection title="LSA Service Planner" />    
        <div class="fieldDiv">            
            <table class="fields">
                <tr>
                    <th>LSA Service Planner Contact Name</th>
                </tr>
                <tr>
                    <td><apex:inputField value="{!f.LSA_Service_Planner_Contact_Name__c}" /></td>
                </tr>                
            </table>
            <br />
            <apex:outputPanel layout="block" style="width: 350px; display: none;" id="signedWarning" >
                <apex:pageMessage severity="error" strength="1" summary="The form must be signed or a signed PDF attached" />         
            </apex:outputPanel>    
            <table class="fields">                
                <tr>
                    <th>Signature</th>                    
                    <th>Date</th>                    
                </tr>
                <tr>
                    <td>                        
                        <apex:inputHidden value="{!Signature}" id="sig" />
                        <script>
                            var sig = document.getElementById('{!$Component.sig}');
                        </script>
                        <c:SignatureComponent />
                    </td>
                    <td style="vertical-align: top;"><apex:inputField value="{!f.Signature_Date__c}" /></td>
                </tr>                
            </table>
            <br />                
            <table class="fields">
                <tr>
                    <th>OR</th>                                                                                            
                </tr>                
                <tr>
                    <td><apex:inputField value="{!f.Refer_to_attached_signed_PDF__c}" />&nbsp;Refer to attached signed PDF</td>
                </tr>
            </table> 
            <script>
                if ({!FieldsRequired} && !hasDrawn && !{!f.Refer_to_attached_signed_PDF__c})
                {
                    document.getElementById('{!$Component.signedWarning}').style.display = '';
                }
            </script>                        
        </div>
                        
    </apex:pageBlock>    
    </apex:form>
</apex:page>