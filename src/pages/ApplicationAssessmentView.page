<apex:page standardController="Form_Detail__c" extensions="FormDetailEditExtension" title="{!Form_Detail__c.RecordType.Name}: {!Form_Detail__c.Name}" lightningStylesheets="true">
    <apex:variable value="{!Form_Detail__c}" var="fd" />
    <apex:sectionHeader subtitle="{!fd.RecordType.Name}" title="{!fd.Name}" />
    <apex:pageMessages />
    <script>
        /*
        {!fd.Form_Application_Assessment__r.Client__c}
        {!fd.Form_Application_Assessment__r.Client__r.Age__c}
        {!fd.Form_Application_Assessment__r.Client__r.Allocated_to__c}
        {!fd.RecordType.Name}
        {!fd.RecordType.DeveloperName}
        */
    </script>
    <c:FormStyles /> 
   
    <apex:pageBlock title="Application Assessment" mode="maindetail">  
        <apex:pageBlockButtons rendered="{!fd.RecordType.DeveloperName != 'Assessment_Complete' || $UserRole.Name == 'CE'}">    
            <apex:form >
                <apex:commandButton value="Make Recommendation" onclick="top.location.href = '/{!fd.Id}/e?retURL=/{!fd.Id}'; return false;" rendered="{!$ObjectType.Form_Detail__c.updateable}" />
                <apex:commandButton value="Delete" action="{!DeleteRecord}" onclick="if (!confirm('Are you sure?')) { return false; }" rendered="{!$ObjectType.Form_Detail__c.deletable}" />
             </apex:form>
        </apex:pageBlockButtons>       
        <apex:pageBlockSection showHeader="false" >
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="Participant" />
                <apex:outputPanel > 
                    <a href="/{!fd.Form_Application_Assessment__r.Client__c}" >{!fd.Form_Application_Assessment__r.Client__r.Name} (DOB: <apex:outputField value="{!fd.Form_Application_Assessment__r.Client__r.Date_Of_Birth__c}" />)</a>                                           
                </apex:outputPanel>
            </apex:pageBlockSectionItem>
            <apex:outputField value="{!fd.Form_Application_Assessment__r.Client__r.Allocated_to__c}" />  
            <apex:outputField value="{!fd.Form_Application_Assessment__c}" />         
        </apex:pageBlockSection>
        
        <apex:pageBlockSection title="Assessment Checklist" columns="1">
            <apex:outputField value="{!fd.Accident_reported_to_police__c}" />
            <apex:outputField value="{!fd.Proof_of_identity_attached__c}" />
            <apex:outputField value="{!fd.Consent_Authorisation_Declaration_Comp__c}" label="Consent Authorisation/Declaration Completed?" />
            <apex:outputField value="{!fd.LSA_Medical_Certificate_attached__c}" />
            <apex:outputField value="{!fd.FIM_by_accredited_assessor__c}" />
            <apex:outputField value="{!fd.Name_of_assessor__c}" />
            <apex:outputField value="{!fd.Applicant_is_injured_person_or_insurer__c}" />
            <apex:outputField value="{!fd.Lawful_authority_type__c}" />
            <apex:outputField value="{!fd.Injuries_confirmed_by_specialist__c}" />
            <apex:outputField value="{!fd.Applicant_role_in_accident__c}" />
        </apex:pageBlockSection>
        
        <apex:pageBlockSection title="Injury" >
            <apex:outputField value="{!fd.Bodily_Injury_Sustained__c}" />
        </apex:pageBlockSection>        
        <c:MedicalInformation f="{!LatestMedicalCertificate}" collapsible="true" />
                
        <c:FIMInformation f="{!LatestFIMScoreSheet}" collapsible="true" />
        
        <apex:pageBlockSection title="Accident" columns="1">
            <apex:outputField value="{!fd.Injury_arose_out_of_use_of_motor_vehicle__c}" />    
            <apex:outputField value="{!fd.Accident_occurred_in_South_Australia__c}" />
            <apex:outputField value="{!fd.Form_Application_Assessment__r.Client__r.SAPOL_Vehicle_Collision_Report_Number__c}" />                        
            <apex:outputField value="{!fd.Form_Application_Assessment__r.Client__r.Date_of_Accident__c}" />            
            <apex:outputField value="{!fd.Form_Application_Assessment__r.Client__r.Time_of_Accident__c}" />
            <apex:outputField value="{!fd.Form_Application_Assessment__r.Client__r.Location_of_Accident__c}" />
            <apex:outputField value="{!fd.Form_Application_Assessment__r.Client__r.Postcode_where_accident_occurred__c}" />
            <apex:outputField value="{!fd.Form_Application_Assessment__r.Client__r.Accident_Description__c}" />
        </apex:pageBlockSection>
             
        <c:CTPInformation f="{!LatestApplicationForm}" collapsible="true" />        
        <br />
        
        <c:ClientForms c="{!Client}" />
        
        <apex:pageBlockSection title="Assessment" columns="1" collapsible="false">
            <apex:outputField value="{!fd.Injury_meets_criteria_set_out_in_rules__c}" />
            <apex:outputField value="{!fd.Service_Planner_Recommendation__c}" />
            <apex:outputField value="{!fd.Service_Planner_Complete__c}" /> 
            <apex:outputField value="{!fd.Completed_By__c}" />            
        </apex:pageBlockSection>
        
        <apex:pageBlockSection title="Senior Delegate" columns="1" collapsible="false">
            <apex:outputField value="{!fd.Manager_ASD_Recommendation__c}" />
            <apex:outputField value="{!fd.Manager_ASD_Complete__c}" />            
        </apex:pageBlockSection>
        
        <apex:pageBlockSection title="App Rules Committee" columns="1" collapsible="false">
            <apex:outputField value="{!fd.Application_Rules_Comm_Chair_Concurs__c}" />            
            <apex:outputField value="{!fd.Notes_from_Application_Rules_Comm_Chair__c}" />  
        </apex:pageBlockSection>
        
        <apex:pageBlockSection title="Chief Executive Decision" columns="1" collapsible="false">            
            <apex:outputField value="{!fd.Notify_Application_Rules_Comm__c}" />
            <apex:outputField value="{!fd.Notes_for_Application_Rules_Comm_Chair__c}" />  
            <apex:outputField value="{!fd.Injury_eligible_under_the_Act_and_Rules__c}" />
            <apex:outputField value="{!fd.Injuries_are_the_result_of_an_MVA_in_SA__c}" />
            <apex:outputField value="{!fd.Injuries_not_excluded_under_Act__c}" />
            <apex:outputField value="{!fd.Status__c}" />            
            <apex:outputField value="{!fd.Status_Date__c}" />            
            <apex:outputField value="{!fd.Status_Reason__c}" />                        
            <apex:outputField value="{!fd.Note_for_Not_Accepted_Letter__c}" />            
            <apex:outputField value="{!fd.Re_Assessment_Date__c}" />            
            <apex:outputField value="{!fd.Application_made_with_lawful_authority__c}" label="Application has been made with lawful authority" rendered="{!LEFT(fd.Status__c, 7) == 'Pending'}" />
            <apex:outputField value="{!fd.Guardianship_application_pending__c}" rendered="{!LEFT(fd.Status__c, 7) == 'Pending'}" />
            <apex:outputField value="{!fd.Guardianship_application_details__c}" label="Details" rendered="{!LEFT(fd.Status__c, 7) == 'Pending'}" style="width: 300px;" />
            <apex:outputField value="{!fd.Guardianship_application_date_of_hearing__c}" label="Date of hearing, if known" rendered="{!LEFT(fd.Status__c, 7) == 'Pending'}" />   
        </apex:pageBlockSection>
        
        <apex:pageBlockSection title="System Information" collapsible="false">  
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="Created By" />
                <apex:outputPanel >
                    <apex:outputField value="{!fd.CreatedById}" />,&nbsp;
                    <apex:outputField value="{!fd.CreatedDate}" />
                </apex:outputPanel>
            </apex:pageBlockSectionItem> 
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="Last Modified By" />
                <apex:outputPanel >
                    <apex:outputField value="{!fd.LastModifiedById}" />,&nbsp;
                    <apex:outputField value="{!fd.LastModifiedDate}" />
                </apex:outputPanel>
            </apex:pageBlockSectionItem>                                              
            <apex:pageBlockSectionItem />           
            <apex:outputField value="{!fd.OwnerId}" />              
        </apex:pageBlockSection>                        
    </apex:pageBlock>    
    <c:ObjectHistoryList Record="{!fd}"  />
    <apex:relatedList subject="{!Form_Detail__c.id}" list="CombinedAttachments"/>
</apex:page>