<apex:page StandardController="RCR_Contract__c" extensions="RCRClientServiceAgreementExtension" renderAs="pdf" cache="false" expires="0">
    <apex:variable value="{!RCRContract}" var="c"/>
    <apex:variable value="{!OR($Profile.Name == 'RCR Portal', $Profile.Name == 'RCR Portal (Non Submit)')}" var="inPortal" />
    <apex:variable value="{!IF(c.RecordType.Name == 'Service Order - CBMS' && NOT(RCR_Contract__c.Has_CBMS_Contract__c), false, true)}" var="ContractValid"/>
    <html> 
    <head>
    <style>
         body {
            font-family:arial,sans-serif; 
            font-size:12px;
        }
        p
        {
            font-size:12px;
        }
        p.bold {
            font-weight:bold;
        }
        
        div.image
        {
            float:right;
        }
        
        div.NoBreak {
            page-break-inside: avoid;
        }
        
        div.right-align
        {
            clear:both;
            float:right;
            margin-bottom:20px;
        }
        th {
            background-color:#D8D8D8;
            vertical-align:top;
        }
        th.center {
            text-align:center;
        }
        
        th>div {
            width:100%;
        }
        
        td.center {
            text-align:center;
        }
        
        td.right {
            text-align:right;
        }
        
        div.content
        {
            width:100%;
            clear:both;
        }
        
        span.bold
        {
            font-weight:700;
        }
        
        div.break-after
        {
            page-break-after:always;
            width:100%;
            clear:both;
        }
        
        p.title
        {
            font-size:12px;
            font-weight:bold;
            text-decoration:underline;
            text-align:center;
            margin-bottom:20px;
        }
        
        h1
        {
            font-size:12px;
        }
        h2
        {
            font-size:11px;
            font-weight:normal;
            padding-left:20px;
        }
        h3
        {
            padding-left:40px;
            font-size:11px;
            font-weight:normal;
        }
        h4
        {
            padding-left:60px;
            font-size:11px;
            font-weight:normal;
        }
        
        div.terms p
        {
           padding-left:20px; 
        }

       table
       {
           width:100%;
           font-size:11px;
       }
       
       table.details
       {
           border: 1px solid black;
           border-collapse: collapse;
       }
       
       table.details td, table.details th
       {
           border: 1px solid black;
           padding: 3px;
           font-size:10px;
       }
       
       table.header
       {
           width:100%;
           text-align:center;
       }
       @Page {
           margin-top: 10mm;
           margin-left: 19mm;
           margin-right: 19mm;
           
           @top-center { 
               content: '{!IF(inPortal, 'Reference Copy Only - Refer to online RCR service portal for current CSA status. Date:' & TEXT(DAY(TODAY())) & '/' & TEXT(MONTH(TODAY())) & '/' & TEXT(YEAR(TODAY())), '')}';
               color: red;
               font-family:arial,sans-serif; 
               font-size:11px;
           }
       }
    </style>
    </head>
    <body>
<div style="{!IF(ContractValid, 'display:none;', 'width:100%;font-size:1.0em')}">
    A CBMS Service Order must be linked to a CBMS Contract before a Client Service Agreement can be generated.
</div>
<div style="{!IF(ContractValid, '', 'display:none;')}">
    <div class="break-after">
    <div class="image">
        <apex:image alt="DCSI Logo" url="{!$Resource.DCSILogoColour}" width="350px" height="86px" />
    </div>
    <div class="right-align">
        <p>
            Disability SA
        </p>
        <p>      
            Level 4 SW Riverside Building<br/>
            North Terrace<br/>
            Adelaide  SA  5000
        </p>    
        <p>
            PO Box 70 Rundle Mall<br/>
            Adelaide  SA  5000
        </p>
        Tel:    &nbsp; <apex:outputText value="{!ContractManagerPhone}" /><br/>
        Fax:    &nbsp; <apex:outputText value="{!ContractManagerFax}" /><br/>
        ABN:    11 525 031 144
    </div>

    <div class="content">
        Ref:&nbsp;<apex:outputText value="{!c.Name}" />
            <br/>
            <apex:outputText value="{!CurrentUser.Account_Name__c}" rendered="{!InPortal}" />
            <apex:outputText value="{!c.Account__r.Name}" rendered="{!NOT(InPortal)}" />
            <br/>
            <apex:outputText value="{!CurrentUser.Account_Billing_Address__c}" escape="false" rendered="{!InPortal}" />
                        <apex:outputText value="{0}<br/> {1} {2} {3}" escape="false" rendered="{!NOT(InPortal)}">
                            <apex:param value="{!c.Account__r.BillingStreet}" />
                            <apex:param value="{!c.Account__r.BillingCity}" />
                            <apex:param value="{!c.Account__r.BillingState}"/>
                            <apex:param value="{!c.Account__r.BillingPostalCode}" />
                        </apex:outputText>
            <br/>
            <apex:outputText value="ABN: {!CurrentUser.Account_ABN__c}" rendered="{!InPortal}" />
            <apex:outputText value="ABN: {!c.Account__r.ABN__c}" rendered="{!NOT(InPortal)}" />
            <br/>
        
</div>
<br/>
<div>
        <p>
        Dear
        <apex:outputText value="{!IF(ISBLANK(CurrentUser.Account_Contact_Name__c), ' Service Provider',  ' ' + CurrentUser.Account_Contact_Name__c)}"
            rendered="{!InPortal}" />
        <apex:outputText value="{!IF(ISBLANK(c.Account__r.Contact_First_Name__c), ' Service Provider', ' ' + c.Account__r.Contact_First_Name__c + ' ' + c.Account__r.Contact_Surname__c)}"
            rendered="{!NOT(InPortal)}" />
        </p>
        <p>
        <span class="bold">RE: Individual Support Services for<apex:outputText value=" {!IF(NOT(ISBLANK(c.Client_Name__c)), c.Client_Name__c, c.Group_Program__c)}"/></span>
        </p>
        <p>
        I am requesting that you/your company (“you”) are engaged by the Minister for Disabilities (“Minister”) to provide the services described in the attached schedule.
        </p>
        <p>
        The Minister is prepared to engage you to undertake the services on a contract for services based on the following conditions.
        </p>
        <p>
<span class="bold">Proposal</span><br/>
1.  The contract will be on the Terms and Conditions of Engagement, a copy of which is attached to this letter.<br/>
2.  You will provide the services described in Item 2 of Schedule 1 (“<span class="bold">Services</span>”) for the term set out in Item 3 of Schedule 1.<br/>
3.  You will effect and maintain the insurances as required in Item 6 of Schedule 1.<br/>
4.  The fee payable by the Minister to you for the provision of the Services is set out in Item 7 of Schedule 1 and is payable at the time and in the manner set out in that Item.
</p>
<p>
Please note that the Department for Communities and Social Inclusion has included a special condition relating
to police checks for staff who provide services to people with disability. This change ensures consistency across
disability services in South Australia and provides additional safeguards for people with disability in the community.
</p>
<p>
More information about the DCSI Screening Unit can be found at<br/>
<apex:outputLink value="http://www.dcsi.sa.gov.au/services/screening" target="_blank">www.dcsi.sa.gov.au/services/screening</apex:outputLink>
</p>
<p>
To accept this proposal please sign at the foot of the duplicate copy of this letter and return to the Disability Brokerage Unit within 7 days
of receipt of this letter.
</p>
<p>
Yours sincerely
<br/>
<br/>
<apex:outputPanel rendered="{!NOT(ISBLANK(c.Approver_User_ID__c))}">
<apex:image url="{!URLFOR($Resource.RCRImages, 'img/DirectorsSignature.JPG')}" height="100" />
</apex:outputPanel>
<br/>
<apex:outputText value="{!MinistersContractManagerName}" /><br/>
<apex:outputText value="{!MinistersContractManagerPosition}" escape="false"/>
<br/><br/>
<apex:outputText value="{0,date,dd MMMM yyyy}">
    <apex:param value="{!c.Date_Approved__c}" /> 
</apex:outputText>
</p>
    </div>    
    </div>
    <div class="break-after">
    <p class="title">
    ACCEPTANCE
    </p>
<p>
The above proposal (including the Schedules and Terms and Conditions of Engagement attached) whereby 
on&nbsp;<apex:outputField value="{!c.Date_Approved__c}"/> the Minister offers to 
engage<apex:outputText value=" {!IF(InPortal, CurrentUser.Account_Name__c, c.Account__r.Name)} " />(ABN:<apex:outputText value=" {!IF(InPortal, CurrentUser.Account_ABN__c, c.Account__r.ABN__c)}" />) 
to perform the Services, referred to as Service Order<apex:outputText value=" {!c.Name}" />, and described in Item 2 of Schedule 1 is accepted.
</p>
<br/>
<p>Dated:&nbsp;
    <apex:outputField value="{!c.CSA_Acceptance_Date__c}" rendered="{!IF(c.RCR_Service_Order_Acceptances__r.size > 0 && NOT(ISBLANK(c.CSA_Acceptance_Date__c)), true, false)}"/>
</p>
<br/>
<br/>
<br/>
<p>Signed<br/><br/>
    <apex:outputLabel value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{!If(c.RCR_Service_Order_Acceptances__r.size > 0 && NOT(ISBLANK(c.CSA_Acceptance_Date__c)), 'Accepted Electronically<br/><br/>', '<br/>………………………………………………………………………')}" 
    style="font-style:italic"
    escape="false"/>
</p>
<p>Name<br/><br/>
    <apex:outputText value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{!c.RCR_Service_Order_Acceptances__r[0].Authorised_Person__r.Name}<br/><br/>" 
        rendered="{!c.RCR_Service_Order_Acceptances__r.size > 0 && NOT(ISBLANK(c.CSA_Acceptance_Date__c))}"
        style="font-style:italic"
        escape="false"/>
    <apex:outputLabel value="………………………………………………………………………" escape="false" rendered="{!IF(ISBLANK(c.CSA_Acceptance_Date__c), true, false)}"/>
</p>
<p>Position<br/><br/>
    <apex:outputLabel value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{!c.RCR_Service_Order_Acceptances__r[0].Position__c}<br/><br/>" 
    style="font-style:italic"
    escape="false" 
    rendered="{!IF(c.RCR_Service_Order_Acceptances__r.size > 0 && NOT(ISBLANK(c.CSA_Acceptance_Date__c)), true, false)}"/>
    <apex:outputLabel value="<br/>………………………………………………………………………" escape="false" rendered="{!IF(ISBLANK(c.CSA_Acceptance_Date__c), true, false)}"/>
</p>
    </div>
    <div class="break-after">
    <p class="title">TERMS AND CONDITIONS OF ENGAGEMENT</p>

<div class="terms">
<h1>1. SERVICES</h1>

<h2><b>1.1 </b>The Service Provider must perform the services specified in Item 2 of Schedule 1 (“<b>Services</b>”) in accordance with the performance milestones as set out in that Item.</h2>

<h2><b>1.2 </b>The Service Provider must provide the reports specified in Item 8 of Schedule 1 at such times and in such format as is set out in that Item.</h2>

<h2><b>1.3 </b>The Service Provider shall attend such meetings and give such presentations as the Minister’s Representative may from time to time reasonably request.</h2>

<h2><b>1.4 </b>The Service Provider must perform the Services in accordance with this Contract:</h2>

<h3>1.4.1 professionally, ethically, carefully, skilfully and competently; </h3>

<h3>1.4.2 in a timely and efficient manner;</h3>

<h3>1.4.3 in accordance with the best practices current in the Service Provider’s industry; and</h3>

<h3>1.4.4 strictly in accordance with the standards referred to in Item 2 of Schedule 1 (if any).</h3>

<h2><b>1.5 </b>The Service Provider shall keep proper records of the Service Provider’s work pursuant to this Contract and shall as and when reasonably required by the Minister supply to the Minister evidence of the Service Provider’s progress in undertaking the tasks to be performed pursuant to these services.</h2>

<h2><b>1.6 </b>The Service Provider must not subcontract any part of the performance of the Services without the prior written approval of the Minister. The Service Provider remains responsible for obligations performed by subcontractors to the same extent as if such obligations were performed by the Service Provider.</h2>

<h2><b>1.7 </b>The Services must be performed personally by the persons specified in Item 4 of Schedule 1 if any (“<b>Service Provider’s Staff</b>”).</h2>

<h2><b>1.8 </b>In the performance of the services, the Service Provider must deal fairly with the Minister in a manner consistent with the public interest.</h2>

<h2><b>1.9 </b>Without limiting 1.8 above, the Service Provider agrees to conduct itself in a manner that does not invite, directly or indirectly, the Minister’s officers, employees or agents or any public sector employee (as defined in the Public Sector Act 2009) to behave unethically, to prefer private interests over the Minister’s interests or to otherwise contravene the Code of Ethics for the South Australian Public Sector. </h2>

<h1>2. FEE AND PAYMENT</h1>

<h2><b>2.1 </b>The Minister shall pay the Service Provider the Fee for the Services at the rate specified in Item 7 of Schedule 1. The Service Provider must provide such billing information as is required in Item 7.</h2>

<h2><b>2.2 </b>The Service Provider shall invoice the Minister at the intervals set out in Item 7. Provided that the Service Provider has performed the Services to the Minister’s reasonable satisfaction, of the performance milestones, undisputed invoices will be paid by the Minister within thirty (30) days of the invoice being received. All invoices must be tax invoices.</h2>

<h2><b>2.3 </b>The Fee is inclusive of GST.</h2>

<h2><b>2.4 </b>The Service Provider represents that:</h2>

<h3>2.4.1 It is registered under the <i>A New Tax System (Australian Business Number) Act 1999</i> (Cth);</h3>

<h3>2.4.2 The ABN shown in Item 10 of Schedule 1 is its ABN.</h3>

<h2><b>2.5 </b>The Service Provider acknowledges that should the representations referred to in clause 2.4 be or become incorrect:</h2>

<h3>2.5.1 the Minister may be obliged under the <i>Taxation Administration Act 1953</i> (Cth) to deduct a withholding from the Fee and will not be obliged to gross up the Fee or make any other compensation to the Service Provider; and</h3>

<h3>2.5.2 if the supply of Services is not a Taxable Supply, the Minister is entitled to reduce the Fee by the amount which would have been attributable to GST had the supply been a Taxable Supply.</h3>

<h1>3. CONFIDENTIALITY AND SECURITY</h1>

<h2><b>3.1 </b>Subject to clause 9.3, all information which the Service Provider obtains in the course of providing the Services is confidential and the Service Provider must not disclose such information to any person without the prior written consent of the Minister, except when such disclosure is necessary for the purpose of carrying out the Services. The Service Provider must do everything reasonably possible to preserve the confidentiality of the information.</h2>

<h2><b>3.2 </b>The Service Provider, if required by the Minister, must give its consent to the conduct of a police check or any other enquiry and the Service Provider, if required by the Minister, must procure the consent of the Service Provider’s Staff to the conduct of a police check or any other enquiry. </h2>
<div class="break-after"/>
<h1>4. INTELLECTUAL PROPERTY AND DOCUMENTS</h1>

<h2><b>4.1 </b>The Minister owns all intellectual property rights in all things produced by the Service Provider as part of the Services. The Service Provider must ensure that in performing the Services it does not infringe the intellectual property rights of any person. The Service Provider shall indemnify the Minister against all costs, expenses and liability arising out of any claim that the performance of the Services by the Service Provider infringes the intellectual property rights of any person.</h2>

<h2><b>4.2 </b>The Minister does not own the Service Provider’s intellectual property rights in existence before the date of this Contract or copyright in existing publications or other work produced by or on behalf of the Service Provider prior to, or otherwise than in the course of providing the Services.</h2>

<h2><b>4.3 </b>If this Contract is terminated before the Service Provider has completed the Services, the Service Provider must licence to the Minister, free of charge, the intellectual property rights in any matter referred to in clause 5.2 if the Minister needs such a licence to enable it to complete the Services.</h2>

<h2><b>4.4 </b>The Minister owns all documents prepared by the Service Provider in connection with this Contract. Upon termination and at any time on demand by the Minister, the Service Provider must deliver to the Minister all documents provided by or originating from the Minister and all documents produced by the Service Provider in the course of performing the Services.</h2>

<h1>5. INSURANCE</h1>

<h2><b>5.1 </b>The Service Provider must effect and maintain at its expense during the term:</h2>

<h3>5.1.1 Public Liability Insurance in the name of the Service Provider for not less than the amount specified in Item 6 of Schedule 1 for any one event and in the aggregate for products liability in any one policy period or such other amount as the Minister may reasonably require and such policy shall include and name the Minister and the Crown in right of the State of South Australia as principal;</h3>

<h3>5.1.2 Professional Indemnity Insurance in the name of the Service Provider for not less than the amount specified in Item 6 of Schedule 1 for any one event and in the aggregate in any one policy period or such other amount as the Minister may reasonably require and will be renewed and maintained for a period of twelve months following the date of expiration or earlier termination of this Contract; and</h3>

<h3>5.1.3 Workers compensation insurance in accordance with the applicable worker’s compensation legislation.</h3>

<h2><b>5.2 </b>The Service Provider will maintain the insurances pursuant to this clause with insurers satisfactory to the Minister. The Minister in specifying levels of insurance in this Contract accepts no liability for the completeness of their listing, the adequacy of the sum insured, limit of liability, scope of coverage, conditions or exclusions of those insurances in respect to how they may or may not respond to any loss damage or liability.</h2>

<h1>6. NON PERFORMANCE</h1>

<p>If the Services, or any performance milestone required by the Minister’s Representative is not undertaken to the Minister’s required standard or is not undertaken on time having regard to the time set for completion, then the Minister may withhold payment until the Services are undertaken to the required standard or the Minister may terminate the Contract.</p>

<h1>7. TERMINATION</h1>

<h2><b>7.1 </b>If the Service Provider:</h2>

<h3>7.1.1 fails to commence or complete work pursuant to this Contract in accordance with the time requirements and performance milestones specified in this Contract;</h3>

<h3>7.1.2 fails to perform, observe and carry out all the agreements and obligations imposed upon the Service Provider by this Contract; </h3>

<h3>7.1.3 fails to comply with any default notice given by the Minister to the Service Provider requiring a breach to be remedied within forty eight (48) hours after the service of the default notice; </h3>

<h3>7.1.4 has submitted a declaration in relation to unlawful collusion to the Principal in the procurement process preceding this Contract which is found to be false in any particular;</h3>

<h3>7.1.5 abandons or refuses to proceed with Services; or</h3>

<h3>7.1.6 enters into any form of insolvency administration if a Company or becomes bankrupt if an individual, </h3>

<h3>then the Minister may at any time thereafter without prejudice to any other legal remedies it may have, terminate this Contract by written notice to the Service Provider.</h3>

<h2><b>7.2 </b>The Minister may terminate this Contract without cause by giving written notice. The Minister may give reasonable directions to the Service Provider in relation to the performance of the Services up to the date of termination.</h2>

<h2><b>7.3 </b>If the Minister terminates the Contract, the Minister must pay only the proportion of the fee commensurate with that part (if any) of the Services undertaken to the Minister’s satisfaction (acting reasonably).</h2>
<div class="break-after"/>
<h1>8. Special Conditions </h1>

<h2><b>8.1 </b>The Service Provider must comply with the special conditions (if any) outlined in Schedule 2. If any of the documents comprising this Agreement are inconsistent, they shall take priority in the following order:</h2>

<h3>8.1.1 the special conditions (if any) in Schedule 2;</h3>

<h3>8.1.2 these standard terms and conditions (excluding Schedule 1); and</h3>

<h3>8.1.3 Schedule 1.</h3>

<h1>9. GENERAL</h1>

<h2><b>9.1 Service Provider Status</b></h2>

<p>The Service Provider is being engaged for the provision of services and nothing in this Contract constitutes any relationship of employer and employee or partnership between the parties. The Minister has no obligations to the Service Provider’s Staff. The Service Provider is responsible for complying with the <i>Income Tax Assessment Act 1997</i> (Cth) in respect of the Service Provider’s employees and the Minister is not required to make PAYE deductions from the Fee.</p>

<h2><b>9.2 Auditor-General</b></h2>

<p>Nothing in this Contract derogates from the powers of the Auditor-General under the <i>Public Finance and Audit Act 1987 </i>(SA).</p>

<h2><b>9.3 Contract Disclosure</b></h2>

<p>The Minister may disclose this Contract and/or information relating to this Contract in either printed or electronic form and either generally to the public or to a particular person as a result of a specific request. Nothing in this clause derogates form the Service Provider’s obligations under any provision of this Contract or from the provisions of the <i>Freedom of Information Act 1991</i> (SA).</p>

<h2><b>9.4 Conflicts of interest</b></h2>

<p>The Service Provider must not have any conflicts of interest in relation to the performance of the Services under this Contract and if an actual or potential conflict of interest exists, arises or may arise in the course of performing the Services (either for the Service Provider or the Service Provider’s Staff) the Service Provider must disclose that information to the Project Manager as soon as practicable after it becomes aware of the conflict or potential conflict.</p>

<h2><b>9.5 Contract and Variations</b></h2>

<h3>9.5.1 This Contract and the entire agreement between the Minister and the Service Provider in respect of this consultancy is comprised by:</h3>

<h4>(a) these terms and conditions, </h4>

<h4>(b) the Schedules, </h4>

<h4>(c) the proposal letter; and </h4>

<h4>(d) the Service Provider’s acceptance. </h4>

<h3>9.5.2 Any variation to the Contract must be in writing and signed by the Minister and the Service Provider.</h3>

<h2><b>9.6 No Assignment</b></h2>

<p>The Service Provider must not assign, novate or encumber any of its rights or obligations under this Contract without first obtaining the written consent of the Minister.</p>

<h2><b>9.7 Survival</b></h2>

<p>The clauses of this Contract relating to documents, intellectual property rights, insurance and conflicts of interest survive the expiry or termination of this Contract, and in relation to confidentiality, the obligations continue to apply unless the Minister notifies the Service Provider of its release from those obligations.</p>

<h2><b>9.8 Proper Law and Jurisdiction of the Courts</b></h2>

<h3>9.8.1 The laws in force in South Australia apply to this Contract.</h3>

<h3>9.8.2 The courts of South Australia have exclusive jurisdiction to determine any legal proceedings in relation to this Contract.</h3>
</div>
</div>
<div class="break-after">
<p class="title">SCHEDULE 1</p>

<p class="bold">ITEM 1 Service Provider</p>

<p><apex:outputText value=" {!IF(InPortal, CurrentUser.Account_Name__c, c.Account__r.Name)} " /><br/>ABN: <apex:outputText value=" {!IF(InPortal, CurrentUser.Account_ABN__c, c.Account__r.ABN__c)}" /></p>

<p class="bold">ITEM 2 Services</p>

<p>
    The Service Provider must provide the following Services, referred to as Service Order&nbsp;<apex:outputText value="{!c.Name}" />, for:<br/>
    <apex:outputText value="{!c.Client_Name__c}" escape="false"/><br/>
    <apex:outputText value="{!c.Client_Address__c}" /><br/>
    <apex:outputText value="{!c.Client_Suburb__c}" />
    &nbsp;
    <apex:outputText value="{!c.Client_State__c}" />
    &nbsp;
    <apex:outputText value="{!c.Client_Postcode__c}" /><br/>
    <apex:outputText value="{!c.Client_Phone__c}" /><br/><br/>
    <apex:outputText value="{!c.Comments__c}" />
</p>

<p>
    <table class="header">
        <tr class="header">
            <td>
                <span class="bold"><apex:outputText value="SCHEDULE OF SERVICES"></apex:outputText></span>
            </td>
        </tr>
    </table>
</p>
<p>
    Unless otherwise agreed the Services for the Client are not to exceed the following
    maximum number of units.
</p>
<div class="NoBreak">
        <p class="bold" style="text-align:center">
            The above service consists of the following breakdown
        </p>
        <p class="bold">
            Service Schedule
        </p>
        <apex:dataTable value="{!ContractPeriodBreakdowns}" var="p" styleClass="details" columnsWidth="38%,8%,8%,10%,6%,13%,6%,10%">
            <apex:column >
                <apex:facet name="header">Service</apex:facet>
                <apex:outputText value="{!p.ServiceDescription}"/>
            </apex:column>
            <apex:column headerClass="center" styleClass="center">
                <apex:facet name="header">Start Date</apex:facet>
                <apex:outputText value="{0,date,dd/MM/yyyy}">
                   <apex:param value="{!p.StartDate}"/>
                </apex:outputText>
            </apex:column>
            <apex:column headerClass="center" styleClass="center">
                <apex:facet name="header">Finish Date</apex:facet>
                <apex:outputText value="{0,date,dd/MM/yyyy}">
                   <apex:param value="{!p.EndDate}"/>
                </apex:outputText>
            </apex:column>
            <apex:column headerClass="center" styleClass="center">
                <apex:facet name="header">Period</apex:facet>
                <apex:outputText value="{!p.Period}"/>
            </apex:column>
            <apex:column headerClass="center" styleClass="center">
                <apex:facet name="header">Period Units</apex:facet>
                <apex:outputText value="{!p.PeriodUnits}"/>
            </apex:column>
            <apex:column headerClass="center">
                <apex:facet name="header">Flexibility</apex:facet>
                <apex:outputText value="{!p.Flexibility}"/>
            </apex:column>
            <apex:column footerValue="Total" footerClass="right" headerClass="center" styleClass="center">
                <apex:facet name="header">Total Units</apex:facet>
                <apex:outputText value="{!p.TotalUnits}"/>
            </apex:column>
            <apex:column headerClass="center" footerClass="right" styleClass="right">
                <apex:facet name="header">Total Cost</apex:facet>
                <apex:facet name="footer"><apex:outputField value="{!c.Total_Service_Order_Cost__c}"/></apex:facet>
                <apex:outputText value="{0,number,$###,###,##0.00}">
                   <apex:param value="{!p.TotalCost}"/>
                </apex:outputText>
            </apex:column>
        </apex:dataTable>
</div>
<div class="NoBreak">        
        <p class="bold">
            Rate Schedule
        </p>
        <table class="details">
           <tr>
               <th width="35%">Service</th>
               <th width="10%" class="center">Start Date</th>
               <th width="10%" class="center">Finish Date</th>
               <th width="5%" class="center">P/H</th>
               <th width="10%" class="center">Total Units</th>
               <th width="10%" class="center">Rate</th>
               <th width="10%" class="center">Total Cost</th>
           </tr>
        <apex:repeat value="{!ContractRateBreakdowns}" var="r">
            <apex:repeat value="{!r.ScheduledServiceBreakdownsByRate}" var="rate">
            <tr>
                <td><apex:outputText value="{!r.ServiceDescription}"/></td>
                <td class="center"><apex:outputField value="{!rate.Start_Date__c}"/></td>
                <td class="center"><apex:outputField value="{!rate.End_Date__c}"/></td>
                <td class="center"><apex:outputText value="{!IF(rate.Public_Holiday__c, 'Yes', 'No')}"/></td>
                <td class="center"><apex:outputField value="{!rate.Quantity__c}"/></td>
                <td class="right"><apex:outputField value="{!rate.Rate__c}"/></td>
                <td class="right"><apex:outputField value="{!rate.Total__c}"/></td>
            </tr>
           </apex:repeat>
           <tr style="{!IF(r.ScheduledServiceBreakdownsByRate.size == 0, '', 'display:none')}">
               <td>
                   <apex:outputText value="{!r.AdHocService.Comment__c}"/>
               </td>
               <td class="center">
                   <apex:outputText value="{0,date,dd/MM/yyyy}">
                       <apex:param value="{!r.AdHocService.Start_Date__c}"/>
                   </apex:outputText>
               </td>
               <td class="center">
                   <apex:outputText value="{0,date,dd/MM/yyyy}">
                       <apex:param value="{!r.AdHocService.End_Date__c}"/>
                   </apex:outputText>
               </td>
               <td>&nbsp;</td>
               <td class="center">
                   <apex:outputText value="{!r.AdHocService.Qty__c}"/>
               </td>
               <td class="right">
                   <apex:outputText value="{0,number,$###,###,##0.00}">
                       <apex:param value="{!r.AdHocService.Rate__c}"/>
                   </apex:outputText>
               </td>
               <td class="right">
                   <apex:outputText value="{0,number,$###,###,##0.00}">
                       <apex:param value="{!r.AdHocService.Amount__c}"/>
                   </apex:outputText>
               </td>
           </tr>
        </apex:repeat>
           <tr>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
               <td class="right">Total</td>
               <td class="right"><apex:outputField value="{!c.Total_Service_Order_Cost__c}"/></td>
           </tr>
        </table>
</div>
<div class="NoBreak">        
        <p class="bold">
            Service Types
        </p>
        <apex:dataTable value="{!ContractServiceTypes}" var="ScheduledContracts" styleClass="details" columnsWidth="39%,11%,42%,10%">
            <apex:column >
                <apex:facet name="header">Service</apex:facet>
                <apex:outputText value="{!ScheduledContracts.ServiceDescription}"/>
            </apex:column>
            <apex:column styleClass="center" headerClass="center">
                <apex:facet name="header">Program</apex:facet>
                <apex:outputText value="{!ScheduledContracts.Program}"/>
            </apex:column>
            <apex:column footerValue="Total" footerClass="right">
                <apex:facet name="header">Service Type</apex:facet>
                <apex:outputText value="{!ScheduledContracts.ServiceType}"/>
            </apex:column>
            <apex:column styleClass="center" headerClass="center" footerValue="{!ContractServiceTotalQuantity}" footerClass="center">
                <apex:facet name="header">Units</apex:facet>
                <apex:outputText value="{0,number,##0.00}"><apex:Param value="{!ScheduledContracts.Quantity}" /></apex:outputText>   
            </apex:column>
        </apex:dataTable>
</div>
<p class="bold">ITEM 3 Term</p>

<p>The Service Provider must commence providing the Services on&nbsp;<apex:outputField value="{!c.Start_Date__c}" />.</p>

<p>The Service Provider must continue to provide the Services until&nbsp;<apex:outputField value="{!c.End_Date__c}" />.</p>

<p class="bold">ITEM 4 Service Provider’s Staff</p>

<p>Not Applicable</p>

<p class="bold">ITEM 5 Location</p>

<p>The Services should be provided at&nbsp;<apex:outputText value="{!c.Client_Address__c}" />,&nbsp;<apex:outputText value="{!c.Client_Suburb__c}" />.</p>

<p class="bold">ITEM 6 Insurances </p>

<p>Public Liability insurance:</p>

<p><apex:outputtext value="{0, number, $###,###,###,##0.00}">
    <apex:param value="{!PublicLiabilityInsurance}" />
   </apex:outputtext></p>

<p>Professional Indemnity insurance:</p>

<p><apex:outputtext value="{0, number, $###,###,###,##0.00}">
    <apex:param value="{!PublicIndemnityInsurance}"/>
    </apex:outputtext>
</p>

<p class="bold">ITEM 7 Fee</p>

<p>The Minister will pay to the Service Provider the following fees for the Services at the times and in the following manner:</p>
<p>
<apex:outputtext value="{0, number, $###,###,###,##0.00}">
    <apex:param value="{!c.Total_Service_Order_Cost__c}" />
</apex:outputtext></p>

<p class="bold">ITEM 8 Service Provider’s ABN is&nbsp;<apex:outputText value=" {!IF(InPortal, CurrentUser.Account_ABN__c, c.Account__r.ABN__c)}" />.</p>

<p class="bold">ITEM 9 The Minister’s Representative is:</p>
<p>
<apex:outputtext value="{!MinistersContractManagerName}" />,<br/>
<apex:outputText value="{!MinistersContractManagerPosition}" escape="false" />
</p>
</div>
<div class="no-break">
<p class="title">SCHEDULE 2<br/>SPECIAL CONDITIONS</p>
<p class="bold">
POLICE CHECKS
</p>
<p>
<h2>
1.1 "Police Check" means a National Criminal History Record and Background Screening Check undertaken exclusively by the Screening Unit
of the Department for Communities and Social Inclusion.
</h2>
<h2>
1.2 The Service Provider will ensure that all of the Service Provider's Staff have undergone Police Checks in accordance with clause
1.3 and where otherwise deemed appropriate by the Minister from time to time.
</h2>
<h2>
1.3 Subject to clause 1.4, and as a minimum, the Service Provider will ensure that Police Checks are undertaken for any of the Service
Provider's Staff, including any new staff.
</h2>
<h3>
1.3.1 who are to provide Servcie directly to Clients, or have regular contact with Clients, or work in close proximity to Clients
on a regular basis; or
</h3>
<h3>
1.3.2 who supervise or manage Service Provider's Staff in positions requiring or involving regular contact with Clients or working
in close proximity to Clients on a regular basis; or
</h3>
<h3>
1.3.3 have access to records relating to Clients
</h3>
<h3>
and such Police Checks must be undertaken by the Service Provider every three years for each of he Service Provider's Staff.
</h3>
<h2>
1.4 Subject to clause 1.6, if the Service Provider has Police Check policy ("Policy") in place with requirements that exceed
the minimum standard described in clause 1.3, then the provisions of the Policy will take precedence over the miniumn
standard described in clause 1.3 and the Service Provider will ensure that it complies with the requirements of its
Policy at all times.
</h2>
<h2>
1.5 Nothing in this clause 1 will limit or be construed as limiting the Service Provider's obligation to comply with the
requirements (if any) in the respect of police checks as set out in any applicable legislation (including any regulations).
</h2>
<h2>
1.6 The Minister may under this Contract require the Service Provider to undertake Police Checks for:
</h2>
<h3>
1.6.1 an alternate period relating to all the Service Provider's Staff; or
</h3>
<h3>
1.6.2 such persons or class of persons within the Service Provider's Staff as determined by the Minister from time to time.
</h3>
</p>

</div>
</div>
</body>
</html>
</apex:page>