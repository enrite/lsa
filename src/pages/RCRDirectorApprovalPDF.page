<apex:page controller="RCRDirectorApprovalController" renderAs="pdf">
<html>
        <head>
            <style>
                @page {
                    size:A3 landscape;
                    @top-center {
                        font-family:Arial,sans-serif;
                        font-size:20px;
                        font-weight:bold;
                        content: "PTIC Approvals for the period {!StartDt} to {!EndDt}";
                    }
                    @bottom-center {
                        content: "Page " counter(page);
                    }
                }
                body {
                    font-family:Arial,sans-serif;
                    font-size:12px;
                } 
                .myTbl {
                    width:100%;
                    border-collapse:collapse;
                }
                .myTbl td {
                    vertical-align:top !important;
                }
                .details {
                    border:1px solid black;
                    border-collapse:collapse;
                }
                .details tr {
                    page-break-inside:avoid;
                    page-break-after:auto;
                }
                .details td {
                    vertical-align:top !important;
                    border:1px solid black;
                    padding:2px;
                }
                .details th {
                    background-color:#e3e3d7;
                    border:1px solid black;
                }
                .tableHdr {
                    font-weight:bold;
                    background-color:#e3e3d7 !important;
                    border:1px solid black;
                }
                .subTbl {
                    border:1px solid black;
                    border-collapse:collapse;
                    width:100%;
                }
                .subTbl th {
                    background-color:#e3e3d7 !important;
                }
                .subTblNoBorder {
                    border:0px !important;
                    width:100%;
                }
                .subTblNoBorder td {
                    border:0px !important;
                }
                .subTblNoBorder th {
                    border:0px !important;
                }
                table.myTbl th.center {
                    text-align:center;
                }
                .bold {
                    font-weight:bold;
                }
                .center {
                    text-align:center;
                }
                .right {
                    text-align:right;
                }
                .middle {
                    vertical-align:middle !important;
                }
                .evenRow {
                    background-color:#f2f3f3;
                }
                .oddRow {
                    background-color:#e0e3e5;
                }
                .red {
                    background-color:red;
                }
                .customPopup {
                    position:absolute;
                    top:100px;
                    left:300px;
                    z-index: 9999;
                }
                .hide {
                    display:none;
                }
                .myFrame {
                    background-color:#3d8d8d;
                    padding:10px;
                    border-radius:20px;
                }
                .background {
                     opacity:0.5;
                     filter: alpha(opacity = 50);
                     z-index:9998;
                     position:absolute;
                     top:0;
                     left:0;
                     height:100%;
                     width:100%;
                     background-color:#ADD8E6; 
                     display:none;          
                }
            </style>
        </head>
        <body>
            <apex:form id="myForm">
                
                <apex:pageBlock >
                    
                    <apex:sectionHeader title="New Requests"/>

                    <apex:repeat value="{!Requests}" var="r">
                        <div style="border:1px solid black;page-break-inside:avoid" class="evenRow">
                            <table class="myTbl">
                                <tr class="tableHdr" style="height:25px">
                                    <th style="width:10%">Request Details</th>
                                    <th style="width:26%;text-align:center">Allocation Change</th>
                                    <th style="width:34%">Solution Details</th>
                                    <th style="width:10%">Criteria</th>
                                    <th style="width:10%">Recommendation</th>
                                    <th style="width:10%">Status</th>
                                </tr>
                                <tr>
                                    <td>
                                        <apex:panelGrid columns="1" rowClasses="tableHdr left,dataRow left">
                                            <apex:outputLabel value="Client/Group"/>
                                            <apex:outputText value="{!r.ServiceRequest.Client__r.Name}"/>
                                            
                                            <apex:outputLabel value="Request #"/>
                                            <apex:outputText value="{!r.ServiceRequest.Name}"/>
                                            
                                            <apex:outputLabel value="Panel Date"/>
                                            <apex:outputField value="{!r.ServiceRequest.Panel_Date__c}"/>

                                            <apex:outputLabel value="Date Range"/>
                                            <apex:panelGroup ><apex:outputField value="{!r.ServiceRequest.Start_Date__c}"/>&nbsp;to&nbsp;<apex:outputField value="{!r.ServiceRequest.End_Date__c}"/></apex:panelGroup>
                                            
                                            <apex:outputLabel value="New Request Type"/>
                                            <apex:outputText value="{!r.ServiceRequest.New_Request_Type__c}"/>
                                        </apex:panelGrid>
                                    </td>
                                    <td>
                                        <!-- CALCULATE ALLOCATION CHANGE TOTALS -->
                                        <apex:variable value="{!0.0}" var="recurrentTotal"/>
                                        <apex:variable value="{!0.0}" var="onceOffTotal"/>
                                        <apex:variable value="{!0.0}" var="recurrentFYETotal"/>
                                        <apex:variable value="{!0.0}" var="onceOffFYETotal"/>
                                        <apex:repeat value="{!r.ServiceAllocations}" var="key">
                                            <apex:variable value="{!recurrentTotal + r.ServiceAllocations[key].RecurrentAlloc}" var="recurrentTotal"/>
                                            <apex:variable value="{!onceOffTotal + r.ServiceAllocations[key].OnceOffAlloc}" var="onceOffTotal"/>
                                            <apex:variable value="{!recurrentFYETotal + r.ServiceAllocations[key].RecurrentFYEAlloc}" var="recurrentFYETotal"/>
                                            <apex:variable value="{!onceOffFYETotal + r.ServiceAllocations[key].OnceOffFYEAlloc}" var="onceOffFYETotal"/>
                                        </apex:repeat>
                                        
                                        <!-- ALLOCATION CHANGE TABLE -->
                                        <apex:pageBlockTable value="{!r.ServiceAllocations}" var="key" styleClass="subTbl" captionClass="tableHdr">
                                            <apex:column value="{!r.ServiceAllocations[key].ServiceType}" headerValue="Service" footerValue="Total" footerClass="bold"/>
                                            <apex:column headerValue="Recurrent $" styleClass="right" headerClass="right" footerClass="right bold">
                                                <apex:outputText value="{0, number, currency}">
                                                    <apex:param value="{!r.ServiceAllocations[key].RecurrentAlloc}"/>
                                                </apex:outputText>
                                                
                                                <apex:facet name="footer">
                                                    <apex:outputText value="{0, number, currency}">
                                                        <apex:param value="{!recurrentTotal}"/>
                                                    </apex:outputText>
                                                </apex:facet>
                                            </apex:column>
                                            <apex:column headerValue="Once Off $" styleClass="right" headerClass="right" footerClass="right bold">
                                                <apex:outputText value="{0, number, currency}">
                                                    <apex:param value="{!r.ServiceAllocations[key].OnceOffAlloc}"/>
                                                </apex:outputText>
                                                
                                                <apex:facet name="footer">
                                                    <apex:outputText value="{0, number, currency}">
                                                        <apex:param value="{!onceOffTotal}"/>
                                                    </apex:outputText>
                                                </apex:facet>
                                            </apex:column>
                                            <apex:column headerValue="Recurrent FYE $" styleClass="right" headerClass="right" footerClass="right bold">
                                                <apex:outputText value="{0, number, currency}">
                                                    <apex:param value="{!r.ServiceAllocations[key].RecurrentFYEAlloc}"/>
                                                </apex:outputText>
                                                
                                                <apex:facet name="footer">
                                                    <apex:outputText value="{0, number, currency}">
                                                        <apex:param value="{!recurrentFYETotal}"/>
                                                    </apex:outputText>
                                                </apex:facet>
                                            </apex:column>
                                            <apex:column headerValue="Once Off FYE $" styleClass="right" headerClass="right" footerClass="right bold">
                                                <apex:outputText value="{0, number, currency}">
                                                    <apex:param value="{!r.ServiceAllocations[key].OnceOffFYEAlloc}"/>
                                                </apex:outputText>
                                                
                                                <apex:facet name="footer">
                                                    <apex:outputText value="{0, number, currency}">
                                                        <apex:param value="{!onceOffFYETotal}"/>
                                                    </apex:outputText>
                                                </apex:facet>
                                            </apex:column>
                                        </apex:pageBlockTable>
                                    </td>
                                    <td>
                                        {!r.ServiceRequest.Service_to_be_provided__c}
                                    </td>
                                    <td>
                                        <apex:dataTable value="{!r.SelectedCriteriaFieldSet}" var="a" styleClass="subTbl" rendered="{!r.SelectedCriteriaFieldSet.size > 0}">
                                            <apex:column value="{!a}" headerClass="center"/>
                                        </apex:dataTable>
                                    </td>
                                    <td>
                                        <apex:outputLabel value="{!r.ServiceRequest.Recommendation__c}"/><br/>
                                        <apex:outputField value="{!r.ServiceRequest.Recommendation_Reason__c}"/>
                                    </td>
                                    <td>
                                        <apex:outputText value="{!IF(r.ServiceRequest.Status__c = 'Pending Approval', 'Director Approved - Waiting Service Order Approval.', r.ServiceRequest.Status__c)}"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        <apex:outputLabel value="Current Situation" styleClass="bold"/><br/>
                                        <apex:outputPanel rendered="{!NOT(ISBLANK(r.ServiceRequest.Crit_Hlth_Self__c))}">
                                            <apex:outputLabel value="Self risk to health or safety: " styleClass="bold"/>
                                            <apex:outputField value="{!r.ServiceRequest.Crit_Hlth_Self__c}"/> 
                                            <br/><br/>
                                        </apex:outputPanel>
                                        
                                        <apex:outputPanel rendered="{!NOT(ISBLANK(r.ServiceRequest.Crit_Hlth_Oth__c))}">
                                            <apex:outputLabel value="Risk Health Safety to Others: " styleClass="bold"/>
                                            <apex:outputField value="{!r.ServiceRequest.Crit_Hlth_Oth__c}"/> 
                                            <br/><br/>
                                        </apex:outputPanel>
                                        
                                        <apex:outputPanel rendered="{!NOT(ISBLANK(r.ServiceRequest.Crit_Abuse__c))}">
                                            <apex:outputLabel value="Risk of abuse/neglect of self or others: " styleClass="bold"/>
                                            <apex:outputField value="{!r.ServiceRequest.Crit_Abuse__c}"/> 
                                            <br/><br/>
                                        </apex:outputPanel>
                                        
                                        <apex:outputPanel rendered="{!NOT(ISBLANK(r.ServiceRequest.Crit_Child__c))}">
                                            <apex:outputLabel value="Individual has dependent child/children: " styleClass="bold"/>
                                            <apex:outputField value="{!r.ServiceRequest.Crit_Child__c}"/> 
                                            <br/><br/>
                                        </apex:outputPanel>
                                        
                                        <apex:outputPanel rendered="{!NOT(ISBLANK(r.ServiceRequest.Crit_Cult__c))}">
                                            <apex:outputLabel value="Disadvantage due to cultural background: " styleClass="bold"/>
                                            <apex:outputField value="{!r.ServiceRequest.Crit_Cult__c}"/> 
                                            <br/><br/>
                                        </apex:outputPanel>
                                        <apex:outputPanel rendered="{!NOT(ISBLANK(r.ServiceRequest.Crit_Deter__c))}">
                                            <apex:outputLabel value="Risk due to nature of disability: " styleClass="bold"/>
                                            <apex:outputField value="{!r.ServiceRequest.Crit_Deter__c}"/> 
                                            <br/><br/>
                                        </apex:outputPanel>
                                        
                                        <apex:outputPanel rendered="{!NOT(ISBLANK(r.ServiceRequest.Crit_Homeless_Comm__c))}">
                                            <apex:outputLabel value="Risk of homeless/inappropriate placement: " styleClass="bold"/>
                                            <apex:outputField value="{!r.ServiceRequest.Crit_Homeless_Comm__c}"/> 
                                            <br/><br/>
                                        </apex:outputPanel>
                                        
                                        <apex:outputPanel rendered="{!NOT(ISBLANK(r.ServiceRequest.Crit_Mob__c))}">
                                            <apex:outputLabel value="Mobility: " styleClass="bold"/>
                                            <apex:outputField value="{!r.ServiceRequest.Crit_Mob__c}"/> 
                                            <br/><br/>
                                        </apex:outputPanel>
                                        
                                        <apex:outputPanel rendered="{!NOT(ISBLANK(r.ServiceRequest.Crit_Net__c))}">
                                            <apex:outputLabel value="No natural carer or support network: " styleClass="bold"/>
                                            <apex:outputField value="{!r.ServiceRequest.Crit_Net__c}"/> 
                                        </apex:outputPanel>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </apex:repeat>
                    
                    <div style="page-break-before: always;"><apex:sectionHeader title="Amendments"/></div>
                    <apex:repeat value="{!Amendments}" var="r">
                        <div style="border:1px solid black;page-break-inside:avoid" class="evenRow">
                            <table class="myTbl">
                                <tr class="tableHdr" style="height:25px">
                                    <th style="width:10%">Request Details</th>
                                    <th style="width:26%;text-align:center">Allocation Change</th>
                                    <th style="width:34%">Solution Details</th>
                                    <th style="width:10%">Criteria</th>
                                    <th style="width:10%">Recommendation</th>
                                    <th style="width:10%">Status</th>
                                </tr>
                                <tr>
                                    <td>
                                        <apex:panelGrid columns="1" rowClasses="tableHdr left,dataRow left">
                                            <apex:outputLabel value="Client/Group"/>
                                            <apex:outputText value="{!r.ServiceRequest.Client__r.Name}"/>
                                            
                                            <apex:outputLabel value="Request #"/>
                                            <apex:outputText value="{!r.ServiceRequest.Name}"/>
                                            
                                            <apex:outputLabel value="Panel Date"/>
                                            <apex:outputField value="{!r.ServiceRequest.Panel_Date__c}"/>

                                            <apex:outputLabel value="Date Range"/>
                                            <apex:panelGroup ><apex:outputField value="{!r.ServiceRequest.Start_Date__c}"/>&nbsp;to&nbsp;<apex:outputField value="{!r.ServiceRequest.End_Date__c}"/></apex:panelGroup>
                                            
                                            <apex:outputLabel value="New Request Type"/>
                                            <apex:outputText value="{!r.ServiceRequest.New_Request_Type__c}"/>
                                        </apex:panelGrid>
                                    </td>
                                    <td>
                                        <!-- CALCULATE ALLOCATION CHANGE TOTALS -->
                                        <apex:variable value="{!0.0}" var="recurrentTotal"/>
                                        <apex:variable value="{!0.0}" var="onceOffTotal"/>
                                        <apex:variable value="{!0.0}" var="recurrentFYETotal"/>
                                        <apex:variable value="{!0.0}" var="onceOffFYETotal"/>
                                        <apex:repeat value="{!r.ServiceAllocations}" var="key">
                                            <apex:variable value="{!recurrentTotal + r.ServiceAllocations[key].RecurrentAlloc}" var="recurrentTotal"/>
                                            <apex:variable value="{!onceOffTotal + r.ServiceAllocations[key].OnceOffAlloc}" var="onceOffTotal"/>
                                            <apex:variable value="{!recurrentFYETotal + r.ServiceAllocations[key].RecurrentFYEAlloc}" var="recurrentFYETotal"/>
                                            <apex:variable value="{!onceOffFYETotal + r.ServiceAllocations[key].OnceOffFYEAlloc}" var="onceOffFYETotal"/>
                                        </apex:repeat>
                                        
                                        <!-- ALLOCATION CHANGE TABLE -->
                                        <apex:pageBlockTable value="{!r.ServiceAllocations}" var="key" styleClass="subTbl" captionClass="tableHdr">
                                            <apex:column value="{!r.ServiceAllocations[key].ServiceType}" headerValue="Service" footerValue="Total" footerClass="bold"/>
                                            <apex:column headerValue="Recurrent $" styleClass="right" headerClass="right" footerClass="right bold">
                                                <apex:outputText value="{0, number, currency}">
                                                    <apex:param value="{!r.ServiceAllocations[key].RecurrentAlloc}"/>
                                                </apex:outputText>
                                                
                                                <apex:facet name="footer">
                                                    <apex:outputText value="{0, number, currency}">
                                                        <apex:param value="{!recurrentTotal}"/>
                                                    </apex:outputText>
                                                </apex:facet>
                                            </apex:column>
                                            <apex:column headerValue="Once Off $" styleClass="right" headerClass="right" footerClass="right bold">
                                                <apex:outputText value="{0, number, currency}">
                                                    <apex:param value="{!r.ServiceAllocations[key].OnceOffAlloc}"/>
                                                </apex:outputText>
                                                
                                                <apex:facet name="footer">
                                                    <apex:outputText value="{0, number, currency}">
                                                        <apex:param value="{!onceOffTotal}"/>
                                                    </apex:outputText>
                                                </apex:facet>
                                            </apex:column>
                                            <apex:column headerValue="Recurrent FYE $" styleClass="right" headerClass="right" footerClass="right bold">
                                                <apex:outputText value="{0, number, currency}">
                                                    <apex:param value="{!r.ServiceAllocations[key].RecurrentFYEAlloc}"/>
                                                </apex:outputText>
                                                
                                                <apex:facet name="footer">
                                                    <apex:outputText value="{0, number, currency}">
                                                        <apex:param value="{!recurrentFYETotal}"/>
                                                    </apex:outputText>
                                                </apex:facet>
                                            </apex:column>
                                            <apex:column headerValue="Once Off FYE $" styleClass="right" headerClass="right" footerClass="right bold">
                                                <apex:outputText value="{0, number, currency}">
                                                    <apex:param value="{!r.ServiceAllocations[key].OnceOffFYEAlloc}"/>
                                                </apex:outputText>
                                                
                                                <apex:facet name="footer">
                                                    <apex:outputText value="{0, number, currency}">
                                                        <apex:param value="{!onceOffFYETotal}"/>
                                                    </apex:outputText>
                                                </apex:facet>
                                            </apex:column>
                                        </apex:pageBlockTable>
                                    </td>
                                    <td>
                                        {!r.ServiceRequest.Service_to_be_provided__c}
                                    </td>
                                    <td>
                                        <apex:dataTable value="{!r.SelectedCriteriaFieldSet}" var="a" styleClass="subTbl" rendered="{!r.SelectedCriteriaFieldSet.size > 0}">
                                            <apex:column value="{!a}" headerClass="center"/>
                                        </apex:dataTable>
                                    </td>
                                    <td>
                                        <apex:outputLabel value="{!r.ServiceRequest.Recommendation__c}"/><br/>
                                        <apex:outputField value="{!r.ServiceRequest.Recommendation_Reason__c}"/>
                                    </td>
                                    <td>
                                        <apex:outputText value="{!IF(r.ServiceRequest.Status__c = 'Pending Approval', 'Director Approved - Waiting Service Order Approval.', r.ServiceRequest.Status__c)}"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        <apex:outputLabel value="Current Situation" styleClass="bold"/><br/>
                                        <apex:outputPanel rendered="{!NOT(ISBLANK(r.ServiceRequest.Crit_Hlth_Self__c))}">
                                            <apex:outputLabel value="Self risk to health or safety: " styleClass="bold"/>
                                            <apex:outputField value="{!r.ServiceRequest.Crit_Hlth_Self__c}"/> 
                                            <br/><br/>
                                        </apex:outputPanel>
                                        
                                        <apex:outputPanel rendered="{!NOT(ISBLANK(r.ServiceRequest.Crit_Hlth_Oth__c))}">
                                            <apex:outputLabel value="Risk Health Safety to Others: " styleClass="bold"/>
                                            <apex:outputField value="{!r.ServiceRequest.Crit_Hlth_Oth__c}"/> 
                                            <br/><br/>
                                        </apex:outputPanel>
                                        
                                        <apex:outputPanel rendered="{!NOT(ISBLANK(r.ServiceRequest.Crit_Abuse__c))}">
                                            <apex:outputLabel value="Risk of abuse/neglect of self or others: " styleClass="bold"/>
                                            <apex:outputField value="{!r.ServiceRequest.Crit_Abuse__c}"/> 
                                            <br/><br/>
                                        </apex:outputPanel>
                                        
                                        <apex:outputPanel rendered="{!NOT(ISBLANK(r.ServiceRequest.Crit_Child__c))}">
                                            <apex:outputLabel value="Individual has dependent child/children: " styleClass="bold"/>
                                            <apex:outputField value="{!r.ServiceRequest.Crit_Child__c}"/> 
                                            <br/><br/>
                                        </apex:outputPanel>
                                        
                                        <apex:outputPanel rendered="{!NOT(ISBLANK(r.ServiceRequest.Crit_Cult__c))}">
                                            <apex:outputLabel value="Disadvantage due to cultural background: " styleClass="bold"/>
                                            <apex:outputField value="{!r.ServiceRequest.Crit_Cult__c}"/> 
                                            <br/><br/>
                                        </apex:outputPanel>
                                        <apex:outputPanel rendered="{!NOT(ISBLANK(r.ServiceRequest.Crit_Deter__c))}">
                                            <apex:outputLabel value="Risk due to nature of disability: " styleClass="bold"/>
                                            <apex:outputField value="{!r.ServiceRequest.Crit_Deter__c}"/> 
                                            <br/><br/>
                                        </apex:outputPanel>
                                        
                                        <apex:outputPanel rendered="{!NOT(ISBLANK(r.ServiceRequest.Crit_Homeless_Comm__c))}">
                                            <apex:outputLabel value="Risk of homeless/inappropriate placement: " styleClass="bold"/>
                                            <apex:outputField value="{!r.ServiceRequest.Crit_Homeless_Comm__c}"/> 
                                            <br/><br/>
                                        </apex:outputPanel>
                                        
                                        <apex:outputPanel rendered="{!NOT(ISBLANK(r.ServiceRequest.Crit_Mob__c))}">
                                            <apex:outputLabel value="Mobility: " styleClass="bold"/>
                                            <apex:outputField value="{!r.ServiceRequest.Crit_Mob__c}"/> 
                                            <br/><br/>
                                        </apex:outputPanel>
                                        
                                        <apex:outputPanel rendered="{!NOT(ISBLANK(r.ServiceRequest.Crit_Net__c))}">
                                            <apex:outputLabel value="No natural carer or support network: " styleClass="bold"/>
                                            <apex:outputField value="{!r.ServiceRequest.Crit_Net__c}"/> 
                                        </apex:outputPanel>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </apex:repeat>
                    
                </apex:pageBlock>
            </apex:form>
        </body>
    </html>                    
</apex:page>