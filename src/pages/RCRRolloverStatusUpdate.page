<apex:page standardController="RCR_Contract__c" extensions="RCRRolloverButtons" action="{!updateRolloverStatus}">
    <apex:includeScript value="{!$Resource.jQuery}"/>
    <style>
        .cntr {
            text-align:center;
        }
        .hdg {
            margin-top: 9px;
            margin-bottom: 0;
            color: #000;
            font-size: 1.5em;
            font-weight: normal;
        }
        .bold {
            font-weight:bold;
        }
    </style>
    <script>
        function checkAll(cb, checkboxes)
        {     
            var selector;
            var chkBoxes;
            var otherChkBox;
            
            if(cb.title == 'RevAll')
            {
                selector = 'input[id$="reviewed"]'; 
                chkBoxes = $(selector);

                for(i=0; i < chkBoxes.length; i++)
                {
                    chkBoxes[i].checked = cb.checked;
                }
            }
            else if(cb.title == 'PendingAll')
            {
                selector = 'input[id$="pending"]'; 
                chkBoxes = $(selector);

                for(i=0; i < chkBoxes.length; i++)
                {
                    chkBoxes[i].checked = cb.checked;
                }
            }
        }
        
        function toggleOption(cb, rowNum)
        {
            var selector;
            var selectAllChkBox;
            
            if(cb.title == 'Reviewed')
            {               
                if(!cb.checked)
                {
                    selector = 'input[title$="RevAll"]';
                    selectAllChkBox = $(selector);
                    selectAllChkBox[0].checked = false;
                }
            }
            else if(cb.title == 'Pending')
            {               
                if(!cb.checked)
                {
                    selector = 'input[title$="PendingAll"]';
                    selectAllChkBox = $(selector);
                    selectAllChkBox[0].checked = false;
                }
            }
        }
    </script>
    
    <apex:pageMessages id="msgs"/>

    <apex:form rendered="{!Action = 'coordinatorReview'}">
        <apex:sectionHeader title="Rollover Status Update"/>
        <apex:pageBlock >
            <apex:pageBlockSection columns="1">
                <apex:outputPanel >
                    <apex:outputLabel value="Client/Group: " styleClass="hdg bold"/>
                    <apex:outputLabel styleClass="hdg">
                        <apex:outputLink value="/{!ClientRollovers[0].Rollover.Client__c}" target="Client" >{!ClientRollovers[0].Rollover.Client__r.Name}</apex:outputLink>
                    </apex:outputLabel>
                    <apex:outputLabel value="{!'(' + ClientRollovers[0].Rollover.Client__r.CCMS_ID__c + ')'}" 
                                      styleClass="hdg"
                                      rendered="{!NOT(ISBLANK(ClientRollovers[0].Rollover.Client__r.CCMS_ID__c))}"/>
                </apex:outputPanel>

                
                <apex:pageBlockTable value="{!ClientRollovers}" var="r">
                    <apex:column headerClass="cntr" styleClass="cntr">
                        <apex:facet name="header">
                            Reviewed by Brokerage
                            <br/>
                            <input type="checkbox" title="RevAll" onclick="checkAll(this, 'RevAll');" immediate="true"></input>
                        </apex:facet>
                        <apex:inputCheckbox id="reviewed" 
                                            title="Reviewed" 
                                            value="{!r.BrokerageReviewed}" 
                                            onclick="toggleOption(this, this.parentNode.parentNode.rowIndex);"
                                            rendered="{!r.DisplayCheckboxes}"/>
                    </apex:column>
                    <apex:column headerClass="cntr" styleClass="cntr">
                        <apex:facet name="header">
                            Assign to Coordinator
                            <br/>
                            <input type="checkbox" title="PendingAll" onclick="checkAll(this, 'PendingAll');" immediate="true"></input>
                        </apex:facet>
                        <apex:inputCheckbox id="pending" 
                                            title="Pending" 
                                            value="{!r.PendingCoordinator}" 
                                            onclick="toggleOption(this, this.parentNode.parentNode.rowIndex);"
                                            rendered="{!r.DisplayCheckboxes}"/>
                    </apex:column>
                    <apex:column headerValue="Rollover Request">
                        <apex:outputLink value="/{!r.Rollover.Id}" target="Client" >{!r.Rollover.Name}</apex:outputLink>
                    </apex:column>
                    <apex:column value="{!r.Rollover.Rollover_Status__c}"/>
                    <apex:column value="{!r.Rollover.Account__r.Name}"/>
                    <apex:column value="{!r.Rollover.Funding_Source__c}"/>
                    <apex:column value="{!r.Rollover.Start_Date__c}"/>
                    <apex:column value="{!r.Rollover.End_Date__c}"/>
                    <apex:column value="{!r.Rollover.Allocation_Total_Approved__c}"/>
                    <apex:column value="{!r.Rollover.Allocation_Total_Not_Approved__c}"/>
                    <apex:column value="{!r.Rollover.Total_Service_Order_Cost__c}"/>
                </apex:pageBlockTable>
            </apex:pageBlockSection>
            <apex:commandButton value="Submit for Review" action="{!submitForReview}"/>
            <apex:commandButton value="Cancel" action="{!cancelOverride}"/>
        </apex:pageBlock>
    </apex:form>
</apex:page>