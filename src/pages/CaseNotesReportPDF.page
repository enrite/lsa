<apex:page standardController="Referred_Client__c" extensions="CaseNotesReportPDFExtension" renderAs="pdf" showHeader="false" sidebar="false" applyHtmlTag="false" applyBodyTag="false" readOnly="true">
    <apex:variable value="{!Referred_Client__c}" var="c" />
    <head>
        <style>                   
            @page
            {
                size: A4 landscape;
                margin-top: 2cm;
                margin-bottom: 1cm;                
                margin-left: 2cm;
                margin-right: 2cm;               
                
                @top-center 
                {                   
                    content: element(header);
                }
                
                @bottom-center
                {                   
                    content: element(footer);
                }
            }
            
            div.header 
            {    
                font-family: Sans-Serif;
                font-size: 12pt;                                                                                          
                position: running(header);                           
            }
            
            div.footer 
            {  
                font-family: Sans-Serif;                              
                font-size: 8pt;                                                
                position: running(footer);          
            } 
            
            .pagenumber:before 
            {                
                content: counter(page);             
            }                       
            
            .pagecount:before 
            {             
                content: counter(pages);            
            }                         
                        
            body
            {       
                font-family: Sans-Serif;         
                font-size: 9pt;               
            } 
            
            table, td, th
            {
                border-collapse: collapse;
                padding: 0px;     
            } 
            
            .noteTbl
            {
                width: 100%;
                table-layout: fixed;
                white-space: pre-line;
            }
            
            .noteTbl td, .noteTbl th
            {
                padding: 2px;
                vertical-align: top;
                text-align: left;
            }

            .noteTbl td
            {
                word-wrap: break-all;        
            }
        </style>                
    </head>
    
    <div class="header">
        <div style="width: 100%; text-align: center; border: solid 1px white;" >
            <b>LSA Participant {!IF(ShowNotes, 'Case Notes ', '')}{!IF(ShowNotes && ShowEmails, 'and ', '')}{!IF(ShowEmails, 'Emails ', '')}Report</b><br />
            Participant: {!c.Name}<br />
            <apex:outputPanel layout="none" rendered="{!NOT(ISBLANK(FromDate) && ISBLANK(ToDate))}">
                Date Range:&nbsp;<apex:outputText value="{0,date,dd/MM/yyyy}" rendered="{!NOT(ISBLANK(FromDate))}" ><apex:param value="{!FromDate}" /></apex:outputText>{!IF(ISBLANK(FromDate), 'Participant start date', '')}&nbsp;to&nbsp;<apex:outputText value="{0,date,dd/MM/yyyy}" rendered="{!NOT(ISBLANK(ToDate))}" ><apex:param value="{!ToDate}" /></apex:outputText>{!IF(ISBLANK(ToDate), 'today', '')} 
            </apex:outputPanel>
        </div>
    </div>  
          
    <div class="footer">                 
        <table style="width: 100%;">
            <tr>
                <td style="width: 33%; text-align: left;">Print date:&nbsp;<apex:outputText value="{0,date,dd/MM/yyyy}" ><apex:param value="{!TODAY()}" /></apex:outputText></td>
                <td style="width: 33%; text-align: center;">Produced by: {!$User.FirstName} {!$User.LastName}</td>
                <td style="width: 33%; text-align: right;">Page <span class="pagenumber"/> of <span class="pagecount"/></td>
            </tr>
        </table>    
    </div>
    
    <body>
        <div style="font-size: 14pt; font-weight: bold;">
            Case Notes And Emails
        </div>
        <br /> 
        <apex:outputPanel layout="none" rendered="{!ReportRows.size == 0}">  
            No records found
        </apex:outputPanel>
        <apex:outputPanel layout="none" rendered="{!ReportRows.size > 0}">                     
            <table class="noteTbl">
                <tr>
                    <th style="width:10%">Date</th>
                    <th style="width:10%">Type</th>
                    <th style="width:20%">Resulting From/Received From</th>
                    <th>Category/Subject</th>
                    <th>Relating to Contact</th>
                    <th style="width:20%">Attachments</th>
                </tr>    
                <apex:repeat value="{!ReportRows}" var="r">
                    <tr><td colspan="6"><hr /></td></tr>
                    <tr>
                        <td>
                            <apex:outputText value="{0,date,dd/MM/yyyy}" >
                                <apex:param value="{!IF(ISBLANK(r.Email), r.Note.Note_Date__c, r.Email.Email_Date__c)}" />
                            </apex:outputText>
                        </td>
                        <td>
                            <apex:outputText value="{!IF(ISBLANK(r.Email), 'Case Note', 'Email')}"/>
                        </td>
                        <td>
                            <apex:outputText value="{!IF(ISBLANK(r.Email), r.Note.Resulting_From__c, r.Email.Received_From__c)}"/>
                        </td>
                        <td>
                            <apex:outputText value="{!IF(ISBLANK(r.Email), SUBSTITUTE(r.Note.Category__c, ';', '<br />'), r.Email.Email_Subject__c)}" 
                                    escape="false" />
                        </td>
                        <td>
                            <apex:outputText value="{!IF(NOT(ISBLANK(r.Note)), r.Note.Relating_to_Contact__r.First_Name__c + ' ' + r.Note.Relating_to_Contact__r.Surname__c, 'N/A')}"/>
                        </td>
                        <td>
                            <apex:repeat value="{!r.Note.Attachments}" var="att">
                                <apex:outputText value="{!att.Name}<br/>" escape="false" />
                            </apex:repeat>
                        </td>
                    </tr>  
                    <tr>
                        <td>
                            <apex:outputText value="Note/Body" /> 
                        </td>
                        <td colspan="5">
                            <apex:outputText value="{!IF(ISBLANK(r.Email), r.Note.Note__c, r.Email.Email_Body__c)}"
                                    escape="false" />   
                        </td>   
                    </tr>                  
                </apex:repeat>            
            </table>
        </apex:outputPanel>
    </body>    
</apex:page>