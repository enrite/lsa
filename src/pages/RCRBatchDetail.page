<apex:page standardController="RCR_Invoice_Batch__c" extensions="RCRInvoiceBatchController" tabStyle="RCR__tab">
     <apex:stylesheet value="{!URLFOR($Resource.A2HCStyle)}" />
    <script>
        if (window != top)
        {
            top.location.href = location.href;
        }
    </script>
    <apex:variable value="{!RCR_Invoice_Batch__c}" var="b"/>
    <apex:form >
        <apex:pageBlock >
            <apex:sectionHeader title="Activity Statement Batch"/>
            <apex:pageBlockSection columns="3">
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Batch Number"/>
                    <apex:outputText value="{!TEXT(b.Batch_Number__c)}" />
                </apex:pageBlockSectionItem>
               <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Sent Date"/>
                    <apex:outputField value="{!b.Sent_Date__c}"/>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem /> 
            </apex:pageBlockSection>
            <apex:pageBlockSection columns="1">    
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Description"/>
                    <apex:outputField value="{!b.Description__c}"/>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            <apex:pageBlockSection columns="3">                    
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Total ex GST"/>
                    <apex:outputField value="{!b.Batch_Total_ex_GST__c}" />
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="GST"/>
                    <apex:outputField value="{!b.Batch_Total_GST__c}" />
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Total inc GST"/>
                    <apex:outputField value="{!b.Batch_Total_inc_GST__c}" />
                </apex:pageBlockSectionItem>                
            </apex:pageBlockSection>
            <apex:pageBlockSection columns="1" rendered="{!NOT(ISBLANK(b.CBMS_Error__c))}">     
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="CBMS Error"/>
                    <apex:outputField value="{!b.CBMS_Error__c}"/>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            <div>
                <apex:commandButton value="Cancel" styleClass="saveBtn btnRow" immediate="true" action="{!cancelOverride}" />
            </div>
            <apex:pageBlockSection columns="1" title="Activity Statements">
                <apex:pageBlockTable value="{!BatchInvoices}" var="i">
                    <apex:column headerValue="Statement Number">
                        <apex:outputLink value="{!'../' + i.Id}">{!i.Invoice_Number__c}</apex:outputLink>
                    </apex:column>
                    <apex:column value="{!i.All_Line_Count__c}"/>
                    <apex:column value="{!i.Lines_Not_Reconciled__c}"/>
                    <apex:column value="{!i.Start_Date__c}"/>
                    <apex:column value="{!i.End_Date__c}"/>
                    <apex:column value="{!i.Date__c}"/>
                    <apex:column value="{!i.Status__c}" footerValue="Total" footerClass="rightAlign"/>
                    <apex:column value="{!i.Total_Reconciled_ex_GST__c}"
                                 headerClass="rightAlign"
                                 styleClass="rightAlign"                            
                                 footerClass="rightAlign">
                        <apex:facet name="header">Approved ex GST</apex:facet>
                        <apex:facet name="footer">
                            <apex:outputText value="{!TotalexGST}" />
                        </apex:facet>
                    </apex:column>
                    <apex:column value="{!i.Total_GST_Reconciled__c}"
                                 headerClass="rightAlign"
                                 styleClass="rightAlign"                            
                                 footerClass="rightAlign">
                        <apex:facet name="header">Approved GST</apex:facet>
                        <apex:facet name="footer">
                            <apex:outputText value="{!TotalGST}" />
                        </apex:facet>
                    </apex:column>                                        
                    <apex:column value="{!i.Total_Reconciled__c}"
                                 headerClass="rightAlign"
                                 styleClass="rightAlign"                            
                                 footerClass="rightAlign">
                        <apex:facet name="header">Approved inc GST</apex:facet>
                        <apex:facet name="footer">
                            {!TotalincGST}
                        </apex:facet>
                    </apex:column>  
                    <apex:column headerValue="Rejected inc GST" 
                            headerClass="rightAlign" 
                            styleClass="rightAlign"                                                        
                            footerClass="rightAlign">
                        <apex:facet name="footer">
                            {!TotalFailed}
                        </apex:facet> 
                        <apex:outputText value="{0, number, currency}">
                            <apex:param value="{!i.Total_Failed_Reconciliation__c}"/>
                        </apex:outputText>
                    </apex:column>   
                </apex:pageBlockTable>
            </apex:pageBlockSection>
        </apex:pageBlock>
    </apex:form>
    <apex:relatedList list="RCR_Service_Order_Payments__r" />
    <c:ObjectHistoryList Record="{!b}" />
    <!-- <apex:relatedList list="RCR_MP_Service_Order_Payments__r" />-->
</apex:page>