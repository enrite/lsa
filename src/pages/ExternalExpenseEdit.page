<apex:page standardController="External_Expense__c" extensions="ExternalExpenseEditExtension"
           lightningStylesheets="true"
           title="{!$ObjectType.External_Expense__c.Label}: {!BLANKVALUE(External_Expense__c.Name, 'New ' + $ObjectType.External_Expense__c.Label)}">
    <apex:variable value="{!External_Expense__c}" var="e" />
    <apex:sectionHeader title="{!$ObjectType.External_Expense__c.Label} Edit" subtitle="{!BLANKVALUE(e.Name, 'New ' + $ObjectType.External_Expense__c.Label)}" />
    <apex:pageMessages id="msg" />
    <c:WaitNotify />
    <apex:actionStatus onstart="showWaitNotify();" onstop="hideWaitNotify();"  id="busy" />
    <apex:form >
    <apex:pageBlock mode="edit" title="{!$ObjectType.External_Expense__c.Label} Edit">
        <apex:pageBlockButtons >
            <apex:commandButton value="Save" action="{!SaveOverride}" />            
            <apex:commandButton value="Add Line Items" action="{!AddLineItems}" reRender="items,msg" status="busy" />
            <apex:commandbutton value="Upload Expenses" action="{!URLFOR($Page.ExternalExpenseUpload)}" immediate="true" />
            <apex:commandButton value="Cancel" action="{!Cancel}" immediate="true" />
        </apex:pageBlockButtons>
        <apex:pageBlockSection columns="1">
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="Owner" />
                <apex:outputPanel layout="block" styleClass="requiredInput">
                    <apex:outputPanel layout="block" styleClass="requiredBlock" />  
                    <apex:inputField value="{!e.OwnerId}" id="own" required="false" >
                        <script>
                            document.getElementById('{!$Component.own}_mlktp').style.display = 'none';
                        </script>
                    </apex:inputField>  
                </apex:outputPanel>
            </apex:pageBlockSectionItem>            
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="Month" />
                <apex:outputPanel >
                    <apex:outputPanel layout="block" styleClass="requiredInput" >
                        <apex:outputPanel layout="block" styleClass="requiredBlock" />    
                        <apex:inputField value="{!e.Month__c}" />
                    </apex:outputPanel>                    
                </apex:outputPanel>
            </apex:pageBlockSectionItem>            
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="Year" />
                <apex:outputPanel >
                    <apex:outputPanel layout="block" styleClass="requiredInput" >
                        <apex:outputPanel layout="block" styleClass="requiredBlock" />    
                        <apex:selectList value="{!e.Year__c}" size="1">
                            <apex:selectOptions value="{!Years}" />
                        </apex:selectList>
                    </apex:outputPanel>                    
                </apex:outputPanel>
            </apex:pageBlockSectionItem>           
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="Account" />
                <apex:outputPanel >
                    <apex:outputPanel layout="block" styleClass="requiredInput">
                        <apex:outputPanel layout="block" styleClass="requiredBlock" />                          
                        <apex:inputField value="{!External_Expense__c.Account__c}" >
                            <apex:actionSupport event="onchange" action="{!FindGLCodes}" reRender="items,msg" status="busy" />
                        </apex:inputField>                                                
                    </apex:outputPanel>                    
                </apex:outputPanel>
            </apex:pageBlockSectionItem>                 
        </apex:pageBlockSection>   
        <div class="pbSubheader brandTertiaryBgr tertiaryPalette">
            <h3>RCR External Expense Line Items</h3>
        </div> 
        <apex:outputPanel id="items"> 
            <apex:pageBlockSection columns="1">           
                <apex:pageBlockTable value="{!Keys}" var="k">
                    <apex:column styleClass="actionColumn">
                        <apex:commandLink value="Remove" action="{!RemoveLineItem}" rendered="{!ISBLANK(LineItems[k].Id)}" styleClass="actionLink" onclick="if (!confirm('Are you sure?')) { return false; }" reRender="items,msg" status="busy"  >                    
                            <apex:param assignTo="{!ItemKey}" value="{!k}" name="ItemKey" />
                        </apex:commandLink>
                    </apex:column>
                    <apex:column headerValue="Participant">                        
                        <apex:inputField value="{!LineItems[k].Participant__c}" >                        
                            <apex:actionSupport event="onchange" rerender="gl" />
                        </apex:inputField>
                    </apex:column>
                    <apex:column headerValue="Date of Transaction">
                        <apex:outputPanel layout="block" styleClass="requiredInput" >
                            <apex:outputPanel layout="block" styleClass="requiredBlock" />
                            <apex:inputField value="{!LineItems[k].Date_of_Transaction__c}" /> 
                        </apex:outputPanel>   
                    </apex:column>
                    <apex:column headerValue="Date of Payment">
                        <apex:outputPanel layout="block" styleClass="requiredInput" >
                            <apex:outputPanel layout="block" styleClass="requiredBlock" />
                            <apex:inputField value="{!LineItems[k].Date_of_Payment__c}" />
                        </apex:outputPanel>
                    </apex:column>
                    <apex:column headerValue="Supplier">                        
                        <apex:inputField value="{!LineItems[k].Supplier__c}" />                         
                    </apex:column>
                    <apex:column headerValue="Expense Description">
                        <apex:outputPanel layout="block" styleClass="requiredInput" >
                            <apex:outputPanel layout="block" styleClass="requiredBlock" />
                            <apex:inputField value="{!LineItems[k].Expense_Description__c}" /> 
                        </apex:outputPanel>  
                    </apex:column>
                    <apex:column headerValue="GL Code">
                        <apex:outputPanel id="gl">
                            <apex:outputPanel layout="block" styleClass="requiredInput" rendered="{!NOT(ISBLANK(LineItems[k].Participant__c))}" >
                                <apex:outputPanel layout="block" styleClass="requiredBlock" />
                                <apex:selectList value="{!LineItems[k].GL_Code__c}" size="1"> 
                                    <apex:selectOptions value="{!GLCodes}" />
                                </apex:selectList>
                            </apex:outputPanel> 
                        </apex:outputPanel> 
                    </apex:column>
                    <apex:column headerValue="Plan Action">
                        <apex:outputPanel id="pa">
                            <apex:outputPanel layout="block" styleClass="requiredInput" rendered="{!NOT(ISBLANK(LineItems[k].Participant__c))}" >
                                <apex:outputPanel layout="block" styleClass="requiredBlock" />
                                <apex:inputField value="{!LineItems[k].Plan_Action__c}" />
                            </apex:outputPanel>
                        </apex:outputPanel>
                    </apex:column>
                    <apex:column headerValue="Amount" width="90px"> 
                        <apex:outputPanel layout="block" styleClass="requiredInput" >
                            <apex:outputPanel layout="block" styleClass="requiredBlock" />
                            <apex:inputField value="{!LineItems[k].Amount__c}" style="width: 70px;" onchange="valueChanged = true;" /> 
                        </apex:outputPanel>                                           
                    </apex:column>
                    <apex:column headerValue="GST" width="90px"> 
                        <apex:outputPanel layout="block" styleClass="requiredInput" >
                            <apex:outputPanel layout="block" styleClass="requiredBlock" />
                            <apex:inputField value="{!LineItems[k].GST__c}" style="width: 70px;" onchange="valueChanged = true;" />                              
                        </apex:outputPanel>                                                                                                     
                    </apex:column>
                    <apex:column headerValue="Total Amount" width="90px">
                        <b><apex:outputText value="{0,number,$#,##0.00}" id="coltot"><apex:param value="{!BLANKVALUE(LineItems[k].Amount__c, 0) + BLANKVALUE(LineItems[k].GST__c, 0)}" /></apex:outputText></b>
                    </apex:column>                    
                </apex:pageBlockTable>                               
            </apex:pageBlockSection>
            <apex:outputPanel id="tot">
                <table style="width: 100%;">
                    <tr>
                        <td style="text-align: right; padding-right: 30px;"><b>Totals</b></td>
                        <td style="width: 95px;"><apex:outputText value="{0,number,$#,##0.00}"><apex:param value="{!Totals.Amount__c}" /></apex:outputText></td>
                        <td style="width: 95px;"><apex:outputText value="{0,number,$#,##0.00}"><apex:param value="{!Totals.GST__c}" /></apex:outputText></td>
                        <td style="width: 100px;"><b><apex:outputText value="{0,number,$#,##0.00}"><apex:param value="{!Totals.Amount__c + Totals.GST__c}" /></apex:outputText></b></td>
                    </tr>    
                </table>            
            </apex:outputPanel>
        </apex:outputPanel>
    </apex:pageBlock>
    <apex:actionFunction name="calculateTotals" reRender="coltot,tot" />
    <script>
        // this will see if the total needs to be recalculated every 500ms
        var valueChanged = false;
        setTimeout('triggerCalculation();', 500);
        
        function triggerCalculation()
        {
            if (valueChanged)
            {
                valueChanged = false;
                calculateTotals();                
            }
            
            setTimeout('triggerCalculation();', 500);
        }                
    </script>
    </apex:form>
</apex:page>