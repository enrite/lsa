<apex:page standardcontroller="Referred_Client__c" extensions="RCRCompensableClientPurchasedPDF" renderAs="pdf" cache="false" expires="0">
    <!-- STYLE VARIABLES -->
    <apex:variable value="width:100%;" var="addressTable"/>
    <apex:variable value="width:100%;border:solid black 1px;border-collapse:collapse;" var="detailsTable"/>
    <apex:variable value="vertical-align:top;" var="top"/>
    <apex:variable value="text-align:right;height:85px;width:100%;background-color:#002469;margin-bottom:10px;padding-top:50px;" var="bnr1"/>
    <apex:variable value="height:85px;font-size:2em;float:left;padding-left:100px;color:white;" var="bnr1a"/>
    <apex:variable value="text-align:center;font-size:2em;height:55px;width:100%;background-color:#f9df78;padding-top:20px;color:#0d47b6;" var="bnr2"/>
    <apex:variable value="text-align:center;width:100%;font-size:1em;padding-top:20px;color:#0d47b6;" var="hdgLvl1"/>
    <apex:variable value="font-size:1em;font-weight:bold;color:#0d47b6;" var="hdgLvl2"/>
    <apex:variable value="font-size:0.75em;font-weight:bold;color:#0d47b6;" var="hdLvl2"/>
    <apex:variable value="font-size:0.75em;font-weight:bold;" var="dLvl2"/>
    <apex:variable value="text-align:right;" var="right"/>
    <apex:variable value="text-align:center;" var="center"/>
    <apex:variable value="border: solid black 1px;" var="cellBorder"/>
 
<apex:variable value="{!Referred_Client__c}" var="Client"/>  
 
<html>
<head>
    <style type="text/css">
        body {
            font-family:arial,sans-serif;
            font-size:12pt;
        }
        @page {
            margin-top: 10mm;
            margin-bottom: 25mm;
            @bottom-center {
                content: element(footer);
            } 
        }
        div.footer 
        {                
            font-family: Sans-Serif; 
            font-size: 12pt;
            color:#0d47b6;
            display: block;             
            padding: 5px;               
            padding-bottom: 1cm;
            position: running(footer);          
        }
        
        .headerClass
        {
            font-size:1em;font-weight:bold;color:#0d47b6;
        }
    </style>
</head>
<body>

            
            <div style="{!bnr1}">
                <div style="{!bnr1a}">Disability SA</div>
                <apex:image alt="DCSI Logo" url="{!$Resource.DCSILogoForPDF}" height="55px" style="margin-right:10px"/>
            </div>
            <div style="{!bnr2}">Compensable Client - Purchased Services</div>
            <br/>
            <table style="width:100%">
                <tr>
                    <td style="{!hdgLvl2};width:25%">Re Client:</td>
                    <td>
                        <apex:outputField value="{!Client.Title__c}"/>.&nbsp;<apex:outputField value="{!Client.Name}"/>
                    </td>
                </tr>
                <tr>
                    <td style="{!hdgLvl2}">DOB:</td>
                    <td>
                        <apex:outputField value="{!Client.Date_Of_Birth__c}"/>
                    </td>
                </tr>
                <tr>
                    <td style="{!hdgLvl2}">Activity Date Range:</td>
                    <td>
                        <apex:outputText value="{!StartDate} - {!EndDate}"/>
                    </td>
                </tr>
            </table>
            <br/>    
            <br/>
            
            <apex:repeat value="{!PurchasedServices}" var="all"  > 
            <apex:pageBlock rendered="{!all != null}">
            <div><span style="{!hdgLvl2}">Service Provider: </span>{!all.Provider}</div><br />
                <table style="{!detailsTable}">
                    <tr>
                        <td style="{!hdLvl2} {!center} {!cellBorder} width:10%;">Invoice Date</td>
                        <td style="{!hdLvl2} {!center} {!cellBorder} width:11%;" >Invoice Number</td>
                        <td style="{!hdLvl2} {!center} {!cellBorder} width:10%;">For Services Between</td>
                        <td style="{!hdLvl2} {!center} {!cellBorder} width:10%;">Date Payment Processed</td>
                        <td style="{!hdLvl2} {!center} {!cellBorder} width:12%;">Amount</td>
                        <td style="{!hdLvl2} {!center} {!cellBorder} width:12%;">GST</td>
                        <td style="{!hdLvl2} {!center} {!cellBorder} width:12%;">Total Paid</td>
                        <td style="{!hdLvl2} {!center} {!cellBorder} width:13%;">Batch Id</td>
                        <td style="{!hdLvl2} {!center} {!cellBorder} width:10%;">CBMS Batch</td>
                    </tr>
                    <apex:repeat value="{!PurchasedServiceDetails[all.Provider]}" var="p">
                        <tr style="{!cellBorder}"> 
                            <td style="{!dLvl2} {!center} {!cellBorder}">{!p.InvoiceDate}</td>
                            <td style="{!dLvl2} {!center} {!cellBorder}">{!p.InvoiceNumber}</td>
                            <td style="{!dLvl2} {!center} {!cellBorder}">{!p.InvoiceStartDate} - {!p.InvoiceEndDate}</td>
                            <td style="{!dLvl2} {!center} {!cellBorder}">{!p.BatchSentDate}</td>
                            <td style="{!dLvl2} {!right} {!cellBorder}">
                                <apex:outputText value="{0, number, currency}">
                                    <apex:param value="{!p.TotalExGST}"/>
                                </apex:outputText>
                            </td>
                            <td style="{!dLvl2} {!right} {!cellBorder}">
                                <apex:outputText value="{0, number, currency}">
                                    <apex:param value="{!p.TotalGST}"/>
                                </apex:outputText>
                            </td>
                            <td style="{!dLvl2} {!right} {!cellBorder}">
                                <apex:outputText value="{0, number, currency}">
                                    <apex:param value="{!p.TotalIncGST}"/>
                                </apex:outputText>
                            </td>
                            <td style="{!dLvl2} {!center} {!cellBorder}">{!p.BatchId}</td>
                            <td style="{!dLvl2} {!center} {!cellBorder}">{!p.CBMSBatchNo}</td>
                            </tr>
                    </apex:repeat>
                </table>
                <table style="{!detailsTable}">
                    <apex:repeat value="{!PurchasedServiceTotals[all.Provider]}" var="pst">
                    <tr>
                        <td  style="width:41%; {!hdLvl2} {!right}">This period total - {!all.Provider}:</td>
                        <td style="width:12%; {!hdLvl2} {!right}"><apex:outputText value="{0, number, currency}">
                                <apex:param value="{!pst.ServiceGrandTotalExGST}"/>
                            </apex:outputText></td>
                        <td style="width:12%; {!hdLvl2} {!right}"><apex:outputText value="{0, number, currency}">
                                <apex:param value="{!pst.ServiceGrandTotalGST}"/>
                            </apex:outputText></td>
                        <td style="width:12%; {!hdLvl2} {!right}"><apex:outputText value="{0, number, currency}">
                                <apex:param value="{!pst.ServiceGrandTotalIncGST}"/>
                            </apex:outputText></td>
                           <td style="width:13%;"></td>
                           <td style="width:10%;"></td>
                    </tr>
                    </apex:repeat>      
                </table>
            </apex:pageBlock>
            <br/>
            <br/>
            </apex:repeat>
            <table style="{!detailsTable}"> 
                <tr>
                   <td style="width:41%; {!hdLvl2} {!right}">Total services purchased for {!Client.Name}:</td>
                       <td style="width:12%; {!hdLvl2} {!right}"><apex:outputText value="{0, number, currency}">
                               <apex:param value="{!GrandTotalExGST}"/>
                           </apex:outputText></td>
                       <td style="width:12%; {!hdLvl2} {!right}"><apex:outputText value="{0, number, currency}">
                               <apex:param value="{!GrandTotalGST}"/>
                           </apex:outputText></td>
                       <td style="width:12%; {!hdLvl2} {!right}"><apex:outputText value="{0, number, currency}">
                               <apex:param value="{!GrandTotalIncGST}"/>
                           </apex:outputText></td>
                           <td style="width:13%;"></td>
                           <td style="width:10%;"></td>
                   </tr>               
            </table>  
    </body>
</html>
</apex:page>