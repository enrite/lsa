public class RCRDirectorApprovalController extends A2HCPageBase 
{
    public RCRDirectorApprovalController()
    {      
        //getRequests(true);
    }
    
    transient List<RequestWrapper> tRequests;
    transient List<RequestWrapper> tAmendments;
    transient integer approveCount = 0;
    transient integer rejectCount = 0;
    transient integer errorCount = 0;
    
    @TestVisible
    private  Map<Id, ProcessInstanceWorkitem> PendingApprovalWorkItems
    {
    	get
    	{
    		if(PendingApprovalWorkItems == null)
    		{
    			PendingApprovalWorkItems = new Map<Id, ProcessInstanceWorkitem>();
    		}
    		return PendingApprovalWorkItems;
    	}
    	private set;
    }
    
    public List<SelectOption> ApprovalDecisionOpts
    {
    	get
    	{
    		if(ApprovalDecisionOpts == null)
    		{
    			ApprovalDecisionOpts = new List<SelectOption>();
    			ApprovalDecisionOpts.add(new SelectOption('', ''));
    			ApprovalDecisionOpts.add(new SelectOption('Pending', 'Pending'));
    			ApprovalDecisionOpts.add(new SelectOption('Approved', 'Approved'));
    			ApprovalDecisionOpts.add(new SelectOption('Rejected', 'Rejected')); 
    		}
    		return ApprovalDecisionOpts;
    	}
    	private set;
    }
    
    @TestVisible   
    public List<RequestWrapper> Requests
    {
        get
        {
            if(tRequests == null)
            {
                tRequests = new List<RequestWrapper>();
                getRequests(APSRecordTypes.RCRServiceOrder_NewRequest);
            }
            return tRequests;
        }
        private set
        {
            tRequests = value;
        }
    }
  	
  	@TestVisible
  	public List<RequestWrapper> Amendments
    {
        get
        {
            if(tAmendments == null)
            {
                tAmendments = new List<RequestWrapper>();
            }
            return tAmendments;
        }
        private set
        {
            tAmendments = value;
        }
    }
    
    public string StartDt
    {
    	get;
    	set;
    }
    
    public string EndDt
    {
    	get;
    	set;
    }
    
    public void getRequests(String recordType)
    {
        try 
        {          
        	throwTestError();
            date startDate = Date.parse(getParameter('startDate'));
            date endDate = Date.parse(getParameter('endDate'));
            
            StartDt = startDate.format();
            EndDt = endDate.format();
            
            List<RCR_Contract__c> soRequests =  [SELECT   Id,
                                                    Name,
                                                    Client__c,
                                                    Client__r.Name,
                                                    Crit_Hlth_Self__c,
                                                    Crit_Hlth_Oth__c,
                                                    Crit_Abuse__c,
                                                    Crit_Child__c,
                                                    Crit_Cult__c,
                                                    Crit_Deter__c,
                                                    Crit_Homeless__c,
                                                    Crit_Risk_Homeless__c,
                                                    Crit_FamSA__c,
                                                    Crit_Novita__c,
                                                    Crit_Transfer__c,
                                                    Crit_Homeless_Comm__c,
                                                    Crit_Mob__c,
                                                    Crit_Net__c,
                                                    Status__c,
                                                    Panel_Date__c,
                                                    Recommendation__c,
                                                    Recommendation_Reason__c,
                                                    Start_Date__c,
                                                    End_Date__c,
                                                    Service_to_be_provided__c,
                                                    Approval_Process_Step__c,
                                                    RecordType.Name,
                                                    New_Request_Type__c,
                                                    (SELECT id,
                                                            Once_Off__c,
                                                            Service_Type__c,
                                                            Total__c,
                                                            Quantity__c,
                                                            Unit_of_Measure__c
                                                     FROM   RCR_Service_Order_Rate_Calculations__r)
//                                                    (SELECT Id,
//                                                            Name,
//                                                            Service_Type__c,
//                                                            Financial_Year__c,
//                                                            Allocation_Type__c,
//                                                            Amount__c,
//                                                            FYE_Amount__c
//                                                     FROM   IF_Allocations__r)
                                           FROM     RCR_Contract__c
                                           WHERE    (RecordType.Name = :APSRecordTypes.RCRServiceOrder_NewRequest OR 
                                           			 RecordType.Name = :APSRecordTypes.RCRServiceOrder_Amendment)
                                           AND      Panel_Date__c >= :startDate AND Panel_Date__c <= :endDate
                                           AND		Panel_Date__c != null
                                           ORDER BY Director_Report_Sort_Order__c asc, Panel_Date__c asc];
                                           

            for(ProcessInstanceWorkitem workItem : [SELECT 	Id,
                                                       		ProcessInstance.TargetObjectId
                                                    FROM   	ProcessInstanceWorkitem
                                                    WHERE   ProcessInstance.TargetObjectId IN :soRequests])
            {
                PendingApprovalWorkItems.put(workItem.ProcessInstance.TargetObjectId, workItem);
            }
         	
         	Schema.FieldSet fieldSetDescribe = sObjectType.RCR_Contract__c.fieldSets.Criteria;
            for(RCR_Contract__c so : soRequests)
            {
                if(so.RecordType.Name == APSRecordTypes.RCRServiceOrder_NewRequest)
                {
                	Requests.add(new RequestWrapper(so, PendingApprovalWorkItems.get(so.Id), fieldSetDescribe));
                }
                else if(so.RecordType.Name == APSRecordTypes.RCRServiceOrder_Amendment)
                {
                	Amendments.add(new RequestWrapper(so, PendingApprovalWorkItems.get(so.Id), fieldSetDescribe));
                }
            }              
        }
        catch(Exception ex)
        {
            A2HCException.formatException(ex);
        }
    }
    
    
    public void ApproveReject()
    {
    	try
    	{
    		throwTestError();
    		List<Approval.ProcessWorkitemRequest> processWorkRequests = new List<Approval.ProcessWorkitemRequest>();    		
    		Approval.ProcessWorkitemRequest pwir;
    		for(RequestWrapper d : Requests)
			{           				
				if(PendingApprovalWorkItems.containsKey(d.ServiceRequest.Id))
				{
					pwir = getWorkItemRequest(d);
					if(pwir == null)
					{
						continue;
					}
					processWorkRequests.add(pwir);
				}
			}
			
			for(RequestWrapper d : Amendments)
			{           				
				if(PendingApprovalWorkItems.containsKey(d.ServiceRequest.Id))
				{
					pwir = getWorkItemRequest(d);
					if(pwir == null)
					{
						continue;
					}
					processWorkRequests.add(pwir);
				}
			} 	  	

    		Approval.ProcessResult[] results = Approval.process(processWorkRequests, false);    		
    		for(Approval.ProcessResult result : results)
    		{
    			if(!result.isSuccess())
    			{
    				errorCount++;
    			}
    		}    		
    		A2HCException.formatMessage('There were ' + approveCount + 
    										' Service Requests approved and ' + rejectCount + 
    										' rejected. There were ' + errorCount + 
    										' errors during this approval process.');
    		Requests = null;
    	}
    	catch(Exception ex)
    	{
    		A2HCException.formatException(ex);
    	}
    }
    
    public PageReference printReport()
    {
        try
        {
        	throwTestError();
            PageReference pg = Page.RCR;
            pg.getParameters().put('startDate', getParameter('startDate'));
            pg.getParameters().put('endDate', getParameter('endDate'));
            return pg.setRedirect(true);
        }
        catch(Exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }
    
    @TestVisible
    private Approval.ProcessWorkitemRequest getWorkItemRequest(RequestWrapper d)    
	{
		Approval.ProcessWorkitemRequest pwir = new Approval.ProcessWorkitemRequest();
		if(d.ApprovalDecision == 'Approved')
		{
			pwir.setAction('Approve');
			approveCount++;
		}
		else if(d.ApprovalDecision == 'Rejected')
		{
			pwir.setAction('Reject');
			rejectCount++;
		}
		else
		{
			return null;
		}					
		pwir.setWorkitemId(PendingApprovalWorkItems.get(d.ServiceRequest.Id).Id);
		return pwir;
	}
    
    public class ApprovalDecisionWrapper
    {
    	public ApprovalDecisionWrapper(string pRequestId, string pStatus)
    	{
    		RequestId = pRequestId;
    		Decision = pStatus;
    	}
    	
    	public string RequestId
    	{
    		get;
    		private set;
    	}
    	
    	public string Decision
    	{
    		get;
    		set;
    	}
    }
    
    public class RequestWrapper
    {
        public RequestWrapper(RCR_Contract__c pServiceRequest, ProcessInstanceWorkitem pWorkItem, Schema.FieldSet fieldSetDescibe)
        {
            //Index = pIndex;
            ServiceRequest = pServiceRequest;
            Status = ServiceRequest.Status__c;
            WorkItem = pWorkItem;
            SelectedCriteriaFieldSet = getFieldsetCheckedLabels(fieldSetDescibe);
            if(WorkItem != null)
            {
            	ApprovalDecision = 'Pending';
            }
            
            if(ServiceRequest.Status__c == APS_Picklistvalues.RCRContract_Status_Approved || ServiceRequest.Status__c == APS_Picklistvalues.RCRContract_Status_Rejected)
            {
            	ApprovalDecision = ServiceRequest.Status__c;
            }
            setServiceAllocations();
        }
        
        public string ApprovalDecision
        {
        	get;
        	set;
        }
        
        
        public ProcessInstanceWorkitem WorkItem
        {
        	get;
        	set;
        }
        
        
        public RCR_Contract__c ServiceRequest
        {
            get;
            set;
        }
        
        
        public boolean CurrentSituationNotDefined
        {
        	get
        	{
        		return string.isBlank(ServiceRequest.Crit_Hlth_Self__c) &&
        				string.isBlank(ServiceRequest.Crit_Hlth_Oth__c) &&
        				string.isBlank(ServiceRequest.Crit_Abuse__c) &&
        				string.isBlank(ServiceRequest.Crit_Child__c) &&
        				string.isBlank(ServiceRequest.Crit_Cult__c) &&
        				string.isBlank(ServiceRequest.Crit_Deter__c) &&
        				string.isBlank(ServiceRequest.Crit_Homeless_Comm__c) &&
        				string.isBlank(ServiceRequest.Crit_Mob__c) &&
        				string.isBlank(ServiceRequest.Crit_Net__c);
        	}
        }
        
        
        public Map<string, AllocationWrapper> ServiceAllocations
        {
            get
            {
                if(ServiceAllocations == null)
                {
                    ServiceAllocations = new Map<string, AllocationWrapper>();
                }
                return ServiceAllocations;
            }
            private set;
        }

        
        public List<string> SelectedCriteriaFieldSet
        {
            get;
            private set;
        }
        
        public string Status
        {
            get;
            set;
        }
        
        @testVisible       
        private void setServiceAllocations()
        {
//            for(IF_Personal_Budget__c alloc : ServiceRequest.IF_Allocations__r)
//            {
//                if(!ServiceAllocations.containsKey(alloc.Service_Type__c))
//                {
//                    ServiceAllocations.put(alloc.Service_Type__c, new AllocationWrapper(alloc.Service_Type__c));
//                }
//                AllocationWrapper allocWrapper = ServiceAllocations.get(alloc.Service_Type__c);
//
//                if(alloc.Allocation_Type__c == APS_PicklistValues.IFAllocation_Type_OnceOff)
//                {
//                    allocWrapper.OnceOffAlloc += alloc.Amount__c;
//                    allocWrapper.OnceOffFYEAlloc += alloc.FYE_Amount__c;
//                }
//                else
//                {
//                    allocWrapper.RecurrentAlloc += alloc.Amount__c;
//                    allocWrapper.RecurrentFYEAlloc += alloc.FYE_Amount__c;
//                }
//            }
        }
        
        @testVisible
        private List<string> getFieldsetCheckedLabels(Schema.FieldSet fieldset)
        {
            List<string> selectedCheckboxLabels = new List<string>();
            for(Schema.FieldSetMember fsm : fieldset.getFields())
            {           
                if(fsm.getType() == Schema.DisplayType.Boolean)
                {       
                    if((Boolean)ServiceRequest.get(fsm.getFieldPath()))
                    {
                        selectedCheckboxLabels.add(fsm.getLabel());
                    }
                }
            }
            return selectedCheckboxLabels;
        }
    }
    
    public class AllocationWrapper
    {
        public AllocationWrapper(string pServiceType)
        {
            ServiceType = pServiceType;
            RecurrentAlloc = 0.0;
            RecurrentFYEAlloc = 0.0;
            OnceOffAlloc = 0.0;
            OnceOffFYEAlloc = 0.0;
        }
        
        public string ServiceType
        {
            get;
            private set;
        }
        
        public decimal RecurrentAlloc
        {
            get;
            private set;
        }
        
        public decimal OnceOffAlloc
        {
            get;
            private set;
        }
        
        public decimal RecurrentFYEAlloc
        {
            get;
            private set;
        }
        
        public decimal OnceOffFYEAlloc
        {
            get;
            private set;
        }
    }
}