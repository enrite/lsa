public with sharing class RCRParticipantReviewExtension 
	extends PageControllerBase
{
	public RCRParticipantReviewExtension(ApexPages.StandardController ctrlr)
	{
		controller = ctrlr;
	}
	
	private ApexPages.StandardController controller;
	transient List<SelectOption> serviceList;
	transient List<SelectOption> programList;
	transient Map<ID, RCR_Contract_Service_Type__c> serviceTypes;
	
	@TestVisible
	private Map<ID, RCR_Contract_Service_Type__c> AllContractServiceTypesByCssID
	{
		get
		{
			if(serviceTypes == null)
			{
				serviceTypes = new Map<ID, RCR_Contract_Service_Type__c>();
                for(RCR_Contract_Service_Type__c cst : [SELECT 	ID,
                												Service_Type__c,
                												Quantity__c,
                												RCR_Program__c,
                												RCR_Contract_Scheduled_Service__c,
                												RCR_Program_Category_Service_Type__c
        												FROM	RCR_Contract_Service_Type__c
        												WHERE	RCR_Contract_Scheduled_Service__r.RCR_Contract__c = :ServiceOrder.ID
        												ORDER BY Service_Type__c])
				{
					serviceTypes.put(cst.RCR_Contract_Scheduled_Service__c, cst);
				}
			}
			return serviceTypes;
		}
	}
	
	public List<SelectOption> Programs
	{
		get
		{
			if(programList == null)
			{
				programList = getSelectListFromObjectList([SELECT ID,
																	Name
															FROM	RCR_Program__c
															ORDER BY Name], true);
				
			}
			return programList;
		}
	}
	
	public RCR_Contract__c ServiceOrder
	{
		get
		{
			if(ServiceOrder == null)
			{
				ServiceOrder = RCRContractExtension.getContract(controller.getID());
			}
			return ServiceOrder;
		}
		set;
	}
	
	public String DefaultFlexibility
	{
		get
		{
			return APS_PicklistValues.RCRContractScheduledService_Flexibilty_Accumulative;
		}
		set;
	}
	
	public Component.c.Schedule mySched
	{
		get;
		set;
	}
	
	public List<SchedServiceWrapper> ScheduleServiceWrappers 
    {
        get
        {
            if(ScheduleServiceWrappers == null)
            {
                ScheduleServiceWrappers = new List<SchedServiceWrapper>();                
                for(RCR_Contract_Scheduled_Service__c css : ServiceOrder.RCR_Contract_Scheduled_Services__r)
                {
                	ScheduleServiceWrappers.add(new SchedServiceWrapper(css, AllContractServiceTypesByCssID.get(css.ID)));
                }
            }
            return ScheduleServiceWrappers;
        }
        set; 
    }
	
	public List<SelectOption> Services
	{
		get
		{
			if(serviceList == null)
			{
				serviceList = getSelectListFromObjectList([SELECT ID,
																CodeAndDescription__c
														FROM	RCR_Service__c
														WHERE	Account__c = :ServiceOrder.Account__c
														ORDER BY CodeAndDescription__c], 
														'ID',
														'CodeAndDescription__c', 
														false);
			}
			return serviceList;
		}
		set;
	}
	
	public PageReference createAmendment()
	{
		try
		{
			RCR_Contract__c newAmendment = RCRServiceOrderRequestController.cloneServiceOrder(ServiceOrder, null, true);
			newAmendment.Status__c = APS_PicklistValues.RCRContract_Status_New;
			newAmendment.Visible_To_Service_Providers__c = false;
			newAmendment.Destination_Record__c = null;
			TriggerBypass.RCRServiceOrder = true;
			update newAmendment;
			PageReference pg = Page.RCRRequest2;
			pg.getParameters().put('id',  newAmendment.ID);
			return pg.setRedirect(false);
		}
		catch(Exception ex)
		{
			return A2HCException.formatException(ex);
		}
	}
	
	public void saveOverride()
	{
		try
		{
			DMLAsSystem.updateRecord(ServiceOrder);
			for(SchedServiceWrapper w : ScheduleServiceWrappers) 
			{
				upsert w.ScheduledService;
				upsert w.ServiceType;
				w.ScheduleComponentController.saveSchedule(w.ScheduledService.ID, w.ScheduledService.Start_Date__c, w.ScheduledService.Original_End_Date__c);
				RCRScheduledServiceExtension ext = new RCRScheduledServiceExtension(false);
				ext.externalSaveAndCalculate(w.ScheduledService.ID);
				ext.setTotals(w.ScheduledService);
			}
		}
		catch(Exception ex)
		{
			A2HCException.formatException(ex);
		}
	}
	
	public PageReference acceptPlan()
	{
		try
		{
			ServiceOrder.OwnerId = UserUtilities.getQueue('RCR_Participation_Plan_Review').ID;
			ServiceOrder.Amendment_Accepted_By__c = UserInfo.getUserId();
			ServiceOrder.Amendment_Accepted_On__c = Date.today();
			ServiceOrder.Status__c = APS_PicklistValues.RCRContract_Status_ParticipationPlanAccepted;
			DMLAsSystem.updateRecord(ServiceOrder);
			
			ApexPages.StandardSetController stSetController = new ApexPages.StandardSetController([SELECT ID 
																									FROM RCR_Contract__c 
																									LIMIT 1]);
			return  A2HCUtilities.redirectToListView(ServiceOrder.getSobjectType(), stSetController, 'Participation Plans For Review');
		}
		catch(Exception ex)
		{
			return A2HCException.formatException(ex);
		}
	}
	
	public class SchedServiceWrapper
	{
		public SchedServiceWrapper(RCR_Contract_Scheduled_Service__c css, RCR_Contract_Service_Type__c pCssServiceType)
		{
			ScheduledService = css;
			ScheduleComponentController = new ScheduleController(css.ID, APS_PicklistValues.RCRContractScheduledService_Flexibilty_Accumulative);
			ServiceType = pCssServiceType;
			if(ServiceType == null)
			{
				ServiceType = new RCR_Contract_Service_Type__c(RCR_Contract_Scheduled_Service__c = css.ID);
			}
			SelectedProgram = ServiceType.RCR_Program__c;
		}
		
		public decimal PeriodQuantity
		{
			get
			{
				return ScheduleComponentController.Schedule.Quantity_Per_Period__c;
			}
		}
		
		public String SelectedProgram
		{
			get;
			set;
		}
		
		public String DayOfWeek
		{
			get
			{
				return ScheduledService.RCR_Service__r.Day_of_Week__c;
			}
		}
		
		public RCR_Contract_Scheduled_Service__c ScheduledService
		{
			get;
			set;
		}
		
		public ScheduleController ScheduleComponentController
		{
			get;
			set;
		}
		
		public RCR_Contract_Service_Type__c ServiceType
		{
			get;
			set;
		}
		
		public List<SelectOption> ServiceTypeList
		{
			get
			{
				if(ServiceTypeList == null)
				{				
					refreshServiceTypes();
				}
				return ServiceTypeList;
			}
			private set;
		}

		public void refreshServiceTypes()
		{	
			ServiceTypeList = new List<SelectOption>{new SelectOption('', '--None--')};
			for(RCR_Program_Category_Service_Type__c pcst :[SELECT ID,
																	Name,
																	RCR_Program_Category__r.Name
															FROM	RCR_Program_Category_Service_Type__c
															WHERE	RCR_Program_Category__r.RCR_Program__c = :SelectedProgram
															ORDER BY RCR_Program_Category__r.Name, Name])
			{
				ServiceTypeList.add(new SelectOption(pcst.ID, pcst.RCR_Program_Category__r.Name + ' - '  + pcst.Name));
			}
		}

	}
}