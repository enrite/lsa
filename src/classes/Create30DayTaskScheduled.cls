public class Create30DayTaskScheduled implements System.Schedulable
{
    public void execute(System.SchedulableContext sc)
    {
        Create30DayTaskBatch invBatch = new Create30DayTaskBatch(RCR_Invoice__c.SObjectType);
        Database.executeBatch(invBatch, getBatchSize('30_Day_Task_Invoice', 20));
    }

    public static Integer getBatchSize(String dataset, Integer defaultSize)
    {
        Batch_Size__c instance = Batch_Size__c.getInstance(dataset);
        if(instance != null && instance.Records__c != null)
        {
            return instance.Records__c.intValue();
        }
        return defaultSize;
    }
}