public class UserAuditQueuable implements Queueable 
{
    public UserAuditQueuable(List<User_Audit__c> pAudits)
    {
        audits = pAudits;
    }
    private List<User_Audit__c> audits;

    public void execute(QueueableContext context) 
    {
        insert audits;        
    }
}