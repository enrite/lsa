/**
 * Created by me on 08/10/2018.
 */

public without sharing class LPAlterContractBatch implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts
{
    public LPAlterContractBatch(Set<ID> serviceOrderIds, Set<ID> lpItemsForRejection, Set<ID> lpPendingItemsForApproval)
    {
        this.serviceOrderIds = serviceOrderIds;
        this.lpItemsForRejection = lpItemsForRejection;
        this.lpPendingItemsForApproval = lpPendingItemsForApproval;
    }

    private Set<ID> serviceOrderIds;
    private Set<ID> lpItemsForRejection = new Set<ID>();
    private Set<ID> lpPendingItemsForApproval = new Set<ID>();

    public Database.QueryLocator start(Database.BatchableContext param1)
    {
        return Database.getQueryLocator([SELECT ID
                                        FROM    RCR_Contract__c
                                        WHERE   ID IN :serviceOrderIds]);
    }

    public void execute(Database.BatchableContext param1, List<RCR_Contract__c> scope)
    {
        LPRESTInterface lpInterface = new LPRESTInterface();
        for(RCR_Contract__c so : LPRESTInterface.getContracts(scope))
        {
            lpInterface.amendContract(so, false);
        }
        lpInterface.saveRecords();
    }

    public void finish(Database.BatchableContext param1)
    {
        if(!lpItemsForRejection.isEmpty())
        {
            LPRejectBatch rejBatch = new LPRejectBatch(lpItemsForRejection, lpPendingItemsForApproval);
            Database.executeBatch(rejBatch, 20);
        }
        else if(!lpPendingItemsForApproval.isEmpty())
        {
            LPAuthoriseBatch authBatch = new LPAuthoriseBatch(lpPendingItemsForApproval);
            Database.executeBatch(authBatch, 100);
        }
    }
}