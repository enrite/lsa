@isTest
private class TestRCRServiceRateVariationRequest 
{
    static testMethod void myUnitTest() 
    {
        TestLoadData.loadRCRData();
                
        Test.StartTest();
    
        Account acct = [SELECT Id FROM Account LIMIT 1];
        RCR_Contract__c serviceOrder = [SELECT  Id,
                                                Total_Service_Order_Cost__c,
                                                (SELECT Id,
                                                        RCR_Service__c
                                                 FROM   RCR_Contract_Scheduled_Services__r)
                                        FROM    RCR_Contract__c                                        
                                        LIMIT 1];
        RCR_Service_Rate__c rate = [SELECT  Id
                                    FROM    RCR_Service_Rate__c
                                    WHERE   RCR_Service__c = :serviceOrder.RCR_Contract_Scheduled_Services__r[0].RCR_Service__c
                                    ORDER BY Start_Date__c desc
                                    LIMIT 1];                                       
        
        RCR_Service_Rate_Variation__c rateVariation = new RCR_Service_Rate_Variation__c();
        rateVariation.Account__c = acct.Id;
        insert rateVariation;
        
        RCR_Service_Rate_Audit__c rateAudit = new RCR_Service_Rate_Audit__c();
        rateAudit.RCR_Service_Rate_Variation__c = rateVariation.id;
        rateAudit.RCR_Contract_Scheduled_Service__c = serviceOrder.RCR_Contract_Scheduled_Services__r[0].Id;
        rateAudit.RCR_Service_Order__c = serviceOrder.Id;
        rateAudit.RCR_Service_Rate__c = rate.Id;
        rateAudit.Recalculation_ID__c = 10000000;
        rateAudit.Service_Order_Value_Before__c = serviceOrder.Total_Service_Order_Cost__c;
        insert rateAudit;
        
        
        ApexPages.StandardController controller = new ApexPages.StandardController(new RCR_Service_Rate_Variation__c());
        RCRServiceRateVariationRequestController cntrl = new RCRServiceRateVariationRequestController(controller);
        
        cntrl.VariationRequest = rateVariation; 
        cntrl.getServiceOrdersAndRates();
        
        system.debug(cntrl.AuthorisedToApprove);
        system.debug(cntrl.AcceptedByIsReadOnly);
        system.debug(cntrl.TotalCostVariation);
        system.debug(cntrl.DateTimeGenerated);
        system.debug(cntrl.DepartmentName);
        system.debug(cntrl.Building);
        system.debug(cntrl.StreetAddress);
        system.debug(cntrl.Suburb);
        system.debug(cntrl.Postcode);
        system.debug(cntrl.PostBox);
        system.debug(cntrl.Phone);
        system.debug(cntrl.Fax);
        system.debug(cntrl.ABN);
        system.debug(cntrl.DCSIDepartmentName);
        system.debug(cntrl.DCSIPostbox);
        system.debug(cntrl.DCSISuburb);
        system.debug(cntrl.DCSIPostBoxPostcode);
        system.debug(cntrl.MinistersContractManagerName);
        system.debug(cntrl.MinistersContractManagerPosition);
        system.debug(cntrl.ServiceOrders);
        system.debug(cntrl.Rates);
        system.debug(cntrl.OriginalServiceOrderCosts);
        
        cntrl.submitForApproval();
        cntrl.approve();
        cntrl.saveOverride();
        cntrl.cancelOverride();
        
        Test.StopTest();
    }
}