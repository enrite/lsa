public class RCRServiceOrderCloneSchedule implements Schedulable
{   
    public RCRServiceOrderCloneSchedule()
    {
    }
    
    public RCRServiceOrderCloneSchedule(RCRServiceOrderCloneBase.RunMode runMode)
    { 
        mode = runMode;
        prevBatch = null;
        newEndDate = null;
    }
    
    public RCRServiceOrderCloneSchedule(RCRServiceOrderCloneBase previousBatch)
    {
        mode = previousBatch.Mode;
        prevBatch = previousBatch;      
        newEndDate = null;
    }
    
    public RCRServiceOrderCloneSchedule(Date pNewEndDate, RCRServiceOrderCloneBase.RunMode pMode) 
    {
        prevBatch = null;
        newEndDate = pNewEndDate;   
        mode = pMode;   
    }
    
    private Date newEndDate; 
    private RCRServiceOrderCloneBase prevBatch; 
    private RCRServiceOrderCloneBase.RunMode mode;
     
    public String JobName
    {
        get
        {
            if(mode == RCRServiceOrderCloneBase.RunMode.Rollover)
            {
                return ScheduleJobNames.CreateRolloverRequest; 
            }
            else if(mode == RCRServiceOrderCloneBase.RunMode.RolloverApproved)
            {
                return ScheduleJobNames.ApproveRolloverRequest;
            }
            else if(mode == RCRServiceOrderCloneBase.RunMode.RequestApproved)
            {
                return ScheduleJobNames.ApproveNewRequest; 
            }
            else
            {
                throw new A2HCException('Invalid job run mode');
            }
        }           
    }
    
    public void execute(SchedulableContext SC) 
    {
        run(SC.getTriggerID());
    }
    
    @TestVisible
    private void run(String triggerID)  
    {
        ID batchprocessid; 
        if(prevBatch == null)
        {
            RCRScheduledServiceOrderCloneBatch cssBatch = new RCRScheduledServiceOrderCloneBatch();  
            cssBatch.ScheduleId = triggerID; 
            if(mode == RCRServiceOrderCloneBase.RunMode.Rollover)
            {
                cssBatch.NewEndDate = newEndDate;
            }
            cssBatch.Mode = mode;
            batchprocessid = Database.executeBatch(cssBatch, Batch_Size__c.getInstance('RCR_Service_Order_Clone').Records__c.IntValue());
        } 
        else if(prevBatch instanceof RCRAdhocServiceOrderCloneBatch)
        {
            RCRServiceOrderSubmitForCSABatch csaBatch = new RCRServiceOrderSubmitForCSABatch(prevBatch); 
            csaBatch.ScheduleId = triggerID;  
            batchprocessid = Database.executeBatch(csaBatch, Batch_Size__c.getInstance('RCR_Service_Order_Clone').Records__c.IntValue()); 
        }
        else if(newEndDate != null) 
        {           
            RCRScheduledServiceOrderCloneBatch cssBatch = new RCRScheduledServiceOrderCloneBatch(newEndDate); 
            cssBatch.ScheduleId = triggerID;  
            cssBatch.Mode = RCRServiceOrderCloneBase.RunMode.Rollover;
            batchprocessid = Database.executeBatch(cssBatch, Batch_Size__c.getInstance('RCR_Service_Order_Clone').Records__c.IntValue());
        }
        else if(prevBatch != null && prevBatch instanceof RCRScheduledServiceOrderCloneBatch) 
        {           
            RCRAdhocServiceOrderCloneBatch adhocBatch = new RCRAdhocServiceOrderCloneBatch(prevBatch);  
            adhocBatch.ServiceOrderIds = prevBatch.ServiceOrderIds;
            adhocBatch.ScheduleId = triggerID; 
            adhocBatch.Mode = prevBatch.Mode;  
            batchprocessid = Database.executeBatch(adhocBatch, Batch_Size__c.getInstance('RCR_Service_Order_Clone').Records__c.IntValue());
        }
    }
    
    
}