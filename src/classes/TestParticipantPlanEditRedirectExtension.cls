@isTest
public class TestParticipantPlanEditRedirectExtension
{                   
    static testMethod void myTestMethod()
    {   
        TestLoadFormData data = new TestLoadFormData();
                 
        Test.StartTest();
        
        Test.setCurrentPage(Page.ParticipantPlanEditRedirect);
        ApexPages.CurrentPage().getParameters().put('test', 'test');
                                                           
        Plan__c p = new Plan__c();
        p.Client__c = data.Client.Id;
        p.RecordTypeId = data.RecordTypes.get('MyPlan').Id;
        p.Plan_Status__c = 'Draft';
        insert p;
                               
        ParticipantPlanEditRedirectExtension ext = new ParticipantPlanEditRedirectExtension(new ApexPages.StandardController(p));
        ext.Redir();
        
        Plan__c p2 = new Plan__c();
        p2.Client__c = data.Client.Id;
        p2.RecordTypeId = data.RecordTypes.get('Discharge_Plan').Id;
        p2.Plan_Status__c = 'Draft';        
        
        ext = new ParticipantPlanEditRedirectExtension(new ApexPages.StandardController(p2));
        ext.Redir();
        
        insert p2;
        
        ext = new ParticipantPlanEditRedirectExtension(new ApexPages.StandardController(p2));
        ext.Redir();
                       
        Test.StopTest();        
    }
}