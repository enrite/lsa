@isTest//(seeAllData=true)
private class TestRCRBulkRolloverApproval 
{

    static testMethod void myUnitTest() 
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        RCRBulkRolloverApproval ext = new RCRBulkRolloverApproval();
        
        ext.SelectedAccount = null;
        system.debug(ext.SelectedAccount);
        system.debug(ext.ActionType);

        system.assert(ext.CanRolloverApprove != null);
        system.assert(ext.CanRolloverRecommend != null); 
        system.assert(ext.CanCSAApprove != null);       
        system.assert(ext.AccountsWithRollovers != null);
        system.assert(ext.Rollovers != null);
        system.assert(ext.Totals != null);
        
        ext.doSearch();
        
        ext.SelectedAccount = [SELECT Id FROM Account LIMIT 1 /*WHERE Name = 'TestAccount'*/].Id;
        
        Test.setCurrentPage(new PageReference('/apex/RCRBulkRolloverApprovals'));
        ApexPages.currentPage().getParameters().put('type', 'CSAApproval');
        system.debug('ActionType: ' + ext.ActionType);
        system.assert(ext.AccountsWithRollovers != null);
        system.debug(ext.AccountsWithRollovers);
        ext.cancel();
        ext.doSearch();
        
        RCR_Contract__c testSO = [SELECT    Id,
                                            CSA_Bulk_Approved__c,
                                            Rollover_Recommendation_Descision__c,
                                            Total_Service_Order_Cost__c,
                                            End_Date__c,
                                            Rollover_Status__c,
                                            Rollover_Rejection_Reason__c
                                    FROM    RCR_Contract__c 
                                    /*WHERE   Name = 'TestSO'*/
                                    LIMIT 1];
        
        RCRBulkRolloverApproval.ServiceOrderWrapper wrap = new RCRBulkRolloverApproval.ServiceOrderWrapper(testSO);
        wrap.ApprovedOnceOff = true;
        ext.Rollovers.add(wrap);
        ext.approve();
        
        ext.approveRejectRollover();
        
        system.debug(ext.Totals.TotalPercentageAllocationShortfall);
        
        List<RCRBulkRolloverApproval.ServiceOrderWrapper> wrappers = new List<RCRBulkRolloverApproval.ServiceOrderWrapper>();
        for(integer i = 0; i <= 4; i++)
        {
            wrap = new RCRBulkRolloverApproval.ServiceOrderWrapper(testSO);
            wrap.ApprovedOnceOff = (i == 0);
            wrap.ApprovedRecurrent = (i == 1);
            wrap.ReduceServices = (i == 2);
            wrap.UseAlternateProvider = (i == 3);
            wrap.TotalAllocation = 1.0;
            system.debug(wrap.PercentageAllocationShortfall);
            wrappers.add(wrap);
        }
        ext.Rollovers.addAll(wrappers);
        ext.saveOverride();

        ext.Rollovers = null;
        ext.Rollovers.add(wrappers[0]);
        ext.approveRejectRollover();
        
        ext.Rollovers = null;
        ext.Rollovers.add(wrappers[1]);
        ext.approveRejectRollover();
        
        ext.Rollovers = null;
        ext.Rollovers.add(wrappers[2]);
        ext.approveRejectRollover();                
        
        Test.StopTest();
    }
    
    static testMethod void myUnitTest2() 
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        RCRBulkRolloverApproval ext = new RCRBulkRolloverApproval();
        
        RCR_Contract__c testSO = [SELECT    Id,
                                            CSA_Bulk_Approved__c,
                                            Rollover_Recommendation_Descision__c,
                                            Total_Service_Order_Cost__c,
                                            End_Date__c,
                                            Rollover_Status__c,
                                            Rollover_Rejection_Reason__c
                                    FROM    RCR_Contract__c 
                                    /*WHERE   Name = 'TestSO'*/
                                    LIMIT 1];
                                    
        RCRBulkRolloverApproval.ServiceOrderWrapper wrap = new RCRBulkRolloverApproval.ServiceOrderWrapper(testSO);
        ext.Rollovers.add(wrap);
        ext.approveRejectRollover();
        
        Test.StopTest();
    }
    
    static testMethod void myUnitTest3() 
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        RCRBulkRolloverApproval ext = new RCRBulkRolloverApproval();
        
        RCR_Contract__c testSO = [SELECT    Id,
                                            CSA_Bulk_Approved__c,
                                            Rollover_Recommendation_Descision__c,
                                            Total_Service_Order_Cost__c,
                                            End_Date__c,
                                            Rollover_Status__c,
                                            Rollover_Rejection_Reason__c,
                                            Source_Record__r.Total_Service_Order_Cost__c
                                    FROM    RCR_Contract__c 
                                    /*WHERE   Name = 'TestSO'*/
                                    LIMIT 1];
                                    
        for(integer i = 0; i <= 3; i++)
        {
            RCRBulkRolloverApproval.ServiceOrderWrapper wrap = new RCRBulkRolloverApproval.ServiceOrderWrapper(testSO);
            ext.Rollovers.add(wrap);
        }
        ext.calculateTotals();
        
        Test.StopTest();
    }
    
    
    static testMethod void testExceptions()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        RCRBulkRolloverApproval ext = new RCRBulkRolloverApproval();
        ext.forceTestError = true;
        
        try
        {
            ext.doSearch();
        }
        catch(Exception ex){}
        try
        {
            ext.approve();
        }
        catch(Exception ex){}
        try
        {
            ext.saveOverride();
        }
        catch(Exception ex){}
        try
        {
            ext.approveRejectRollover();
        }
        catch(Exception ex){}
        try
        {
            ext.cancel();
        }
        catch(Exception ex){}
        
        Test.StopTest();
    }
}