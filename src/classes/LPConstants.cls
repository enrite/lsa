public class LPConstants
{
    public static final String CLAIMSTATUS_PENDINGAUTHORISATION = 'Pending Authorisation';
    public static final String CLAIMSTATUS_AUTHORISED = 'Authorised';
    public static final String CLAIMSTATUS_REJECTED = 'Rejected';
    public static final String CLAIMSTATUS_CANCELLED = 'Cancelled';

}