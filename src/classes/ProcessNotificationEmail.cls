global class ProcessNotificationEmail implements Messaging.InboundEmailHandler 
{ 
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) 
    {             
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        
        // create a new notification record
        Notification__c n = new Notification__c();
        n.Received_From__c = email.fromName + ' (' + email.fromAddress + ')';
        n.Email_Subject__c = email.subject;
        n.Email_Body__c = (email.plainTextBody != null ? email.plainTextBody : email.htmlBody);
        
        insert n;        
                
        List<Attachment> attachments = new List<Attachment>();
        
        // loop through the email attachments and vreate attachment records for the notification                   
        if (email.binaryAttachments != null && email.binaryAttachments.size() > 0) 
        {
            for (integer i = 0 ; i < email.binaryAttachments.size(); i++) 
            {
                Attachment a = new Attachment();
                a.ParentId = n.Id;
                a.Name = email.binaryAttachments[i].fileName;
                a.Body = email.binaryAttachments[i].body;
            
                attachments.add(a);                            
            }
        }
        
        // insert the attachments
        insert attachments;
        
        result.success = true;
        
        return result; 
    }     
}