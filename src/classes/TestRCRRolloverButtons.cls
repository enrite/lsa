/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest//(SeeAllData=true)
private class TestRCRRolloverButtons 
{
    static testMethod void test1() 
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        RCR_Contract__c so = TestRCR.getTestContract();
        addValidFields(so);
        update so;
        RCRRolloverButtons ext = new RCRRolloverButtons(new ApexPages.StandardController(so));
        
        system.assert(ext.Rollover != null);
        
        ext.RolloverComment = 'test';
        system.assert(ext.RolloverComment != null);
        
        system.assert(ext.HasErrors != null);
        system.assert(ext.ClientRollovers != null);
        
        ext.Action = 'toggle';
        ext.updateRolloverStatus();
        
        ext.Action = 'assignToBrokerage';
        ext.updateRolloverStatus();
        
        ext.Action = 'adjust';
        ext.updateRolloverStatus();
        

        
        ext.submitForReview();
        
        ext.submitForAdjustment();
        
        Test.StopTest();
    }
    
    static testMethod void test2() 
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        RCR_Contract__c so = TestRCR.getTestContract();
        addValidFields(so);
        update so;
        RCRRolloverButtons ext = new RCRRolloverButtons(new ApexPages.StandardController(so));
        
        ext.Rollover.Destination_Record__c = null;
        ext.submitForAllocationApproval();
        
        ext.Rollover.Destination_Record__c = so.ID;
        ext.submitForAllocationApproval();
        
        ext.Rollover.Rollover_Status__c = APS_PicklistValues.RCRContract_RolloverStatus_RolloverNotRequired;
        ext.submitForAllocationApproval();
        
        ext.cancelOverride();
        
        Test.StopTest();
    }
    
    static testMethod void test3() 
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        RCR_Contract__c so = TestRCR.getTestContract();
        addValidFields(so);
        update so;
        RCRRolloverButtons ext = new RCRRolloverButtons(new ApexPages.StandardController(so));
        ID scId = ext.Rollover.Service_Coordinator__c;
        for(RCRRolloverButtons.RolloverWrapper r : ext.ClientRollovers)
        {
            r.BrokerageReviewed = true;
            r.Rollover.Service_Coordinator__c = null;
        }
        ext.submitForReview();
        
        for(RCRRolloverButtons.RolloverWrapper r : ext.ClientRollovers)
        {
            r.PendingCoordinator = true;
            r.Rollover.Service_Coordinator__c = scId;
        }
        ext.submitForReview();
        
        for(RCRRolloverButtons.RolloverWrapper r : ext.ClientRollovers)
        {
            r.PendingCoordinator = false;
            r.Rollover.Service_Coordinator__c = scId;
        }
        ext.submitForReview();
        
        Test.StopTest(); 
    }
    
    static testMethod void test4()  
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        RCR_Contract__c so = TestRCR.getTestContract();
        addValidFields(so);
        update so;
        
        RCR_Funding_Detail__c fd = new RCR_Funding_Detail__c(RCR_Service_Order__c = so.ID); 
        fd.Maximum_Amount__c = 0;
        fd.Cost_Centre__c = RCR_Cost_Centre_Code__c.getInstance('ROLLOVER').Code__c;
        insert fd;
        RCRRolloverButtons ext = new RCRRolloverButtons(new ApexPages.StandardController(so));
        
        ext.validForServiceOrderApproval(); 
        
        ext.Rollover.Status__c = APS_PicklistValues.RCRContract_RolloverStatus_PendingApproval;
        ext.validForServiceOrderApproval(); 
        
        ext.Rollover.Status__c = APS_PicklistValues.RCRContract_RolloverStatus_ReadyForAllocation;  
        ext.validForServiceOrderApproval();
        
        Test.StopTest(); 
    }
        
    
    private static void addValidFields(RCR_Contract__c so)
    {            
        so.Level__c = '1';
        so.Service_Coordinator__c = [SELECT     ID 
                                                    FROM    User 
                                                    WHERE   UserRole.Name LIKE 'RCR%' AND
                                                            IsActive = true
                                                    LIMIT 1].ID;
        so.Regional_Manager__c = UserInfo.getUserID();
        so.Team_Manager__c = UserInfo.getUserID();
        so.Director__c = so.Regional_Manager__c;    
        so.Type__c = 'Brokerage';
        so.Funding_Source__c = APS_PicklistValues.RCRContract_FundingSource_PTIC;
        so.Service_to_be_provided__c = 'test';
        so.Risk_Behav_Oth__c = true;
        so.Risk_Oth_Det__c = 'test';
        so.New_Request_Type__c = 'test';
        so.Reason_Chosen__c = 'test';
        so.RecordTypeId = A2HCUtilities.getRecordType('RCR_Contract__c', APSRecordTypes.RCRServiceOrder_Rollover).ID;             
    }
}