@isTest
public class TestFormDetailEditExtension
{                   
    static testMethod void myTestMethod()
    {
        TestLoadFormData data = new TestLoadFormData();
         
        Test.StartTest();         
                         
        Form__c mc = new Form__c(RecordTypeId = data.RecordTypes.get('Medical_Certificate').Id);
        mc.Client__c = data.Client.Id;
        insert mc;                                                 
        
        Form__c fim = new Form__c(RecordTypeId = data.RecordTypes.get('FIM_Score_Sheet').Id);
        fim.Client__c = data.Client.Id;
        insert fim;                                                 
                                           
        Form__c f = new Form__c(RecordTypeId = data.RecordTypes.get('Application_Form_from_Other').Id);
        f.Client__c = data.Client.Id;
        insert f;    
        
        Note n = new Note(ParentId = data.Client.Id, Title = 'test');
        insert n;                             
        
        Attachment att = new Attachment(ParentId = data.Client.Id, Name = 'test', Body = Blob.valueOf('test'));
        insert att;                             
                                        
        Form_Detail__c ce = new Form_Detail__c(Form_Application_Assessment__c = f.Id,
                                               RecordTypeId = data.RecordTypes.get('Chief_Executive_Application_Assessment').Id,                                               
                                               //Application_Rules_Committee_Approval__c = true,
                                               Status_Reason__c = 'test');        
        insert ce;
                        
        ce = [SELECT Id,
                     Form_Application_Assessment__c,
                     Form_Application_Assessment__r.Client__c,
                     Form_Application_Assessment__r.Client__r.Age__c,
                     Status__c,
                     Status_Reason__c
              FROM Form_Detail__c
              WHERE Id = :ce.Id];
                                   
        ce.Status__c = 'Lifetime Participant';
        FormDetailEditExtension ext = new FormDetailEditExtension(new ApexPages.StandardController(ce));
        ext.SaveOverride();
        
        ext.RedirectToGenerateLetter();
        system.debug(ext.LatestMedicalCertificate);
        system.debug(ext.LatestApplicationForm);
        system.debug(ext.LatestFIMScoreSheet);
        system.debug(ext.Client);
        
        List<FormDetailEditExtension.NoteAttachment> nas = ext.NotesAndAttachments;
        nas[0].getType();
        nas[0].getTitle();
        nas[0].getLastModifiedDate();
        nas[0].getLastModifiedDate();
        nas[0].getType();
        
        ce.Status__c = 'Accepted as Interim Participant';        
        ext = new FormDetailEditExtension(new ApexPages.StandardController(ce));
        ext.SaveOverride();
                        
        ce.Status__c = 'Not Accepted';        
        ext = new FormDetailEditExtension(new ApexPages.StandardController(ce));
        ext.SaveOverride();
        
        data.Client.Date_of_Birth__c = Date.today().addYears(-10);
        update data.Client;
        
        ce = [SELECT Id,
                     Form_Application_Assessment__c,
                     Form_Application_Assessment__r.Client__c,
                     Form_Application_Assessment__r.Client__r.Age__c,
                     Status__c,
                     Status_Reason__c
              FROM Form_Detail__c
              WHERE Id = :ce.Id];
        
        ce.Status__c = 'Accepted as Interim Participant';        
        ext = new FormDetailEditExtension(new ApexPages.StandardController(ce));
        ext.SaveOverride();
        
        delete data.ParentGuardian;
        
        ce.Status__c = 'Lifetime Participant';
        ext = new FormDetailEditExtension(new ApexPages.StandardController(ce));
        ext.SaveOverride();
        
        ce.Status__c = 'Accepted as Interim Participant';        
        ext = new FormDetailEditExtension(new ApexPages.StandardController(ce));
        ext.SaveOverride();
        
        ce.Status__c = 'Not Accepted';        
        ext = new FormDetailEditExtension(new ApexPages.StandardController(ce));
        ext.SaveOverride();
        
        ext.DeleteRecord();
        
        ext = new FormDetailEditExtension();
        
        Test.StopTest();
    }
}