@isTest
public class TestRCRValidateServiceOrderAmendExt
{                   
    static testMethod void myTestMethod()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        RCR_Contract__c so = [SELECT Id,
                                     Plan_Action__r.Approved__c
                              FROM RCR_Contract__c
                              LIMIT 1];
         
        RCRValidateServiceOrderAmendExtension ext = new RCRValidateServiceOrderAmendExtension(new ApexPages.StandardController(so));
        ext.Validate();
        
        Test.StopTest();       
    }
}