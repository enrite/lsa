@isTest
private class TestRCRPTICProgressiveReportController
{
//    static testMethod void myUnitTest()
//    {
//        TestLoadData.loadRCRData();
//
//        Test.StartTest();
//
//        RCR_Contract__c so = [SELECT Id,
//                                     Client__c,
//                                     RecordTypeId,
//                                     Funding_Source__c,
//                                     Allocation_Approved_Date__c
//                              FROM RCR_Contract__c
//                              WHERE Client__c != null
//                              LIMIT 1];
//
//        so.RecordTypeId = [SELECT Id
//                           FROM RecordType
//                           WHERE Name = :APSRecordTypes.RCRServiceOrder_NewRequest].Id;
//        so.Funding_Source__c = APS_Picklistvalues.RCRContract_FundingSource_PTIC;
//        so.Allocation_Approved_Date__c = Date.today();
//        so.Crit_Homeless__c = true;
//        so.Crit_Risk_Homeless__c = true;
//        so.Crit_FamSA__c = true;
//        so.Crit_Novita__c = true;
//        so.Crit_Transfer__c = true;
//        so.Background_Behavioural_Issues__c = true;
//        so.Background_Deteriorating_Condition__c = true;
//        so.Background_Carer_Issues__c = true;
//        so.Background_Hospital_Discharge__c = true;
//        so.Background_Ministerial_Imperative__c = true;
//        so.Background_NGO_Sector_Request__c = true;
//        so.Background_Accomm_Services_Request__c = true;
//        update so;
//
////        IF_Personal_Budget__c pb = new IF_Personal_Budget__c();
////        pb.Client__c = so.Client__c;
////        pb.Financial_Year__c = [SELECT Id FROM Financial_Year__c WHERE Start_Date__c <= :Date.today() AND End_Date__c >= :Date.today() LIMIT 1].Id;
////        pb.RCR_Service_Order__c = so.Id;
////        pb.Status__c = APS_PicklistValues.IFAllocation_Status_Approved;
////        pb.Allocation_Type__c = 'Once Off';
////        pb.Amount__c = 1;
////        pb.FYE_Amount__c = 1;
////        insert pb;
//
//        ApexPages.CurrentPage().getParameters().put('startDate', String.valueOf(Date.today().addYears(-1)));
//        ApexPages.CurrentPage().getParameters().put('endDate', String.valueOf(Date.today().addYears(1)));
//
//        RCRPTICProgressiveReportController c = new RCRPTICProgressiveReportController();
//        system.debug(c.ClientAgeGroups);
//        system.debug(c.ClientDisabilities);
//        system.debug(c.RequestTypesCount);
//        system.debug(c.Allocations);
//        system.debug(c.Totals);
//        c.getSimpleCounts();
//
//        RCRPTICProgressiveReportController.ServicesWrapper sw = new RCRPTICProgressiveReportController.ServicesWrapper('test');
//        system.debug(sw.ServiceCount);
//        system.debug(sw.TotalServiceCost);
//        system.debug(sw.TotalServiceFYECost);
//
//        Test.StopTest();
//    }
}