public class A2HCException extends Exception
{
    public static PageReference formatWarning(String message)
    {
    	formatException(ApexPages.Severity.WARNING, message);
        return null;
    }
    
     public static PageReference formatWarning(ApexPages.Severity severity, String message)
    {
    	formatException(severity, message); 
        return null;
    }
    
    public static PageReference formatMessage(String message)
    {
    	return formatWarning(ApexPages.Severity.INFO, message);
    }

    public static PageReference formatException(String message)
    {
        return formatException(ApexPages.Severity.ERROR, message);
    }
    
    public static PageReference formatException(Exception e)
    {
    	formatException(ApexPages.Severity.ERROR, e);
        return null;
    }

    public static PageReference formatException(ApexPages.Severity severity, String message)
    {
    	if(ApexPages.currentPage() == null)
    	{
    		throw new A2HCException(message);
    	}
        ApexPages.addMessage(new ApexPages.Message(severity, message));
        return null;
    }
    
    public static PageReference formatException(ApexPages.Severity severity, Exception e)
    {
        formatException(severity, getExceptionMessage(e));
        return null; 
    }
    
    public static PageReference formatException(Approval.ProcessResult result)
    {
        formatException(ApexPages.Severity.ERROR, getExceptionMessage(result));
        return null; 
    }

    public static Boolean formatValidationException(String message)
    {
        formatException(message);
        return false;
    }

	public static PageReference formatExceptionAndRollback(Savepoint sp, String message)
	{
        if(sp != null)
        {
            Database.rollback(sp);
        }
        return formatException(message);
    }

    public static PageReference formatExceptionAndRollback(Savepoint sp, Exception e)
    {
        if(sp != null)
        {
            Database.rollback(sp);
        }
        return formatException(e);
    }

    public static String getExceptionMessage(Exception e)
    {	
    	String msg = e.getMessage();

    	if(msg.contains('insufficient access rights'))
    	{
            return 'You cannot edit this record.';
    	}
    	else if(e instanceof DMLException)
        {       
        	DMLException dmlEx = (DMLException)e;
        	if(dmlEx.getDmlType(0) == StatusCode.ENTITY_IS_LOCKED)
        	{
        		return 'Record ' + dmlEx.getDmlId(0) + ' is locked.'; 
        	}
        	else if(dmlEx.getDmlType(0) == StatusCode.INSUFFICIENT_ACCESS_OR_READONLY)
        	{
        		return 'You do not have permissions to edit record + ' + dmlEx.getDmlId(0) + '.';
        	}
        	else if(msg.contains('the entity is locked for editing'))
	    	{
	    		return 'Another user or process is editing this record + ' + dmlEx.getDmlId(0) + ', try again shortly.';
	    	}
           return ((DMLException)e).getDmlMessage(0);
        }
        if(!(e instanceof A2HCException))
        {        	
        	// don't display in PROD
			if(A2HCUtilities.isSandbox())	
			{
	    		msg += ' ' + e.getStackTraceString();
			}
			else
			{
				msg = ' Line:' + e.getLineNumber().format() + ' ' +  msg;
			}
        }
        return msg;
    }
    
    public static String getExceptionMessage(Exception e, Integer maxLength)
    {
    	String msg = getExceptionMessage(e);     	
    	if(msg.length() > maxLength)
        {
        	msg = msg.substring(0, maxLength);
        }
        return msg;
    }
    
    public static String[] getExceptionMessages(Database.SaveResult[] results)
    {
    	List<String> msgs = new List<String>();  
    	for(Database.SaveResult result : results)
    	{    	
	    	if(!result.isSuccess() && result.getErrors() != null)
			{	
				msgs.add(result.getErrors()[0].getMessage());
				if(ApexPages.currentPage() == null)
				{
					throw new A2HCException(msgs[0]);
				}				
			}
    	}
        return msgs;
    }
    
    public static String getExceptionMessage(Approval.ProcessResult result)
    {
    	if(!result.isSuccess() && result.getErrors() != null)
		{	
			String msg = '';
			for(Database.Error err : result.getErrors())
			{
				msg += err.getMessage() + '\n';
			}
			msg.removeEnd('\n');
			return msg;
		}
        return null;
    }
}