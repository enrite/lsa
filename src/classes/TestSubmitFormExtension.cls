@isTest
public class TestSubmitFormExtension
{          
    static testMethod void testPortal()
    {
        TestLoadFormData data = new TestLoadFormData();
        
        System.runAs(data.PortalUser)
        {                                   
            Form__c f = new Form__c();
            f.RecordTypeId = data.RecordTypes.get('Severe_Injury_Notification').Id;  
            insert f; 
            
            f = [SELECT Id,
                        Client__c,
                        RecordType.Name,
                        RecordType.DeveloperName
                 FROM Form__c
                 WHERE Id = :f.Id];         
        
            SubmitFormExtension ext = new SubmitFormExtension(new ApexPages.StandardController(f));               
        
            ext.CheckSubmitOrProcess();
        }                                
    }
             
    static testMethod void testSINF()
    {
        TestLoadFormData data = new TestLoadFormData();
                                                 
        Form__c f = new Form__c();        
        f.First_Name__c = 'test';
        f.Surname__c = 'test';
        f.Street__c = 'test';
        f.Suburb__c = 'test';
        f.State__c = 'test';
        f.Postcode__c = 'test';
        f.Home_Phone__c = '123456789';
        f.SAPOL_Vehicle_Collision_Report_Number__c = 'test';
        f.Date_of_Accident__c = Date.today();
        f.Time_of_Accident_hours__c = '13';
        f.Time_of_Accident_minutes__c = '00';
        f.Location_of_Accident__c = 'test';
        f.Injured_person_s_part_in_accident__c = 'test';                
        f.Injured_person_s_description_of_accident__c = 'test';        
        f.Applicant_s_injuries__c = 'test';
        f.RecordTypeId = data.RecordTypes.get('Severe_Injury_Notification').Id;  
        insert f; 
        
        f = [SELECT Id,
                    Allocated_To__c,
                    Hospital_Facility__c,
                    Client__c,
                    Signature_Date__c,
                    Title__c,
                    First_Name__c,
                    Surname__c,
                    Street__c,
                    Suburb__c,
                    State__c,
                    Postcode__c,
                    Home_Phone__c,
                    Mobile_Phone__c,
                    Email__c,
                    Date_of_Birth__c,
                    Is_an_Interpreter_Required__c,
                    Interpreter_Language__c,
                    SAPOL_Vehicle_Collision_Report_Number__c,
                    Date_of_Accident__c,
                    Time_of_Accident_hours__c,
                    Time_of_Accident_minutes__c,
                    Location_of_Accident__c,
                    Injured_person_s_part_in_accident__c,
                    Injured_person_s_description_of_accident__c,
                    Applicant_s_injuries__c,                    
                    RecordType.Name,
                    RecordType.DeveloperName,
                    Contact_Title__c,
                    Contact_First_Name__c,
                    Contact_Surname__c,
                    Contact_Street__c,
                    Contact_Suburb__c,
                    Contact_State__c,
                    Contact_Postcode__c,
                    Relationship_to_Injured_Person__c,
                    Contact_Email__c,
                    Contact_Home_Phone__c,
                    Contact_Work_Phone__c,
                    Contact_Mobile_Phone__c,
                    Contact_Date_of_Birth__c,
                    Is_an_Interpreter_Required_for_Contact__c,
                    Contact_Interpreter_Language__c,
                    Contact_ATSI_origin__c,
                    Contact_preferred_written_communication__c,
                    Contact_preferred_phone_number__c,                    
                    Lawful_authority_act_on_behalf__c,
                    Birth_Certificate__c,
                    Guardianship_Order__c,
                    Custody_Order__c,
                    If_yes_details_of_lawful_authority__c
             FROM Form__c
             WHERE Id = :f.Id];         
        
        SubmitFormExtension ext = new SubmitFormExtension(new ApexPages.StandardController(f));               
        
        ext.CheckSubmitOrProcess();
                                                
        ext.Process();        
    }
        
    static testMethod void testMedicalCertificate()
    {
        TestLoadFormData data = new TestLoadFormData();
                                           
        Form__c f = new Form__c();
        f.RecordTypeId = data.RecordTypes.get('Medical_Certificate').Id;
        insert f;
    
        f = [SELECT Id,
                    Client__c,
                    Title__c,
                    First_Name__c,
                    Surname__c,
                    RecordType.Name,
                    RecordType.DeveloperName
             FROM Form__c
             WHERE Id = :f.Id];         
        
        SubmitFormExtension ext = new SubmitFormExtension(new ApexPages.StandardController(f)); 
        
        ext.Process();               
    }
                
    static testMethod void testFIMScoreSheet()
    {
        TestLoadFormData data = new TestLoadFormData();
                                           
        Form__c f = new Form__c();
        f.RecordTypeId = data.RecordTypes.get('FIM_Score_Sheet').Id;
        insert f;
    
        f = [SELECT Id,
                    Client__c,
                    RecordType.Name,
                    RecordType.DeveloperName
             FROM Form__c
             WHERE Id = :f.Id];         
        
        SubmitFormExtension ext = new SubmitFormExtension(new ApexPages.StandardController(f));     
        
        ext.Process();           
    }
    
    static testMethod void testAppFormOther()
    {
        TestLoadFormData data = new TestLoadFormData();
                                           
        Form__c f = new Form__c();
        f.Client__c = data.Client.Id;
        f.Signature_Date__c = Date.today();
        f.First_Name__c = 'test';
        f.Surname__c = 'test';
        f.Street__c = 'test';
        f.Suburb__c = 'test';
        f.State__c = 'test';
        f.Postcode__c = 'test';
        f.Home_Phone__c = '123456789';
        f.SAPOL_Vehicle_Collision_Report_Number__c = 'test';
        f.Date_of_Accident__c = Date.today();
        f.Time_of_Accident_hours__c = '13';
        f.Time_of_Accident_minutes__c = '00';
        f.Location_of_Accident__c = 'test';
        f.Injured_person_s_part_in_accident__c = 'test';                
        f.Injured_person_s_description_of_accident__c = 'test';        
        f.Applicant_s_injuries__c = 'test';
        f.RecordTypeId = data.RecordTypes.get('Application_Form_from_Other').Id;
        f.Contact_First_Name__c = 'test';
        f.Contact_preferred_written_communication__c = 'Email';
        insert f;
                        
        f = [SELECT Id,
                    Client__c,
                    Signature_Date__c,
                    Title__c,
                    First_Name__c,
                    Surname__c,
                    Street__c,
                    Suburb__c,
                    State__c,
                    Postcode__c,
                    Home_Phone__c,
                    SAPOL_Vehicle_Collision_Report_Number__c,
                    Date_of_Accident__c,
                    Time_of_Accident_hours__c,
                    Time_of_Accident_minutes__c,
                    Location_of_Accident__c,
                    Injured_person_s_part_in_accident__c,
                    Injured_person_s_description_of_accident__c,
                    Applicant_s_injuries__c,                    
                    RecordType.Name,
                    RecordType.DeveloperName,
                    Contact_Title__c,
                    Contact_First_Name__c,
                    Contact_Surname__c,
                    Contact_Street__c,
                    Contact_Suburb__c,
                    Contact_State__c,
                    Contact_Postcode__c,
                    Relationship_to_Injured_Person__c,
                    Contact_Email__c,
                    Contact_Home_Phone__c,
                    Contact_Work_Phone__c,
                    Contact_Mobile_Phone__c,
                    Contact_Date_of_Birth__c,
                    Is_an_Interpreter_Required_for_Contact__c,
                    Contact_Interpreter_Language__c,
                    Contact_ATSI_origin__c,
                    Contact_preferred_written_communication__c,
                    Contact_preferred_phone_number__c,                    
                    Lawful_authority_act_on_behalf__c,
                    Birth_Certificate__c,
                    Guardianship_Order__c,
                    Custody_Order__c,
                    If_yes_details_of_lawful_authority__c
             FROM Form__c
             WHERE Id = :f.Id];   
                                
        SubmitFormExtension ext = new SubmitFormExtension(new ApexPages.StandardController(f));     
        
        ext.CheckSubmitOrProcess();         
        
        Form_Detail__c fd = new Form_Detail__c(Form_Motor_Vehicle__c = f.Id,
                                               RecordTypeId = data.RecordTypes.get('Motor_Vehicle').Id);
        insert fd;   
        
        f = [SELECT Id,
                    Client__c,
                    Signature_Date__c,
                    Title__c,
                    First_Name__c,
                    Surname__c,
                    Street__c,
                    Suburb__c,
                    State__c,
                    Postcode__c,
                    Home_Phone__c,
                    SAPOL_Vehicle_Collision_Report_Number__c,
                    Date_of_Accident__c,
                    Time_of_Accident_hours__c,
                    Time_of_Accident_minutes__c,
                    Location_of_Accident__c,
                    Injured_person_s_part_in_accident__c,
                    Injured_person_s_description_of_accident__c,
                    Applicant_s_injuries__c,                    
                    RecordType.Name,
                    RecordType.DeveloperName,
                    Contact_Title__c,
                    Contact_First_Name__c,
                    Contact_Surname__c,
                    Contact_Street__c,
                    Contact_Suburb__c,
                    Contact_State__c,
                    Contact_Postcode__c,
                    Relationship_to_Injured_Person__c,
                    Contact_Email__c,
                    Contact_Home_Phone__c,
                    Contact_Work_Phone__c,
                    Contact_Mobile_Phone__c,
                    Contact_Date_of_Birth__c,
                    Is_an_Interpreter_Required_for_Contact__c,
                    Contact_Interpreter_Language__c,
                    Contact_ATSI_origin__c,
                    Contact_preferred_written_communication__c,
                    Contact_preferred_phone_number__c,                    
                    Lawful_authority_act_on_behalf__c,
                    Birth_Certificate__c,
                    Guardianship_Order__c,
                    Custody_Order__c,
                    If_yes_details_of_lawful_authority__c,
                    (SELECT Id
                     FROM Motor_Vehicles__r)
             FROM Form__c
             WHERE Id = :f.Id];   
                                
        ext = new SubmitFormExtension(new ApexPages.StandardController(f));     

        system.debug(ext.PossibleMatches);
        
        ext.ClientId = data.Client.Id;
        ext.AssignClient();

        system.debug(ext.ParentGuardians);
        
        ext.Process();                                             
    }
    
    static testMethod void testAppFormInsurer()
    {
        TestLoadFormData data = new TestLoadFormData();
                                           
        Form__c f = new Form__c();
        f.Client__c = data.Client.Id;
        f.Signature_Date__c = Date.today();
        f.First_Name__c = 'test';
        f.Surname__c = 'test';
        f.Street__c = 'test';
        f.Suburb__c = 'test';
        f.State__c = 'test';
        f.Postcode__c = 'test';
        f.Home_Phone__c = '123456789';
        f.SAPOL_Vehicle_Collision_Report_Number__c = 'test';
        f.Date_of_Accident__c = Date.today();
        f.Time_of_Accident_hours__c = '13';
        f.Time_of_Accident_minutes__c = '00';
        f.Location_of_Accident__c = 'test';
        f.Injured_person_s_part_in_accident__c = 'test';                
        f.Injured_person_s_description_of_accident__c = 'test';        
        f.Applicant_s_injuries__c = 'test';
        f.RecordTypeId = data.RecordTypes.get('Application_Form_from_Insurer').Id;  
        f.Contact_First_Name__c = 'test';      
        insert f;
        
        Form_Detail__c fd = new Form_Detail__c(Form_Motor_Vehicle__c = f.Id,
                                               RecordTypeId = data.RecordTypes.get('Motor_Vehicle').Id);
        insert fd;                                               
        
        f = [SELECT Id,                    
                    Client__c,
                    Signature_Date__c,
                    Title__c,
                    First_Name__c,
                    Surname__c,
                    Street__c,
                    Suburb__c,
                    State__c,
                    Postcode__c,
                    Home_Phone__c,
                    SAPOL_Vehicle_Collision_Report_Number__c,
                    Date_of_Accident__c,
                    Time_of_Accident_hours__c,
                    Time_of_Accident_minutes__c,
                    Location_of_Accident__c,
                    Injured_person_s_part_in_accident__c,
                    Injured_person_s_description_of_accident__c,
                    Applicant_s_injuries__c,                    
                    RecordType.Name,
                    RecordType.DeveloperName,
                    Contact_Title__c,
                    Contact_First_Name__c,
                    Contact_Surname__c,
                    Contact_Street__c,
                    Contact_Suburb__c,
                    Contact_State__c,
                    Contact_Postcode__c,
                    Relationship_to_Injured_Person__c,
                    Contact_Email__c,
                    Contact_Home_Phone__c,
                    Contact_Work_Phone__c,
                    Contact_Mobile_Phone__c,
                    Contact_Date_of_Birth__c,
                    Is_an_Interpreter_Required_for_Contact__c,
                    Contact_Interpreter_Language__c,
                    Contact_ATSI_origin__c,
                    Contact_preferred_written_communication__c,
                    Contact_preferred_phone_number__c,                    
                    Lawful_authority_act_on_behalf__c,
                    Birth_Certificate__c,
                    Guardianship_Order__c,
                    Custody_Order__c,
                    If_yes_details_of_lawful_authority__c,
                    (SELECT Id
                     FROM Motor_Vehicles__r)
             FROM Form__c
             WHERE Id = :f.Id];   
                                                 
        SubmitFormExtension ext = new SubmitFormExtension(new ApexPages.StandardController(f));     
                        
        ext.SelectedParentGuardian = data.ParentGuardian.Id;       
        
        ext.Process();         
    } 
    
    static testMethod void testAppFormInsurer2()
    {
        TestLoadFormData data = new TestLoadFormData();
                                           
        Form__c f = new Form__c();
        f.Client__c = data.Client.Id;
        f.Signature_Date__c = Date.today();
        f.First_Name__c = 'test';
        f.Surname__c = 'test';
        f.Street__c = 'test';
        f.Suburb__c = 'test';
        f.State__c = 'test';
        f.Postcode__c = 'test';
        f.Home_Phone__c = '123456789';
        f.SAPOL_Vehicle_Collision_Report_Number__c = 'test';
        f.Date_of_Accident__c = Date.today();
        f.Time_of_Accident_hours__c = '13';
        f.Time_of_Accident_minutes__c = '00';
        f.Location_of_Accident__c = 'test';
        f.Injured_person_s_part_in_accident__c = 'test';                
        f.Injured_person_s_description_of_accident__c = 'test';        
        f.Applicant_s_injuries__c = 'test';
        f.RecordTypeId = data.RecordTypes.get('Application_Form_from_Insurer').Id;          
        insert f;
        
        Form_Detail__c fd = new Form_Detail__c(Form_Motor_Vehicle__c = f.Id,
                                               RecordTypeId = data.RecordTypes.get('Motor_Vehicle').Id);
        insert fd;                                               
        
        f = [SELECT Id,                    
                    Client__c,
                    Signature_Date__c,
                    Title__c,
                    First_Name__c,
                    Surname__c,
                    Street__c,
                    Suburb__c,
                    State__c,
                    Postcode__c,
                    Home_Phone__c,
                    SAPOL_Vehicle_Collision_Report_Number__c,
                    Date_of_Accident__c,
                    Time_of_Accident_hours__c,
                    Time_of_Accident_minutes__c,
                    Location_of_Accident__c,
                    Injured_person_s_part_in_accident__c,
                    Injured_person_s_description_of_accident__c,
                    Applicant_s_injuries__c,                    
                    RecordType.Name,
                    RecordType.DeveloperName,
                    Contact_Title__c,
                    Contact_First_Name__c,
                    Contact_Surname__c,
                    Contact_Street__c,
                    Contact_Suburb__c,
                    Contact_State__c,
                    Contact_Postcode__c,
                    Relationship_to_Injured_Person__c,
                    Contact_Email__c,
                    Contact_Home_Phone__c,
                    Contact_Work_Phone__c,
                    Contact_Mobile_Phone__c,
                    Contact_Date_of_Birth__c,
                    Is_an_Interpreter_Required_for_Contact__c,
                    Contact_Interpreter_Language__c,
                    Contact_ATSI_origin__c,
                    Contact_preferred_written_communication__c,
                    Contact_preferred_phone_number__c,                    
                    Lawful_authority_act_on_behalf__c,
                    Birth_Certificate__c,
                    Guardianship_Order__c,
                    Custody_Order__c,
                    If_yes_details_of_lawful_authority__c,
                    (SELECT Id
                     FROM Motor_Vehicles__r)
             FROM Form__c
             WHERE Id = :f.Id];   
                                                 
        SubmitFormExtension ext = new SubmitFormExtension(new ApexPages.StandardController(f));     
                                        
        ext.Process();         
    }        
}