public with sharing class ServiceProviderExtension
    extends A2HCPageBase
{
    public ServiceProviderExtension()
    {
    }

	public ServiceProviderExtension(object controller)
    {
    }
/*
    public ServiceProviderExtension(NeedsExtension controller)
    {
        if(controller.Needs_Identification != null)
        {
            SelectedReferralSource = controller.Needs_Identification.HACC_Referral_Source__c;
        }
    }
*/
    public ServiceProviderExtension(ApexPages.StandardController controller)
    {
    }

    // cut out a bit of view state, so make them static
    private static List<SelectOption> allProviders;
    private static List<SelectOption> currentProviders;
    private transient List<SelectOption> referralSources;
    private transient List<SelectOption> allocationProviders;
    private static List<SelectOption> eReferralProviders;
    private transient List<SelectOption> referralSourceProviders;

    private enum ProviderType
    {
        All,
        Current,
        Allocation,
        ReferralSource
    }

    public String SelectedReferralSource
    {
        get;
        set
        {
            // force a refresh of the referral source provder list on change
            if(SelectedReferralSource == null || SelectedReferralSource != value)
            {
                referralSourceProviders = null;
            }
            SelectedReferralSource = value;
        }
    }

    public List<SelectOption> AllProviderList
    {
        get
        {
            if(allProviders == null)
            {
                allProviders = loadList(ProviderType.All);
            }
            return allProviders;
        }
    }

    public List<SelectOption> CurrentProviderList
    {
        get
        {
            if(currentProviders == null)
            {
                currentProviders = loadList(ProviderType.Current);
            }
            return currentProviders;
        }
    }

    public List<SelectOption> ReferralSourceList
    {
        get
        {
            if(referralSources == null)
            {
                referralSources = getReferralSourceList();
            }
            return referralSources;
        }
    }

    public List<SelectOption> ReferralSourceProviderList
    {
        get
        {
            if(referralSourceProviders == null)
            {
                referralSourceProviders = loadList(ProviderType.ReferralSource);
            }
            return referralSourceProviders;
        }
    }

    public List<SelectOption> AllocationProviderList
    {
        get
        {
            if(allocationProviders == null)
            {
                allocationProviders = loadList(ProviderType.Allocation);
            }
            return allocationProviders;
        }
    }

    public List<SelectOption> RCRProviderList
    {
        get
        {
        	if(RCRProviderList == null)
        	{
        		RCRProviderList = new List<SelectOption>();
        		RCRProviderList.add(new SelectOption('', '<All>'));
        		for(Account a :	[SELECT	ID,
										Name
								FROM	Account
								WHERE	Available_To__c INCLUDES ('RCR')
								ORDER BY Name])
				{
					RCRProviderList.add(new SelectOption(a.ID, a.Name));
				}
        	}
			return RCRProviderList;
        }
        set;
    }

    public PageReference getProviders()
    {
        return null;
    }

    private List<SelectOption> loadList(ProviderType listType)
    {
        try
        {
            List<SelectOption> optionList = new List<SelectOption>();
/*
            ID serviceProviderRecordType = A2HCUtilities.getRecordType('Account', 'Service Provider').ID;

			Map<String, Integer> eRefAccounts = new Map<String, Integer>();
			if(listType == ProviderType.Allocation)
			{
				for(AggregateResult aggResult : [SELECT	AccountID,
														COUNT(Name) UserCount
												FROM	User
												WHERE	Receive_eReferral_Email__c = true
												GROUP BY AccountID])
				{
					eRefAccounts.put((String)aggResult.get('AccountID'), (Integer)aggResult.get('UserCount'));
				}
			}
            optionList.add(new SelectOption('', '--None--'));

            for(Account org : [SELECT   a.Name, 
                                        a.ID,
                                        a.Used_in_Alt_IDs__c,
                                        a.Current_Service_Providers__c,
                                        a.Allocation_Providers__c,
                                        a.HACC_Referral_Source__c,
                                        (SELECT ID
                                        FROM    Sites__r)
                               FROM     Account a
                               WHERE    RecordTypeId = :serviceProviderRecordType
                               ORDER BY a.Name])
            {
                Boolean ignore =   (listType != ProviderType.All ) &&
                                    (
                                        (listType == ProviderType.Current && !org.Current_Service_Providers__c) ||
                                        (listType == ProviderType.Allocation && !org.Allocation_Providers__c) ||
                                        (listType == ProviderType.ReferralSource &&
                                            (org.HACC_Referral_Source__c == null ||
                                            org.HACC_Referral_Source__c == 'Not stated/inadequately described')) // Bug 84
                                    );

				Boolean addAsterisk = false;
                if(!ignore)
                {
                    if(listType == ProviderType.Allocation)
                    {
                    	if(org.Sites__r.size() > 0 || eRefAccounts.get(org.ID) > 0)
                    	{
                    		addAsterisk = true;
                    	}
                    }
                    optionList.add(new SelectOption(org.ID, org.Name + (addAsterisk ? '*' : '')));
                }
            }*/
            return optionList;
        }
        catch (Exception e)
        {
            A2HCException.formatException(e);
            return  new List<SelectOption>();
        }
    }

    private List<SelectOption> getReferralSourceList()
    {
        Schema.DescribeFieldResult descResult = Account.HACC_Referral_Source__c.getDescribe();
        referralSources = new List<SelectOption>();
        referralSources.add(new SelectOption('', '--None--'));
        for(PickListEntry entry : descresult.getPicklistValues())
        {

            referralSources.add(new SelectOption(entry.getLabel(), entry.getValue()));
        }
        return referralSources;
    }
}