public with sharing class RCRGroupAgreementExtension 
	extends A2HCPageBase
{
	public RCRGroupAgreementExtension(ApexPages.StandardController ctrl)
	{
		controller = ctrl;
	} 
	
	private ApexPages.StandardController controller;
	
	public List<RCR_Contract__c> ServiceOrders
	{
		get
		{
			if(ServiceOrders == null)
			{
				ServiceOrders = new List<RCR_Contract__c>();
				for(RCR_Contract__c so :  AllServiceOrders)
				{
					if((!InPortal ||
							(InPortal &&
							so.Visible_to_Service_Providers__c)) &&
							(so.RecordType.Name == APSRecordTypes.RCRServiceOrder_ServiceOrder ||
							so.RecordType.Name == APSRecordTypes.RCRServiceOrder_ServiceOrderCBMS))
					{
						ServiceOrders.add(so);
					}
				}
			}
			return ServiceOrders;
		}
		private set;
	}
	
	public List<RCR_Contract__c> Requests
	{
		get
		{
			if(Requests == null)
			{
				Requests = new List<RCR_Contract__c>();
				for(RCR_Contract__c so :  AllServiceOrders)
				{
					if(!InPortal &&
							(so.RecordType.Name == APSRecordTypes.RCRServiceOrder_NewRequest ||
							so.RecordType.Name == APSRecordTypes.RCRServiceOrder_Amendment) &&
							so.Status__c != APS_PicklistValues.RCRContract_Status_Approved &&
							so.Status__c != APS_PicklistValues.RCRContract_Status_Rejected)
					{
						Requests.add(so);
					}
				}				
			}
			return Requests;
		}
		private set;
	}
	
	private List<RCR_Contract__c> AllServiceOrders
	{
		get
		{
			if(AllServiceOrders == null)
			{
				AllServiceOrders = [SELECT	ID,
											Name,
											Status__c,
											Client_Name__c,
											Start_Date__c,
											Original_End_Date__c,
											Cancellation_Date__c,
											Total_Adhoc_Activity_Cost__c,
											Total_Scheduled_Service_Cost__c,
											Total_Service_Order_Cost__c,
											Amendment_Cost_Difference__c,
											Visible_to_Service_Providers__c,
											Client__c,
											Client__r.Name,
											RecordType.Name											
									FROM	RCR_Contract__c
									WHERE	RCR_Group_Agreement__c = :controller.getID()
									ORDER BY Start_Date__c DESC, Name];
			}
			return AllServiceOrders;
		}
		set;
	}
	
	public PageReference submitWithdrawl()
	{
		try
		{
			/*RCR_Contract__c so = (RCR_Contract__c)controller.getRecord();
			Boolean valid = isRequiredFieldValid(so.Withdrawl_Date__c, 'Withdrawl Date', true);
			valid = isRequiredFieldValid(so.Withdrawl_Reason__c, 'Withdrawl Reason', valid);
			if(!valid)
			{
				return null;
			}
			return controller.save();*/
			return null;
		}
		catch(Exception ex)
		{
			return A2HCException.formatException(ex);
		}
	}
}