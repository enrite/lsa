public with sharing class RCRUpdNegRatesAdHocServices extends A2HCPageBase
{
    public RCRUpdNegRatesAdHocServices()
    {
        disableUpdButton = false;
    }
    
    
    public string SelectedAccount
    {
        get;
        set;
    }
    
    public string ServiceOrderNumber
    {
        get;
        set;
    }
    
    public boolean IncludeExpenses
    {
        get
        {
            if(IncludeExpenses == null)
            {
                return false;
            }
            return IncludeExpenses;
        }
        set;
    }
    
    public boolean IncludeAlreadyIndexed
    {
        get
        {
            if(IncludeAlreadyIndexed == null)
            {
                IncludeAlreadyIndexed = false;
            }
            return IncludeAlreadyIndexed;
        }
        set;
    }
    
    public decimal NegotiatedRateIncrease
    {
        get;
        set;
    }
    
    public decimal AdHocServiceIncrease
    {
        get;
        set;
    }
    
    public boolean disableUpdButton
    {
        get;
        set;
    }
    
    public decimal TotalCurrentCost
    {
        get
        {
            if(TotalCurrentCost == null)
            {
                TotalCurrentCost = 0.0;
            }
            return TotalCurrentCost;
        }
        private set;
    }
    
    public decimal TotalNewCost
    {
        get
        {
            if(TotalNewCost == null)
            {
                TotalNewCost = 0.0;
            }
            return TotalNewCost;
        }
        private set;
    }
    
    public decimal TotalVariation
    {
        get
        {
            if(TotalVariation == null)
            {
                TotalVariation = 0.0;
            }
            return TotalVariation;
        }
        private set;
    }
    
    public List<SelectOption> Accounts
    {
        get
        {
            if(Accounts == null)
            {
                Set<Id> accountIDs = new Set<Id>();
                for(RCR_Contract_Adhoc_Service__c ahs : [SELECT RCR_Contract__r.Account__c
	                                                     FROM   RCR_Contract_Adhoc_Service__c
	                                                     WHERE  RCR_Contract__r.RecordType.Name = :APSRecordTypes.RCRServiceOrder_Rollover
	                                                     AND    RCR_Contract__r.Rollover_Status__c = :APS_PicklistValues.RCRContract_RolloverStatus_ReadyForAllocation
	                                                     AND    RCR_Contract__r.Schedule__c = null])
                {
                	accountIDs.add(ahs.RCR_Contract__r.Account__c);
                }
                
                for(RCR_Contract_Scheduled_Service__c css : [SELECT RCR_Contract__r.Account__c
                                                             FROM   RCR_Contract_Scheduled_Service__c
                                                             WHERE  RCR_Contract__r.RecordType.Name = :APSRecordTypes.RCRServiceOrder_Rollover
                                                             AND    RCR_Contract__r.Rollover_Status__c = :APS_PicklistValues.RCRContract_RolloverStatus_ReadyForAllocation
                                                             AND    RCR_Contract__r.Schedule__c = null])
                {
                	accountIDs.add(css.RCR_Contract__r.Account__c);
                }
                Accounts = getSelectListFromObjectList([SELECT ID,
					                							Name
					        							FROM 	Account 
					        							WHERE 	ID IN :accountIDs
					        							ORDER BY Name], true);
            }
            return Accounts;
        }
        set;
    }
    
    public Map<string, RolloverWrapper> RolloverWrappers
    {
        get
        {
            if(RolloverWrappers == null)
            {
                RolloverWrappers = new Map<string, RolloverWrapper>();
            }
            return RolloverWrappers;
        }
        private set;
    }
    
    @TestVisible
    private List<RCR_Contract_Scheduled_Service__c> AllScheduledServices
    {
        get
        {
            if(AllScheduledServices == null)
            {
                AllScheduledServices = new List<RCR_Contract_Scheduled_Service__c>();
            }
            return AllScheduledServices;
        }
        private set;
    }
    
    @TestVisible
    private List<RCR_Contract_Adhoc_Service__c> AllAdHocServices
    {
        get
        {
            if(AllAdHocServices == null)
            {
                AllAdHocServices = new List<RCR_Contract_Adhoc_Service__c>();
            }
            return AllAdHocServices;
        }
        private set;
    }
    
    
    public PageReference preview()
    {
        try
        {       
            RolloverWrappers = null;
            AllAdHocServices = null;
            AllScheduledServices = null;
            TotalCurrentCost = null;
            TotalNewCost = null;
            TotalVariation = null;
            
            disableUpdButton = false;
    
            if(SelectedAccount == '' || SelectedAccount == null)
            {
                return A2HCException.formatException('You must select an \'Account\'.');
            }
            if(NegotiatedRateIncrease == 0.0 && AdHocServiceIncrease == 0.0)
            {
                return A2HCException.formatException('You must provide a \'Negotiated Rate Increase\' or \'AdHoc Service Increase\' or both.');
            }
            
            if(AdHocServiceIncrease != 0.0)
            {
                for(RCR_Contract_Adhoc_Service__c ahs : [SELECT Id,
                                                                Name,
                                                                Amount__c,
                                                                Rate__c,
                                                                RCR_Contract__r.Client__r.Name,
                                                                RCR_Contract__r.Name,
                                                                (SELECT Id,
                                                                        Name
                                                                 FROM   RCR_Negotiated_and_AdHoc_Indexations__r)
                                                         FROM   RCR_Contract_Adhoc_Service__c
                                                         WHERE  RCR_Contract__r.Account__c = :SelectedAccount
                                                         AND    RCR_Contract__r.Name like :(ServiceOrderNumber != '' ? ServiceOrderNumber : '%')
                                                         AND    RCR_Contract__r.RecordType.Name = :APSRecordTypes.RCRServiceOrder_Rollover
                                                         AND    RCR_Contract__r.Rollover_Status__c = :APS_PicklistValues.RCRContract_RolloverStatus_ReadyForAllocation
                                                         AND    RCR_Contract__r.Schedule__c = null
                                                         AND    (Expenses__c = false OR Expenses__c = :IncludeExpenses)])
                {
                    if(ahs.RCR_Negotiated_and_AdHoc_Indexations__r.size() == 0 || IncludeAlreadyIndexed)
                    {
                        AllAdHocServices.add(ahs);
                                    
                        if(!RolloverWrappers.containsKey(ahs.RCR_Contract__r.Client__r.Name))
                        {
                            RolloverWrappers.put(ahs.RCR_Contract__r.Client__r.Name, new RolloverWrapper(ahs.RCR_Contract__r.Client__r.Name));
                        }
                        RolloverWrapper wrap = RolloverWrappers.get(ahs.RCR_Contract__r.Client__r.Name);
                        
                        if(!wrap.ServiceRequests.containsKey(ahs.RCR_Contract__r.Name))
                        {
                            wrap.ServiceRequests.put(ahs.RCR_Contract__r.Name, new ServiceRequestWrapper(ahs.RCR_Contract__r.Name, ahs.RCR_Contract__c));
                        }
                        wrap.ServiceRequests.get(ahs.RCR_Contract__r.Name).Services.add(new ServiceWrapper(ahs, NegotiatedRateIncrease, AdHocServiceIncrease));             
                        
                        if(ahs.RCR_Negotiated_and_AdHoc_Indexations__r.size() > 0)
                        {
                            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, 'Increases have already been applied to some of the Scheduled or AdHoc Services.');
                            ApexPages.addMessage(msg);
                        }
                    }
                }
            }
            
            if(NegotiatedRateIncrease != 0.0)
            {
                for(RCR_Contract_Scheduled_Service__c css : [SELECT Id,
                                                                    Name,
                                                                    Total_Cost__c,
                                                                    Negotiated_Rate_Standard__c,
                                                                    Negotiated_Rate_Public_Holidays__c,
                                                                    RCR_Contract__r.Client__r.Name,
                                                                    RCR_Contract__r.Name,
                                                                    RCR_Service__r.Name,
                                                                    RCR_Service__r.Description__c,
                                                                    (SELECT Id,
                                                                            Name
                                                                     FROM   RCR_Negotiated_and_AdHoc_Indexations__r)
                                                             FROM   RCR_Contract_Scheduled_Service__c
                                                             WHERE  RCR_Contract__r.Account__c = :SelectedAccount
                                                             AND    RCR_Contract__r.Name like :(ServiceOrderNumber != '' ? ServiceOrderNumber : '%')
                                                             AND    RCR_Contract__r.RecordType.Name = :APSRecordTypes.RCRServiceOrder_Rollover
                                                             AND    RCR_Contract__r.Rollover_Status__c = :APS_PicklistValues.RCRContract_RolloverStatus_ReadyForAllocation
                                                             AND    RCR_Contract__r.Schedule__c = null
                                                             AND    Use_Negotiated_Rates__c = true
                                                             AND    (Expenses__c = false OR Expenses__c = :IncludeExpenses)])
                {
                    if(css.RCR_Negotiated_and_AdHoc_Indexations__r.size() == 0 || IncludeAlreadyIndexed)
                    {
                        AllScheduledServices.add(css);
                        
                        if(!RolloverWrappers.containsKey(css.RCR_Contract__r.Client__r.Name))
                        {
                            RolloverWrappers.put(css.RCR_Contract__r.Client__r.Name, new RolloverWrapper(css.RCR_Contract__r.Client__r.Name));
                        }
                        RolloverWrapper wrap = RolloverWrappers.get(css.RCR_Contract__r.Client__r.Name);
                        
                        if(!wrap.ServiceRequests.containsKey(css.RCR_Contract__r.Name))
                        {
                            wrap.ServiceRequests.put(css.RCR_Contract__r.Name, new ServiceRequestWrapper(css.RCR_Contract__r.Name, css.RCR_Contract__c));
                        }
                        wrap.ServiceRequests.get(css.RCR_Contract__r.Name).Services.add(new ServiceWrapper(css, NegotiatedRateIncrease, AdHocServiceIncrease));
                        
                        if(css.RCR_Negotiated_and_AdHoc_Indexations__r.size() > 0 && !ApexPages.hasMessages())
                        {
                            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, 'Increases have already been applied to some of the Scheduled or AdHoc Services.');
                            ApexPages.addMessage(msg);
                        }
                    }
                }
            }
            
            // Calculate Totals
            for(string key1 : RolloverWrappers.keySet())
            {
                for(string key2 : RolloverWrappers.get(key1).ServiceRequests.keySet())
                {
                    for(ServiceWrapper servWrap : RolloverWrappers.get(key1).ServiceRequests.get(key2).Services)
                    {
                        TotalCurrentCost += servWrap.CurrentCost;
                        TotalNewCost += servWrap.NewCost;
                        TotalVariation += servWrap.CostVariation;
                    }
                }
            }
            return null;
        }
        catch(Exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }
    
    
    public PageReference updateRatesAndCosts()
    {
        try
        {
            List<Id> scheduledServiceIDs = new List<Id>();
            List<RCR_Negotiated_and_AdHoc_Indexation__c> indexationAuditRecords = new List<RCR_Negotiated_and_AdHoc_Indexation__c>();
            
            AggregateResult[] result = [SELECT  MAX(Indexation_Number__c) maxIndexationNumber 
                                        FROM    RCR_Negotiated_and_AdHoc_Indexation__c];

            decimal indexationNumber = (decimal)result[0].get('maxIndexationNumber');
            indexationNumber = indexationNumber == null ? 1 : indexationNumber + 1;
            
            for(RCR_Contract_Scheduled_Service__c css :  AllScheduledServices)
            {
                decimal originalStandardRate = css.Negotiated_Rate_Standard__c;
                decimal originalPublicHolidayRate = css.Negotiated_Rate_Public_Holidays__c;
                decimal originalCost = css.Total_Cost__c;
                
                scheduledServiceIDs.add(css.Id);
                
                css.Negotiated_Rate_Standard__c = css.Negotiated_Rate_Standard__c + (css.Negotiated_Rate_Standard__c * (NegotiatedRateIncrease / 100));
                css.Negotiated_Rate_Public_Holidays__c = css.Negotiated_Rate_Public_Holidays__c + (css.Negotiated_Rate_Public_Holidays__c * (NegotiatedRateIncrease / 100));
                
                // Create Auditing Record.  
                indexationAuditRecords.add(createAuditRecord(indexationNumber, originalStandardRate, originalPublicHolidayRate, originalCost, css));
            }
            
            for(RCR_Contract_Adhoc_Service__c ahs : AllAdHocServices)
            {
                decimal originalStandardRate = ahs.Rate__c;
                decimal originalCost = ahs.Amount__c;
                
                ahs.Amount__c = ahs.Amount__c + (ahs.Amount__c * (AdHocServiceIncrease / 100));
                if(ahs.Rate__c != null && ahs.Rate__c != 0.0)
                {
                    ahs.Rate__c = ahs.Rate__c + (ahs.Rate__c * (AdHocServiceIncrease / 100));
                }
                
                // Create Auditing Record.  
                indexationAuditRecords.add(createAuditRecord(indexationNumber, originalStandardRate, null, originalCost, ahs));
            }

            update AllScheduledServices;
            update AllAdHocServices;
            upsert IndexationAuditRecords;
            
            if(AllScheduledServices.size() > 0)
            {
                RCRBatchUpdateScheduledServiceTotalCost batch = new RCRBatchUpdateScheduledServiceTotalCost(); 
                batch.runMode = RCRBatchUpdateScheduledServiceTotalCost.Mode.SpecificScheduledServices;
                batch.cssIDs = scheduledServiceIDs;
                ID batchprocessid = Database.executeBatch(batch, Batch_Size__c.getInstance('Rate_Recalculation').Records__c.IntValue());    
            }
            
            disableUpdButton = true;

            return null;
        }
        catch(Exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }
    
    
    public PageReference cancelOverride()
    {
        try
        {
            return Page.RCR;
        }
        catch(exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }
    
    
    private RCR_Negotiated_and_AdHoc_Indexation__c createAuditRecord(decimal indexationNumber, decimal originalStandardRate, decimal originalPublicHolidayRate, decimal originalCost, sObject obj)
    {
        RCR_Negotiated_and_AdHoc_Indexation__c auditRecord = new RCR_Negotiated_and_AdHoc_Indexation__c(Indexation_Number__c = indexationNumber);
        
        if(obj instanceof RCR_Contract_Scheduled_Service__c)
        {
            RCR_Contract_Scheduled_Service__c css = (RCR_Contract_Scheduled_Service__c)obj;
            auditRecord.RCR_Service_Order__c = css.RCR_Contract__c;
            auditRecord.RCR_Contract_Scheduled_Service__c = css.Id;
            auditRecord.Standard_Rate_Before_Indexation__c = originalStandardRate.setScale(2);
            auditRecord.Standard_Rate_After_Indexation__c = css.Negotiated_Rate_Standard__c;
            auditRecord.Public_Holiday_Rate_Before_Indexation__c = originalPublicHolidayRate;
            auditRecord.Public_Holiday_Rate_After_Indexation__c = css.Negotiated_Rate_Public_Holidays__c;
            auditRecord.Cost_Before_Indexation__c = originalCost;
            auditRecord.Cost_After_Indexation__c = css.Total_Cost__c;
        }
        else if(obj instanceof RCR_Contract_Adhoc_Service__c)
        {
            RCR_Contract_Adhoc_Service__c ahs = (RCR_Contract_Adhoc_Service__c)obj;
            auditRecord.RCR_Service_Order__c = ahs.RCR_Contract__c;
            auditRecord.RCR_Contract_Adhoc_Service__c = ahs.Id;
            auditRecord.Standard_Rate_Before_Indexation__c = originalStandardRate;
            auditRecord.Standard_Rate_After_Indexation__c = ahs.Rate__c;
            auditRecord.Cost_Before_Indexation__c = originalCost;
            auditRecord.Cost_After_Indexation__c = ahs.Amount__c;
        }
        return auditRecord;
    }
    
    
    public class RolloverWrapper
    {
        public RolloverWrapper(string pClientOrGroupName)
        {
            ClientOrGroupName = pClientOrGroupName;
        }
        
        public string ClientOrGroupName
        {
            get;
            private set;
        }
        
        public Map<string, ServiceRequestWrapper> ServiceRequests
        {
            get
            {
                if(ServiceRequests == null)
                {
                    ServiceRequests = new Map<string, ServiceRequestWrapper>();
                }
                return ServiceRequests;
            }
            set;
        }
    }
    
    
    public class ServiceRequestWrapper
    {
        public ServiceRequestWrapper(string pRequestName, Id pRequestID)
        {
            RequestName = pRequestName;
            RequestID = pRequestID;
        }
        
        public string RequestName
        {
            get;
            set;
        }
        
        public Id RequestID
        {
            get;
            set;
        }

        public List<ServiceWrapper> Services
        {
            get
            {
                if(Services == null)
                {
                    Services = new List<ServiceWrapper>();
                }
                return Services;
            }
            private set;
        }
    }
    
    
    public class ServiceWrapper
    {
        public ServiceWrapper(sObject pService, decimal pNegotiatedIncrease, decimal pAdHocIncrease)
        {
            Service = pService;
            negotiatedIncrease = pNegotiatedIncrease;
            adHocIncrease = pAdHocIncrease;
        }
        
        private decimal negotiatedIncrease;
        private decimal adHocIncrease;
        
        public sObject Service
        {
            get;
            private set;
        }
        
        public string ImpactedService
        {
            get
            {
                if(Service instanceof RCR_Contract_Adhoc_Service__c)
                {
                    return 'AdHoc Service';
                }
                else
                {
                    RCR_Contract_Scheduled_Service__c css = (RCR_Contract_Scheduled_Service__c)Service;
                    return css.RCR_Service__r.Name;
                }
            }
        }
        
        public string Description
        {
            get
            {
                if(Service instanceof RCR_Contract_Adhoc_Service__c)
                {
                    return '';
                }
                else
                {
                    RCR_Contract_Scheduled_Service__c css = (RCR_Contract_Scheduled_Service__c)Service;
                    return css.RCR_Service__r.Description__c;
                }
            }
        }
        
        
        public decimal CurrentRateStandard
        {
            get
            {
                if(Service instanceof RCR_Contract_Adhoc_Service__c)
                {
                    return null;
                }
                else
                {
                    RCR_Contract_Scheduled_Service__c css = (RCR_Contract_Scheduled_Service__c)Service;
                    return css.Negotiated_Rate_Standard__c;
                }
            }
        }
        
        public decimal CurrentRatePublicHoliday
        {
            get
            {
                if(Service instanceof RCR_Contract_Adhoc_Service__c)
                {
                    return null;
                }
                else
                {
                    RCR_Contract_Scheduled_Service__c css = (RCR_Contract_Scheduled_Service__c)Service;
                    return css.Negotiated_Rate_Public_Holidays__c;
                }
            }
        }
        
        public decimal NewRateStandard
        {
            get
            {
                if(Service instanceof RCR_Contract_Adhoc_Service__c)
                {
                    return null;
                }
                else
                {
                    RCR_Contract_Scheduled_Service__c css = (RCR_Contract_Scheduled_Service__c)Service;
                    return (css.Negotiated_Rate_Standard__c + (css.Negotiated_Rate_Standard__c * (negotiatedIncrease / 100)));
                }
            }
        }
        
        public decimal NewRatePublicHoliday
        {
            get
            {
                if(Service instanceof RCR_Contract_Adhoc_Service__c)
                {
                    return null;
                }
                else
                {
                    RCR_Contract_Scheduled_Service__c css = (RCR_Contract_Scheduled_Service__c)Service;
                    return (css.Negotiated_Rate_Public_Holidays__c + (css.Negotiated_Rate_Public_Holidays__c * (negotiatedIncrease / 100)));
                }
            }
        }
        
        public decimal CurrentCost
        {
            get
            {
                if(Service instanceof RCR_Contract_Adhoc_Service__c)
                {
                    RCR_Contract_Adhoc_Service__c ahs = (RCR_Contract_Adhoc_Service__c)Service;
                    return ahs.Amount__c;
                }
                else
                {
                    RCR_Contract_Scheduled_Service__c css = (RCR_Contract_Scheduled_Service__c)Service;
                    return css.Total_Cost__c;
                }
            }
        }
        
        public decimal NewCost
        {
            get
            {
                if(Service instanceof RCR_Contract_Adhoc_Service__c)
                {
                    RCR_Contract_Adhoc_Service__c ahs = (RCR_Contract_Adhoc_Service__c)Service;
                    return (ahs.Amount__c + (ahs.Amount__c * (adHocIncrease / 100)));
                }
                else
                {
                    RCR_Contract_Scheduled_Service__c css = (RCR_Contract_Scheduled_Service__c)Service;
                    return (css.Total_Cost__c + (css.Total_Cost__c * (negotiatedIncrease / 100)));
                }
            }
        }
        
        public decimal CostVariation
        {
            get
            {
                if(Service instanceof RCR_Contract_Adhoc_Service__c)
                {
                    RCR_Contract_Adhoc_Service__c ahs = (RCR_Contract_Adhoc_Service__c)Service;
                    return ahs.Amount__c * (adHocIncrease / 100);
                }
                else
                {
                    RCR_Contract_Scheduled_Service__c css = (RCR_Contract_Scheduled_Service__c)Service;
                    return css.Total_Cost__c * (negotiatedIncrease / 100);
                }
            }
        }
        
        public boolean IndexationAlreadyApplied
        {
            get
            {
                if(Service instanceof RCR_Contract_Adhoc_Service__c)
                {
                    RCR_Contract_Adhoc_Service__c ahs = (RCR_Contract_Adhoc_Service__c)Service;
                    if(ahs.RCR_Negotiated_and_AdHoc_Indexations__r.size() > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    RCR_Contract_Scheduled_Service__c css = (RCR_Contract_Scheduled_Service__c)Service;
                    if(css.RCR_Negotiated_and_AdHoc_Indexations__r.size() > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }
    }
    
    
}