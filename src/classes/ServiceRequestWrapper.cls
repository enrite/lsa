public class ServiceRequestWrapper
	extends A2HCPageBase
{	
	public ServiceRequestWrapper(RCR_Contract_Scheduled_Service__c pScheduledService,
									Integer pRowIndex)
	{
		ScheduledService = pScheduledService;
		RowIndex = pRowIndex;

		SchedController = new ScheduleController();
		SchedController.ParentRecordID = ScheduledService.ID;	
	}

	public ScheduleController SchedController
	{
		get;
		set;
	}

	public Integer RowIndex
	{
		get;
		set;
	}

	public String DayOfWeek
	{
		get
		{
			return ScheduledService == null ? '' : ScheduledService.RCR_Service__r.Day_of_Week__c;
		}
	}

	public RCR_Contract_Scheduled_Service__c ScheduledService
	{
		get;
		set;
	}

	public List<RCR_Contract_Service_Type__c> ServiceTypes
	{
		get
		{
			if(ServiceTypes == null)
			{
				ServiceTypes = [SELECT ID,
										RCR_Program_Category_Service_Type__c,
										RCR_Program_Category_Service_Type__r.Name,
										RCR_Program__c,
										Quantity__c,
										Allocation_Service_Type__c,
										RCR_Contract_Scheduled_Service__c,
										RCR_Contract_Adhoc_Service__c
								FROM	RCR_Contract_Service_Type__c
								WHERE	RCR_Contract_Scheduled_Service__c = :ScheduledService.ID AND
										RCR_Contract_Scheduled_Service__c != null
								ORDER BY RCR_Program_Category_Service_Type__r.Name];
			}
			return ServiceTypes;
		}
		set;
	}
}