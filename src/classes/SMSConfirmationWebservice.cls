@RestResource(urlMapping='/SMSDelivery/*')
global without sharing class SMSConfirmationWebservice 
{   
    // call using URL https://<base URL>.force.com/services/apexrest/SMSDelivery/confirm?smsref=<refNumber>&status=<status>
    @HttpGet
    global static void confirm()
    {
        String smsref = RestContext.request.params.get('smsref');
        String status = RestContext.request.params.get('status');
        updateSMSNotification(smsref, status);
    }

    @TestVisible
    private static void updateSMSNotification(String referenceNumber, String status)
    {                    
        for(SMS_Message_Recipient__c sms : [SELECT  ID
                                            FROM    SMS_Message_Recipient__c
                                            WHERE   Gateway_Reference__c = :referenceNumber AND
                                                    Gateway_Reference__c != null])
        {
            sms.Message_Delivered__c = Datetime.now();
            sms.Message_Status__c = status;
            update sms;
        }  
    }
}