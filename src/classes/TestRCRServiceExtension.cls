/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestRCRServiceExtension 
{
    
    static testMethod void myUnitTest() 
    {
        TestLoadData.loadRCRdata();
        
        Test.StartTest();
        
        Test.setCurrentPage(new PageReference('/apex/RCRCreateServiceTypeServices'));
        
        RCR_Service__c service = [SELECT Id FROM RCR_Service__c LIMIT 1];
        
        ApexPages.currentPage().getParameters().put('retURL', service.Id);
        RCRServiceExtension ext = new RCRServiceExtension(new ApexPages.Standardcontroller(service)); 
        
        ext.cancelOverride();

        ApexPages.currentPage().getParameters().put('srvcId', service.Id);
        ext = new RCRServiceExtension(new ApexPages.Standardcontroller(service));
        
        for(integer i = 0; i < ext.NDAApprovedServiceTypes.size(); i++)
        {
            ext.NDAApprovedServiceTypes[i].Selected = false;
        }
        
        for(integer i = 0; i < ext.HACCApprovedServiceTypes.size(); i++)
        {
            ext.HACCApprovedServiceTypes[i].Selected = false;
        }

        ext.SaveOverride();
        
        ApexPages.currentPage().getParameters().put('retURL', service.Id);
        ext = new RCRServiceExtension(new ApexPages.Standardcontroller(service));
        
        for(integer i = 0; i < ext.NDAApprovedServiceTypes.size(); i++)
        {
            ext.NDAApprovedServiceTypes[i].Selected = true;
        }
        
        
        for(integer i = 0; i < ext.HACCApprovedServiceTypes.size(); i++)
        {
            ext.HACCApprovedServiceTypes[i].Selected = true;
        }
        
        System.debug('NDAApprovedServiceTypes.size(): ' + ext.NDAApprovedServiceTypes.size());

        ext.SaveOverride();
        
        Test.StopTest();
    }
}