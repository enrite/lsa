public class GrantEmailAttachmentQueuable  implements Queueable, Database.AllowsCallouts
{
    public GrantEmailAttachmentQueuable(List<Grant_Application__c> applications)
    {
        this.applications = applications;
    }

    private List<Grant_Application__c> applications;

    public void execute(QueueableContext context)
    {
        List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();
        EmailTemplate template = [SELECT    ID
                                    FROM    EmailTemplate
                                    WHERE   DeveloperName = 'Grant_Awarded_u100k'];

        for(Grant_Application__c appl : applications)
        {
            List<Messaging.EmailFileAttachment> attachments = new List<Messaging.EmailFileAttachment>();
            Messaging.SingleEmailMessage msg = Messaging.renderStoredEmailTemplate(template.ID, appl.Contact__c, appl.ID);
system.debug(appl.Contact__c) ;
           msg.setTargetObjectId(appl.Contact__c);
 system.debug(msg.getToAddresses());
system.debug(msg.getTargetObjectId());
 system.debug(msg.getWhatId());
 system.debug(msg.getPlainTextBody());
            attachments.add(getAttachment(appl, Page.GrantApplicationAtt1, 'Attachment1.pdf'));
            attachments.add(getAttachment(appl, Page.GrantApplicationAtt1_1, 'Attachment1.1.pdf'));
            attachments.add(getAttachment(appl, Page.GrantApplicationAtt2, 'Attachment2.pdf'));
            attachments.add(getAttachment(appl, Page.GrantApplicationAtt3, 'Attachment3.pdf'));

            msg.setFileAttachments(attachments);
            messages.add(msg);
        }
        Messaging.sendEmail(messages);
    }

    private Messaging.EmailFileAttachment getAttachment(Grant_Application__c appl, PageReference pg, String fileName)
    {
        Messaging.EmailFileAttachment fAtt = new Messaging.EmailFileAttachment();
        pg.getParameters().put('id', appl.ID);
        Blob pgContent = pg.getContent();
        fAtt.setBody(pgContent);
        fAtt.setContentType('application/pdf');
        fAtt.setInline(false);
        fAtt.setFileName(fileName);
        return fAtt;
    }
}