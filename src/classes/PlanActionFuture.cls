public with sharing class PlanActionFuture 
{
	@future
    public static void createApprovalAudits(Set<ID> approvedPlanApprovalIDs)
    { 	
        TriggerBypass.PlanAction = true;
        TriggerBypass.PlanApproval = true;
        TriggerBypass.Plan = true;

    	List<Plan_Action_Audit__c> planActionAudits = new List<Plan_Action_Audit__c>();         
	    for (Plan_Action__c pa : [SELECT 	Id,
	                                       Approved__c,
	                                       Amendment_Details__c,
	                                       Clinical_Reasoning__c,
	                                       Estimated_Cost__c,
	                                       Necessary_Reasonable__c,
	                                       Necessary_and_Reasonable_Criteria__c,
	                                       Pre_Defined_Services__c,
	                                       Previously_Approved_Cost__c,
	                                       Provider__c,
	                                       Quote_Attached__c,
	                                       Item_Description__c,
	                                       Plan_Approval__c
	                                FROM    Plan_Action__c
	                                WHERE   Plan_Approval__c IN :approvedPlanApprovalIDs])
	    {
	        Plan_Action_Audit__c paa = new Plan_Action_Audit__c();
	        paa.Plan_Action__c = pa.ID;
	        paa.Plan_Approval__c = pa.Plan_Approval__c;
	        paa.Approved__c = pa.Approved__c;
	        paa.Amendment_Details__c = pa.Amendment_Details__c;
	        paa.Clinical_Reasoning__c = pa.Clinical_Reasoning__c;
	        paa.Estimated_Cost__c = pa.Estimated_Cost__c;
	        paa.Necessary_Reasonable__c  = pa.Necessary_Reasonable__c;
	        paa.Necessary_and_Reasonable_Criteria__c = pa.Necessary_and_Reasonable_Criteria__c;
	        paa.Pre_Defined_Services__c = pa.Pre_Defined_Services__c;
	        paa.Previous_Cost__c = pa.Previously_Approved_Cost__c;
	        paa.Provider__c = pa.Provider__c;
	        paa.Quote_Attached__c = pa.Quote_Attached__c;
	        paa.Service_Description__c= pa.Item_Description__c;
	        planActionAudits.add(paa);
	    }
                                 
	    insert planActionAudits;               
    }

	@future
	public static void unsetLatestUpdate(Set<ID> planActionIds)
	{
		List<Plan_Action__c> planActions = new List<Plan_Action__c>();
		for(Plan__c plan :[SELECT 	ID,
									(SELECT ID,
											Recently_Updated__c
									FROM 	Plan_Actions__r)
							FROM	Plan__c
							WHERE 	ID IN (SELECT 	Participant_Plan__c
											FROM 	Plan_Action__c
											WHERE 	ID IN :planActionIds)])
		{
			for(Plan_Action__c pa : plan.Plan_Actions__r)
			{
				if(pa.Recently_Updated__c != planActionIds.contains(pa.ID))
				{
					pa.Recently_Updated__c = planActionIds.contains(pa.ID);
					planActions.add(pa);
				}
			}
		}
		TriggerBypass.PlanAction = true;
		update planActions;
	}
}