public class StaffAvailabilityThisWeekController
{   
    public List<Staff_Availability__c> Availabilities
    {
        get
        {
            return [SELECT Id,
                           Staff_Member__r.Name,
                           Staff_Member__r.MobilePhone,
                           Monday__c,
                           Tuesday__c,
                           Wednesday__c,
                           Thursday__c,
                           Friday__c,
                           Monday_Comments__c,
                           Tuesday_Comments__c,
                           Wednesday_Comments__c,
                           Thursday_Comments__c,
                           Friday_Comments__c
                    FROM Staff_Availability__c
                    WHERE Current_Week__c = true
                    AND RecordType.DeveloperName = 'Staff_Availability'
                    ORDER BY Staff_Member__r.Name];                                                                
        }           
    }
    
    public String Monday
    {
        get
        {
            return (Date.today() - math.mod(Date.newinstance(1900, 1, 1).daysBetween(Date.today()), 7)).format();  
        }
    }
}