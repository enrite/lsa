public class BatchSetPlanActionGLCategory implements Database.Batchable<sObject> {
    public BatchSetPlanActionGLCategory()
    {}
    
    public Database.QueryLocator start(Database.BatchableContext bc) 
    {
    	return Database.getQueryLocator([SELECT 	Id
    									FROM 		Plan_Action__c 
    									WHERE 		GL_Category__c = null AND 
    												ID IN (SELECT 	Plan_Action__c 
    														FROM 	RCR_Contract__c
                                                            WHERE   RecordType.DeveloperName = 'Service_Order')]);
    }
    
    public void execute(Database.BatchableContext BC, list<Plan_Action__c> scope) 
    {
    	for(Plan_Action__c pa : scope)
    	{
    		for(RCR_Contract_Scheduled_Service__c css : [SELECT ID,
                                                                GL_Category__c
                                                        FROM    RCR_Contract_Scheduled_Service__c
                                                        WHERE   RCR_Contract__r.Plan_Action__c = :pa.ID AND 
                                                                RCR_Contract__r.RecordType.DeveloperName = 'Service_Order'
                                                        ORDER BY Total_Cost__c DESC 
                                                        LIMIT 1])
            {
                pa.GL_Category__c = css.GL_Category__c;
            }
    	}
        TriggerBypass.PlanAction = true;
        update scope;
    }

    public void finish(Database.BatchableContext BC) {}
}