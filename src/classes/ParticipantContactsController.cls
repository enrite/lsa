/**
 * Created by me on 06/08/2019.
 */

public with sharing class ParticipantContactsController
{
    public transient List<Participant_Contact__c> tDecisionMakingContacts;
    public transient List<Participant_Contact__c> tEmergencyContacts;
    public transient List<Participant_Contact__c> tAllContacts;

    public ID ClientId
    {
        get;
        set;
    }

    private List<Participant_Contact__c> AllContacts
    {
        get
        {
            if(tAllContacts == null)
            {
                tAllContacts = [SELECT ID,
                        Name,
                        Full_Name__c,
                        Relationship_to_Participant__c,
                        Preferred_Phone__c,
                        Email__c,
                        In_Case_Of_Emergency__c,
                        Decision_Making_Contact__c
                FROM    Participant_Contact__c
                WHERE    Participant__c = :ClientId];
            }
            return tAllContacts;
        }
    }

    public List<Participant_Contact__c> DecisionMakingContacts
    {
        get
        {
            if(tDecisionMakingContacts == null)
            {
                tDecisionMakingContacts = new List<Participant_Contact__c>();
                for(Participant_Contact__c pc : AllContacts)
                {
                    if(pc.Decision_Making_Contact__c)
                    {
                        tDecisionMakingContacts.add(pc);
                    }
                }
            }
            return tDecisionMakingContacts;
        }
    }

    public List<Participant_Contact__c> EmergencyContacts
    {
        get
        {
            if(tEmergencyContacts == null)
            {
                tEmergencyContacts = new List<Participant_Contact__c>();
                for(Participant_Contact__c pc : AllContacts)
                {
                    if(pc.In_Case_Of_Emergency__c)
                    {
                        tEmergencyContacts.add(pc);
                    }
                }
            }
            return tEmergencyContacts;
        }
    }
}