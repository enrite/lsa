@isTest
public class TestCaseNotesReportPDFExtension
{          
    static testMethod void myTestMethod()
    {
        TestLoadFormData data = new TestLoadFormData();
        
        Test.setCurrentPage(Page.CaseNotesReportPDF);
        ApexPages.CurrentPage().getParameters().put('from', Date.today().format());
        ApexPages.CurrentPage().getParameters().put('to', Date.today().format());
                
        CaseNotesReportPDFExtension ext = new CaseNotesReportPDFExtension(new ApexPages.StandardController(data.Client));
        system.debug(ext.ShowNotes);
        system.debug(ext.ShowEmails);
        system.debug(ext.Notes);
        system.debug(ext.Emails);
    }
}