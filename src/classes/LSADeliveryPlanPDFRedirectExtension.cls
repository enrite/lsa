public with sharing class LSADeliveryPlanPDFRedirectExtension
{
    public LSADeliveryPlanPDFRedirectExtension(ApexPages.StandardController c)
    {
        record = (Plan__c)c.getRecord();
    }

    private Plan__c record;
    
    public PageReference Redirect()
    {
        PageReference ref;

        // see if we can find a service order linked to this plan through an attendant care and support plan action
        for (RCR_Contract__c so : [SELECT Id,
                                          RecordType.DeveloperName,
                                          Plan_Action__r.Participant_Plan__r.Plan_Status__c
                                   FROM RCR_Contract__c
                                   WHERE Plan_Action__r.RecordType.DeveloperName = 'Attendant_Care_and_Support_MyPlan'
                                   AND Plan_Action__r.Participant_Plan__c = :record.Id])
        {
            ref = Page.DeliveryPlanPDF;
            ref.getParameters().put('id', so.Id);

            // if both the service order and plan are approved then attach the PDF to the service order
            if (so.RecordType.DeveloperName == 'Service_Order'
                && so.Plan_Action__r.Participant_Plan__r.Plan_Status__c == 'Approved')
            {                                  
                Attachment a = new Attachment();
                a.ParentId = so.Id;
                a.Name = 'Delivery_Plan_' + Date.today().format() + '.pdf';
                if (!Test.isRunningTest())
                {
                    a.Body = ref.getContentAsPDF();    
                    insert a;
                }
            }                            
        }                                   
                                       
        return ref;
    }
}