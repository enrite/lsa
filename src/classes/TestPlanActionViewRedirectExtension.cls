@isTest
private class TestPlanActionViewRedirectExtension {
  @testSetup 
  static void setupTestData()
  {
    TestLoadFormData data = new TestLoadFormData();
    
    Id myPlanRecordType = Schema.SObjectType.Plan__c.getRecordTypeInfosByName().get('MyPlan').RecordTypeId; 

    Plan__c p = new Plan__c();
        p.Client__c = data.Client.Id;
        p.RecordTypeId = myPlanRecordType;
        p.Plan_Status__c = 'Draft';
        insert p;

        Plan_Action__c pa = new Plan_Action__c(Participant_Plan__c = p.id);
        insert pa;
  }

  @isTest
  static void testRedirect()
    {                    
        Test.StartTest();
        
        Plan_Action__c pa = [select id from Plan_Action__c limit 1];
                                
        PlanActionViewRedirectExtension ext = new PlanActionViewRedirectExtension(new ApexPages.StandardController(pa));
        ext.Redir();   
        
        ApexPages.CurrentPage().getHeaders().put('referer', 'participantportal');
        ApexPages.CurrentPage().getParameters().put('x', 'x');
        
        ext = new PlanActionViewRedirectExtension(new ApexPages.StandardController(pa));
        ext.Redir();        
                       
        Test.StopTest();        
    }
}