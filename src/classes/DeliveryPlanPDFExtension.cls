public with sharing class DeliveryPlanPDFExtension
{
    public DeliveryPlanPDFExtension(ApexPages.StandardController c)
    {
        record = (RCR_Contract__c)c.getRecord();
    }

    public RCR_Contract__c record;
    
    public String DateProduced
    {
        get
        {
            if (DateProduced == null)
            {
                DateProduced = Datetime.now().format('d/MM/yyyy h:mmaa');
            }
            return DateProduced;            
        }
        set;
    }
            
    public List<Plan_Goal__c> PlanGoals
    {
        get
        {
            return [SELECT Id,
                           Name,
                           Goal_Details__c
                    FROM Plan_Goal__c
                    WHERE Participant_Plan__c = :record.Plan_Action__r.Participant_Plan__c
                    ORDER BY CreatedDate];        
        }        
    }    
}