global class RCRScheduleRateRecalculation implements Schedulable 
{
    global void execute(SchedulableContext SC)
    {
        Map<Id, List<RCR_Service_Rate_Audit__c>> auditRecords = new Map<Id, List<RCR_Service_Rate_Audit__c>>();
        
        for(RCR_Service_Rate_Audit__c auditRecord : [SELECT r.Id,
                                                            r.Name, 
                                                            r.RCR_Service_Rate__c, 
                                                            r.RCR_Contract_Scheduled_Service__c, 
                                                            r.RCR_Service_Order__c,
                                                            r.Recalculation_ID__c,
                                                            r.Scheduled_Service_Value_Before__c, 
                                                            r.Scheduled_Service_Value_After__c,
                                                            r.Service_Order_Value_Before__c, 
                                                            r.Service_Order_Value_After__c, 
                                                            r.Proposed_Rate_Activation__c, 
                                                            r.Errors__c,
                                                            r.Datestamp__c,
                                                            r.Costs_Recalculated__c,
                                                            r.CBMS_Updated__c
                                                    FROM    RCR_Service_Rate_Audit__c r
                                                    WHERE   r.RCR_Service_Rate__c != null
                                                    AND     r.Proposed_Rate_Activation__c = true
                                                    AND     r.Costs_Recalculated__c = null
                                                    AND     r.Service_Order_Value_After__c = null
                                                    AND     r.Scheduled_Service_Value_After__c = null
                                                    AND     r.CBMS_Updated__c = null
                                                    ORDER BY r.Recalculation_ID__c])
        {
            if(!auditRecords.containsKey(auditRecord.RCR_Contract_Scheduled_Service__c))
            {
                auditRecords.put(auditRecord.RCR_Contract_Scheduled_Service__c, new List<RCR_Service_Rate_Audit__c>());
            }
            auditRecords.get(auditRecord.RCR_Contract_Scheduled_Service__c).add(auditRecord);
        }
                                                        
        RCRBatchUpdateScheduledServiceTotalCost batch = new RCRBatchUpdateScheduledServiceTotalCost();   
        batch.RateAuditRecordsByScheduledService = auditRecords;
        batch.runMode = RCRBatchUpdateScheduledServiceTotalCost.Mode.ProposedRateUpdateByNightProcess;
        batch.ProposedRateActivation = true;
        ID batchprocessid = Database.executeBatch(batch, Batch_Size__c.getInstance('Rate_Recalculation').Records__c.IntValue());                                                        
    }

    public void run(string triggerID)
    {}
    }