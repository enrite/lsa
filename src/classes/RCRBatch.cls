public with sharing class RCRBatch extends A2HCPageBase
{

	public RCRBatch(ApexPages.StandardController controller)
	{
	}

	public RCRBatch()
	{
		BatchID = getParameter('Id');

		InvoiceBatch = [SELECT	r.Id,
								r.Name,
								r.Batch_Number__c,
								r.Sent_Date__c,
								r.Description__c,
								(SELECT Id,
										Name,
										RCR_Invoice_Batch__c,
										Invoice_Number__c,
										Total__c,
										Lines_Not_Reconciled__c,
										All_Line_Count__c,
										Status__c,
										Start_Date__c,
										End_Date__c,
										Date__c,
										RCR_Invoice_Upload__c
								 FROM	RCR_Invoices__r)
						FROM	RCR_Invoice_Batch__c r
						WHERE 	r.Id = :BatchID];
	}

	public Id BatchID
	{
		get;
		private set;
	}

	public RCR_Invoice_Batch__c InvoiceBatch
	{
		get;
		private set;
	}

	public Pagereference cancel()
    {
        try
        {
        	String retURL = getParameter('retURL');
			String searchCriteria = getParameter('srch');
			searchCriteria = searchCriteria == null ? '' : searchCriteria;
        	PageReference page = null;
        	if(retURL != null)
        	{
        		page = new PageReference(retURL + (retURL.contains('?') ? '&' : '?') + 'srch=' + searchCriteria);
        	}
        	else
        	{
        		page = new PageReference('/apex/RCRBatchSearch?srch=' + searchCriteria);
        	}
        	return page.setRedirect(true);
        }
        catch(Exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }
	
	
    static testMethod void myUnitTest()
    {
		RCR_Invoice_Batch__c b = new RCR_Invoice_Batch__c();
		insert b;

    	RCRBatch batch = new RCRBatch(new ApexPages.Standardcontroller(b));

    	Test.setCurrentPage(new PageReference('/apex/RCRBatchDetail'));
    	ApexPages.currentPage().getParameters().put('id', b.ID);
    	batch = new RCRBatch();

		batch.cancel();

    	ApexPages.currentPage().getParameters().put('retURL','blah');
    	batch.cancel();

    	ApexPages.currentPage().getParameters().put('srch', 'blah');
    	batch.cancel();
    }
}