@isTest
public class TestCustomApproveRejectController
{                   
    static testMethod void myTestMethod()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
        
        RCR_Contract__c so = [SELECT Id,
                                     Service_Planner__c,
                                     Attendant_Care_Support__c
                              FROM RCR_Contract__c                                                         
                              LIMIT 1];
        so.Attendant_Care_Support__c = 'No';
        so.Service_Planner__c = UserInfo.getUserId();
        update so;                              

        Test.setCurrentPage(Page.CustomApproveReject);

        Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
        request.setObjectId(so.Id);
        Approval.process(request);
                
        ProcessInstanceWorkItem wi = [SELECT Id
                                      FROM ProcessInstanceWorkItem 
                                      ORDER BY CreatedDate DESC
                                      LIMIT 1];
        
        ApexPages.CurrentPage().getParameters().put('id', wi.Id);
        CustomApproveRejectController ctr = new CustomApproveRejectController();
        ctr.Approve();
        
        request = new Approval.ProcessSubmitRequest();
        request.setObjectId(so.Id);
        Approval.process(request);
                
        wi = [SELECT Id
              FROM ProcessInstanceWorkItem 
              ORDER BY CreatedDate DESC
              LIMIT 1];
        
        ApexPages.CurrentPage().getParameters().put('id', wi.Id);
        ctr = new CustomApproveRejectController();
        ctr.Reject();
                
        Test.StopTest();                                   
    }
}