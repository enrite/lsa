public class RCRSupportEmailController 
{
	public void RCRSupportEmailController()
	{
		 
	}
	
	
	public PageReference sendEmail()
	{
		try
		{
			string msgBody = '';
			
			if(string.isBlank(Subject) ||
			   string.isBlank(SelectedScreenIssue) ||
			   string.isBlank(SelectSystemAction) ||
			   string.isBlank(MessageDetails) ||
			   string.isBlank(ContactPhoneNumber))
			{
				return A2HCException.formatException('All fields must be completed to send email.');
			}
			
			Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
			List<string> addresses = new List<string>();
	
			msgBody += 'Dear RCR Support,<br/><br/>';
			if(!String.isBlank(SelectedScreenIssue))
			{
				msgBody += 'Screen Issue: ' + SelectedScreenIssue + '<br/>';
			}
			if(!String.isBlank(SelectSystemAction))
			{
				msgBody += 'System Action: ' + SelectSystemAction + '<br/>';
			}
			if(!String.isBlank(MessageDetails))
			{
				msgBody += '<br/>' + MessageDetails;
			}
			
			msgBody += '<br/><br/><br/>';
			msgBody += 'Regards,<br/><br/>';
			msgBody += UserInfo.getName() + ' (' + UserInfo.getUserName() + ')<br/>';
			msgBody += 'Email: ' + UserInfo.getUserEmail() + '<br/>';
			msgBody += 'Contact Phone: ' + ContactPhoneNumber;
			
			for(Email_Addresses__c addr : [SELECT Email__c FROM Email_Addresses__c WHERE Name LIKE 'RCR Support%'])
			{
				addresses.add(addr.Email__c);
			}
			msg.setToAddresses(addresses);
			msg.setSubject(Subject);
			msg.setHtmlBody(msgBody);
			Messaging.sendEmail(new Messaging.Singleemailmessage[]{msg});
			
			A2HCException.formatMessage('Message sent.');
			SelectedScreenIssue = '';
			SelectSystemAction = '';
			Subject = '';
			MessageDetails = '';
			ContactPhoneNumber = '';
			
			return null; 
		} 
		catch(Exception ex)
		{
			return A2HCException.formatException(ex);
		}
	}
	
	
	public string SelectedScreenIssue 
	{
		get;
		set;
	}
	
	
	public string SelectSystemAction
	{
		get;
		set;
	}
	
	
	public string Subject
	{
		get;
		set;
	} 
	
	
	public string MessageDetails
	{
		get;
		set;
	}
	
	
	public string ContactPhoneNumber
	{
		get;
		set;
	}
	
	
	public List<SelectOption> ScreenIssueOptions
	{
		get
		{
			if(ScreenIssueOptions == null)
			{
				ScreenIssueOptions = new List<SelectOption>();
				ScreenIssueOptions.add(new SelectOption('', ''));
				for(RCR_Support_Requests__c sr : [SELECT Name
												  FROM	 RCR_Support_Requests__c
												  WHERE	 Issue_Action__c = 'Screen Issue'
												  ORDER BY Name])
				{
					ScreenIssueOptions.add(new SelectOption(sr.Name, sr.Name));
				}
			}
			return ScreenIssueOptions;
		}
		set;
	}
	
	
	public List<SelectOption> SystemActionOptions
	{
		get
		{
			if(SystemActionOptions == null)
			{
				SystemActionOptions = new List<SelectOption>();
				SystemActionOptions.add(new SelectOption('', ''));
				for(RCR_Support_Requests__c sr : [SELECT Name
												  FROM	 RCR_Support_Requests__c
												  WHERE	 Issue_Action__c = 'System Action'
												  ORDER BY Name])
				{
					SystemActionOptions.add(new SelectOption(sr.Name, sr.Name));
				}
			}
			return SystemActionOptions;
		}
		set;
	}
}