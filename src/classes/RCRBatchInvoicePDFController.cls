public with sharing class RCRBatchInvoicePDFController
{
    public RCRBatchInvoicePDFController(ApexPages.StandardController cont)
    {
        controller = cont;
    }

    private ApexPages.StandardController controller;
    private transient List<AttachmentWrapper> tBatchInvoiceAttachments;

    public List<AttachmentWrapper> BatchInvoiceAttachments
    {
        get {
            if(tBatchInvoiceAttachments == null)
            {
                tBatchInvoiceAttachments = new List<AttachmentWrapper>();
                Set<String> fileExtensions = new Set<String>();
                fileExtensions.addAll(Email2Invoice_File_Types__c.getOrgDefaults().File_Extensions__c.split(';'));
                Map<ID, RCR_Invoice__c> invoicesById = new Map<ID, RCR_Invoice__c>(
                [
                        SELECT  ID,
                                Invoice_Number__c,
                                Account__r.Name
                        FROM RCR_Invoice__c
                        WHERE RCR_Invoice_Batch__c = :controller.getID()
                ]
                );
                for(Attachment a :
                [
                        SELECT ID,
                                ParentId,
                                Name,
//                                Body,
                                ContentType
                        FROM Attachment
                        WHERE ParentID IN :invoicesById.keySet()])
                {
                    if(fileExtensions.contains(a.Name.substringAfterLast('.')))
                    {
                        tBatchInvoiceAttachments.add(new AttachmentWrapper(a, invoicesById.get(a.ParentId)));
                    }
                }
            }
            tBatchInvoiceAttachments.sort();
            return tBatchInvoiceAttachments;
        }
    }

    public class AttachmentWrapper implements Comparable
    {
        public AttachmentWrapper(Attachment a, RCR_Invoice__c invoice)
        {
            File = a;
            this.AccountName = invoice.Account__r.Name;
            this.StatementNumber = invoice.Invoice_Number__c;
        }

        public Attachment File
        {
            get;
            set;
        }

        public String StatementNumber
        {
            get;
            set;
        }

        public String AccountName
        {
            get;
            set;
        }

        public Integer compareTo(Object obj)
        {
            AttachmentWrapper compareTo = (AttachmentWrapper)obj;
            if(compareTo.StatementNumber == StatementNumber)
            {
                return 0;
            }
            return compareTo.StatementNumber > StatementNumber ? -1 : 1;
        }
    }

}