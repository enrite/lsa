@isTest
public class TestLSAContractPDFRedirectExtension
{                   
    static testMethod void myTestMethod()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
        
        RCR_Contract__c c = [SELECT Id,
                                    Client__c
                             FROM RCR_Contract__c
                             LIMIT 1];
                                                                                      
        TestLoadFormData data = new TestLoadFormData();
        
        Plan__c p = new Plan__c();
        p.Client__c = c.Client__c;
        p.RecordTypeId = data.RecordTypes.get('MyPlan').Id;
        p.Plan_Status__c = 'Draft';
        insert p;
        
        Plan_Action__c pa = new Plan_Action__c();
        pa.Client__c = c.Client__c;
        pa.Participant_Plan__c = p.Id;
        pa.RecordTypeId = data.RecordTypes.get('Attendant_Care_and_Support').Id;
        insert pa;
        
        Plan_Goal__c g = new Plan_Goal__c();
        g.Participant_Plan__c = p.Id;
        insert g;
        
        c.Plan_Action__c = pa.Id;
        c.Attendant_Care_Support__c = 'Yes';
        update c;
        
        c = [SELECT Id,
                    Plan_Action__r.Participant_Plan__c,
                    Plan_Action__r.Participant_Plan__r.RecordType.DeveloperName,
                    Plan_Action__r.RecordType.DeveloperName,
                    RecordType.DeveloperName,
                    Attendant_Care_Support__c
             FROM RCR_Contract__c
             WHERE Id = :c.Id];
        
        Test.setCurrentPage(Page.LSAContractPDFRedirect);
        ApexPages.CurrentPage().getParameters().put('test', 'test');
        
        LSAContractPDFRedirectExtension ext = new LSAContractPDFRedirectExtension(new ApexPages.StandardController(c));
        ext.Redirect();
        
        Test.StopTest();        
    }
}