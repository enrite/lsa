@isTest(seeAllData=true)
private class TestRCRReportController 
{
    static testMethod void myUnitTest() 
    {
    	date d = Date.newInstance(2013, 1, 1);
    	RCRReportController cntrl = new RCRReportController();
    	
    	cntrl.SelectedReport = cntrl.Reports[0].getValue();    
    	cntrl.StartDate = d.format();
    	cntrl.EndDate = d.addMonths(6).format();
    	cntrl.runReport(); 
    }
}