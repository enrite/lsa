/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@IsTest//(SeeAllData=true)
private class TestRCRServiceOrderClone {

    static testMethod void testClientProgram() 
    {
        TestLoadData.loadRCRData();
                
        Test.StartTest();
    
        RCRServiceOrderCloneBase cloneClass = new RCRServiceOrderCloneBase();
        RCR_Contract__c so = TestRCR.getTestContract();
        
        system.debug(cloneClass.getServiceOrder(so.ID)); 
        
        RCR_Group_Agreement__c ga = new RCR_Group_Agreement__c();
        ga.Account__c = so.Account__c;
        ga.Cost_Centre__c = 'Test';
        ga.Grant_Amount__c = so.Total_Service_Order_Cost__c;
        ga.Grant_Paid_Via__c = 'RCR';
        ga.Start_Date__c = so.Start_Date__c;
        ga.End_Date__c = so.End_Date__c;
        ga.Visible_to_Service_Providers__c = true;
        insert ga;
        
        system.debug(cloneClass.getClientOrProgram(so));
        
        so.RCR_Group_Agreement__c = ga.ID;
        so.Client__c = null;
        system.assert(cloneClass.getClientOrProgram(so) == null);
        
        for(Referred_Client__c cl : [SELECT ID,
                                            Name
                                    FROM    Referred_Client__c
                                    WHERE   Account__r.API_Name__c = :APS_PicklistValues.Account_DCSI_RCR_Programs
                                    LIMIT 1])
        {
            RCR_Contract__c newSO = so.clone();
            newSO.Start_Date__c = so.Start_Date__c.addYears(1);
            newSO.Original_End_Date__c = so.End_Date__c.addYears(1);
//            RCR_CBMS_Contract__c cbmsContract = new RCR_CBMS_Contract__c(Group_Program__c = cl.Name);
//            insert cbmsContract;
//            newSO.RCR_CBMS_Contract__c = cbmsContract.ID;
            insert newSO;
            newSO = cloneClass.getServiceOrder(newSO.ID);
            system.debug(cloneClass.getClientOrProgram(newSO));
            
//            cbmsContract.Group_Program__c = 'Not Found';
//            update cbmsContract;
            newSO = cloneClass.getServiceOrder(newSO.ID);
            system.debug(cloneClass.getClientOrProgram(newSO));
        }
        
        Test.StopTest();
    }
    
    static testMethod void testCloneServiceType() 
    {
        TestLoadData.loadRCRData();
                
        Test.StartTest();
    
        RCRServiceOrderCloneBase cloneClass = new RCRServiceOrderCloneBase();
        for(RCR_Contract_Service_Type__c serviceType : [SELECT ID, 
                                                                RCR_Contract_Scheduled_Service__c
                                                        FROM    RCR_Contract_Service_Type__c
                                                        WHERE   RCR_Contract_Scheduled_Service__c != null
                                                        LIMIT 1])
        {
            List<RCR_Contract_Service_Type__c> serviceTypes = new List<RCR_Contract_Service_Type__c>{serviceType};
            cloneClass.cloneServiceTypes(serviceTypes, serviceType.RCR_Contract_Scheduled_Service__c, 'Scheduled');
        }
        
        for(RCR_Contract_Service_Type__c serviceType : [SELECT ID, 
                                                                RCR_Contract_Adhoc_Service__c
                                                        FROM    RCR_Contract_Service_Type__c
                                                        WHERE   RCR_Contract_Adhoc_Service__c != null
                                                        LIMIT 1])
        {
            List<RCR_Contract_Service_Type__c> serviceTypes = new List<RCR_Contract_Service_Type__c>{serviceType};
            cloneClass.cloneServiceTypes(serviceTypes, serviceType.RCR_Contract_Adhoc_Service__c, 'Adhoc');
        }
        
        Test.StopTest();
    }
    
    static testMethod void testServiceDates() 
    {
        TestLoadData.loadRCRData();
                
        Test.StartTest();
    
        RCRServiceOrderCloneBase cloneClass = new RCRServiceOrderCloneBase();
        RCR_Contract_Scheduled_Service__c css = [SELECT Start_Date__c,
                                                        End_Date__c,
                                                        RCR_Contract__c
                                                FROM    RCR_Contract_Scheduled_Service__c
                                                LIMIT 1];
        RCR_Contract__c so = cloneClass.getServiceOrder(css.RCR_Contract__c);
        RCR_Contract__c newSo = new RCR_Contract__c(Start_Date__c = so.Start_Date__c.addDays(1));
        cloneClass.setServiceDates(so, newSo, css, new RCR_Contract_Scheduled_Service__c());
        
        so.Start_Date__c = so.Start_Date__c.addDays(-100);
        cloneClass.setServiceDates(so, newSo, css, new RCR_Contract_Scheduled_Service__c());
        
        so.Start_Date__c = so.Start_Date__c.addDays(200);
        cloneClass.setServiceDates(so, newSo, css, new RCR_Contract_Scheduled_Service__c());
        
        Test.StopTest();
    }
}