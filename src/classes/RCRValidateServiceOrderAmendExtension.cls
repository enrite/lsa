public class RCRValidateServiceOrderAmendExtension
{
    public RCRValidateServiceOrderAmendExtension(ApexPages.StandardController c)
    {
        record = (RCR_Contract__c)c.getRecord();
    }

    private RCR_Contract__c record;
    
    public PageReference Validate()
    {
        try
        {
            // check if the service order has an approved plan action
            if (record.Plan_Action__r.Approved__c)
            {
                // return null to display error
                return null;
            }
            
            // if passed validation then redirect to pass parameter
            return new PageReference(ApexPages.CurrentPage().getParameters().get('url'));
        }
        catch(Exception e)
        {
            return A2HCException.formatException(e);
        }
    }
}