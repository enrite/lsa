public with sharing class MyPlanExtension
{
    public MyPlanExtension(ApexPages.StandardController c)
    {
        record = (Plan__c)c.getRecord();
        
        // default status to Draft if this is a new plan
        if (record.Id == null)
        {
            record.Plan_Status__c = 'Draft';
        }
        
        FindPlanGoals();
    }

    private Plan__c record;
    public List<String> PlanGoalsKeys { get; set; }
    public Map<String, Plan_Goal__c> PlanGoals { get; set; }
    public String PlanGoalKey { get; set; }
    private List<Plan_Goal__c> deletePlanGoals = new List<Plan_Goal__c>();
    
    public void FindPlanGoals()        
    {
        try
        {
            PlanGoalsKeys = new List<String>();
            PlanGoals = new Map<String, Plan_Goal__c>();
            
            for (Plan_Goal__c g : [SELECT Id,
                                          Name,
                                          Participant_Plan__c,
                                          Discussion__c,
                                          Order__c
                                   FROM Plan_Goal__c
                                   WHERE Participant_Plan__c = :record.Id
                                   ORDER BY Order__c ASC NULLS LAST])
            {
                String key = A2HCUtilities.GenerateKey(15);
                
                PlanGoalsKeys.add(key);
                PlanGoals.put(key, g);
            }                                                   
        }
        catch(Exception e)
        {
            A2HCException.formatException(e);
        }            
    } 
    
    public void AddPlanGoal()
    {
        try
        {
            String key = A2HCUtilities.GenerateKey(15);
                
            PlanGoalsKeys.add(key);
            PlanGoals.put(key, new Plan_Goal__c(Participant_Plan__c = record.Id));    
        }
        catch(Exception e)
        {
            A2HCException.formatException(e);
        }     
    }
               
    public void MoveUp()
    {
        try
        {
            for (Integer i = 0; i < PlanGoalsKeys.size(); i++)
            {
                if ((i + 1) < PlanGoalsKeys.size() && PlanGoalsKeys[i + 1] == PlanGoalKey)
                {
                    String temp = PlanGoalsKeys[i];
                    PlanGoalsKeys[i] = PlanGoalsKeys[i + 1];
                    PlanGoalsKeys[i + 1] = temp;
                    break;
                }    
            }
        }  
        catch(Exception e)
        {
            A2HCException.formatException(e);
        }    
    }
    
    public void MoveDown()
    {
        try
        {
            for (Integer i = 0; i < PlanGoalsKeys.size(); i++)
            {
                if ((i + 1) < PlanGoalsKeys.size() && PlanGoalsKeys[i] == PlanGoalKey)
                {
                    String temp = PlanGoalsKeys[i];
                    PlanGoalsKeys[i] = PlanGoalsKeys[i + 1];
                    PlanGoalsKeys[i + 1] = temp;
                    break;
                }    
            }
        }  
        catch(Exception e)
        {
            A2HCException.formatException(e);
        }    
    }
    
    public void MoveTop()
    {
        try
        {
            List<String> temp = new List<String>();
            temp.add(PlanGoalKey);
        
            for (Integer i = 0; i < PlanGoalsKeys.size(); i++)
            {
                if (PlanGoalsKeys[i] != PlanGoalKey)
                {
                    temp.add(PlanGoalsKeys[i]);
                }                             
            }
            
            PlanGoalsKeys = temp;
        }  
        catch(Exception e)
        {
            A2HCException.formatException(e);
        }    
    }
    
    public void MoveBottom()
    {
        try
        {
            List<String> temp = new List<String>();
                    
            for (Integer i = 0; i < PlanGoalsKeys.size(); i++)
            {
                if (PlanGoalsKeys[i] != PlanGoalKey)
                {
                    temp.add(PlanGoalsKeys[i]);
                }                             
            }
            
            temp.add(PlanGoalKey);
            
            PlanGoalsKeys = temp;
        }  
        catch(Exception e)
        {
            A2HCException.formatException(e);
        }    
    }
    
    public void RemovePlanGoal()
    {
        try
        {
            for (Integer i = 0; i < PlanGoalsKeys.size(); i++)
            {
                if (PlanGoalsKeys[i] == PlanGoalKey)
                {                                    
                    PlanGoalsKeys.remove(i);
                                                            
                    break;
                }                             
            }
            
            if (PlanGoals.get(PlanGoalKey).Id != null)
            {
                deletePlanGoals.add(PlanGoals.get(PlanGoalKey));
            }
            
            PlanGoals.remove(PlanGoalKey);
            
            
        }
        catch(Exception e)
        {
            A2HCException.formatException(e);
        }     
    }
        
    public PageReference SaveOverride()
    {           
        try
        {
            Boolean sendSatalystMessage = false;
            if(record.ID == null && record.Plan_Status__c == 'Draft')
            {
                for(Plan__c otherPlan : [SELECT     ID
                                        FROM        Plan__c
                                        WHERE       Client__c = :record.Client__c AND
                                                    RecordType.DeveloperName = 'MyPlan' AND
                                                    Plan_Status__c IN ('Approved','Amendment')])
                {
                    sendSatalystMessage = true;
                }
            }
            upsert record;
            if(sendSatalystMessage)
            {
                SatalystOutboundMessage.sendSatalystMessage(record.ID); 
            }
            // delete any plan goals that have been removed
            if (deletePlanGoals.size() > 0)
            {
                delete deletePlanGoals;
            }
            
            // set the plan goal master details and orders if necessary
            Integer i = 1;
            for (String k : PlanGoalsKeys)            
            {                            
                if (PlanGoals.get(k).Participant_Plan__c == null)
                {
                    PlanGoals.get(k).Participant_Plan__c = record.Id;
                }            
                
                PlanGoals.get(k).Order__c = i;
                i++;
            }
            
            upsert PlanGoals.values();
        
            return new PageReference('/' + record.Id);
        }
        catch(Exception e)
        {
            return A2HCException.formatException(e);
        }                   
    }      
}