global class postsandboxrefresh implements SandboxPostCopy
{
    global void runApexClass(SandboxContext context)
    {
        List<contact> contacts = [
                select id,
                        email
                from contact
                where email != null
        ];
        for(contact c : contacts)
        {
            c.email = c.email + '.sandbox';
        }
        update contacts;

        List<Referred_Client__c> clients = [
                select  id,
                        Email__c
                from Referred_Client__c
                where email__c != null
        ];
        for(Referred_Client__c c : clients)
        {
            c.Email__c = c.Email__c + '.sandbox';
        }
        update clients;

        List<Email_Addresses__c> emailaddresses = [
                select id
                from Email_Addresses__c
        ];
        for(Email_Addresses__c e : emailaddresses)
        {
            e.Email__c = 'louise.fitzgerald@sa.gov.au';
        }
        update emailaddresses;
    }
}