@isTest
public class TestParticipantFormsExtension
{                    
    static testMethod void myTestMethod()
    {
        TestLoadFormData data = new TestLoadFormData();
                                           
        Plan__c p = new Plan__c();
        p.Client__c = data.Client.Id;
        insert p;
        
        ParticipantFormsExtension ext = new ParticipantFormsExtension(new ApexPages.StandardController(p));
        system.debug(ext.Forms);           
    }
}