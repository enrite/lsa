public virtual class RCRCompensableClientPurchasedPDF extends A2HCPageBase 
{
    public Referred_Client__c ReferredClient
    {
        get
        {
            if(ReferredClient == null)
            {
                ReferredClient = new Referred_Client__c();
            }
            return ReferredClient;
        }
        set;
    }
    
    public String StartDate
    {
    	get
    	{
    		if(StartDate == null)
    		{
    			StartDate = ApexPages.currentPage().getParameters().get('sDate');
    		}
    		return StartDate;
		}
		set;
    }
    
    public String EndDate
    {
    	get
    	{
    		if(EndDate == null)
    		{
    			EndDate = ApexPages.currentPage().getParameters().get('eDate');
    		}
    		return EndDate;
		}
		set;
    }
    
    public decimal GrandTotalExGST
    {
    	get;
    	private set;
    }
    
    public decimal GrandTotalGST 
    {
    	get;
    	private set;
    }
    
    public decimal GrandTotalIncGST
    {
    	get;
    	private set;
    }
    
    public string SelectedCompensableOption
    {
    	get
    	{
    		if (SelectedCompensableOption == null)
    		{
    			SelectedCompensableOption =	ApexPages.currentPage().getParameters().get('cOptions');
    		}
    		return SelectedCompensableOption;
    	}
    	set;
    }
    
    @TestVisible private Set<string> setCompensableSearchOpts()
    {

		Set<string> compensableSearchOpts = new Set<string>();    	
		
		if(SelectedCompensableOption == '' || SelectedCompensableOption == null)
		{
			compensableSearchOpts.add('');
			compensableSearchOpts.add('Yes');
			compensableSearchOpts.add('Maybe');
		}
		else if(SelectedCompensableOption == 'Yes')
		{
			compensableSearchOpts.add('Yes');
		}
		else if(SelectedCompensableOption == 'Maybe')
		{
			compensableSearchOpts.add('Maybe');
		}
		else if(SelectedCompensableOption == 'YesMaybe')
		{
			compensableSearchOpts.add('Yes');
			compensableSearchOpts.add('Maybe');
		}
		return compensableSearchOpts;
    }
    
    public RCRCompensableClientPurchasedPDF(ApexPages.StandardController cntrl)
    {
        controller = cntrl;
        ReferredClient.Id = controller.getId();
                
        doSearch();
    }
    
    private ApexPages.StandardController controller;
	
	public List<AllServices> PurchasedServices
	{
		get
		{
			if(PurchasedServices == null)
    		{
    			PurchasedServices = new List<AllServices>();
    		}
    		return PurchasedServices;
		}
		private set;
	}
	
	public Map<string, List<ServiceDetail>> PurchasedServiceDetails
    {
    	get
    	{
    		if(PurchasedServiceDetails == null)
    		{
    			PurchasedServiceDetails = new Map<string, List<ServiceDetail>>();
    		}
    		return PurchasedServiceDetails;
    	}
    	private set;
    }
    
    public Map<string, ServiceTotal> PurchasedServiceTotals
    {
    	get
    	{
    		if(PurchasedServiceTotals == null)
    		{
    			PurchasedServiceTotals = new Map<string, ServiceTotal>();
    		}
    		return PurchasedServiceTotals;
    	}
    	private set;
    }

	

	public PageReference doSearch()
    {   	
    	try
    	{
    		PurchasedServiceDetails = null;
    		PurchasedServices = null;
    		GrandTotalExGST = 0.0;
    		GrandTotalGST = 0.0;
    		GrandTotalIncGST = 0.0;
    		    		
    		Set<string> compensableSearchOpts = setCompensableSearchOpts();
    		  		    	
	    	AggregateResult[] ar = [SELECT 	i.RCR_Invoice__r.Invoice_Number__c invNumber,
												i.RCR_Invoice__r.Date__c	invDate,
												i.RCR_Invoice__r.Start_Date__c startDate,
												i.RCR_Invoice__r.End_Date__c endDate,
												i.RCR_Invoice__r.Batch_Sent_Date__c sentDate,
	    										i.RCR_Invoice__r.RCR_Invoice_Batch__c batchId,
												i.RCR_Service_Order__r.Compensable__c compensable,
												SUM(i.Total_ex_GST__c)	totalExGST,
	    										SUM(i.Total_GST__c) 	totalGST,
	    										SUM(i.Total__c)			totalIncGST,
	    										i.RCR_Service_Order__r.Account__r.Name provider
	    							  FROM		RCR_Invoice_Item__c i
	    							  WHERE		i.Activity_Date__c >= :Date.parse(StartDate)
	    							  AND		i.Activity_Date__c <= :Date.parse(EndDate)
	    							  AND		i.RCR_Service_Order__r.Compensable__c IN :compensableSearchOpts
	    							  AND		i.RCR_Service_Order__r.Client__c = :controller.getId()
	    							  AND		i.RCR_Invoice__r.Status__c = :APS_PicklistValues.RCRInvoice_Status_PaymentProcessed
	    							  AND		i.Status__c = :APS_PicklistValues.RCRInvoiceItem_Status_Approved
	    							  GROUP BY	i.RCR_Invoice__r.Invoice_Number__c,
												i.RCR_Invoice__r.Date__c,
												i.RCR_Invoice__r.Start_Date__c,
												i.RCR_Invoice__r.End_Date__c,
												i.RCR_Invoice__r.Batch_Sent_Date__c,
												i.RCR_Invoice__r.RCR_Invoice_Batch__c,
												i.RCR_Service_Order__r.Account__r.Name,
												i.RCR_Service_Order__r.Compensable__c];
			Set<ID> batchIds = new Set<ID>();
			for(AggregateResult result : ar)
			{
				batchIds.add((ID)result.get('batchId'));
			}	
			
			Map<ID, RCR_Invoice_Batch__c> batchesByID = new Map<ID, RCR_Invoice_Batch__c>(
												[SELECT	Name, 
														Batch_Number__c
                								FROM	RCR_Invoice_Batch__c 
                								WHERE	ID IN :batchIds]);
											
			for(AggregateResult result : ar)
			{
				if(!PurchasedServiceDetails.containsKey((string)result.get('provider')))
                {
                	String providerName = (string)result.get('provider');
                	PurchasedServices.add(new AllServices(ProviderName));
                    PurchasedServiceDetails.put((string)result.get('provider'), new List<ServiceDetail>());
                    PurchasedServiceTotals.put((string)result.get('provider'), new ServiceTotal(0,0,0));
                }
                
	    		PurchasedServiceDetails.get((string)result.get('provider')).add(new ServiceDetail(
	    													  (string)result.get('invNumber'),
	    													  (date)result.get('invDate'),
	    													  (date)result.get('startDate'),
	    													  (date)result.get('endDate'),
	    													  (date)result.get('sentDate'),
	    													  (string)result.get('compensable'),
	    													  (decimal)result.get('totalExGST'), 
	    													  (decimal)result.get('totalGST'), 
	    													  (decimal)result.get('totalIncGST'),
	    													  (string)result.get('provider'),
	    													  batchesByID.get((ID)result.get('batchId')).Batch_Number__c,
	    													  batchesByID.get((ID)result.get('batchId')).Name)); 
	    													  
				GrandTotalExGST += (decimal)result.get('totalExGST');
				GrandTotalGST += (decimal)result.get('totalGST');
				GrandTotalIncGST += (decimal)result.get('totalIncGST');
				
				if(PurchasedServiceTotals.containsKey((string)result.get('provider')))
                {
					PurchasedServiceTotals.get((string)result.get('provider')).ServiceGrandTotalExGST += (decimal)result.get('totalExGST');
					PurchasedServiceTotals.get((string)result.get('provider')).ServiceGrandTotalGST += (decimal)result.get('totalGST');
					PurchasedServiceTotals.get((string)result.get('provider')).ServiceGrandTotalIncGST += (decimal)result.get('totalIncGST');
                }    													  
	    	}
	    	
	    	if(PurchasedServiceDetails.size() > 0)
	    	{
	    		return null;
	    	}
	    	else
	    	{
	    		return A2HCException.formatMessage('No results found for the Search Criteria you entered.');
	    	}
    	}
    	catch(Exception ex)
    	{
    		return A2HCException.formatException(ex);
    	}
    }
    
    public class AllServices
    {
    	public AllServices(string pProvider)
    	{
    		Provider = pProvider;
    	}
    	
    	public string Provider
    	{
    		get;
    		private set;
    	}
    }
   
   	public class ServiceTotal
   	{
   		public ServiceTotal(decimal pServiceGrandTotalExGST, decimal pServiceGrandTotalGST, decimal pServiceGrandTotalIncGST)
   		{
   			ServiceGrandTotalExGST = pServiceGrandTotalExGST;
   			ServiceGrandTotalGST = pServiceGrandTotalGST;
   			ServiceGrandTotalIncGST = pServiceGrandTotalIncGST;
   		}
   		
   		public decimal ServiceGrandTotalExGST
    	{
    		get;
    		private set;
    	}
    	
    	public decimal ServiceGrandTotalGST
    	{
    		get;
    		private set;
    	}
    	
    	public decimal ServiceGrandTotalIncGST
    	{
    		get;
    		private set;
    	}
   	}
    
    public class ServiceDetail
    {
    	public ServiceDetail(string pInvNumber, date pInvDate, date pStartDate, date pEndDate, date pSentDate, string pCompensable, decimal pTotalExGST, decimal pTotalGST, decimal pTotalIncGST, string pProvider, decimal pCBMSBatchNo, string pBatchId)
    	{
    		InvoiceNumber = pInvNumber;
    		InvoiceDate = pInvDate == null ? null : pInvDate.format();
    		InvoiceStartDate = pStartDate == null ? null : pStartDate.format();
    		InvoiceEndDate = pEndDate == null ? null : pEndDate.format();
    		BatchSentDate = pSentDate == null ? null : pSentDate.format();
    		Compensable = pCompensable;
    		TotalExGST = pTotalExGST;
    		TotalGST = pTotalGST;
    		TotalIncGST = pTotalIncGST;
    		Provider = pProvider;
    		CBMSBatchNo = pCBMSBatchNo;
    		BatchId = pBatchId;
    	}
    	
    	public string InvoiceNumber
    	{
    		get;
    		private set;
    	}
    	
    	public string InvoiceDate
    	{
    		get;
    		private set;
    	}
    	
    	public string InvoiceStartDate
    	{
    		get;
    		private set;
    	}
    	
    	public string InvoiceEndDate
    	{
    		get;
    		private set;
    	}
    	
    	public string BatchSentDate
    	{
    		get;
    		private set;
    	}
    	
    	public string Compensable
    	{
    		get;
    		private set;
    	}
    	
    	public decimal TotalExGST
    	{
    		get;
    		private set;
    	}
    	
    	public decimal TotalGST
    	{
    		get;
    		private set;
    	}
    	
    	public decimal TotalIncGST
    	{
    		get;
    		private set;
    	}
    	
    	public string Provider
    	{
    		get;
    		private set;
    	}
    	
    	public decimal CBMSBatchNo
    	{
    		get;
    		private set;
    	}
    	
    	public string BatchId
    	{
    		get;
    		private set;
    	}
    }

}