global class CalendarFunctions
{
    public static List<date> listOfStringsToDate(string selectedDates)
    {
        List<date> selectedDatesList = new List<Date>(); 
        if(selectedDates.length() < 1)
            return selectedDatesList;
                
        List<String> strDates = selectedDates.split(',');
        for(String s : strDates)
        {
            s = s.replace('/','-');
            Date d = date.valueOf(s + ' 00:00:00');
            selectedDatesList.add(d);
        }   
        return selectedDatesList;
    }
    
    public static string dateListToString(List<Date> ld)
    {
        string s = '';
        
        for(Date d :ld){
            if (s.Length() > 0)
                s+=',';
            s+=d.year() + '-' + d.month() + '-' + d.day();
        }

        return s;
    }

}