/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestRCRApprovedServiceTypesExtension 
{

    static testMethod void myUnitTest() 
    {
        Account acc = new Account(Name = 'test');
        insert acc;
        
        
        
        RCR_Program__c prog = new RCR_Program__c(Name = 'test');
		insert prog;
        
        RCR_Program_Category__c progCat = new RCR_Program_Category__c(Name = 'test', RCR_Program__c = prog.Id);
		insert progCat;
		
		RCR_Account_Program_Category__c accProgCat = new RCR_Account_Program_Category__c(Name = 'test', Account__c = acc.Id, RCR_Program_Category__c = progCat.Id);
        insert accProgCat;
        
        RCR_Program_Category_Service_Type__c progCatServType = new RCR_Program_Category_Service_Type__c(Name = 'test', RCR_Program_Category__c = progCat.Id);
		insert progCatServType;
        
        List<RCR_Approved_Service_Type__c> approvedServiceTypes = new List<RCR_Approved_Service_Type__c>();
		approvedServiceTypes.add(new RCR_Approved_Service_Type__c(RCR_Account_Program_Category__c = accProgCat.Id, RCR_Program_Category_Service_Type__c = progCatServType.Id, Approved__c = true));
		approvedServiceTypes.add(new RCR_Approved_Service_Type__c(RCR_Account_Program_Category__c = accProgCat.Id, RCR_Program_Category_Service_Type__c = progCatServType.Id, Approved__c = false));
		insert approvedServiceTypes;
		

        ApexPages.currentPage().getParameters().put('retURL', accProgCat.Id);
        RCRApprovedServiceTypesExtension ext = new RCRApprovedServiceTypesExtension(new ApexPages.StandardController(accProgCat));

        for(integer i = 0; i < ext.ServiceTypes.size(); i++)
        {
        	ext.ServiceTypes[i].Selected = true;
        }
        
        ext.cancelOverride();
        ext.saveOverride();
        
        for(integer i = 0; i < ext.ServiceTypes.size(); i++)
        {
        	ext.ServiceTypes[i].Selected = false;
        }
        
        ext.saveOverride();
        
        ApexPages.currentPage().getParameters().put('srvcId', accProgCat.Id);
        ext = new RCRApprovedServiceTypesExtension(new ApexPages.StandardController(accProgCat));
        
        system.debug('*************' + ext.ServiceTypes);
    }
}