public with sharing class SPPortalParticipantsController 
    extends A2HCPageBase
{
    public SPPortalParticipantsController(ApexPages.StandardController cont) 
    {
        controller = cont;
    }

    public SPPortalParticipantsController() 
    {
  		CurrentContracts = true;
        StartDateDefault = Date.today().format();
    }

    private ApexPages.StandardController controller;

    public transient List<RCR_Contract__c> tContracts;

    public String ParticipantSearch {get;set;}
    public Boolean CurrentContracts {get;set;}
    public Date StartDateFrom {get;set;}
    public Date StartDateTo {get;set;}

    public String StartDateDefault
    {
        get;
        private set;
    }

    public List<Referred_Client__c> Participants
    {
        get 
        {
            if(Participants == null)
            {               
                Participants = [SELECT ID,
                                        Name,
                                        Date_Of_Birth__c,
                                        Current_Status__c,
                                        Address__c
                                FROM    Referred_Client__c
                                WHERE   ID IN (SELECT Client__C 
                                            FROM    RCR_Contract__c 
                                            WHERE   Start_Date__c <= :Date.today() AND 
                                                    End_Date__c >= :Date.today())
                                ORDER BY Family_Name__c,
                                        Given_Name__c];
            }
            return Participants;
        }
        private set;
    }

    public List<RCR_Contract__c> ParticipantServiceOrders
    {
        get 
        {
            if(ParticipantServiceOrders == null)
            {               
                ParticipantServiceOrders = [SELECT     ID,
                                                        Name,
                                                        Start_Date__c,
                                                        End_Date__c,
                                                        Total_Service_Order_Cost__c ,
                                                        (SELECT GL_Category__c FROM RCR_Contract_Scheduled_Services__r)
                                            FROM        RCR_Contract__c
                                            WHERE       RecordType.Name = 'Service Order' AND 
                                                        Client__c = :controller.getID() AND 
                                                        Client__c != null
                                            ORDER BY Start_Date__c];
            }
            return ParticipantServiceOrders;
        }
        private set;
    }


    public List<RCR_Contract__c> Contracts
    {
        get 
        {
            if(tContracts == null)
            {               
                tContracts = new List<RCR_Contract__c>();
            }
            return tContracts;
        }
    }

    public void clear()
    {}

    public void searchOnLoad()
    {
        StartDateTo = Date.today();
        search();
        StartDateTo = null;
    }

    public void search()
    {
    	try
    	{
    		Date stDate = StartDateFrom == null ? Date.newInstance(2000, 1, 1) : StartDateFrom;
    		Date toDate = StartDateTo == null ? Date.newInstance(3000, 1, 1) : StartDateTo;
    		String clientName = (String.isBlank(ParticipantSearch) ? '' : ('%' + ParticipantSearch)) + '%';
    		tContracts = [SELECT 	ID,
    								Name,
    								Start_Date__c,
		            				End_Date__c,
		            				Total_Service_Order_Cost__c,
    								Client_Name__c,
    								Date_of_Birth__c
    					FROM 		RCR_Contract__c 
    					WHERE 		RecordType.Name = 'Service Order' AND
    								Client_Name__c LIKE :clientName AND 
    								Start_Date__c >= :stDate AND 
    								Start_Date__c <= :(CurrentContracts && StartDateFrom == null && StartDateTo == null ?  Date.today() : toDate) AND 
									End_Date__c >= :(CurrentContracts ?  Date.today() : Date.newInstance(2000, 1, 1))
						ORDER BY	Client_Name__c,
									Start_Date__c];
    	}
    	catch(Exception ex)
    	{
    		A2HCException.formatException(ex);
    	}
    }
}