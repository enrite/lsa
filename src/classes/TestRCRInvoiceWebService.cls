/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestRCRInvoiceWebService 
{
@IsTest//(SeeAlldata=true)
    static void myUnitTest()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        system.debug(RCRInvoiceWebService.PublicHolidays);

        RCR_Service__c service = [SELECT    ID,
                                    Name,
                                    Level__c,
                                    Day_of_Week__c,
                                    Description__c,
                                    No_Set_Rates__c,
                                    (SELECT Category__c,
                                            Service_Type__c
                                     FROM   RCR_Approved_Service_Type_Services__r),
                                    (SELECT Start_Date__c,
                                            End_Date__c,
                                            Rate__c,
                                            Public_Holiday_Rate__c
                                    FROM    RCR_Service_Rates__r),
                                    (SELECT ID,
                                            Name,
                                            Service_Code__c,
                                            Start_Date__c,
                                            End_Date__c,
                                            Original_End_Date__c,
                                            Flexibility__c,
                                            Public_Holidays_Not_Allowed__c,
                                            RCR_Service__r.Name,
                                            RCR_Contract__r.Name,
                                            RCR_Contract__r.Level__c,
                                            RCR_Contract__r.Start_Date__c,
                                            RCR_Contract__r.End_Date__c,
                                            Exception_Count__c,
                                            Use_Negotiated_Rates__c,
                                            Negotiated_Rate_Standard__c,
                                            Negotiated_Rate_Public_Holidays__c
                                    FROM    RCR_Contract_Scheduled_Services__r
                                    LIMIT 5)
                            FROM    RCR_Service__c
                            WHERE   Level__c = '1' AND
                                    ID IN (SELECT RCR_Service__c
                                            FROM RCR_Contract_Scheduled_Service__c)
                            LIMIT 1];
        
        RCR_Invoice_Batch__c batch = [SELECT    ID
                                        FROM    RCR_Invoice_Batch__c
                                        LIMIT 1];       
        
        try { system.debug(RCRInvoiceWebService.getNumberOfInvoicesInBatch(batch.Id)); } catch(Exception e) { }        
        try { system.debug(RCRInvoiceWebService.getMasterpieceAttachment(batch.Id)); } catch(Exception e) { }
        try { system.debug(RCRInvoiceWebService.getMasterpiecePaymentsForBatch(batch.Id)); } catch(Exception e) { }
        try { RCRInvoiceWebService.getScheduledServiceTotalCost(null, null, null, null); } catch(Exception e) { }
                                        
        //RCRInvoiceWebService.getServiceOrderPayments(batch.ID);
         
//        RCRInvoiceWebService.updateServiceOrderPayments([SELECT ID FROM RCR_Service_Order_Payment__c LIMIT 1], 12345); 
        
        RCRInvoiceWebService.updateBatch(batch.ID, 12345,  'test', 'test');

       

        for(RCR_Contract_Scheduled_Service__c css : service.RCR_Contract_Scheduled_Services__r)
        {
            ScheduleDateEngine dtEngine = new ScheduleDateEngine(css.ID, css.Start_Date__c, css.End_date__c);
            RCRInvoiceWebService.getTotals(service, css, css.Start_Date__c, css.End_date__c, dtEngine);
            RCRInvoiceWebService.getContractServices(css.RCR_Contract__c);
        }

        system.debug(RCRInvoiceWebService.getFiles(batch.ID));
        
        Test.StopTest();
    }
    
    @IsTest//(SeeAlldata=true)
    static void myUnitTest2()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        Boolean ranNormalRateTest = false;
        Boolean ranNegotiatedRateTest = false;
        
        for(Schedule__c schedule : [SELECT  Name
                                    FROM    Schedule__c
                                    WHERE   Number_of_weeks_in_period__c = 'Service Date Range'
                                    LIMIT 10])
        {
            for(RCR_Contract_Scheduled_Service__c css : [SELECT ID,
                                                                Name,
                                                                RCR_Contract__c,
                                                                RCR_Service__c,
                                                                Service_Code__c,
                                                                Start_Date__c,
                                                                End_Date__c,
                                                                Original_End_Date__c,
                                                                Flexibility__c,
                                                                Public_Holidays_Not_Allowed__c,
                                                                RCR_Service__r.Name,
                                                                RCR_Contract__r.Name,
                                                                RCR_Contract__r.Level__c,
                                                                RCR_Contract__r.Start_Date__c,
                                                                RCR_Contract__r.End_Date__c,
                                                                Exception_Count__c,
                                                                Use_Negotiated_Rates__c,
                                                                Negotiated_Rate_Standard__c,
                                                                Negotiated_Rate_Public_Holidays__c
                                                        FROM    RCR_Contract_Scheduled_Service__c
                                                        WHERE   Flexibility__c = :APS_PicklistValues.RCRContractScheduledService_Flexibilty_Accumulative AND
                                                                ID = :schedule.Name])
            {
                if(css.Use_Negotiated_Rates__c && !ranNegotiatedRateTest)
                {
                    RCR_Service__c service = RCRServiceExtension.getRCRService(css.RCR_Service__c);
                    //RCRInvoiceWebService.getContractServices(service, css, true); 
                    ranNegotiatedRateTest = true;
                }
                if(!css.Use_Negotiated_Rates__c && !ranNormalRateTest)
                {
                    RCR_Service__c service = RCRServiceExtension.getRCRService(css.RCR_Service__c);
                    //RCRInvoiceWebService.getContractServices(service, css, true); 
                    ranNormalRateTest = true;
                }
            }
            if(ranNormalRateTest && ranNegotiatedRateTest)
            {
                break;
            }   
        }
        
        Test.StopTest();           
    }
    
    @IsTest//(SeeAlldata=true)
    static void myUnitTest3()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        Boolean ranNormalRateTest = false;
        Boolean ranNegotiatedRateTest = false;
        
        for(Schedule__c schedule : [SELECT  Name
                                    FROM    Schedule__c
                                    //WHERE   Number_of_weeks_in_period__c != 'Service Date Range'
                                    LIMIT 10])
        {
            for(RCR_Contract_Scheduled_Service__c css : [SELECT ID,
                                                                Name,
                                                                RCR_Contract__c,
                                                                RCR_Service__c,
                                                                Service_Code__c,
                                                                Start_Date__c,
                                                                End_Date__c,
                                                                Original_End_Date__c,
                                                                Flexibility__c,
                                                                Public_Holidays_Not_Allowed__c,
                                                                RCR_Service__r.Name,
                                                                RCR_Contract__r.Name,
                                                                RCR_Contract__r.Level__c,
                                                                RCR_Contract__r.Start_Date__c,
                                                                RCR_Contract__r.End_Date__c,
                                                                Exception_Count__c,
                                                                Use_Negotiated_Rates__c,
                                                                Negotiated_Rate_Standard__c,
                                                                Negotiated_Rate_Public_Holidays__c
                                                        FROM    RCR_Contract_Scheduled_Service__c
                                                        WHERE   Flexibility__c != :APS_PicklistValues.RCRContractScheduledService_Flexibilty_Accumulative AND
                                                                ID = :schedule.Name])
            {
                if(css.Use_Negotiated_Rates__c && !ranNegotiatedRateTest)
                {
                    RCR_Service__c service = RCRServiceExtension.getRCRService(css.RCR_Service__c);
                    //RCRInvoiceWebService.getContractServices(service, css, true); 
                    ranNegotiatedRateTest = true;
                }
                if(!css.Use_Negotiated_Rates__c && !ranNormalRateTest)
                {
                    RCR_Service__c service = RCRServiceExtension.getRCRService(css.RCR_Service__c);
                    //RCRInvoiceWebService.getContractServices(service, css, true); 
                    ranNormalRateTest = true;
                }
            }
            if(ranNormalRateTest && ranNegotiatedRateTest)
            {
                break;
            }   
        }
        
        Test.StopTest();           
    }
}