public with sharing class ParticipantPlanEditRedirectExtension
{
    public ParticipantPlanEditRedirectExtension(ApexPages.StandardController c)
    {
        record = (Plan__c)c.getRecord();
    }

    private Plan__c record;

    public void sendSatalystMessage()
    {
        try
        {
            SatalystOutboundMessage.sendSatalystMessageImmediate(record.ID);
            record.Satalyst_Message_Sent__c = Datetime.now();
            update record;

            A2HCException.formatMessage('Bot message sent');
        }
        catch(Exception e)
        {
            A2HCException.formatException(e);
        }
    }
    
    public PageReference Redir()
    {
        try
        {                                                                                             
            Map<String, PageReference> pages = new Map<String, PageReference>();
            pages.put('MyPlan', Page.MyPlanEdit);                         
        
            RecordType rt = [SELECT Name,
                                    DeveloperName
                             FROM RecordType
                             WHERE Id = :record.RecordTypeId
                             OR Id = :ApexPages.CurrentPage().getParameters().get('RecordType')];
                                    
            // find the page reference to redirect to
            PageReference pg;
            
            if (pages.containsKey(rt.DeveloperName))
            {                        
                pg = pages.get(rt.DeveloperName);
            }            
            else
            {
                if (record.Id == null)
                {
                    pg = new PageReference('/' + Plan__c.sObjectType.getDescribe().getKeyPrefix() + '/e');
                }
                else
                {
                    pg = new PageReference('/' + record.Id + '/e');
                }
                
                pg.getParameters().put('nooverride', '1');
            }
            
            // add the parameters for this page to the new page reference
            for (String k : ApexPages.CurrentPage().getParameters().keySet())
            {
                if (k != 'save_new' && k != '_CONFIRMATIONTOKEN')
                {
                    pg.getParameters().put(k, ApexPages.CurrentPage().getParameters().get(k));
                }
            }
            
            return pg;
        }
        catch(Exception e)
        {
            return A2HCException.formatException(e);
        }
    }
}