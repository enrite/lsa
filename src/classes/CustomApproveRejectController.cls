public class CustomApproveRejectController
{
    public CustomApproveRejectController()
    {
        Initialise();        
    }
        
    public String ObjectLabel { get; set; }
    public String ObjectNameLabel { get; set; }
    public String RecordId { get; set; }
    public String RecordName { get; set; }
    public String Comments { get; set; }
    public Boolean CloseWindow { get; set; }

    private void Initialise()
    {
        ObjectLabel = ApexPages.CurrentPage().getParameters().get('objLbl'); 
        ObjectNameLabel = ApexPages.CurrentPage().getParameters().get('nameLbl');
        RecordId = ApexPages.CurrentPage().getParameters().get('id');
        RecordName = ApexPages.CurrentPage().getParameters().get('name');  
        CloseWindow = false;      
    }
    
    public void Approve()
    {
        try
        {                        
            // approve the request
            Approval.ProcessResult result =  Approval.process(Request('Approve'));
                                      
            CloseWindow = true;
        }
        catch(Exception e)
        {
            A2HCException.formatException(e);
        }    
    }
    
    public void Reject()
    {
        try
        {                        
            // reject the request
            Approval.ProcessResult result =  Approval.process(Request('Reject'));
                                      
            CloseWindow = true;
        }
        catch(Exception e)
        {
            A2HCException.formatException(e);
        }    
    }
    
    private Approval.ProcessWorkitemRequest Request(String action)
    {
        // Instantiate the new ProcessWorkitemRequest object and populate it
        Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
        req.setComments(Comments);
        req.setAction(action);
        req.setNextApproverIds(new Id[] { UserInfo.getUserId() });                    
        req.setWorkitemId(ApexPages.CurrentPage().getParameters().get('id'));
        
        return req;
    }
}