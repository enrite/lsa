/**
 * Created by me on 19/03/2019.
 */

public class SatalystPlanJSON
{
    public String DraftMyPlanId;

    public static String serialisePlan(Plan__c plan)
    {
        SatalystPlanJSON planJSON = new SatalystPlanJSON();
        planJSON.DraftMyPlanId = plan.ID;
        return JSON.serialize(planJSON);
    }
}