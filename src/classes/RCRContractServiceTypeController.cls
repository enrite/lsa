public class RCRContractServiceTypeController extends ComponentControllerBase
{
    public RCRContractServiceTypeController()
    {
    }
    
    public RCRContractServiceTypeController(List<RCR_Contract_Service_Type__c> defaultContractTypes, sObject pParentObject)
    {
        for(RCR_Contract_Service_Type__c serviceType : defaultContractTypes)
        {
            ContractServiceTypes.add(new ContractServiceTypeWrapper(ContractServiceTypes.size(), serviceType));
        }
        ParentObject = pParentObject;
    }
    
    public Boolean PageMode
    {
        get
        {
            return PageMode == null ? true : PageMode;
        }
        set;
    }

    public string SelectedProgram
    {
        get;
        set;
    }

    public string SelectedProgramCategory
    {
        get;
        set;
    }

    public String SelectedServiceType
    {
        get;
        set;
    }        

    public integer rowIndex
    {
        get;
        set;
    }

    public RCR_Contract__c RCRContract
    {
        get;
        set;
    }

    public sObject ParentObject
    {
        get;
        set;
    }

    public Set<Id> allowedPrograms = new Set<Id>();
    public Set<Id> allowedProgramCategories = new Set<Id>();

    public String ServiceTypeString
    {
        get
        {
            String s = '';
            for(ContractServiceTypeWrapper w : ContractServiceTypes)
            {
                s += w.ContractServiceType.RCR_Program_Category_Service_Type__r.Name == null ? '' : (w.ContractServiceType.RCR_Program_Category_Service_Type__r.Name + '<br/>');
                // this will be displayed through a formula field
                // so limit to less than 255 characters
                if(s.length() < 240)
                {
                    ServiceTypeString = s;
                }
                else
                {
                    ServiceTypeString += '... and more';
                    break;
                }
            }
            return ServiceTypeString;
        }
        private set;
    }


    public String GLCategoryString
    {
        get
        {
           String s = '';
           for(ContractServiceTypeWrapper w : ContractServiceTypes)
            {
                if(s.length()>0)
                {
                    s += '\n';
                }

                if(w.ContractServiceType.RCR_Program_Category_Service_Type__r != null)
                {
                    s += w.ContractServiceType.RCR_Program_Category_Service_Type__r.RCR_Program_Category__r.Name == null ? '' : (w.ContractServiceType.RCR_Program_Category_Service_Type__r.RCR_Program_Category__r.Name);
                }

                if(s.length() < 240)
                {
                    GLCategoryString = s;
                }
                else
                {
                    GLCategoryString += '... and more';
                    break;
                }
            }
            return GLCategoryString;
        }
        private set;
    }   

    public Boolean IsAdhocService
    {
        get
        {
            return ParentObject != null && ParentObject.getSObjectType() == RCR_Contract_Adhoc_Service__c.sObjectType;
        }
    }

    public List<SelectOption> Programs
    {
        get
        {
            if(Programs == null)
            {
                Programs = new List<SelectOption>();
                Programs.add(new SelectOption('', '--None--'));
                                
                for(RCR_Program__c p : [SELECT  Id,
                                                Name
                                        FROM    RCR_Program__c
                                        WHERE   Id IN :allowedPrograms
                                        ORDER BY Name])
                {
                    Programs.add(new SelectOption(p.Id, p.Name));                                   
                }                                
            }
                                                            
            return Programs;
        }
        set;
    }

    public List<SelectOption> ProgramCategories
    {
        get
        {
            if(ProgramCategories == null)
            {
                ProgramCategories = new List<SelectOption>();
                ProgramCategories.add(new SelectOption('', '--None--'));
            }
            return ProgramCategories;
        }
        private set;
    }

    public List<SelectOption> ServiceTypes
    {
        get
        {
            if(ServiceTypes == null)
            {
                ServiceTypes = new List<SelectOption>();
            }
            return ServiceTypes;
        }
        private set;
    }

    public List<ContractServiceTypeWrapper> ContractServiceTypes
    {
        get
        {
            if(ContractServiceTypes == null)
            {
                ContractServiceTypes = new List<ContractServiceTypeWrapper>();
                if(ParentObject != null)
                {
                    for(RCR_Contract_Service_Type__c serviceType : [SELECT ID,
                                                                            RCR_Program_Category_Service_Type__c,
                                                                            RCR_Program_Category_Service_Type__r.Name,
                                                                            RCR_Program_Category_Service_Type__r.RCR_Program_Category__r.Name,
                                                                            RCR_Program__c,
                                                                            Quantity__c,
                                                                            Allocation_Service_Type__c,
                                                                            RCR_Contract_Scheduled_Service__c,
                                                                            RCR_Contract_Adhoc_Service__c
                                                                    FROM    RCR_Contract_Service_Type__c
                                                                    WHERE   RCR_Contract_Scheduled_Service__c = :(ParentObject.Id == null ? '~' : ParentObject.Id) OR
                                                                            RCR_Contract_Adhoc_Service__c = :(ParentObject.Id == null ? '~' : ParentObject.Id)
                                                                    ORDER BY RCR_Program_Category_Service_Type__r.Name])
                    {
                        ContractServiceTypes.add(new ContractServiceTypeWrapper(ContractServiceTypes.size(), serviceType));
                    }
                }
            }
            return ContractServiceTypes;
        }
        set;
    }

    public PageReference loadProgramCategories()
    {
        try
        {
            SelectedProgramCategory = null;
            getProgramCategories();
            return null;
        }
        catch(Exception e)
        {
            return A2HCException.formatException(e);
        }
    }

    public PageReference getServiceTypes()
    {
        try
        {
            ServiceTypes = null;
            ServiceTypes.add(new SelectOption('', '--None--'));
            if(SelectedProgramCategory != null && SelectedProgramCategory != '-1')
            {
                Set<ID> existingServiceTypes = new Set<ID>();
                for(ContractServiceTypeWrapper w : ContractServiceTypes)
                {
                    existingServiceTypes.add(w.ContractServiceType.RCR_Program_Category_Service_Type__c);
                }                
                
                // find the applicable approved service type Ids
                Set<Id> astIds = new Set<Id>();
                if (ParentObject.getSObjectType() == RCR_Contract_Scheduled_Service__c.sObjectType)
                {
                    for (RCR_Approved_Service_Type_Service__c asts : [SELECT Id,
                                                                             RCR_Approved_Service_Type__c 
                                                                      FROM RCR_Approved_Service_Type_Service__c
                                                                      WHERE RCR_Service__c = :((RCR_Contract_Scheduled_Service__c)ParentObject).RCR_Service__c])
                    {
                        astIds.add(asts.RCR_Approved_Service_Type__c);
                    }                                                                      
                }                 

                for(RCR_Approved_Service_Type__c serviceType : [SELECT (SELECT  RCR_Program_Category_Service_Type__c,
                                                                                RCR_Program_Category_Service_Type__r.Name,
                                                                                RCR_Program_Category_Service_Type__r.RCR_Program_Category__r.Name,
                                                                                Approved__c
                                                                        FROM    RCR_Approved_Service_Types__r
                                                                        WHERE   Id IN :astIds
                                                                        AND     Approved__c = true
                                                                        ORDER BY RCR_Program_Category_Service_Type__r.Name)
                                                                FROM     RCR_Account_Program_Category__c
                                                                WHERE    Id = :SelectedProgramCategory].RCR_Approved_Service_Types__r)                                                          
                {                                             
                    if(!existingServiceTypes.contains(serviceType .RCR_Program_Category_Service_Type__c))
                    {
                        ServiceTypes.add(new SelectOption(serviceType .RCR_Program_Category_Service_Type__c, serviceType.RCR_Program_Category_Service_Type__r.Name));                                                        
                    }                                     
                }                
            }
            /*
            if(SelectedProgramCategory != null && SelectedProgramCategory != '-1')
            {
                Set<ID> existingServiceTypes = new Set<ID>();
                for(ContractServiceTypeWrapper w : ContractServiceTypes)
                {
                    existingServiceTypes.add(w.ContractServiceType.RCR_Program_Category_Service_Type__c);
                }
                List<RCR_Approved_Service_Type__c> authorisedServiceTypes = new List<RCR_Approved_Service_Type__c>();
                List<RCR_Approved_Service_Type__c> unAuthorisedServiceTypes = new List<RCR_Approved_Service_Type__c>();
                                
                for(RCR_Approved_Service_Type__c serviceType : [SELECT (SELECT  RCR_Program_Category_Service_Type__c,
                                                                                RCR_Program_Category_Service_Type__r.Name,
                                                                                Approved__c
                                                                        FROM    RCR_Approved_Service_Types__r
                                                                        ORDER BY RCR_Program_Category_Service_Type__r.Name)
                                                                FROM     RCR_Account_Program_Category__c
                                                                WHERE    Id = :SelectedProgramCategory].RCR_Approved_Service_Types__r)                                                          
                {                                    
                    if(serviceType.Approved__c)
                    {                        
                        authorisedServiceTypes.add(serviceType);
                    }
                    else
                    {
                        unAuthorisedServiceTypes.add(serviceType);
                    }
                }

                if(authorisedServiceTypes.size() > 0)
                {
                    SelectOption option = new SelectOption('', '<optgroup label=\'Authorised\'></optgroup>');
                    option.setEscapeItem(false);
                    ServiceTypes.add(option);

                    for(RCR_Approved_Service_Type__c t : authorisedServiceTypes)
                    {
                        if(!existingServiceTypes.contains(t.RCR_Program_Category_Service_Type__c))
                        {
                            ServiceTypes.add(new SelectOption(t.RCR_Program_Category_Service_Type__c, t.RCR_Program_Category_Service_Type__r.Name));                                                        
                        }
                    }                                        
                }

                if(unAuthorisedServiceTypes.size() > 0)
                {
                    SelectOption option = new SelectOption('', '<optgroup label=\'Un-authorised\'></optgroup>');
                    option.setEscapeItem(false);
                    ServiceTypes.add(option);

                    for(RCR_Approved_Service_Type__c t : unAuthorisedServiceTypes)
                    {
                        if(!existingServiceTypes.contains(t.RCR_Program_Category_Service_Type__c))
                        {
                            ServiceTypes.add(new SelectOption(t.RCR_Program_Category_Service_Type__c, t.RCR_Program_Category_Service_Type__r.Name));
                        }
                    }
                }
            }
            */
            return null;
        }
        catch(Exception e)
        {
            return A2HCException.formatException(e);
        }
    }

    public PageReference addContractServiceType()
    {
        try
        {
            if(SelectedProgram == null)
            {
                A2HCException.formatException('A program must be selected.');
                return null;
            }
            if(SelectedProgramCategory == null)
            {
                A2HCException.formatException('A category must be selected.');
                return null;
            }
            if(SelectedServiceType == null)
            {
                A2HCException.formatException('A service type must be selected.');
                return null;
            }
            
            // check if the selected service type has the correct GL code
            for (RCR_Program_Category_Service_Type__c pgst : [SELECT Finance_Code_GG__c,
                                                                     Finance_Code_NSAG__c            
                                                              FROM RCR_Program_Category_Service_Type__c
                                                              WHERE Id = :SelectedServiceType])
            { system.debug('debug1:' + RCRContract.Account__r.Finance_Code__c); system.debug('debug2:' + pgst);
                if ((RCRContract.Account__r.Finance_Code__c == 'GG' && pgst.Finance_Code_GG__c == null)
                    || (RCRContract.Account__r.Finance_Code__c == 'NSAG' && pgst.Finance_Code_NSAG__c == null))
                {
                    return A2HCException.formatException('This Service Type cannot be selected as it does not have a GL Code associated with it.');
                }  
            }   
                        
            // Prevent duplicate service types being added to list.
            for(ContractServiceTypeWrapper cstw : ContractServiceTypes)
            {
                if(cstw.ContractServiceType.RCR_Program_Category_Service_Type__c == SelectedServiceType)
                {
                    A2HCException.formatException('This Service Type has already been selected.');
                    return null;
                }
            }           
            RCR_Contract_Service_Type__c cst = new RCR_Contract_Service_Type__c(
                                RCR_Program__c = SelectedProgram,
                                RCR_Program_Category_Service_Type__c = SelectedServiceType);

            if(ParentObject.getSObjectType() == RCR_Contract_Scheduled_Service__c.sObjectType)
            {
                cst.RCR_Contract_Scheduled_Service__c = ParentObject.Id;
            }
            else if(ParentObject.getSObjectType() == RCR_Contract_Adhoc_Service__c.sObjectType)
            {
                cst.RCR_Contract_Adhoc_Service__c = ParentObject.Id;
            }

            ContractServiceTypes.add(new ContractServiceTypeWrapper(ContractServiceTypes.size(), cst));
            getServiceTypes();
            return null;
        }
        catch(Exception e)
        {
            return A2HCException.formatException(e);
        }
    }

    public decimal getTotalQuantity()
    {
        decimal qty = 0.0;
        for(ContractServiceTypeWrapper s : ContractServiceTypes)
        {
            qty += s.ContractServiceType.Quantity__c == null ? 0.0 :s.ContractServiceType.Quantity__c;
        }
        return qty;
    }

    public void saveContractServiceTypes()
    {
        List<RCR_Contract_Service_Type__c> cst = new List<RCR_Contract_Service_Type__c>();
        for(ContractServiceTypeWrapper s : ContractServiceTypes)
        {
            if(ParentObject.getSObjectType() == RCR_Contract_Scheduled_Service__c.sObjectType)
            {
                s.ContractServiceType.RCR_Contract_Scheduled_Service__c = ParentObject.Id;
            }
            else if(ParentObject.getSObjectType() == RCR_Contract_Adhoc_Service__c.sObjectType)
            {
                s.ContractServiceType.RCR_Contract_Adhoc_Service__c = ParentObject.Id;
            }
            /* skip the quantity check
            if(PageMode)
            {                
                if(!IsAdhocService && s.ContractServiceType.Quantity__c == null)
                {
                    throw new A2HCException('Service type quantity is required.');
                }
            }
            else if(s.ContractServiceType.Quantity__c == null)
            {
                continue;
            }
            */
            cst.add(s.ContractServiceType);
        }
        upsert cst;

        Set<ID> savedServiceTypes = new Set<ID>();
        for(RCR_Contract_Service_Type__c cs : cst)
        {
            savedServiceTypes.add(cs.ID);
        }
        delete [SELECT  ID
                FROM    RCR_Contract_Service_Type__c
                WHERE   (RCR_Contract_Scheduled_Service__c = :ParentObject.Id OR
                        RCR_Contract_Adhoc_Service__c = :ParentObject.Id) AND
                        ID NOT IN :savedServiceTypes];

        ContractServiceTypes = null;
    }

    public void SelectServiceType()
    {
        try
        {                                                                                   
            if (ContractServiceTypes.size() > 0)
            {
                rowIndex = 0;
                deleteContractServiceType();            
            } 
                        
            if (SelectedServiceType != null)
            {
                addContractServiceType();    
            }                
        }
        catch(Exception e)
        {
            A2HCException.formatException(e);
        }
    }    

    public PageReference deleteContractServiceType()
    {
        try
        {
            ContractServiceTypes.remove(rowIndex);
            integer cnt = 0;
            for(ContractServiceTypeWrapper item : ContractServiceTypes)
            {
                item.rowIndx = cnt++;
            }
            getServiceTypes();
            return null;
        }
        catch(exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }

    public void cloneContractServiceTypes()
    {
        try
        {
            for(ContractServiceTypeWrapper cstw : ContractServiceTypes)
            {
                cstw.ContractServiceType = cstw.ContractServiceType.clone(false, true);
            }
        }
        catch(exception ex)
        {
            throw new A2HCException(ex);
        }
    }
    
    @TestVisible
    private void getProgramCategories()
    {
        ProgramCategories = null;
        if(RCRContract.Account__c != null && SelectedProgram != null)
        {            
            for(RCR_Account_Program_Category__c serviceType : [SELECT   r.Id,
                                                                        r.Name
                                                               FROM     RCR_Account_Program_Category__c r
                                                               WHERE    r.Account__c = :RCRContract.Account__c
                                                               AND      r.RCR_Program_Category__r.RCR_Program__c = :SelectedProgram
                                                               AND      r.RCR_Program_Category__c IN :allowedProgramCategories
                                                               ORDER BY r.Name])
            {
                ProgramCategories.add(new SelectOption(serviceType.Id, serviceType.Name));                
            }                        
        }
    }

    public class ContractServiceTypeWrapper
    {
        public ContractServiceTypeWrapper(integer pRowIndx, RCR_Contract_Service_Type__c pContractServiceType)
        {
            RowIndx = pRowIndx;
            ContractServiceType = pContractServiceType;
        }

        public integer RowIndx
        {
            get;
            set;
        }

        public RCR_Contract_Service_Type__c ContractServiceType
        {
            get;
            set;
        }
    }


}