public class LPInvoiceDetailJSON
{
    public Decimal funded;
    public Decimal totalClaimAmount;
    public biller biller;
    public Decimal unfunded;
    public String invoiceNumber;
    public String submittedAtUtc;
    public String invoiceDate;
    public list<claim> claims;
    public String invoiceKey;
    public String programCode;

    public class quantity
    {
        public Decimal units;
        public String duration;
    }
    public class fundingSource
    {
        public list<String> insufficientContributionReasons;
        public Boolean requiresAuthorization;
        public Decimal contribution;
        public String contractNumber;
        public String name;
        public String preauthorizationNumber;
    }

    public class claim
    {
        public Decimal unitPriceIncTax;
        public Decimal totalPrice;
        public quantity quantity;
        public Decimal totalClaimAmount;
        public Decimal unitTax;
        public Decimal funded;
        public String itemName;
        public Decimal unfunded;
        public String itemCode;
        public String taxCode;
        public String lineItemId;
        public String claimKey;
        public list<fundingSource> fundingSources;
        public String serviceEndTime;
        public list<String> validationErrors;
        public String serviceStartTime;
        public state state;
    }
    public class biller
    {
        public String name;
        public String billerKey;
    }
    public class state
    {
        public Integer value;
        public String displayName;
    }

    public static LPInvoiceDetailJSON parse(String json)
    {
        system.debug(json);
        return (LPInvoiceDetailJSON) System.JSON.deserialize(json, LPInvoiceDetailJSON.class);
    }

    public List<RCR_Invoice__c> createInvoices(LPRESTInterface lpInterface, LPInvoiceRequestJSON invoiceRequestJSON)
    {
        Set<String> invoiceKeys = new Set<String>();
        Account serviceProvider = new Account(LP_Biller_Key__c = biller.billerKey);
        Referred_Client__c client = new Referred_Client__c();
        for(Referred_Client__c c :[SELECT ID
                                    FROM    Referred_Client__c
                                    WHERE   Client_ID__c = :invoiceRequestJSON.data.membershipNumber AND
                                            Client_ID__c != null])
        {
            client = c;
        }

        Map<String, RCR_Invoice_Item__c> cancelledItemsByLPKey = new Map<String, RCR_Invoice_Item__c>();
        for(LPInvoiceDetailJSON.claim cl: claims)
        {
            switch on cl.state.displayName
            {
                when 'Cancelled', 'Refunded'
                {
                    cancelledItemsByLPKey.put(cl.claimKey, null);
                }
            }
        }
        Set<ID> serviceOrdersToRecalculate = new Set<ID>();
        for(RCR_Invoice_Item__c cancelledItem : [SELECT ID,
                                                        Status__c,
                                                        LP_Claim_Key__c,
                                                        RCR_Contract_Scheduled_Service__r.RCR_Contract__c,
                                                        RCR_Invoice__r.Name,
                                                        RCR_Invoice__r.RCR_Invoice_Batch__c
                                                FROM    RCR_Invoice_Item__c
                                                WHERE   LP_Claim_Key__c IN :cancelledItemsByLPKey.keySet() AND
                                                        LP_Claim_Key__c != null])
        {
            cancelledItemsByLPKey.put(cancelledItem.LP_Claim_Key__c, cancelledItem);
        }

        ID itemRecordTypeId = RCR_Invoice_Item__c.SObjectType.getDescribe().getRecordTypeInfosByDeveloperName().get('Created').getRecordTypeId();
        Map<String, RCR_Invoice__c> invoicesByInvoiceNo = new Map<String, RCR_Invoice__c>();
        Map<String, RCR_Invoice_Item__c> invoiceItemsByInvoiceNo = new Map<String, RCR_Invoice_Item__c>();
        List<RCR_Invoice_Item__c> cancelledInvoiceItems = new List<RCR_Invoice_Item__c>();
        Boolean taskCreated = false;
        Integer i = 1;
        for(LPInvoiceDetailJSON.claim cl: claims)
        {
            switch on cl.state.displayName
            {
                when 'Authorized', 'AuthorizationRequired'
                {
                    RCR_Invoice__c invoice = new RCR_Invoice__c();
                    invoice.RecordTypeId = RCR_Invoice__c.SObjectType.getDescribe().getRecordTypeInfosByDeveloperName().get('Created').getRecordTypeId();
                    invoice.Account__r = serviceProvider;
                    invoice.LP_Invoice_Number__c = invoiceNumber + '(' + String.valueOf(i) + ')';
                    invoice.LP_Invoice_Key__c = invoiceKey;
                    //invoice.Invoice_Number__c = invoiceNumber; // this will be replaced with the RCR_Invoice__c.Name field after insert
                    Date dt = LPRESTInterface.parseDate(invoiceDate);
                    invoice.Date__c = dt;
                    invoice.Start_Date__c = dt;
                    invoice.End_Date__c = dt;
                    invoice.Use_New_Process__c = true;
                    invoicesByInvoiceNo.put(invoice.LP_Invoice_Number__c, invoice);
                    invoiceItemsByInvoiceNo.put(invoice.LP_Invoice_Number__c, getInvoiceItem(itemRecordTypeId, client, cl));
                    i++;
                }
                when 'Cancelled', 'Refunded'
                {
                    RCR_Invoice_Item__c cancelledItem = cancelledItemsByLPKey.get(cl.claimKey);
                    if(cancelledItem == null)
                    {
                        RCR_Invoice_Item__c item = getInvoiceItem(itemRecordTypeId, client, cl);
                        item.LP_Cancellation_Key__c = cl.claimKey;
                        cancelledInvoiceItems.add(item);
                    }
                    else
                    {
                        if(cancelledItem.RCR_Invoice__r.RCR_Invoice_Batch__c == null)
                        {
                            cancelledItem.LP_Cancellation_Key__c = cl.claimKey;
                            serviceOrdersToRecalculate.add(cancelledItem.RCR_Contract_Scheduled_Service__r.RCR_Contract__c);
                            cancelledInvoiceItems.add(cancelledItem);
                        }
                        else if(!taskCreated)
                        {
                            Task t = new Task();
                            t.Assigned_to_Group__c = 'Finance';
                            t.Subject = 'Invoice cancelled/refunded in LanternPay';
                            t.Description = 'LanternPay invoice ' + cancelledItem.RCR_Invoice__r.Name + ' has been refunded in LanternPay and an adjustment will be needed in SALSA';
                            insert t;
                            taskCreated = true;
                        }
                    }
                }
            }
        }
        if(!invoicesByInvoiceNo.isEmpty())
        {
            lpInterface.statementsByInvoiceNo.putAll(invoicesByInvoiceNo);
            lpInterface.statementItemsByInvoiceNo.putAll(invoiceItemsByInvoiceNo);
        }
//        if(invoiceItems.size() > 0)
//        {
//            lpInterface.statementsByInvoiceNo.put(invoice.LP_Invoice_Number__c, invoice);
//            lpInterface.statementItemsByInvoiceNo.put(invoice.LP_Invoice_Number__c, invoiceItems);
//        }
        if(!cancelledInvoiceItems.isEmpty())
        {
            update cancelledInvoiceItems;
            RCRReconcileInvoice.setServiceTotals(serviceOrdersToRecalculate, true);
        }
        return invoicesByInvoiceNo.values();
    }

    private RCR_Invoice_Item__c getInvoiceItem(Id itemRecordTypeId, Referred_Client__c client, LPInvoiceDetailJSON.claim cl)
    {
        RCR_Invoice_Item__c item = new RCR_Invoice_Item__c();
        item.RecordTypeId = itemRecordTypeId;
        if(client != null)
        {
            item.Client__c = client.ID;
        }
        for(LPInvoiceDetailJSON.fundingSource fs : cl.fundingSources)
        {
            item.Service_Order_Number__c = fs.contractNumber;
            item.LP_Scheduled_Service__c = fs.preauthorizationNumber;
            item.LP_Claim_Status__c = fs.requiresAuthorization ? LPConstants.CLAIMSTATUS_PENDINGAUTHORISATION : LPConstants.CLAIMSTATUS_AUTHORISED;
        }
        item.LP_Claim_Key__c = cl.claimKey;
        item.Activity_Date__c = LPRESTInterface.parseDate(cl.serviceStartTime);
        item.Code__c = cl.itemCode;
        item.Quantity__c = cl.quantity.units * (cl.state.value == 10 ? -1.0 : 1.0);
        item.LP_Entered_Time__c = cl.quantity.duration;
        item.LP_State__c = cl.state.displayName;
        Decimal gst = cl.unitTax == null ? 0 : cl.unitTax;
        item.Rate__c = cl.unitPriceIncTax - gst;
        item.GST__c = gst;
        item.Total_Item_GST__c = item.Quantity__c * cl.unitTax;// item.Rate__c * item.GST__c;
        item.Total__c = cl.totalClaimAmount; //item.Rate__c * item.Quantity__c;
        item.Bulk_Upload__c = true;
        return item;
    }
}