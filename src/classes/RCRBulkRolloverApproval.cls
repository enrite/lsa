public class RCRBulkRolloverApproval extends A2HCPageBase 
{
    private final Integer ROLLOVERAPPROVAL_BATCHSIZE = 300;
    private final Integer CSAAPPROVAL_BATCHSIZE = 300;
    
    public string SelectedAccount
    {
        get
        {
            if(SelectedAccount == null)
            {
            	
                SelectedAccount = '';
            }
            return SelectedAccount;
        }
        set;
    }
    
    public string Client
    {
    	get;
    	set;
    }
    
    public string CCMSID
    {
    	get;
    	set;
    }
    
    public string ActionType
    {
        get
        {
        	return getParameter('type');
        }
    }
    
   public Boolean CanRolloverApprove
    {
    	get
    	{
    		if(CanRolloverApprove == null)
    		{
    			CanRolloverApprove = isUserInPermissionSet('RCR_Bulk_Rollover_Approval') || isUserInPermissionSet('RCR_Bulk_Rollover_Approval_INT');
    		}
    		return CanRolloverApprove;
    	}
    	private set;
    }
    
    public Boolean CanRolloverRecommend
    {
    	get
    	{
    		if(CanRolloverRecommend == null)
    		{
    			CanRolloverRecommend = isUserInPermissionSet('RCR_Bulk_Rollover_Recommend') || isUserInPermissionSet('RCR_Bulk_Rollover_Recommend_INT');
    		}
    		return CanRolloverRecommend;
    	}
    	private set;
    }
    
    public Boolean CanCSAApprove
    {
    	get
    	{
    		if(CanCSAApprove == null)
    		{
    			if(CanCSAApprove == null)
	    		{
	    			CanCSAApprove = isUserInPermissionSet('Bulk_CSA_Approval');
    			}
    		}
    		return CanCSAApprove;
    	}
    	private set;
    }
    
    public List<SelectOption> AccountsWithRollovers
    {
        get
        {    	
            if(AccountsWithRollovers == null)
            { 
                if(ActionType == 'CSAApproval')
                {
                	AccountsWithRollovers = getSelectListFromObjectList(
                								[SELECT 	ID,
                											Name
                									FROM	Account
                									WHERE ID IN (SELECT Account__c
                												FROM	RCR_Contract__c
                												WHERE   RecordType.Name = :APSRecordTypes.RCRServiceOrder_ServiceOrder
				                                                AND     Source_Record__r.RecordType.Name = :APSRecordTypes.RCRSERVICEORDER_ROLLOVER				                                                
				                                                AND     CSA_Status__c = null
				                                                AND		CSA_Bulk_Approved__c = false)
	                                                ORDER BY Name], true);
                }
                else
               	{
               		AccountsWithRollovers = getNewSelectList(true);
               	}
            }                 
            return AccountsWithRollovers;
        }
        private set;
    }   
    
    @testVisible
    public List<ServiceOrderWrapper> Rollovers
    {
    	get
    	{
    		if(Rollovers == null)
    		{
    			Rollovers = new List<ServiceOrderWrapper>();
    		}
    		return Rollovers;
    	}
    	private set;
    }
    
    
    public TotalsWrapper Totals
    {
    	get
    	{
    		if(Totals == null)
    		{
    			Totals = new TotalsWrapper();
    		}
    		return Totals;
    	}
    	private set;
    }

    
    public void doSearch()
    { 
    	try
    	{
    		throwTestError();
    		
    		Rollovers = null;
    		Totals = new TotalsWrapper();

    		if(ActionType == 'CSAApproval')
	        {
	        	if(String.isBlank(SelectedAccount) && String.isBlank(Client) && String.isBlank(CCMSID))
	        	{
	        		A2HCException.formatException('You must enter at least one search criteria.');
	            	return;
	        	}
	        	
	            for(RCR_Contract__c serviceOrder : [SELECT  Id,
	                                                        Name,
	                                                        Account__c,
	                                                        Account__r.Name,
	                                                        Start_Date__c,
	                                                        End_Date__c,
	                                                        Total_Service_Order_Cost__c,
	                                                        CCMS_ID__c,
	                                                        Client__c,
	                                                        Client__r.Name,
	                                                        Client__r.CCMS_ID__c,
	                                                        CSA_Status__c,
	                                                        Visible_To_Service_Providers__c,
	                                                        CSA_Bulk_Approved__c,
	                                                        Rollover_Rejection_Reason__c,
	                                                        Rollover_Recommendation_Descision__c,
	                                                        Source_Record__r.Source_Record__r.Orig_Cost__c,
	                                                        Source_Record__r.Total_Service_Order_Cost__c,
	                                                        Manual_CSA_Acceptance__c
	                                                FROM    RCR_Contract__c
	                                                WHERE   (Account__c = :SelectedAccount OR
	                                                        Name LIKE :(SelectedAccount == '' ? '%' : '~'))
	                                                AND		RecordType.Name = :APSRecordTypes.RCRServiceOrder_ServiceOrder
	                                                AND     Source_Record__r.RecordType.Name = :APSRecordTypes.RCRSERVICEORDER_ROLLOVER
	                                                AND     CSA_Status__c = :APS_PicklistValues.RCRContractCSAStatus_Pending_Approval
	                                                AND		CSA_Bulk_Approved__c = false
		                                            AND     (
			                                            		(
			                                                        Client__r.CCMS_ID__c != null AND
			                                                        Client__r.CCMS_ID__c LIKE :(String.isBlank(CCMSID) ? '%' : CCMSID) AND
			                                                        Client__r.Name LIKE :('%' + Client + '%') 
			                                                    )  
			                                                    OR
			                                                    (   
			                                                        Client__r.Account__r.API_Name__c = :APS_PicklistValues.Account_DCSI_RCR_Programs AND
			                                                        Client__r.CCMS_ID__c LIKE :(String.isBlank(CCMSID) ? '' : '~') AND
			                                                        Client__r.Name LIKE :('%' + Client + '%') 
			                                                    )
			                                                ) 
	                                                ORDER BY Client__r.Family_Name__c, 
	                                                		Name                                            		
	                                                LIMIT 	:(CSAAPPROVAL_BATCHSIZE + 1)])
	            {
	                Rollovers.add(new ServiceOrderWrapper(serviceOrder));
	            }    
	            
	            if(Rollovers.size() > CSAAPPROVAL_BATCHSIZE)
	            {  
					Rollovers.remove(Rollovers.size() - 1);           	
        			A2HCException.formatWarning('Your search returned more than the limit of ' + 
        					CSAAPPROVAL_BATCHSIZE.format() + 
        					' records. You will need to process the remainder of the Service Orders in the next batch.');	   
	            }         	          	
	            calculateTotals(); 
	        }   
    	}
    	catch(Exception ex)
    	{
    		A2HCException.formatException(ex);
    	}
    } 

    public PageReference approve()
    {
        try
        {
            throwTestError();
            
            List<RCR_Contract__c> serviceOrders = new List<RCR_Contract__c>();
            List<Id> serviceOrderIDs = new List<Id>();
            List<Id> csaAcceptanceIDs = new List<Id>();
            
            for(User u : [SELECT Id
	                      FROM   User u
	                      WHERE  Authorised_To_Accept_CSA__c = true
	                      AND    Contact.AccountId = :SelectedAccount
	                      AND    Contact.RecordType.Name = :APSRecordTypes.Contact_ServiceProvider])
	        {
				csaAcceptanceIDs.add(u.Id);
	        } 
            
            for(ServiceOrderWrapper wrapper : Rollovers)
            {
            	if(wrapper.ApprovedOnceOff)
            	{
	                wrapper.ServiceOrder.CSA_Bulk_Approved__c = true;
	                if(csaAcceptanceIDs.size() == 0)
	                {
	                	wrapper.ServiceOrder.Manual_CSA_Acceptance__c = true;
	                }
	                serviceOrders.add(wrapper.serviceOrder);
	                serviceOrderIDs.add(wrapper.serviceOrder.Id);
            	}
            }
            
            update serviceOrders;
            
            ServiceOrderFuture.submitServiceOrdersForApproval(serviceOrderIDs, 'Service Order', csaAcceptanceIDs, true); 
            
            Rollovers = null;
            //SelectedAccount = null;
            doSearch();

            ApexPages.currentPage().getParameters().put('type', 'CSAApproval');
            A2HCException.formatMessage(serviceOrders.size().format() + ' Service Order(s) were submitted for Approval. You will be notified by email when complete.');
            
            return null;
        }
        catch(Exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }
    
    public PageReference saveOverride()
    {
    	try
    	{
			throwTestError();
			
			List<RCR_Contract__c> serviceOrders = new List<RCR_Contract__c>();
    		 
			for(ServiceOrderWrapper wrap : Rollovers)
            {
                if(wrap.ApprovedOnceOff)
                {
                	wrap.ServiceOrder.Rollover_Recommendation_Descision__c = APS_PicklistValues.RCRContract_RolloverRecommend_ApproveOnceOff;
                }
                else if(wrap.ApprovedRecurrent)
                {
                	wrap.ServiceOrder.Rollover_Recommendation_Descision__c = APS_PicklistValues.RCRContract_RolloverRecommend_ApproveRecurrent;
                }
                else if(wrap.ReduceServices)
                {
                	wrap.ServiceOrder.Rollover_Recommendation_Descision__c = APS_PicklistValues.RCRContract_RolloverRecommend_ReduceServices;
                }
                else if(wrap.UseAlternateProvider)
                {
                	wrap.ServiceOrder.Rollover_Recommendation_Descision__c = APS_PicklistValues.RCRContract_RolloverRecommend_IdentifyLowerCostProv;
                }
                serviceOrders.add(wrap.ServiceOrder);
            }
            update serviceOrders;
            
            return null;
    	}
    	catch(Exception ex)
    	{
    		return A2HCException.formatException(ex);
    	}
    }
    
    
    public PageReference approveRejectRollover()
    {
        try
        {
        	throwTestError();
        	
            List<RCR_Contract__c> approvedServiceOrders = new List<RCR_Contract__c>();
            List<RCR_Contract__c> rejectedServiceOrders = new List<RCR_Contract__c>();
            List<RCR_Contract__c> allServiceOrders = new List<RCR_Contract__c>();
            List<id> serviceOrderIDs = new List<id>();
//            List<IF_Personal_Budget__c> allocations = new List<IF_Personal_Budget__c>();
            
            Map<date, Financial_Year__c> financialYearsByEndDate = new Map<date, Financial_Year__c>();            
            for(Financial_Year__c fy : [SELECT 	Id, 
            									Start_Date__c, 
        										End_Date__c 
										FROM 	Financial_Year__c])
            {
                financialYearsByEndDate.put(fy.End_Date__c, fy);
            }
            
            date now = date.today();
                       
            integer noActionServiceOrders = 0;
            for(ServiceOrderWrapper wrap : Rollovers)
            {
                if(wrap.ApprovedOnceOff || wrap.ApprovedRecurrent)
                {
                    if(wrap.AllocationDiff != null && wrap.AllocationDiff < 0)
                    {
//                        IF_Personal_Budget__c allocation = new IF_Personal_Budget__c(Client__c = wrap.ServiceOrder.Client__c, RCR_Service_Order__c = wrap.ServiceOrder.ID);
//
//                        allocation.Financial_Year__c = financialYearsByEndDate.get(A2hCUtilities.getEndOfFinancialYear(wrap.ServiceOrder.End_Date__c)).Id;
//
//                        allocation.Amount__c = (wrap.AllocationDiff * -1);
//                        allocation.FYE_Amount__c = (wrap.AllocationDiff * -1);
//                        allocation.Allocation_Source__c = APS_PicklistValues.IFAllocation_Source_IndividualSupport;
//                        allocation.Approval_Criteria__c = APS_PicklistValues.IFAllocation_ApprovalCriteria_AutomaticApprove;
//                        allocation.Status__c = APS_PicklistValues.IFAllocation_Status_New;
//                        allocation.Reason__c = APS_PicklistValues.IFAllocation_Reason_Increase;
//                        allocation.Comment__c = 'Authorised increase as part of Bulk Rollover Approval.';
//
//                        if(wrap.ApprovedOnceOff)
//                        {
//                            allocation.Allocation_Type__c = APS_PicklistValues.IFAllocation_Type_OnceOff;
//                        }
//                        else if(wrap.ApprovedRecurrent)
//                        {
//                            allocation.Allocation_Type__c = APS_PicklistValues.IFAllocation_Type_Recurrent;
//                        }
//
//                        allocations.add(allocation);
                    }
                    wrap.ServiceOrder.Rollover_Status__c = APS_PicklistValues.RCRContract_RolloverStatus_BulkApproved;
                    wrap.ServiceOrder.Rollover_Rejection_Reason__c = null;
                    approvedServiceOrders.add(wrap.ServiceOrder);
                    serviceOrderIDs.add(wrap.ServiceOrder.Id);
                }
                else if(wrap.ReduceServices || wrap.UseAlternateProvider)
                {
                    wrap.ServiceOrder.Rollover_Status__c = APS_PicklistValues.RCRContract_RolloverStatus_BulkRejected;
                    wrap.ServiceOrder.Rollover_Rejection_Reason__c = (wrap.ReduceServices) ? APS_PicklistValues.RCRContract_RolloverRecommend_ReduceServices : APS_PicklistValues.RCRContract_RolloverRecommend_IdentifyLowerCostProv;
                    rejectedServiceOrders.add(wrap.ServiceOrder);
                    serviceOrderIDs.add(wrap.ServiceOrder.Id);
                }
                else
                {
                	// Checkboxes not selected.
                    noActionServiceOrders++;
                }
            }
//            upsert allocations;

            allServiceOrders.addAll(approvedServiceOrders);
            allServiceOrders.addAll(rejectedServiceOrders);
            upsert allServiceOrders;
            
            // Submit Approved Service Order Rollovers to the Approval Process: To be Automatically Approved.
            ServiceOrderFuture.submitRolloversForApproval(serviceOrderIDs);
            Set<ID> allocationsForApprovalIDs = new Set<ID>();
//            for(IF_Personal_Budget__c allocation : allocations)
//            {
//            	allocationsForApprovalIDs.add(allocation.ID);
//            }
            ServiceOrderFuture.submitForApproval(allocationsForApprovalIDs);
            
            if(rejectedServiceOrders.size() > 0)
            {
            	A2HCException.formatMessage(rejectedServiceOrders.size().format() + ' Service Order(s) were Rejected.');
            }
            if(noActionServiceOrders > 0)
            {
                A2HCException.formatMessage(noActionServiceOrders.format() + ' Service Order(s) were not actioned at this time.');
            }
            if(approvedServiceOrders.size() > 0)
            {
                A2HCException.formatMessage(approvedServiceOrders.size().format() + ' Service Order(s) were submitted for approval. You will be notified by email when complete.');
            }
            
            AccountsWithRollovers = null;
            doSearch();
            
            PageReference page = ApexPages.currentPage();
            page.getParameters().put('type', 'BulkRollover');
            AccountsWithRollovers = null;
            return null;
        }
        catch(Exception ex)
        {
            PageReference page = ApexPages.currentPage();
            page.getParameters().put('type', 'BulkRollover');
            return A2HCException.formatException(ex);
        }
    }
    
    
    public PageReference cancel()
    {
        try
        {
            throwTestError();
            
            return Page.RCR;
        }
        catch(Exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }
    
    @testVisible
    private void calculateTotals()
    {
        for(ServiceOrderWrapper soWrap : Rollovers) 
        {   
            Totals.TotalNewCost += soWrap.ServiceOrder.Total_Service_Order_Cost__c;
           	Totals.TotalOriginalCost += soWrap.ServiceOrder.Source_Record__r.Total_Service_Order_Cost__c == null ? 0.0: soWrap.ServiceOrder.Source_Record__r.Total_Service_Order_Cost__c;
        }
    }
    
    
    public class ServiceOrderWrapper
    {
        public ServiceOrderWrapper(RCR_Contract__c pServiceOrder)
        {
            ServiceOrder = pServiceOrder;
            ApprovedOnceOff = ServiceOrder.Rollover_Recommendation_Descision__c == APS_PicklistValues.RCRContract_RolloverRecommend_ApproveOnceOff ? true : false;
            ApprovedRecurrent = ServiceOrder.Rollover_Recommendation_Descision__c == APS_PicklistValues.RCRContract_RolloverRecommend_ApproveRecurrent ? true : false;
            ReduceServices = ServiceOrder.Rollover_Recommendation_Descision__c == APS_PicklistValues.RCRContract_RolloverRecommend_ReduceServices ? true : false;
            UseAlternateProvider = ServiceOrder.Rollover_Recommendation_Descision__c == APS_PicklistValues.RCRContract_RolloverRecommend_IdentifyLowerCostProv ? true : false;
            setTotalAllocation();
        }
        
        public boolean ApprovedOnceOff
        {
            get;
            set;
        }
        
        public boolean ApprovedRecurrent
        {
            get;
            set;
        }
        
        public boolean ReduceServices
        {
            get;
            set;
        }
        
        public boolean UseAlternateProvider
        {
            get;
            set;
        }
        
        public RCR_Contract__c ServiceOrder
        {
            get;
            set;
        }
        
        public string PercentageAllocationShortfall
        {
            get
            {
                string value = '';

                if(AllocationDiff == null || AllocationDiff >= 0)
                {
                	return value;
                }
                
                decimal percentage = (((AllocationDiff / ServiceOrder.Total_Service_Order_Cost__c) * 100)).setScale(2); 
                if(percentage < 0)
                {
                    percentage = percentage * -1;
                }
                
                value = percentage.format() + '%';
                
                return value;
            }
        }
        
        @testVisible
        public decimal TotalAllocation
        {
            get;
            private set;
        }
        
        public decimal AllocationDiff
        {
            get
            {
                return TotalAllocation - ServiceOrder.Total_Service_Order_Cost__c == 0.0 ? null : TotalAllocation - ServiceOrder.Total_Service_Order_Cost__c;
            }
        }
        
        private void setTotalAllocation()
        {
            TotalAllocation = 0.0;
//            for(IF_Personal_Budget__c pb : ServiceOrder.IF_Allocations__r)
//            {
//            	if(pb.Financial_Year__r.Name == A2HCUtilities.getFinancialYearDesc(ServiceOrder.End_Date__c))
//            	{
//                	TotalAllocation += pb.Amount__c;
//            	}
//            }
        }
    }
    
    
    public class TotalsWrapper
    {        
        public decimal TotalOriginalCost
        {
            get
            {
                if(TotalOriginalCost == null)
                {
                    TotalOriginalCost = 0.0;
                }
                return TotalOriginalCost;
            }
            private set;
        }
        
        public decimal TotalNewCost
        {
            get
            {
                if(TotalNewCost == null)
                {
                    TotalNewCost = 0.0;
                }
                return TotalNewCost;
            }
            private set;
        }
        
        public decimal TotalAllocation
        {
            get
            {
                if(TotalAllocation == null)
                {
                    TotalAllocation = 0.0;
                }
                return TotalAllocation;
            }
            private set;
        }
        
        public decimal TotalAllocationVariation
        {
            get
            {
                if(TotalAllocationVariation == null)
                {
                    TotalAllocationVariation = 0.0;
                }
                return TotalAllocationVariation;
            }
            private set;
        }
        
        public string TotalPercentageAllocationShortfall
        {
            get
            {
                // Avoid divide by 0 errors.
                if(TotalAllocation == 0)
                {
                    if(TotalNewCost == 0)
                    {
                        return '0%';
                    }
                    else if(TotalNewCost > 0)
                    {
                        return '100%';
                    }
                }
                
                string value = '';
                //decimal percentage = (100 - ((TotalAllocation / TotalNewCost) * 100)).setScale(2); 
                decimal percentage = (((TotalAllocationVariation / TotalNewCost) * 100)).setScale(2); 
                
                if(percentage < 0)
                {
                    percentage = percentage * -1;
                    value = percentage.format() + '%';
                }
                
                return value;
            }
        }
    }
}