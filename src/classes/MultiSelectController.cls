/**
 * Created by USer on 23/08/2016.
 */

public with sharing class MultiSelectController
{
    // SelectOption lists for public consumption
    public SelectOption[] LeftOptions { get; set; }
    public SelectOption[] RightOptions { get; set; }

    public List<String> getSelected()
    {
system.debug(RightOptions);
        List<String> opts = new List<String>();
        if(RightOptions != null) {
            for(SelectOption opt : RightOptions) {
                opts.add(opt.getValue());
            }
        }
        return opts;
    }

    // Parse &-separated values and labels from value and
    // put them in option
    private void setOptions(SelectOption[] options, String value)
    {
        options.clear();
        String[] parts = value.split('&');
        for(Integer i = 0; i < parts.size() / 2; i++) {
            options.add(new SelectOption(EncodingUtil.urlDecode(parts[i * 2], 'UTF-8'),
                    EncodingUtil.urlDecode(parts[(i * 2) + 1], 'UTF-8')));
        }
    }

    // Backing for hidden text field containing the options from the
    // left list
    public String LeftOptionsHidden
    {
        get;
        set
        {
            leftOptionsHidden = value;
            setOptions(leftOptions, value);
        }
    }

    // Backing for hidden text field containing the options from the
    // right list
    public String RightOptionsHidden
    {
        get;
        set
        {
            rightOptionsHidden = value;
            setOptions(rightOptions, value);
        }
    }
}