public without sharing class SharingRule 
{
	public enum AccessLevel
	{
		Read,
		Edit
	}
	
	public static void createServiceOrderSharing(sObject record, ID userID)
    {
        createServiceOrderSharing(record, AccessLevel.Edit, userID);
    }
	
    public static void createServiceOrderSharing(sObject record, AccessLevel access, ID userID)
    {
        if(!userCanEditRecord(record, access, userID))
        {
            insert new RCR_Contract__Share(AccessLevel = access.name(), ParentId = record.ID, RowCause = Schema.RCR_Contract__Share.RowCause.Manual, UserOrGroupId = userID);                
        }
    }
    
    public static Boolean userCanEditRecord(sObject record, AccessLevel access, ID userID)
    {
    	for(UserRecordAccess accessRecord : [SELECT  	RecordId,
														HasEditAccess,
														HasReadAccess
								                FROM    UserRecordAccess u
								                WHERE   UserId = :userID AND
								                        RecordId = :record.ID])
        {
	    	return access == AccessLevel.Edit ? accessRecord.HasEditAccess : accessRecord.HasReadAccess;
		}
    	return false;
    }
}