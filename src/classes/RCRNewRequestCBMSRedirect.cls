public with sharing class RCRNewRequestCBMSRedirect 
{
	public RCRNewRequestCBMSRedirect(ApexPages.StandardController cntlr)
	{
		
	}
	
	public PageReference redir()
    {
    	string recordTypeId = A2HCUtilities.getRecordType('RCR_Contract__c', APSRecordTypes.RCRServiceOrder_NewRequestCBMS).Id;
        return A2HCUtilities.redirectToStandardEdit(RCR_Contract__c.getsObjectType(), null, recordTypeId);
    }
}