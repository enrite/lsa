/**
 * An apex page controller that takes the user to the right start page based on credentials or lack thereof
 */
public with sharing class CommunitiesLandingController {
    
    // Code we will invoke on page load.
    public PageReference forwardToStartPage() 
    {    	
    	for(User u : [SELECT Grant_Disclaimer__c,
                            Profile.Name
    				FROM 	User 
    				WHERE 	ID = :UserInfo.getUserId()])
    	{
            if(u.Profile.Name == 'LSA SP Community')
            {
    		    return Page.SPPortalHome;
            }
            if(u.Profile.Name == 'LSA Managed Client Community')
            {
    		    return Page.MCPortalClients;
            }
            if(u.Profile.Name == 'LSA Grant Community')
            {
                if(u.Grant_Disclaimer__c == null)
                {
                    return Page.GPDisclaimer;
                }
                return Page.GPMyGrants;
            }
    	}
        return Network.communitiesLanding();
    }
    
    public CommunitiesLandingController() {}
}