/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestA2HCutilities {

    static testMethod void testRedirect()
    {     
        TestLoadData.loadRCRData();
        
        Test.StartTest();
       
        List<RCR_Contract__c> refList = [SELECT Id, Name FROM RCR_Contract__c LIMIT 20];
        ApexPages.StandardSetController setContr = new ApexPages.StandardSetController(refList);

        A2HCUtilities.redirectToListView(RCR_Contract__c.getsObjectType(), setContr, 'All');
        A2HCUtilities.redirectToListView(RCR_Contract__c.getsObjectType(), setContr, 'abcde');
        A2HCUtilities.redirectToStandardEdit(RCR_Contract__c.getsObjectType(), refList[0].Id, null); 
        
        Test.StopTest();
    }

    static testMethod void testgetAddChildRecordURL()
    {
        Account acc = new Account(Name = 'test');
        insert acc;

        String url = A2HCUtilities.getAddChildRecordURL(Contact.getsObjectType(), 'afield', acc);
        System.assert(url != null);
    }

    static testMethod void testRandomString()
    {
        Account acc = new Account(Name = 'test');
        insert acc;

        ApexPages.StandardController stdController = new ApexPages.StandardController(acc);
        A2HCUtilities a2 = new A2HCUtilities(stdController);
        System.assert(A2HCUtilities.getRandomString() != null);
    }

    static testMethod void testAll()
    {
        A2HCUtilities.isTrimmedStringNullOrEmpty(null);
        A2HCUtilities.isTrimmedStringNullOrEmpty('test');

        A2HCUtilities.escapeSearchString(null);
        A2HCUtilities.escapeSearchString('test');

        A2HCUtilities.tryParseDate(null);
        A2HCUtilities.tryParseDate('01/01/2010');

        A2HCUtilities.tryParseDecimal(null);
        A2HCUtilities.tryParseDecimal('10.2');

        A2HCUtilities.tryParseInteger(null);
        A2HCUtilities.tryParseInteger('10');

        A2HCUtilities.DecimalToCurrency(0.0, true);
        A2HCUtilities.DecimalToCurrency(10.34, false);
        A2HCUtilities.DecimalToCurrency(10.34, true);

        A2HCUtilities.parseHTMLRequestDate('Wed Jan 28 00:00:00 GMT 2011');
        A2HCUtilities.parseHTMLRequestDate('Wed Feb 28 00:00:00 GMT 2011');
        A2HCUtilities.parseHTMLRequestDate('Wed Mar 28 00:00:00 GMT 2011');
        A2HCUtilities.parseHTMLRequestDate('Wed Apr 28 00:00:00 GMT 2011');
        A2HCUtilities.parseHTMLRequestDate('Wed May 28 00:00:00 GMT 2011');
        A2HCUtilities.parseHTMLRequestDate('Wed Jun 28 00:00:00 GMT 2011');
        A2HCUtilities.parseHTMLRequestDate('Wed Jul 28 00:00:00 GMT 2011');
        A2HCUtilities.parseHTMLRequestDate('Wed Aug 28 00:00:00 GMT 2011');
        A2HCUtilities.parseHTMLRequestDate('Wed Sep 28 00:00:00 GMT 2011');
        A2HCUtilities.parseHTMLRequestDate('Wed Oct 28 00:00:00 GMT 2011');
        A2HCUtilities.parseHTMLRequestDate('Wed Nov 28 00:00:00 GMT 2011');
        A2HCUtilities.parseHTMLRequestDate('Wed Dec 28 00:00:00 GMT 2011');

        A2HCUtilities.parseHTMLRequestDateTime('Wed Jan 28 00:00:00 GMT 2011');
        A2HCUtilities.parseHTMLRequestDateTime('Wed Feb 28 00:00:00 GMT 2011');
        A2HCUtilities.parseHTMLRequestDateTime('Wed Mar 28 00:00:00 GMT 2011');
        A2HCUtilities.parseHTMLRequestDateTime('Wed Apr 28 00:00:00 GMT 2011');
        A2HCUtilities.parseHTMLRequestDateTime('Wed May 28 00:00:00 GMT 2011');
        A2HCUtilities.parseHTMLRequestDateTime('Wed Jun 28 00:00:00 GMT 2011');
        A2HCUtilities.parseHTMLRequestDateTime('Wed Jul 28 00:00:00 GMT 2011');
        A2HCUtilities.parseHTMLRequestDateTime('Wed Aug 28 00:00:00 GMT 2011');
        A2HCUtilities.parseHTMLRequestDateTime('Wed Sep 28 00:00:00 GMT 2011');
        A2HCUtilities.parseHTMLRequestDateTime('Wed Oct 28 00:00:00 GMT 2011');
        A2HCUtilities.parseHTMLRequestDateTime('Wed Nov 28 00:00:00 GMT 2011');
        A2HCUtilities.parseHTMLRequestDateTime('Wed Dec 28 00:00:00 GMT 2011');

        A2HCUtilities.concatStrings(null, '1');
        A2HCUtilities.concatStrings('1', null);
        A2HCUtilities.concatStrings('2', '1');

        A2HCUtilities.toInitCase(null);
        A2HCUtilities.toInitCase('');
        A2HCUtilities.toInitCase('dcvxvxcv');

        A2HCUtilities.getEndOfFinancialYear(date.parse('1/1/2012'));
        A2HCUtilities.getEndOfFinancialYear(date.parse('7/7/2012'));
        A2HCUtilities.getEndOfFinancialYear(null);

        A2HCUtilities.dateRangesOverlap(date.today(), date.today().adddays(10), date.today().adddays(3), date.today().adddays(12));
        A2HCUtilities.dateRangesOverlap(null, null, null, null);

        A2HCUtilities.getDayOfWeek(date.today());

        List<Date> d = new List<Date>(new date[]{date.today(), date.today().adddays(1), date.today().adddays(2), date.today().adddays(3)});
        A2HCUtilities.getEarliestDate(d);
        A2HCUtilities.getEarliestDate(null);

        List<Contact> contacts = [SELECT    ID,
                                            Name
                                FROM        Contact
                                LIMIT 20];

        A2HCUtilities.sortList(contacts, 'Name', 'ASC');

        A2HCUtilities.isDateInDaysOfWeek(date.today(), 'Monday');
        A2HCUtilities.isDateInDaysOfWeek(null, null);

        A2HCUtilities.getDayOfWeek(dateTime.now());

        A2HCUtilities.handleEmptyString(null);
        A2HCUtilities.handleEmptyString('sdfsdf');

        List<Date> dts = new List<Date>();
        dts.add(Date.today());
        A2HCUtilities.getLatestDate(dts);
        
        try
        {
            A2HCUtilities.getRecordType('Account', 'abc');
        }
        catch(Exception ex){}

        A2HCUtilities.isDateBetween(Date.today(), Date.today().addDays(-1), Date.today().addDays(1));
        A2HCUtilities.isDateBetween(Date.today(), Date.today().addDays(1), Date.today().addDays(2));

        Set<Date> dtSet = new Set<Date>();
        for(Integer i = 0; i < 10; i++)
        {
            dtSet.add(Date.today().addDays(i));
        }
        A2HCUtilities.dateListIncludesDateWithinPeriod(dtSet, Date.today().addDays(-1), Date.today().addDays(2));

        //A2HCUtilities.getAnyUserForAccount([SELECT AccountID FROM Contact WHERE AccountID != null LIMIT 1].ID);
        //A2HCUtilities.getAnyPortalUserForAccount([SELECT ID FROM Account WHERE IsCustomerPortal = true LIMIT 1].ID);
        
        try
        {
            A2HCUtilities.getRecordTypes('fail');
        }
        catch(Exception ex){}
        try
        {
            A2HCUtilities.getRecordType('Contact', 'A2HC Client');
        }
        catch(Exception ex){}
        
        A2HCUtilities.getFinancialYearDesc(Date.today());
        A2HCUtilities.getStartOfFinancialYear(Date.today());
        A2HCUtilities.getFinancialYears(Date.today(), Date.today());
        A2HCUtilities.getSFFiscalYears(Date.today(), Date.today()); 
        
        A2HCUtilities.isSandbox();
    }
    
    static testMethod void testCopy()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
        
        RCR_Program__c c1 = [SELECT Id,
                                    Name,
                                    OwnerId
                             FROM RCR_Program__c
                             LIMIT 1];

        RCR_Program__c c2 = [SELECT Id,
                                    Name,
                                    OwnerId
                             FROM RCR_Program__c
                             WHERE Id != :c1.Id
                             LIMIT 1];                                                            
        
        A2HCUtilities.copyAllFields(c1, c2);
        
        Test.StopTest();
    }
}