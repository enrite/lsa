public class LPMemberActivationResponseJSON
{
    public String id { get; set; }
    public Double created { get; set; }
    public Data data { get; set; }
    public String type_Z { get; set; } // in json: type

//    public LPMemberActivationResponseJSON(JSONParser parser) {
//        while(parser.nextToken() != System.JSONToken.END_OBJECT) {
//            if(parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
//                String text = parser.getText();
//                if(parser.nextToken() != System.JSONToken.VALUE_NULL) {
//                    if(text == 'id') {
//                        id = parser.getText();
//                    } else if(text == 'created') {
//                        created = parser.getDoubleValue();
//                    } else if(text == 'data') {
//                        data = new Data(parser);
//                    } else if(text == 'type') {
//                        type_Z = parser.getText();
//                    } else {
//                        System.debug(LoggingLevel.WARN, 'LPMemberActivationJSON consuming unrecognized property: ' + text);
//                        consumeObject(parser);
//                    }
//                }
//            }
//        }
//    }

    public class Data {
        public String membershipNumber { get; set; }
        public String programCode { get; set; }
        public String memberUri { get; set; }

//        public Data(JSONParser parser) {
//            while(parser.nextToken() != System.JSONToken.END_OBJECT) {
//                if(parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
//                    String text = parser.getText();
//                    if(parser.nextToken() != System.JSONToken.VALUE_NULL) {
//                        if(text == 'membershipNumber') {
//                            membershipNumber = parser.getText();
//                        } else if(text == 'programCode') {
//                            programCode = parser.getText();
//                        } else if(text == 'memberUri') {
//                            memberUri = parser.getText();
//                        } else {
//                            System.debug(LoggingLevel.WARN, 'Data consuming unrecognized property: ' + text);
//                            consumeObject(parser);
//                        }
//                    }
//                }
//            }
//        }
    }

    public static LPMemberActivationResponseJSON parse(String jsonString)
    {
        system.debug(jsonString);
        jsonString = jsonString.replace('"type":', '"type_Z":');
        return (LPMemberActivationResponseJSON)JSON.deserialize(jsonString, LPMemberActivationResponseJSON.class);
//        System.JSONParser parser = System.JSON.createParser(jsonString);
//        return new LPMemberActivationResponseJSON(parser);
    }

//    public static void consumeObject(System.JSONParser parser) {
//        Integer depth = 0;
//        do {
//            System.JSONToken curr = parser.getCurrentToken();
//            if(curr == System.JSONToken.START_OBJECT ||
//                    curr == System.JSONToken.START_ARRAY) {
//                depth++;
//            } else if(curr == System.JSONToken.END_OBJECT ||
//                    curr == System.JSONToken.END_ARRAY) {
//                depth--;
//            }
//        } while(depth > 0 && parser.nextToken() != null);
//    }
}