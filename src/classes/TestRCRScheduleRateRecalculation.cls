@isTest//(seeAllData=true)
private class TestRCRScheduleRateRecalculation 
{
    static testMethod void myUnitTest() 
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        Account acct = [SELECT Id FROM Account LIMIT 1];
        RCR_Contract__c serviceOrder = [SELECT  Id,
                                                Total_Service_Order_Cost__c,
                                                (SELECT Id,
                                                        RCR_Service__c,
                                                        Total_Cost__c
                                                 FROM   RCR_Contract_Scheduled_Services__r)
                                        FROM    RCR_Contract__c
                                        //WHERE   Name = 'TestSO'
                                        LIMIT 1];
        RCR_Service_Rate__c rate = [SELECT  Id
                                    FROM    RCR_Service_Rate__c
                                    WHERE   RCR_Service__c = :serviceOrder.RCR_Contract_Scheduled_Services__r[0].RCR_Service__c
                                    ORDER BY Start_Date__c desc
                                    LIMIT 1];                                       
        
        RCR_Service_Rate_Variation__c rateVariation = new RCR_Service_Rate_Variation__c();
        rateVariation.Account__c = acct.Id;
        insert rateVariation;
        
        RCR_Service_Rate_Audit__c rateAudit = new RCR_Service_Rate_Audit__c();
        rateAudit.RCR_Service_Rate_Variation__c = rateVariation.id;
        rateAudit.RCR_Contract_Scheduled_Service__c = serviceOrder.RCR_Contract_Scheduled_Services__r[0].Id;
        rateAudit.RCR_Service_Order__c = serviceOrder.Id;
        rateAudit.RCR_Service_Rate__c = rate.Id;
        rateAudit.Recalculation_ID__c = 10000000;
        rateAudit.Scheduled_Service_Value_Before__c = serviceOrder.RCR_Contract_Scheduled_Services__r[0].Total_Cost__c;
        rateAudit.Service_Order_Value_Before__c = serviceOrder.Total_Service_Order_Cost__c;
        rateAudit.Proposed_Rate_Activation__c = true;
        insert rateAudit;
        
        RCRScheduleRateRecalculation rateRecalcSched = new RCRScheduleRateRecalculation();
        rateRecalcSched.run('s'); 
        
        Test.StopTest();
    }
}