public with sharing class RCRAdhocServiceExtension extends A2HCPageBase
{
    public RCRAdhocServiceExtension(ApexPages.StandardController controller)
    {
        String adhocServiceID = getParameter('id');
        if(adhocServiceID != null)
        {
            AdhocService = [SELECT  r.Id,
                                    r.Start_Date__c,                                    
                                    r.RCR_Sub_Program__c,                                    
                                    r.Name, 
                                    r.End_Date__c,
                                    r.Original_End_Date__c ,
                                    r.Cancellation_Date__c,
                                    r.Amount__c,
                                    r.Comment__c,
                                    r.Internal_Comments__c,
                                    r.Qty__c,
                                    r.Rate__c,
                                    r.Pre_Cancellation_Amount__c,
                                    r.Expenses__c,
                                    r.Funding_Commitment__c,
                                    r.RCR_Contract__c,
                                    r.RCR_Contract__r.Name,
                                    r.RCR_Contract__r.RecordType.Name,
                                    r.RCR_Account_Program_Category__c,
                                    r.RCR_Account_Program_Category__r.Account__c
                            FROM    RCR_Contract_Adhoc_Service__c r
                            WHERE   r.Id = :adhocServiceID]; 

            RCRContract = RCRContractExtension.getContract(AdhocService.RCR_Contract__c);
        }
        else
        {
            String contractID = getParentIDFromURL();
            if(contractID != null)
            {
                RCRContract = RCRContractExtension.getContract(contractID);
                AdhocService = new RCR_Contract_Adhoc_Service__c(RCR_Contract__c = RCRContract.ID,
                                                                 Start_Date__c = RCRContract.Start_Date__c,
                                                                 Original_End_Date__c = RCRContract.End_Date__c);
            }
        }
    }

    //***********************************************************************************
    //*   Properties and methods to allow us to call the Component Controller functions.
    //***********************************************************************************
    public RCRContractServiceTypeController ServiceTypesController
    {
        get
        {
            if(ServiceTypesController == null)
            {
                ServiceTypesController = new RCRContractServiceTypeController(); 
                ServiceTypesController.ParentObject = AdhocService;
                ServiceTypesController.RCRContract = RCRContract;               
            }
            return ServiceTypesController;
        }
        set;
    }

    public RCR_Contract_Adhoc_Service__c AdhocService
    {
        get;
        set;
    }

    public RCR_Contract__c RCRContract
    {
        get;
        set;
    }
    
    public Boolean ReadOnly
    {
        get
        {
            return AdhocService.Cancellation_Date__c != null && AdhocService.Cancellation_Date__c <= Date.today();
        }
    }

    public List<SelectOption> SubPrograms
    {
        get
        {
            if(SubPrograms == null)
            {
                SubPrograms = new List<SelectOption>();
                SubPrograms.add(new SelectOption('', '--None--'));
                for(RCR_Sub_Program__c subProgram : [SELECT     r.Id,
                                                                r.Name
                                                       FROM     RCR_Sub_Program__c r
                                                       ORDER BY r.Name])
                {
                    SubPrograms.add(new SelectOption(subProgram.Id, subProgram.Name));
                }
            }
            return SubPrograms;
        }
        private set;
    }
    
    public PageReference saveOverride()
    {
        SavePoint sp = Database.setSavepoint();
        boolean newRecord = AdhocService.Id == null;
        try
        {
        	throwTestError();
            if(!isValid())
            {
                return null;
            }
            upsert AdhocService;

            if(!ReadOnly && ServiceTypesController != null)
            {
                ServiceTypesController.ParentObject = AdhocService;
                ServiceTypesController.saveContractServiceTypes();
                AdhocService.Service_Types__c = ServiceTypesController.ServiceTypeString;
                update AdhocService;
            }
            
//            if(RCRContract.RecordType.Name == APSRecordTypes.RCRServiceOrder_ServiceOrderCBMS)
//            {
//                String errMsg = ServiceOrderFuture.checkServiceOrderAllocation(RCRContract);
//                if(errMsg != null)
//                {
//                    throw new A2HCException(errMsg);
//                }
//            }               
            
            return cancelOverride();
        }
        catch (Exception ex)
        {
            if(newRecord)
            {
                if(ServiceTypesController != null)
                {
                    ServiceTypesController.cloneContractServiceTypes();
                }
                AdhocService = AdhocService.clone(false, true);
            }
            return A2HCException.formatExceptionAndRollback(sp, ex);
        }
    }

    private Boolean isValid()
    {       
    	Boolean valid = isRequiredFieldValid(AdhocService.Funding_Commitment__c, 'Funding Commitment', true);
    	valid = isNumericFieldValid(AdhocService.Amount__c, 'Amount',  0, null, valid);
    	valid = isRequiredFieldValid(AdhocService.Start_Date__c, 'Start Date', valid);
    	valid = isRequiredFieldValid(AdhocService.Original_End_Date__c, 'End Date', valid);
        for(RCR_Contract_Adhoc_Service__c service : [SELECT r.Id,
                                                            r.Name,
                                                            r.Start_Date__c,
                                                            r.End_Date__c
                                                     FROM   RCR_Contract_Adhoc_Service__c r
                                                     WHERE  r.Id != :AdhocService.Id
                                                     AND    r.RCR_Contract__c = :RCRContract.ID])
        {       	
            if(service.Start_Date__c < service.End_Date__c && // end date may be earlier than start date if contract cancelled
                    A2HCUtilities.dateRangesOverlap(service.Start_Date__c, service.End_Date__c, AdhocService.Start_Date__c, AdhocService.Original_End_Date__c))
            {
                valid = A2HCException.formatValidationException('This service date range overlaps an existing adhoc service for this contract.');
            }
        }
        if(ServiceTypesController != null && ServiceTypesController.ContractServiceTypes.size() < 1)
        {
            valid = A2HCException.formatValidationException('An adhoc service must have at least one service type listed.');
        }
        return valid;
    }

    public PageReference cancelOverride()
    {
        try
        {
        	throwTestError();
            return getReturnPage(AdhocService.ID, AdhocService.RCR_Contract__c).setRedirect(true);
        }
        catch (Exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }
}