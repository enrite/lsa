public with sharing class ExternalExpenseEditExtension
{
    public ExternalExpenseEditExtension(ApexPages.StandardController c)
    {
        record = (External_Expense__c)c.getRecord();

        FindGLCodes();
        FindLineItems();

        if (record.Id == null)
        {
            // default owner to current user
            record.OwnerId = UserInfo.getUserId();

            // default year to current year
            record.Year__c = String.valueOf(Date.today().year());

            // add 10 line items
            AddLineItems();
        }
    }

    private final Integer PARTICIPANT_INDEX = 0;
    private final Integer EXPENSE_INDEX = 1;
    private final Integer GLCODE_INDEX = 2;
    private final Integer ACTIVITYDATE_INDEX = 3;
    private final Integer PAYMENTDATE_INDEX = 4;
    private final Integer AMOUNT_INDEX = 5;
    private final Integer GST_INDEX = 6;
    private final Integer TOTAL_INDEX = 7;
    private final Integer PLANACTION_INDEX = 8;
    private final Integer SOURCE_INDEX = 9;
    private final Integer FIELD_COUNT = SOURCE_INDEX + 1;

    private External_Expense__c record;
    public List<SelectOption> GLCodes { get; set; }
    public List<String> Keys  { get; set; }
    public Map<String, External_Expense_Line_Item__c> LineItems { get; set; }
    public Map<String, String> Errors { get; set; }
    public String ItemKey { get; set; }

    public Attachment ExpenseFile
    {
        get
        {
            if(ExpenseFile == null)
            {
                ExpenseFile = new Attachment();
            }
            return ExpenseFile;
        }
        set;
    }

    public List<SelectOption> Years
    {
        get
        {
            if (Years == null)
            {
                Years = new List<SelectOption>();

                for (Integer d = 2015; d <= (Date.today().year() + 1); d++)
                {
                    Years.add(new SelectOption(String.valueOf(d), String.valueOf(d)));
                }
            }
            return Years;
        }
        set;
    }

    public void FindGLCodes()
    {
        try
        {
            GLCodes = new List<SelectOption>();
            GLCodes.add(new SelectOption('', '--None--'));
            for(RCR_Program_Category_Service_Type__c pcst : getProgramCategoryServiceTypes())
            {
                GLCodes.add(new SelectOption(pcst.ID, pcst.Name));
            }
        }
        catch(Exception e)
        {
            A2HCException.formatException(e);
        }
    }

    private void FindLineItems()
    {
        try
        {
            Keys = new List<String>();
            LineItems = new Map<String, External_Expense_Line_Item__c>();
            Errors = new Map<String, String>();

            for (External_Expense_Line_Item__c i : [SELECT Id,
                                                           External_Expense__c,
                                                           Participant__c,
                                                           Date_of_Transaction__c,
                                                            Date_of_Payment__c,
                                                           Supplier__c,
                                                           Expense_Description__c,
                                                           GL_Code__c,
                                                           Amount__c,
                                                           GST__c,
                                                            Plan_Action__c
                                                    FROM External_Expense_Line_Item__c
                                                    WHERE External_Expense__c = :record.Id
                                                    ORDER BY CreatedDate])
            {
                String key = A2HCUtilities.GenerateKey(15);

                Keys.add(key);
                LineItems.put(key, i);
                Errors.put(key, '');
            }
        }
        catch(Exception e)
        {
            A2HCException.formatException(e);
        }
    }

    public void AddLineItems()
    {
        try
        {
            for (Integer i = 0; i < 10; i++)
            {
                String key = A2HCUtilities.GenerateKey(15);

                Keys.add(key);
                LineItems.put(key, new External_Expense_Line_Item__c());
                Errors.put(key, '');
            }
        }
        catch(Exception e)
        {
            A2HCException.formatException(e);
        }
    }

    public void RemoveLineItem()
    {
        try
        {
            for (Integer i = 0; i < Keys.size(); i++)
            {
                if (Keys[i] == ItemKey)
                {
                    Keys.remove(i);
                    break;
                }
            }

            LineItems.remove(ItemKey);
        }
        catch(Exception e)
        {
            A2HCException.formatException(e);
        }
    }

    public External_Expense_Line_Item__c Totals
    {
        get
        {
            Totals = new External_Expense_Line_Item__c(Amount__c = 0, GST__c = 0);

            for (External_Expense_Line_Item__c i : LineItems.values())
            {
                Totals.Amount__c += (i.Amount__c != null ? i.Amount__c : 0);
                Totals.GST__c += (i.GST__c != null ? i.GST__c : 0);
            }
            return Totals;
        }
        set;
    }

    public PageReference SaveOverride()
    {
        try
        {
            Boolean error = false;

            // validate the record
            if (record.OwnerId == null) { record.OwnerId.addError('You must enter a value'); error = true; }
            if (record.Month__c == null) { record.Month__c.addError('You must enter a value'); error = true; }
            if (record.Year__c == null) { record.Year__c.addError('You must enter a value'); error = true; }
            if (record.Account__c == null) { record.Account__c.addError('You must enter a value'); error = true; }

            // validate the line items
            List<External_Expense_Line_Item__c> newLineItems = new List<External_Expense_Line_Item__c>();
            for (External_Expense_Line_Item__c i : LineItems.values())
            {
                if (i.Participant__c != null
                    || i.Date_of_Transaction__c != null
                    || i.Supplier__c != null
                    || i.Expense_Description__c != null
                    || i.GL_Code__c != null
                    || i.Amount__c != null
                    || i.GST__c != null)
                {
                    if (i.Date_of_Transaction__c == null)
                    {
                        i.Date_of_Transaction__c.addError('You must enter a value');
                        error = true;
                    }
                    else
                    {
                        if (record.Month__c != Datetime.newInstance(2015, i.Date_of_Transaction__c.month(), 1, 18, 0, 0).format('MMMM')
                            || record.Year__c != String.valueOf(i.Date_of_Transaction__c.year()))
                        {
                            i.Date_of_Transaction__c.addError('This must match the parent expense');
                            error = true;
                        }
                    }
                    if (i.Expense_Description__c == null) { i.Expense_Description__c.addError('You must enter a value'); error = true; }
                    if (i.Participant__c != null && i.GL_Code__c == null) { i.GL_Code__c.addError('You must enter a value'); error = true; }
                    if (i.Amount__c == null) { i.Amount__c.addError('You must enter a value'); error = true; }
                    if (i.GST__c == null) { i.GST__c.addError('You must enter a value'); error = true; }

                    newLineItems.add(i);
                }
            }

            if (error)
            {
                return null;
            }

            upsert record;

            // ensure the parent Id is set on the new line items
            for (External_Expense_Line_Item__c i : newLineItems)
            {
                if (i.External_Expense__c == null)
                {
                    i.External_Expense__c = record.Id;
                }
            }
            upsert newLineItems;

            return new PageReference('/' + record.Id);
        }
        catch(Exception e)
        {
            return A2HCException.formatException(e);
        }
    }

    public PageReference uploadFile()
    {
        Savepoint sp = Database.setSavePoint();
        try
        {
            if(ExpenseFile.Name == null)
            {
                throw new A2HCException('Expense file is required.');
            }
            List<String> errMessages = new List<String>();
            CsvParser parser = new CsvParser(ExpenseFile.Body.toString(), false, FIELD_COUNT);
            // read first line of file
            String[] fields = parser.ReadLine();
            if(fields == null)
            {
                 throw new A2HCException('Expense file could not be read.');
            }
            if(fields.size() < FIELD_COUNT)
            {
                 throw new A2HCException('Expense file is not in the expected format.');
            }
            External_Expense__c extExpense = null;
            List<External_Expense_Line_Item__c> items = new List<External_Expense_Line_Item__c>();
            Map<String, RCR_Program_Category_Service_Type__c> progServiceTypesByGLCode = new Map<String, RCR_Program_Category_Service_Type__c>();
            Map<String, RCR_Program_Category_Service_Type__c> progServiceTypesByDescription = new Map<String, RCR_Program_Category_Service_Type__c>();
            Map<String, Referred_Client__c> clientsByName = new Map<String, Referred_Client__c>();
            for(Referred_Client__c cl : [SELECT Id,
                                                Name
                                        FROM    Referred_Client__c
                                        WHERE   Status__c NOT IN ('Not Accepted', 'Closed')])
            {
                clientsByName.put(cl.Name.toUpperCase(), cl);
            }
            String accountName;
            Account sourceAccount = null;
            do
            {
                if(Limits.getCpuTime() > Limits.getLimitCpuTime() - 500)
                {
                    throw new A2HCException('File has too many rows to process. This error was encountered on the ' + String.valueOf(items.size()) + 'th row. ');
                }
                fields = parser.ReadLine();
                if(fields == null || fields.isEmpty() || fields.size() < FIELD_COUNT)
                {
                    break;
                }
                if(extExpense == null)
                {
                    extExpense = getExpense(fields, sourceAccount, progServiceTypesByGLCode, progServiceTypesByDescription);
                    accountName = fields[SOURCE_INDEX];
                }
                if(accountName != fields[SOURCE_INDEX])
                {
                    throw new A2HCException('File contains multiple expenses sources.');
                }

                External_Expense_Line_Item__c item = new External_Expense_Line_Item__c(External_Expense__c = extExpense.ID);
                String participantName = (String.isBlank(fields[PARTICIPANT_INDEX]) ? '' : fields[PARTICIPANT_INDEX]).trim();
                Referred_Client__c client = clientsByName.get(participantName.toUpperCase());
                if(client == null)
                {
                    String s = String.isBlank(fields[PARTICIPANT_INDEX]) ? 'Participant is required' : ('Invalid participant (' + participantName + ')');
                    addErrorMessage(errMessages, s, items.size());
                }
                else
                {
                    item.Participant__c = client.ID;
                }

                String expenseDescription = fields[EXPENSE_INDEX];
                item.Expense_Description__c = expenseDescription;

                String glCode = (String.isBlank(fields[GLCODE_INDEX]) ? '' : fields[GLCODE_INDEX]).trim();
                RCR_Program_Category_Service_Type__c pcst = null;
                if(String.isNotBlank(glCode))
                {
                    // first try to match on description
                    pcst = progServiceTypesByDescription.get(glCode.toUpperCase());
                    if(pcst == null)
                    {
                        // now try to match on code
                        pcst = progServiceTypesByGLCode.get(glCode.toUpperCase());
                    }
                    if(pcst == null)
                    {
                        item.Invalid_GL_Code__c = glCode;
                    }
                    else
                    {
                        item.GL_Code__c = pcst.ID;
                    }
                }

                Date dt = A2HCUtilities.tryParseDate(fields[ACTIVITYDATE_INDEX]);
                if(dt == null)
                {
                    String s = String.isBlank(fields[ACTIVITYDATE_INDEX]) ? 'Activity Date is required' : ('Invalid Activity Date (' + fields[ACTIVITYDATE_INDEX] + ')');
                    addErrorMessage(errMessages, s, items.size());
                }
                item.Date_of_Transaction__c = dt;

                dt = A2HCUtilities.tryParseDate(fields[PAYMENTDATE_INDEX]);
                if(dt == null)
                {
                    String s = String.isBlank(fields[PAYMENTDATE_INDEX]) ? 'Payment Date is required' : ('Invalid Payment Date (' + fields[PAYMENTDATE_INDEX] + ')');
                    addErrorMessage(errMessages, s, items.size());
                }
                item.Date_of_Payment__c = dt;

                item.Amount__c = A2HCUtilities.tryParseDecimal(fields[AMOUNT_INDEX]);
                if(item.Amount__c == null)
                {
                    String s = String.isBlank(fields[AMOUNT_INDEX]) ? 'Amount is required' : ('Invalid amount (' + fields[AMOUNT_INDEX] + ')');
                    addErrorMessage(errMessages, s, items.size());
                }

                item.GST__c = A2HCUtilities.tryParseDecimal(fields[GST_INDEX]);
                if(item.GST__c == null)
                {
                    String s = String.isBlank(fields[GST_INDEX]) ? 'GST amount is required' : ('Invalid GST amount (' + fields[GST_INDEX] + ')');
                    addErrorMessage(errMessages, s, items.size());
                }

                item.PX_HX_Number__c = fields[PLANACTION_INDEX];
                items.add(item);
            }
            while(Limits.getCpuTime() < Limits.getLimitCpuTime());

            if(extExpense == null || items.isEmpty())
            {
                throw new A2HCException('Expense file does not contain enough information.');
            }
            if(errMessages.size() > 0)
            {
                throw new A2HCException(String.join(errMessages, '<br/>'));
            }
            Set<String> pxNumbers = new Set<String>();
            Map<String, Plan_Action__c> planActionsByName = new Map<String, Plan_Action__c>();
            for(External_Expense_Line_Item__c item : items)
            {
                if(String.isNotBlank(item.PX_HX_Number__c) && item.PX_HX_Number__c.startsWith('PX'))
                {
                    pxNumbers.add(item.PX_HX_Number__c);
                }
            }
            for(Plan_Action__c pa : [SELECT ID,
                                            Name,
                                            Client__c
                                    FROM    Plan_Action__c
                                    WHERE   Name IN :pxNumbers])
            {
                planActionsByName.put(pa.Name, pa);
            }
            for(External_Expense_Line_Item__c item : items)
            {
                if(String.isNotBlank(item.PX_HX_Number__c) &&
                        item.PX_HX_Number__c.startsWith('PX') &&
                        planActionsByName.containsKey(item.PX_HX_Number__c))
                {
                    Plan_Action__c planAction = planActionsByName.get(item.PX_HX_Number__c);
                    if(planAction.Client__c == item.Participant__c)
                    {
                        item.Plan_Action__c = planAction.ID;
                    }
                }
            }
            insert items;

            PageReference pg = new PageReference('/' + extExpense.ID);
            return pg;
        }
        catch(Exception ex)
        {
            ExpenseFile = null;
            return A2HCException.formatExceptionAndRollback(sp, ex);
        }
    }

    private void sendGLCodeNotifications(External_Expense__c extExpense)
    {
        List<Task> tasks = new List<Task>();
        List<User> groupUsers =  [SELECT ID
                                FROM    User
                                WHERE   User.IsActive = true AND
                                        ID IN (SELECT UserOrGroupId
                                                FROM    GroupMember
                                                WHERE   Group.DeveloperName = 'Finance')];
        Task baseTask = new Task();
        baseTask.Status = 'Not Started';
        baseTask.Subject = 'Invalid GL code for external expense item';
        baseTask.Priority = 'Normal';
        baseTask.ActivityDate = Date.today();
        for(User u : groupUsers)
        {
            Task t = baseTask.clone(false, true);
            t.WhatId = extExpense.ID;
            t.OwnerId = u.ID;
            tasks.add(t);
        }
        insert tasks;
    }

    private void addErrorMessage(List<String> errMessages, String message, Integer lineNum)
    {
        errMessages.add('Line ' + String.valueOf(lineNum + 1) + ': ' + message);
    }

    private External_Expense__c getExpense(String[] fields, Account sourceAccount,
            Map<String, RCR_Program_Category_Service_Type__c> progServiceTypesByGLCode,
            Map<String, RCR_Program_Category_Service_Type__c> progServiceTypesByDescription)
    {
        if(fields.size() < SOURCE_INDEX + 1 || String.isBlank(fields[SOURCE_INDEX]))
        {
            throw new A2HCException('Expense source could not be found.');
        }
//        External_Expense__c extExpense = new External_Expense__c(File_Upload__c = true);
        External_Expense__c extExpense = record;
        extExpense.File_Upload__c = true;
        for(Account a : [SELECT ID,
                                Finance_Code__c
                        FROM    Account
                        WHERE   Name = :fields[SOURCE_INDEX]])
        {
            sourceAccount = a;
            extExpense.Account__c = a.Id;
        }
        if(sourceAccount == null)
        {
            throw new A2HCException('Expense source (' + fields[SOURCE_INDEX] + ') is not a valid account.');
        }
        for(RCR_Program_Category_Service_Type__c pcst : getProgramCategoryServiceTypes())
        {
            if(sourceAccount.Finance_Code__c == 'GG' && pcst.Finance_Code_GG__c != null)
            {
                progServiceTypesByGLCode.put(pcst.Finance_Code_GG__c.toUpperCase(), pcst);
                progServiceTypesByDescription.put(pcst.Name.toUpperCase(), pcst);

            }
            else if(sourceAccount.Finance_Code__c == 'NSAG' && pcst.Finance_Code_NSAG__c != null)
            {
                progServiceTypesByGLCode.put(pcst.Finance_Code_NSAG__c.toUpperCase(), pcst);
                progServiceTypesByDescription.put(pcst.Name.toUpperCase(), pcst);
            }
        }
        Date firstExpenseDate = A2HCUtilities.tryParseDate(fields[ACTIVITYDATE_INDEX]);
        if(firstExpenseDate == null)
        {
            throw new A2HCException('Expense date (' + fields[ACTIVITYDATE_INDEX] + ') could not be parsed.');
        }
        //extExpense.Month__c = DateTime.newInstance(firstExpenseDate, Time.newInstance(0, 0, 0, 0)).format('MMMMM');
        //extExpense.Year__c = String.valueOf(firstExpenseDate.year());
        insert extExpense;

        ExpenseFile.ParentID = extExpense.Id;
        ExpenseFile.ContentType = 'text/csv';
        insert ExpenseFile;

        return extExpense;
    }

    private List<RCR_Program_Category_Service_Type__c> getProgramCategoryServiceTypes()
    {
        List<RCR_Program_Category_Service_Type__c> pcstList = new List<RCR_Program_Category_Service_Type__c>();
        for(RCR_Program_Category_Service_Type__c pcst : [SELECT ID,
                                                                Name,
                                                                Finance_Code_GG__c,
                                                                Finance_Code_NSAG__c
                                                        FROM    RCR_Program_Category_Service_Type__c
                                                        WHERE   Finance_Code_GG__c != null OR
                                                                Finance_Code_NSAG__c != null
                                                        ORDER BY Name])
        {
            if(pcst.Finance_Code_GG__c != null || pcst.Finance_Code_NSAG__c != null)
            {
                pcstList.add(pcst);
            }
        }
        return pcstList;
    }
}