@isTest
public class TestLSADeliveryPlanPDFRedirectExtension
{                   
    static testMethod void myTestMethod()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
        
        RCR_Contract__c c = [SELECT Id,
                                    Client__c
                             FROM RCR_Contract__c
                             LIMIT 1];
                                                                                      
        TestLoadFormData data = new TestLoadFormData();
        
        Plan__c p = new Plan__c();
        p.Client__c = c.Client__c;
        p.RecordTypeId = data.RecordTypes.get('MyPlan').Id;
        p.Plan_Status__c = 'Draft';
        insert p;
        
        Plan_Action__c pa = new Plan_Action__c();
        pa.Client__c = c.Client__c;
        pa.Participant_Plan__c = p.Id;
        pa.RecordTypeId = data.RecordTypes.get('Attendant_Care_and_Support').Id;
        insert pa;
        
        Plan_Goal__c g = new Plan_Goal__c();
        g.Participant_Plan__c = p.Id;
        insert g;
        
        c.Plan_Action__c = pa.Id;
        c.Attendant_Care_Support__c = 'Yes';
        update c;
                       
        Test.setCurrentPage(Page.LSAContractPDFRedirect);
        ApexPages.CurrentPage().getParameters().put('test', 'test');
        
        LSADeliveryPlanPDFRedirectExtension ext = new LSADeliveryPlanPDFRedirectExtension(new ApexPages.StandardController(p));
        ext.Redirect();
        
        Test.StopTest();        
    }
}