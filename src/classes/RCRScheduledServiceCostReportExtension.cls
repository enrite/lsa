public with sharing class RCRScheduledServiceCostReportExtension extends A2HCPageBase
{
    public RCRScheduledServiceCostReportExtension(ApexPages.Standardcontroller controller)
    {
        try
        {
            string soID = getParameter('so');

            string cssID = getParameter('css');
            string ahsID = getParameter('ahs');
            
            RCR_Contract__c serviceOrder = null;
            
            if(soID != null && soID != '')
            {
                 serviceOrder = [SELECT  Id,
                                        Name,
                                        RecordType.Name  
                                FROM    RCR_Contract__c
                                WHERE   Id = :soID];
                soID = serviceOrder.Id;
                ServiceOrderName = serviceOrder.Name;    
            }
            if((soID == null || soID == '') && (cssID != null && cssID != ''))
            {       
                serviceOrder = [SELECT  Id,
                                        Name,
                                        RecordType.Name 
                                FROM    RCR_Contract__c
                                WHERE   Id IN (SELECT   RCR_Contract__c
                                               FROM     RCR_Contract_Scheduled_Service__c 
                                               WHERE    Id = :cssID)];
                                
                soID = serviceOrder.Id;
                ServiceOrderName = serviceOrder.Name;                               
            }
            else if((soID == null || soID == '') && (ahsID != null && ahsID != ''))
            {                                          
                serviceOrder = [SELECT  Id,
                                        Name,
                                        RecordType.Name                                     
                                FROM    RCR_Contract__c
                                WHERE   Id IN (SELECT   RCR_Contract__c
                                               FROM     RCR_Contract_Adhoc_Service__c 
                                               WHERE    Id = :ahsID)];
                                
                soID = serviceOrder.Id;
                ServiceOrderName = serviceOrder.Name;
            }
            
            if(serviceOrder != null &&
                    serviceOrder.RecordType.Name != APSRecordTypes.RCRServiceOrder_ServiceOrderCBMS &&
                    serviceOrder.RecordType.Name != APSRecordTypes.RCRServiceOrder_ServiceOrder)
            {
                 throw new A2HCException('Usage Status Report is only available for Service Orders.');
            }
             
            if((soID != null && soID != '' && ServiceOrderName != null))
            {
                getServiceOrders();
                loadServiceOrder(soID);
                
                SelectedServiceOrderID = soID;
                
                if(cssID != null && cssID != '')
                {
                    SelectedScheduledService = ScheduledServicesByID.get(cssID);
                    loadScheduledServiceData();
                }
                else if(ahsID != null && ahsID != '')
                {
                    SelectedAdHocService = AdHocServicesByID.get(ahsID);
                    loadAdHocServiceData();
                }
            }
        }
        catch(exception ex)
        {
            A2HCException.formatException(ex);
        }
    }
    
    public string ServiceOrderName
    {
        get;
        set;
    }
    
    public string SelectedServiceOrderID
    {
        get;
        set;
    }
    
    public string ScheduledServiceID
    {
        get;
        set;
    }
    
    public string AdHocServiceID
    {
        get;
        set;
    }
     
    public decimal TotalServiceOrderDollarsApproved
    {
        get;
        private set;
    }

    public Map<string, decimal> TotalDollarsApprovedByServiceID
    {
        get
        {
            if(TotalDollarsApprovedByServiceID == null)
            { 
                TotalDollarsApprovedByServiceID = new Map<string, decimal>();
            }
            return TotalDollarsApprovedByServiceID;
        }
        private set;
    }
    
    public decimal TotalCSSDollarsApproved
    {
        get;
        private set;
    }
    
    public decimal TotalCSSDollarsRemaining
    {
        get;
        private set;
    }
 
    public decimal TotalAdHocDollarsApproved
    {
        get;
        private set;
    }
    
    public decimal TotalAdHocDollarsRemaining
    {
        get;
        private set;
    }
    
    public decimal TotalServiceOrderDollarsRemaining
    {
        get;
        private set; 
    }
    
    public string ReportDate
    {
        get
        {
            return Datetime.now().format('EEEE, d MMMM yyyy HH:mm');
        }
    }
    
    public RCR_Contract__c ServiceOrder
    {
        get;
        private set;
    }
    
    public RCR_Contract_Scheduled_Service__c SelectedScheduledService
    {
        get;
        set;
    }
    
    public boolean RenderScheduledService
    {
        get
        {
            return SelectedScheduledService != null ? true : false;
        }
    }
    
    public RCR_Contract_Adhoc_Service__c SelectedAdHocService
    {
        get;
        private set;
    }
    
    public boolean RenderAdHocervice
    {
        get
        {
            return SelectedAdHocService != null ? true : false;
        }
    }
    
    public List<RCR_Contract__c> ServiceOrders
    {
        get
        {
            if(ServiceOrders == null)
            {
                ServiceOrders = new List<RCR_Contract__c>();
            }
            return ServiceOrders;
        }
        private set;
    }
    
    private Map<Id, RCR_Contract_Scheduled_Service__c> ScheduledServicesByID
    {
        get
        {
            if(ScheduledServicesByID == null)
            {
                ScheduledServicesByID = new Map<Id, RCR_Contract_Scheduled_Service__c>();
            }
            return ScheduledServicesByID;
        }
        private set;
    }
       
    public List<RCR_Contract_Scheduled_Service__c> ScheduledServices
    {
        get
        {       
            if(ScheduledServices == null)
            {
                ScheduledServices = new List<RCR_Contract_Scheduled_Service__c>(); 
            }
            return ScheduledServices;
        }
        private set;
    }
    
    @TestVisible
    private Map<Id, RCR_Contract_Adhoc_Service__c> AdHocServicesByID
    {
        get
        {
            if(AdHocServicesByID == null)
            {
                AdHocServicesByID = new Map<Id, RCR_Contract_Adhoc_Service__c>();
            }
            return AdHocServicesByID;
        }
        private set;
    }
    
    public List<RCR_Contract_Adhoc_Service__c> AdHocServices
    {
        get  
        {
            if(AdHocServices == null)
            {
                AdHocServices = new List<RCR_Contract_Adhoc_Service__c>();
            }
            return AdHocServices;
        }
        private set;
    }

    
    public List<PeriodWrapper> AdhocPeriodWrappers
    {
        get
        {
            if(AdhocPeriodWrappers == null)
            {
                AdhocPeriodWrappers = new List<PeriodWrapper>();
            }
            return AdhocPeriodWrappers;
        }
        private set;
    }
    
    public List<PeriodWrapper> ScheduledPeriodWrappers
    {
        get
        {
            if(ScheduledPeriodWrappers == null)
            {
                ScheduledPeriodWrappers = new List<PeriodWrapper>();
            }
            return ScheduledPeriodWrappers;
        }
        private set;
    }
    
    public decimal TotalDollarsApproved
    {
        get
        {
            decimal dollars = 0.0;
            
            for(PeriodWrapper wrapper : ScheduledPeriodWrappers) 
            {
                dollars += wrapper.DollarsAlreadyUsed;
            }
            
            for(PeriodWrapper wrapper : AdhocPeriodWrappers) 
            {
                dollars += wrapper.DollarsAlreadyUsed;
            }
            
            return dollars;
        }
    }
    
    
    public PageReference getServiceOrders()
    { 
        try
        {
            SelectedServiceOrderID = '';
            ServiceOrder = null;
            ServiceOrders = null;
            SelectedScheduledService = null;
            ScheduledServicesByID = null;
            ScheduledServices = null;
            SelectedAdHocService = null; 
            AdHocServicesByID = null;
            AdHocServices = null;
            //TotalCSSDollarsApproved = 0.0;
            TotalAdHocDollarsApproved = 0.0;
            TotalServiceOrderDollarsRemaining = 0.0;

            if(ServiceOrderName == null || ServiceOrderName == '')
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You must enter a Service Order Number.'));
                return null;
            }
            
            string soID;
            
            for(RCR_Contract__c c : [SELECT Id,
                                            Name,
                                            Start_Date__c,
                                            End_Date__c,
                                            Account__r.Name 
                                    FROM    RCR_Contract__c 
                                    WHERE   Name = :ServiceOrderName.trim() AND
                                            (RecordType.Name = :APSRecordTypes.RCRServiceOrder_ServiceOrder OR
                                             RecordType.Name = :APSRecordTypes.RCRServiceOrder_ServiceOrderCBMS) 
                                    ORDER BY Start_Date__c desc])
            {
                ServiceOrders.add(c);
            }
            
            if(ServiceOrders.size() < 1)
            {
                A2HCException.formatException('Service Order ' + ServiceOrderName + ' was not found.');
                ScheduledServices = null;
                SelectedScheduledService = null;
                return null;
            }
            
            return null;
        }
        catch(exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }
    
    
    public PageReference loadServiceOrder()
    {
        try
        {
            SelectedServiceOrderID = getParameter('SelectedServiceOrderID');
            
            ServiceOrder = null;
            SelectedScheduledService = null;
            ScheduledServicesByID = null;
            ScheduledServices = null;
            SelectedAdHocService = null; 
            AdHocServicesByID = null;
            AdHocServices = null;
            
            if(SelectedServiceOrderID != null)
            {
                loadServiceOrder(SelectedServiceOrderID);
            }
            return null;
        }
        catch(Exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }
    
    
    private void loadServiceOrder(string serviceOrderId)
    {
        decimal totalDollarsRemianing = 0.0;
        TotalServiceOrderDollarsApproved = 0.0;
        TotalCSSDollarsApproved = 0.0;
        TotalCSSDollarsRemaining = 0.0;
        TotalAdHocDollarsApproved = 0.0;
        TotalAdHocDollarsRemaining = 0.0;
        
        ServiceOrder = [SELECT  s.Id,
                                s.Name,
                                s.Total_Service_Order_Cost__c,
                                s.Total_Scheduled_Service_Cost__c,
                                s.Total_Adhoc_Activity_Cost__c,
                                s.Start_Date__c,
                                s.End_Date__c,
                                s.Client__r.Full_Name__c,
                                (SELECT r.Start_Date__c,
                                        r.RCR_Service__c,
                                        r.RCR_Service__r.Name,
                                        r.RCR_Service__r.Description__c,
                                        r.RCR_Service__r.Account__c,
                                        r.RCR_Sub_Program__c,
                                        r.RCR_Contract__c,
                                        r.RCR_Contract__r.Account__c,
                                        r.RCR_Contract__r.Level__c,
                                        r.RCR_Contract__r.Name,
                                        r.Name,
                                        r.Id,
                                        r.End_Date__c,
                                        r.Original_End_Date__c,
                                        r.Flexibility__c,
                                        r.Public_holidays_not_allowed__c,
                                        r.Comment__c,
                                        r.Internal_Comments__c,
//                                        r.RCR_Contract__r.RCR_CBMS_Contract__r.Contract_Total__c,
                                        r.Total_Cost__c,
                                        r.Total_Quantity__c,
                                        r.Service_Types__c,
                                        r.Total_Submitted__c,
                                        r.Remaining__c,
                                        r.Exception_Count__c,
                                        r.Use_Negotiated_Rates__c,
                                        r.Negotiated_Rate_Standard__c,
                                        r.Negotiated_Rate_Public_Holidays__c
                                 FROM   RCR_Contract_Scheduled_Services__r r
                                 ORDER BY r.RCR_Service__r.Name ASC),
                                (SELECT a.Id,
                                        a.Name,
                                        a.RCR_Contract__c,
                                        a.RCR_Contract__r.Name,
                                        a.Start_Date__c,
                                        a.End_Date__c,
                                        a.Amount__c,
                                        a.Qty__c,
                                        a.Total_Submitted__c,
                                        a.Remaining__c,
                                        a.Service_Types__c, 
                                        a.Comment__c
                                 FROM   RCR_Contract_Adhoc_Services__r a
                                 ORDER BY a.Start_Date__c asc)
                        FROM    RCR_Contract__c s
                        WHERE   s.Id = :ServiceOrderID];
                                                            
        ServiceOrderName = ServiceOrder.Name; 
        
        
        // Get totals approved or pending approval for this Service Order.
        for(AggregateResult result : [SELECT    SUM(Total_ex_GST__c)    totApproved,
                                                RCR_Contract_Adhoc_Service__c   adhocId,
                                                RCR_Contract_Scheduled_Service__c   schedID
                                        FROM    RCR_Invoice_Item__c i
                                        WHERE   (Status__c = :APS_PicklistValues.RCRInvoiceItem_Status_Approved 
                                        OR       Status__c = :APS_PicklistValues.RCRInvoiceItem_Status_PendingApproval)
                                        AND     RCR_Service_Order__c = :ServiceOrderID
                                        GROUP BY RCR_Contract_Adhoc_Service__c,
                                                RCR_Contract_Scheduled_Service__c])
        {
            ID adhocId = (ID)(result.get('adhocId'));
            ID schedID = (ID)(result.get('schedID'));
            decimal total = (decimal)(result.get('totApproved'));
            
            TotalServiceOrderDollarsApproved += total;
            if(adhocId != null)
            {
                TotalDollarsApprovedByServiceID.put(adhocId, total);
                TotalAdHocDollarsApproved += total;
            }
            if(schedID != null)
            {
                TotalDollarsApprovedByServiceID.put(schedID, total);
                TotalCSSDollarsApproved += total;
            }
        }                                
        
        for(RCR_Contract_Scheduled_Service__c css : ServiceOrder.RCR_Contract_Scheduled_Services__r)
        {                 
            css.Total_Submitted__c = css.Total_Submitted__c == null ? 0.00 : css.Total_Submitted__c; 
            ScheduledServices.add(css);
            ScheduledServicesByID.put(css.Id, css);
            totalDollarsRemianing += css.Remaining__c;   
            
            // Ensure that there is an entry for every Scheduled Service even if there are no Invoices for it!
            if(!TotalDollarsApprovedByServiceID.containsKey(css.Id))
            {
                TotalDollarsApprovedByServiceID.put(css.Id, 0.0);
            }     
        }
        
        for(RCR_Contract_Adhoc_Service__c ahs : ServiceOrder.RCR_Contract_Adhoc_Services__r)   
        {
            ahs.Total_Submitted__c = ahs.Total_Submitted__c == null ? 0.00 : ahs.Total_Submitted__c;
            AdHocServices.add(ahs);
            AdHocServicesByID.put(ahs.Id, ahs);
            totalDollarsRemianing += ahs.Remaining__c;
            
            // Ensure that there is an entry for every AdHoc Service even if there are no Invoices for it!
            if(!TotalDollarsApprovedByServiceID.containsKey(ahs.Id))
            {
                TotalDollarsApprovedByServiceID.put(ahs.Id, 0.0);
            }
        }
        
        TotalServiceOrderDollarsRemaining = totalDollarsRemianing;     
    }


    public void refreshScheduledServiceData()
    {
        try
        {
            SelectedScheduledService = null;
            SelectedAdHocService = null; 

            SelectedScheduledService = ScheduledServicesByID.get(ScheduledServiceID);     
            loadScheduledServiceData();
        }
        catch(exception ex)
        {
            A2HCException.formatException(ex);
        }
    }
    
    
    public void refreshAdHocServiceData()
    {
        try
        {
            SelectedAdHocService = null; 
            SelectedScheduledService = null;
            
            system.debug('AdHocServiceID' + AdHocServiceID);
            if(AdHocServiceID != null)
            {
                SelectedAdHocService = AdHocServicesByID.get(AdHocServiceID);
                loadAdHocServiceData();
            }
        }
        catch(exception ex)
        {
            A2HCException.formatException(ex);
        }
    }
    
    
    private void loadScheduledServiceData()
    {
        AdhocPeriodWrappers = null;  
        ScheduledPeriodWrappers = new List<PeriodWrapper>();
  
        List<RCR_Invoice_Item__c> invoiceItems = [SELECT    Id,
                                                            Name,
                                                            Activity_Date__c,
                                                            Code__c,
                                                            Quantity__c,
                                                            Rate__c,
                                                            Total_ex_GST__c,
                                                            Status__c,
                                                            Error__c,
                                                            RCR_Invoice__r.Invoice_Number__c,
                                                            RCR_Invoice__r.RecordType.Name,
                                                            RCR_Invoice__r.Status__c,
                                                            RecordType.Name
                                                    FROM    RCR_Invoice_Item__c
                                                    WHERE   RCR_Contract_Scheduled_Service__c = :SelectedScheduledService.Id
                                                    ORDER BY Activity_Date__c];
                                            
        date maxActivityDate;
        if(invoiceItems.size() > 0)
        {
            maxActivityDate = invoiceItems[invoiceItems.size() -1].Activity_Date__c;
        }

             
        Schedule__c schedule;
        ScheduleDateEngine dtEngine = new ScheduleDateEngine(SelectedScheduledService.Id);
        schedule = dtEngine.Schedules[0];
        
        if(schedule.Number_of_weeks_in_period__c == 'Service Date Range')
        { 
            date lastPeriodEndDate = maxActivityDate > SelectedScheduledService.End_Date__c ? maxActivityDate : SelectedScheduledService.End_Date__c;
            
            ScheduledPeriodWrappers.add(new PeriodWrapper(SelectedScheduledService.Start_Date__c, 
                                                 lastPeriodEndDate, 
                                                 SelectedScheduledService.Total_Cost__c, 
                                                 SelectedScheduledService.Total_Quantity__c,
                                                 SelectedScheduledService, 
                                                 null,
                                                 invoiceItems));
        }
        else
        {
            date startDate;
            RCR_Service__c service = RCRServiceExtension.getRCRService(SelectedScheduledService.RCR_Service__c);
            RCR_Contract__c serviceOrder = RCRServiceOrderRequestController.getServiceOrder(SelectedScheduledService.RCR_Contract__c);
                        
            // Initialise startDate
            if(schedule.Number_of_weeks_in_period__c == 'Monthly')
            {
                // Monthly schedule
                startDate = schedule.Start_Date__c.toStartOfMonth();
            }
            else
            {
                // Weekly schedule            
                startDate = schedule.Start_Date__c.toStartOfWeek();
            }

            Integer weeksInPeriod = ScheduleDateEngine.getNumberOfWeeksFromDescription(schedule.Number_of_weeks_in_period__c);
            
            SelectedScheduledService = [SELECT  Start_Date__c,
                                               RCR_Service__c,
                                               RCR_Service__r.Name,
                                               RCR_Service__r.Description__c,
                                               RCR_Service__r.Account__c,
                                               RCR_Sub_Program__c,
                                               RCR_Contract__c,
                                               RCR_Contract__r.Account__c,
                                               RCR_Contract__r.Level__c,
                                               RCR_Contract__r.Name,
                                               Name,
                                               Id,
                                               End_Date__c,
                                               Original_End_Date__c,
                                               Flexibility__c,
                                               Public_holidays_not_allowed__c,
                                               Comment__c,
                                               Internal_Comments__c,
//                                               RCR_Contract__r.RCR_CBMS_Contract__r.Contract_Total__c,
                                               Total_Cost__c,
                                               Total_Quantity__c,
                                               Service_Types__c,
                                               Total_Submitted__c,
                                               Remaining__c,
                                               Exception_Count__c,
                                               Use_Negotiated_Rates__c,
                                               Negotiated_Rate_Standard__c,
                                               Negotiated_Rate_Public_Holidays__c,
                                               (SELECT  Date__c,
                                                        Description__c,
                                                        Public_Holiday_Quantity__c,
                                                        Standard_Quantity__c
                                                 FROM   RCR_Scheduled_Service_Exceptions__r)
                                           FROM RCR_Contract_Scheduled_Service__c
                                           WHERE    ID = :SelectedScheduledService.ID];
                                           
            date lastPeriodEndDate = maxActivityDate > SelectedScheduledService.End_Date__c ? maxActivityDate : SelectedScheduledService.End_Date__c;
            
            while(startDate <= lastPeriodEndDate)
            {
                // Get the period End Date
                date endDate;
                
                if(schedule.Number_of_weeks_in_period__c == 'Monthly')
                {
                    endDate = startDate.addMonths(1).addDays(-1);
                }
                else
                {
                    endDate = startDate.addDays((7 * weeksInPeriod) - 1);
                }             
              
                Map<decimal, decimal> totals = RCRInvoiceWebService.getTotals(service, 
                                                                                SelectedScheduledService, 
                                                                                startDate, 
                                                                                endDate, 
                                                                                dtEngine);
                                                                    
                system.debug('Periods: ' + totals.keySet().size());
                for(decimal totalCost : totals.keySet())
                {
                    ScheduledPeriodWrappers.add(new PeriodWrapper(startDate,  
                                                                    endDate, 
                                                                    totalCost, 
                                                                    totals.get(totalCost), 
                                                                    SelectedScheduledService, 
                                                                    null, 
                                                                    invoiceItems));
                }
                
      
                // Increment startDate
                if(schedule.Number_of_weeks_in_period__c == 'Monthly')
                {
                    startDate = startDate.addMonths(1);
                }
                else
                {
                    startDate = startDate.addDays(7 * weeksInPeriod);
                }
            }
        }
    }
    
    @TestVisible
    private void loadAdHocServiceData()
    {   
        ScheduledPeriodWrappers = null;  
        AdhocPeriodWrappers = new List<PeriodWrapper>(); 
      
        List<RCR_Invoice_Item__c> invoiceItems = [SELECT    Id,
                                                            Name,
                                                            Activity_Date__c,
                                                            Code__c,
                                                            Quantity__c,
                                                            Rate__c,
                                                            Total_ex_GST__c,
                                                            Status__c,
                                                            Error__c,
                                                            RCR_Invoice__r.Invoice_Number__c,
                                                            RCR_Invoice__r.RecordType.Name,
                                                            RecordType.Name
                                                    FROM    RCR_Invoice_Item__c
                                                    WHERE   RCR_Contract_Adhoc_Service__c = :SelectedAdHocService.Id
                                                    ORDER BY Activity_Date__c];
                                                    
        AdhocPeriodWrappers.add(new PeriodWrapper(SelectedAdHocService.Start_Date__c, 
                                             SelectedAdHocService.End_Date__c, 
                                             SelectedAdHocService.Amount__c, 
                                             SelectedAdHocService.Qty__c,
                                             null,
                                             SelectedAdHocService, 
                                             invoiceItems));                                                  
    }
    
    
    public class PeriodWrapper
    {
        public PeriodWrapper(dateTime pStartDate, 
                             dateTime pEndDate, 
                             decimal pDollarsAvailable, 
                             decimal pQuantityAvailable, 
                             RCR_Contract_Scheduled_Service__c pCSS, 
                             RCR_Contract_Adhoc_Service__c pAdHocService,
                             List<RCR_Invoice_Item__c> pInvoiceItems)
        {
            PeriodStartDate = pStartDate;
            PeriodEndDate = pEndDate;
            DollarsAvailable = pDollarsAvailable;
            QuantityAvailable = pQuantityAvailable;
            ScheduledService = pCSS;
            AdHocService = pAdHocService;            
            CodeMissmatch = false;            
            decimal dollars = 0.0;
            decimal quantity = 0.0;
            
            for(RCR_Invoice_Item__c item : pInvoiceItems)
            {
                if(item.Activity_Date__c >= pStartDate && item.Activity_Date__c <= pEndDate)
                {
                    InvoiceItems.add(item);                    
                    if(!CodeMissmatch)
                    {
                        CodeMissmatch = ScheduledService != null && 
                                item.Code__c != ScheduledService.RCR_Service__r.Name 
                                && item.Status__c == APS_PicklistValues.RCRInvoiceItem_Status_Approved;
                    }
                    if(item.Status__c == APS_PicklistValues.RCRInvoiceItem_Status_Approved)
                    {
                        dollars += item.Total_ex_GST__c;
                        quantity += item.Quantity__c * (item.Total_ex_GST__c < 0 ? -1.0 : 1.0);
                    }
                }
                if(item.Activity_Date__c > pEndDate)
                {
                    break;
                }
            }
            DollarsAlreadyUsed = dollars;
            TotalQuantityUsed = quantity;
        }
            
        
        public dateTime PeriodStartDate
        {
            get;
            private set;
        }
        
        public dateTime PeriodEndDate
        {
            get;
            private set;
        }
        
        public string PeriodDateRange
        {
            get
            {
                return PeriodStartDate.format('dd/MM/yyyy') + ' - ' + PeriodEndDate.format('dd/MM/yyyy');
            }
        }
        
        public decimal DollarsAvailable
        {
            get;
            private set;
        }
        
        public decimal QuantityAvailable
        {
            get;
            private set;
        }
        
        public List<RCR_Invoice_Item__c> InvoiceItems
        {
            get
            {
                if(InvoiceItems == null)
                {
                    InvoiceItems = new List<RCR_Invoice_Item__c>();
                }
                return InvoiceItems;
            }
            private set;
        }
        
        public decimal DollarsAlreadyUsed
        {
            get;
            private set;
        }
        
        public decimal TotalQuantityUsed
        {
            get;
            private set;
        }
        
        public RCR_Contract_Scheduled_Service__c ScheduledService
        {
            get;
            private set;
        }
        
        public RCR_Contract_Adhoc_Service__c AdHocService
        {
            get;
            private set;
        }
        
        public boolean CodeMissmatch
        {
            get;
            private set;
        }
    }
    
    
    @IsTest//(SeeAllData=true)
    private static void test1()
    {
        TestLoadData.loadRCRData();


        Test.StartTest();
        TestLoadData.loadInvoiceItems();
        TestLoadData.loadAdhocServices();     
    
        string cssID = [SELECT RCR_Contract_Scheduled_Service__c 
                         FROM   RCR_Invoice_Item__c 
                         WHERE  RCR_Contract_Scheduled_Service__c != null
                         AND    Status__c = :APS_PicklistValues.RCRInvoiceItem_Status_Approved
                         AND    Total__c > 0.0
                         AND    RCR_Contract_Scheduled_Service__c IN (SELECT Id
                                                                     FROM   RCR_Contract_Scheduled_Service__c
                                                                     WHERE  Flexibility__c = :APS_PicklistValues.RCRContractScheduledService_Flexibilty_Accumulative)
                         LIMIT 1].RCR_Contract_Scheduled_Service__c;
                     
        Test.setCurrentPage(Page.RCRScheduledServiceCostBreakdown);
        ApexPages.currentPage().getParameters().put('css', cssID);
        ApexPages.StandardController sc = new ApexPages.StandardController(new RCR_Contract_Scheduled_Service__c());
        
        RCRScheduledServiceCostReportExtension ext = new RCRScheduledServiceCostReportExtension(sc);

        system.debug(ext.ReportDate);
        system.debug(ext.ServiceOrder);
        system.debug(ext.SelectedScheduledService);
        system.debug(ext.ScheduledServices);
        system.debug(ext.AdHocServices);
        system.debug(ext.TotalDollarsApproved);
        
        for(PeriodWrapper wrapper : ext.AdhocPeriodWrappers)
        {
            system.debug(wrapper.PeriodDateRange);
        }
        
        for(PeriodWrapper wrapper : ext.ScheduledPeriodWrappers)
        {
            system.debug(wrapper.PeriodDateRange);
        }
        
        Test.StopTest();
    }
    
    @IsTest//(SeeAllData=true)
    private static void test2()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        TestLoadData.loadAdhocServices();     
    
        RCR_Invoice_Item__c item = [SELECT RCR_Contract_Scheduled_Service__c,
                                            RCR_Contract_Scheduled_Service__r.RCR_Contract__r.Name 
                                     FROM   RCR_Invoice_Item__c 
                                     WHERE  RCR_Contract_Scheduled_Service__c != null
                                     AND    Status__c = :APS_PicklistValues.RCRInvoiceItem_Status_Approved
                                     AND    Total__c > 0.0
                                     AND    RCR_Contract_Scheduled_Service__c IN (SELECT Id
                                                                                 FROM   RCR_Contract_Scheduled_Service__c
                                                                                 WHERE  Flexibility__c = :APS_PicklistValues.RCRContractScheduledService_Flexibilty_WithinThePeriod)                                                     
                                     LIMIT 1];
                     
        Test.setCurrentPage(Page.RCRScheduledServiceCostBreakdown);
        ApexPages.currentPage().getParameters().put('css', item.RCR_Contract_Scheduled_Service__c);
        ApexPages.StandardController sc = new ApexPages.StandardController(new RCR_Contract_Scheduled_Service__c());
        
        RCRScheduledServiceCostReportExtension ext = new RCRScheduledServiceCostReportExtension(sc);
        
        system.debug(ext.ReportDate);
        system.debug(ext.ServiceOrder);
        system.debug(ext.SelectedScheduledService);
        system.debug(ext.ScheduledServices);
        system.debug(ext.AdHocServices);
        
        for(PeriodWrapper wrapper : ext.AdhocPeriodWrappers)
        {
            system.debug(wrapper.PeriodDateRange);
        }
        
        for(PeriodWrapper wrapper : ext.ScheduledPeriodWrappers)
        {
            system.debug(wrapper.PeriodDateRange);
        }
        
        ext.ServiceOrderName = item.RCR_Contract_Scheduled_Service__r.RCR_Contract__r.Name;
        ext.getServiceOrders();
        ext.refreshScheduledServiceData();
        
        Test.StopTest();
    }
    
    @IsTest//(SeeAllData=true)
    private static void test3()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        TestLoadData.loadAdhocServices();     
    
        string ahsID = [SELECT  RCR_Contract_Adhoc_Service__c 
                         FROM   RCR_Invoice_Item__c 
                         WHERE  RCR_Contract_Adhoc_Service__c != null
                         AND    Status__c = :APS_PicklistValues.RCRInvoiceItem_Status_Approved
                         AND    Total__c > 0.0                                                    
                         LIMIT 1].RCR_Contract_Adhoc_Service__c;
                     
        Test.setCurrentPage(Page.RCRScheduledServiceCostBreakdown);
        ApexPages.currentPage().getParameters().put('ahs', ahsID);
        ApexPages.StandardController sc = new ApexPages.StandardController(new RCR_Contract_Scheduled_Service__c());
        
        RCRScheduledServiceCostReportExtension ext = new RCRScheduledServiceCostReportExtension(sc);

        system.debug(ext.ReportDate);
        system.debug(ext.ServiceOrder);
        system.debug(ext.SelectedScheduledService);
        system.debug(ext.ScheduledServices);
        system.debug(ext.AdHocServices);
        
        for(PeriodWrapper wrapper : ext.AdhocPeriodWrappers)
        {
            system.debug(wrapper.PeriodDateRange);
        }
        
        for(PeriodWrapper wrapper : ext.ScheduledPeriodWrappers)
        {
            system.debug(wrapper.PeriodDateRange);
        }
        ext.refreshAdHocServiceData();
        
        Test.StopTest();
    }
}