public class LPMemberDetailJSON
{

    public String memberKey
    {
        get;
        set;
    }
    public Name name
    {
        get;
        set;
    }
    public String membershipNumber
    {
        get;
        set;
    }

    public cls_embedded embedded;

    public class cls_embedded
    {
        public cls_lpplans[] lpcontracts;
    }

    public class cls_lpplans
    {
        public Boolean isActive;
        public String startDate;
        public String endDate;

        public cls_totalBudget totalBudget;
        public cls_links links;
    }

    public class cls_links
    {
        public cls_lpinvoicehistory lpinvoicehistory;
    }

    public class  cls_lpinvoicehistory
    {
        public String href;
    }

    public class cls_totalBudget
    {
        public Decimal total;
        public Decimal spent;
        public Decimal unspent;

    }

    public static LPMemberDetailJSON parse(String jsonString)
    {
        jsonString =jsonString.replace('_embedded', 'embedded').replace('_links', 'links').replace('lp:invoice-history', 'lpinvoicehistory');
        system.debug(jsonString);
        return (LPMemberDetailJSON)JSON.deserialize(jsonString, LPMemberDetailJSON.class);
    }
}