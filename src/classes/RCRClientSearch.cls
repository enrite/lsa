public  class RCRClientSearch 
	extends ClientSearch
{
	public RCRClientSearch()
    {
    	super(20); 
    }
    
    public RCRClientSearch(ApexPages.StandardSetController controller)
    {
    	super(20);
    }

    public PageReference doSearch()
    {
        try
        {
            super.setSearchFields();                    
            Date dob = null;
            if(String.isNotBlank(SearchDOB))
            {
                dob = A2HCUtilities.tryParseDate(SearchDOB);
            }    
            Clients = [SELECT ID,
                                Name,
                                Client_ID__c, 
                                CCMS_ID__c,
                                Given_Name__c,
                                Family_Name__c,
                                Full_Name__c,
                                Address__c,
                                Status__c,
                                Sex__c,
                                Date_Of_Birth__c,
                                Phone__c,
                                Suburb__c
                        FROM    Referred_Client__c
                        WHERE (
	                        		(
	                        			CCMS_ID__c != null AND
	                        			CCMS_ID__c LIKE :(String.isBlank(SearchCCMSID) ? '%' : SearchCCMSID) AND
	                        			Family_Name__c LIKE :('%' + SearchFamilyName + '%') AND
	                                	Given_Name__c LIKE :('%' + SearchGivenName + '%') 
	                            	)  
	                            	OR
	                        		(	
	                        			Account__r.API_Name__c = :APS_PicklistValues.Account_DCSI_RCR_Programs AND
	                        			CCMS_ID__c LIKE :(String.isBlank(SearchCCMSID) ? '' : '~') AND
		                        		Name LIKE :('%' + SearchFamilyName + '%') AND
		                        		Given_Name__c = :SearchGivenName
	                            	) 
                            	) AND                                                               
                                (
                                	Address__c LIKE :('%' + SearchAddress + '%') OR
                                	Suburb__c LIKE :('%' + SearchAddress + '%') OR
                               		Name LIKE :(String.isBlank(SearchAddress) ? '%' : '~')
                           		) AND
                                (
                                	Date_Of_Birth__c = :dob OR
                                	Name LIKE :(dob == null ? '%' : '~')
                            	) AND
                                (
                                	Status__c = :SearchStatus OR
                               		Name LIKE :(String.isBlank(SearchStatus) ? '%' : '~')
                           		) AND
                                (
                                	Sex__c = :SearchSex OR
                                	Name LIKE :(String.isBlank(SearchSex) ? '%' : '~')
                            	) AND
                            	(
                            		IF_Authority_to_Proceed__c = :IFOnly OR
                            		Name LIKE :(IFOnly == true ? '~' : '%')
                            	)
                        ORDER BY  
                        		Family_Name__c,
                        		Given_Name__c,
                        		Name
                        LIMIT 101];
            SearchDone = true;
            return null;
        }
        catch(Exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }
    
   
}