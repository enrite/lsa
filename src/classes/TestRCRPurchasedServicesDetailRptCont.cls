@isTest
private class TestRCRPurchasedServicesDetailRptCont
{
    public static TestMethod void myTestMethod() 
    {        
        TestLoadData.loadRCRData();
        
        Test.StartTest();                
                
        ApexPages.CurrentPage().getParameters().put('clientid', [SELECT Id FROM Referred_Client__c LIMIT 1].Id);                
                   
        RCRPurchasedServicesDetailRptController ext = new RCRPurchasedServicesDetailRptController();                                   
        system.debug(ext.hrefCompensableOption);        
        system.debug(ext.Client);
        system.debug(ext.CompensableOptions);
        system.debug(ext.StartDate);
        system.debug(ext.EndDate);
        system.debug(ext.GrandTotalExGST);
        system.debug(ext.GrandTotalGST);
        system.debug(ext.GrandTotalIncGST);
        system.debug(ext.SelectedCompensableOption);
        system.debug(ext.PurchasedServiceDetails);
        ext.getClient();
        
        ext.StartDate = String.valueOf(Date.today());
        ext.EndDate = String.valueOf(Date.today());
        ext.doSearch();
        
        RCRPurchasedServicesDetailRptController.ServiceDetail sd = new RCRPurchasedServicesDetailRptController.ServiceDetail('a', Date.today(), Date.today(), Date.today(), Date.today(), 'a', 1, 1, 1);
        system.debug(sd.InvoiceNumber);
        system.debug(sd.InvoiceDate);
        system.debug(sd.InvoiceStartDate);
        system.debug(sd.InvoiceEndDate);
        system.debug(sd.BatchSentDate);
        system.debug(sd.Compensable);
        system.debug(sd.TotalExGST);
        system.debug(sd.TotalGST);
        system.debug(sd.TotalIncGST);
        
        Test.StopTest();
    }
}