/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestCalendarControl {

    static testMethod void myUnitTest() 
    {
        CalendarControl cc = new CalendarControl();
        
        cc.FromDate = Date.valueOf('2011-04-05 00:00:00');
        cc.ToDate = Date.valueOf('2011-06-10 00:00:00');
        
        cc.SelectPastDates = true;
        System.assert(cc.SelectPastDates);
        
        System.assert(cc.getNumberOfMonths() == 3);
        System.assert(cc.getFromMonthIndex() == 3);
        System.assert(cc.getFromYear() == 2011);
      
		System.assert(cc.getToDateOutOfBounds().equals('YAHOO.CC.calendar.cal1.addRenderer("2011/6/11-2011/6/30", YAHOO.CC.calendar.cal1.renderOutOfBoundsDate);'));
		System.assert(cc.getFromDateOutOfBounds().equals('YAHOO.CC.calendar.cal1.addRenderer("2011/4/1-2011/4/4", YAHOO.CC.calendar.cal1.renderOutOfBoundsDate);'));
		
		cc.inStringDateListCSV = '2010-11-28,2010-11-11,2010-12-31';
		List<Date> dates = cc.convertedToList();
    }
}