public with sharing class GrantsAttachmentUploadController extends AttachmentUploadController
{
    public GrantsAttachmentUploadController()
    {
        super();
    }

    public override PageReference upload()
    {
        Savepoint sp = Database.setSavePoint();
        try
        {
            PageReference pg = super.upload();

            // fire workflow
            Grant_Milestone__c gm = new Grant_Milestone__c(ID = getParameter('id'));
            gm.Attachment_Uploaded__c = Datetime.now();
            update gm;

            return pg;
        }
        catch(Exception ex)
        {
            return A2HCException.formatExceptionAndRollback(sp, ex);
        }
    }
}