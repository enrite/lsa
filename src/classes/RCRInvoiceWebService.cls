global class RCRInvoiceWebService
{
//    webservice static RCR_Service_Order_Payment__c[] getServiceOrderPayments(String invoiceBatchID)
//    {
//        return new List<RCR_Service_Order_Payment__c>();
//    }
    
    webservice static RCR_MP_Service_Order_Payment__c[] getMasterpieceServiceOrderPayments(String invoiceBatchID)
    {
        return getMasterpiecePaymentsForBatch(invoiceBatchID);
    }
    
    webservice static RCR_Invoice__c[] getBatchInvoiceCount(String invoiceBatchID)
    {
        return getNumberOfInvoicesInBatch(invoiceBatchID);
    }
    
    webservice static Attachment getMasterpieceFile(String invoiceBatchID)
    {
        Attachment att = getMasterpieceAttachment(invoiceBatchID);
        if(att == null)
        {
            return null;
        }
        for(RCR_Invoice_Batch__c invBatch : [SELECT ID,
                                                    MP_File_Name__c,
                                                    Lantern_Pay_Batch__c
                                            FROM    RCR_Invoice_Batch__c
                                            WHERE   ID = :invoiceBatchID])
        {
            EmailManager em = new EmailManager();
            em.sendMasterpieceReconcilliationReports(invBatch);

            if(invBatch.Lantern_Pay_Batch__c && invBatch.MP_File_Name__c == null)
            {
                invBatch.MP_File_Name__c = 'Payments.txt';
                invBatch.Approved_Date__c = Datetime.now();
                invBatch.Approved_By__c = UserInfo.getUserId();
                update invBatch;
            }
        }
        return att;
    }

    webservice static RCR_Invoice_Batch__c[] getMasterpieceBatches()
    {
        List<RCR_Invoice_Batch__c> approvedBatches = [SELECT ID
                                                    FROM    RCR_Invoice_Batch__c
                                                    WHERE   Lantern_Pay_Batch__c = false AND
                                                            Approved_Date__c != null AND
                                                            Sent_Date__c != null AND
                                                            Masterpiece_Paid__c = null];
        if(approvedBatches.isEmpty())
        {
            return null;
        }
        return approvedBatches;
    }

    private static RCR_Invoice_Batch__c createBatch(List<RCR_Invoice__c> approvedInvoices, Boolean lanternPayBatch, String batchDescription)
    {
        RCR_Invoice_Batch__c batch = new RCR_Invoice_Batch__c();
        batch.Lantern_Pay_Batch__c = lanternPayBatch;
        batch.Sent_Date__c = Datetime.now();
        batch.Description__c = batchDescription; //'Masterpiece batch ' + Date.today().format();
        insert batch;
        for(RCR_Invoice__c inv : approvedInvoices)
        {
            inv.RCR_Invoice_Batch__c = batch.ID;
        }
        update approvedInvoices;

        RCRInvoiceBatchController batchController = new RCRInvoiceBatchController(batch.ID);
        batchController.printAndCreatePaymentRecords();

        return batchController.Batch;
    }

    webservice static RCR_Invoice_Batch__c createLPBatch()
    {
        try
        {
            List<RCR_Invoice__c> lpInvoices = [SELECT ID
                                                FROM    RCR_Invoice__c
                                                WHERE   LP_Invoice_Key__c != null AND
                                                        RecordType.DeveloperName = 'Submitted' AND
                                                        RCR_Invoice_Batch__c = null AND
                                                        ID IN (SELECT   Activity_Statement__c
                                                                FROM    LP_Confirmed_Payment__c
                                                                WHERE   Match_Status__c = 'Matched')];
            if(lpInvoices.isEmpty())
            {
                return null;
            }
            return createBatch(lpInvoices, true, 'LanternPay batch ' + Date.today().format());
        }
        catch (Exception ex)
        {
            EmailManager.sendAdminErrorMessage('ERROR: LP Masterpiece Batch job failed', ex);
            return null;
        }
    }

    webservice static void setBatchesPaid(RCR_Invoice_Batch__c[] batches)
    {
        if(batches == null)
        {
            return;
        }
        Datetime dt = DateTime.now();
        for(RCR_Invoice_Batch__c i : batches)
        {
            i.Masterpiece_Paid__c = dt;
        }
        update batches;
    }

    webservice static void updateBatch(String invoiceBatchID, Integer batchNumber, String masterPieceFileName, String errMsg)
    {
        if(errMsg != null && errMsg.length() > 255)
        {
            errMsg = errMsg.substring(0, 255);
        }
        RCR_Invoice_Batch__c b = new  RCR_Invoice_Batch__c(ID = invoiceBatchID);
        b.CBMS_Error__c = errMsg;
        if(batchNumber != null)
        {
            b.Batch_Number__c = batchNumber;
        }
        if(masterPieceFileName != null)
        {
            b.MP_File_Name__c = masterPieceFileName;
        }
        update b;
        /* this now happens manually from a button on the invoice batch
        if(masterPieceFileName != null)
        {
            (new EmailManager()).sendMasterpieceReconcilliationReports(b);
        }*/
    }

    webservice static RCR_Contract_Service__c[] getContractServices(String contractId)
    { 
        return getAllServices(contractId);
    }

    webservice static Attachment[] getFiles(String batchID)
    {
        return getAllFiles(batchID);
    }

    webservice static string getMasterpieceFileFolder(ID invBatchID)
    {
        for(RCR_Invoice_Batch__c invBatch : [SELECT ID
                                            FROM    RCR_Invoice_Batch__c
                                            WHERE   ID = :invBatchID AND
                                                    Lantern_Pay_Batch__c = true])
        {
            return Masterpiece_File_Folder__c.getInstance().Lantern_Pay_Folder__c;
        }
        return Masterpiece_File_Folder__c.getInstance().Folder__c;
    }

    webservice static Attachment getAttachment(ID invBatchID)
    {
       for(Attachment att : [SELECT ID,
                                    Body
                        FROM Attachment
                        WHERE  ParentId = :invBatchID])
        {
            return att;
        }
        return null;
    }

    public static List<Attachment> getAllFiles(String batchID)
    {
        return [SELECT  Name,
                        Body 
               FROM     Attachment
               WHERE    ParentId = :batchID];
    }

    public static Map<Date, String> PublicHolidays
    {
        get
        {
            if(PublicHolidays == null)
            {
                PublicHolidays = new Map<Date, String>();
                for(Holiday h : [SELECT h.Name, 
                                        h.ActivityDate 
                                FROM    Holiday h 
                                WHERE   IsAllDay = true])
                {
                    PublicHolidays.put(h.ActivityDate, h.Name);
                }
            }
            return PublicHolidays;
        }
        set;
    }
    
    public static decimal getScheduledServiceTotalCost(RCR_Service__c service, RCR_Contract_Scheduled_Service__c scheduledService, List<RCR_Service_Order_Break_Period__c> breakPeriods, boolean doUpdate)
    {
        decimal total = 0.00;
        for(RCR_Contract_Service__c cs : getContractServices(service, scheduledService, breakPeriods, scheduledService.Start_Date__c, scheduledService.End_Date__c, null, doUpdate)) 
        {
            total += cs.Amount__c;
        }
        return total.setScale(2, System.roundingMode.HALF_UP);
    }

    public static Map<decimal, decimal> getTotals(RCR_Service__c service, RCR_Contract_Scheduled_Service__c scheduledService)
    {   
        return getTotals(service, scheduledService, scheduledService.Start_Date__c, scheduledService.End_Date__c, null);
    }
    
    public static Map<decimal, decimal> getTotals(RCR_Service__c service, RCR_Contract_Scheduled_Service__c scheduledService, boolean doUpdate)
    {   
        return getTotals(service, scheduledService, scheduledService.Start_Date__c, scheduledService.End_Date__c, null, doUpdate);
    }   
    
    public static Map<decimal, decimal> getTotals(RCR_Service__c service, RCR_Contract_Scheduled_Service__c scheduledService, Date startDate, Date endDate, ScheduleDateEngine dtEngine)
    {
        decimal totalCost = 0.00;
        decimal totalServices = 0.0;
        Map<decimal, decimal> totals = new Map<decimal, decimal>();
        
        for(RCR_Contract_Service__c cs : getContractServices(service, scheduledService, startDate, endDate, dtEngine))
        {           
            totalCost += cs.Amount__c;
            totalServices += cs.Qty__c;
        }
        totalCost = totalCost.setScale(2, System.roundingMode.HALF_UP);
        totals.put(totalCost, totalServices);       
        return totals;
    }
    
    public static Map<decimal, decimal> getTotals(RCR_Service__c service, RCR_Contract_Scheduled_Service__c scheduledService, Date startDate, Date endDate, ScheduleDateEngine dtEngine, boolean doUpdate)
    {
        decimal totalCost = 0.00;
        decimal totalServices = 0.0;
        Map<decimal, decimal> totals = new Map<decimal, decimal>();
        if(doUpdate)
        {
            DMLAsSystem.deleteRecords([SELECT  Id
                                       FROM     RCR_Scheduled_Service_Breakdown_By_Rate__c
                                       WHERE    RCR_Contract_Scheduled_Service__c = :scheduledService.Id]);
        }
        for(RCR_Contract_Service__c cs : getContractServices(service, scheduledService, null, startDate, endDate, dtEngine, true))
        {           
            totalCost += cs.Amount__c;
            totalServices += cs.Qty__c;
        }
        totalCost = totalCost.setScale(2, System.roundingMode.HALF_UP);
        totals.put(totalCost, totalServices);       
        return totals;
    }
    
    public static Map<decimal, decimal> getTotals(RCR_Service__c service, RCR_Contract_Scheduled_Service__c scheduledService, List<RCR_Service_Order_Break_Period__c> breakPeriods, Date startDate, Date endDate, ScheduleDateEngine dtEngine)
    {
        decimal totalCost = 0.00;
        decimal totalServices = 0.0;
        Map<decimal, decimal> totals = new Map<decimal, decimal>();
        
        for(RCR_Contract_Service__c cs : getContractServices(service, scheduledService, breakPeriods, startDate, endDate, dtEngine, false))
        {           
            totalCost += cs.Amount__c;
            totalServices += cs.Qty__c;
        }
        totalCost = totalCost.setScale(2, System.roundingMode.HALF_UP);
        totals.put(totalCost, totalServices);       
        return totals;
    }

    public static List<RCR_Contract_Service__c> getContractServices(RCR_Service__c service, RCR_Contract_Scheduled_Service__c css, Date startDate, Date endDate, ScheduleDateEngine dateEngine)
    {
        return getContractServices(service, css, null, startDate, endDate, dateEngine, false);
    }


    public static List<RCR_Contract_Service__c> getContractServices(RCR_Service__c service, 
                                                                    RCR_Contract_Scheduled_Service__c css, 
                                                                    List<RCR_Service_Order_Break_Period__c> breakPeriods, 
                                                                    Date startDate, 
                                                                    Date endDate, 
                                                                    ScheduleDateEngine dateEngine, 
                                                                    boolean doUpdate)
    {
        List<RCR_Contract_Service__c> contractServices = new List<RCR_Contract_Service__c>();
        List<RCR_Scheduled_Service_Breakdown_By_Rate__c> breakdownsByRate = new List<RCR_Scheduled_Service_Breakdown_By_Rate__c>();     
        Map<Date, RCR_Scheduled_Service_Exception__c> scheduledServiceExceptions = getScheduledServiceExceptions(css);          
        Decimal maxRate = 0.0;
        
        if(breakPeriods == null)
        {
            breakPeriods = [SELECT     ID,
                                        Start_Date__c,
                                        End_Date__c,
                                        RCR_Service_Order__c 
                            FROM        RCR_Service_Order_Break_Period__c
                            WHERE       RCR_Service_Order__c = :css.RCR_Contract__c];
        }       
        if(dateEngine == null)
        {
            dateEngine = new ScheduleDateEngine(css.Id, startDate, endDate);
        }
        
        Boolean isDateRangeService = css.Flexibility__c == APS_PicklistValues.RCRContractScheduledService_Flexibilty_Accumulative &&
                        dateEngine.Schedules[0].Number_of_weeks_in_period__c == APS_PicklistValues.Schedule_WeeksInPeriod_ServiceDateRange;
                        
        if(css.Use_Negotiated_Rates__c)
        {
            maxRate = css.Negotiated_Rate_Standard__c;
            
            RCR_Service_Rate__c negRate = new RCR_Service_Rate__c();
            negRate.Start_Date__c = css.Start_Date__c;
            negRate.End_Date__c = css.End_Date__c;
            negRate.Rate__c = css.Negotiated_Rate_Standard__c;
            negRate.Public_Holiday_Rate__c = css.Negotiated_Rate_Public_Holidays__c;
            if(!isDateRangeService)
            {
                processRate(negRate, service, css, breakPeriods, startDate, endDate, dateEngine, contractServices, breakdownsByRate, scheduledServiceExceptions);
            }
        }
        else    
        {
            for(RCR_Service_Rate__c rate : service.RCR_Service_Rates__r)
            {           
                if(!A2HCUtilities.dateRangesOverlap(css.Start_Date__c,  css.End_Date__c, rate.Start_Date__c, rate.End_Date__c))
                {
                    continue;
                }
                else
                {
                    if(isDateRangeService)
                    {
                        maxRate = rate.Rate__c > maxRate ? rate.Rate__c : maxRate;
                    }
                    else
                    {
                        processRate(rate, service, css, breakPeriods, startDate, endDate, dateEngine, contractServices, breakdownsByRate, scheduledServiceExceptions);
                    }
                }                           
            }
        }
        if(isDateRangeService)
        {                       
            decimal numberOfWeeks = (css.Start_Date__c.daysBetween(css.End_Date__c) + 1) / 7.0;         
            // should only be one Schedule
            for(Schedule__c s : dateEngine.Schedules)
            {
                decimal totalAmount = s.Quantity_Per_Period__c * maxRate;
                decimal amountPerWeek = (totalAmount / numberOfWeeks).setScale(2);
                decimal hoursPerWeek = (s.Quantity_Per_Period__c / numberOfWeeks) * 24;     
                contractServices.add(new RCR_Contract_Service__c(ContractNumber__c = css.RCR_Contract__r.Name,
                                                                             ContractServiceType__c = 'Scheduled',
                                                                             Service__c = service.Name,
                                                                             ServiceDescription__c = service.Description__c,
                                                                             StartDate__c = css.Start_Date__c,
                                                                             EndDate__c = css.End_Date__c,
                                                                             Qty__c = s.Quantity_Per_Period__c,
                                                                             Rate__c = maxRate,
                                                                             Amount_Per_Week__c = amountPerWeek,
                                                                             Hours_Per_Week__c = hoursPerWeek,
                                                                             Amount__c = totalAmount));
                breakdownsByRate.add(new RCR_Scheduled_Service_Breakdown_By_Rate__c(RCR_Contract_Scheduled_Service__c = css.Id,
                                                                                          Quantity__c = s.Quantity_Per_Period__c, 
                                                                                          Total__c = totalAmount,
                                                                                          Rate__c = maxRate,
                                                                                          Public_Holiday__c = false));
            }
        }   
        if(doUpdate)
        {
            DMLAsSystem.insertRecords(breakdownsByRate, true);
        }
        return contractServices;
    }

//    public static List<RCR_Service_Order_Payment__c> getPaymentsForBatch(String invoiceBatchID)
//    {
//        List<RCR_Service_Order_Payment__c> payments = [SELECT  Batch_Sent_Date__c,
//                        GST_Amount__c,
//                        Invoice_End_Date__c,
//                        Invoice_Start_Date__c,
//                        RCR_Invoice_Batch__c,
//                        RCR_Invoice__c,
//                        Statement_Number__c,
//                        RCR_Invoice__r.Invoice_Number__c,
//                        RCR_Invoice_Batch__r.Name,
//                        RCR_Service_Order__c,
//                        RCR_Service_Order__r.Name,
//                        RCR_Service_Order__r.Account__r.Name,
//                        RCR_Service_Order__r.RCR_CBMS_Contract__r.Name,
//                        Total__c,
////                        RCR_Invoice_Batch__r.Total_Ex_GST__c,
////                        RCR_Invoice_Batch__r.Total_GST__c,
////                        RCR_Invoice_Batch__r.Total_Inc_GST__c,
//                        RCR_Invoice_Batch__r.Approved_By__r.Name
//                FROM    RCR_Service_Order_Payment__c
//                WHERE   RCR_Invoice_Batch__c = :invoiceBatchID AND
//                        Processed_Date__c = null
//                ORDER BY RCR_Invoice__r.CreatedDate asc];
//            // temporary fix for non numeric service order nummbers
//            for(RCR_Service_Order_Payment__c p : payments)
//            {
//                p.RCR_Service_Order__r.Name = p.RCR_Service_Order__r.RCR_CBMS_Contract__r.Name;
//            }
//            return   payments;
//    }
    
    
    public static List<RCR_MP_Service_Order_Payment__c> getMasterpiecePaymentsForBatch(String invoiceBatchID)
    {
        return [SELECT  Id,
                        Invoice_Number__c,
                        Cost_Centre_Code__c,
                        Activity_Code__c,
                        Natural_Account_Code__c,
                        Statement_Number__c,
                        RCR_Invoice__c,
                        RCR_Invoice__r.Name,
                        RCR_Invoice__r.Invoice_Number__c,
                        RCR_Invoice__r.Account__r.Name,
                        RCR_Invoice__r.Account__r.RCR_Service_Provider_Code__c,
                        Batch_Sent_Date__c,
                        Invoice_Start_Date__c,
                        Invoice_End_Date__c,
                        Total__c,
                        GST_Amount__c,
                        RCR_Invoice_Batch__r.Name,
                        RCR_Invoice_Batch__r.Total_Ex_GST_MP__c,
                        RCR_Invoice_Batch__r.Total_GST_MP__c,
                        RCR_Invoice_Batch__r.Total_Inc_GST_MP__c,
                        RCR_Invoice_Batch__r.Approved_By__r.Name
                FROM    RCR_MP_Service_Order_Payment__c
                WHERE   RCR_Invoice_Batch__c = :invoiceBatchID
                ORDER BY RCR_Invoice__r.CreatedDate asc];
    }
    
    
    public static List<RCR_Invoice__c> getNumberOfInvoicesInBatch(string invoiceBatchID)
    {
        return [SELECT  Id
                FROM    RCR_Invoice__c
                WHERE   RCR_Invoice_Batch__c = :invoiceBatchID];
    }
    
    
    public static Attachment getMasterpieceAttachment(string invoiceBatchID)
    {
        for(Attachment a : [SELECT  a.Id,
                                    a.ParentId,
                                    a.Name,
                                    a.Description,
                                    a.ContentType,
                                    a.Body
                            FROM    Attachment a
                            WHERE   a.ParentId = :invoiceBatchID
                            AND     a.Description = 'MasterPieceExport'])
        {
            return a;
        }
        return null;
    }
    
    private static void processRate(RCR_Service_Rate__c rate, 
                                    RCR_Service__c service, 
                                    RCR_Contract_Scheduled_Service__c css, 
                                    List<RCR_Service_Order_Break_Period__c> breakPeriods,
                                    Date startDate, 
                                    Date endDate, 
                                    ScheduleDateEngine dateEngine, 
                                    List<RCR_Contract_Service__c> contractServices, 
                                    List<RCR_Scheduled_Service_Breakdown_By_Rate__c> breakdownsByRate, 
                                    Map<Date, RCR_Scheduled_Service_Exception__c> scheduledServiceExceptions)
    {       
        Decimal numberOfWeeks = 0.0;
        Decimal amountPerWeek = 0.0;
        Decimal totalAmount = 0.0;
        Decimal qty = 0.0;
        Decimal hoursPerWeek = 0.0;
        Integer daysPerWeek = 1;
        String[] days = new List<String>();
        Date tempStartDate = startDate;
        Date tempEndDate = endDate;
        
        RCR_Scheduled_Service_Breakdown_By_Rate__c standardRateBreakdown = new RCR_Scheduled_Service_Breakdown_By_Rate__c(RCR_Contract_Scheduled_Service__c = css.Id,
                                                                                                                          Quantity__c = 0.0, 
                                                                                                                          Total__c = 0.0,
                                                                                                                          Rate__c = rate.Rate__c,
                                                                                                                          Public_Holiday__c = false);
        
        RCR_Scheduled_Service_Breakdown_By_Rate__c publicHolidayRateBreakdown = new RCR_Scheduled_Service_Breakdown_By_Rate__c(RCR_Contract_Scheduled_Service__c = css.Id,
                                                                                                                               Quantity__c = 0.0, 
                                                                                                                               Total__c = 0.0,
                                                                                                                               Rate__c = rate.Public_Holiday_Rate__c,
                                                                                                                               Public_Holiday__c = true);
        standardRateBreakdown.Start_Date__c = rate.Start_Date__c < css.Start_Date__c ? css.Start_Date__c : rate.Start_Date__c;
        standardRateBreakdown.End_Date__c = rate.End_Date__c > css.End_Date__c ? css.End_Date__c : rate.End_Date__c;
        publicHolidayRateBreakdown.Start_Date__c = rate.Start_Date__c < css.Start_Date__c ? css.Start_Date__c : rate.Start_Date__c;
        publicHolidayRateBreakdown.End_Date__c = rate.End_Date__c > css.End_Date__c ? css.End_Date__c : rate.End_Date__c;
            
        if(service.Day_Of_Week__c != null && service.Day_Of_Week__c.contains(';'))
        {
            daysPerWeek = service.Day_Of_Week__c.split(';').size();
        }       
        if(startDate != null && endDate != null)
        {
            // Deterimine which Start Date applies.
            if(rate.Start_Date__c > tempStartDate)
            {
                tempStartDate = rate.Start_Date__c;
            }
            // Determine which End Date applies.
            if(rate.End_Date__c < tempEndDate)
            {
                tempEndDate = rate.End_Date__c;
            }
            if(tempStartDate != null && tempEndDate != null)
            {                       
                for(Date dt : dateEngine.ScheduleQuantityByDay.keySet())
                {   
                    if(A2HCUtilities.isDateBetween(dt, tempStartDate, tempEndDate))
                    {
                        boolean isBreak = false;                        
                        // Deterimine if the date is in a Break Period.
                        for(RCR_Service_Order_Break_Period__c breakPeriod : breakPeriods)
                        {
                            if(A2HCUtilities.isDateBetween(dt, breakPeriod.Start_Date__c, breakPeriod.End_Date__c))
                            {
                                isBreak = true;
                                break;
                            }
                        }                                               
                        // If the date is Public Holiday and Service Should not be provided on Public Holidays skip calculations.
                        if(isBreak ||
                                (PublicHolidays.containsKey(dt) &&
                                css.Public_holidays_not_allowed__c == APS_PicklistValues.RCRContractScheduledService_PublicHolidaysNotAllowed_UseNoPublicHolidays))
                        {
                            continue;
                        }                       
                        decimal qtyForDay = dateEngine.ScheduleQuantityByDay.get(dt);               
                        if(PublicHolidays.containsKey(dt) &&
                           rate.Public_Holiday_Rate__c != null &&
                           css.Public_holidays_not_allowed__c == APS_PicklistValues.RCRContractScheduledService_PublicHolidaysNotAllowed_UsePublicHolidayRates)
                        {                               
                            qty += qtyForDay;
                            totalAmount += (qtyForDay * rate.Public_Holiday_Rate__c);
                            publicHolidayRateBreakdown.Quantity__c += qtyForDay;
                            publicHolidayRateBreakdown.Total__c += (qtyForDay * rate.Public_Holiday_Rate__c);
                        }
                        else if(rate.Public_Holiday_Rate__c != null &&
                                css.Public_holidays_not_allowed__c == APS_PicklistValues.RCRContractScheduledService_PublicHolidaysNotAllowed_UseExceptions &&
                                scheduledServiceExceptions.containsKey(dt))
                        {                               
                            decimal publicHolidayQty = scheduledServiceExceptions.get(dt).Public_Holiday_Quantity__c;
                            decimal standardQty = scheduledServiceExceptions.get(dt).Standard_Quantity__c;

                            qty += (publicHolidayQty + standardQty);
                            totalAmount += ((publicHolidayQty * rate.Public_Holiday_Rate__c) + (standardQty * rate.Rate__c));
                            standardRateBreakdown.Quantity__c += standardQty;
                            standardRateBreakdown.Total__c += (standardQty * rate.Rate__c);
                            publicHolidayRateBreakdown.Quantity__c += publicHolidayQty;
                            publicHolidayRateBreakdown.Total__c += (publicHolidayQty * rate.Public_Holiday_Rate__c);
                        }
                        else
                        {                           
                            // Use Standard Rates
                            qty += qtyForDay;
                            totalAmount += (qtyForDay * rate.Rate__c);
                            standardRateBreakdown.Quantity__c += qtyForDay;
                            standardRateBreakdown.Total__c += (qtyForDay * rate.Rate__c);
                        }
                    }
                }               
                if(css.Public_holidays_not_allowed__c == APS_PicklistValues.RCRContractScheduledService_PublicHolidaysNotAllowed_UseExceptions)
                {
                    // Loop through the Scheduled Service Exceptions.
                    for(Date dt : scheduledServiceExceptions.keySet())
                    {
                        // If the Exception Date is not in the Schedule the values need to be added to the totals anyway. If
                        // the exception date is in the schedule it has already been counted in the previous code.
                        if(A2HCUtilities.isDateBetween(dt, tempStartDate, tempEndDate) && dateEngine.ScheduleQuantityByDay.get(dt) == null)
                        {
                            decimal publicHolidayQty = scheduledServiceExceptions.get(dt).Public_Holiday_Quantity__c;
                            decimal standardQty = scheduledServiceExceptions.get(dt).Standard_Quantity__c;
                            qty += (publicHolidayQty + standardQty);
                            totalAmount += ((publicHolidayQty * rate.Public_Holiday_Rate__c) + (standardQty * rate.Rate__c));

                            standardRateBreakdown.Quantity__c += standardQty;
                            standardRateBreakdown.Total__c += (standardQty * rate.Rate__c);
                            publicHolidayRateBreakdown.Quantity__c += publicHolidayQty;
                            publicHolidayRateBreakdown.Total__c += (publicHolidayQty * rate.Public_Holiday_Rate__c);
                        }
                    }
                }
                
                numberOfWeeks = (tempStartDate.daysBetween(tempEndDate) + 1) / 7.0;
                if(numberOfWeeks <= 0)
                {
                    amountPerWeek = 0.0;
                    hoursPerWeek = 0.0;
                }
                else
                {       
                    amountPerWeek = (totalAmount / numberOfWeeks).setScale(2);
                    hoursPerWeek = (qty / numberOfWeeks) * 24;
                }
            }
        }
        contractServices.add(new RCR_Contract_Service__c(ContractNumber__c = css.RCR_Contract__r.Name,
                                                         ContractServiceType__c = 'Scheduled',
                                                         Service__c = service.Name,
                                                         ServiceDescription__c = service.Description__c,
                                                         StartDate__c = tempStartDate,
                                                         EndDate__c = tempEndDate,
                                                         Qty__c = qty,
                                                         Rate__c = rate.Rate__c,
                                                         Amount_Per_Week__c = amountPerWeek,
                                                         Hours_Per_Week__c = hoursPerWeek,
                                                         Amount__c = totalAmount));
         if(standardRateBreakdown.Quantity__c > 0.0)
         {
            breakdownsByRate.add(standardRateBreakdown);
         }
         if(publicHolidayRateBreakdown.Quantity__c > 0.0)
         {
            breakdownsByRate.add(publicHolidayRateBreakdown);
         }
    }

    private static List<RCR_Contract_Service__c> getAllServices(String contractId)
    {
        List<RCR_Contract_Service__c> ContractServices = new List<RCR_Contract_Service__c>();
        for(RCR_Service__c ps : [   SELECT  cs.ID,
                                            cs.Name,
                                            cs.Day_Of_Week__c,
                                            cs.Description__c,
                                            (SELECT Name,
                                                    Start_Date__c,
                                                    End_Date__c,
                                                    Rate__c,
                                                    RCR_Service__c,
                                                    Public_Holiday_Rate__c
                                             FROM   RCR_Service_Rates__r
                                             ORDER BY Start_Date__c ASC),
                                            (SELECT r.Id,
                                                    r.Name,
                                                    r.Service_Code__c,
                                                    r.Flexibility__c,
                                                    r.Public_Holidays_Not_Allowed__c,
                                                    r.RCR_Contract__r.Name,
                                                    r.RCR_Contract__r.Level__c,
                                                    r.RCR_Contract__r.Start_Date__c,
                                                    r.RCR_Contract__r.End_Date__c,
                                                    r.RCR_Service__c,
                                                    r.RCR_Service__r.Name,
                                                    r.RCR_Service__r.Description__c,
                                                    r.Start_Date__c,
                                                    r.End_Date__c,
                                                    r.Original_End_Date__c,
                                                    r.Exception_Count__c,
                                                    r.Use_Negotiated_Rates__c,
                                                    r.Negotiated_Rate_Standard__c,
                                                    r.Negotiated_Rate_Public_Holidays__c
                                            FROM   RCR_Contract_Scheduled_Services__r r
                                            WHERE   r.RCR_Contract__r.Name = :contractId)
                                     FROM    RCR_Service__c cs
                                     WHERE ID IN(SELECT RCR_Service__c
                                                FROM    RCR_Contract_Scheduled_Service__c
                                                WHERE   RCR_Contract__r.Name = :contractId)])
        {
            for(RCR_Contract_Scheduled_Service__c css : ps.RCR_Contract_Scheduled_Services__r)
            {
                ScheduleDateEngine dateEngine = new ScheduleDateEngine(css.Id);
                
                ContractServices.addAll(getContractServices(ps, css, css.Start_Date__c, css.End_Date__c, dateEngine));
            }
        }

        for(RCR_Contract_Adhoc_Service__c cas : [SELECT s.RCR_Contract__r.Name,
                                                      s.RCR_Account_Program_Category__r.Name,
                                                      s.Start_Date__c,
                                                      s.End_Date__c,
                                                      s.Amount__c,
                                                      s.RCR_Account_Program_Category__c
                                               FROM   RCR_Contract_Adhoc_Service__c s
                                               WHERE  s.RCR_Contract__r.Name = :contractId])
        {
            Decimal numberOfWeeks;
            if(cas.Start_Date__c != null && cas.End_Date__c != null)
            {
                numberOfWeeks = (cas.Start_Date__c.daysBetween(cas.End_Date__c) + 1) / 7.0;
            }

            ContractServices.add(new RCR_Contract_Service__c(ContractNumber__c = cas.RCR_Contract__r.Name,
                                                             ContractServiceType__c = 'AdHoc',
                                                             Service__c = cas.RCR_Account_Program_Category__r.Name,
                                                             ServiceDescription__c = cas.RCR_Account_Program_Category__r.Name,
                                                             StartDate__c = cas.Start_Date__c,
                                                             EndDate__c = cas.End_Date__c,
                                                             Qty__c = null,
                                                             Rate__c = null,
                                                             Amount_Per_Week__c = cas.Amount__c / numberOfWeeks,
                                                             Hours_Per_Week__c = null,
                                                             Amount__c = cas.Amount__c));
        }
        return ContractServices;
    }

    private static Map<Date, RCR_Scheduled_Service_Exception__c> getScheduledServiceExceptions(RCR_Contract_Scheduled_Service__c css)
    {
        Map<Date, RCR_Scheduled_Service_Exception__c> scheduledServiceExceptions = new Map<Date, RCR_Scheduled_Service_Exception__c>();
        if(css.Exception_Count__c > 0 &&
            (css.RCR_Scheduled_Service_Exceptions__r == null || css.RCR_Scheduled_Service_Exceptions__r.isEmpty()))
        {
            for(RCR_Scheduled_Service_Exception__c ex : [SELECT Date__c,
                                                                Description__c,
                                                                Public_Holiday_Quantity__c,
                                                                Standard_Quantity__c
                                                         FROM   RCR_Scheduled_Service_Exception__c
                                                         WHERE  RCR_Contract_Scheduled_Service__c = :css.Id])
            {
                scheduledServiceExceptions.put(ex.Date__c, ex);
            }
        }
        else
        {
            for(RCR_Scheduled_Service_Exception__c sse :css.RCR_Scheduled_Service_Exceptions__r)
            {
                scheduledServiceExceptions.put(sse.Date__c, sse);
            }
        }
        
        return scheduledServiceExceptions;
    }

    

}