/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestRCRUploadSummaryController {

    static testMethod void myUnitTest()
    {
    	/*TestLoadData.loadRCRdata();

		RCR_Invoice_Upload__c upload = new RCR_Invoice_Upload__c();
		upload.Account__c = [SELECT ID FROM Account WHERE Name = 'test'].ID;
		upload.File_Name__c = 'test';
		upload.Date__c = Date.today();
		insert upload;
		
		RCR_Invoice__c invoice = [SELECT ID FROM RCR_Invoice__c WHERE Invoice_Number__c = '1000000000'];

		Test.setCurrentPage(new PageReference('/apex/RCRUploadSummary'));
		RCRUploadSummaryController ext = new RCRUploadSummaryController();

		ApexPages.currentPage().getParameters().put('id', upload.ID);
		User u = [SELECT	ID
				FROM		User
				WHERE		AccountID != null AND
							IsActive = true
				LIMIT 1];

		System.runAs(u)
		{
			ext = new RCRUploadSummaryController();

			system.debug(ext.HelpItems);
			//ext.FileUpload = null;
			//ext.FileUploadId = upload.ID;

		}

		ext = new RCRUploadSummaryController();
		system.debug(ext.FileUpload);

		ApexPages.currentPage().getParameters().put('retURL', 'blah');
		ext.cancel();

		RCRUploadSummaryController.reconcileInvoice(invoice.Id);

		ext.Invoices = null;
		system.debug(ext.Invoices);

		ext.processPayment();

		ext.reconcileAll();*/
    }
}