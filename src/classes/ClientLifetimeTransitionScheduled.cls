public class ClientLifetimeTransitionScheduled implements Schedulable
{
    public void execute(SchedulableContext SC)
    {
        for(Referred_Client__c client : [SELECT ID
                                        FROM    Referred_Client__c
                                        WHERE   Current_Status__c = 'Lifetime assessed, transitioning out' AND
                                                Current_Status_Date__c <= :Date.today()
                                        LIMIT 100])
        {
            try
            {
                client.Current_Status__c = 'Not lifetime participant';
                update client;
            }
            catch(Exception ex)
            {
                //TODO: What to do here??
            }
        }
    }
}