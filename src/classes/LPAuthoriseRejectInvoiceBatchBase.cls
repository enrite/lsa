public abstract class LPAuthoriseRejectInvoiceBatchBase implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts
{
    public LPAuthoriseRejectInvoiceBatchBase(Set<ID> lpItemsToProcess, String action)
    {
        this.lpItemsToProcess = lpItemsToProcess;
        this.action = action;
    }

    public LPAuthoriseRejectInvoiceBatchBase(Set<ID> lpItemsToReject,  Set<ID> lpItemsToApprove, String action)
    {
        this.lpItemsToProcess = lpItemsToReject;
        // items to approve will processed in finish
        this.lpItemsToApprove = lpItemsToApprove;
        this.action = action;
    }


    private Set<ID> lpItemsToProcess = new Set<ID>();
    private Set<ID> lpItemsToApprove = new Set<ID>();
    private String action;

    public Database.QueryLocator start(Database.BatchableContext param1)
    {
        return Database.getQueryLocator([SELECT ID,
                                                (
                                                        SELECT ID
                                                        FROM RCR_Invoice_Items__r
                                                        WHERE ID IN :lpItemsToProcess
                                                )
                                        FROM RCR_Invoice__c
                                        WHERE ID IN (SELECT RCR_Invoice__c
                                                FROM RCR_Invoice_Item__c
                                                WHERE ID IN :lpItemsToProcess)]);
    }

    public void execute(Database.BatchableContext param1, List<RCR_Invoice__c> scope)
    {
        LPRESTInterface lpInterface = new LPRESTInterface();
        for(RCR_Invoice__c inv : scope)
        {
            lpInterface.authorizeInvoice(inv.RCR_Invoice_Items__r, action);
        }
        lpInterface.saveRecords();
    }

    public void finish(Database.BatchableContext param1)
    {
        if(action == LPConstants.CLAIMSTATUS_REJECTED && !lpItemsToApprove.isEmpty())
        {
            LPAuthoriseBatch authBatch = new LPAuthoriseBatch(lpItemsToApprove);
            Database.executeBatch(authBatch, 20);
        }
    }
}