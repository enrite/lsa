public with sharing class DelegatedApprovalController
{
	public DelegatedApprovalController()
	{
		initialise();
	}
	private transient Map<String, String> objNamesByPrefix;
	private transient List<ProcessInstanceWorkItem> delApprovalItems;
	
	public Map<String, String> ObjectNamesByPrefix 
	{
		get
		{
			return objNamesByPrefix;
		}
	}
	
	public List<ProcessInstanceWorkItem> DelegatedApprovalItems
	{
		get
		{
			return delApprovalItems;
		}
	}
	
	private void initialise()
	{
		Set<sObjectType> sObjTypes = new Set<sObjectType>();
		objNamesByPrefix = new Map<String, String>();
		delApprovalItems = new List<ProcessInstanceWorkItem>();
		
		List<User> delegateForUsers = [SELECT ID
									FROM	User
									WHERE	DelegatedApproverId = :UserInfo.getUserId()];
		for(ProcessInstanceWorkItem piwi : [SELECT ID,
													ProcessInstanceId,
													CreatedDate,
													OriginalActorId,
													OriginalActor.Name,  
													ActorId, 
													Actor.Name,
													ProcessInstance.Status,
													ProcessInstance.TargetObjectId, 
													ProcessInstance.TargetObject.Name														
											FROM	ProcessInstanceWorkItem])
		{						
			for(User u : delegateForUsers)
			{
				if(piwi.ActorID == u.ID) 
				{
					sObjTypes.add(piwi.ProcessInstance.TargetObjectId.getSObjectType());
					delApprovalItems.add(piwi);
				}
			}
		}
		
		for(sObjectType sObjTyp : sObjTypes)
		{
			DescribeSObjectResult result = sObjTyp.getDescribe();
			objNamesByPrefix.put(result.getKeyPrefix(), result.getLabel());
		}
	}
}