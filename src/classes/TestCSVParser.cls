/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestCSVParser {

    static testMethod void myUnitTest()
    {
        String csv = 'Program,Contract Number,Client Name,Invoice Date,Invoice Start Date,Invoice End Date,Invoice Number,Service Date,Code,Qty,Rate,GST %,Total Ex Gst,Total inc GST\n' +
						'Home Link SA,72515,,01/08/2011,01/07/2011,31/07/2011,62222,28/07/2011,HL FT LEVEL 4,1,82.95,8.30,82.95,91.25\n' +
						'Home Link SA,72515,,01/08/2011,01/07/2011,31/07/2011,62222,29/07/2011,HL FT LEVEL 8,1,82.95,8.30,82.95,91.25\n' +
						'Home Link SA,72515,,01/08/2011,01/07/2011,31/07/2011,62222,30/07/2011,HL FT LEVEL 2,1,82.95,8.30,82.95,91.25\n' +
						'"Home Link SA,72515",,"01/08/2011",01/07/2011,31/07/2011,62222,31/07/2011,HL FT LEVEL 2,1,82.95,8.30,82.95,"91.25"\n' +
						'Home Link SA,72515,,01/08/2011,01/07/2011,31/07/2011,62222,31/07/2011,Respite,1,82.95,8.30,82.95,91.25\n' +
						'Home Link SA,72515,,01/08/2011,01/07/2011,31/07/2011,62222,31/07/2011,Respite,1,82.95,8.30,82.95,91.25\n' +
						'Home Link SA,72515,,01/08/2011,01/07/2011,31/07/2011,62222,31/07/2011,Respite,1,82.95,8.30,82.95,91.25\n' +
						'Home Link SA,72515,,01/08/2011,01/07/2011,31/07/2011,62222,31/07/2011,Respite,1,82.95,8.30,82.95,91.25\n' +
						'Home Link SA,72515,,01/08/2011,01/07/2011,31/07/2011,62222,31/07/2011,Respite,1,82.95,8.30,82.95,91.25\n' +
						'Home Link SA,72515,,01/08/2011,01/07/2011,31/07/2011,62222,31/07/2011,RespiteZ,1,42.01,5.1,8.46,111.2';

		CSVParser p = new CSVParser(csv, true);
		p = new CSVParser(csv, true, 5 );

		while(p.ReadLine() != null && !p.ReadLine().isEmpty())
		{
			p.ReadLine();
		}


    }
}