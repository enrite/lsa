public class APSRecordTypes 
{
    public static String RCRServiceOrder_Amendment = 'Amendment';  
    public static String RCRServiceOrder_NewRequest = 'New Request';
    public static String RCRServiceOrder_NewRequestCBMS = 'New Request - CBMS';
    public static String RCRServiceOrder_Rollover = 'Rollover';
    public static String RCRServiceOrder_ServiceOrder = 'Service Order';  
    public static String RCRServiceOrder_ServiceOrderCBMS = 'Service Order - CBMS';
    
    public static String Contact_ServiceProvider = 'Service Provider';
    
    public static String RCRInvoice_Created = 'Created';
    public static String RCRInvoice_Submitted = 'Submitted';
    
    public static String NDIS_ServiceContract_ServContract = 'Service Contract';
    public static String NDIS_ServiceContract_Quote = 'Quote';
    
    public static String ENUReferral_ReferralInProgress = 'Referral - In Progress';    
    public static String ENUReferral_ReferralComplete = 'Referral - Complete';
    public static String ENUReferral_AssessmentInProgress = 'Assessment - In Progress';
    public static String ENUReferral_AssessmentComplete = 'Assessment - Complete';

    public static String Client_LSAParticipant = 'LSA Participant';
    public static String Client_ManagedClient = 'Managed Client';
                                                               
    public static String PlanApproval_ChiefExecutiveApproval = 'Chief Executive Approval';

}