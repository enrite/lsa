@isTest
public class TestInvoiceManagementController
{
    static testMethod void myUnitTest()
    {
        TestLoadData.loadRCRdata();

        Test.StartTest();

        Test.setCurrentPage(Page.InvoiceManagement);
        //ApexPages.CurrentPage().getParameters().put('pg', 'AcceptedLifetimePDF');

        InvoiceManagementController imc = new InvoiceManagementController();
        String fromDate = imc.DateFrom;
        String toDate = imc.DateTo;

        imc.SelectedStatus = '-- Al --';
        imc.SelectedUser = '-- Al --';
        imc.SelectedParticipant = 'Bob';
        List<Email_To_Invoice__c> eti = imc.EmailInvoices;
        List<RCR_Invoice__c> inv = imc.ActivityStatements;
        List<Adhoc_Payment__c> ap = imc.AdhocPayments;
        List<SelectOption> so = imc.InvoiceStatuses;
        List<SelectOption> su = imc.Users;

        /*Referred_Client__c rc = TestLoadData.getTestClient();
        system.debug(rc.Name);
        Adhoc_Payment__c adhoc = new Adhoc_Payment__c(Participant__c = rc.Id);
        system.debug(adhoc.Participant__r.Name);
        ap.add(adhoc);

        List<SelectOption> sa = imc.AdhocParticipants;*/

        imc.resetAdhocPayments();

        ApexPages.CurrentPage().getParameters().put('status', 'Loaded');
        InvoiceManagementController imc2 = new InvoiceManagementController();
        List<RCR_Invoice__c> inv2 = imc2.ActivityStatements;

        Test.StopTest();
    }
}