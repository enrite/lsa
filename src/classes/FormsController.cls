public class FormsController
{
    public Map<String, RecordType> RecordTypes
    {
        get
        {
            if (RecordTypes == null)
            {
                RecordTypes = new Map<String, RecordType>();
            
                for (RecordType rt : [SELECT Id,
                                             Name,
                                             DeveloperName
                                      FROM RecordType
                                      WHERE sObjectType = 'Form__c'])
                {
                    RecordTypes.put(rt.DeveloperName, rt);
                }                                      
            }
            return RecordTypes;
        }
        set;    
    }

}