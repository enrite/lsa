public with sharing class RCRRolloverButtons extends A2HCPageBase
{
    public RCRRolloverButtons(ApexPages.StandardController controller)
    {
        Action = getParameter('action');
        id rolloverID = controller.getId();
        if(rolloverID != null)
        {
        	Rollover = RCRContractExtension.getContract(rolloverID);
			RolloverComment = Rollover.Roll_Over_Comment__c;
        }
    }
    
    @TestVisible
    public string Action
    {
    	get;
    	private set;
    }
    
    public RCR_Contract__c Rollover
    {
        get;
        set;
    }
    
    public string RolloverComment
    {
    	get;
    	set;
    }
    
    public boolean HasErrors
    {
    	get
    	{
    		return ApexPages.hasMessages();
    	}
    }
    
    public List<RolloverWrapper> ClientRollovers
    {
    	get
    	{
	    	if(ClientRollovers == null)
	    	{
    			List<Integer> fiscalYears = new List<Integer>();
        		fiscalYears = A2HCUtilities.getSFFiscalYears(Rollover.Start_Date__c, Rollover.End_Date__c);

    			ClientRollovers = new List<RolloverWrapper>();
    			for(RCR_Contract__c c : [SELECT Id,
    											Name,
    											Status__c,
    											Rollover_Status__c,
    											Client__c,
    											Client__r.Name,
    											Client__r.CCMS_ID__c,
    											Funding_Source__c,
    											New_Request_Type__c,
    											Start_Date__c,
    											End_Date__c,
    											Type__c,
    											Total_Service_Order_Cost__c,
    											Allocation_Total_Approved__c,
    											Allocation_Total_Not_Approved__c,
    											Account__c,
    											Account__r.Name,
    											Service_Coordinator__c,
												Director__c,
												Regional_Manager__c,
												Team_Manager__c
    									 FROM	RCR_Contract__c
									 	WHERE	Record_Type_Name__c = :APSRecordTypes.RCRServiceOrder_Rollover
									 	AND		Client__c = :Rollover.Client__c
									 	AND		FISCAL_YEAR(Start_Date__c) IN :fiscalYears
        								ORDER BY CreatedDate desc])
				{
					ClientRollovers.add(new RolloverWrapper(c));
				}
    		}
    		return ClientRollovers;
    	}
    	private set;
    }

    public PageReference updateRolloverStatus()
    {
        try
        {
            system.debug(Action);
            PageReference pg = new ApexPages.Standardcontroller(rollover).view();
            
            if(Rollover.Id != null)
            {           
                if(Action == 'toggle')
                {                         
                    if(Rollover.Status__c == APS_PicklistValues.RCRContract_Status_PendingApproval)
                	{
                		return A2HCException.formatException('This record is Pending Approval and you cannot change the Rollover Status at this time.');
                	}
                    if(Rollover.Rollover_Status__c != APS_PicklistValues.RCRContract_RolloverStatus_RolloverNotRequired)
                    {
                        Rollover.Rollover_Status__c = APS_PicklistValues.RCRContract_RolloverStatus_RolloverNotRequired;
                    }
                    else
                    {
                        Rollover.Rollover_Status__c = APS_PicklistValues.RCRContract_ROlloverStatus_PendingBrokerageReview;
                    }
                    
                }
                else if(Action == 'assignToBrokerage')
                {
                	if(Rollover.Status__c == APS_PicklistValues.RCRContract_Status_PendingApproval)
                	{
                		return A2HCException.formatException('This record is Pending Approval and cannot be re-assigned at this time.');
                	}
                	rollover.Rollover_Status__c = APS_PicklistValues.RCRContract_RolloverStatus_PendingAdjustment;
                }
                else if(Action == 'adjust' || Action == 'coordinatorReview')
                {
                    return null;
                }
            }
            update Rollover;
            return pg;
        }
        catch(Exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }
    
    public PageReference submitForAllocationApproval()
    {
    	Savepoint sp = Database.setSavePoint();
        try
        {
        	if(Rollover.Destination_Record__c != null)
	        {
	            return A2HCException.formatException('Rollover has already been completed.');
	        }
	        if(Rollover.Rollover_Status__c == APS_PicklistValues.RCRContract_RolloverStatus_RolloverNotRequired)
	        {
	        	return A2HCException.formatException('This Rollover\'s Status has been set to Rollover Not Required.');
	        }
	        if(!isValid(false))
			{
				return null;
			}
//        	List<IF_Personal_Budget__c> topUpAllocations = [SELECT  ID
//		                                                   FROM     IF_Personal_Budget__c i
//		                                                   WHERE    RCR_Service_Order__c = :rollover.Id
//		                                                   AND      Status__c = :APS_PicklistValues.IFAllocation_Status_New];
//            if(!topUpAllocations.isEmpty())
//            {
//            	Boolean errors = false;
//				for(Approval.ProcessResult result : submitRecordsForApproval(topUpAllocations, true))
//				{
//					if(!result.isSuccess())
//	                {
//	                    A2HCException.formatException(result);
//	                    errors = true;
//	                }
//				}
//				if(errors)
//				{
//					Database.rollback(sp);
//					return null;
//				}
//				Rollover.Submitted_for_Rollover_Top_Up__c = true;
//        		update Rollover;
//        		Approval.ProcessResult result = submitForApproval(Rollover.ID);
//				if(!result.isSuccess())
//                {
//                    A2HCException.formatException(result);
//                    Database.rollback(sp);
//					return null;
//				}
//				return (new ApexPages.Standardcontroller(rollover).view());
//            }
        	if(!validForServiceOrderApproval())
        	{            	
        		Database.rollback(sp);
				return null;
        	}
        	Approval.ProcessResult result = submitForApproval(rollover.Id);
            if(!result.isSuccess())
            {
                return A2HCException.formatException(result);
            }
            return (new ApexPages.Standardcontroller(rollover).view()); 
        }
        catch(Exception ex)
        {
            return A2HCException.formatExceptionAndRollback(sp, ex);
        }
    }
    
    public PageReference submitForReview()
    {
    	try
    	{
    		boolean hasValidationErrors = false;
    		List<RCR_Contract__c> rollovers = new List<RCR_Contract__c>();
    		for(RolloverWrapper r : ClientRollovers)
    		{
    			if(!r.DisplayCheckboxes)
    			{
    				continue;
    			}
    			if((r.BrokerageReviewed || r.PendingCoordinator) &&
    				   string.isBlank(r.Rollover.Service_Coordinator__c))
                {
                    A2HCException.formatException('Rollover ' + r.Rollover.Name + ': Service Coordinator is required.');
                    hasValidationErrors = true;
                }
    			if(r.PendingCoordinator != null && r.PendingCoordinator)
    			{
    				r.Rollover.Rollover_Status__c = APS_PicklistValues.RCRContract_RolloverStatus_PendingCoordReview;
    				rollovers.add(r.Rollover);
    			}
    			else if(r.BrokerageReviewed != null && r.BrokerageReviewed)
    			{
    				r.Rollover.Rollover_Status__c = APS_PicklistValues.RCRContract_RolloverStatus_BrokerageReviewed;
    				rollovers.add(r.Rollover);
    			}
    		}
    		if(hasValidationErrors)
    		{
    			return null;
    		}
    		if(rollovers.size() > 0)
    		{
    			update rollovers;
    		}
    		else
    		{
    			return A2HCException.formatException('You must select at least one Rollover for Service Coordinator Review.');
    		}
    		PageReference pg = new ApexPages.Standardcontroller(rollover).view();
            return pg;
    	}
    	catch(Exception ex)
    	{
    		return A2HCException.formatException(ex);
    	}
    }
    
    public PageReference submitForAdjustment()
    {
    	try
    	{
    		if(string.isBlank(RolloverComment))
            {
                return A2HCException.formatException('Rollover Comment is required when \'Request Changes\' button is clicked.'); 
            }
            Rollover.Roll_Over_Comment__c = RolloverComment;
            Rollover.Rollover_Status__c = APS_PicklistValues.RCRContract_RolloverStatus_PendingAdjustment;
            update Rollover;
    		PageReference pg = new ApexPages.Standardcontroller(rollover).view();
            return pg;
    	}
    	catch(Exception ex)
    	{
    		return A2HCException.formatException(ex);
    	}
    }
    
    public PageReference cancelOverride()
    {
    	try
    	{
    		PageReference pg = new ApexPages.Standardcontroller(rollover).view();
            return pg;
    	}
    	catch(Exception ex)
    	{
    		return A2HCException.formatException(ex);
    	}
    }
    
    private Boolean isValid(Boolean validateTeam)
    {
    	
        Boolean valid = isRequiredFieldValid(rollover.Start_Date__c, 'Start Date', true);
		valid = isRequiredFieldValid(rollover.Original_End_Date__c, 'End Date', valid);
		valid = isRequiredFieldValid(rollover.Level__c, 'Level', valid);
		valid = isRequiredFieldValid(rollover.Type__c, 'Type', valid);
		valid = isRequiredFieldValid(rollover.Funding_Source__c, 'Funding Source', valid);
		valid = isRequiredFieldValid(rollover.Service_Coordinator__c, 'Service Coordinator', valid);
		valid = isRequiredFieldValid(rollover.Team_Manager__c, 'Team Manager', valid);
		valid = isRequiredFieldValid(rollover.Director__c, 'Director', valid);
		valid = isRequiredFieldValid(rollover.Regional_Manager__c, 'Regional Manager', valid);
		if(!valid)
		{
		 	return false;
		}
        if(Rollover.Allocation_Total_Not_Approved__c > 0 && Rollover.Funding_Source__c == APS_PicklistValues.RCRContract_FundingSource_UseOfAllocation)
        {
            return A2HCException.formatValidationException('Funding Source must not be Use of Allocation if there are unapproved Allocation.');
        }
        return true;
    }
    
    @TestVisible
    private Boolean validForServiceOrderApproval()
    {
    	if(!isValid(true))
    	{
    		return false;
    	}
    	Rollover = RCRContractExtension.getContract(Rollover.ID);
    	if(Rollover.Status__c == APS_PicklistValues.RCRContract_RolloverStatus_PendingApproval ||
    			Rollover.Status__c == APS_PicklistValues.RCRContract_Status_AllocationPendingApproval)
    	{
    		return A2HCException.formatValidationException('Rollover is currently pending approval.');
    	}
//        String errMsg = ServiceOrderFuture.checkServiceOrderAllocation(Rollover, true);
//        if(String.isNotBlank(errMsg))
//        {
//        	return A2HCException.formatValidationException(errMsg);
//        }
        List<RCR_Funding_Detail__c> fundingDetails = [SELECT	ID,
	 	 														Maximum_Amount__c,
	 	 														Cost_Centre__c 
 														FROM	RCR_Funding_Detail__c
 														WHERE	RCR_Service_Order__c = :rollover.ID];
 		if(fundingDetails.isEmpty())
 		{
 		 	return A2HCException.formatValidationException('At least one Funding Detail is required.');
 		}
 		decimal totalFundingDetailsAmt = Rollover.Funding_Details_Maximum_Amount__c;
 		if(fundingDetails.size() == 1 &&
 					fundingDetails[0].Cost_Centre__c == RCR_Cost_Centre_Code__c.getInstance('ROLLOVER').Code__c && 
 					fundingDetails[0].Maximum_Amount__c == 0)
 		{
 		 	fundingDetails[0].Maximum_Amount__c = rollover.Total_Service_Order_Cost__c;
 		 	totalFundingDetailsAmt = rollover.Total_Service_Order_Cost__c;
	 	 	update fundingDetails[0];
 		}
	 	else if(totalFundingDetailsAmt > Rollover.Total_Service_Order_Cost__c)
        {
        	return A2HCException.formatValidationException('Total Funding Detail amount must be less than or equal to the Rollover Total Cost.');
        }
        return true;
    }
    
    public class RolloverWrapper
    {
    	public RolloverWrapper(RCR_Contract__c pRollover)
    	{
    		Rollover = pRollover;
    	}
    	
    	public boolean BrokerageReviewed
    	{
    		get;
    		set;
    	}
    	
    	public boolean PendingCoordinator
    	{
    		get;
    		set;
    	}
    	
    	public boolean DisplayCheckboxes
    	{
    		get
    		{
    			return Rollover.Rollover_Status__c != APS_PicklistValues.RCRContract_RolloverStatus_BulkApproved && 
    						Rollover.Rollover_Status__c != APS_PicklistValues.RCRContract_RolloverStatus_Complete && 
    						Rollover.Rollover_Status__c !=  APS_PicklistValues.RCRContract_RolloverStatus_RolloverNotRequired &&
    						Rollover.Status__c != APS_PicklistValues.RCRContract_Status_Approved &&
    						Rollover.Status__c !=  APS_PicklistValues.RCRContract_Status_PendingApproval &&
    						Rollover.Status__c !=  APS_PicklistValues.RCRContract_Status_AllocationPendingApproval;
    		}
    	}

    	public RCR_Contract__c Rollover
    	{
    		get;
    		set;
    	}
    }
}