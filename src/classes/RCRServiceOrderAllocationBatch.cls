global class RCRServiceOrderAllocationBatch 
//    extends RCRServiceOrderCloneBase
    implements Database.Batchable<sObject>, Database.Stateful
{ 
//    public List<Financial_Year__c> FinancialYears
//    {
//        get
//        {
//            if(FinancialYears == null)
//            {
//                FinancialYears = new List<Financial_Year__c>();
//                for(Financial_Year__c fy : [SELECT  ID,
//                                                    Name,
//                                                    Start_Date__c
//                                            FROM    Financial_Year__c
//                                            WHERE   Start_Date__c >= :(Date.newInstance(2012, 7, 1)) AND
//                                                    Start_Date__c <= :(Date.newInstance(2014, 7, 1))
//                                            ORDER BY Start_Date__c])
//                {
//                    FinancialYears.add(fy);
//                }
//            }
//            return FinancialYears;
//        }
//        set;
//    }
//
//    public Set<ID> AllocationIdsForApproval
//    {
//        get
//        {
//            if(AllocationIdsForApproval == null)
//            {
//                AllocationIdsForApproval = new Set<ID>();
//            }
//            return AllocationIdsForApproval;
//        }
//        set;
//    }
//
//    public decimal NextYearAdjustment
//    {
//        get;
//        set;
//    }
//
//    public decimal FollowingYearAdjustment
//    {
//        get;
//        set;
//    }
    
    global Database.QueryLocator start(Database.BatchableContext batch)
    {
        // need to save the service order ids for finaliseBatch()
//        for(RCR_Contract__c serviceOrder : [SELECT  ID
//                                            FROM    RCR_Contract__c
//                                            WHERE   Schedule__c = :ScheduleID AND
//                                                    RecordType.Name = :APSRecordTypes.RCRServiceOrder_Rollover])
//        {
//            ServiceOrderIds.add(serviceOrder.ID);
//        } system.debug('debug1:' + ServiceOrderIds); system.debug('debug2:' + ScheduleID);
//        return Database.getQueryLocator([SELECT     ID,
//                                                    Name,
//                                                    Total_Service_Order_Cost__c,
//                                                    Client__c,
//                                                    LastModifiedByID,
//                                                    Source_Record__r.Total_Service_Order_Cost__c,
//                                                    (SELECT ID
//                                                    FROM    IF_Allocations__r
//                                                    WHERE   Financial_Year__c = :FinancialYears[0].ID)
//                                            FROM    RCR_Contract__c
//                                            WHERE   ID IN :ServiceOrderIds]);
        return Database.getQueryLocator([SELECT     ID
                                            FROM    RCR_Contract__c
                                                    LIMIT 0]);
                            
    }

    global void execute(Database.BatchableContext batch, List<RCR_Contract__c> serviceOrders)
    {
//        List<Batch_Log__c> batchLogs = new List<Batch_Log__c>();
//        if(FinancialYears.size() != 3)
//        {
//            throw new A2HCException('Financial years not defined.');
//        }
//        List<ID> finYearIds = new List<ID>{FinancialYears[0].ID, FinancialYears[1].ID, FinancialYears[2].ID};
//
//        for(RCR_Contract__c serviceOrder : serviceOrders)
//        {
//            System.Savepoint sp = Database.setSavepoint();
//            try
//            {
//                createAllocations1213(serviceOrder, batchLogs, batch);
//                UserIds.add(serviceOrder.LastModifiedByID);
//            }
//            catch(Exception ex)
//            {
//                handleError(serviceOrder, null, sp, batchLogs, batch, ex, '');
//            }
//        }
//        insert batchLogs;
    }

    global void finish(Database.BatchableContext info)
    {   
//        ServiceOrderFuture.submitForApprovalImmediate(AllocationIdsForApproval);
//        finaliseBatch(info, 'Rollover_Allocation_Complete');
    }
    
//    // this is for initial allocation for first time rollovers
//    // not to be used after the 2012/2013 to 2013/104 rollover
//    private void createAllocations1213(RCR_Contract__c serviceOrder,
//                                    List<Batch_Log__c> batchLogs,
//                                    Database.BatchableContext batch)
//    {
//        // delete any 2013/2014 and 2014/2015 allocations from any previous runs
//        delete[SELECT   ID
//                FROM    IF_Personal_Budget__c
//                WHERE   RCR_Service_Order__c = :serviceOrder.ID];
//
//        List<IF_Personal_Budget__c> newAllocations = new List<IF_Personal_Budget__c>();
//        for(Integer i = 0; i < FinancialYears.size() ; i++)
//        {
//            decimal adjustment = null;
//            String comment = null;
//            if(i == 0)
//            {
//                adjustment = 0.0;
//                comment = 'Baseline Allocation';
//            }
//            else if (i == 1 && NextYearAdjustment != null)
//            {
//                adjustment = NextYearAdjustment;
//                comment = 'Indexed allocation from 2012/2013. Indexation of ' + NextYearAdjustment.toPlainString() + '% was used';
//            }
//            else if (i == 2 && FollowingYearAdjustment != null)
//            {
//                adjustment = FollowingYearAdjustment;
//                comment = 'Indexed allocation from 2012/2013. Indexation of ' + FollowingYearAdjustment.toPlainString() + '% was used';
//            }
//            if(adjustment != null)
//            {
//                IF_Personal_Budget__c newAllocation = new IF_Personal_Budget__c(Client__c = serviceOrder.Client__c, RCR_Service_Order__c = serviceOrder.ID);
//                newAllocation.Financial_Year__c = FinancialYears[i].ID;
//                newAllocation.Date_Allocated__c = FinancialYears[i].Start_Date__c;
//                newAllocation.Allocation_Type__c = APS_PicklistValues.IFAllocation_Type_Recurrent;
//                newAllocation.Reason__c = APS_PicklistValues.IFAllocation_Reason_Rollover;
//                newAllocation.Allocation_Source__c = APS_PicklistValues.IFAllocation_Source_IndividualSupport;
//                newAllocation.Status__c = APS_PicklistValues.IFAllocation_Status_New;
//                newAllocation.Approval_Criteria__c = APS_PicklistValues.IFAllocation_ApprovalCriteria_AutomaticApprove;
//                newAllocation.Amount__c = serviceOrder.Total_Service_Order_Cost__c * (1.0 + (adjustment / 100.0));
//                newAllocation.FYE_Amount__c = newAllocation.Amount__c;
//                newAllocation.Comment__c = comment;
//                newAllocations.add(newAllocation);
//            }
//        }
//        insert newAllocations;
//
//        // set up relationship history
//        for(IF_Personal_Budget__c allocation : newAllocations)
//        {
//            AllocationIdsForApproval.add(allocation.ID);
//            if(allocation.Financial_Year__c == FinancialYears[1].ID)
//            {
//                allocation.Rolled_Over_From__c = newAllocations[0].ID;
//            }
//            if(allocation.Financial_Year__c == FinancialYears[2].ID &&
//                    newAllocations.size() >= 2 &&
//                    newAllocations[1].Financial_Year__c == FinancialYears[1].ID)
//            {
//                allocation.Rolled_Over_From__c = newAllocations[1].ID;
//            }
//        }
//        update newAllocations;
//        addBatchLog(serviceOrder, batchLogs, batch, newAllocations.size().format() + ' Allocations created', null);
//    }
//
//    @IsTest//(SeeAllData=true)
//    static void test1()
//    {
//        TestLoadData.loadRCRData();
//
//        Test.StartTest();
//
//        List<RCR_Contract__c> serviceOrders = new List<RCR_Contract__c>();
//
//        for(RCR_Contract__c so : [SELECT    ID,
//                                            Schedule__c
//                                  FROM      RCR_Contract__c
//                                  /*WHERE     Name = 'TEST Service Order'*/
//                                  LIMIT 1])
//        {
//            so.RecordTypeId = A2HCUtilities.getRecordType('RCR_Contract__c', APSRecordTypes.RCRServiceOrder_Rollover).ID;
//            so.Schedule__c = 'MYUNITTEST';
//            serviceOrders.add(so);
//        }
//system.debug('debug0:' + serviceOrders);
//        update serviceOrders;
//
//        RCRServiceOrderAllocationBatch batch = new RCRServiceOrderAllocationBatch();
//        batch.ScheduleID = 'MYUNITTEST';
//        ID batchprocessid = Database.executeBatch(batch, 1);
//
//        Test.StopTest();
//    }
}