@isTest
private class TestRCRMasterpieceBatchReport 
{
    static testMethod void myUnitTest() 
    {
        RCR_Invoice_Batch__c batch = new RCR_Invoice_Batch__c();
        batch.MP_File_Name__c = 'blah.txt';
        insert batch;
        
        RCR_MP_Service_Order_Payment__c payment = new RCR_MP_Service_Order_Payment__c();
        payment.Total__c = 150.00;
        payment.GST_Amount__c = 15.00;
        payment.RCR_Invoice_Batch__c = batch.Id;
        insert payment;
        
        batch = [SELECT Total_Ex_GST_MP__c,
                        Total_GST_MP__c,
                        Total_Inc_GST_MP__c,
                        MP_File_Name__c
                 FROM   RCR_Invoice_Batch__c 
                 WHERE  Id = :batch.Id];
        
        system.assert(batch.Total_Ex_GST_MP__c == 150.0);
        system.assert(batch.Total_GST_MP__c == 15.0);
        system.assert(batch.Total_Inc_GST_MP__c == 165.0);
        system.assert(batch.MP_File_Name__c == 'blah.txt');
        
        Test.setCurrentPage(Page.RCRMasterpieceReconcilliationReport);
        ApexPages.currentPage().getParameters().put('id', batch.ID);
        
        RCRMasterpieceBatchReport batchRpt = new RCRMasterpieceBatchReport(new ApexPages.StandardController(batch));
        system.debug(batchRpt.RunDate);
        
        batchRpt.forceTestError = true;  
        batchRpt.runReport(batch.Id);
        
        batchRpt = new RCRMasterpieceBatchReport(batch.ID);
        system.assert(batchRpt.RunDate != null);
        system.assert(batchRpt.InvoiceCount != null);
        system.assert(batchRpt.VendorCount != null);
    }
}