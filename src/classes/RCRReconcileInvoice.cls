public without sharing class RCRReconcileInvoice extends A2HCPageBase
{
    public RCRReconcileInvoice()
    {
    }

    public RCRReconcileInvoice(ApexPages.StandardController controller)
    {
        String invoiceId = getParameter('id');
        if(invoiceId != null)
        {
            Invoice = getInvoice(invoiceId);
        }
    }

    public Boolean HasError
    {
        get;
        set;
    }

    public string FileUploadId
    {
        get
        {
            return getParameter('FileUploadId');
        }
        set;
    }

    public RCRReconcileInvoice(String invoiceId)
    {
        if(invoiceId != null)
        {
            Invoice = getInvoice(invoiceId);
        }
    }

    public RCR_Invoice__c Invoice
    {
        get
        {
            if(Invoice == null)
            {
                Invoice = new RCR_Invoice__c();
            }
            return Invoice;
        }
        public set;
    }

    public string ProcessingMessage
    {
        get
        {
            ProcessingMessage = '';
            if(Invoice.RecordTypeId == A2HCUtilities.getRecordType('RCR_Invoice__c', 'Processing').Id)
            {
                if(getParameter('submitted') == 'Y')
                {
                    ProcessingMessage = errorMsgs.get('Activity_Statement_Async_Approval');
                }
                else
                {
                    ProcessingMessage = errorMsgs.get('Activity_Statement_Processing');
                }
            }
            return ProcessingMessage;
        }
        private set;
    }

    public List<RCR_Invoice_Item__c> PendingItems
    {
        get
        {
            if(PendingItems == null)
            {
                PendingItems =
                [
                        SELECT ID,
                                Name,
                                Code__c,
                                RCR_Service_Order__r.Name,
                                Reconciled_To__c,
                                Quantity__c,
                                Rate__c,
                                Total_ex_GST__c,
                                Total_GST__c,
                                Total__c,
                                Error__c
                        FROM RCR_Invoice_Item__c
                        WHERE RCR_Invoice__c = :Invoice.ID
                        AND
                        Status__c = :(APS_PicklistValues.RCRInvoiceItem_Status_PendingApproval)
                ];
            }
            return PendingItems;
        }
        private set;
    }

    public List<RCR_Invoice_Item__c> FailedItems
    {
        get
        {
            if(FailedItems == null)
            {
                FailedItems =
                [
                        SELECT ID,
                                Name,
                                Code__c,
                                Service_Order_Number__c,
                                Quantity__c,
                                Rate__c,
                                Total_ex_GST__c,
                                Total_GST__c,
                                Total__c,
                                Error__c
                        FROM RCR_Invoice_Item__c
                        WHERE RCR_Invoice__c = :Invoice.ID
                        AND
                        Status__c = :(APS_PicklistValues.RCRInvoiceItem_Status_Rejected)
                ];
            }
            return FailedItems;
        }
        private set;
    }

    public Boolean CanWithdraw
    {
        get;
        set;
    }

    private Map<String, String> errorMsgs
    {
        get
        {
            if(errorMsgs == null)
            {
                errorMsgs = new Map<String, String>();

                for(RCR_Error_Message__c msg :
                [
                        SELECT Name,
                                Message__c
                        FROM RCR_Error_Message__c
                ])
                {
                    errorMsgs.put(msg.Name, msg.Message__c);
                }
            }
            return errorMsgs;
        }
        private set;
    }

    public PageReference redir()
    {
        try
        {
            if(CurrentUser.Profile.Name == 'LSA SP Community')
            {
                PageReference pg = Page.SPPortalStatement;
                pg.getParameters().put('id', Invoice.ID);
                pg.getParameters().put('retURL', getParameter('retURL'));
                return pg.setRedirect(true);
            }
            return null;
        }
        catch(Exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }

    public PageReference removeFromBatch()
    {
        Savepoint sp = Database.setSavepoint();
        try
        {
            if(Invoice.RCR_Invoice_Batch__c == null)
            {
                throw new A2HCException('Invoice has not been added to a batch');
            }
            RCR_Invoice_Batch__c invBatch = new RCR_Invoice_Batch__c(ID = Invoice.RCR_Invoice_Batch__c);

            Invoice.RCR_Invoice_Batch__c = null;
            Invoice.RecordTypeId = RCR_Invoice__c.sObjectType.getDescribe().getRecordTypeInfosByName().get('Created').getRecordTypeId();
            update Invoice;

            RCRInvoiceBatchController.setHighestInvoice(invBatch);
            update invBatch;

            delete
            [
                    SELECT ID
                    FROM Attachment
                    WHERE ParentId = :invBatch.ID
                    AND
                    Description = :RCRInvoiceBatchController.MASTERPIECE_EXPORT
            ];

            List<RCR_Invoice_Item__c> items =
            [
                    SELECT ID
                    FROM RCR_Invoice_Item__c
                    WHERE RCR_invoice__c = :Invoice.ID
            ];
            ID itemCreatedRTId = RCR_Invoice_Item__c.sObjectType.getDescribe().getRecordTypeInfosByName().get('Created').getRecordTypeId();
            for(RCR_Invoice_Item__c item : items)
            {
                item.RecordTypeId = itemCreatedRTId;
                item.RCR_Contract_Scheduled_Service__c = null;
                item.Reconciled_Service__c = null;
            }
            update items;

            PageReference pg = new PageReference('/' + Invoice.Id);
            pg.setRedirect(true);
            return pg;
        }
        catch(Exception ex)
        {
            return A2HCException.formatExceptionAndRollback(sp, ex);
        }
    }

    public PageReference reconcile()
    {
        return reconcileInvoice(Invoice.Use_New_Process__c || Invoice.LP_Invoice_Key__c != null);
    }

    public static void reconcileNew(ID invoiceId)
    {
        RCRReconcileInvoice thisInstance = new RCRReconcileInvoice(invoiceId);
        thisInstance.reconcileInvoice(true);
    }

    public PageReference reconcileNew()
    {
        return reconcileInvoice(true);
    }

    private PageReference reconcileInvoice(Boolean useNew)
    {
        Savepoint sp = Database.setSavepoint();

        try
        {
            // check for changes by other sessions or users
            HasError = !checkInvoiceOKToProcess();
            if(HasError)
            {
                return null;
            }

            Invoice.Reconciliation_User__c = UserInfo.getUserId();
            update Invoice;

            if(BatchUtilities.isScheduleWaiting(Invoice.Schedule__c))
            {
                return null;
            }

            List<RCR_Invoice__c> invoices = new List<RCR_Invoice__c>
            {
                    Invoice
            };
            RCRInvoiceSchedule invoiceSchedule = new RCRInvoiceSchedule();
            invoiceSchedule.scheduleReconciliation(invoices, useNew);

            PageReference pg = new PageReference('/' + Invoice.Id);
            pg.setRedirect(true);
            return pg;
        }
        catch(Exception ex)
        {
            return A2HCException.formatExceptionAndRollback(sp, ex);
        }
    }

    public PageReference submit()
    {
        Savepoint sp = Database.setSavepoint();
        try
        {
            String retURL = getParameter('retURL');
            String action = getParameter('action');
            PageReference page = new PageReference(retURL == null ? ('/' + Invoice.Id) : retURL);
            List<RCR_Invoice_Item__c> items = [SELECT ID,
                                                    Name,
                                                    Code__c,
                                                    Error__c,
                                                    Quantity__c,
                                                    Rate__c,
                                                    Total__c,
                                                    Status__c,
                                                    Activity_Date__c,
                                                    RCR_Service_Order__c,
                                                    RCR_Invoice__r.SDS__c,
                                                    RecordTypeId
                                            FROM    RCR_Invoice_Item__c
                                            WHERE   RCR_Invoice__c = :Invoice.Id
                                            ORDER BY Service_Order_Number__c, Activity_Date__c ASC];
            if(action == 'reject')
            {
                for(RCR_Invoice_Item__c item : items)
                {
                    item.Error__c = 'Rejected by ' + CurrentUser.Name + ' on ' + Date.today().format();
                }
                update items;
                Invoice.Reject__c = true;
                Invoice.Rejected_By__c = UserInfo.getUserId();
                Invoice.Reason_For_Rejection__c = 'MyWay invoice regeneration';
                if(Invoice.SDS__c && !Invoice.Invoice_Number__c.endsWith('-R'))
                {
                    Invoice.Invoice_Number__c += '-R';
                }
                update Invoice;
            }
            else
            {
                Integer immediateMaxItems;
                Integer batchSize;
                // check for changes by other sessions or users
                if(Invoice.Lines_Not_Reconciled__c > 0)
                {
                    return A2HCException.formatException(errorMsgs.get('Invoice_LinesNotReconciled'));
                }

                if(!checkInvoiceOKToProcess())
                {
                    return null;
                }

                AggregateResult result = [SELECT    SUM(Total__c) totalPending
                                            FROM    RCR_Invoice_Item__c
                                            WHERE   RCR_Invoice__c = :Invoice.ID AND
                                                    Status__c != :APS_PicklistValues.RCRInvoiceItem_Status_Loaded AND
                                                    Status__c != :APS_PicklistValues.RCRInvoiceItem_Status_Rejected];
                if((decimal) (result.get('totalPending')) == 0.0)
                {
                    return A2HCException.formatException(errorMsgs.get('Invoice_ZeroValue'));
                }
                try
                {
                    immediateMaxItems = Batch_Size__c.getInstance('RCR_Invoice_Submit_Max_Items').Records__c.intValue();
                    batchSize = Batch_Size__c.getInstance('RCR_Invoice_Submit_Async').Records__c.IntValue();
                }
                catch(Exception ex)
                {
                    throw new A2HCException('Batch size settings are not defined.');
                }


                if((Invoice.Start_Date__c == null || Invoice.End_Date__c == null)
                        && Invoice.All_Line_Count__c > 0)
                {
                    A2HCUtilities.sortList(items, 'Activity_Date__c', 'ASC');
                    Invoice.Start_Date__c = items[0].Activity_Date__c;
                    Invoice.End_Date__c = items[items.size() - 1].Activity_Date__c;
                }
                if(Invoice.All_Line_Count__c < immediateMaxItems)
                {
                    Invoice.RecordTypeId = A2HCUtilities.getRecordType('RCR_Invoice__c', 'Submitted').Id;
                    update Invoice;
                    submitInvoice(items);
                }
                else
                {
                    submitBatch(batchSize);
                    page.getParameters().put('submitted', 'Y');
                    page.setRedirect(true);
                }
            }
            return page;
        }
        catch(Exception ex)
        {
            return A2HCException.formatExceptionAndRollback(sp, ex);
        }
    }

    public PageReference generateRCTI()
    {
        return null;
    }

    public PageReference createTasks()
    {
        try
        {
            Set<String> serviceOrderNumbers = new Set<String>();
            String comment = '';
            List<RCR_Invoice_Item__c> items =
            [
                    SELECT ID,
                            Name,
                            RCR_Invoice__c,
                            Error__c,
                            Service_Order_Number__c
                    FROM RCR_Invoice_Item__c
                    WHERE RCR_Invoice__c = :Invoice.ID AND
                    Status__c = :APS_PicklistValues.RCRInvoiceItem_Status_Rejected
                    ORDER BY Service_Order_Number__c
            ];

            for(RCR_Invoice_Item__c item : items)
            {
                serviceOrderNumbers.add(item.Service_Order_Number__c);
                comment += item.Name + ': ' + item.Error__c + '\n';
            }
            Integer taskCommentLength = Task.Description.getDescribe().getLength();
            String allocatedTo = '';
            for(RCR_Contract__c so :
            [
                    SELECT Client__r.Allocated_To__r.Name
                    FROM RCR_Contract__c
                    WHERE Name IN :serviceOrderNumbers AND
                    RecordType.Name = :APSRecordTypes.RCRServiceOrder_ServiceOrder AND
                    Client__r.Allocated_To__r.IsActive = true
                    LIMIT 1
            ])
            {
                allocatedTo = so.Client__r.Allocated_To__r.Name;
            }
            String taskPrefix = Task.sObjectType.getDescribe().getKeyPrefix();
            String activityStatementPrefix = RCR_Invoice__c.sObjectType.getDescribe().getKeyPrefix();
            String taskUrl = '/' + taskPrefix + '/e?retURL=' + Invoice.ID + '&tsk1=' + allocatedTo + '&tsk3=' + Invoice.Name + '&tsk3_mlktp=' + activityStatementPrefix + '&tsk5=Invoice+failed+reconciliation';
            String commentUrl = '&tsk6=' + comment.abbreviate(taskCommentLength);
            // keep URL under 2000 characters
            if(taskUrl.length() + commentUrl.length() > 2000)
            {
                commentUrl = commentUrl.left(2000 - taskUrl.length());
            }
            PageReference pg = new PageReference(taskUrl + commentUrl);
            return pg;
        }
        catch(Exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }

    public PageReference createServiceQuery()
    {
        try
        {
            Schema.DescribeSObjectResult result = Service_Query__c.sObjectType.getDescribe();
            String prefix = result.getKeyPrefix();
            String recTypeId = A2HCUtilities.getRecordType('Service_Query__c', 'RCR Open').ID;
            String invoiceFieldId = Name_Fields__c.getInstance('ServiceQueryInvoiceName').Name_Field_ID__c;
            String accountFieldId = Name_Fields__c.getInstance('ServiceQueryAccountName').Name_Field_ID__c;

            PageReference pg = new PageReference('/' + prefix + '/e?' +
                    invoiceFieldId + '=' + Invoice.Name +
                    '&' + accountFieldId + '=' + Invoice.Account__r.Name +
                    '&RecordType=' + recTypeId +
                    '&retURL=/' + Invoice.Id);
            return pg.setRedirect(true);
        }
        catch(Exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }

    public void submitBatch(Integer batchSize)
    {
        Invoice.RecordTypeId = A2HCUtilities.getRecordType('RCR_Invoice__c', 'Processing').Id;
        update Invoice;

        RCRInvoiceApprovalBatch apprBatch = new RCRInvoiceApprovalBatch();
        apprBatch.InvoiceID = Invoice.ID;
        Database.executeBatch(apprBatch, batchSize);
    }

    public void submitInvoice(List<RCR_Invoice_Item__c> items)
    {
        ID recordTypeID = A2HCUtilities.getRecordType('RCR_Invoice_Item__c', 'Submitted').Id;
        Set<ID> serviceOrderIds = new Set<ID>();
        for(RCR_Invoice_Item__c i : items)
        {
            i.RecordTypeId = recordTypeID;
            serviceOrderIds.add(i.RCR_Service_Order__c);
        }
        update items;

        getInvoice(Invoice.Id);
        // create service query records for each rejected activity statement in the invoice
        //createServiceQueries(Invoice.ID);
        setServiceTotals(serviceOrderIds);
    }

    public void checkLPInvoiceStatus()
    {
        CanWithdraw = false;
        String msg = null;
        if(Invoice.LP_Invoice_Key__c == null)
        {
            msg = 'This statement cannot be withdrawn as it is not a LanternPay invoice';
        }
        else if(Invoice.RCR_Invoice_Batch__c != null)
        {
            msg = 'This statement cannot be withdrawn as it has been included in a Masterpiece batch';
        }
        else if(Invoice.Withdrawn_Date__c != null)
        {
            msg = 'This statement has already been withdrawn.';
        }
        else
        {
            Integer matchedPayments =
            [
                    SELECT COUNT()
                    FROM LP_Confirmed_Payment__c
                    WHERE Activity_Statement__c = :Invoice.ID AND
                    Activity_Statement__c != null AND
                    Match_Status__c = :LPRESTInterface.LPPAYMENT_MATCHSTATUS_MATCHED
            ];
            if(matchedPayments > 0)
            {
                msg = 'This statement cannot be withdrawn as it has LanternPay payments associated with it';
            }
        }
        if(msg == null)
        {
            CanWithdraw = true;
        }
        else
        {
            A2HCException.formatWarning(msg);
        }
    }

    public PageReference withdrawLPInvoice()
    {
        return withdrawLPInvoice(Invoice);
    }

    public static PageReference withdrawLPInvoice(RCR_Invoice_Item__c item)
    {
        return withdrawLPInvoice(getInvoice(item.RCR_Invoice__c));
    }

    public static PageReference withdrawLPInvoice(RCR_Invoice__c inv)
    {
        Savepoint sp = Database.setSavepoint();
        try
        {
            Set<ID> serviceOrderIds = new Set<ID>();
            List<Task> tasksToClose = new List<Task>();
            String taskMessage = 'Task closed as statement has been withdrawn';
            List<RCR_Invoice_Item__c> items = [SELECT ID,
                                                        RCR_Contract_Scheduled_Service__r.RCR_Contract__c,
                                                        (SELECT ID,
                                                                        Description
                                                                FROM Tasks
                                                                WHERE IsClosed = false)
                                                FROM RCR_Invoice_Item__c
                                                WHERE RCR_Invoice__c = :inv.ID];
            for(RCR_Invoice_Item__c item : items)
            {
                serviceOrderIds.add(item.RCR_Contract_Scheduled_Service__r.RCR_Contract__c);
                item.Withdrawn__c = true;
                for(Task t : item.Tasks)
                {
                    t.Status = 'Completed';
                    t.Description = t.Description == null ? taskMessage : (t.Description += '\n\n' + taskMessage);
                    tasksToClose.add(t);
                }
            }
            update tasksToClose;
            update items;
            if(!serviceOrderIds.isEmpty())
            {
                setServiceTotals(serviceOrderIds, true);
            }
            inv.Withdrawn_Date__c = DateTime.now();
            inv.Withdrawn_By__c = UserInfo.getUserId();
            update inv;
            return new PageReference('/' + inv.ID);
        }
        catch(Exception ex)
        {
            return A2HCException.formatExceptionAndRollback(sp, ex);
        }
    }

    public static void createServiceQueries(Set<Id> invoiceIDs)
    {
        List<Service_Query__c> serviceQueries = new List<Service_Query__c>();
        Set<ID> serviceOrderIDs = new Set<ID>();
        
        AggregateResult[] ar = [SELECT  SUM(Total__c)   Total,
                                        COUNT(ID)       Lines,
                                        RCR_Invoice__c,
                                        RCR_Service_Order__c
                                 FROM   RCR_Invoice_Item__c
                                 WHERE  RCR_Invoice__c IN :invoiceIDs AND
                                        Status__c = :APS_PicklistValues.RCRInvoiceItem_Status_Rejected
                                 GROUP BY RCR_Invoice__c, 
                                        RCR_Service_Order__c];
        if(ar.isEmpty())
        {
            return;
        }
        for(AggregateResult result : ar)
        {
            serviceOrderIDs.add((String)result.get('RCR_Service_Order__c'));
        }
        
        Map<ID, RCR_Contract__c> serviceOrdersByID = new Map<ID, RCR_Contract__c>(
                                 [SELECT    ID,
                                            Name
                                    FROM    RCR_Contract__c
                                    WHERE   ID IN :serviceOrderIDs AND
                                            (RecordType.Name = :APSRecordTypes.RCRServiceOrder_ServiceOrder OR
                                            RecordType.Name = :APSRecordTypes.RCRServiceOrder_ServiceOrderCBMS)]);

        for(AggregateResult result : ar)
        {
            String s = 'Some items failed reconciliation';
            Service_Query__c sq = new Service_Query__c(RCR_Invoice__c = (String)result.get('RCR_Invoice__c'));
            ID serviceOrderId = (ID)result.get('RCR_Service_Order__c');
            if(serviceOrdersByID.containsKey(serviceOrderId))
            {
                sq.RCR_Service_Order__c = serviceOrderId;
                s += ' for service order ' + serviceOrdersByID.get(serviceOrderId).Name; 
            }
            s += '<br/>Total amount: $' + ((decimal)result.get('Total')).format()+ '<br/>' +
                            'Total lines failed: ' + ((Integer)result.get('Lines')).format();
            sq.Description__c = s;
            serviceQueries.add(sq);
        }
        insert serviceQueries;
    }
    
    public void createServiceQueries(String invoiceID)
    {
        createServiceQueries(new Set<Id> {invoiceID});
    }

    public static void setServiceTotals(Set<ID> serviceOrderIds)
    {
        setServiceTotals(serviceOrderIds, true);
    }

    public static void setServiceTotals(Set<ID> serviceOrderIds, Boolean amendLPContracts)
    {
        Map<ID, decimal> schedTotalsByIDs = new Map<ID, decimal>();
        Map<ID, decimal> schedTotalQtysByIDs = new Map<ID, decimal>();
        Set<ID> planActionIds = new Set<ID>();
        Set<ID> lanternPayContractIds = new Set<ID>();
        AggregateResult[] ar = [SELECT  SUM(Total_ex_GST__c)    totSubm,
                                        SUM(Quantity__c)        totQty,
                                        RCR_Contract_Scheduled_Service__c   schedID
                                FROM    RCR_Invoice_Item__c i
                                WHERE   //RCR_Invoice__r.RecordType.Name = 'Submitted' AND
                                        RecordType.Name = 'Submitted' AND
                                        Status__c = :APS_PicklistValues.RCRInvoiceItem_Status_Approved AND
                                        RCR_Service_Order__c IN :serviceOrderIds
                                GROUP BY RCR_Contract_Scheduled_Service__c];
        // LanternPay can cancel a statement item after initial approval so we need to keep going for the scenario where there is only one statement item
        // item reconciled to a service and it has subsequently been LP cancelled to reset the service submitted
//        if(ar.isEmpty())
//        {
//            return;
//        }
        for(AggregateResult result : ar)
        {
            ID schedID = (ID)(result.get('schedID'));
            decimal total = (decimal)(result.get('totSubm'));
            decimal totalQty = (decimal)(result.get('totQty'));
            if(schedID != null)
            {
                schedTotalsByIDs.put(schedID, total);
                schedTotalQtysByIDs.put(schedID, totalQty);
            }
        }

        List<RCR_Contract_Scheduled_Service__c> schedServices = [SELECT ID,
                                                                    RCR_Contract__c,
                                                                    RCR_Contract__r.LP_Contract_Key__c,
                                                                    RCR_Contract__r.Plan_Action__c,
                                                                    (SELECT ID
                                                                    FROM    RCR_Invoice_Items__r
                                                                    WHERE   Status__c = :APS_PicklistValues.RCRInvoiceItem_Status_Approved
                                                                    LIMIT 1)
                                                            FROM    RCR_Contract_Scheduled_Service__c
                                                            WHERE   RCR_Contract__c IN :serviceOrderIds];

        for(RCR_Contract_Scheduled_Service__c css : schedServices)
        {
            if(css.RCR_Contract__r.LP_Contract_Key__c != null)
            {
                lanternPayContractIds.add(css.RCR_Contract__c);
            }
            if(css.RCR_Contract__r.Plan_Action__c != null)
            {
                planActionIds.add(css.RCR_Contract__r.Plan_Action__c);
            }
            if(css.RCR_Invoice_Items__r.isEmpty())
            {
                css.Total_Submitted__c = 0.0;
                css.Quantity_Used__c = 0.0;
            }
            else
            {
                if(schedTotalsByIDs.containsKey(css.ID))
                {
                    css.Total_Submitted__c = schedTotalsByIDs.get(css.ID);
                }
                if (schedTotalQtysByIDs.containsKey(css.ID))
                {
                    css.Quantity_Used__c = schedTotalQtysByIDs.get(css.ID);
                }
            }
        }
        TriggerByPass.RCRContractScheduledService = true;
        TriggerByPass.RCRServiceOrder = true;
        update schedServices;
        TriggerByPass.RCRContractScheduledService = false;
        TriggerByPass.RCRServiceOrder = false;

        List<Plan_Action__c> planActions = new List<Plan_Action__c>();
        for(AggregateResult result : [SELECT Plan_Action__c,
                                            SUM(Total_Submitted__c) totSubmitted
                                    FROM    RCR_Contract__c 
                                    WHERE   Plan_Action__c IN :planActionIds AND 
                                            RecordType.Name = :APSRecordTypes.RCRServiceOrder_ServiceOrder
                                    GROUP BY Plan_Action__c])
        {
            Plan_Action__c pa = new Plan_Action__c(ID = (String)result.get('Plan_Action__c'));
            pa.Total_Submitted__c = (Decimal)result.get('totSubmitted');
            planActions.add(pa);
        }
        TriggerBypass.PlanAction = true;
        update planActions;

        if(amendLPContracts && !lanternPayContractIds.isEmpty() && !System.isBatch() && !System.isFuture())
        {
            LPRESTInterface.alterContractFuture(lanternPayContractIds);
        }
    }

    public static RCR_Invoice__c getInvoice(String invoiceId)
    {
        for(RCR_Invoice__c i : [SELECT  ID,
                                        Name,
                                        Date__c,
                                        Invoice_Number__c,
                                        Start_Date__c,
                                        End_Date__c,
                                        Status__c,
                                        RCR_Invoice_Batch__r.Batch_Number__c,
                                        RCR_Invoice_Upload__c,
                                        Account__r.Name,
                                        RecordTypeId,
                                        RecordType.Name,
                                        Total__c,
                                        Total_Reconciled__c,
                                        Total_Failed_Reconciliation__c,
                                        Lines_Requiring_Approval__c,
                                        Lines_Not_Reconciled__c,
                                        Ready_For_Reconciliation__c,
                                        Reconciliation_User__c,
                                        Processing__c,
                                        Schedule__c,
                                        Use_New_Process__c,
                                        Account__r.RCTI_Agreement__c,
                                        Override_validation__c,
                                        All_Line_Count__c,
                                        Invoice_Received_Date__c,
                                        LP_Invoice_Key__c,
                                        Withdrawn_Date__c,
                                        SDS__c,
                                        (SELECT ID,
                                                Name
                                        FROM    Attachments)
                                FROM    RCR_Invoice__c
                                WHERE   ID = :invoiceId])
        {
            return i;
        }
        return null;
    }
    
    private Boolean checkInvoiceOKToProcess()
    {
        RCR_Invoice__c currentInvoiceState = getInvoice(Invoice.ID);
//        if(currentInvoiceState.Invoice_Received_Date__c == null)
//        {
//            A2HCException.formatException('Invoice Received Date is required before reconciling.');
//            return false;
//        }
        if(currentInvoiceState.RecordType.Name == 'Submitted') 
        {
            A2HCException.formatException('This invoice has already been submitted for approval.');
            return false;
        }
        if(currentInvoiceState.RecordType.Name == 'Processing')
        {
            A2HCException.formatException('This invoice is being processed by another user.');
            return false;
        }
        return true;
    }

    
    
}