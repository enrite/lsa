@isTest
private class TestRCRUploadRatesController 
{
    static testMethod void myUnitTest() 
    {
        TestLoadData.loadRCRData();
                
        Test.StartTest();
    
        RCR_Service_Rate__c rate = null;
        
        
        RCR_Service__c service =  [SELECT Id,
                                          Name,
                                          Level__c,
                                          Account__c,
                                         (SELECT    Id,
                                                    Start_Date__c,
                                                    End_Date__c
                                          FROM      RCR_Contract_Scheduled_Services__r),
                                          (SELECT   RCR_Service__r.Account__r.Name,
                                                    RCR_Service__r.Name,
                                                    RCR_Service__r.Level__c,
                                                    Start_Date__c,
                                                    End_Date__c,
                                                    Rate__c,
                                                    Public_Holiday_Rate__c,
                                                    Proposed_Rate_Standard__c,
                                                    Proposed_Rate_Public_Holidays__c,
                                                    Id
                                            FROM    RCR_Service_Rates__r)
                                  FROM   RCR_Service__c                                 
                                  LIMIT  1];
    
    
        for(RCR_Contract_Scheduled_Service__c css : service.RCR_Contract_Scheduled_Services__r)
        {
            for(RCR_Service_Rate__c r : service.RCR_Service_Rates__r)
            {
                if(A2HCUtilities.dateRangesOverlap(css.Start_Date__c, css.End_Date__c, r.Start_Date__c, r.End_Date__c))
                {
                    rate = r;
                    break;
                }
            }
        }
        
        system.debug(rate);
        if(rate == null)
        {
            return;
        }
        
        string csv = 'RCR Service Rate ID,Service Code,Service Level,Start Date,End Date,Rate,Public Holiday Rate,Proposed Rate (Standard),Proposed Rate (Public Holidays),Split Date,Service Provider\n';
        csv += rate.Id + ',';
        csv += rate.RCR_Service__r.Name + ',';
        csv += rate.RCR_Service__r.Level__c + ',';
        csv += rate.Start_Date__c.format() + ',';
        csv += rate.End_Date__c.format() + ',';
        csv += rate.Rate__c + ',';
        csv += rate.Public_Holiday_Rate__c + ',';
        csv += rate.Proposed_Rate_Standard__c + ',';
        csv += rate.Proposed_Rate_Public_Holidays__c + ',,';
        csv += rate.RCR_Service__r.Account__r.Name + '\n';
        system.debug('\n' + csv);
                
        
        RCRUploadRatesController cntrlr = new RCRUploadRatesController();
        
        cntrlr.ErrorMsgs = null;
        system.debug(cntrlr.ErrorMsgs);
        system.debug(cntrlr.File);
        system.debug(cntrlr.RateUpload);
        system.debug(cntrlr.exceptionCount);
        system.debug(cntrlr.DisplayRates);
        system.debug(cntrlr.SplitRates);
        system.debug(cntrlr.Rates);
        system.debug(cntrlr.ServiceCodeIDsByAccountNameCodeAndLevel);
        system.debug(cntrlr.existingRatesByAccountAndCode);

        cntrlr.displayRates();
        system.debug(cntrlr.exceptionCount);

        
        Attachment att = new Attachment();
        att.Name = 'test';
        att.Description = 'test';
        att.Body = Blob.valueOf(csv);
        cntrlr.File = att;
        cntrlr.File.Name = 'test';
        
        system.debug(cntrlr.File);
        
        cntrlr.displayRates();
        cntrlr.cancel();
        cntrlr.saveOverride();
        
        
        RCRUploadRatesController.RateWrapper rWrap = new RCRUploadRatesController.RateWrapper(1, service.Account__c, service.Name, service.Level__c, rate);
        rWrap.SplitDate = rate.Start_Date__c.addDays(100);
        cntrlr.splitRate(rWrap);
        
        Test.stopTest();
    } 
}