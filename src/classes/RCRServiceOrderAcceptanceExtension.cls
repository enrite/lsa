public with sharing class RCRServiceOrderAcceptanceExtension extends A2HCPageBase 
{
    public RCRServiceOrderAcceptanceExtension(ApexPages.Standardcontroller controller)
    {
        string contractID = getParameter('id');  
        AcceptanceType = getParameter('type');          
        VariationID = getParameter('variationID');
        
        if(contractID != null)
        {
            RCRContract = getServiceOrder(contractId);
        }
        
        if(VariationID != null)
        { 
			ServiceRateVariation = getServiceRateVariation();
        }                
    }
    
    
    public Id VariationID
    {
    	get;
    	private set;
    }     
    
    public boolean AuthorisedToAcceptCSA
    {
        get
        {
            return CurrentUser.Authorised_To_Accept_CSA__c;
        }
    }
    
    public RCR_Contract__c RCRContract
    {
        get;
        private set;
    }
    
    public RCR_Service_Rate_Variation__c ServiceRateVariation
    {
    	get;
    	private set;
    }
    
    public string AcceptanceType
    {
    	get;
    	set;
    }
    
    public RCR_Service_Order_Acceptance__c ProviderAcceptance
    {
        get
        {
            if(ProviderAcceptance == null)
            {
                ProviderAcceptance = new RCR_Service_Order_Acceptance__c();
                if(RCRContract != null)
                {
	                ProviderAcceptance.RCR_Service_Order__c = RCRContract.Id;
                }
                ProviderAcceptance.Contract_Manager_Name__c = CurrentUser.Name;
                ProviderAcceptance.Contract_Manager_Phone__c = CurrentUser.Phone;
                ProviderAcceptance.Contract_Manager_Fax__c = CurrentUser.Fax;
                ProviderAcceptance.Position__c = CurrentUser.Title;
                ProviderAcceptance.Date_Accepted_Rejected__c = null;
            }
            return ProviderAcceptance;
        }
        set;
    }
    
    
    public List<RCR_Service_Order_Acceptance__c> ProviderAcceptances
    {
    	get
    	{
    		if(ProviderAcceptances == null)
    		{
    			ProviderAcceptances = new List<RCR_Service_Order_Acceptance__c>();
    		}
    		return ProviderAcceptances;
    	}
    	private set;
    }

    public PageReference accept()
    {
        
        Savepoint sp = Database.setSavepoint();         
        try 
        {
        	throwTestError();
        	
            Datetime acceptedTime = Datetime.now();
            
            if(AcceptanceType == 'Cancellation')
            {
            	if(RCRContract.Cancellation_Acknowledged_Date__c != null)
	            {
	                return A2HCException.formatException('The Cancellation of the Service Order has already been aknowledged on ' + RCRContract.Cancellation_Acknowledged_Date__c.format('dd/MM/yyyy hh:mm a') + '.');
	            }
            }
            else
            {
	            if(RCRContract.CSA_Acceptance_Date__c != null)
	            {
	                return A2HCException.formatException('The Service Order has already been accepted on ' + RCRContract.CSA_Acceptance_Date__c.format('dd/MM/yyyy hh:mm a') + '.');
	            }
            }
        
        	if(AcceptanceType == 'Cancellation')
        	{
        		RCRContract.Cancellation_Acknowledged_By__c = CurrentUser.FullName__c;
        		RCRContract.Cancellation_Acknowledged_Date__c = Datetime.now();
        	}
        	else
        	{
        		RCRContract.CSA_Status__c = APS_PicklistValues.RCRContractCSAStatus_Accepted;
	            RCRContract.CSA_Status_Recall__c = APS_PicklistValues.RCRContractCSAStatus_Accepted;
	            RCRContract.CSA_Acceptance_Date__c = acceptedTime;
	            RCRContract.CSA_Accepted_On_Recall__c = acceptedTime;
        	}
            
            update RCRContract;

            if(AcceptanceType != 'Cancellation')
            {
	            ProviderAcceptance.Approved_On__c = RCRContract.Date_Approved__c;
	            ProviderAcceptance.DCSI_Authorised_Delegate__c = RCRContract.Approver_User_Name__c;         
	            ProviderAcceptance.Authorised_Person__c = CurrentUser.Id;
	            ProviderAcceptance.Date_Accepted_Rejected__c = acceptedTime;
	            ProviderAcceptance.Acceptance_Status__c = APS_PicklistValues.RCRContractCSAStatus_Accepted;
	            upsert ProviderAcceptance;
	
	            notifySubmitter(APS_PicklistValues.RCRContractCSAStatus_Accepted);
            }
            getAcceptance();
            return null;
        }
        catch(exception ex)
        {
            Database.rollback(sp);
            RCRContract = getServiceOrder(RCRContract.ID);
            ProviderAcceptance = null;
            return A2HCException.formatException(ex);
        }
    }
    

    public PageReference rejectCSA()
    {
    	Savepoint sp = Database.setSavepoint();  
        try
        {   
            throwTestError();
        	
            if(RCRContract.CSA_Acceptance_Date__c != null)
            {
                return A2HCException.formatException('The Service Order has already been accepted on ' + RCRContract.CSA_Acceptance_Date__c.format('dd/MM/yyyy hh:mm a') + '.');
            }
            
            RCRContract.CSA_Status__c = APS_PicklistValues.RCRContractCSAStatus_Rejected;
            RCRContract.CSA_Status_Recall__c = APS_PicklistValues.RCRContractCSAStatus_Rejected;
            RCRContract.Visible_To_Service_Providers__c = false;
            RCRContract.CSA_Acceptance_Date__c = null;
            RCRContract.CSA_Accepted_On_Recall__c = null;
            RCRContract.CSA_Bulk_Approved__c = false;
            update RCRContract;
            
            ProviderAcceptance.Approved_On__c = RCRContract.Date_Approved__c;
            ProviderAcceptance.DCSI_Authorised_Delegate__c = RCRContract.Approver_User_Name__c;         
            ProviderAcceptance.Authorised_Person__c = CurrentUser.Id;
            ProviderAcceptance.Date_Accepted_Rejected__c = Datetime.now();
            ProviderAcceptance.Acceptance_Status__c = APS_PicklistValues.RCRContractCSAStatus_Rejected;
            upsert ProviderAcceptance;
            
            notifySubmitter(APS_PicklistValues.RCRContractCSAStatus_Rejected);
            getAcceptance();
            
            return null;
        }
        catch(Exception ex)
        {
            Database.rollback(sp);
            RCRContract = getServiceOrder(RCRContract.ID);
            ProviderAcceptance = null;
            return A2HCException.formatException(ex);
        }
    }
    
        
    public PageReference acceptRollovers()
    {
    	Savepoint sp = Database.setSavepoint();
    	try
    	{
    		throwTestError();
    		
    		Datetime acceptedTime = Datetime.now();
    		
    		List<RCR_Contract__c> serviceOrders = new List<RCR_Contract__c>();
            	
        	for(RCR_Contract__c so : [SELECT 	Id,
        										Name,
        										Date_Approved__c
        							  FROM		RCR_Contract__c
    								  WHERE		Source_Record__r.RecordType.Name = :APSRecordTypes.RCRServiceOrder_Rollover AND
    								  			Name LIKE :(Test.isRunningTest() ? 'TestSO' : '%')])
    		{
    			so.CSA_Status__c = APS_PicklistValues.RCRContractCSAStatus_Accepted;
	            so.CSA_Status_Recall__c = APS_PicklistValues.RCRContractCSAStatus_Accepted;
	            so.CSA_Acceptance_Date__c = acceptedTime;
	            so.CSA_Accepted_On_Recall__c = acceptedTime;
	            serviceOrders.add(so);
	            
    			RCR_Service_Order_Acceptance__c acceptance = new RCR_Service_Order_Acceptance__c(RCR_Service_Order__c = so.Id);
    			acceptance.Approved_On__c = so.Date_Approved__c;
	            acceptance.DCSI_Authorised_Delegate__c = so.Approver_User_Name__c;         
	            acceptance.Authorised_Person__c = CurrentUser.Id;
	            acceptance.Date_Accepted_Rejected__c = acceptedTime;
	            acceptance.Acceptance_Status__c = APS_PicklistValues.RCRContractCSAStatus_Accepted;
    			ProviderAcceptances.add(acceptance);
    		}
    		upsert serviceOrders;
    		insert ProviderAcceptances;
    		return null;
    	}
    	catch(Exception ex)
    	{
    		Database.rollback(sp);
    		return A2HCException.formatException(ex);
    	}
    }
    
    
	public PageReference acceptServiceRateVariation()
	{
		Savepoint sp = Database.setSavepoint();
		try
		{
			throwTestError();
			
			Datetime acceptedTime = Datetime.now();
			Set<Id> serviceOrderIDs = new Set<Id>();
			List<RCR_Contract__c> serviceOrders = new List<RCR_Contract__c>();
			
			ProviderAcceptances = null;
			
			ServiceRateVariation.Accepted_By__c = UserInfo.getUserId();
			ServiceRateVariation.Accepted_By_Text__c = UserInfo.getUserName();
			ServiceRateVariation.Date_Accepted__c = acceptedTime;
			update ServiceRateVariation;

        	for(RCR_Service_Rate_Audit__c auditRecord : ServiceRateVariation.RCR_Service_Rate_Audits__r)
        	{
        		serviceOrderIDs.add(auditRecord.RCR_Service_Order__c);
        	}
system.debug(serviceOrderIDs);        	
        	for(RCR_Contract__c so : [SELECT 	Id,
        										Name,
        										Date_Approved__c,
        										Approver_User_Name__c
        							  FROM		RCR_Contract__c
    								  WHERE		Id IN :serviceOrderIDs])
    		{
    			// NOT SURE ABOUT UPDATING THE SERVICE ORDERS
    			
    			//so.CSA_Status__c = APS_PicklistValues.RCRContractCSAStatus_Accepted;
	            //so.CSA_Status_Recall__c = APS_PicklistValues.RCRContractCSAStatus_Accepted;
	            //so.CSA_Acceptance_Date__c = acceptedTime;
	            //so.CSA_Accepted_On_Recall__c = acceptedTime;
	            //serviceOrders.add(so);
	            
    			RCR_Service_Order_Acceptance__c acceptance = new RCR_Service_Order_Acceptance__c(RCR_Service_Order__c = so.Id);
    			acceptance.Approved_On__c = ServiceRateVariation.Date_Approved__c;
	            acceptance.DCSI_Authorised_Delegate__c = ServiceRateVariation.Approved_By__c;         
	            acceptance.Authorised_Person__c = CurrentUser.Id;
	            acceptance.Date_Accepted_Rejected__c = acceptedTime;
	            acceptance.Acceptance_Status__c = APS_PicklistValues.RCRContractCSAStatus_Accepted;
    			ProviderAcceptances.add(acceptance);
    		}	
    		//upsert serviceOrders;
system.debug('ProviderAcceptances: ' + ProviderAcceptances);    		
    		insert ProviderAcceptances;
			return null;
		}
		catch(Exception ex)
		{
			Database.rollback(sp);
    		return A2HCException.formatException(ex);
		}
	}
    
    public void savePDF()
    {   
        try
        {
            throwTestError();
            
            if(AcceptanceType == 'Cancellation')
            {
            	// Save the Cancellation of Funded Service PDF
				Attachment att = new Attachment();
	            PageReference pg = Page.RCRCancellationOfFundedServicePDF;
	            pg.getParameters().put('id', RCRContract.Id);
	            att.Body = Test.isRunningTest() ? blob.valueOf('Blah') : pg.getContent();
	            att.ContentType = 'application/pdf';
	            att.ParentId = RCRContract.Id;
	            att.Name = 'Cancellation_of_Funded_Service.pdf';
	            att.Description = 'Cancellation of Funded Service';
	            insert att;     			
            }
            else if((AcceptanceType == 'Rollover' || AcceptanceType == 'RateVariation') && ProviderAcceptances.size() > 0)
            {
            	PageReference pg;
            	if(AcceptanceType == 'Rollover')
            	{
            		pg = Page.RCRBulkRolloverAcceptancePDF;
            	}
            	else if(AcceptanceType == 'RateVariation')
            	{
            		pg = Page.RCRServiceRateVariationPDF;
            		pg.getParameters().put('id', ServiceRateVariation.Id);
            	}

            	blob pdfContent = Test.isRunningTest() ? blob.valueOf('Blah') : pg.getContent();
            	
            	
            	Map<string, Attachment> attachments = new Map<string, Attachment>();
            	for(RCR_Service_Order_Acceptance__c acceptance : ProviderAcceptances)
            	{
            		Attachment att = new Attachment();
            		att.Body = pdfContent;
		            att.ContentType = 'application/pdf';
		            att.ParentId = acceptance.Id;
		            att.Name = 'Client_Service_Agreement.pdf';
		            att.Description = 'Bulk Client Service Agreement';
		            attachments.put(acceptance.Id, att);
            	}
            	
            	insert attachments.values();
            	
            	for(RCR_Service_Order_Acceptance__c acceptance : ProviderAcceptances)
            	{
            		acceptance.CSA_Attachment_ID__c = attachments.get(acceptance.Id).Id;
            	}
            	update ProviderAcceptances;
            }
            else
            {
	            // Save the Client Service Agreement as an Attachment to the Service Order.
	            Attachment att = new Attachment();
	            PageReference pg = Page.RCRClientServiceAgreementPDF;
	            pg.getParameters().put('id', RCRContract.Id);

	            att.Body = Test.isRunningTest() ? blob.valueOf('Blah') : pg.getContent();
	            att.ContentType = 'application/pdf';
	            att.ParentId = ProviderAcceptance.Id;
	            att.Name = 'Client_Service_Agreement.pdf';
	            att.Description = 'Client Service Agreement';
	            insert att;
	    
	            ProviderAcceptance.CSA_Attachment_ID__c = att.Id;
	            update ProviderAcceptance;  
            }
        }
        catch(exception ex)
        {
            A2HCException.formatException(ex);
        }
    }
      

    public PageReference createServiceQuery()
    {
        try
        {
        	throwTestError();
			
			PageReference pg = Page.RCRServiceQuery;
			pg.getParameters().put('retURL', '/apex/RCRServiceOrderAcceptance?id=' + RCRContract.Id);
			pg.getParameters().put('serviceOrder', RCRContract.Id);
			pg.getParameters().put('CSARelated', 'true');
            return pg.setRedirect(true);
        }
        catch(exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }
    
    
    public PageReference cancelOverride()
    {
        PageReference pg = new PageReference('/home/home.jsp');
        return pg.setRedirect(true);
    }
    
    
    private void getAcceptance()
    {           
        if(RCRContract != null)
        {
            for(RCR_Service_Order_Acceptance__c a : [SELECT Id,
                                                            Name,
                                                            Authorised_Person__c,
                                                            Contract_Manager_Fax__c,
                                                            Contract_Manager_Name__c,
                                                            Contract_Manager_Phone__c,
                                                            Date_Accepted_Rejected__c,
                                                            Acceptance_Status__c,
                                                            Approved_On__c,
                                                            DCSI_Authorised_Delegate__c,
                                                            Position__c,
                                                            RCR_Service_Order__c,
                                                            Staff_Member_Mobile__c,
                                                            Staff_Member_Name__c,
                                                            Staff_Member_Phone__c
                                                  FROM      RCR_Service_Order_Acceptance__c
                                                  WHERE     RCR_Service_Order__c = :RCRContract.Id
                                                  ORDER BY  CreatedDate desc
                                                  LIMIT 1])
            {
                ProviderAcceptance = a;
            }
        }   
    }

    private void notifySubmitter(string action)
    {
        if(RCRContract.Submited_For_Approval_User_ID__c != null)
        {
            EmailManager emailMgr = new EmailManager();
            emailMgr.sendSubmitterServiceProviderAcceptanceNotification(RCRContract, action, CurrentUser);  
        }
    }
    
    private RCR_Contract__c getServiceOrder(String contractID)
    {
    	return [SELECT  Id,
                        Name,
                        CCMS_ID__c,
                        CSA_Status__c,
                        CSA_Acceptance_Date__c,
                        Cancellation_Acknowledged_Date__c,
                        Cancellation_Acknowledged_By__c,
                        //RCR_CBMS_Contract__c,
                        Visible_To_Service_Providers__c,
                        Date_Approved__c,
                        Approver_User_Name__c,
                        Approver_User_ID__c,
                        Submited_For_Approval_User__c,
                        Submited_For_Approval_User_ID__c,
                        Service_to_be_provided__c 
               FROM     RCR_Contract__c
               WHERE    Id = :contractID];
    }
    
    
    private RCR_Service_Rate_Variation__c getServiceRateVariation()
    {
    	return [SELECT 	Id,
    					Name,
    					Account__c,
    					Total_Original_Service_Order_Cost__c,
    					Total_New_Service_Order_Value__c,
    					Total_Variation__c,
    					Status__c,
    					Submitted_By__c,
    					Approved_By__c,
    					Accepted_By__c,
    					(SELECT Id,
    							RCR_Service_Order__c
    					 FROM	RCR_Service_Rate_Audits__r)
    			FROM	RCR_Service_Rate_Variation__c
    			WHERE	Id = :VariationID];
    }
}