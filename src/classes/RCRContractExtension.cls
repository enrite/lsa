public without sharing class RCRContractExtension extends A2HCPageBase
{
    public RCRContractExtension(ApexPages.StandardController ctrlr)
    {
        standardController = ctrlr;
    }
    
    private transient ApexPages.StandardController standardController;
    
    public static RCR_Contract__c getContract(String serviceOrderID)
    {
        String qry = 'SELECT ';
        Schema.DescribesObjectResult result = RCR_Contract__c.sObjectType.getDescribe();
        Map<String, Schema.SObjectField> fieldsByName = result.fields.getMap();
        for(String fieldName : fieldsByName.keySet())
        {
            qry += fieldName + ',';
        } 
       
        qry +=      'Account__r.Name,' +
                    'Account__r.Available_To__c, ' +
                    'Account__r.Is_IF__c, ' +
                    'Account__r.Finance_Code__c, ' +
                    'Account__r.LP_Biller_Key__c, ' +
                    'Client__r.Name,' +  
                    'Client__r.Full_Name__c,' +  
                    'Client__r.CCMS_ID__c,' +
                    'Client__r.Title__c,' +
                    'Client__r.Account__r.Name,' +
                    'Client__r.LP_Member_Key__c, ' +
                    'Client__r.RCR_CCMS_Eligibility__c,' +
                    'Client__r.RCR_CCMS_Eligibility_Date__c,' +
                    'Client__r.RCR_CCMS_Eligibility_Office_Location__c,' +
                    'Client__r.RCR_CCMS_Eligibility_Reason__c,' +
                    'Client__r.RCR_CCMS_Eligibility_Region__c,' +
                    'Client__r.Primary_Disability__c,' +
                    'Client__r.RCR_CCMS_Independence_Scale__c,' + 
                    'Client__r.Accommodation_Setting__c,' +
                    'Client__r.Main_language_spoken_at_home__c,' +
                    'Client__r.Living_Arrangements__c,' +
                    'Client__r.Interpreter_Required__c,' +
                    'Client__r.Marital_Status__c,' +
                    'Client__r.Proficiency_in_English__c,' + 
                    'Client__r.Religious_Affiliation__c,' + 
                    'Client__r.Main_Income_Source__c,' + 
                    'Client__r.Country_of_Birth__c,' +
                    'Client__r.Labour_Force_Status__c,' +
                    'Client__r.Indigenous_Status__c,' +
                    'Client__r.Most_Effective_Communication_Method__c,' +
                    'Client__r.Ethnicity__c,' +
                    'Client__r.Preferred_Communication_Method__c,' +
                    'Service_Coordinator__r.Name,' +  
                    'Service_Coordinator__r.Phone,' +
                    'RCR_Office_Team__r.RCR_Office__c,' +  
                    'RCR_Office_Team__r.RCR_Office__r.RCR_Region__c,' + 
                    'RCR_Office_Team__r.RCR_Office__r.RCR_Region__r.Name,' +               
                    'RCR_Office_Team__r.RCR_Office__r.RCR_Region__r.Cost_Centre_Code__c,' +  
                    'RCR_Office_Team__r.RCR_Office__r.RCR_Region__r.RCR_Directorate__c,' +  
                    'RCR_Group_Agreement__r.Start_Date__c,' +
                    'RCR_Group_Agreement__r.End_Date__c,' + 
                    'RCR_Group_Agreement__r.Grant_Amount__c,' +  
                    'RCR_Group_Agreement__r.Grant_Paid_Via__c,' +
                    'RCR_Group_Agreement__r.Cost_Centre__c,' +
                    'Source_Record__r.RecordType.Name,' +
                    'Source_Record__r.Start_Date__c,' +
                    'Source_Record__r.End_Date__c,' +
                    'Source_Record__r.Total_Service_Order_Cost__c,' +
                    'Destination_Record__r.RecordType.Name,' +
                    'RecordType.Name,' +
                    'RecordType.DeveloperName,' +
                    'Owner.Name,';                                                                            
   
        qry += '(SELECT     ID,' +  
                            'Name,' +  
                            'Start_Date__c,' +  
                            'End_Date__c,' +  
                            'Original_End_Date__c,' +  
                            'Flexibility__c,' +  
                            'Expenses__c,' +
                            'Service_Types__c,' +  
                            'RCR_Service__c,' +  
                            'Service_Code__c,' +  
                            'Total_Quantity__c,' +  
                            'Total_Cost__c,' +  
                            'Exception_Count__c,' +  
                            'Use_Negotiated_Rates__c,' +  
                            'Negotiated_Rate_Standard__c,' +  
                            'Negotiated_Rate_Public_Holidays__c,' +  
                            'Public_holidays_not_allowed__c,' +  
                            'Funding_Commitment__c,' +  
                            'Source_Record__c,' +  
                            'Client__c,' +  
                            'Comment__c,' +  
                            'Internal_Comments__c,' +  
                            'GL_Category__c,' +
                            'Standard_Service_Code__c, ' +
                            'Standard_Service_Code_Description__c, ' +
                            'RCR_Service__r.Name,' +  
                            'RCR_Service__r.Description__c,' +  
                            'RCR_Service__r.Day_of_Week__c,' +  
                            'RCR_Service__r.CodeAndDescription__c,' +  
                            'RCR_Contract__r.Name' +
                ' FROM       RCR_Contract_Scheduled_Services__r' +
                ' ORDER BY CreatedDate),' +  
                '(SELECT     ID,' +
                            'Name ,' +
                            'Start_Date__c,' +
                            'End_Date__c,' +
                            'Amount__c,' +
                            'Source_Record__c' +
                ' FROM       RCR_Contract_Adhoc_Services__r),' +
                '(SELECT ID,' + 
                           'Name,' + 
                           'Start_Date__c,' + 
                            'Activity_Code_Based_On_Client_Details__c,' + 
                            'Activity_Code__c,' + 
                            'Output__c,' +                             
                            'Maximum_Amount__c,' + 
                            'Cost_Centre__c,' + 
                            'RCR_Service_Order__c,' + 
                            'RCR_Contract_Scheduled_Service__c,' + 
                            'Total_Approved__c' +                             
                    ' FROM  RCR_Funding_Details__r)' +
//                    '(SELECT  ID,' +
//                            'Name,' +
//                            'Amount__c,' +
//                            'FYE_Amount__c,' +
//                            'Allocation_Source__c,' +
//                            'Allocation_Type__c,' +
//                            'Status__c,' +
//                            'Reason__c,' +
//                            'Region__c,' +
//                            'Service_Type__c,' +
//                            'RCR_Service_Order__c,' +
//                            'Client__c,' +
//                            'Financial_Year__c,' +
//                            'Financial_Year__r.Name' +
//                    ' FROM    IF_Allocations__r' +
//                    ' ORDER BY Financial_Year__r.Name)' +
        ' FROM    RCR_Contract__c' +  
        ' WHERE   ID=\'' +  String.escapeSingleQuotes(serviceOrderID) + '\'';     
        return (RCR_Contract__c)Database.query(qry);
    }

    public PageReference recalulateServiceOrderSubmitted()    
    {
        try
        {
            RCRReconcileInvoice.setServiceTotals(new Set<ID>{standardController.getId()}); 
            Pagereference pg = Page.RCRServiceOrder;
            pg.getParameters().put('id', standardController.getId());
            return pg.setRedirect(true);
        }
        catch (Exception ex)
        {
            return A2HCException.formatException(ex);
        }        
    }     
    
    
}