/**
 * Created by me on 09/08/2019.
 */

public with sharing class MyWayExtension extends A2HCPageBase
{
    public MyWayExtension(ApexPages.StandardController ctrlr)
    {
        ServiceOrder = (RCR_Contract__c)ctrlr.getRecord();
    }

    private RCR_Contract__c ServiceOrder
    {
        get;
        set;
    }

    public Boolean ShowRegenerateMyWay
    {
        get
        {
            return RejectedInvoice.ID != null &&
                    (IsAdminProfile ||
                            CurrentUser.UserRole.DeveloperName == 'Service_Planners' ||
                            CurrentUser.UserRole.DeveloperName == 'Lead_Service_Planner' ||
                            CurrentUser.UserRole.DeveloperName == 'Manager_ASD' ||
                            CurrentUser.UserRole.DeveloperName == 'CE');
        }
    }

    public RCR_Invoice__c RejectedInvoice
    {
        get
        {
            if(RejectedInvoice == null)
            {
                RejectedInvoice = new RCR_Invoice__c();
                for(RCR_Invoice__c inv : [SELECT ID,
                                                Name,
                                                Invoice_Number__c,
                                                Start_Date__c,
                                                End_Date__c,
                                                Service_Order__c,
                                                Service_Order__r.Name,
                                                Service_Order__r.Client_Name__c
                                        FROM    RCR_Invoice__c
                                        WHERE   SDS__c = true AND
                                                Status__c ='Rejected' AND
                                                Withdrawn_Date__c = null AND
                                                Invoice_Number__c LIKE '%-R' AND
                                                Service_Order__c = :ServiceOrder.ID AND
                                                Service_Order__c != null AND
                                                Start_Date__c >= :Date.today().toStartOfMonth()
                                        LIMIT 1])
                {
                    RejectedInvoice = inv;
                }
            }
            return RejectedInvoice;
        }
        private set;
    }

    public void recreateMyWayInvoice()
    {
        Savepoint sp = Database.setSavepoint();
        try
        {
            List<RCR_Invoice_Item__c> items = [SELECT ID
                                            FROM    RCR_Invoice_Item__c
                                            WHERE   RCR_Invoice__c = :RejectedInvoice.ID];
            for(RCR_Invoice_Item__c item : items)
            {
                item.Withdrawn__c = true;
            }
            update items;
            RejectedInvoice.Withdrawn_Reason__c = 'MyWay statement to be regenerated';
            RejectedInvoice.Withdrawn_Date__c = DateTime.now();
            RejectedInvoice.Withdrawn_By__c = UserInfo.getUserId();
            if(!RejectedInvoice.Invoice_Number__c.endsWith('-R'))
            {
                RejectedInvoice.Invoice_Number__c += '-R';
            }
            update RejectedInvoice;

            RCRReconcileInvoice.setServiceTotals(new Set<ID>{RejectedInvoice.Service_Order__c}, false);

            PaymentAuthorityBatch batch = new PaymentAuthorityBatch(RejectedInvoice.Service_Order__c, RejectedInvoice.Start_Date__c, RejectedInvoice.End_Date__c);
            Database.executeBatch(batch);
            A2HCException.formatMessage('Statement will be regenerated shortly');
        }
        catch(Exception ex)
        {
            A2HCException.formatExceptionAndRollback(sp, ex);
        }

    }


}