public with sharing class RCRCancellationOfFundedServiceExtension extends A2HCPageBase 
{
    public RCRCancellationOfFundedServiceExtension(ApexPages.StandardController controller)
    {
    	string serviceOrderID = getParameter('id');
        getServiceOrder(serviceOrderID);
    }
    
    public RCRCancellationOfFundedServiceExtension()
    {
        string serviceOrderID = getParameter('id'); 
        getServiceOrder(serviceOrderID);
    }
    
    public RCR_Contract__c ServiceOrder
    {
        get;
        private set;
    }
    
    private void getServiceOrder(string serviceOrderID)
    {
        if(serviceOrderID != null)
        {
            ServiceOrder = [SELECT  Id,
            						Name,
                                    Start_Date__c,
                                    End_Date__c,
                                    Region__c,
                                    Cancellation_Date__c,
                                    RCR_Cancellation_Date__c,
                                    Cancellation_Reason__c,
//                                    RCR_CBMS_Contract__r.Cancellation_Reason__c,
                                    Cancellation_Approver__c,
                                    Cancellation_Approval_Date__c,
                                    Cancellation_Acknowledged_Date__c,
                                    Cancellation_Acknowledged_By__c,
                                    Account__r.Name,
                                    Account__r.BillingStreet,
                                    Account__r.BillingCity,
                                    Account__r.BillingState,
                                    Account__r.BillingPostalCode,
                                    Account__r.Phone,
                                    Account__r.Fax,
                                    CCMS_ID__c,
                                    Client__r.Title__c,
                                    Client__r.Full_Name__c,
                                    Client__r.Address__c,
                                    Client__r.Suburb__c,
                                    Client__r.State__c,
                                    Client__r.PostCode__c,
                                    Client__r.Phone__c,
                                    Group_Program__c,
                                    Service_Coordinator__r.Name,
                                    Service_Coordinator__r.Phone,
                                    RCR_Office_Team__r.Name
                            FROM    RCR_Contract__c
                            WHERE   Id = :serviceOrderID];
        }
    }
    

}