@isTest
private class TestPlanGoalsViewRedirectExtension {
	@testSetup 
	static void setupTestData()
	{
		TestLoadFormData data = new TestLoadFormData();
		
		Id myPlanRecordType = Schema.SObjectType.Plan__c.getRecordTypeInfosByName().get('MyPlan').RecordTypeId; 

		Plan__c p = new Plan__c();
        p.Client__c = data.Client.Id;
        p.RecordTypeId = myPlanRecordType;
        p.Plan_Status__c = 'Draft';
        insert p;

        Plan_Goal__c pg = new Plan_Goal__c(Participant_Plan__c=p.id);
        insert pg;
	}

	@isTest
	static void testRedirect()
    {   
                 
        Test.StartTest();
        
        Plan_Goal__c pg = [select id from Plan_Goal__c limit 1];
        PlanGoalsViewRedirectExtension ext = new PlanGoalsViewRedirectExtension(new ApexPages.StandardController(pg));
        ext.Redir();
        
                       
        Test.StopTest();        
    }
}