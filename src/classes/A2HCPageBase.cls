public virtual class A2HCPageBase
{
    // dont implement sharing with this class, it enforces sharing
    // in any inherited classes whether you want it or not
    private static List<SelectOption> yesNo;
    private static List<SelectOption> yesNoUndefined;
    private static List<SelectOption> stList;

    @TestVisible private transient Boolean forceTestError = false;

    protected final String YES = 'Yes';
    protected final String NO = 'No';
    protected final String NONE = '--None--';
    protected final String ALL = '--All--';
    protected final string ISTRUE = 'true';
    protected final string ISFALSE = 'false';

    public Map<string, boolean> accessablePages
    {
        get
        {
            if(accessablePages == null)
            {
                accessablePages = UserUtilities.accessablePages;
            }
            return accessablePages;
        }
        private set;
    }

    public void throwTestError()
    {
        if(Test.isRunningTest() && forceTestError)
        {
            throw new A2HCException('This is a test!.');
        }
    }

//    public static User getAnyA2HCOfficer()
//    {
//        return UserUtilities.getA2HCOfficer(true, null, null, null);
//    }
//
//    public static User getA2HCOfficer(String userName, String firstName, String lastName)
//    {
//        return UserUtilities.getA2HCOfficer(false, userName, firstName, lastName);
//    }

    public static void submitForApproval(Id record, string objectLabel, Boolean notifySubmitter)
    {
        if(record == null)
        {
            return;
        }
        Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
        request.setObjectId(record);
        Approval.ProcessResult result = Approval.process(request);
        if(notifySubmitter)
        {
            EmailManager.sendApprovalProcessComplete(new String[]{UserInfo.getUserEmail()}, objectLabel, 1, (result.getInstanceStatus() == 'Approved' ? 1 : 0));
        }
    }

    public static void submitForApproval(List<sObject> records, Boolean notifySubmitter)
    {
        if(records == null || records.isEmpty())
        {
            return;
        }
        List<ID> recordIds = new List<ID>();
        String objLabel = records[0].getSObjectType().getDescribe().getLabel();
        for(sObject rec : records)
        {
            recordIds.add(rec.ID);
        }
        submitForApproval(recordIds, objLabel, notifySubmitter);
    }

    public static void submitForApproval(List<ID> recordIds, String objectLabel, Boolean notifySubmitter)
    {
        Integer apprCount = 0;
        for(ID recID : recordIds)
        {
            Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
            request.setObjectId(recID);
            try
            {
                Approval.ProcessResult result = Approval.process(request);
                apprCount = apprCount + (result.getInstanceStatus() == 'Approved' ? 1 : 0);
            }
            catch(Exception ex)
            {}
        }
        if(notifySubmitter && !recordIds.isEmpty())
        {
            EmailManager.sendApprovalProcessComplete(new String[]{UserInfo.getUserEmail()}, objectLabel, recordIds.size(), apprCount);
        }
    }

    @future
    public static void submitForApprovalFuture(List<ID> recordIds, String objectLabel, Boolean notifySubmitter)
    {
        Integer apprCount = 0;
        for(ID recID : recordIds)
        {
            Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
            request.setObjectId(recID);
            try
            {
                Approval.ProcessResult result = Approval.process(request);
                apprCount = apprCount + (result.getInstanceStatus() == 'Approved' ? 1 : 0);
            }
            catch(Exception ex)
            {}
        }
        if(notifySubmitter && !recordIds.isEmpty())
        {
            EmailManager.sendApprovalProcessComplete(new String[]{UserInfo.getUserEmail()}, objectLabel, recordIds.size(), apprCount);
        }
    }

    public String CurrentDateTime
    {
        get
        {
            return Datetime.now().format('dd/MM/yyyy hh:mm a');
        }
    }

    public String CurrentDateTimeSeconds
    {
        get
        {
            return Datetime.now().format('dd/MM/yyyy hh:mm:ss a');
        }
    }

    public String CurrentDate
    {
        get
        {
            return Date.today().format();
        }
    }

    public String ReturnURL
    {
        get;
        set;
    }

    public Boolean Is_IE
    {
        get
        {
            if(ApexPages.currentPage().getHeaders().containsKey('User-Agent'))
            {
                return ApexPages.currentPage().getHeaders().get('User-Agent').contains('MSIE');
            }
            return false;
        }
    }

    public List<SelectOption> YesNoList
    {
        get
        {
            if(yesNo == null)
            {
                yesNo = new List<SelectOption>();
                yesNo.add(new SelectOption(YES, YES));
                yesNo.add(new SelectOption(NO, NO));
            }

            return yesNo;
        }
    }

    public List<SelectOption> YesNoNoneList
    {
        get
        {
            if(YesNoNoneList == null)
            {
                YesNoNoneList = new List<SelectOption>(YesNoList);
                YesNoNoneList.add(0, new SelectOption('', NONE));
            }

            return YesNoNoneList;
        }
        private set;
    }

    public List<SelectOption> YesNoUndefinedList
    {
        get
        {
            if(yesNoUndefined == null)
            {
                yesNoUndefined = new List<SelectOption>(YesNoList);
                yesNoUndefined.add(new SelectOption('Unknown', 'Unknown'));
            }

            return yesNoUndefined;
        }
    }

    public List<SelectOption> StateList
    {
        get
        {
            if(stList == null)
            {
                stList = new List<SelectOption>();
                stList.add(new SelectOption('', NONE));
                stList.add(new SelectOption('SA', 'SA'));
                stList.add(new SelectOption('VIC', 'VIC'));
                stList.add(new SelectOption('NSW', 'NSW'));
                stList.add(new SelectOption('QLD', 'QLD'));
                stList.add(new SelectOption('TAS', 'TAS'));
                stList.add(new SelectOption('ACT', 'ACT'));
                stList.add(new SelectOption('NT', 'NT'));
                stList.add(new SelectOption('WA', 'WA'));
            }

            return stList;
        }
    }

    public List<Application_Help_Item__c> RCRHelpItems
    {
        get
        {
            if(RCRHelpItems == null)
            {
                RCRHelpItems = [SELECT  c.Name,
                                        c.Help_Heading__c,
                                        c.Help_Text__c
                                FROM    Application_Help_Item__c c
                                WHERE   c.Application__c = 'RCR'];
            }
            return RCRHelpItems;
        }
        private set;
    }

    public String RandomString
    {
        get
        {
            return A2HCUtilities.getRandomString();
        }
    }

    public String SalesforceURL
    {
        get
        {
            return ApexPages.currentPage().getHeaders().get('Host');
        }
    }

    public Boolean ShowPageDebugging
    {
        get
        {
            Page_Debugging__c db = Page_Debugging__c.getInstance(UserInfo.getProfileId());

            if(db == null || db.Debugging_On__c == null)
            {
                return false;
            }

            return db.Debugging_On__c;
        }
        set;
    }

    public Boolean ShowReportURL
    {
        get
        {
            Report_URL__c repURL = Report_URL__c.getInstance(UserInfo.getProfileId());

            if(repURL == null)
            {
                return false;
            }
            else
            {
                return repURL.URL__c != null;
            }
        }
        set;
    }

    public Boolean IsAdminProfile
    {
        get
        {
            return CurrentUser.Profile.Name == 'System Administrator' || CurrentUser.Profile.Name == 'System Administrator Modify All';
        }
    }


    public Boolean InPortal
    {
        get
        {
            return UserInfo.getUserType() == 'CspLitePortal';
        }
    }

    public User CurrentUser
    {
        get
        {
            if(CurrentUser == null)
            {
                CurrentUser = UserUtilities.getUser(UserInfo.getUserId());
            }
            return CurrentUser;
        }
        private set;
    }

    public virtual PageReference dummy()
    {
        return null;
    }

    protected List<SelectOption> getNewSelectListWithOptgroup(String optgroupName)
    {
        SelectOption opt = new SelectOption('', '<optgroup label=\'' + optgroupName + '\'></optgroup>');
        opt.setEscapeItem(false);
        return new SelectOption[]{opt};
    }

    protected List<SelectOption> getNewSelectList(Boolean addNoneOption)
    {
        if(addNoneOption)
        {
            return getNewSelectList(NONE);
        }
        else
        {
            return getNewSelectList('');
        }
    }

    protected List<SelectOption> getNewSelectList(String firstOption)
    {
        List<SelectOption> lst = new List<SelectOption>();
        if(!String.isBlank(firstOption))
        {
            lst.add(new SelectOption('', firstOption));
        }
        return lst;
    }

    protected List<SelectOption> getSelectListFromPicklist(Schema.DescribeFieldResult field, Boolean addNoneOption)
    {
        List<SelectOption> optionList = getNewSelectList(addNoneOption);
        optionList.addAll(getSelectListFromPicklist(field));
        return optionList;
    }

    protected List<SelectOption> getSelectListFromPicklist(Schema.DescribeFieldResult field, String firstOption)
    {
        List<SelectOption> optionList = getNewSelectList(false);
        optionList.addAll(getSelectListFromPicklist(field));
        optionList.add(0, new SelectOption('', firstOption));
        return optionList;
    }

    protected List<SelectOption> getSelectListFromPicklist(Schema.DescribeFieldResult field, Boolean addNoneOption, String excludeValue)
    {
        List<SelectOption> optionList = getNewSelectList(addNoneOption);
        optionList.addAll(getSelectListFromPicklist(field));
        if(excludeValue != null)
        {
            for(Integer i = optionList.size() - 1; i >= 0; i--)
            {
                if(optionList[i].getValue() == excludeValue)
                {
                    optionList.remove(i);
                }
            }
        }
        return optionList;
    }

    protected List<SelectOption> getSelectListFromPicklist(Schema.DescribeFieldResult field)
    {
        List<Schema.Picklistentry> opts = field.getPicklistValues();
        List<SelectOption> optionList = new List<SelectOption>();
        for(Schema.Picklistentry opt : opts)
        {
            optionList.add(new SelectOption(opt.getValue(), opt.getLabel()));
        }
        return optionList;
    }

    protected List<SelectOption> getSelectListFromObjectList(List<sObject> sObjects, Boolean addNoneOption)
    {
        return getSelectListFromObjectList(sObjects, 'ID', 'Name', addNoneOption);
    }

    protected List<SelectOption> getSelectListFromObjectList(List<sObject> sObjects, String fieldForID, String fieldForLabel, Boolean addNoneOption)
    {
        List<SelectOption> optionList = getNewSelectList(addNoneOption);
        for(sObject obj : sObjects)
        {
            String idValue = (String)obj.get(fieldForID);
            String labelValue = (String)obj.get(fieldForLabel);
            optionList.add(new SelectOption(idValue, labelValue));
        }
        return optionList;
    }

    protected PageReference getReturnPage(String recordID, String parentRecordID)
    {
        String retURL = getParameter('retURL');
        PageReference page = null;
        if(retURL != null && retURL != 'null')
        {
            page = new PageReference(retURL);
        }
        else if(parentRecordID != null)
        {
            page = new PageReference('/' + parentRecordID);
        }
        if(page != null)
        {
            ReturnURL = page.getUrl();
        }
        return page;
    }

    protected void deleteAttachments(String parentRecordId, String attachmentName)
    {
        delete [SELECT  ID
                FROM    Attachment
                WHERE   ParentId = :parentRecordId AND
                        Name LIKE :(attachmentName == null ? '%' : attachmentName)];
    }

    protected PageReference getReturnPageFromURL()
    {
        String retURL = getParameter('retURL');
        String searchCriteria = getParameter('srch');
        PageReference page = null;
        if(retURL != null)
        {
            page = new PageReference(retURL + (retURL.contains('?') ? '&' : '?') + 'srch=' + (searchCriteria == null ? '' : EncodingUtil.urlEncode(searchCriteria, 'UTF-8')));
            page.setRedirect(true);
        }
        return page;
    }

    protected void deleteChildRecord(String recordId)
    {
        deleteChildRecord(recordId, false);
    }

    protected void deleteChildRecord(String recordId, Boolean ignoreError)
    {
        if(String.isEmpty(recordId))
        {
            return;
        }
        Database.DeleteResult result;
        try
        {
            result = Database.delete(recordId);
        }
        catch(Exception ex)
        {
            if(!ignoreError)
            {
                // just the first message will do
                throw new A2HCException(ex.getMessage());
            }
        }
        if(!result.isSuccess() && !ignoreError)
        {
            // just the first message will do
                throw new A2HCException(result.getErrors()[0].getMessage());
        }
    }

    protected String getParameter(String parameterName)
    {
        if(ApexPages.currentPage() == null)
        {
            return null;
        }
        return ApexPages.currentPage().getParameters().get(parameterName);
    }

    protected PageReference setParameter(PageReference pg, String parameterName, String parameterVal)
    {
        if(pg != null)
        {
            pg.getParameters().put(parameterName, parameterVal);
        }
        return pg;
    }

    protected ID getDefaultRecordTypeId(Schema.DescribesObjectResult result)
    {
        String id = null;
        for(RecordTypeInfo rti : result.getRecordTypeInfos())
        {
            if(rti.isDefaultRecordTypeMapping())
            {
                id = rti.getRecordTypeId();
                break;
            }
        }
        return id;
    }

    protected String getParentIDFromURL()
    {
        String retURL = getParameter('retURL');
        String parentID = getParentIdFromStandardURL(retURL);
        if(parentID == null)
        {
            if(retURL != null && retURL.indexOf('/') >= 0)
            {
                parentID = retURL.substring(retURL.lastIndexOf('/') + 1);
            }
        }
        return parentID;
    }

    protected boolean validateRichText(String val)
    {
        if(val != null)
        {
            //first replace all <BR> tags with \n to support new lines
            string result = val.replaceAll('<br/>', '\n');
            result = result.replaceAll('<br />', '\n');

            //regular expression to match all HTML/XML tags
            final string HTML_TAG_PATTERN = '<.*?>';

            // compile the pattern
            pattern myPattern = pattern.compile(HTML_TAG_PATTERN);

            // get your matcher instance
            matcher myMatcher = myPattern.matcher(result);

            //remove the tags
            result = myMatcher.replaceAll('');

            return result.length() > 0;
        }
        else
        {
            return false;
        }
    }

    protected void addReturnURL(PageReference pg, String retURL, String parameter)
    {
        if(retURL == null)
        {
            return;
        }
        retURL = retURL.startsWith('/') ? retURL : '/' + retURL;
        String url = retURL + (String.isBlank(parameter) ? '' : ('?' + parameter));
        pg.getParameters().put('retURL', url);
    }

    protected Approval.ProcessResult submitForApproval(ID recordID)
    {
        Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
        request.setObjectId(recordID);
        return Approval.process(request);
    }

    protected Approval.ProcessResult[] submitForApproval(List<sObject> records)
    {
        return submitRecordsForApproval(records, false);
    }

    protected Approval.ProcessResult[] submitRecordsForApproval(List<sObject> records, Boolean allOrNone)
    {
        List<Approval.ProcessSubmitRequest> requests = new List<Approval.ProcessSubmitRequest>();
        for(sObject record : records)
        {
            Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
            request.setObjectId(record.ID);
            requests.add(request);
        }
        return Approval.process(requests, allOrNone);
    }

    protected virtual PageReference submitToApprovalProcess(ID recordID)
    {
        String url = '/p/process/Submit?id=' + recordID + '&retURL=/' + recordID;
        PageReference pg = new PageReference(EncodingUtil.urlEncode(url, 'UTF-8'));
        pg.getParameters().put('retURL', (EncodingUtil.urlEncode('/' +  recordID, 'UTF-8')));
        return pg;
    }

    protected Approval.ProcessResult recallApprovalRequest(ID recordID, String comment)
    {
        for(ProcessInstanceWorkitem pwi : [SELECT   ID
                                                    FROM    ProcessInstanceWorkitem
                                                    WHERE   ProcessInstance.TargetObjectId = :recordID])
        {
            Approval.ProcessWorkitemRequest pwir = new Approval.ProcessWorkitemRequest();
            pwir.setComments(comment);
            pwir.setAction('Reject');
            pwir.setWorkitemId(pwi.ID);
            return Approval.process(pwir);
        }
        return null;
    }

    protected Approval.processResult[] processApprovalRequests(List<sObject> records, String action, String comment, Boolean allOrNone)
    {
        return A2HCUtilities.processApprovalRequests(records, action, comment, allOrNone);
    }

    protected Group getGroup(String groupDeveloperName)
    {
        return UserUtilities.getGroup(groupDeveloperName);
    }

    protected Boolean isUserInPermissionSet(String permissionSetName)
    {
        return UserUtilities.isUserInPermissionSet(permissionSetName, UserInfo.getUserId(), IsAdminProfile);
    }

    protected Boolean isUserInGroup(String groupDeveloperName, ID userId)
    {
        return UserUtilities.isUserInGroup(groupDeveloperName, userId);
    }

    protected Boolean isUserInRole(String roleDeveloperName, ID userId, Boolean includeSubordinates)
    {
        return UserUtilities.isUserInRole(roleDeveloperName, userId, includeSubordinates);
    }

    protected Boolean isNumericFieldValid(object val, String fieldName, decimal minValue, decimal maxValue, Boolean wasPreviouslyValid)
    {
        if(!isRequiredFieldValid(val, fieldName, wasPreviouslyValid, true))
        {
            return false;
        }
        decimal d = A2HCUtilities.tryParseDecimal(String.valueOf(val));
        {
            if(d == null)
            {
                return A2HCException.formatValidationException(fieldName + ' must be numeric.');
            }
            else if((minValue != null && d < minValue) ||
                        (maxValue != null && d > maxValue))
            {
                String msg = fieldName + ' must be ';
                if(maxValue == null)
                {
                    msg += 'greater than ' + minValue.toPlainString();
                }
                else if(minValue == null)
                {
                    msg += 'less than ' + maxValue.toPlainString();
                }
                else
                {
                    msg += 'between ' + minValue.toPlainString() + ' and ' + maxValue.toPlainString() + '.';
                }
                return A2HCException.formatValidationException(msg);
            }
        }
        return wasPreviouslyValid;
    }

    protected Boolean isRequiredFieldValid(sObject sObj, SObjectField f, Boolean wasPreviouslyValid)
    {
        return isRequiredFieldValid(sObj.get(f), f.getDescribe().getLabel(), null, wasPreviouslyValid, true);
    }

    protected Boolean isRequiredFieldValid(object val, String fieldName, Boolean wasPreviouslyValid)
    {
        return isRequiredFieldValid(val, fieldName, null, wasPreviouslyValid, true);
    }

    protected Boolean isRequiredFieldValid(object val, String fieldName, String msgSuffix, Boolean wasPreviouslyValid)
    {
        return isRequiredFieldValid(val, fieldName, msgSuffix, wasPreviouslyValid, true);
    }

    protected Boolean isRequiredFieldValid(object val, String fieldName, Boolean wasPreviouslyValid, Boolean displayMessage)
    {
        return  isRequiredFieldValid(val, fieldName, null, wasPreviouslyValid, displayMessage);
    }

    protected Boolean isRequiredFieldValid(object val, String fieldName, String msgSuffix, Boolean wasPreviouslyValid, Boolean displayMessage)
    {
        if(val == null)
        {
            if(displayMessage)
            {
                getRequiredFieldMessage(fieldName, msgSuffix);
            }
            return false;
        }
        return wasPreviouslyValid ? true : wasPreviouslyValid;
    }

    protected Boolean isFieldSetCompleted(sObject record, Schema.FieldSet fieldSet, Boolean previouslyValid, Boolean oneFailAllFail, Boolean displayFieldMessage)
    {
        Boolean complete = true;
        for(Schema.FieldSetMember fsm : fieldSet.getFields())
        {
            if(fsm.getType() == Schema.DisplayType.Boolean)
            {
                if(!(Boolean)record.get(fsm.getFieldPath()))
                {
                    if(oneFailAllFail)
                    {
                        return false;
                    }
                    if(displayFieldMessage)
                    {
                        getRequiredFieldMessage(fsm.getLabel());
                    }
                    complete = false;
                }
                else if(!oneFailAllFail)
                {
                    return true;
                }
            }
            else
            {
                if(!isRequiredFieldValid(record.get(fsm.getFieldPath()), fsm.getLabel(), previouslyValid, displayFieldMessage))
                {
                    if(oneFailAllFail)
                    {
                        return false;
                    }
                    complete = false;
                }
                else if(!oneFailAllFail)
                {
                    return true;
                }
            }
        }
        return complete;
    }

    protected void getRequiredFieldMessage(String fieldName)
    {
        A2HCException.formatException(fieldName + ' is required.');
    }

    protected void getRequiredFieldMessage(String fieldName, String msgSuffix)
    {
        A2HCException.formatException(fieldName + ' is required' + (msgSuffix == null ? '.' : msgSuffix + '.'));
    }

    private String getParentIdFromStandardURL(String retURL)
    {
        if(retURL == null)
        {
            return null;
        }
        URL newURL = new URL(URL.getSalesforceBaseUrl(), EncodingUtil.urlDecode(retURL, 'UTF-8'));
        String queryString = newURL.getQuery();
        if(queryString == null)
        {
            return null;
        }

        for(String s : queryString.split('&'))
        {
            if(s.startsWith('id='))
            {
                return s.substring(s.indexOf('id=') + 3);
            }
        }
        return null;
    }


}