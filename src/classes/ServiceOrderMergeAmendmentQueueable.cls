/**
 * Created by IanGoode on 07/05/2018.
 */
public class ServiceOrderMergeAmendmentQueueable implements Queueable
{
    public ServiceOrderMergeAmendmentQueueable(ID amendmentID)
    {
        this.amendmentID = amendmentID;
        amendedSo = RCRContractExtension.getContract(amendmentID);
        serviceOrder = RCRContractExtension.getContract(amendedSo.Source_Record__c);
    }

    public ServiceOrderMergeAmendmentQueueable(ID amendmentID,
            Map<ID, RCR_Contract_Scheduled_Service__c> amendedCssById,
            Map<ID, RCR_Contract_Scheduled_Service__c> existingCssById,
            Date originalStartDate,
            Date originalEndDate,
            Decimal originalTotalCost)
    {
        this(amendmentID);
        this.amendedCssById = amendedCssById;
        this.existingCssById = existingCssById;
        this.originalStartDate = originalStartDate;
        this.originalEndDate = originalEndDate;
        this.originalTotalCost = originalTotalCost;
    }

    private ID amendmentID;
    private RCR_Contract__c amendedSo;
    private RCR_Contract__c serviceOrder;
    private Map<ID, RCR_Contract_Scheduled_Service__c> amendedCssById = null;
    private Map<ID, RCR_Contract_Scheduled_Service__c> existingCssById = null;
    private Date originalStartDate;
    private Date originalEndDate;
    private Decimal originalTotalCost;

    public void execute(QueueableContext context)
    {
        Savepoint sp = Database.setSavepoint();
        try
        {
            if(originalStartDate == null || originalEndDate == null || originalTotalCost == null)
            {
                originalStartDate = serviceOrder.Start_Date__c;
                originalEndDate = serviceOrder.End_Date__c;
                originalTotalCost = serviceOrder.Total_Service_Order_Cost__c;
            }

            // first run
            if(amendedCssById == null && existingCssById == null)
            {
                Set<ID> amendedCssSourceIds = new Set<ID>();
                List<RCR_Contract_Scheduled_Service__c> cssToDelete = new List<RCR_Contract_Scheduled_Service__c>();
                copyServiceOrderFields();
                amendedCssById = new Map<ID, RCR_Contract_Scheduled_Service__c>();
                existingCssById = new Map<ID, RCR_Contract_Scheduled_Service__c>();
                for(RCR_Contract_Scheduled_Service__c amendedCss : amendedSo.RCR_Contract_Scheduled_Services__r)
                {
                    amendedCssById.put(amendedCss.ID, amendedCss);
                    if(amendedCss.Source_Record__c != null)
                    {
                        amendedCssSourceIds.add(amendedCss.Source_Record__c);
                    }
                }
                for(RCR_Contract_Scheduled_Service__c existingCss : serviceOrder.RCR_Contract_Scheduled_Services__r)
                {
                    if(amendedCssSourceIds.contains(existingCss.ID))
                    {
                        existingCssById.put(existingCss.ID, existingCss);
                    }
                    else
                    {
                        // this scheduled service has been deleted in the amendment, so delete from the service order
                        cssToDelete.add(existingCss);
                    }
                }
                delete cssToDelete;
                if(closeToLimits())
                {
                    nextQueueable();
                }
            }

            Integer cssCount = 0;
            for(RCR_Contract_Scheduled_Service__c amendedCss : amendedCssById.values())
            {
                RCR_Contract_Scheduled_Service__c existingCss = existingCssById.get(amendedCss.Source_Record__c);
                existingCss = mergeScheduledService(amendedCss, existingCss);
                existingCss = copySchedule(amendedCss, existingCss);
                existingCss = cloneServiceTypes(amendedCss, existingCss);
                update existingCss;
                calculateNewCost(existingCss);

                amendedCssById.remove(amendedCss.ID);
                cssCount++;
                if(cssCount >= Batch_Size__c.getInstance('Amendment Merge').Records__c.intValue() || closeToLimits())
                {
                    break;
                }
            }
            if(amendedCssById.isEmpty())
            {
                RCRReconcileInvoice.setServiceTotals(new Set<ID>{serviceOrder.ID}, false);
                serviceOrder = RCRContractExtension.getContract(serviceOrder.ID);

                        EmailManager.sendHTMLMessage(UserInfo.getUserEmail(),
                        'Amendment Approved',
                        'The following RCR Amendment has been approved and all the related information has now been updated into the Service Order:<br/><br/>' +
                                'Service Order: ' + serviceOrder.Name + '<br/>' +
                                'Client/Program: : ' + serviceOrder.Client__r.Name + '<br/>' +
                                'Start Date: ' + serviceOrder.Start_Date__c.format() + '<br/>' +
                                'End Date: ' + serviceOrder.End_Date__c.format() + '<br/>' +
                                'Funding Source:' + serviceOrder.Funding_Source__c + '<br/>' +
                                'Type: ' + serviceOrder.Type__c + '<br/>' +
                                'Out of Session Request: ' + (serviceOrder.Out_of_Session_Request__c ? 'Yes' : 'No') + '<br/>' +
                                URL.getSalesforceBaseUrl().toExternalForm() + '/' + serviceOrder.ID,
                        'LSA',
                        'noreply@lsa.sa.gov.au');

                if(serviceOrder.LP_Contract_Key__c != null &&
                        (serviceOrder.Start_Date__c != originalStartDate ||
                        serviceOrder.End_Date__c != originalEndDate ||
                        serviceOrder.Total_Service_Order_Cost__c != originalTotalCost))
                {
                    LPRESTInterface.alterContractFuture(serviceOrder.ID);
                }
            }
            else
            {
                nextQueueable();
            }
        }
        catch(Exception ex)
        {
            EmailManager.sendHTMLMessage(UserInfo.getUserEmail(),
                    'Amendment for Service Order ' + amendedSo.Name + ' failed',
                    'Error: ' + ex.getMessage(),
                    'LSA',
                    'noreply@lsa.sa.gov.au');
            if(sp != null)
            {
                Database.rollback(sp);
            }
        }
    }

    private void nextQueueable()
    {
        ServiceOrderMergeAmendmentQueueable nextQueueable = new ServiceOrderMergeAmendmentQueueable(amendmentID,
                                                    amendedCssById,
                                                    existingCssById,
                                                    originalStartDate,
                                                    originalEndDate,
                                                    originalTotalCost);
        System.enqueueJob(nextQueueable);
    }

    private void copyServiceOrderFields()
    {
        ID planActionId = serviceOrder.Plan_Action__c;
        A2HCUtilities.copyAllFields(amendedSo, serviceOrder, new Set<String>{'Destination_Record__c', 'Source_Record__c', 'OwnerID', 'RecordTypeID'});
        amendedSo.Destination_Record__c = serviceOrder.ID;
        amendedSo.Source_Record__c = serviceOrder.ID;
        amendedSo.Plan_Action__c = null;
        update amendedSo;

        serviceOrder.RecordTypeId = RCR_Contract__c.SObjectType.getDescribe().getRecordTypeInfosByName().get('Service Order').getRecordTypeId();
        serviceOrder.CSA_Acceptance_Date__c = Date.today();
        serviceOrder.CSA_Status__c = 'Accepted';
        serviceOrder.Visible_to_Service_Providers__c = true;
        serviceOrder.Plan_Action__c = planActionId;
        if(serviceOrder.Total_Service_Order_Cost__c != amendedSo.Total_Service_Order_Cost__c)
        {
            serviceOrder.CSA_Accepted_On_Recall__c = null;
            serviceOrder.CSA_Bulk_Approved__c = false;
            serviceOrder.CSA_Status_Recall__c = null;
            serviceOrder.Date_Approved__c = null;
        }
        serviceOrder.Participant_Plan_Review__c = false;
        update serviceOrder;

        delete [SELECT  ID
        FROM    RCR_Funding_Detail__c
        WHERE   RCR_Service_Order__c = :serviceOrder.ID];
    }


    private RCR_Contract_Scheduled_Service__c mergeScheduledService(RCR_Contract_Scheduled_Service__c amendedCss, RCR_Contract_Scheduled_Service__c existingCss)
    {
        if(existingCss == null)
        {
            existingCss = amendedCss.clone(false);
            existingCss.RCR_Contract__c = serviceOrder.ID;
            insert existingCss;
        }
        else
        {
            existingCss.RCR_Service__c = amendedCss.RCR_Service__c;
            existingCss.Start_Date__c = amendedCss.Start_Date__c;
            existingCss.Original_End_Date__c = amendedCss.Original_End_Date__c;
            existingCss.Use_Negotiated_Rates__c = amendedCss.Use_Negotiated_Rates__c;
            existingCss.Negotiated_Rate_Public_Holidays__c = amendedCss.Negotiated_Rate_Public_Holidays__c;
            existingCss.Negotiated_Rate_Standard__c = amendedCss.Negotiated_Rate_Standard__c;
            existingCss.Flexibility__c = amendedCss.Flexibility__c;
            existingCss.Funding_Commitment__c = amendedCss.Funding_Commitment__c;
            existingCss.Expenses__c = amendedCss.Expenses__c;
            existingCss.GL_Category__c = amendedCss.GL_Category__c;

            update existingCss;
        }
        return existingCss;
    }

    private RCR_Contract_Scheduled_Service__c copySchedule(RCR_Contract_Scheduled_Service__c amendedCss, RCR_Contract_Scheduled_Service__c existingCss)
    {
        Boolean scheduleFound = false;
        for(Schedule__c s : [SELECT ID,
                                    Name,
                                    Start_Date__c,
                                    End_Date__c,
                                    Schedule_Recurrence_Pattern__c,
                                    Number_of_weeks_in_period__c,
                                    Quantity_Per_Period__c,
                            (SELECT Schedule__c,
                                    Week_Number__c,
                                    Week_Of_Month__c,
                                    Monday__c,
                                    Tuesday__c,
                                    Wednesday__c,
                                    Thursday__c,
                                    Friday__c,
                                    Saturday__c,
                                    Sunday__c,
                                    Total_Quantity__c
                            FROM   Schedule_Periods__r
                            ORDER BY Week_Number__c)
                            FROM    Schedule__c s
                            WHERE   Name = :amendedCss.ID])
        {
            scheduleFound = true;

            delete [SELECT  ID
                    FROM    Schedule__c
                    WHERE   Name = :existingCss.ID];

            Schedule__c newSchedule = s.clone(false, true);
            newSchedule.Name = existingCss.ID;
            insert newSchedule;

            List<Schedule_Period__c> schedPeriods = s.Schedule_Periods__r.deepClone(false);
            for(Schedule_Period__c sp : schedPeriods)
            {
                sp.Schedule__c = newSchedule.ID;
            }
            insert schedPeriods;
        }
        if(!scheduleFound)
        {
            Schedule__c newSchedule = new Schedule__c(Name = existingCss.ID, Start_Date__c = existingCss.Start_Date__c, End_Date__c = existingCss.Original_End_Date__c);
            insert newSchedule;
        }
        return existingCss;
    }

    private RCR_Contract_Scheduled_Service__c cloneServiceTypes(RCR_Contract_Scheduled_Service__c amendedCss, RCR_Contract_Scheduled_Service__c existingCss)
    {
        delete [SELECT  Id
        FROM    RCR_Contract_Service_Type__c r
        WHERE   RCR_Contract_Scheduled_Service__c = :existingCss.ID];

        List<RCR_Contract_Service_Type__c> newContractServiceTypes = new List<RCR_Contract_Service_Type__c>();
        existingCss.Service_Types__c = '';
        for(RCR_Contract_Service_Type__c cst : [SELECT  Id,
                                                        Name,
                                                        Quantity__c,
                                                        Program_Category_Name__c,
                                                        Service_Type__c,
                                                        RCR_Contract_Scheduled_Service__c,
                                                        RCR_Program__c,
                                                        RCR_Program_Category_Service_Type__c,
                                                        RCR_Program_Category_Service_Type__r.Name
                                                FROM    RCR_Contract_Service_Type__c
                                                WHERE   RCR_Contract_Scheduled_Service__c = :amendedCss.ID
                                                ORDER BY Service_Type__c])
        {
            RCR_Contract_Service_Type__c newCST = cst.clone(false, true);
            newCST.RCR_Service_Order__c = null;
            newCST.RCR_Contract_Scheduled_Service__c = existingCss.ID;
            newContractServiceTypes.add(newCST);
            existingCss.Service_Types__c += cst.Service_Type__c +'<br/>';
        }
        insert newContractServiceTypes;
        return existingCss;
    }

    private void calculateNewCost(RCR_Contract_Scheduled_Service__c existingCss)
    {
        for(RCR_Contract_Scheduled_Service__c css : [ SELECT  ID,
                                                            Name,
                                                            Start_Date__c,
                                                            RCR_Sub_Program__c,
                                                            End_Date__c,
                                                            Original_End_Date__c,
                                                            Cancellation_Date__c,
                                                            Flexibility__c,
                                                            Public_holidays_not_allowed__c,
                                                            Comment__c,
                                                            Exception_Count__c,
                                                            Internal_Comments__c,
                                                            Total_Cost__c,
                                                            Total_Quantity__c,
                                                            Use_Negotiated_Rates__c,
                                                            Negotiated_Rate_Standard__c,
                                                            Negotiated_Rate_Public_Holidays__c,
                                                            Funding_Commitment__c,
                                                            Client__c,
                                                            Client__r.Name,
                                                            RCR_Service__c,
                                                            RCR_Service__r.Name,
                                                            RCR_Service__r.Account__c,
                                                            RCR_Contract__c,
                                                            RCR_Contract__r.Account__c,
                                                            RCR_Contract__r.Level__c,
                                                            RCR_Contract__r.Name,
                                                            RCR_Contract__r.Client__c,
                                                            RCR_Contract__r.Client__r.Name,
                                                            RCR_Contract__r.RecordType.Name,
                                                            Expenses__c,
                                                            (SELECT Date__c,
                                                                    Public_Holiday_Quantity__c,
                                                                    Standard_Quantity__c,
                                                                    RCR_Contract_Scheduled_Service__c
                                                            FROM   RCR_Scheduled_Service_Exceptions__r
                                                            ORDER BY Date__c),
                                                            (SELECT Id,
                                                                    RCR_Program__c,
                                                                    RCR_Contract_Adhoc_Service__c,
                                                                    RCR_Contract_Scheduled_Service__c,
                                                                    RCR_Program_Category_Service_Type__c,
                                                                    Quantity__c
                                                            FROM   RCR_Contract_Service_Types__r),
                                                            (SELECT    ID
                                                            FROM        RCR_Scheduled_Service_Breakdowns__r),
                                                            (SELECT     ID
                                                            FROM        RCR_Scheduled_Service_Breakdowns_By_Rate__r)
                                                    FROM   RCR_Contract_Scheduled_Service__c
                                                    WHERE  ID = :existingCss.ID])
        {
            RCRScheduledServiceExtension ext = new RCRScheduledServiceExtension(false);
            css.Total_Cost__c = ext.setTotals(css);
            ext.saveScheduledService(css);
        }
    }

    private Boolean closeToLimits()
    {
        return (Limits.getQueries() >= Limits.getLimitQueries() - 10) ||
                (Limits.getDmlRows() >= Limits.getLimitDmlRows() - 1000) ||
                (Limits.getDmlStatements() >= Limits.getLimitDmlStatements() - 10);
    }
}