public class RCRAdhocServiceOrderCloneBatch 
		extends RCRServiceOrderCloneBase 
		implements Database.Batchable<sObject>, Database.Stateful 
{	   
	public RCRAdhocServiceOrderCloneBatch(RCRServiceOrderCloneBase previousBatch) 
	{
		super(previousBatch);
	}
	
    public Database.QueryLocator start(Database.BatchableContext batch)  
    {
    	reparentBatchLogs(batch); 
    	
    	if(ScheduleId != null)
    	{
	        for(RCR_Contract__c c : [SELECT ID
	        						FROM	RCR_Contract__c
                                    WHERE   Schedule__c = :ScheduleId]) 
			{                                    
                ServiceOrderIds.add(c.ID);                  
			}
    	} 
    	return Database.getQueryLocator([SELECT Id,
												Name,
												Amount__c,
												Start_Date__c,
												End_Date__c,
												Original_End_Date__c,
												Cancellation_Date__c,
												Service_Types__c,
												Qty__c,
												Rate__c,
												Comment__c,
												Internal_Comments__c,
												Expenses__c,
												RCR_Contract__c,
                                                RCR_Contract__r.Name,
//                                                RCR_Contract__r.RCR_CBMS_Contract__c,
                                                RCR_Contract__r.Signed_By_All_Parties__c,
                                                (SELECT Id,
												        RCR_Program__c,
												        RCR_Contract_Adhoc_Service__c,
												        RCR_Contract_Scheduled_Service__c,
												        RCR_Program_Category_Service_Type__c,
												        Quantity__c
												 FROM   RCR_Contract_Service_Types__r)
                                       FROM     RCR_Contract_Adhoc_Service__c
                                       WHERE    RCR_Contract__c IN :ServiceOrderIds]);                                      
    }

    public void execute(Database.BatchableContext batch, List<RCR_Contract_Adhoc_Service__c> adhocServices) 
    {
		List<Batch_Log__c> batchLogs = new List<Batch_Log__c>();
        for(RCR_Contract_Adhoc_Service__c ahs : adhocServices)
        {        
        	System.savePoint sp = Database.setSavepoint();  
        	RCR_Contract__c existingServiceOrder = getServiceOrder(ahs.RCR_Contract__c);
        	RCR_Contract__c newServiceOrder = null;
        	try
        	{		        
		        lastModifiedUserIds.add(existingServiceOrder.LastModifiedByID);
		        
	        	newServiceOrder = processRecord(existingServiceOrder, sp, batchLogs, batch);
	        	if(newServiceOrder != null)
	        	{
		        	RCR_Contract_Adhoc_Service__c newAdhocService = cloneAdHocService(existingServiceOrder, newServiceOrder, ahs); 
		        	addBatchLog(existingServiceOrder, 
		        						batchLogs, 
		        						batch, 
		        						'Adhoc Service created: ' + newAdhocService.Start_Date__c.format() + ' to ' + newAdhocService.Start_Date__c.format() + '.', 
		        						null);  
	        	}   						
	        }
	        catch(Exception ex)
	        {
	        	handleError(existingServiceOrder,  
	        					newServiceOrder,
	        					sp, 
	        					batchLogs, 
	        					batch, 
	        					ex, 
	        					'Adhoc Service ' + ahs.Start_Date__c.format() + ' - ' + ahs.End_Date__c.format());			
	        }	       	            
        }
        upsert batchLogs;
    }

    public void finish(Database.BatchableContext info)
    {    	
    	String templateName = null;
    	if(Mode == RunMode.RolloverApproved)
    	{
    		// clear schedule here as new service orders will be locked in approval process 
    		// after next batch process
    		List<RCR_Contract__c> serviceOrders = [SELECT 	ID,
	    													Rollover_Status__c
				    		 						FROM    RCR_Contract__c
			                                       	WHERE   Destination_Record__c IN :ServiceOrderIds];
            for(RCR_Contract__c destSo : serviceOrders)
            {
    			destSo.Schedule__c = null;
    			destSo.Is_Cloning__c = false;
             }
	         DMLAsSystem.updateRecords(serviceOrders); 
	         
    		 RCRServiceOrderSubmitForCSABatch batch = new RCRServiceOrderSubmitForCSABatch(this);
    		 System.scheduleBatch(batch, 'CSA Approval', 0);
    		 return;
    	}
		else if (Mode == RunMode.Rollover)
		{
			templateName = 'Service_Order_Rollover_Complete';
		}
		else if(Mode == RunMode.RequestApproved)
		{
			//only send email if errors encountered
			for(Batch_Log__c bl : [SELECT 	Error__c
									FROM	Batch_Log__c
									WHERE	Name = :info.getJobId()])
			{		
				if(String.isNotBlank(bl.Error__c))	
				{		
					templateName = 'Request_Approval_Errors';
					break;
				}
			}
		}	
    	finaliseBatch(info, templateName); 					
    }
    
    private List<RCR_Contract_Adhoc_Service__c> getAdHocServices(RCR_Contract__c oldServiceOrder)
    {
    	return [SELECT  Start_Date__c,
                         Name,
                         Id,
                         End_Date__c, 
                         Original_End_Date__c ,
                         Amount__c,
                         Comment__c,
                         Internal_Comments__c,
                         Qty__c,
                         Rate__c,
                         Service_Types__c,
                         Funding_Commitment__c,
                         Rollover_Comment__c,
                         (SELECT    Id,
                                    RCR_Program__c,
                                    RCR_Contract_Adhoc_Service__c,
                                    RCR_Contract_Scheduled_Service__c,
                                    RCR_Program_Category_Service_Type__c,
                                    Quantity__c
                           FROM     RCR_Contract_Service_Types__r)
                 FROM    RCR_Contract_Adhoc_Service__c
                 WHERE   RCR_Contract__c = :oldServiceOrder.ID];     
    } 
    

    private RCR_Contract_Adhoc_Service__c cloneAdHocService(RCR_Contract__c oldServiceOrder, 
    										RCR_Contract__c newServiceOrder,
    										RCR_Contract_Adhoc_Service__c oldAdhocService)
    {
    	RCR_Contract_Adhoc_Service__c newAdHocService = oldAdhocService.clone(false, true);
		newAdHocService.RCR_Contract__c = newServiceOrder.Id;
        newAdHocService.Source_Record__c = oldAdhocService.ID; 
        newAdHocService.Rollover_Comment__c = null;
        if(Mode == RunMode.Rollover)
		{	       
        	setServiceDates(oldServiceOrder, newServiceOrder, oldAdhocService, newAdHocService); 
		}
   		newAdHocService.Funding_Commitment__c = APS_PicklistValues.RCRContract_FundingCommitment_Recurrent;
   		
   		if(oldAdhocService.RCR_Contract__r.Signed_By_All_Parties__c == null)
        {
        	setRolloverComment(newAdHocService, 'Contract has not been signed by both parties.');
        }		
        insert newAdHocService;
        
        cloneServiceTypes(oldAdhocService.RCR_Contract_Service_Types__r, newAdHocService.ID, 'AdHoc');        
        return newAdHocService;
    }

}