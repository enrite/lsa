@isTest
public class TestFormDetailViewRedirectExtension
{                    
    static testMethod void myTestMethod()
    {
        TestLoadFormData data = new TestLoadFormData();
                                           
        Form__c f = new Form__c();         
        insert f;
        
        Form_Detail__c ce = new Form_Detail__c(RecordTypeId = data.RecordTypes.get('Chief_Executive_Application_Assessment').Id, Form_Application_Assessment__c = f.Id);        
        insert ce;
        
        Test.setCurrentPage(Page.FormDetailEditRedirect);
        ApexPages.CurrentPage().getParameters().put('RecordType', data.RecordTypes.get('Chief_Executive_Application_Assessment').Id);
                               
        FormDetailViewRedirectExtension ext = new FormDetailViewRedirectExtension(new ApexPages.StandardController(ce));               
        ext.Redir(); 
        
        Form_Detail__c ir = new Form_Detail__c(RecordTypeId = data.RecordTypes.get('Internal_Review').Id, Form_Application_Assessment__c = f.Id);        
        insert ir;      
        
        ApexPages.CurrentPage().getParameters().put('RecordType', data.RecordTypes.get('Internal_Review').Id);
        
        ext = new FormDetailViewRedirectExtension(new ApexPages.StandardController(ir));               
        ext.Redir();                                                                           
    }
}