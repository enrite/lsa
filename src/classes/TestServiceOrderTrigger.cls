/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestServiceOrderTrigger  
{
    @IsTest
    static void myUnitTest()
    {
        TestLoadData.loadRCRData();

        Test.StartTest();

        ServiceOrderTrigger soTrigger = new ServiceOrderTrigger(null);
        RCR_Contract__c testSo = TestRCR.getTestContract();
        RCR_Contract__c oldTestSo = testSo.clone(false, true);

        testSo.Service_Request_Approval_Recalled__c = true;
        soTrigger.doCSAApproval(testSo, oldTestSo);

        testSo.Service_Request_Approval_Recalled__c = false;
        testSo.Approval_Request_Recalled__c = true;
        soTrigger.doCSAApproval(testSo, oldTestSo);

        testSo.Service_Request_Approval_Recalled__c = false;
        testSo.Approval_Request_Recalled__c = false;

        oldTestSo.CSA_Status__c = APS_PicklistValues.RCRContractCSAStatus_Pending_Approval;
        testSo.CSA_Status__c = APS_PicklistValues.RCRContractCSAStatus_Pending_Acceptance;
        soTrigger.doCSAApproval(testSo, oldTestSo);

        soTrigger.serviceProviderUsersByAccountID.put(testSo.Account__c, new List<User>{
                new User(ID = UserInfo.getUserID())
        });
        soTrigger.doCSAApproval(testSo, oldTestSo);

        oldTestSo.CSA_Status__c = APS_PicklistValues.RCRContractCSAStatus_Pending_Acceptance;
        testSo.CSA_Status__c = APS_PicklistValues.RCRContractCSAStatus_Accepted;
        testSo.Submited_For_Approval_User_ID__c = UserInfo.getUserID();
        soTrigger.doCSAApproval(testSo, oldTestSo);

        oldTestSo.CSA_Status__c = APS_PicklistValues.RCRContractCSAStatus_Pending_Approval;
        testSo.CSA_Status__c = APS_PicklistValues.RCRContractCSAStatus_Rejected;
        soTrigger.doCSAApproval(testSo, oldTestSo);

        //soTrigger.setActionRequired(null, null);

        //soTrigger.ensureSufficientENUAllocation(testSo);

        ServiceOrderTrigger.createNewServiceOrder(testSO);

        Test.StopTest();
    }
}