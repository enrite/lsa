global class A2HCUtilities
{
    public A2HCUtilities(ApexPages.StandardController stdController)
    {
    }
    
    public static Map<String, Schema.SObjectType> GlobalDescribe
    {
        get
        {
            if(GlobalDescribe == null)
            {
                GlobalDescribe = Schema.getGlobalDescribe();
            }
            return GlobalDescribe; 
        }
        private set;
    }

    public static PageReference redirectToListView(sObjectType objectType, ApexPages.StandardSetController setCont, String listName)
    {
        DescribeSObjectResult descResult = objectType.getDescribe();
        String keyPrefix = descResult.getKeyPrefix();
        String objectName = descResult.getName();
        String listId = null;
        for(SelectOption option : setCont.getListViewOptions())
        {
            if(option.getLabel() == listName)
            {
                listId = option.getValue();
                break;
            }
        }
        // not found, just get the first one
        if(isStringNullOrEmpty(listId))
        {
            for(SelectOption option : setCont.getListViewOptions())
            {
                listId = option.getValue();
                break;
            }
        }

        PageReference newPage = new PageReference('/' + keyPrefix + '?fcf=' + listId.subString(0, listId.length() - 3));
        return newPage.setRedirect(true);
    }
    
    public static PageReference redirectToStandardEdit(sObjectType objectType, string recordId, string recordTypeID)
    {
        DescribeSObjectResult descResult = objectType.getDescribe();
        String keyPrefix = descResult.getKeyPrefix();
        String objectName = descResult.getName();
        string retURl = ApexPages.currentPage().getParameters().get('retURL');
        
        string pgURL;
        
        if(recordId != null)
        {
            pgURL = recordId + '/e?nooverride=1';
        }
        else 
        {
            pgURL = '/' + keyPrefix + '/e';
        }
        
        if(recordTypeID != null)
        {
            pgURL += '?RecordType=' + recordTypeID;
        }
        
        if(retURL != null)
        {
            pgURL += '&retURL=' + retURL;
        }

        PageReference pg = new PageReference(pgURL);
        
        return pg.setRedirect(true);
    }

    public static String getAddChildRecordURL(sObjectType childObjectType, String parentNameFieldId, sObject parentRecord)
    {
        DescribeSObjectResult descResult = childObjectType.getDescribe();
        String keyPrefix = descResult.getKeyPrefix();

        String url = '/' + keyPrefix + '/e?' +
                            parentNameFieldId + '=' +
                            parentRecord.get('Name') + '&' +
                            parentNameFieldId + '_lkid=' +
                            parentRecord.ID + '&saveURL=/' +
                            parentRecord.ID + '&retURL=/' +
                            parentRecord.ID;

        return url;
    }

    public static String getRandomString()
    {
        return Math.random().format().replace('.', '');
    }

    public static Boolean isTrimmedStringNullOrEmpty(String s)
    {
        return  String.isBlank(s);
    }

    public static Boolean isStringNullOrEmpty(String s)
    {
        return String.isEmpty(s); 
    }

    public static String escapeSearchString(String s)
    {
        if(isTrimmedStringNullOrEmpty(s))
        {
            return null;
        }
        else
        {
            return String.escapeSingleQuotes(s);
        }
    }

    public static Date tryParseDate(String str)
    {
        Date dt = null;
        try
        {
             if(String.isBlank(str))
            {
                return dt;
            }
            dt = Date.parse(str.trim());
            return dt;
        }
        finally
        {
            return dt;
        }
    }

    public static decimal tryParseDecimal(String str)
    {
        decimal d = null;
        try
        {
            if(String.isBlank(str))
            {
                return d;
            }
            d = decimal.valueOf(str.replace(',', '').trim());
            return d;
        }
        finally
        {
            return d;
        }
    }

    public static Integer tryParseInteger(String str)
    {
        Integer i = null;
        if(tryParseDecimal(str) == null)
        {
            return i;
        }
        return tryParseDecimal(str).intValue();
    }
    
    public static String decimalToCurrency(Decimal val)
    {
        return decimalToCurrency(val, true);
    }
    
    public static String decimalToCurrency(Decimal val, boolean addDollarSign)
    {
        string str;
        Integer dec;
        
        if(val == null)
        {
            str = '';
        }
        else if(val == 0)
        {
            str = '0.00';
        }
        else
        {
            str = Math.abs(val.setScale(2, System.Roundingmode.HALF_UP)).format();
            str = str.contains('.') ? str : (str + '.00');         
            while(str.substringAfter('.').length() < 2)
            {
                str += '0';             
            }
        }
        return (val < 0 ? '-' : '') + (addDollarSign && val != null ? '$' : '') + str;
    }

    public static Boolean isDateBetween(Date theDate, Date startDate, Date endDate)
    {
        if(theDate == null || startDate == null || endDate == null)
        {
            return false;
        }
        return (theDate.daysBetween(startDate) <= 0 &&
                            theDate.daysBetween(endDate) >= 0);
    }

    public static boolean dateRangesOverlap(Date startDate1, Date endDate1, Date startDate2, Date endDate2)
    {
        if(startDate1 == null || endDate1 == null || startDate2 == null || endDate2 == null)
        {
            return false;
        }
        return ((startDate1 <= endDate2 && startDate2 <= endDate1) ||
                (startDate1 >= startDate2 && endDate1 <= endDate2));
    }

    public static boolean isDateInDaysOfWeek(Date theDate, String daysOfWeek)
    {
        if(theDate == null || daysOfWeek == null)
        {
            return false;
        }
        return daysOfWeek.contains(getDayOfWeek(theDate));
    }

    public static boolean dateListIncludesDateWithinPeriod(Set<Date> dates, Date startDate, Date endDate)
    {
        Date tempDate = startDate;

        while(tempDate <= endDate)
        {
            if(dates.contains(tempDate))
            {
                return true;
            }
            tempDate = tempDate.addDays(1);
        }
        return false;
    }

    public static String getDayOfWeek(DateTime theDateTime)
    {
        return theDateTime.format('EEEE');
    }

    public static String getDayOfWeek(Date theDate)
    {
        DateTime theDateTime = DateTime.newInstance(theDate, Time.newInstance(12,0,0,0));
        return getDayOfWeek(theDateTime);
    }

    public static String handleEmptyString(String str)
    {
        if(str == null || str == 'null')
        {
            return '';
        }
        else
        {
            return str;
        }
    }

    public static User getAnyUserForAccount(String accID)
    {
        User usr = new User();

        for(User u :[SELECT ID
                    FROM    User
                    WHERE   AccountID = :accID
                    LIMIT 1])
        {
            usr = u;
        }
        return usr;
    }

    public static User getAnyPortalUserForAccount(String accID)
    {
        User usr = new User();
        for(User u :[SELECT ID
                    FROM    User
                    WHERE   AccountID = :accID AND
                            UserType = 'CSPLitePortal'
                    LIMIT 1])
        {
            usr = u;
        }
        return usr;
    }

    public static Date parseHTMLRequestDate(String dateString)
    {
        // date fields passed as request parameters are in the format
        // Wed Sep 28 00:00:00 GMT 2011
        String[] parts = dateString.split(' ');
        return Date.parse(parts[2] + '/' + getMonth(parts[1]) + '/' + parts[5]);
    }

    public static Datetime parseHTMLRequestDateTime(String dateString)
    {
        // date fields passed as request parameters are in the format
        // Wed Sep 28 00:00:00 GMT 2011
        String[] parts = dateString.split(' ');
        return DateTime.valueOf(parts[5] + '-' + getMonth(parts[1]) + '-' + parts[2] + ' ' + parts[3]);
    }

    public static Date getEarliestDate(List<Date> dts)
    {
        if(dts == null || dts.size() == 0)
        {
            return null;
        }
        dts.sort();
        return dts[0];
    }

    public static Date getLatestDate(List<Date> dts)
    {
        if(dts == null || dts.size() == 0)
        {
            return null;
        }
        dts.sort();
        return dts[dts.size() - 1];
    }

    public static Date getEndOfFinancialYear(Date d)
    {
        if(d == null)
        {
            return null;
        }
        Integer year = d.year();
        if(d.month() > 6)
        {
            year++;
        }

        return Date.newInstance(year, 6, 30);
    }
       
    public static String getFinancialYearDesc(Date d)
    {
        if(d == null)
        {
            return null;
        }
        d =  getEndOfFinancialYear(d);
        Date finYearStart = d.addYears(-1).addDays(1);
        return (finYearStart.year().format() + ' - ' + d.year().format()).replace(',', '');
    }
    
    public static Date getStartOfFinancialYear(Date d)
    {
        return getEndOfFinancialYear(d).addYears(-1).addDays(1);
    }

     public static List<String> getFinancialYears(Date startDate, Date endDate)
     {
        List<String> years = new List<String>();
        String finYear = A2HCUtilities.getFinancialYearDesc(startDate);
        String lastFinYear = A2HCUtilities.getFinancialYearDesc(endDate);               
        years.add(finYear);
        
        startDate = startDate.addYears(1).addDays(-1);
        finYear = A2HCUtilities.getFinancialYearDesc(startDate);
        while(finYear != lastFinYear && startDate < endDate)
        {
            startDate = startDate.addYears(1).addDays(-1);
            finYear = A2HCUtilities.getFinancialYearDesc(startDate);
            years.add(finYear);
        }
        return years;
     }
     
     public static List<Integer> getSFFiscalYears(Date startDate, Date endDate)
     {
        List<Integer> fiscalYearsSF = new List<Integer>();
        Date finYrStart = A2HCUtilities.getStartOfFinancialYear(startDate);
        Date finYrEnd = A2HCUtilities.getEndOfFinancialYear(endDate);
        while(finYrStart < finYrEnd)
        {           
            fiscalYearsSF.add(finYrStart.year());
            finYrStart = finYrStart.addYears(1);
        }       
        return fiscalYearsSF;
     }

     public static void sortList(List<sObject> items, String sortField, String sortDirection)
     {
        if(sortField == null || sortDirection == null || items == null || items.isEmpty())
        {
            return;
        }
        List<sObject> resultList = new List<sObject>();
        //Create a map that can be used for sorting
        Map<object, List<sObject>> objectMap = new Map<object, List<sObject>>();
        for(sObject ob : items)
        {
            if(ob.get(sortField) == null)
            {
                continue;
            }
            if(objectMap.get(ob.get(sortField)) == null)
            {  // For non Sobject use obj.ProperyName
                objectMap.put(ob.get(sortField), new List<Sobject>());
            }
            objectMap.get(ob.get(sortField)).add(ob);
        }
        //Sort the keys
        List<object> keys = new List<object>(objectMap.keySet());
        keys.sort();
        for(object key : keys)
        {
            resultList.addAll(objectMap.get(key));
        }
        //Apply the sorted values to the source list
        items.clear();
        if(sortDirection.toLowerCase() == 'asc')
        {
            for(sObject ob : resultList)
            {
                items.add(ob);
            }
        }
        else if(sortDirection.toLowerCase() == 'desc')
        {
            for(integer i = resultList.size()-1; i >= 0; i--)
            {
                items.add(resultList[i]);
            }
        }
    }

    public static String toInitCase(String s)
    {
        if(s == null)
        {
            return null;
        }

        s = s.trim();
        if(s.length() == 0)
        {
            return null;
        }

        String retString = '';
        for(String p : s.split(' '))
        {
            retString += p.substring(0, 1).toUpperCase() + p.substring(1).toLowerCase() + ' ';
        }
        return retString.trim();
    }

    public static String concatStrings(String s, String s2)
    {
        if(s == null)
        {
            return s2;
        }
        if(s2 == null)
        {
            return s;
        }
        return s + s2;
    }
    
    public static Boolean multiPicklistValueContainsString(String picklistVal, String searchVal)
    {   
        if(picklistVal == null || searchVal == null)
        {
            return false;
        }
        // if only one value selected or last value in picklist
        if(!picklistVal.endsWith(';'))
        {
            picklistVal += ';';
        }
        return picklistVal.contains(searchVal);
    }

    public static RecordType getRecordType(String sObjectName, String recordTypeName)
    {
        List<RecordType> rts = getRecordTypesFromMap(sObjectName);
        if(rts == null)
        {
            throw new A2HCException('Record types for object ' + sObjectName + ' not found.');
        }
        for(RecordType rt : rts)
        {
            if(recordTypeName == rt.Name)
            {
                return rt;
            }
        }
        throw new A2HCException('Record type ' + recordTypeName + ' for object ' + sObjectName + ' not found.');
    }

    public static List<RecordType> getRecordTypes(String sObjectName)
    {
        return getRecordTypesFromMap(sObjectName);
    }
    
    public static Boolean isSandbox() 
    {
        String s  =  System.URL.getSalesforceBaseUrl().getHost();
        return (Pattern.matches('(.*\\.)?cs[0-9]*?\\..*force.com', s));
    }
    
    public static void copyAllFields(sObject copyFromRecord, sObject copyToRecord)
    {
        copyAllFields(copyFromRecord, copyToRecord, new Set<String>());
    }
    
    public static void copyAllFields(sObject copyFromRecord, sObject copyToRecord, Set<String> fieldsToExclude)
    {
        Schema.DescribesObjectResult result = copyFromRecord.getSObjectType().getDescribe();
        Map<String, Schema.SObjectField> fieldsByName = result.fields.getMap();
        for(Schema.SObjectField field : fieldsByName.values())
        {
            Schema.DescribeFieldResult fieldDescribe = field.getDescribe();
            if(fieldDescribe.isUpdateable() && !fieldsToExclude.contains(fieldDescribe.getName()))
            {
                copyToRecord.put(field, copyFromRecord.get(field));
            }
        } 
    }
    
    public static List<sObject> cloneRelatedList(sObject newParentRecord, 
                                                List<sObject> relatedRecords, 
                                                DescribeFieldResult relatedFieldDescResult,
                                                Boolean insertNewRecords)
    {
        List<sObject> newRelatedRecords = relatedRecords.deepClone(false, true);
        for(sObject relatedRecord : newRelatedRecords)
        {           
            relatedRecord.put(relatedFieldDescResult.getName(), newParentRecord.ID);
        }       
        if(insertNewRecords)
        {
            insert newRelatedRecords;
        }   
        return newRelatedRecords;
    }
    
    public static String getDateString(Date d)
    {
        return d == null ? '' : d.format();
    }
    
    public static String getDateString(DateTime dt)
    {
        return getDateString(dt == null ? (Date)null : dt.date());
    }
    
    public static String getAllFieldsQueryString(sObjectType objType, ID objID)
    {
        return getAllFieldsQueryString(objType, objID, null);
    }
    
    public static String getAllFieldsQueryString(sObjectType objType, ID objID, String[] relationshipFields)
    {
        final string qryStart = 'SELECT ';
        Schema.DescribesObjectResult result;
        try
        {
            result = objType.getDescribe();
        }
        catch(Exception ex)
        {
            throw new A2HCException('Object type not valid.');
        }
        String qryString = qryStart;
        for(String fieldName : result.fields.getMap().keySet())
        {
            qryString += fieldName + ',';
        }
        if(qryString == qryStart)
        {
            throw new A2HCException('No fields found for object type.');
        }
        if(relationshipFields != null)
        {
            for(String s : relationshipFields)
            {
                qryString += s + ',';
            }
        }
        return qryString.removeEnd(',') + ' FROM ' + result.getName() + ' WHERE ID = \'' + objID + '\'' ;
    }
    
    public static Approval.processResult[] processApprovalRequests(List<sObject> records, String action, String comment, Boolean allOrNone)
    {
        if(action != 'Approve' && action != 'Reject')
        {
            throw new A2HCException('Invalid approval action requested.');
        }
        List<Approval.ProcessWorkitemRequest> workItemRequests = new List<Approval.ProcessWorkitemRequest>();
        for(ProcessInstanceWorkitem pwi : [SELECT   ID
                                                FROM    ProcessInstanceWorkitem
                                                WHERE   ProcessInstance.TargetObjectId IN :records])
        {
            Approval.ProcessWorkitemRequest pwr = new Approval.ProcessWorkitemRequest();
            pwr.setWorkitemId(pwi.ID);
            pwr.setAction(action);
            pwr.setComments(comment);
            workItemRequests.add(pwr);
        }            
        return Approval.process(workItemRequests, allOrNone);
    }
    
    private static List<RecordType> getRecordTypesFromMap(String sObjectName)
    {
        if(!RecordTypesByObject.containsKey(sObjectName))
        {
            sObjectType objType = GlobalDescribe.get(sObjectName);      
            for(RecordTypeInfo rti : objType.getDescribe().getRecordTypeInfos())
            {
                if(!RecordTypesByObject.containsKey(sObjectName))
                {
                    RecordTypesByObject.put(sObjectName, new List<RecordType>());
                }
                RecordTypesByObject.get(sObjectName).add(new RecordType(ID = rti.getRecordTypeID(), Name = rti.getName())); 
            }
        }
        return RecordTypesByObject.get(sObjectName);
    }

    private static Map<String, List<RecordType>> RecordTypesByObject
    {
        get
        {
            if(RecordTypesByObject == null)
            {
                RecordTypesByObject = new Map<String, List<RecordType>>();
            }
            return RecordTypesByObject;
        }
        set;
    }


    private static String getMonth(String month)
    {
        if(month == null)
        {
            return month;
        }
        month = month.toLowerCase();
        if(month.startsWith('jan'))
        {
            return '1';
        }
        else if(month.startsWith('feb'))
        {
            return '2';
        }
        else if(month.startsWith('mar'))
        {
            return '3';
        }
        else if(month.startsWith('apr'))
        {
            return '4';
        }
        else if(month.startsWith('may'))
        {
            return '5';
        }
        else if(month.startsWith('jun'))
        {
            return '6';
        }
        else if(month.startsWith('jul'))
        {
            return '7';
        }
        else if(month.startsWith('aug'))
        {
            return '8';
        }
        else if(month.startsWith('sep'))
        {
            return '9';
        }
        else if(month.startsWith('oct'))
        {
            return '10';
        }
        else if(month.startsWith('nov'))
        {
            return '11';
        }
        else if(month.startsWith('dec'))
        {
            return '12';
        }
        else
        {
            return null;
        }
    }
    
    public static Database.SaveResult[] reassignApprovalRequests(List<sObject> records, ID currentAssigneeId, ID newAssigneeId)
    {       
        List<Approval.ProcessWorkitemRequest> requests = new List<Approval.ProcessWorkitemRequest>();
        
        List<ProcessInstanceWorkitem> currentApprovals = [SELECT    ID,
                                                    ProcessInstance.TargetObjectId
                                            FROM    ProcessInstanceWorkitem 
                                            WHERE   ProcessInstance.TargetObjectId IN :records AND
                                                    ActorId = :currentAssigneeId
                                                    ];
        for(ProcessInstanceWorkitem pwi : currentApprovals)     
        {
            pwi.ActorId = newAssigneeId;
        }       
        return Database.update(currentApprovals);
    }
    
    public static String GenerateKey(Integer len)
    {
        Blob blobKey = crypto.generateAesKey(128);
        String key = EncodingUtil.convertToHex(blobKey);
        return key.substring(0, len);
    }    
}