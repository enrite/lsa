public with sharing class LPContractResponseJSON
{
    public cls_links links;
    public String contractKey;	//40c46d83-d67e-010f-0f2f-5b7f705a2887
    public cls_biller biller;
    public String startDate;	//2018-01-01
    public String endDate;	//2018-06-01
    public cls_budget budget;
    public String contractNumber;
    public cls_preauthorizations[] preauthorizations;
    public String title;	//Establishment Fee For Personal Care/Community Access

    class cls_links
    {
        public cls_self self;
        public cls_curies curies;
    }

    class cls_self
    {
        public String href;	//https://api.sandbox.lanternpay.com/members/4551f321-b87b-48f3-8d46-7d9188ad2b57/contracts
    }

    class cls_curies
    {
        public String name;	//lp
        public String href;	//https://docs.lanternpay.com/reference#{rel}
        public boolean templated;
    }

    class cls_biller {
        public String billerKey;	//39487bab-df9a-060c-3556-973c71d19463
        public String name;	//Dodgy brothers
    }

    class cls_budget {
        public Decimal total;	//0
        public Decimal spent;	//0
        public Decimal unspent;	//0
    }

    class cls_preauthorizations {
        public cls_budget budget;
        public cls_quantity quantity;
        public cls_hours hours;
        public cls_item item;
        public String description;	//Establishment Fee for Personal Care/Community Access (20 hours per month).
    }

    class cls_quantity {
        public Decimal total;	//0
        public Decimal used;	//0
        public Decimal remaining;	//0
    }

    class cls_hours {
        public String total;	//0:00
        public String used;	//0:00
        public String remaining;	//0:00
    }

    class cls_item {
        public String key;	//60857e4c-24f3-4565-b931-d4ec178b16ef
        public String code;	//01_049_0107_1_1
        public String name;	//Establishment Fee For Personal Care/Community Access
        public Decimal unitPrice;	//123
    }

    public static LPContractResponseJSON parse(String jsonString)
    {
        jsonString = jsonString.replace('"_links"', '"links"');
        return (LPContractResponseJSON)JSON.deserialize(jsonString, LPContractResponseJSON.class);
    }

//    static testMethod void testParse() {
//        String json=		'{"_links":{"self":{"href":"https://api.sandbox.lanternpay.com/members/4551f321-b87b-48f3-8d46-7d9188ad2b57/contracts"},"curies":{"name":"lp","href":"https://docs.lanternpay.com/reference#{rel}","templated":true}},"contractKey":"40c46d83-d67e-010f-0f2f-5b7f705a2887","biller":{"billerKey":"39487bab-df9a-060c-3556-973c71d19463","name":"Dodgy brothers"},"startDate":"2018-01-01","endDate":"2018-06-01","budget":{"total":3207,"spent":0,"unspent":3207},"contractNumber":null,"preauthorizations":[{"budget":{"total":0,"spent":0,"unspent":0},"quantity":{"total":0,"used":0,"remaining":0},"hours":{"total":"0:00","used":"0:00","remaining":"0:00"},"item":{"key":"60857e4c-24f3-4565-b931-d4ec178b16ef","code":"01_049_0107_1_1","name":"Establishment Fee For Personal Care/Community Access","unitPrice":123},"description":"Establishment Fee for Personal Care/Community Access (20 hours per month)."}],"title":"Establishment Fee For Personal Care/Community Access"}';
//        fromJSON obj = parse(json);
//        System.assert(obj != null);
//    }
}