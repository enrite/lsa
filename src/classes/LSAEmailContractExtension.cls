public with sharing class LSAEmailContractExtension {
    RCR_Contract__c record;
    
    public LSAEmailContractExtension(ApexPages.StandardController con) {
        record = (RCR_Contract__c)con.getRecord();
    }

    public ID contactId{get;set;}

    public List<SelectOption> Contacts{
        get{
            if(Contacts==null)
            {
                Contacts = new List<SelectOption>();
                List<Contact> cList = [SELECT Id, Name, Email FROM Contact where AccountId = :record.Account__C and Email <> null];
                for(Contact c : cList)
                {
                    Contacts.add(new SelectOption(c.Id, c.Name+' <'+c.Email+'>'));
                }
            }
            return Contacts;
        }set;
    }

    public void email()
    {
        try
        {
            String serviceOrderId = record.Id;
            RCR_Contract__c contract = [SELECT Id,
                                                Name,
                                                Attendant_Care_Support__c,
                                                RecordType.DeveloperName,
                                                Plan_Action__r.RecordType.DeveloperName,
                                                Plan_Action__r.Participant_Plan__r.Plan_Status__c,
                                                Plan_Action__r.Participant_Plan__r.RecordType.DeveloperName
                                        FROM    RCR_Contract__c
                                        WHERE   ID = :serviceOrderId];
            EmailTemplate template = [SELECT    Id,
                                                Subject
                                        FROM    EmailTemplate
                                        WHERE    Name = 'LSA Contract Email'
                                        LIMIT 1];

             // Reference the attachment page, pass in the account ID
            //PageReference pdf = Page.LSAContractPDF;

            LSAContractPDFRedirectExtension ext = new LSAContractPDFRedirectExtension(new ApexPages.StandardController(contract));
            PageReference pdf = ext.redirect();

            // add the parameters for this page to the new page reference
           // pdf.getParameters().put('id',serviceOrderId);
            //pdf.setRedirect(true);

            // Take the PDF content
            Blob b = !Test.IsRunningTest() ? pdf.getContent() : Blob.valueOf('UNIT.TEST');

            // Create the email attachment
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName(contract.Name+'.pdf');
            efa.setBody(b);

            OrgWideEmailAddress owa = [select id, Address from OrgWideEmailAddress where displayname = 'LSA Financials' LIMIT 1];
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage(); 

            // Sets the paramaters of the email
            email.setTargetObjectId(contactId); 
            email.setOrgWideEmailAddressId(owa.id);
            email.setTemplateID(template.Id);
            email.setWhatId(serviceOrderId);
            email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
            email.setSaveAsActivity(true);

            // Sends the email
            Messaging.SendEmailResult [] results;
            results = Test.isRunningTest() ? new List<Messaging.SendEmailResult>() : Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});

            for(Messaging.SendEmailResult result : results)
            {
                if(result.isSuccess())
                {
                    Task t = [SELECT Id
                            FROM    Task
                            WHERE   Activitydate = TODAY AND
                                    WhatId = :serviceOrderId AND
                                    WhoId = :contactId
                            ORDER BY CreatedDate DESC
                            LIMIT 1];

                    EmailMessage em = [SELECT ID
                                        FROM    EmailMessage
                                        WHERE   ActivityId = :t.ID
                                        ORDER BY CreatedDate DESC
                                        LIMIT 1];

                    Attachment att = new Attachment();
                    att.ContentType = 'application/pdf';
                    att.Body = b;
                    att.ParentId = em.Id;
                    att.Name = contract.Name+'.pdf';

                    insert att;
                }
            }
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'The Contract has been sent successfully to the selected contact.'));
        }
        catch(Exception ex)
        {
            A2HCException.formatException(ApexPages.Severity.ERROR, ex);
        }
    }
}