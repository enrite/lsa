/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestSchedule {

    @IsTest
    private static void ScheduleDateEngineTest1()
    {
        TestLoadData.loadRCRData();
                
        Test.StartTest();
    
        TestLoadData.loadAdhocServices();
    
        RCR_Contract_Scheduled_Service__c css = null;
        
        Set<String> scheduleNames = new Set<String>();
        for (Schedule__c s : [SELECT Name
                              FROM Schedule__c])
        {
            scheduleNames.add(s.Name);
        }                                                            
        
        for(RCR_Contract_Scheduled_Service__c c : [SELECT   ID,
                                                            Name,
                                                            Start_Date__c,
                                                            End_Date__c
                                                    FROM    RCR_Contract_Scheduled_Service__c
                                                    WHERE   Total_Cost__c > 0
                                                    AND Id IN :scheduleNames
                                                    LIMIT 1])
        {
            css = c;
        }
        
        if(css == null)
        {
            return;
        }
        
        ScheduleDateEngine dtEngine = new ScheduleDateEngine(css.ID);
        
        dtEngine.getSchedulePeriodEndDate(css.Start_Date__c, SchedulePeriod.Current);
        dtEngine.getSchedulePeriodEndDate(css.Start_Date__c, SchedulePeriod.Previous);
        
        dtEngine.getSchedulePeriodEndDate(css.ID);
        
        dtEngine.getSchedulePeriodServiceStartDate(css.ID);
        
        dtEngine.getSchedulePeriodStartDate(css.Start_Date__c, SchedulePeriod.Current);
        dtEngine.getSchedulePeriodStartDate(css.Start_Date__c, SchedulePeriod.Previous);
        dtEngine.getSchedulePeriodStartDate(dtEngine.Schedules[0], css.Start_Date__c, SchedulePeriod.Current);
        
        ScheduleDateEngine.getNumberOfWeeksFromDescription('1 Week');
        ScheduleDateEngine.getNumberOfWeeksFromDescription('2 Weeks');
        ScheduleDateEngine.getNumberOfWeeksFromDescription('3 Weeks'); 
        ScheduleDateEngine.getNumberOfWeeksFromDescription('4 Weeks');
        ScheduleDateEngine.getNumberOfWeeksFromDescription('test');
        
        system.debug(dtEngine.ScheduleCalendarDays);
        
        system.debug(dtEngine.ScheduledServiceExceptions);
        
        system.debug(dtEngine.ScheduleParentLoadByCode);
        
        system.debug(dtEngine.ScheduleQuantityByDay);
        
        system.debug(dtEngine.Schedules);
        
        system.debug(dtEngine.exceptionsByServiceCode);
        
        
        Map<Id, string> idNamesMap = new Map<Id, string>();
        idNamesMap.put(css.ID, css.Name);       
        dtEngine = new ScheduleDateEngine(idNamesMap, css.Start_Date__c, css.End_Date__c);
        
        dtEngine = new ScheduleDateEngine(css.ID, css.Start_Date__c, css.End_Date__c);
        
        List<Id> pParentIds = new Id[] {css.ID};
        dtEngine = new ScheduleDateEngine(pParentIds, css.Start_Date__c, css.End_Date__c);
        
        //dtEngine = new ScheduleDateEngine(dtEngine.Schedules[0]);
        
        Test.StopTest();
    }
    
    @IsTest
    private static void ScheduleDateEngineTest2()
    {
        TestLoadData.loadRCRData();
                
        Test.StartTest();
        
        TestLoadData.loadAdhocServices();
        
        Schedule__c schedule = [SELECT  Name
                                FROM    Schedule__c
                                //WHERE   Number_of_weeks_in_period__c = 'Service Date Range'
                                LIMIT 1];

        RCR_Contract_Scheduled_Service__c css = null;       
        for(RCR_Contract_Scheduled_Service__c c : [SELECT   ID,
                                                            Name,
                                                            Start_Date__c,
                                                            End_Date__c
                                                    FROM    RCR_Contract_Scheduled_Service__c
                                                    WHERE   ID = :schedule.Name
                                                    LIMIT 1])
        {
            css = c;
        }
        
        if(css == null)
        {
            return;
        }
        
        ScheduleDateEngine dtEngine = new ScheduleDateEngine(css.ID);
        
        dtEngine.getSchedulePeriodStartDate(dtEngine.Schedules[0], css.Start_Date__c, SchedulePeriod.Current);
        dtEngine.getSchedulePeriodEndDate(css.Start_Date__c, SchedulePeriod.Current);
        
        system.debug(dtEngine.ScheduleCalendarDays);
        
        system.debug(dtEngine.ScheduledServiceExceptions);
        
        system.debug(dtEngine.ScheduleParentLoadByCode);
        
        system.debug(dtEngine.ScheduleQuantityByDay);
        
        system.debug(dtEngine.Schedules);
        
        system.debug(dtEngine.exceptionsByServiceCode);
        
        Test.StopTest();
    }
    
    @IsTest
    private static void ScheduleDateEngineTest3()
    {
        TestLoadData.loadRCRData();
                
        Test.StartTest();
        
        TestLoadData.loadAdhocServices();
        
        Schedule__c schedule = [SELECT  Name
                                FROM    Schedule__c
                                //WHERE   Number_of_weeks_in_period__c = 'Monthly'
                                LIMIT 1];

        RCR_Contract_Scheduled_Service__c css = null;       
        for(RCR_Contract_Scheduled_Service__c c : [SELECT   ID,
                                                            Name,
                                                            Start_Date__c,
                                                            End_Date__c
                                                    FROM    RCR_Contract_Scheduled_Service__c
                                                    WHERE   ID = :schedule.Name
                                                    LIMIT 1])
        {
            css = c;
        }
        
        if(css == null)
        {
            return;
        }
        
        ScheduleDateEngine dtEngine = new ScheduleDateEngine(css.ID);
        
        dtEngine.getSchedulePeriodStartDate(dtEngine.Schedules[0],css.Start_Date__c, SchedulePeriod.Current);
        dtEngine.getSchedulePeriodEndDate(css.Start_Date__c, SchedulePeriod.Current);
        
        system.debug(dtEngine.ScheduleCalendarDays);
        
        system.debug(dtEngine.ScheduledServiceExceptions);
        
        system.debug(dtEngine.ScheduleParentLoadByCode);
        
        system.debug(dtEngine.ScheduleQuantityByDay);
        
        system.debug(dtEngine.Schedules);
        
        system.debug(dtEngine.exceptionsByServiceCode);
        
        Test.StopTest();
    }
    
    @IsTest
    static void ScheduleControllerTest1()
    {
        TestLoadData.loadRCRData();
                
        Test.StartTest();
    
        TestLoadData.loadAdhocServices();
    
        for(RCR_Contract_Scheduled_Service__c css : [SELECT     Id,
                                                            Start_Date__c,
                                                            End_Date__c
                                                    FROM    RCR_Contract_Scheduled_Service__c
                                                    LIMIT 5])
        {
            ScheduleController cntlr = new ScheduleController();
    
            cntlr.ParentRecordID = css.ID;
            system.debug(cntlr.Schedule);
    
            cntlr.SelectedRecurrencePattern = null;
            system.debug(cntlr.SelectedRecurrencePattern);
    
            cntlr.saveSchedule(null, css.Start_Date__c, css.End_Date__c);
            PageReference page = cntlr.setSchedulePeriods();
    
            system.debug(cntlr.NumberOfWeeks);
    
            system.debug(cntlr.getScheduleQuantityPerPeriod());
    
            cntlr.SelectedFlexibility = 'Flexible across Scheduled Service Date Range';
            system.debug(cntlr.SelectedFlexibility);
        }
        
        Test.StopTest();
    }
    
     @IsTest
    public static void ScheduleCalendarControllerTest1()
    {
        TestLoadData.loadRCRData();
                
        Test.StartTest();
    
        TestLoadData.loadAdhocServices();
    
        RCR_Contract_Scheduled_Service__c css = [SELECT Id, 
                                                        RCR_Contract__c,
                                                        Start_Date__c 
                                                 FROM   RCR_Contract_Scheduled_Service__c 
                                                 WHERE /*Id IN (SELECT RCR_Contract_Scheduled_Service__c 
                                                                FROM RCR_Scheduled_Service_Exception__c) AND*/
                                                        Total_Cost__c > 0
                                                 limit 1];
        Test.setCurrentPage(Page.RCRSchedServiceCalendar);
        ApexPages.currentPage().getParameters().put('Id', css.RCR_Contract__c);
        TestCal cnt = new TestCal();
        
        system.debug(cnt.Weeks);
        system.debug(cnt.Today); 
        system.debug(cnt.IdNamesList); 
         
        cnt.Year = null;
        system.debug(cnt.Year);
        cnt.Month = null;
        system.debug(cnt.Month);
        system.debug(cnt.MonthName);
        system.debug(cnt.StrYear);
        system.debug(cnt.adHocIDs);
        system.debug(cnt.ServiceOrderMonths);
        system.debug(cnt.schedServicesIDs);
        cnt.ShowHolidayMessage = null;
        system.debug(cnt.ShowHolidayMessage);
        cnt.AccrossDateRangeScheduledServices = null;
        system.debug(cnt.AccrossDateRangeScheduledServices);
        cnt.AdHocServices = null;
        system.debug(cnt.AdHocServices);
        cnt.DataSeriesColours = null; 
        system.debug(cnt.DataSeriesColours);
        cnt.ExceptionsByServiceCode = null;
        system.debug(cnt.ExceptionsByServiceCode);


        cnt.getNextMonth();
        cnt.getPreviousMonth();
        
        cnt.SelectedServiceOrderMonth = css.Start_Date__c.format();
        cnt.jumpToMonth();

        cnt.Month = 1;
        cnt.getPreviousMonth();
        cnt.getNextMonth();
        system.debug(cnt.Weeks[0].Days[0].Day);
        system.debug(cnt.Weeks);
        system.debug(cnt.dataSeriesColours);
        cnt.Weeks[0].Days[0].DayDate=null;
        system.debug(cnt.Weeks[0].Days[0].Day);

        cnt.cancel();
        
        Test.StopTest();
    }
    
    @IsTest
    public static void ScheduleCalendarControllerTest2()
    {
        TestLoadData.loadRCRData();
                
        Test.StartTest();
    
        TestLoadData.loadAdhocServices();
    
        RCR_Contract_Scheduled_Service__c css = null;
        for(RCR_Contract_Scheduled_Service__c c : [SELECT Id, 
                                                        RCR_Contract__c,
                                                        Start_Date__c 
                                                 FROM   RCR_Contract_Scheduled_Service__c 
                                                 WHERE RCR_Contract__r.Start_Date__c > :Date.today() AND
                                                        Total_Cost__c > 0
                                                 limit 1])
         {
            css = c;
         }
         if(css == null)
         {
            return;
         }
        Test.setCurrentPage(Page.RCRSchedServiceCalendar);
        ApexPages.currentPage().getParameters().put('Id', css.RCR_Contract__c);
        TestCal cnt = new TestCal();
        
        system.debug(cnt.Weeks);
        system.debug(cnt.Today);
        system.debug(cnt.idNamesList);
        cnt.Year = null;
        system.debug(cnt.Year);
        cnt.Month = null;
        system.debug(cnt.Month);
        system.debug(cnt.MonthName);
        system.debug(cnt.StrYear);
        system.debug(cnt.adHocIDs);
        system.debug(cnt.ServiceOrderMonths);
        system.debug(cnt.schedServicesIDs);
        cnt.ShowHolidayMessage = null;
        system.debug(cnt.ShowHolidayMessage);
        cnt.AccrossDateRangeScheduledServices = null;
        system.debug(cnt.AccrossDateRangeScheduledServices);
        cnt.AdHocServices = null;
        system.debug(cnt.AdHocServices);
        cnt.DataSeriesColours = null;
        system.debug(cnt.DataSeriesColours);
        cnt.ExceptionsByServiceCode = null;
        system.debug(cnt.ExceptionsByServiceCode);


        cnt.getNextMonth();
        cnt.getPreviousMonth();
        
        cnt.SelectedServiceOrderMonth = css.Start_Date__c.format();
        cnt.jumpToMonth();

        cnt.Month = 1;
        cnt.getPreviousMonth();
        cnt.getNextMonth();
        system.debug(cnt.Weeks[0].Days[0].Day);
        system.debug(cnt.Weeks);
        system.debug(cnt.dataSeriesColours);
        cnt.Weeks[0].Days[0].DayDate=null;
        system.debug(cnt.Weeks[0].Days[0].Day);

        cnt.cancel();
        
        Test.StopTest();
    }
    
    public class TestCal extends ScheduleCalendarController
    {
    }
}