public with sharing class RCRBulkRolloverAcceptanceController extends A2HCPageBase 
{
	public void RCRBulkRolloverAcceptanceController()
	{
		
	}
	
	public List<RCR_Contract__c> ServiceOrders
	{
		get
		{
			ServiceOrders = [SELECT Id,
            						Name,
                                    Start_Date__c,
                                    End_Date__c,
                                    Region__c,
                                    Total_Service_Order_Cost__c,
                                    RCR_Cancellation_Date__c,
                                    Cancellation_Reason__c,
                                    Cancellation_Approver__c,
                                    Cancellation_Approval_Date__c,
                                    Cancellation_Acknowledged_Date__c,
                                    Cancellation_Acknowledged_By__c,
                                    Account__r.Name,
                                    Account__r.BillingStreet,
                                    Account__r.BillingCity,
                                    Account__r.BillingState,
                                    Account__r.BillingPostalCode,
                                    Account__r.Phone,
                                    Account__r.Fax,
                                    CCMS_ID__c,
                                    Client__r.Title__c,
                                    Client__r.Full_Name__c,
                                    Client__r.Address__c,
                                    Client__r.Suburb__c,
                                    Client__r.State__c,
                                    Client__r.PostCode__c,
                                    Client__r.Phone__c,
                                    Group_Program__c 
                             FROM   RCR_Contract__c
						 	 WHERE	RecordType.Name = 'Service Order'
						 	 AND	Source_Record__r.RecordType.Name = 'Rollover'
						 	 AND	CSA_Status__c = 'Pending Acceptance'];
						 	 
			return ServiceOrders;
		}
		private set;
	}
	
	public string DateTimeGenerated
	{
		get
		{
			return Datetime.now().format();
		}
	}
	
	
	public string DepartmentName
	{
		get
		{
			return Disability_SA__c.getValues('DisabilitySA').Department_Name__c;
		}
	}
	
	public string Building
	{
		get
		{
			return Disability_SA__c.getValues('DisabilitySA').Building__c;
		}
	}
	
	public string StreetAddress
	{
		get
		{
			return Disability_SA__c.getValues('DisabilitySA').Street_Address__c;
		}
	}
	
	public string Suburb
	{
		get
		{
			return Disability_SA__c.getValues('DisabilitySA').Suburb__c;
		}
	}
	
	public string Postcode
	{
		get
		{
			return Disability_SA__c.getValues('DisabilitySA').Postcode__c;
		}
	}
	
	public string PostBox
	{
		get
		{
			return Disability_SA__c.getValues('DisabilitySA').Post_Box__c;
		}
	}
	
	public string Phone
	{
		get
		{
			return Disability_SA__c.getValues('DisabilitySA').Phone__c;
		}
	}
	
	public string Fax
	{
		get
		{
			return Disability_SA__c.getValues('DisabilitySA').Fax__c;
		}
	}
	
	public string ABN
	{
		get
		{
			return Disability_SA__c.getValues('DisabilitySA').ABN__c;
		}
	}
	
	public string DCSIDepartmentName
	{
		get
		{
			return Disability_SA__c.getValues('DCSI').Department_Name__c;
		}
	}
	
	public string DCSIPostbox
	{
		get
		{
			return Disability_SA__c.getValues('DCSI').Post_Box__c;
		}
	}
	
	public string DCSISuburb
	{
		get
		{
			return Disability_SA__c.getValues('DCSI').Suburb__c;
		}
	}
	
	public string DCSIPostBoxPostcode
	{
		get
		{
			return Disability_SA__c.getValues('DCSI').Postcode__c; 
		}
	}
	
	public String MinistersContractManagerName
    {
        get
        {
            return RCR_Client_Service_Agreement__c.getValues('CSA').Minister_s_Contract_Manger__c;
        }
    }
    
    public String MinistersContractManagerPosition
    {
        get
        {
            return RCR_Client_Service_Agreement__c.getValues('CSA').Ministers_Contract_Manager_Position__c;
        }
    }   
    
    @IsTest(SeeAllData=true)
    private static void test1()
    {
    	RCRBulkRolloverAcceptanceController c = new RCRBulkRolloverAcceptanceController();
    	
    	system.debug(c.ServiceOrders);
    	system.debug(c.DateTimeGenerated);
    	system.debug(c.DepartmentName);
    	system.debug(c.Building);
    	system.debug(c.StreetAddress);
    	system.debug(c.Suburb);
    	system.debug(c.Postcode);
    	system.debug(c.PostBox);
    	system.debug(c.Phone);
    	system.debug(c.Fax);
    	system.debug(c.ABN);
    	system.debug(c.DCSIDepartmentName);
    	system.debug(c.DCSIPostbox);
    	system.debug(c.DCSISuburb);
    	system.debug(c.DCSIPostBoxPostcode);
    	system.debug(c.MinistersContractManagerName);
    	system.debug(c.MinistersContractManagerPosition);
    } 
}