public class Create30DayTaskBatch implements Database.Batchable<sObject>, Database.Stateful
{
    public Create30DayTaskBatch(SObjectType sObjType)
    {
        this.sObjType = sObjType;
    }

    private SObjectType sObjType;

    private List<User> GroupUsers
    {
        get
        {
            if(GroupUsers == null)
            {
                GroupUsers = [SELECT    ID
                                FROM    User
                                WHERE   User.IsActive = true AND
                                        ID IN (SELECT UserOrGroupId
                                        FROM    GroupMember
                                        WHERE   Group.DeveloperName = 'Senior_Accountant')];
            }
            return GroupUsers;
        }
        set;
    }

    public Database.QueryLocator start(Database.BatchableContext bc)
    {
        if(sObjType == RCR_Invoice__c.SObjectType)
        {
            return Database.getQueryLocator([SELECT ID
                                            FROM    RCR_Invoice__c
                                            WHERE   Email_To_Invoice__c != null AND
                                                    Date__c != null AND
                                                    Date__c <= :Date.today().addDays(-30) AND
                                                    Create_30_Day_Tasks__c = false]);
        }
        else if (sObjType == Adhoc_Payment__c.SObjectType)
        {
            return Database.getQueryLocator([SELECT ID
                                            FROM    Adhoc_Payment__c
                                            WHERE   Email_To_Invoice__c != null AND
                                                    Date_of_Transaction__c != null AND
                                                    Date_of_Transaction__c <= :Date.today().addDays(-30) AND
                                                    Create_30_Day_Tasks__c = false]);
        }
        else
        {
            return null;
        }
    }

    public void execute(Database.BatchableContext bc,  List<sObject> scope)
    {
        Set<ID> relatedIds = new Set<ID>();
        for(SObject sObj : scope)
        {
            relatedIds.add(sObj.ID);
            if(sObjType == RCR_Invoice__c.SObjectType)
            {
                RCR_Invoice__c inv = (RCR_Invoice__c)sObj;
                inv.put(RCR_Invoice__c.Create_30_Day_Tasks__c, true);
            }
            else if (sObjType == Adhoc_Payment__c.SObjectType)
            {
                Adhoc_Payment__c adhocPayment = (Adhoc_Payment__c)sObj;
                adhocPayment.put(Adhoc_Payment__c.Create_30_Day_Tasks__c, true);
            }
        }
        EmailToInvoiceExtension.create30DayTasks(relatedIds, GroupUsers);
        update scope;
    }

    public void finish(Database.BatchableContext bc)
    {
        if(sObjType == RCR_Invoice__c.SObjectType)
        {
            Create30DayTaskBatch adhocBatch = new Create30DayTaskBatch(Adhoc_Payment__c.SObjectType);
            Database.executeBatch(adhocBatch, Create30DayTaskScheduled.getBatchSize('30_Day_Task_Adhoc', 20));
        }
    }
}