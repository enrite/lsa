public with sharing class RCRBulkAllocationApprovalController extends A2HCPageBase 
{
	
//
//	public string ClientOrGroup
//	{
//		get;
//		set;
//	}
//
//	public string CCMSID
//	{
//		get;
//		set;
//	}
//
//
//	public List<RolloverApprovalWrapper> Rollovers
//	{
//		get
//		{
//			if(Rollovers == null)
//			{
//				Rollovers = new List<RolloverApprovalWrapper>();
//			}
//			return Rollovers;
//		}
//		set;
//	}
//
//
//
//	public PageReference doSearch()
//	{
//		try
//		{
//			throwTestError();
//			Rollovers = null;
//
//			Set<Id> rolloverIDs = new Set<Id>();
//			Set<Id> allocationIDs = new Set<Id>();
//
//			for(RCR_Contract__c rollover : [SELECT	Id,
//													Name,
//													Client_Name__c,
//													Start_Date__c,
//													End_Date__c,
//													Total_Service_Order_Cost__c,
//													Client__r.CCMS_ID__c,
//													Account__r.Name,
//													Source_Record__r.Total_Service_Order_Cost__c,
//													(SELECT	Id,
//															Name,
//															Allocation_Type__c,
//															Amount__c,
//															Status__c,
//															Client__r.Name
//													 FROM	IF_Allocations__r
//													 WHERE	Reason__c = :APS_PicklistValues.IFAllocation_Reason_RolloverTopUp)
//											FROM	RCR_Contract__c
//											WHERE	Status__c = :APS_PicklistValues.RCRContract_Status_PendingApproval
//											AND		Approval_Process_Step__c = 'CRAU Top Up Approval'
//											AND		Record_Type_Name__c = 'Rollover'
//											AND		(
//		                                                    (
//		                                                        Client__r.CCMS_ID__c != null AND
//		                                                        Client__r.CCMS_ID__c LIKE :(String.isBlank(CCMSID) ? '%' : CCMSID) AND
//		                                                        Client__r.Name LIKE :('%' + ClientOrGroup + '%')
//		                                                    )
//		                                                    OR
//		                                                    (
//		                                                        Client__r.Account__r.API_Name__c = :APS_PicklistValues.Account_DCSI_RCR_Programs AND
//		                                                        Client__r.CCMS_ID__c LIKE :(String.isBlank(CCMSID) ? '' : '~') AND
//		                                                        Client__r.Name LIKE :('%' + ClientOrGroup + '%')
//		                                                    )
//                                                )])
//			{
//				Rollovers.add(new RolloverApprovalWrapper(rollover));
//				rolloverIDs.add(rollover.Id);
//				for(IF_Personal_Budget__c alloc : rollover.IF_Allocations__r)
//				{
//					allocationIDs.add(alloc.Id);
//				}
//			}
//
//			// Get the Pending Approval Rollover Work Items.
//			Map<Id, ProcessInstanceWorkitem> pendingRolloverApprovalsByTargetID = new Map<Id, ProcessInstanceWorkitem>();
//			for(ProcessInstanceWorkitem workItem : [SELECT 	Id,
//                                                       		ProcessInstance.TargetObjectId
//                                                    FROM   	ProcessInstanceWorkitem
//                                                    WHERE   ProcessInstance.Status = 'Pending'
//                                                    AND		ProcessInstance.TargetObjectId IN :rolloverIDs])
//            {
//                pendingRolloverApprovalsByTargetID.put(workItem.ProcessInstance.TargetObjectId, workItem);
//            }
//
//            // Get the Pending Approval Allocation Work Items.
//            Map<Id, ProcessInstanceWorkitem> pendingAllocationApprovalsByTargetID = new Map<Id, ProcessInstanceWorkitem>();
//			for(ProcessInstanceWorkitem workItem : [SELECT 	Id,
//                                                       		ProcessInstance.TargetObjectId
//                                                    FROM   	ProcessInstanceWorkitem
//                                                    WHERE   ProcessInstance.Status = 'Pending'
//                                                    AND		ProcessInstance.TargetObjectId IN :allocationIDs])
//            {
//                pendingAllocationApprovalsByTargetID.put(workItem.ProcessInstance.TargetObjectId, workItem);
//            }
//
//            // Set the Pending Rollover and Allocation Work Items in the wrapper class.
//            for(RolloverApprovalWrapper wrap : Rollovers)
//            {
//            	wrap.RolloverWorkItem = pendingRolloverApprovalsByTargetID.get(wrap.Rollover.Id);
//            	for(IF_Personal_Budget__c alloc : wrap.Rollover.IF_Allocations__r)
//            	{
//            		if(alloc.Status__c == APS_PicklistValues.IFAllocation_Status_PendingApproval)
//            		{
//            			wrap.AllocationWorkItems.add(pendingAllocationApprovalsByTargetID.get(alloc.Id));
//            		}
//            	}
//            }
//
//			return null;
//		}
//		catch(Exception ex)
//		{
//			return A2HCException.formatException(ex);
//		}
//	}
//
//
//	public PageReference approveOrReject()
//	{
//		Savepoint sp = Database.setSavepoint();
//		try
//		{
//			throwTestError();
//			integer approvedCount = 0;
//			integer rejectedCount = 0;
//			integer errorCount = 0;
//			List<Approval.ProcessWorkitemRequest> requests = new List<Approval.ProcessWorkitemRequest>();
//			List<RCR_Contract__c> rolloversToUpdate = new List<RCR_Contract__c>();
//
//			for(RolloverApprovalWrapper wrap : Rollovers)
//            {
//            	if(wrap.Approve)
//            	{
//            		if(wrap.DirectorApprovalNotRequired)
//            		{
//            			wrap.Rollover.Director_Approval_Not_Required__c = true;
//            			rolloversToUpdate.add(wrap.Rollover);
//            		}
//        			requests.add(getRequest(wrap.RolloverWorkItem.Id, 'Approve'));
//        			approvedCount++;
//            	}
//            	else if(wrap.Reject)
//            	{
//        			requests.add(getRequest(wrap.RolloverWorkItem.Id, 'Reject'));
//        			rejectedCount++;
//
//        			for(ProcessInstanceWorkitem allocWorkItem : wrap.AllocationWorkItems)
//        			{
//	        			requests.add(getRequest(allocWorkItem.Id, 'Reject'));
//        			}
//            	}
//            }
//
//            if(approvedCount == 0 && rejectedCount == 0)
//            {
//            	return A2HCException.formatException('You have not selected to Approve or Reject any records.');
//            }
//
//            if(rolloversToUpdate.size() > 0)
//            {
//            	update rolloversToUpdate;
//            }
//
//            Approval.ProcessResult[] results;
//            if(!Test.isRunningTest())
//            {
//            	results =  Approval.process(requests, true);
//            }
//
//            if(results != null)
//            {
//	            for(Approval.ProcessResult result : results)
//	            {
//	            	if(!result.isSuccess())
//	            	{
//	            		errorCount++;
//	            	}
//	            }
//            }
//
//            if(approvedCount > 0)
//            {
//            	A2HCException.formatMessage(approvedCount + ' Rollover(s) were approved.');
//            }
//            if(rejectedCount > 0)
//            {
//            	A2HCException.formatMessage(rejectedCount + ' Rollover(s) were rejected.');
//            }
//            if(errorCount == 0)
//            {
//            	A2HCException.formatMessage('All approvals or rejections were processed successfully.');
//            }
//
//			Rollovers = null;
//			doSearch();
//			return null;
//		}
//		catch(Exception ex)
//		{
//			return A2HCException.formatExceptionAndRollback(sp, ex);
//		}
//	}
//
//
//	public PageReference cancel()
//	{
//		try
//		{
//			PageReference pg = Page.RCR;
//			pg.setRedirect(true);
//			return pg;
//		}
//		catch(Exception ex)
//		{
//			return A2HCException.formatException(ex);
//		}
//	}
//
//	@testVisible
//	private Approval.ProcessWorkitemRequest getRequest(Id recordId, string action)
//	{
//		Approval.ProcessWorkitemRequest request = new Approval.ProcessWorkitemRequest();
//		if(recordId == null)
//		{
//			return request;
//		}
//		request.setAction(action);
//		request.setWorkitemId(recordId);
//		return request;
//	}
//
//	public class RolloverApprovalWrapper
//	{
//		public RolloverApprovalWrapper(RCR_Contract__c pRollover)
//		{
//			Rollover = pRollover;
//			DirectorApprovalNotRequired = true;
//		}
//
//		public boolean Approve
//		{
//			get;
//			set;
//		}
//
//		public boolean Reject
//		{
//			get;
//			set;
//		}
//
//		public boolean DirectorApprovalNotRequired
//		{
//			get;
//			set;
//		}
//
//		public RCR_Contract__c Rollover
//		{
//			get;
//			set;
//		}
//
//		public ProcessInstanceWorkItem RolloverWorkItem
//        {
//        	get;
//        	set;
//        }
//
//        public List<ProcessInstanceWorkitem> AllocationWorkItems
//        {
//        	get
//        	{
//        		if(AllocationWorkItems == null)
//        		{
//        			AllocationWorkItems = new List<ProcessInstanceWorkitem>();
//        		}
//        		return AllocationWorkItems;
//        	}
//        	private set;
//        }
//	}
}