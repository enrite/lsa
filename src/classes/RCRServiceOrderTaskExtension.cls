public with sharing class RCRServiceOrderTaskExtension 
    extends A2HCPageBase
{
    public RCRServiceOrderTaskExtension(ApexPages.StandardController controller) 
    {
        String serviceOrderId = getParameter('soID');
        String taskType = getParameter('taskType');
        Task tsk = (Task)controller.getRecord();
        tsk.WhatId = serviceOrderId;
        tsk.Status = APS_PicklistValues.Task_Status_NotStarted;
        tsk.RecordTypeID = A2HCUtilities.getRecordType('Task', 'RCR Service Order').ID; 
        
        if(taskType == 'servCoord')
        {
            tsk.Subject = APS_PicklistValues.Task_Subject_RequestReviewBySeniorCoordinator;
            tsk.Description = UserInfo.getName() + ' has requested you to review the Request before submitting it for approval';
        }
        else if(taskType == 'brokerage')
        {
            tsk.Subject = APS_PicklistValues.Task_Subject_PleaseCompleteTheServiceRequest;
            tsk.Description = UserInfo.getName() + ' has requested that the following request be prepared and reviewed ready for submission into the approval process';
        }
        
        ServiceOrder = RCRServiceOrderRequestController.getServiceOrder(serviceOrderID);
        tsk.Service_Order__c = ServiceOrder.Name; 
        tsk.Service_Order_Type__c = ServiceOrder.RecordType.Name;
    }
    
    public RCR_Contract__c ServiceOrder
    {
        get;
        private set;
    }
    
    @IsTest//(SeeAllData=true)
    private static void test1()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        RCR_Contract__c c = [SELECT Id
                             FROM   RCR_Contract__c
                             //WHERE  Name = 'TestSO'
                             LIMIT 1];
                             
        PageReference pg = Page.RCRServiceOrderTask;
        pg.getParameters().put('soID', c.Id);
        pg.getParameters().put('taskType', 'servCoord');
        Test.setCurrentPage(pg);
        
        RCRServiceOrderTaskExtension ext = new RCRServiceOrderTaskExtension(new ApexPages.StandardController(new Task()));
        system.debug(ext.ServiceOrder);
        
        Test.StopTest();
    }
    
    @IsTest//(SeeAllData=true)
    private static void test2()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        RCR_Contract__c c = [SELECT Id
                             FROM   RCR_Contract__c
                             //WHERE  Name = 'TestSO'
                             LIMIT 1];
                             
        PageReference pg = Page.RCRServiceOrderTask;
        pg.getParameters().put('soID', c.Id);
        pg.getParameters().put('taskType', 'brokerage');
        Test.setCurrentPage(pg);
        
        RCRServiceOrderTaskExtension ext = new RCRServiceOrderTaskExtension(new ApexPages.StandardController(new Task()));
        system.debug(ext.ServiceOrder);
        
        Test.StopTest();
    }
}