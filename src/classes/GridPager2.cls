public virtual class GridPager2
	extends A2HCPageBase
{
	public GridPager2()
	{}

	public GridPager2(Integer pPageSize)
	{
		pageSz = pPageSize;
	}

	public GridPager2 GridPagerController
    {
        get
        {
            return this;
        }
    }

	private Integer pageSz = null;
	private Integer pageNum = 1;

	public Integer PageSize
	{
		get
		{
system.debug(pageSz);
			return (pageSz == null) ? 20  : pageSz;
		}
	}

	// indicates whether there are more records after the current page set.
	public Boolean HasNext
	{
		get
		{
			return PageNumber < (TotalRecords / PageSize);
		}
		set;
	}

	// indicates whether there are more records before the current page set.
	public Boolean HasPrevious
	{
		get
		{
			return PageNumber > 1;
		}
		set;
	}


	public List<Integer> PageNumbers
 	{
 		get
 		{
 			if(PageNumbers == null)
 			{
 				PageNumbers = new List<Integer>();
 				Integer start = PageNumber < 5 ? 1 : PageNumber - 4;
		 		for(Integer i = start; i <= TotalPages; i++)
		 		{
		 			PageNumbers.add(i);
		 			if(PageNumbers.size() >= 10)
		 			{
		 				break;
		 			}
		 		}
 			}
 			return PageNumbers;
 		}
 		private set;
 	}

 	public Integer PageNumber
	{
		get
		{
			return pageNum;
		}
		set
		{
			pageNum = (value == null || value == 0) ? 1 : value;
			PageNumbers = null;
		}
	}

	public Integer TotalPages
	{
		get
		{
			return TotalRecords == 0 ? 1 : Math.ceil((TotalRecords * 1.00) / (PageSize * 1.00)).intValue();
		}
	}

	public Integer TotalRecords
	{
		get
		{
			return TotalRecords == null ? 0 : Math.min(MaxRecords, TotalRecords);
		}
		protected set
		{
			TotalRecords = value;
			PageNumbers = null;
		}
	}

	protected List<sObject> Records
	{
		get
		{
			if(Records == null)
			{
				Records = new List<sObject>();
			}
			return Records;
		}
		set;
	}

	public Integer QueryOffset
	{
		get
		{
			return Math.min((PageNumber - 1) * PageSize, MaxRecords);
		}
	}

	private Integer MaxRecords
	{
		get
		{
			// need to avoid a NUMBER_OUTSIDE_VALID_RANGE error
			return 2000;
		}
	}

	public virtual void goPage()
	{
		PageNumbers = null;
	}

	// returns the first page of records
 	public virtual void first()
 	{
 		PageNumber = 1;
 	}

 	// returns the last page of records
 	public virtual void last()
 	{
 		PageNumber = TotalPages;
 	}
 	// returns the PageReference of the original page, if known, or the home page.
 	public virtual PageReference cancel()
 	{
 		return getReturnPageFromURL();
 	}
 	
 	protected String setSearchString(String s, Boolean substituteQuotes)
    {
    	return s == null ? '' : (substituteQuotes ? s.replace('\'', '_') : String.escapeSingleQuotes(s));
    }


}