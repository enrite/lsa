public with sharing class CANSFormExtension extends A2HCPageBase
{
    public CANSFormExtension(ApexPages.StandardController ctrlr)
    {
        controller = ctrlr;
        cansLevel = (CANS_Level__c)controller.getRecord();
        getCANSScore();
    }

    private ApexPages.StandardController controller;
    private CANS_Level__c cansLevel;

    public String HighestLevel
    {
        get;
        private set;
    }

    public PageReference saveOverride()
    {
        try
        {

            getCANSScore();
            Boolean valid = isRequiredFieldValid(cansLevel, CANS_Level__c.Date_of_Assessment__c, true);
            switch on HighestLevel
            {
                when 'A'
                {
                    valid = isRequiredFieldValid(cansLevel, CANS_Level__c.Group_A_CANS_Level__c, valid);
                    cansLevel.CANS_Level__c = cansLevel.Group_A_CANS_Level__c;
                    cansLevel.Group_B_CANS_Level__c = null;
                    cansLevel.Group_C_CANS_Level__c = null;
                    cansLevel.Group_D_CANS_Level__c = null;
                }
                when 'B'
                {
                    valid = isRequiredFieldValid(cansLevel, CANS_Level__c.Group_B_CANS_Level__c, valid);
                    cansLevel.CANS_Level__c = cansLevel.Group_B_CANS_Level__c;
                    cansLevel.Group_A_CANS_Level__c = null;
                    cansLevel.Group_C_CANS_Level__c = null;
                    cansLevel.Group_D_CANS_Level__c = null;
                }
                when 'C'
                {
                    valid = isRequiredFieldValid(cansLevel, CANS_Level__c.Group_C_CANS_Level__c, valid);
                    cansLevel.CANS_Level__c = cansLevel.Group_C_CANS_Level__c;
                    cansLevel.Group_A_CANS_Level__c = null;
                    cansLevel.Group_B_CANS_Level__c = null;
                    cansLevel.Group_D_CANS_Level__c = null;
                }
                when 'D'
                {
                    valid = isRequiredFieldValid(cansLevel, CANS_Level__c.Group_D_CANS_Level__c, valid);
                    cansLevel.CANS_Level__c = cansLevel.Group_D_CANS_Level__c;
                    cansLevel.Group_A_CANS_Level__c = null;
                    cansLevel.Group_B_CANS_Level__c = null;
                    cansLevel.Group_C_CANS_Level__c = null;
                }
                when else
                {
                    if(!cansLevel.Does_not_require_support__c && !cansLevel.CANS_15__c)
                    {
                        A2HCException.formatException(CANS_Level__c.Does_not_require_support__c.getDescribe().getLabel() + ' must be true when score is zero');
                        valid = false;
                    }
                    cansLevel.Group_A_CANS_Level__c = null;
                    cansLevel.Group_B_CANS_Level__c = null;
                    cansLevel.Group_C_CANS_Level__c = null;
                    cansLevel.Group_D_CANS_Level__c = null;
                    cansLevel.CANS_Level__c = '0';
                }
            }
            return valid ? controller.save() : null;
        }
        catch(Exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }

    public void getCANSScore()
    {
        getGroupAScore();
        getGroupBScore();
        getGroupCScore();
        getGroupDScore();
        cansLevel.CANS_Total__c = cansLevel.Group_A_subtotal__c + cansLevel.Group_B_subtotal__c + cansLevel.Group_C_subtotal__c + cansLevel.Group_D_subtotal__c;
        getHighestLevel();
    }

    private void getGroupAScore()
    {
        cansLevel.Group_A_subtotal__c = getGroupScore(new Set<SObjectField>{
                CANS_Level__c.Tracheostomy_management__c,
                CANS_Level__c.Nasogastric_PEG_feeding__c,
                CANS_Level__c.Bed_mobility__c,
                CANS_Level__c.Wanders_gets_lost__c,
                CANS_Level__c.Potential_to_harm_others__c,
                CANS_Level__c.Difficulty_communicating_basic_needs__c,
                CANS_Level__c.Continence__c,
                CANS_Level__c.Eating_and_drinking__c,
                CANS_Level__c.Transfers_mobility__c,
                CANS_Level__c.Nasogastric_PEG_feeding__c,
                CANS_Level__c.Other_GroupA__c});

    }

    private void getGroupBScore()
    {
        cansLevel.Group_B_subtotal__c = getGroupScore(new Set<SObjectField>{
                CANS_Level__c.Personal_hygiene_toileting__c,
                CANS_Level__c.Bathing_dressing__c,
                CANS_Level__c.Preparation_of_light_meal_snack__c,
                CANS_Level__c.Other_GroupB__c});
    }

    private void getGroupCScore()
    {
        cansLevel.Group_C_subtotal__c = getGroupScore(new Set<SObjectField>{
                CANS_Level__c.Shopping__c,
                CANS_Level__c.Domestic_incl_preparation_of_main_meal__c,
                CANS_Level__c.Medication_use__c,
                CANS_Level__c.Money_management__c,
                CANS_Level__c.Everyday_devices__c,
                CANS_Level__c.Money_management__c,
                CANS_Level__c.Transport_and_outdoor_surfaces__c,
                CANS_Level__c.Parenting_skills__c,
                CANS_Level__c.Interpersonal_relationships__c,
                CANS_Level__c.Leisure_and_recreation__c,
                CANS_Level__c.Employment_study__c,
                CANS_Level__c.Other_GroupC__c});
    }

    private void getGroupDScore()
    {
        cansLevel.Group_D_subtotal__c = getGroupScore(new Set<SObjectField>{
                CANS_Level__c.Informational_supports__c,
                CANS_Level__c.Emotional_supports__c,
                CANS_Level__c.Other_GroupD__c});
    }

    private Integer getGroupScore(Set<SObjectField> groupFields)
    {
        Integer score = 0;
        for(SObjectField fld : groupFields)
        {
            score += getFieldScore(fld);
        }
        return score;
    }

    private Integer getFieldScore(SObjectField fld)
    {
        if(fld.getDescribe().soapType == SoapType.BOOLEAN && cansLevel.get(fld) != null)
        {
            return (Boolean)cansLevel.get(fld) ? 1 : 0;
        }
        return 0;
    }

    private void getHighestLevel()
    {
        String val = '';
        if(cansLevel.Group_A_subtotal__c > 0)
        {
            val = 'A';
        }
        else if(cansLevel.Group_B_subtotal__c > 0)
        {
            val = 'B';
        }
        else if(cansLevel.Group_C_subtotal__c > 0)
        {
            val = 'C';
        }
        else if(cansLevel.Group_D_subtotal__c > 0)
        {
            val = 'D';
        }
        HighestLevel = val;
        system.debug(HighestLevel);
    }
}