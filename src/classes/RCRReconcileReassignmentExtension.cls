public with sharing class RCRReconcileReassignmentExtension 
    extends A2HCPageBase
{
	public RCRReconcileReassignmentExtension(ApexPages.StandardController controller)
	{
		String invoiceItemID = getParameter('id');  
		if(invoiceItemID != null)
		{
			getInvoiceItem(InvoiceItemID);
		}
		if(IsReversal)
		{
			A2HCException.formatException(ApexPages.Severity.ERROR, 'You cannot reassign a reversal.');
		}
	}
	
	@TestVisible
	private RCR_Invoice_Item__c InvoiceItem
	{
		get;
		private set;
	}
	
	public boolean IsReversal
	{
		get
		{
			return InvoiceItem.Total__c < 0 ? true : false;
		}
	}

	public PageReference reassign()
	{
		try
		{
			string returnMsg;
			List<String> newCodes;
			String newCodeMsg;
			RCR_Contract_Scheduled_Service__c selectedSchedService = null;
			RCR_Contract_Adhoc_Service__c selectedAdhocService = null;			
	        SavePoint sp = Database.setSavepoint();
	        
	        for(ServiceWrapper servWrap : AlternativeScheduledServices)
	        {
				if(servWrap.Selected)
				{
					selectedSchedService = servWrap.ScheduledService;					
					newCodeMsg = servWrap.ScheduledService.Service_Code__c;
					break;
				}
	        }
	        
	        if(selectedSchedService == null)
	        {
	        	for(ServiceWrapper servWrap : AlternativeAdHocServices)
	        	{
	        		if(servWrap.Selected)
					{
		        		selectedAdhocService = servWrap.AdHocService;
						newCodeMsg = 'Ad Hoc Service ' + servWrap.AdHocService.Name;
						break;
					}
	        	}
	        }
	        
	        // If nothing has been selected return here. Dont do anything.
	        if(selectedSchedService == null && selectedAdhocService == null)
	        {
	        	return A2HCException.formatException('You must select a service to reassign to.');
	        }
			
			if(InvoiceItem.Status__c != APS_PicklistValues.RCRInvoiceItem_Status_Rejected)
			{
				InvoiceItem.Exceeds_Tolerance__c = false;
				RCRReconcileBatch batch = new RCRReconcileBatch();
				returnMsg = batch.reconcileSingleItem(InvoiceItem, selectedSchedService, selectedAdhocService);
			}
			
			if(returnMsg == null)
			{
				InvoiceItem.RCR_Contract_Scheduled_Service__c = selectedSchedService == null ? null : selectedSchedService.ID;
				InvoiceItem.RCR_Contract_Adhoc_Service__c = selectedAdhocService == null ? null : selectedAdhocService.ID;
				InvoiceItem.Reconcile_Log__c += '\nReconcilation reassigned by: ' + CurrentUser.Name + ' using Service Code ' + InvoiceItem.Code__c;
				update InvoiceItem;
				
				// need to explicitly call this as the trigger
				// only fires this on change of Record Type
				RCRReconcileInvoice.setServiceTotals(new Set<ID> {InvoiceItem.RCR_Service_Order__c});
				
				getInvoiceItem(InvoiceItem.ID);
				AlternativeAdHocServices = null;
				AlternativeScheduledServices = null;

				A2HCException.formatException(ApexPages.Severity.INFO, 'The reassignment to ' + newCodeMsg + ' was successful.');
			}
			else
			{
				A2HCException.formatException(returnMsg);
				Database.rollback(sp);
			}			
			return null;
		}
		catch (Exception ex)
		{
			return A2HCException.formatException(ex);
		}
	}

	public PageReference cancelOverride()
	{
		try
        {
            return new PageReference('/' + InvoiceItem.ID);
        }
        catch (Exception ex)
        {
            return A2HCException.formatException(ex);
        }
	}

	public List<ServiceWrapper> AlternativeScheduledServices
	{
		get
		{
			if(AlternativeScheduledServices == null)
			{
				AlternativeScheduledServices = new List<ServiceWrapper>();
				for(RCR_Contract_Scheduled_Service__c css : [SELECT ID,
				                                                    Name,
				                                                    Service_Code__c,
				                                                    Start_Date__c,
				                                                    End_Date__c,
				                                                    Flexibility__c,
				                                                    Public_Holidays_Not_Allowed__c,
				                                                    Total_Cost__c,
				                                                    Service_Description__c,
				                                                    Service_Types__c,					                                                    
				                                                    RCR_Service__c,
				                                                    RCR_Service__r.Name,
				                                                    RCR_Contract__r.Name,
				                                                    RCR_Contract__r.Level__c,
				                                                    RCR_Contract__r.Start_Date__c,
				                                                    RCR_Contract__r.End_Date__c,
				                                                    Exception_Count__c,
				                                                    Use_Negotiated_Rates__c,
				                                                    Negotiated_Rate_Standard__c,
				                                                    Negotiated_Rate_Public_Holidays__c
															 FROM   RCR_Contract_Scheduled_Service__c
															 WHERE  RCR_Contract__c = :InvoiceItem.RCR_Service_Order__c
															 AND	Id != :InvoiceItem.RCR_Contract_Scheduled_Service__c
															 AND	End_Date__c >= :InvoiceItem.Activity_Date__c
														 	 AND 	Start_Date__c <= :InvoiceItem.Activity_Date__c
															 ORDER BY Service_Code__c asc])
				{
					AlternativeScheduledServices.add(new ServiceWrapper(css, null));
				}
			}
			return AlternativeScheduledServices;
		}
		private set;
	}


	public List<ServiceWrapper> AlternativeAdHocServices
	{
		get
		{
			if(AlternativeAdHocServices == null)
			{
				AlternativeAdHocServices = new List<ServiceWrapper>();
				if(InvoiceItem.RCR_Contract_Adhoc_Service__c == null)
				{
					for(RCR_Contract_Adhoc_Service__c cas : [SELECT Id,
																	Name,
																	Service_Types__c,
																	Start_Date__c,
																	End_Date__c,
																	Amount__c
															 FROM 	RCR_Contract_Adhoc_Service__c
															 WHERE	RCR_Contract__c = :InvoiceItem.RCR_Service_Order__c
														  	 AND	End_Date__c >= :InvoiceItem.Activity_Date__c
															 AND 	Start_Date__c <= :InvoiceItem.Activity_Date__c])
					{
						AlternativeAdHocServices.add(new ServiceWrapper(null, cas));
						break;
					}
				}
			}
			return AlternativeAdHocServices;
		}
		private set;
	}

	private void getInvoiceItem(String itemID)
	{
		InvoiceItem = [SELECT  ID,
			                        Name,
			                        Activity_Date__c,
			                        Code__c,
			                        Comment__c,
			                        Error__c,
			                        Quantity__c,
			                        Rate__c,
			                        GST__c,
			                        Total__c,
			                        Total_GST__c,
			                        Total_ex_GST__c,
			                        Exceeds_Tolerance__c,
			                        Override_Tolerance__c,
			                        Status__c,
			                        RCR_Contract_Scheduled_Service__c,
			                        RCR_Contract_Scheduled_Service__r.Name,
			                        RCR_Contract_Adhoc_Service__c,
			                        RCR_Invoice__r.Status__c,
			                        RCR_Invoice__r.Account__c,
			                        RCR_Invoice__r.Name,
			                        RCR_Service_Order__c,
			                        Service_Order_Number__c,
			                        Unique_Identifier__c,
			                        Period_Start_Date__c,
			                        Period_End_Date__c,
			                        Summary_AdHoc_Item__c,
			                        Write_To_Log__c,
			                        Reconcile_Log__c,
			                        RCR_Invoice__r.Override_validation__c
			                FROM    RCR_Invoice_Item__c
    						WHERE	Id = :itemID];
	}


	public class ServiceWrapper
	{
		public ServiceWrapper(RCR_Contract_Scheduled_Service__c pScheduledService, 
								RCR_Contract_Adhoc_Service__c pAdHocService)
		{
			ScheduledService = pScheduledService;
			AdHocService = pAdHocService;
			Selected = false;
		}

		public RCR_Contract_Scheduled_Service__c ScheduledService
		{
			get;
			private set;
		}

		public RCR_Contract_Adhoc_Service__c AdHocService
		{
			get;
			private set;
		}

		public boolean Selected
		{
			get;
			set;
		}
	}

	
}