/**
 * Created by USer on 05/09/2016.
 */

public with sharing class GrantExtension extends A2HCPageBase
{
    public GrantExtension()
    {
        grantApplication = new Grant_Application__c(Status__c = 'In Progress');
        String grantID = getParameter('gid');
        if(String.isNotBlank(grantID))
        {
            GrantRound = getGrant(new List<ID>{grantID});
            if(grantApplication.Grant_Round__c == null)
            {
                grantApplication.Grant_Round__c = GrantRound.ID;
            }
        }
    }

    public GrantExtension(ApexPages.StandardController ctrlr)
    {
        grantApplication = (Grant_Application__c)ctrlr.getRecord();
        String grantID = getParameter('gid');
        if(String.isNotBlank(grantID) || grantApplication.ID != null)
        {
            GrantRound = getGrant(new List<ID>{grantID, grantApplication.Grant_Round__c});
            if(grantApplication.Grant_Round__c == null)
            {
                grantApplication.Grant_Round__c = GrantRound.ID;
            }
        }
        if(grantApplication.ID == null)
        {
            grantApplication.Status__c = 'In Progress';
        }
    }

    private transient List<Grant_Round__c> tOpenGrants;
    private transient List<Grant_Application__c> tMyApplications;
    private Grant_Application__c grantApplication;
    private transient List<Attachment> tAttachments;

    public Grant_Round__c GrantRound
    {
        get
        {
            if(GrantRound == null)
            {
                GrantRound = new Grant_Round__c();
            }
            return GrantRound;
        }
        private set;
    }

    public ID ApplicationID
    {
        get;
        private set;
    }


    public String ReCatchaResponse
    {
        get;
        set;
    }

    public List<SelectOption> GrantTypes
    {
        get
        {
            if(GrantTypes == null)
            {
                GrantTypes = new List<SelectOption>();
                if(GrantRound.Grant_Type__c != null)
                {
                    GrantTypes.add(new SelectOption('', ''));
                    for(String s : GrantRound.Grant_Type__c.split(';'))
                    {
                        GrantTypes.add(new SelectOption(s, s));
                    }
                }
            }
            return GrantTypes;
        }
        private set;
    }

    public List<SelectOption> GrantCategories
    {
        get
        {
            if(GrantCategories == null)
            {
                GrantCategories = new List<SelectOption>();
                if(GrantRound.Grant_Type__c != null)
                {
                    GrantCategories.add(new SelectOption('', ''));
                    for(String s : GrantRound.Grant_Category__c.split(';'))
                    {
                        GrantCategories.add(new SelectOption(s, s));
                    }
                }
            }
            return GrantCategories;
        }
        private set;
    }

    public List<Grant_Round__c> OpenGrants
    {
        get
        {
            if(tOpenGrants == null)
            {
                tOpenGrants = [SELECT ID,
                                    Name,
                                    Close_Date__c,
                                    Open_Date__c
                            FROM    Grant_Round__c
                            WHERE   Open_Date__c <= :Date.today() AND
                                    Close_Date__c >= :Date.today()];
            }
            return tOpenGrants;
        }
    }

    public List<Grant_Application__c> MyApplications
    {
        get
        {
            if(tMyApplications == null)
            {
                tMyApplications = [SELECT ID,
                                        Name,
                                        Status__c,
                                        Proposed_Start_Date__c,
                                        Proposed_End_Date__c,
                                        Amount_Applied_for__c,
                                        Name_of_Project__c,
                                        Brief_Project_Summary__c,
                                        Amount_Approved__c
                                FROM    Grant_Application__c
                                WHERE   Contact__c = :CurrentUser.ContactId AND
                                        Status__c = 'Approved'];
            }
            return tMyApplications;
        }
    }

    public List<Attachment> Attachments
    {
        get
        {
            if(tAttachments == null)
            {
                tAttachments = [SELECT ID,
                                        Name
                                FROM    Attachment
                                WHERE   ParentID = :grantApplication.ID AND
                                        CreatedByID IN (SELECT ID
                                                        FROM User
                                                        WHERE UserType != 'Standard')];
            }
            return tAttachments;
        }
    }

    @future
    public static void sendApprovalEmails(Set<ID> applicationIds)
    {
        EmailTemplate approvalTemplate = [SELECT  ID
                                        FROM    EmailTemplate
                                        WHERE   DeveloperName = 'Application_Submitted_Approval'];

        Document assessmentTemplate = [SELECT   ID
                                        FROM    Document
                                        WHERE   DeveloperName = 'Grant_Application_Assessment_Template'];

        OrgWideEmailAddress owe = [SELECT ID
                                    FROM    OrgWideEmailAddress
                                    WHERE   DisplayName = 'Lifetime Support Authority'];

        List<String> assessorEmails = new List<String>();
        for(User u : [SELECT ID,
                                Email
                        FROM    User
                        WHERE   IsActive = true AND
                                ID IN (SELECT   UserOrGroupId
                                        FROM    GroupMember
                                        WHERE   Group.DeveloperName = 'Grant_Assessors')])
        {
            assessorEmails.add(u.Email);
        }

        Map<ID, Attachment> attachmentsById = new Map<ID, Attachment>(
                                                            [SELECT ID,
                                                                    Name,
                                                                    ParentID,
                                                                    Body
                                                            FROM    Attachment
                                                            WHERE   ParentID IN :applicationIds]);

        List<Messaging.SingleEmailMessage> msgs = new List<Messaging.SingleEmailMessage>();
        for(Grant_Application__c appl : [SELECT ID,
                                        Approval_Tier__c,
                                        Grant_Round__r.External_Assessor_Email__c,
                                        (SELECT ID
                                        FROM    Attachments)
                                FROM    Grant_Application__c
                                WHERE   ID IN :applicationIds])
        {
            Messaging.SingleEmailMessage templateMsg = Messaging.renderStoredEmailTemplate(approvalTemplate.ID, UserInfo.getUserId(), appl.ID);

            if(!assessorEmails.isEmpty()) {
                Messaging.SingleEmailMessage assessorMsg = new Messaging.SingleEmailMessage();
                assessorMsg.setHtmlBody(templateMsg.getHTMLBody());
                assessorMsg.setToAddresses(assessorEmails);
                assessorMsg.setOrgWideEmailAddressId(owe.ID);
                msgs.add(assessorMsg);
            }

            if(appl.Grant_Round__r.External_Assessor_Email__c != null &&
                    (appl.Approval_Tier__c == 'Tier 2' || appl.Approval_Tier__c == 'Tier 3'))
            {
                List<String> emailAddresses = appl.Grant_Round__r.External_Assessor_Email__c.split(';');

                Messaging.SingleEmailMessage extAssessorMsg = new Messaging.SingleEmailMessage();
                extAssessorMsg.setHtmlBody(templateMsg.getHTMLBody());
                extAssessorMsg.setToAddresses(emailAddresses);
                extAssessorMsg.setEntityAttachments(new List<String>{assessmentTemplate.ID});
                extAssessorMsg.setOrgWideEmailAddressId(owe.ID);

                List<Messaging.EmailFileAttachment > emAtts = new List<Messaging.EmailFileAttachment >();
                for(Attachment a : appl.Attachments)
                {
                    Messaging.EmailFileAttachment emAtt = new Messaging.EmailFileAttachment();
                    Attachment file = attachmentsById.get(a.ID);
                    emAtt.setBody(file.Body);
                    emAtt.setFileName(file.Name);
                    emAtts.add(emAtt);
                }
                extAssessorMsg.setFileAttachments(emAtts);

                msgs.add(extAssessorMsg);
            }
        }
        Messaging.sendEmail(msgs);
    }

    public PageReference redir()
    {
        try
        {
            if(grantApplication.Status__c != null && grantApplication.Status__c != 'In Progress')
            {
                PageReference pg = Page.GPThankYou;
                pg.getParameters().put('id', grantApplication.ID);
                return pg.setRedirect(true);
            }
            return null;
        }
        catch(Exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }

    public void saveImmediate()
    {
        try
        {
            ApexPages.getMessages().clear();
            upsert grantApplication;
            ApplicationID = grantApplication.ID;
        }
        catch(Exception ex)
        {
            A2HCException.formatException(ex);
        }
    }

    public void deleteAttachment()
    {
        try
        {
            String attachmentID = getParameter('attId');
            delete [SELECT ID
                    FROM    Attachment
                    WHERE ID = :attachmentID];
            tAttachments = null;
        }
        catch(Exception ex)
        {
            A2HCException.formatException(ex);
        }
    }

    public PageReference saveApplication()
    {
        try
        {
            Boolean valid = isRequiredFieldValid(grantApplication, Grant_Application__c.Name_of_Project__c, true);
            valid = isRequiredFieldValid(grantApplication, Grant_Application__c.Grant_Type_picklist__c, valid);
            valid = isRequiredFieldValid(grantApplication, Grant_Application__c.Grant_Category_picklist__c, valid);
            valid = isRequiredFieldValid(grantApplication, Grant_Application__c.Amount_Applied_For__c, valid);
            valid = isRequiredFieldValid(grantApplication, Grant_Application__c.Proposed_Start_Date__c, valid);
            valid = isRequiredFieldValid(grantApplication, Grant_Application__c.Proposed_End_Date__c, valid);
            valid = isRequiredFieldValid(grantApplication, Grant_Application__c.Brief_Project_Summary__c, valid);
            valid = isRequiredFieldValid(grantApplication, Grant_Application__c.Project_Objectives__c, valid);
            valid = isRequiredFieldValid(grantApplication, Grant_Application__c.Purpose_of_Project__c, valid);
            valid = isRequiredFieldValid(grantApplication, Grant_Application__c.Name_of_Applicant_Organisation__c, valid);
            valid = isRequiredFieldValid(grantApplication, Grant_Application__c.Type_of_Applicant_Organisation__c, valid);
            valid = isRequiredFieldValid(grantApplication, Grant_Application__c.Street_Address__c, valid);
            valid = isRequiredFieldValid(grantApplication, Grant_Application__c.Postal_Address__c, valid);
            valid = isRequiredFieldValid(grantApplication, Grant_Application__c.Contact_First_Name__c, valid);
            valid = isRequiredFieldValid(grantApplication, Grant_Application__c.Contact_Surname__c, valid);
            valid = isRequiredFieldValid(grantApplication, Grant_Application__c.Contact_Position__c, valid);
            valid = isRequiredFieldValid(grantApplication, Grant_Application__c.Phone_Work__c, valid);
            valid = isRequiredFieldValid(grantApplication, Grant_Application__c.Email__c, valid);

            if(grantApplication.Type_of_Applicant_Organisation__c == 'Other (please specify)' && String.isBlank(grantApplication.Other_Organisation_Type__c))
            {
                A2HCException.formatException('Other Organisation Type is required when Type of Applicant Organisation is Other');
                valid = false;
            }
            if(!valid)
            {
                return null;
            }

            GoogleReCaptcha reCaptcha = new GoogleReCaptcha();
            if(!reCaptcha.verify(ReCatchaResponse))
            {
                throw new A2HCException('Please verify again;');
            }
            grantApplication.Status__c = 'Review';
            if(CurrentUser.ContactID != null)
            {
                grantApplication.Contact__c = CurrentUser.ContactID;
            }
            upsert grantApplication;

            PageReference pg = Page.GPThankYou;
            pg.getParameters().put('id', grantApplication.ID);
            return pg.setRedirect(true);
        }
        catch(Exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }

    private Grant_Round__c getGrant(List<ID> ids)
    {
        return [SELECT    ID,
                            Name,
                            Close_Date__c,
                            Open_Date__c,
                            Grant_Type__c,
                            Grant_Category__c
                    FROM    Grant_Round__c
                    WHERE   ID IN :ids];
    }
}