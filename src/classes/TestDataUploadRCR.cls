/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestDataUploadRCR {

    static testMethod void myUnitTest()
    {
/*    	
    	Account acc = [SELECT ID,
    							Name
						FROM	Account
						WHERE	Name = 'UnitingCare Wesley Adelaide'];

    	User u = [SELECT ID,
						AccountID
				FROM	User
				WHERE	AccountID = :acc.ID  AND
						UserType = 'CSPLitePortal'
				LIMIT 1];

		RCR_Program__c prog = new RCR_Program__c(Account__c = acc.ID, Name = 'Home Link SA');
		insert prog;

		RCR_Program_Service_Type__c psType = new RCR_Program_Service_Type__c(RCR_Program__c = prog.Id);
		psType.Name = 'Respite';
		insert psType;

		RCR_Program_Service_Type__c psType2 = new RCR_Program_Service_Type__c(RCR_Program__c = prog.Id);
		psType2.Name = 'test';
		insert psType2;

		RCR_Program_Service__c ps = new RCR_Program_Service__c(RCR_Program_Service_Type__c = psType.ID);
		ps.Name = 'Respite';
		ps.Day_of_Week__c = 'Monday; Tuesday; Wednesday; Thursday; Friday; Saturday; Sunday';
		insert ps;

		RCR_Program_Service__c ps2 = new RCR_Program_Service__c(RCR_Program_Service_Type__c = psType2.ID);
		ps2.Name = 'HL FT LEVEL 2';
		ps2.Day_of_Week__c = 'Monday; Tuesday; Wednesday; Thursday; Friday; Saturday; Sunday';
		insert ps2;

		RCR_Program_Service_Rate__c rate = new RCR_Program_Service_Rate__c(Program_Service__c = ps.ID);
		rate.Start_Date__c = date.today().addYears(-1);
		rate.End_Date__c = date.today().addYears(1);
		rate.Rate__c = 1.00;
		insert rate;

		RCR_Program_Service_Rate__c rate2 = new RCR_Program_Service_Rate__c(Program_Service__c = ps2.ID);
		rate2.Start_Date__c = date.today().addYears(-1);
		rate2.End_Date__c = date.today().addYears(1);
		rate2.Rate__c = 1.00;
		insert rate2;

		RCR_Contract__c contract = new RCR_Contract__c(RCR_Program__c = prog.ID);
		contract.Level__c = '2';
		contract.Name = '72515';
		insert contract;

		RCR_Invoice_Batch__c b = new RCR_Invoice_Batch__c();
		insert b;

		RCR_Invoice__c i = new RCR_Invoice__c(RCR_Contract__c = contract.ID, RCR_Invoice_Batch__c = b.ID);
		i.Invoice_Number__c = '1zz';
		i.Start_Date__c = Date.today().addDays(-30);
		i.End_Date__c = Date.today();
		i.Date__c = Date.today();
		insert i;

		RCR_Invoice_Item__c it = new RCR_Invoice_Item__c(RCR_Invoice__c = i.ID);
		it.Activity_Date__c = date.today();
		it.Code__c = ps.Name;
		it.GST__c = rate.Rate__c * 0.1;
		it.Quantity__c = 1.0;

		RCR_Invoice_Item__c it2 = new RCR_Invoice_Item__c(RCR_Invoice__c = i.ID);
		it2.Activity_Date__c = date.today();
		it2.Code__c = ps2.Name;
		it2.GST__c = rate.Rate__c * 0.1;
		it2.Quantity__c = 1.0;

		RCR_Contract_Scheduled_Service__c css = new RCR_Contract_Scheduled_Service__c(RCR_Contract__c = contract.ID, RCR_Program_Service__c = ps.ID);
		css.End_Date__c	= date.today().addYears(1);
		css.Start_Date__c= date.today().addYears(-1);
		css.Quantity_Per_Day__c = 1.0;
		insert css;

		RCR_Contract_Adhoc_Service__c ass = new RCR_Contract_Adhoc_Service__c(RCR_Contract__c = contract.ID, RCR_Program_Service_Type__c = psType2.ID);
		ass.End_Date__c	= date.today().addYears(1);
		ass.Start_Date__c= date.today().addYears(-1);
		ass.Amount__c = 100.0;
		insert ass;

    	RCR_Data_Load__c r = new RCR_Data_Load__c();
    	r.CBMS_Contract_ID__c = '72494';
    	r.CCMS_ID__c = '11934';
    	r.Client_Name__c = 'Weeks, Michelle';
    	r.End_Date__c = Date.parse('30/06/2012');
    	r.Start_Date__c = date.parse('01/07/2011');
    	r.Level__c = '3';
    	r.Rate__c = null;
    	r.RFS_ID__c = '16871';
    	r.Scheduled__c = 'Non-scheduled';
    	r.Service__c = 'Respite';
    	r.Total__c = 5449.0;
    	insert r;

    	r = new RCR_Data_Load__c();
    	r.CBMS_Contract_ID__c = '72495';
    	r.CCMS_ID__c = '18018';
    	r.Client_Name__c = 'Robertson, Amanda';
    	r.End_Date__c = Date.parse('30/06/2012');
    	r.Start_Date__c = date.parse('01/07/2011');
    	r.Level__c = '2';
    	r.Rate__c = 82.95;
    	r.RFS_ID__c = '16872';
    	r.Scheduled__c = 'Scheduled';
    	r.Service__c = 'HL FT LEVEL 2';
    	r.Total__c = 30359.7;
    	insert r;

        DataUploadRCR.loadData();
        DataUploadRCR.refreshData();
        //DataUploadRCR.updateContractIDs();
        DataUploadRCR.up();
        
*/        
    }
}