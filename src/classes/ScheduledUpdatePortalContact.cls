global class ScheduledUpdatePortalContact implements Schedulable {
	/**
	 * @description Executes the scheduled Apex job. 
	 * @param sc contains the job ID
	 */ 
	global void execute(SchedulableContext sc) 
    {
        try 
        {
            List<Contact> contactsToUpdate = new List<Contact>();
		    for(User u : [SELECT ID,
                                Email,
                                ContactId
                          FROM  User 
                          WHERE IsActive = true AND 
                                UserType = 'CSPLitePortal' AND 
                                Portal_Contact_Email_Match__c = false])
            {
                contactsToUpdate.add(new Contact(ID = u.ContactId, Email = u.Email));
            }
            update contactsToUpdate;
        }
        catch(Exception ex)
        {
            EmailManager.sendPlainTextMessage(new String[]{UserInfo.getUserEmail()}, 
                                    'ScheduledUpdatePortalContact job failed', 
                                    ex.getMessage() + '\n' + ex.getStackTraceString(), 
                                    'LSA', 
                                    'noreply@salesforce.com');
        }
	}
}