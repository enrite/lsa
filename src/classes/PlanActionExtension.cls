public class PlanActionExtension extends A2HCPageBase
{
    public PlanActionExtension(ApexPages.StandardController controller)
    {
        this.controller = controller;
    }

    private ApexPages.StandardController controller;

    public Boolean ApprovalInProgress
    {
        get
        {
            Plan_Action__c planAction = (Plan_Action__c)controller.getRecord();
            return planAction.Plan_Approval__c != null && planAction.Plan_Approval__r.Plan_Approval_Date__c == null;
            //return ((Plan_Action__c) controller.getRecord()).Plan_Approval__c != null;
        }
    }

    public PageReference redir()
    {
        PageReference pg = null;
        String retURL = getParameter('retURL');
        if(String.isBlank(getParameter('id')))
        {
            // user has cancelled an edit, just send them on their way
            pg = new PageReference(retURL);
        }
        else
        {
            Plan_Action__c planAction = (Plan_Action__c) controller.getRecord();
            Boolean isApproved = planAction.Plan_Approval__r.Plan_Approval_Date__c != null;
            Boolean canEdit = planAction.Plan_Approval__c == null;
            if(!canEdit)
            {
                for(Plan_Approval__c pa : [
                        SELECT ID
                        FROM Plan_Approval__c
                        WHERE ID = :planAction.Plan_Approval__c
                        AND RecordType.DeveloperName = 'Service_Planner_Recommendation'
                        AND Service_Planner_Complete__c = false
                ]) {
                    canEdit = true;
                }
            }
            if(canEdit || isApproved)
            {
                pg = new PageReference('/' + planAction.ID + '/e');
                pg.getParameters().put('nooverride', '1');
                pg.getParameters().put('retURL', retURL);
            }
        }
        return pg;
    }
}