public class LPMemberJSON
{
    public String membershipNumber;
    public String programCode;
    public String dateOfBirth;
    public Name name;
    public Webhooks webHooks;
    public Plan plan;

    public class Name
    {
        public String first;
        public String last;
    }

    public class Plan
    {
        public String startDate;
        public String endDate;
        public Coverage[] coverages;
    }

    public class Coverage
    {
        public String categoryKey;
    }

    public class Webhooks
    {
        public String onMemberActivated;
        public String onAddMemberFailed;
    }

    public String getJSON(Referred_Client__c client, Lantern_Pay__c settings)
    {
        membershipNumber = client.Client_ID__c;
        programCode = settings.Program_Code__c;
        dateOfBirth = LPRESTInterface.getDateString(client.Date_Of_Birth__c);

        name = new LPMemberJSON.Name();
        name.first = client.Given_Name__c;
        name.last = client.Family_Name__c;

        webHooks = new LPMemberJSON.Webhooks();
        webHooks.onMemberActivated = settings.Member_Activated_Webhook__c;
        webHooks.onAddMemberFailed = settings.Member_Failed_Webhook__c;

        plan = new LPMemberJSON.Plan();
        plan.startDate = LPRESTInterface.getDateString(client.Status_Date__c);
        plan.endDate = null;

        LPMemberJSON.Coverage coverage = new LPMemberJSON.Coverage();
        coverage.categoryKey = settings.Category_Key__c;
        plan.coverages = new List<LPMemberJSON.Coverage>();
        plan.coverages.add(coverage);

        String jsonString = JSON.serialize(this);
        return jsonString.replace('webHooks', '_webHooks');
    }
}