@isTest
private class TestRCRInvoiceApprovalBatch
{
    public static TestMethod void myTestMethod() 
    {   
        TestLoadData.loadRCRData();
        
        Test.StartTest();
                    
        RCRInvoiceApprovalBatch batch = new RCRInvoiceApprovalBatch();           
        Database.executeBatch(batch, 10);
        
        Test.StopTest();
    }
}