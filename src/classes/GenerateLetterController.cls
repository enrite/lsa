public with sharing class GenerateLetterController
{            
    public String AttachmentId { get; set; }
    public String SentToEmailAddress { get; set; }

    public void GenerateLetter()
    {           
        try
        {                                                                                                                                       
            // generate the letter of acknowledgement and attach it to the form
            PageReference pdf = new PageReference('/apex/' + ApexPages.CurrentPage().getParameters().get('pg'));
            pdf.getParameters().put('id', ApexPages.CurrentPage().getParameters().get('id')); 
            if (ApexPages.CurrentPage().getParameters().containsKey('pages'))
            {
                pdf.getParameters().put('pages', ApexPages.CurrentPage().getParameters().get('pages'));
            }
            
            Blob b = Blob.valueOf('letter');
                        
            // we can only get the letter content if we're not in a test class                        
            if (!Test.isRunningTest())
            {
                b = pdf.getContentAsPDF();
            }
            
            Attachment a = new Attachment();
            a.ParentId = ApexPages.CurrentPage().getParameters().get('parent');
            a.ContentType = 'application/pdf';
            a.Name = ApexPages.CurrentPage().getParameters().get('file');
            a.Description = ApexPages.CurrentPage().getParameters().get('desc');
            a.Body = b;
            insert a;
            
            AttachmentId = a.Id;
            
            // see if we should automatically email the letter
            /*
            if (ApexPages.CurrentPage().getParameters().containsKey('email'))
            {                                                                                                                              
                Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();                
                msg.setToAddresses(new String[] { ApexPages.CurrentPage().getParameters().get('email') });
                msg.setSubject(ApexPages.CurrentPage().getParameters().get('subject'));
                msg.setHTMLBody(ApexPages.CurrentPage().getParameters().get('bdy'));

                Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
                attach.setContentType('application/pdf');
                attach.setFileName(a.Name);
                attach.setInline(false);
                attach.Body = b;        
                msg.setFileAttachments(new Messaging.EmailFileAttachment[] { attach });

                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { msg });
                
                SentToEmailAddress = ApexPages.CurrentPage().getParameters().get('email');                
            }*/                                                       
        }    
        catch(Exception e)
        {          
            A2HCException.formatException(e);
        }
    }
}