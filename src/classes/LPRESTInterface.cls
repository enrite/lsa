public class LPRESTInterface
{
    public LPRESTInterface()
    {
    }

    public LPRESTInterface(sObject sObj)
    {
        record = sObj;
    }

    private Lantern_Pay__c Settings
    {
        get
        {
            if(Settings == null)
            {
                Settings = Lantern_Pay__c.getOrgDefaults();
                if(Settings == null)
                {
                    throw new A2HCException('Lantern Pay custom settings are not defined');
                }
            }
            return Settings;
        }
        set;
    }

    private final String NAMED_CREDENTIAL = 'callout:' + Settings.Named_Credential__c;
    private final String PLAN_MANAGERS = '/planmanagers/';
    private final String MEMBERS = 'members';
    private final String NAMED_CREDENTIAL_PLAN_MANAGERS_PATH = NAMED_CREDENTIAL + PLAN_MANAGERS;
    private final String NAMED_CREDENTIAL_PLAN_PATH = NAMED_CREDENTIAL + PLAN_MANAGERS;
    private final String NAMED_CREDENTIAL_CONTRACT_PATH = NAMED_CREDENTIAL + '/' + MEMBERS + '/';

    public static final String LPPAYMENT_MATCHSTATUS_MATCHED = 'Matched';
    public static final String LPPAYMENT_MATCHSTATUS_NOTMATCHED = 'Not-Matched';

    public static final String LPINVOICE_APPROVAL_AUTHORIZE = 'authorize';
    public static final String LPINVOICE_APPROVAL_REJECT = 'reject';

    private sObject record;
    private List<LP_API_Error__c> messages = new List<LP_API_Error__c>();
    private Map<ID, Referred_Client__c> clientsByIds = new Map<ID, Referred_Client__c>();
    private Map<ID, RCR_Contract__c> contractsByID = new Map<ID, RCR_Contract__c>();

    public Map<String, RCR_Invoice__c> statementsByInvoiceNo = new Map<String, RCR_Invoice__c>();
    public Map<String, RCR_Invoice_Item__c> statementItemsByInvoiceNo = new Map<String, RCR_Invoice_Item__c>();
    public List<LP_Confirmed_Payment__c> lpPayments = new List<LP_Confirmed_Payment__c>();

    @future(Callout=true)
    public static void addMember(Set<ID> clientIDs)
    {
        HttpRequest request;
        HttpResponse response;
        try
        {
            LPRESTInterface lpInterface = new LPRESTInterface();
            for(Referred_Client__c client : getClients(clientIDs))
            {

                lpInterface.addMember(client, request, response);
            }
            lpInterface.saveClients();
            lpInterface.saveMessages();
        }
        catch(Exception ex)
        {
            logError('Add Client', request, response, ex, true);
        }
    }

    @future(Callout=true)
    public static void alterContractFuture(Set<ID> contractIds)
    {
        HttpRequest request;
        HttpResponse response;
        try
        {
            LPRESTInterface lpInterface = new LPRESTInterface();
            for(RCR_Contract__c so : getContracts(contractIds))
            {
                lpInterface.amendContract(so, false, request, response);
            }
            lpInterface.saveRecords();
        }
        catch(Exception ex)
        {
            logError('Add Client', request, response, ex, true);
        }
    }

    @future(Callout=true)
    public static void alterContractFuture(ID contractId)
    {
        LPRESTInterface lpInterface = new LPRESTInterface();
        for(RCR_Contract__c so : getContracts(new Set<ID>{contractId}))
        {
            lpInterface.amendContract(so, true);
        }
    }

    @future(Callout=true)
    public static void authoriseInvoice(ID invoiceItemId)
    {
        authoriseRejectInvoice(invoiceItemId, LPINVOICE_APPROVAL_AUTHORIZE);
    }

    @future(Callout=true)
    public static void rejectInvoice(ID invoiceItemId)
    {
        authoriseRejectInvoice(invoiceItemId, LPINVOICE_APPROVAL_REJECT);
    }

    public static Boolean getAuthorisationKey(RestRequest request)
    {
        system.debug(request);
        String key = Lantern_Pay__c.getOrgDefaults().Webhook_Key__c;
        return key == null || key == request.headers.get('X-SALSA-Key');
    }

//    public static void alterContract(RCR_Contract__c contract)
//    {
//        LPRESTInterface lpInterface = new LPRESTInterface();
//        lpInterface.amendContract(contract, true);
//    }

    public static void addContracts(List<RCR_Contract__c> approvedContracts)
    {
        List<RCR_Contract__c> contracts = getContracts(approvedContracts);
        if(contracts.isEmpty())
        {
            return;
        }
        Set<ID> clientIds = getClientIds(contracts);
        Map<ID, RCR_Service__c> servicesByID = getContractServices(contracts);
        Map<ID, Referred_Client__c> clientsById = new Map<ID, Referred_Client__c>(getClients(clientIds));
        LPRESTInterface lpInterface = new LPRESTInterface();
        for(RCR_Contract__c contract : contracts)
        {
            lpInterface.addContract(contract, clientsById.get(contract.Client__c), servicesByID);
        }
        lpInterface.saveContracts();
        lpInterface.saveMessages();
    }

    public static String getMemberKey(LPMemberActivationResponseJSON activationJSON)
    {
        LPRESTInterface lpInterface = new LPRESTInterface();
        return lpInterface.parseMemberKey(activationJSON);
    }

    public void logError(String method, HttpRequest request, HttpResponse response, Exception ex)
    {
        messages.add(logError(method, request, response, ex, false));
    }

    public void logError(String method, HttpResponse response, Exception ex, Map<String, String> keys)
    {
        String msg = response == null ? ex.getStackTraceString() : (response.getBody() + '\n' + ex.getStackTraceString());
        Integer code = response == null ? null : response.getStatusCode();
        messages.add(logError(method, msg, code, keys, false));
    }

    public static LP_API_Error__c logError(String method, String message, Integer code, Map<String, String> keys, Boolean saveMessage)
    {
        LP_API_Error__c apiError = new LP_API_Error__c();
        apiError.RecordTypeId = LP_API_Error__c.sObjectType.getDescribe().getRecordTypeInfosByDeveloperName().get('Error').getRecordTypeId();
        apiError.API_Call__c = method;
        apiError.Message__c = message;
        apiError.ErrorCode__c = code == null ? null : String.valueOf(code);
        if(keys != null && !keys.isEmpty())
        {
            apiError.Key__c = keys.get('Key');
            apiError.LPKey__c = keys.get('LPKey');
        }
        if(saveMessage)
        {
            insert apiError;
        }
        return apiError;
    }

    public static LP_API_Error__c logError(String method, HttpRequest request, HttpResponse response, Exception ex, Boolean saveMessage)
    {
        LP_API_Error__c apiError = new LP_API_Error__c();
        apiError.RecordTypeId = LP_API_Error__c.sObjectType.getDescribe().getRecordTypeInfosByDeveloperName().get('Error').getRecordTypeId();
        apiError.API_Call__c = method;
        apiError.Message__c = ex == null ? null : ex.getStackTraceString();
        apiError.ErrorCode__c = response == null ? null : String.valueOf(response.getStatusCode());
        apiError.Request__c = request == null ? null : request.getBody();
        apiError.Response__c = response == null ? null : response.getBody();
        if(saveMessage)
        {
            insert apiError;
        }
        return apiError;
    }

    public static Set<ID> getClientIds(List<RCR_Contract__c> contracts)
    {
        Set<ID> clientIds = new Set<ID>();
        for(RCR_Contract__c contract : contracts)
        {
            clientIds.add(contract.Client__c);
        }
        return clientIds;
    }

    public static Map<ID, RCR_Service__c> getContractServices(List<RCR_Contract__c> contracts)
    {
        Set<ID> serviceIds = new Set<ID>();
        for(RCR_Contract__c contract : contracts)
        {
            for(RCR_Contract_Scheduled_Service__c css : contract.RCR_Contract_Scheduled_Services__r)
            {
                serviceIds.add(css.RCR_Service__c);
            }
        }
        return new Map<ID, RCR_Service__c>([SELECT ID,
                                                    (SELECT ID,
                                                            Start_Date__c,
                                                            End_Date__c,
                                                            Rate__c
                                                    FROM    RCR_Service_Rates__r
                                                    ORDER BY Start_Date__c)
                                            FROM    RCR_Service__c
                                            WHERE   ID IN :serviceIds]);
    }

    public static String getDateString(Date d)
    {
        if(d == null)
        {
            return '';
        }
        return String.valueOf(d.year()) + '-' + String.valueOf(d.month()).leftPad(2, '0')  + '-' + String.valueOf(d.day()).leftPad(2, '0');
    }

    public static String getFormattedDecimal(Decimal d)
    {
        if(d == null)
        {
            return '';
        }
        return d.setScale(2).toPlainString().leftPad(2, '0');
    }

    // converts time in decimal format to HHH:MM format
    // eg 34.02 => 34:01
    public static String getFormattedHour(Decimal d)
    {
        if(d == null)
        {
            return '';
        }
        d = d.setScale(2);
        // get hour component
        Decimal hours = Math.floor(d);
        //get minute component in decimal
        Decimal minutes = d - hours;
        // convert decimal to MM format
        Integer min = (minutes * 0.6 * 100).intValue();
        return String.valueOf(hours.intValue()) + ':' + String.valueOf(min).leftPad(2, '0');
    }

    public static Date parseDate(String s)
    {
        Date dt = null;
        if(String.isNotBlank(s))
        {
            try
            {
                String[] dtParts = s.split('-');
                if(dtParts.size() == 3)
                {
                    Integer y = Integer.valueOf(dtParts[0]);
                    Integer m = Integer.valueOf(dtParts[1]);
                    Integer d = Integer.valueOf(dtParts[2]);
                    dt = Date.newInstance(y, m, d);
                }
            }
            catch(Exception ex)
            {}
        }
        return dt;
    }

    public static String convertLPDateString(String s)
    {
        Date dt = parseDate(s);
        return dt == null ? '' : dt.format();
    }

    public static List<Referred_Client__c> getClients(Set<ID> clientIDs)
    {
        return [SELECT ID,
                        Given_Name__c,
                        Family_Name__c,
                        Client_ID__c,
                        Date_Of_Birth__c,
                        Current_Status__c,
                        Current_Status_Date__c,
                        Status_Date__c,
                        LP_Member_Key__c,
                        LP_Member_Status__c,
                        CreatedDate
                FROM    Referred_Client__c
                WHERE   (Name LIKE :(clientIDs == null ? '%' : '~') OR
                        ID IN :clientIDs) AND
                        (LP_Ready_to_Integrate__c = :(clientIDs == null) OR
                        Name LIKE :(clientIDs == null ? '~' : '%'))
                ORDER BY Family_Name__c,
                        Given_Name__c
                LIMIT 101];
    }

    public static List<RCR_Contract__c> getContracts(Set<ID> contractIds)
    {
        List<RCR_Contract__c> contracts = new List<RCR_Contract__c>();
        for(ID soID : contractIds)
        {
            contracts.add(new RCR_Contract__c(ID = soID));
        }
        return  getContracts(contracts);
    }

    public static List<RCR_Contract__c> getContracts(List<RCR_Contract__c> contracts)
    {
        return [SELECT ID,
                        Name,
                        Start_Date__c,
                        End_Date__c,
                        LP_Contract_Key__c,
                        Total_Submitted__c,
                        Client__r.LP_Member_Key__c,
                        Total_Service_Order_Cost__c,
                        Account__r.LP_Biller_Key__c,
                        Account__r.Lantern_Pay_Active__c,
                        (SELECT ID,
                                Name,
                                Start_Date__c,
                                End_Date__c,
                                Total_Quantity__c,
                                Quantity_Used__c,
                                Remaining__c,
                                Use_Negotiated_Rates__c,
                                Negotiated_Rate_Standard__c,
                                Total_Cost__c,
                                RCR_Service__c,
                                Standard_Service_Code__c,
                                Standard_Service_Code_Description__c
                        FROM    RCR_Contract_Scheduled_Services__r)
                FROM    RCR_Contract__c
                WHERE   ID IN :contracts AND
                        Account__r.Lantern_Pay_Active__c = true];
    }

    public void addMember(Referred_Client__c client)
    {
        addMember(client, null, null);
    }

    public void addMember(Referred_Client__c client, HttpRequest request, HttpResponse response)
    {
        client.LP_Member_Status__c = 'Requested';
        request = getRequest('POST', getMemberURL());
        LPMemberJSON memberJSON = new LPMemberJSON();
        request.setBody(memberJSON.getJSON(client, Settings));
        request.setHeader('Content-Length', String.valueOf(request.getBody().length()));
        response = (new Http()).send(request);
        if(response.getStatusCode() == 202)
        {
            clientsByIds.put(client.ID, client);
        }
        logMessage(client, request, response);
    }

    public void saveRecords()
    {
        saveClients();
        saveContracts();
        saveStatements();
        savePayments();
        saveMessages();
    }

    public void logMessage(sObject sObj, HttpRequest request, HttpResponse response)
    {
        logMessage(sObj, null, request, response);
    }

    public void logMessage(sObject sObj, String key, HttpRequest request, HttpResponse response)
    {
        if(Settings.Log_All_Messages__c)
        {
            LP_API_Error__c message = new LP_API_Error__c();
            message.RecordTypeId = LP_API_Error__c.sObjectType.getDescribe().getRecordTypeInfosByDeveloperName().get('Message').getRecordTypeId();
            message.Record_Id__c = sObj == null ? null : sObj.ID;
            if(String.isNotBlank(message.Record_Id__c))
            {
                // have a crack at setting the related record, but press on if error
                try
                {
                    ID recordId = ID.valueOf(message.Record_Id__c);
                    if(recordId.getSobjectType() == RCR_Contract__c.SObjectType)
                    {
                        message.RCR_Service_Order__c = recordId;
                    }
                    else if(recordId.getSobjectType() == RCR_Invoice__c.SObjectType)
                    {
                        message.RCR_Activity_Statement__c = recordId;
                    }
                    else if(recordId.getSobjectType() == Referred_Client__c.SObjectType)
                    {
                        message.Participant__c = recordId;
                    }
                }
                catch(Exception ex)
                {}
            }
            message.Key__c = key;
            message.Request__c = String.isBlank(request.getBody()) ? request.getEndpoint() : request.getBody();
            message.Response__c = response.getBody();
            message.API_Call__c = request.getEndpoint();
            message.ErrorCode__c = String.valueOf(response.getStatusCode());
            messages.add(message);
        }
    }

    public List<LPMemberContractsJSON.cls_lpcontracts> getMemberContracts(Referred_Client__c client)
    {
        HttpRequest request = getRequest('GET', getMemberContractsURL(client.LP_Member_Key__c));
        //HttpRequest request = getRequest('GET', getMemberContractsURL('02d15a4-699d-47c8-a510-84acc5cd9d33'));
        HttpResponse response = (new Http()).send(request);
        system.debug(response.getBody());
        if(response.getStatusCode() == 200)
        {
            return LPMemberContractsJSON.parse(response.getBody()).getContracts();
        }
        return new List<LPMemberContractsJSON.cls_lpcontracts>();
    }

    public List<LPMemberInvoicesJSON.cls_lpinvoices> getMemberInvoices(Referred_Client__c client)
    {
        return getMemberInvoices(client, null, null);
    }

    public List<LPMemberInvoicesJSON.cls_lpinvoices> getMemberInvoices(Referred_Client__c client, Date startDate, Date endDate)
    {
        HttpRequest request = getRequest('GET', getMemberURL(client));
        HttpResponse response = (new Http()).send(request);
        system.debug(response.getBody());
        if(response.getStatusCode() == 200)
        {
            request = getRequest('GET', getMemberInvoiceHistoryURL(startDate, endDate));
            response = (new Http()).send(request);
            if(response.getStatusCode() == 200)
            {
                return LPMemberInvoicesJSON.parse(response.getBody()).getInvoices();
            }
        }
        return new List<LPMemberInvoicesJSON.cls_lpinvoices>();
    }

    public void addContract(RCR_Contract__c contract, Referred_Client__c client, Map<ID, RCR_Service__c> servicesByID)
    {
        addContract(contract, client, servicesByID, false);
    }

    public void addContract(RCR_Contract__c contract, Referred_Client__c client, Map<ID, RCR_Service__c> servicesByID, Boolean isAmendment)
    {
        if(contract.Account__r.LP_Biller_Key__c == null)
        {
            A2HCException.formatException('Service Order provider is not registered with Lantern Pay');
        }
        HttpRequest request = getRequest('POST', getContractURL(client.LP_Member_Key__c));
        LPContractJSON contractJSON = new LPContractJSON();
        request.setBody(contractJSON.getJSON(contract, servicesByID, Settings, isAmendment));
        system.debug(request.getBody());
        request.setHeader('Content-Length', String.valueOf(request.getBody().length()));
        request.setTimeout(120000);
        HttpResponse response = (new Http()).send(request);
        system.debug(response.getBody());
        if(response.getStatusCode() == 200)
        {
            LPContractResponseJSON responseJSON = LPContractResponseJSON.parse(response.getBody());
            system.debug(responseJSON);
            contract.LP_Contract_Key__c = responseJSON.contractKey;
            contractsByID.put(contract.ID, contract);
        }
        logMessage(contract, request, response);
    }

    public void amendContract(RCR_Contract__c contract, Boolean saveRecords)
    {
        amendContract(contract, saveRecords, null, null);
    }

    public void amendContract(RCR_Contract__c contract, Boolean saveRecords, HttpRequest request, HttpResponse response)
    {
        if(!contract.Account__r.Lantern_Pay_Active__c)
        {
            return;
        }
        request = getRequest('POST', getTerminateContractURL(contract.LP_Contract_Key__c));
        request.setTimeout(120000);
        response = (new Http()).send(request);
        system.debug(response.getStatusCode());
        system.debug(response.getBody());
        logMessage(contract, request, response);
        if(response.getStatusCode() == 204)
        {
            Referred_Client__c client = null;
            system.debug(contract.Client__c);
            for(Referred_Client__c cl : getClients(new Set<ID>{contract.Client__c}))
            {
                client = cl;
            }
            if(client == null)
            {
                throw new A2HCException('Client not registered with LanternPay');
            }
            addContract(contract, client, getContractServices(new List<RCR_Contract__c>{contract}), true);
            contractsByID.put(contract.ID, contract);
        }
        if(saveRecords)
        {
            saveRecords();
        }
    }

    public void addInvoice(LPInvoiceRequestJSON invoiceRequestJSON)
    {
        HttpResponse response;
        HttpRequest request;
        try
        {
            request = getRequest('GET', getNamedCredentialURL(invoiceRequestJSON.data.invoiceUri));
            response = (new Http()).send(request);
            logMessage(null, request, response);
            if(response.getStatusCode() == 200)
            {
                LPInvoiceDetailJSON invoiceJSON = LPInvoiceDetailJSON.parse(response.getBody());
                for(RCR_Invoice__c invoice : invoiceJSON.createInvoices(this, invoiceRequestJSON))
                {
                    logMessage(invoice, invoice.LP_Invoice_Key__c, request, response);
                }
            }
        }
        catch(Exception ex)
        {
            logError('Add Invoice', request, response, ex);
        }
    }

    public void createPayments(LPPaymentRequestJSON paymentRequestJSON)
    {
        HttpResponse response;
        HttpRequest request;
        try
        {
            request = getRequest('GET', getNamedCredentialURL(paymentRequestJSON.data.paymentUri));
            response = (new Http()).send(request);
            logMessage(null, request, response);
            if(response.getStatusCode() == 200)
            {
                LPPaymentDetailJSON paymentJSON = LPPaymentDetailJSON.parse(response.getBody());
                paymentJSON.createPayments(this);
                saveRecords();
            }
        }
        catch(Exception ex)
        {
            logError('Create Payments', request, response, ex);
        }
    }

    public void authorizeInvoice(RCR_Invoice_Item__c invoiceItem, String action)
    {
        authoriseInvoice(new List<ID>{invoiceItem.ID}, action, false);
    }

    private static void authoriseRejectInvoice(ID invoiceItemId, String action)
    {
        LPRESTInterface lpInterface = new LPRESTInterface();
        lpInterface.authoriseInvoice(new List<ID>{invoiceItemId}, action, true);
        lpInterface.saveRecords();
    }

    private void authoriseInvoice(List<ID> invoiceItemIds, String action, Boolean saveRecords)
    {
        HttpResponse response;
        HttpRequest request;
        try
        {
            RCR_Invoice__c invoice = null;
            for(RCR_Invoice__c inv : [SELECT ID,
                                            LP_Invoice_Key__c,
                                            (SELECT ID,
                                                    LP_Claim_Key__c,
                                                    RCR_Service_Order__c
                                            FROM    RCR_Invoice_Items__r
                                            WHERE ID IN :invoiceItemIds)
                                    FROM    RCR_Invoice__c
                                    WHERE   LP_Invoice_Key__c != null AND
                                            ID IN (SELECT RCR_Invoice__c
                                                    FROM   RCR_Invoice_Item__c
                                                    WHERE   ID = :invoiceItemIds)])
            {
                invoice = inv;
            }
            if(invoice == null)
            {
                return;
            }
            request = getRequest('POST', getInvoiceURL(invoice.LP_Invoice_Key__c, action));
            String jsonString = LPAuthoriseInvoiceJSON.serialise(invoice, action);
            request.setBody(jsonString);
            request.setHeader('Content-Length', String.valueOf(jsonString.length()));
            response = (new Http()).send(request);
            logMessage(invoice, request, response);
            if(response.getStatusCode() == 200)
            {
                // now LP contracts need amending as contract remaining needs to be updated
                Map<ID, RCR_Contract__c> serviceOrdersById = new Map<ID, RCR_Contract__c>();
                for(RCR_Invoice_Item__c item : invoice.RCR_Invoice_Items__r)
                {
                    serviceOrdersById.put(item.RCR_Service_Order__c, new RCR_Contract__c(ID = item.RCR_Service_Order__c));
                }
                for(RCR_Contract__c so : getContracts(serviceOrdersById.values()))
                {
                    amendContract(so, false);
                }
                if(saveRecords)
                {
                    saveRecords();
                }
            }
        }
        catch(Exception ex)
        {
            logError('Authorise Invoice', request, response, ex);
        }
    }

    public void reconcileInvoices()
    {
        if(!statementsByInvoiceNo.isEmpty())
        {
            RCRInvoiceSchedule invSchedule = new  RCRInvoiceSchedule();
            invSchedule.scheduleReconciliation(statementsByInvoiceNo.values(), true);
        }
    }

    private LPMemberDetailJSON getMemberDetail(Referred_Client__c client)
    {
        LPMemberDetailJSON memberDetailJSON;
        HttpRequest request = getRequest('GET', getMemberURL(client));
        HttpResponse response = (new Http()).send(request);
        if(response.getStatusCode() == 200)
        {
            memberDetailJSON = LPMemberDetailJSON.parse(response.getBody());
            if(client.LP_Member_Key__c == null)
            {
                if(memberDetailJSON.memberKey != null)
                {
                    client.LP_Member_Status__c = 'Activated';
                    client.LP_Member_Date__c = Datetime.now();
                    client.LP_Member_Key__c = memberDetailJSON.memberKey;
                    clientsByIds.put(client.ID, client);
                }
            }
        }
        logMessage(client, request, response);
        return memberDetailJSON;
    }

    private String parseMemberKey(LPMemberActivationResponseJSON activationJSON)
    {
        HttpResponse response;
        HttpRequest request;
        LPMemberDetailJSON memberDetailJSON;
        try
        {
            system.debug(activationJSON.data.memberUri);
            request = getRequest('GET', getNamedCredentialURL(activationJSON.data.memberUri));
            response = (new Http()).send(request);
            memberDetailJSON = LPMemberDetailJSON.parse(response.getBody());
            return memberDetailJSON.memberKey;
        }
        catch(Exception ex)
        {
            if(memberDetailJSON != null && memberDetailJSON.memberKey != null)
            {
                logError('Get Client Detail', response, ex, new Map<String, String>{'Key' => memberDetailJSON.memberKey});
            }
            else
            {
                logError('Add Client', request, response, ex);
            }
            return null;
        }
    }

    private void saveMessages()
    {
        if(!statementsByInvoiceNo.isEmpty())
        {
            for(LP_API_Error__c message : messages)
            {
                if(String.isNotBlank(message.Key__c))
                {
                    for(RCR_Invoice__c invoice : statementsByInvoiceNo.values())
                    {
                        if(invoice.LP_Invoice_Key__c == message.Key__c)
                        {
                            message.RCR_Activity_Statement__c = invoice.ID;
                        }
                    }
                }
            }
        }
        upsert messages;
    }

    private void saveClients()
    {
        if(!clientsByIds.isEmpty())
        {
            TriggerBypass.Client = true;
            update clientsByIds.values();
            TriggerBypass.Client = false;
        }
    }

    private void saveContracts()
    {
        if(!contractsByID.isEmpty())
        {
            TriggerBypass.RCRServiceOrder = true;
            update contractsByID.values();
            TriggerBypass.RCRServiceOrder = false;
        }
    }

    private void saveStatements()
    {
        if(!statementsByInvoiceNo.isEmpty())
        {
            upsert statementsByInvoiceNo.values();
            List<RCR_Invoice_Item__c> items = new List<RCR_Invoice_Item__c>();
            for(String lpInvoiceNo : statementItemsByInvoiceNo.keySet())
            {
                RCR_Invoice__c invoice = statementsByInvoiceNo.get(lpInvoiceNo);
                RCR_Invoice_Item__c item = statementItemsByInvoiceNo.get(lpInvoiceNo);
                item.RCR_Invoice__c = invoice.ID;
                items.add(item);
            }
            upsert items;
            // now set the invoice number as it must be unique per provider, invoice numbers can be duplicated in LP
            List<RCR_Invoice__c> invoices = [SELECT ID,
                                                    Name
                                            FROM    RCR_Invoice__c
                                            WHERE   LP_Invoice_Number__c IN :statementsByInvoiceNo.keySet()];
            for(RCR_Invoice__c inv : invoices)
            {
                inv.Invoice_Number__c = inv.Name;
            }
            update invoices;
        }
    }

    private void savePayments()
    {
        if(!lpPayments.isEmpty())
        {
            upsert lpPayments LP_Confirmed_Payment__c.Claim_Key__c;
        }
    }

    private HttpRequest getRequest(String method, String url)
    {
        HttpRequest request = new HttpRequest();
        request.setEndpoint(url);
        request.setMethod(method);
        request.setHeader('X-LanternPay-Api-Client-Id', Settings.LanternPay_Api_Client_Id__c);
        if(Settings.Timeout_millisecs__c != null)
        {
            request.setTimeout(Settings.Timeout_millisecs__c.intValue());
        }
        if(method == 'POST')
        {
            request.setHeader('Content-Type', 'application/json; charset=utf-8');
        }
        return request;
    }

    private String getNamedCredentialURL(String url)
    {
        // need to replace the URL host with named credential to get authentication
        URL lpUrl = new URL(url);
        String query = lpUrl.getQuery();
        return NAMED_CREDENTIAL + lpUrl.getPath() + (query == null ? '' : ('?' + query));
    }

    private String getMemberURL()
    {
        return NAMED_CREDENTIAL_PLAN_MANAGERS_PATH + Settings.Plan_Manager_Key__c + '/' + MEMBERS;
    }

    private String getMemberURL(Referred_Client__c client)
    {
        return getMemberURL() + '/' + client.Client_ID__c + '?programCode=' + Settings.Program_Code__c;
    }

    private String getContractURL(String memberKey)
    {
        return NAMED_CREDENTIAL_CONTRACT_PATH + memberKey + '/contracts';
    }

    private String getTerminateContractURL(String contractKey)
    {
        return NAMED_CREDENTIAL + '/contracts/' + contractKey + '/terminate';
    }

    private String getInvoiceURL(String invoiceKey, String action)
    {
        return NAMED_CREDENTIAL + '/invoices/' + invoiceKey + '/authorize';// + action;
    }

    private String getMemberContractsURL(String memberKey)
    {
        return NAMED_CREDENTIAL + '/members/' + memberKey + '/contracts';
    }

    private String getMemberInvoiceHistoryURL(Date startDate, Date endDate)
    {
        String fromDate = startDate == null ? '2018-01-01' : getDateString(startDate);
        String toDate = endDate == null ? '2100-01-01' : getDateString(endDate);
        return NAMED_CREDENTIAL_PLAN_MANAGERS_PATH + Settings.Plan_Manager_Key__c + '/invoices?fromDate=' + fromDate + '&toDate=' + toDate;
    }
}