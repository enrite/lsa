@isTest
private class TestAccrualsReportController
{
    
    static testMethod void myUnitTest() 
    {
        TestLoadData.loadRCRdata();
        
        Test.StartTest();
                        
        AccrualsReportController ctr = new AccrualsReportController();
        ctr.Params.Start_Date__c = Date.newInstance(2000, 1, 1);
        ctr.Params.End_Date__c = Date.newInstance(2100, 1, 1);
        ctr.FindData();
        system.debug(ctr.CSVString);        
        
        Test.StopTest();
    }
}