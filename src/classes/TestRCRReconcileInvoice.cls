/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestRCRReconcileInvoice
{

   @IsTest(SeeAllData=true)
    static void test1() 
    {
        RCR_Invoice__c invoice = null;
        String cssID = null;
        for(RCR_Invoice_Item__c it : [SELECT ID,
                                            Reconciled_To__c,
                                            Error__c,
                                            RCR_Invoice__c,
                                            RCR_Contract_Scheduled_Service__c,
                                            RCR_Invoice__r.Invoice_Number__c
                                    FROM    RCR_Invoice_Item__c
                                    WHERE   RCR_Invoice__r.All_Line_Count__c < = 8 AND
                                            RCR_Invoice__r.Status__c LIKE 'Reconciled%' AND
                                            RCR_Invoice__r.RecordType.Name = 'Created' AND
                                            (RCR_Contract_Scheduled_Service__r.RCR_Contract__r.RecordType.Name = :APSRecordTypes.RCRServiceOrder_ServiceOrder OR
                                            RCR_Contract_Scheduled_Service__r.RCR_Contract__r.RecordType.Name = :APSRecordTypes.RCRServiceOrder_ServiceOrderCBMS)                                       
                                    LIMIT 100])
        {
            if(it.RCR_Invoice__r.Invoice_Number__c.length() <= 12)
            {
                invoice = [SELECT ID,
                                Account__c
                         FROM   RCR_Invoice__c
                         WHERE  ID = :it.RCR_Invoice__c];
                         
                 cssID = it.RCR_Contract_Scheduled_Service__c;
                 break;
            }
        }
        if(invoice == null)
        {
        	return;
        }

        RCR_Invoice_Upload__c fileUpload = new RCR_Invoice_Upload__c();
        fileUpload.Account__c = invoice.Account__c;
        fileUpload.Date__c = Date.today();
        fileUpload.File_Name__c = 'test.xxx';
        fileUpload.Description__c = 'test upload';
        insert fileUpload;

        Test.startTest();
		
		RCRReconcileInvoice reconExt = new RCRReconcileInvoice();
		reconExt = new RCRReconcileInvoice(new ApexPages.StandardController([SELECT ID FROM RCR_Invoice__c LIMIT 1]));
        reconExt = new RCRReconcileInvoice(invoice.Id);

        reconExt.Invoice = null;
        system.debug(reconExt.Invoice);

        system.assert(reconExt.PendingItems != null);

        Test.setCurrentPageReference(Page.RCRAutoReconcile);
        ApexPages.currentPage().getParameters().put('FileUploadId', fileUpload.Id);
        reconExt = new RCRReconcileInvoice(new ApexPages.StandardController(invoice));
        system.debug(reconExt.FileUploadID);

        ApexPages.currentPage().getParameters().put('id', invoice.ID);
        reconExt = new RCRReconcileInvoice(new ApexPages.StandardController(invoice));

        reconExt.reconcile();
        
        List<RCR_Invoice_Item__c> items = [SELECT  ID,
                                                        Name,
                                                        Code__c,
                                                        Error__c,
                                                        Quantity__c,
                                                        Rate__c,
                                                        Total__c,
                                                        Status__c,
                                                        Activity_Date__c,
                                                        RCR_Service_Order__c,
                                                        RecordTypeId
                                                 FROM   RCR_Invoice_Item__c
                                                 WHERE  RCR_Invoice__c = :Invoice.Id AND
                                                 		(Error__c = '' OR
                                                 		Error__c = null) AND
                                                 		(RCR_Contract_Scheduled_Service__r.RCR_Contract__r.RecordType.Name = :APSRecordTypes.RCRServiceOrder_ServiceOrder OR
                                            			RCR_Contract_Scheduled_Service__r.RCR_Contract__r.RecordType.Name = :APSRecordTypes.RCRServiceOrder_ServiceOrderCBMS)
                                                 ORDER BY Service_Order_Number__c, Activity_Date__c ASC]; 

        for(RCR_Invoice_Item__c i : items)
        {
            i.RCR_Contract_Scheduled_Service__c = cssID;
            i.Reconciled_Service__c = cssID;
           // i.Error__c = '';
            
        }
        update items;

        reconExt.Invoice = RCRReconcileInvoice.getInvoice(reconExt.Invoice.Id);

        reconExt.submit();

        reconExt.createServiceQuery();

        reconExt.createServiceQueries(reconExt.Invoice.Id);

        reconExt.Invoice.RecordTypeId = A2HCUtilities.getRecordType('RCR_Invoice__c', 'Processing').Id;
        update reconExt.Invoice;

        system.debug(reconExt.processingMessage);
        
        ApexPages.currentPage().getParameters().put('submitted', 'Y');
        system.debug(reconExt.processingMessage);
        
        system.debug(reconExt.FailedItems);
        Test.stopTest();
    }

    @isTest(SeeAllData=true)
    private static void test2()
    {
        RCR_Invoice__c invoice = null;
        for(RCR_Invoice_Item__c it : [SELECT ID,
                                            Reconciled_To__c,
                                            Error__c,
                                            RCR_Invoice__c,
                                            RCR_Invoice__r.Invoice_Number__c
                                    FROM    RCR_Invoice_Item__c
                                    WHERE   RCR_Invoice__r.All_Line_Count__c < = 8 AND
                                            RCR_Invoice__r.Status__c LIKE 'Reconciled%' AND
                                            RCR_Invoice__r.RecordType.Name = 'Created'  AND
                                            (RCR_Contract_Scheduled_Service__r.RCR_Contract__r.RecordType.Name = :APSRecordTypes.RCRServiceOrder_ServiceOrder OR
                                            RCR_Contract_Scheduled_Service__r.RCR_Contract__r.RecordType.Name = :APSRecordTypes.RCRServiceOrder_ServiceOrderCBMS) AND
                                            RCR_Contract_Scheduled_Service__c IN (SELECT    ID
                                                                                    FROM    RCR_Contract_Scheduled_Service__c
                                                                                    WHERE   Public_holidays_not_allowed__c = :APS_PicklistValues.RCRContractScheduledService_PublicHolidaysNotAllowed_UseExceptions)                                          
                                    LIMIT 100])
        {
            if(it.RCR_Invoice__r.Invoice_Number__c.length() <= 12)
            {
                invoice = [SELECT ID,
                                Account__c
                         FROM   RCR_Invoice__c
                         WHERE  ID = :it.RCR_Invoice__c];
                 break;
            }
        }
        if(invoice == null)
        {
        	return;
        }

        RCR_Invoice_Upload__c fileUpload = new RCR_Invoice_Upload__c();
        fileUpload.Account__c = invoice.Account__c;
        fileUpload.Date__c = Date.today();
        fileUpload.File_Name__c = 'test.xxx';
        fileUpload.Description__c = 'test upload';
        insert fileUpload;

        Test.startTest();
		RCRReconcileInvoice ext = new RCRReconcileInvoice();
		ext = new RCRReconcileInvoice(new ApexPages.StandardController([SELECT ID FROM RCR_Invoice__c LIMIT 1]));
        ext = new RCRReconcileInvoice(invoice.ID);
        ext.submitBatch(10);
        
        ext.generateRCTI();
        Test.stopTest();
    }
    
    @isTest(SeeAllData=true)
    private static void test3()
    {
        RCR_Invoice__c invoice = null;
        for(RCR_Invoice_Item__c it : [SELECT ID,
                                            Reconciled_To__c,
                                            Error__c,
                                            RCR_Invoice__c,
                                            RCR_Invoice__r.Invoice_Number__c
                                    FROM    RCR_Invoice_Item__c
                                    WHERE   RCR_Invoice__r.Account__r.RCTI_Agreement__c = true 
                                    LIMIT 100])
        {
            if(it.RCR_Invoice__r.Invoice_Number__c.length() <= 12)
            {
                invoice = [SELECT ID,
                                Account__c
                         FROM   RCR_Invoice__c
                         WHERE  ID = :it.RCR_Invoice__c];
                 break;
            }
        }
        if(invoice == null)
        {
        	return;
        }

        Test.startTest();
        RCRReconcileInvoice ext = new RCRReconcileInvoice(invoice.ID);
        ext.generateRCTI();
        
        List<RCR_Invoice_Item__c> items = [SELECT  ID,
                                                        Name,
                                                        Code__c,
                                                        Error__c,
                                                        Quantity__c,
                                                        Rate__c,
                                                        Total__c,
                                                        Status__c,
                                                        Activity_Date__c,
                                                        RCR_Service_Order__c,
                                                        RecordTypeId
                                                 FROM   RCR_Invoice_Item__c
                                                 WHERE  RCR_Invoice__c = :Invoice.Id
                                                 ORDER BY Service_Order_Number__c, Activity_Date__c ASC];

        ext.submitInvoice(items);
        
        ext.Invoice = RCRReconcileInvoice.getInvoice(invoice.Id);
        ext.submitBatch(items.size());
              
        Test.stopTest();
    }
    
}