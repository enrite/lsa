public with sharing class RCRContractScheduledServiceExtension 
	extends A2HCPageBase
{
    public RCRContractScheduledServiceExtension(ApexPages.StandardController con) 
    {
    	controller = con;
    }

    private ApexPages.StandardController controller;

    public PageReference redirect()
    {
    	PageReference pg;
    	if(CurrentUser.Profile.Name == 'LSA SP Community')
    	{
    		pg = Page.SPPortalScheduledService;
    		pg.getParameters().put('id', controller.getId());
    	}
    	else
    	{
    		pg = new PageReference('/' + controller.getId());
    		pg.getParameters().put('nooverride', '1');
    	}
    	pg.getParameters().put('retURL', getParameter('retURL'));
    	return pg;
    }
}