@isTest
public class TestClientFormsController
{                   
    static testMethod void myTestMethod()
    {
        TestLoadFormData data = new TestLoadFormData();
         
        Test.StartTest();         
        
        Form__c f = new Form__c(Client__c = data.Client.Id);
        insert f;
        
        Case_Notes__c cn = new Case_Notes__c(Client__c = data.Client.Id);
        cn.Display_on_Application_Assessment__c = true;
        insert cn;
        
        Attachment a = new Attachment();
        a.ParentId = cn.Id;
        a.Name = 'test';
        a.Body = Blob.valueOf('test');
        insert a;
        
        ClientFormsController c = new ClientFormsController();
        c.Client = data.Client;
        system.debug(c.Forms);
        
        Test.StopTest();       
    }
}