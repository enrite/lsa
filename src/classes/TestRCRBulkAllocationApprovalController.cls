@isTest//(seeAllData=true)
private class TestRCRBulkAllocationApprovalController 
{
//    static testMethod void myUnitTest()
//    {
//        TestLoadData.loadRCRData();
//
//        Test.StartTest();
//
//        RCRBulkAllocationApprovalController cntlr = new RCRBulkAllocationApprovalController();
//
//        cntlr.ClientOrGroup = 'Test';
//        system.debug(cntlr.ClientOrGroup);
//        cntlr.CCMSID = 'Test';
//        system.debug(cntlr.CCMSID);
//
//        //cntlr.doSearch();
//        cntlr.cancel();
//
//        List<RCR_Contract__c> rollovers = new List<RCR_Contract__c>();
//        List<Approval.ProcessSubmitRequest> approvalRequests = new List<Approval.ProcessSubmitRequest>();
//
//        for(RCR_Contract__c rollover : [SELECT  Id,
//                                                Name,
//                                                Client_Name__c,
//                                                Start_Date__c,
//                                                End_Date__c,
//                                                Total_Service_Order_Cost__c,
//                                                Approval_Process_Step__c,
//                                                Client__r.CCMS_ID__c,
//                                                Account__r.Name,
//                                                Source_Record__r.Total_Service_Order_Cost__c
////                                                (SELECT Id,
////                                                        Name,
////                                                        Allocation_Type__c,
////                                                        Amount__c,
////                                                        Status__c,
////                                                        Client__r.Name
////                                                 FROM   IF_Allocations__r
////                                                 WHERE  Reason__c = :APS_PicklistValues.IFAllocation_Reason_RolloverTopUp)
//                                        FROM    RCR_Contract__c
//                                        WHERE   RecordType.Name = 'Rollover'
//                                        AND     Destination_Record__c = null
//                                        AND     Status__c != :APS_PicklistValues.RCRContract_Status_PendingApproval
//                                        AND     Approval_Process_Step__c = null
//                                        LIMIT 10])
//        {
//            cntlr.Rollovers.add(new RCRBulkAllocationApprovalController.RolloverApprovalWrapper(rollover));
//        }
//
//        system.debug(cntlr.Rollovers.size());
//        //cntlr.getRequest(cntlr.Rollovers[0].Rollover.Id, 'Approve');
//
//        boolean theBoolean = false;
//        boolean theOtherBoolean = true;
//        for(RCRBulkAllocationApprovalController.RolloverApprovalWrapper wrap : cntlr.Rollovers)
//        {
//            theBoolean = !theBoolean;
//            theOtherBoolean = !theOtherBoolean;
//
//            system.debug(theBoolean);
//            system.debug(theOtherBoolean);
//            system.debug(wrap.Rollover.IF_Allocations__r);
//
//            system.debug(wrap.Approve);
//            system.debug(wrap.Reject);
//            system.debug(wrap.DirectorApprovalNotRequired);
//            system.debug(wrap.Rollover);
//
//            ProcessInstanceWorkItem workItem = new ProcessInstanceWorkItem();
//            wrap.RolloverWorkItem = workItem;
//            wrap.AllocationWorkItems.add(workItem);
//
//            wrap.Approve = theBoolean;
//            wrap.Reject = theOtherBoolean;
//            wrap.DirectorApprovalNotRequired = true;
//        }
//
//        cntlr.approveOrReject();
//
//        RCRBulkAllocationApprovalController.RolloverApprovalWrapper raw = new RCRBulkAllocationApprovalController.RolloverApprovalWrapper(null);
//        raw.Approve = true;
//        system.debug(raw.Approve);
//        raw.Reject = true;
//        system.debug(raw.Reject);
//        raw.DirectorApprovalNotRequired = true;
//        system.debug(raw.DirectorApprovalNotRequired);
//        raw.RolloverWorkItem = null;
//        system.debug(raw.RolloverWorkItem);
//        system.debug(raw.AllocationWorkItems);
//
//        Test.StopTest();
//    }
//
//    static testMethod void testExceptions()
//    {
//        TestLoadData.loadRCRData();
//
//        Test.StartTest();
//
//         RCRBulkAllocationApprovalController cntlr = new RCRBulkAllocationApprovalController();
//         cntlr.forceTestError = true;
//         try
//         {
//            cntlr.doSearch();
//         }
//         catch(Exception ex){}
//         try
//         {
//            cntlr.approveOrReject();
//         }
//         catch(Exception ex){}
//
//        Test.StopTest();
//    }
}