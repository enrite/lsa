public with sharing class ParticipantPortalMyRequestEditExtension
    extends A2HCPageBase
{
    public ParticipantPortalMyRequestEditExtension(ApexPages.StandardController c)
    {
        record = (Request__c)c.getRecord();
        String clientID = getParameter('clId');
        if(String.isNotBlank(clientID) && record.Client__c == null)
        {
            record.Client__c = clientID;
        }
        for(Referred_Client__c cl : [SELECT Name 
                                    FROM    Referred_Client__c 
                                    WHERE   ID = :record.Client__c])
        {
            ClientName = cl.Name;
        }
    }

    private Request__c record;

    public String ClientName
    {
        get;
        private set;
    }
    
    public PageReference SaveOverride()
    {
        try
        {
            if(!isRequiredFieldValid(record.Request_Details__c, 'Message Details', true))
            {
                return null;
            }
            upsert record;
            
            PageReference ref = Page.ParticipantPortalMyRequestView;
            ref.getParameters().put('id', record.Id);
            ref.setRedirect(true);
            return ref;
        }
        catch(Exception e)
        {
            return A2HCException.formatException(e);
        }
    }
}