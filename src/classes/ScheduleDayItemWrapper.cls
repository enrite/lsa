public class ScheduleDayItemWrapper
{
	public ScheduleDayItemWrapper(Id pId, string pLabel)
	{
		RecordId = pId;
		Label = pLabel;
		IsException = false;
		AvailableForPublicHoliday = false;
		PublicHolidaysAtStandardRate = false;
	}

	public ScheduleDayItemWrapper(Id pId, string pLabel, decimal pQty)
	{
		RecordId = pId;
		Label = pLabel;
		Quantity = pQty;
		IsException = false;
		AvailableForPublicHoliday = false;
		PublicHolidaysAtStandardRate = false;
	}

	public Id RecordId
	{
		get;
		set;
	}

	public string Label
	{
		get;
		set;
	}

	public string Colour
	{
		get;
		set;
	}

	public decimal Quantity
	{
		get;
		set;
	}

	public decimal PublicHolidayQuantity
	{
		get;
		set;
	}

	public Boolean IsException
	{
		get;
		set;
	}

	public Boolean AvailableForPublicHoliday
	{
		get;
		set;
	}
	
	public Boolean PublicHolidaysAtStandardRate
	{
		get;
		set;
	}
}