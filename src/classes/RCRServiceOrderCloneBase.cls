public virtual class RCRServiceOrderCloneBase 
{
    public RCRServiceOrderCloneBase() 
    {
    }
    
    public RCRServiceOrderCloneBase(RCRServiceOrderCloneBase previousBatch)
    {   
        if(previousBatch != null)
        {
            rolloverAllocationIds = previousBatch.RolloversReadyForAllocationIds;
            Mode = previousBatch.Mode;
            NewEndDate = previousBatch.NewEndDate;
            lastModifiedUserIds = previousBatch.UserIds;            
            prevJobId = previousBatch.ApexJobId;
            ServiceOrderIds.addAll(previousBatch.ServiceOrderIds);
        }
    }
    
    public RCRServiceOrderCloneBase(Date pNewEndDate)
    {
        NewEndDate = pNewEndDate;
    }
    
    protected Set<ID> rolloverAllocationIds = new Set<ID>();
    protected Set<ID> lastModifiedUserIds = new Set<ID>();
    private ID prevJobId;
    
    public Set<Id> ServiceOrderIds
    {
        get
        {
            if(ServiceOrderIds == null)
            {
                ServiceOrderIds = new Set<Id>();
            }
            return ServiceOrderIds;
        }
        set;
    }
    
    public Set<Id> RolloversReadyForAllocationIds
    {
        get
        {
            return rolloverAllocationIds;
        }
    }
    
    public Set<Id> UserIds
    {
        get
        {
            return lastModifiedUserIds;
        }
    }
    
    public Id ApexJobId
    {
        get;
        private set;
    }
        
    public String ScheduleId 
    {
        get
        {
            if(ScheduleID != null && ScheduleID.length() > 15)
            {
                return ScheduleID.substring(0, 15);
            }
            return ScheduleID;
        }
        set;
    }
    
    public enum RunMode
    {
        Rollover,
        RolloverApproved,
        RequestApproved
    }
    
    public RunMode Mode
    {
        get;
        set;        
    }
    
    public Date NewEndDate
    {
        get;
        set;
    }
    
    @TestVisible
    protected RCR_Contract__c getServiceOrder(ID serviceOrderID) 
    {
        return RCRContractExtension.getContract(serviceOrderID);
    }
    
    @TestVisible
    protected ID getClientOrProgram(RCR_Contract__c oldServiceOrder)
    {
        if(oldServiceOrder.Client__c != null)
        {
            return oldServiceOrder.Client__c;
        }
        else if(oldServiceOrder.Group_Program__c != null)
        {
            String nameField = oldServiceOrder.Group_Program__c.length() > 80 ? oldServiceOrder.Group_Program__c.substring(0, 80) : oldServiceOrder.Group_Program__c;
            for(Referred_Client__c cl : [SELECT     ID
                                            FROM    Referred_Client__c
                                            WHERE   Account__r.API_Name__c = :APS_PicklistValues.Account_DCSI_RCR_Programs AND
                                                    Name = :nameField])
            {
                return cl.ID;
            }   
            
            ID accID;
            try
            {
                accID = [SELECT ID
                        FROM    Account
                        WHERE   API_Name__c = :APS_PicklistValues.Account_DCSI_RCR_Programs].ID;
            }
            catch(Exception ex)
            {
                throw new A2HCException('DCSI - RCR Programs account does not exist, please contact Support.');
            }
            
            Referred_Client__c programClient = new Referred_Client__c(Account__c = accID, 
                                                                            Name = nameField);                      
            insert programClient;
            return programClient.ID;                                
        }
        return null;
    }
    
    @TestVisible
    protected RCR_Contract__c processRecord(RCR_Contract__c existingServiceOrder, 
                            System.savePoint sp,
                            List<Batch_Log__c> batchLogs, 
                            Database.BatchableContext batch)
    {
        Boolean success = true;                                                  
        RCR_Contract__c newServiceOrder = null; 
        existingServiceOrder = getServiceOrder(existingServiceOrder.ID);
        lastModifiedUserIds.add(existingServiceOrder.LastModifiedById);
        if(existingServiceOrder.Destination_Record__c == null)
        {                                   
            newServiceOrder = copyServiceOrder(existingServiceOrder, batchLogs, batch);
            success = newServiceOrder != null;
        }
        else
        {
            newServiceOrder = getServiceOrder(existingServiceOrder.Destination_Record__c);
        }   
        // add to 
        if(success &&
                Mode == RunMode.Rollover && 
                newServiceOrder.Rollover_Status__c == APS_PicklistValues.RCRContract_RolloverStatus_ReadyForAllocation) 
        {
            rolloverAllocationIds.add(newServiceOrder.ID); 
        }                 
               
        if(!success)
        {
            Database.rollback(sp);
            return null;
        }
        return newServiceOrder;
    }
    
    @TestVisible
    protected RCR_Contract__c copyServiceOrder(RCR_Contract__c existingServiceOrder)
    {
        return copyServiceOrder(existingServiceOrder, null, null);
    } 
    
    @TestVisible
    protected RCR_Contract__c copyServiceOrder(RCR_Contract__c existingServiceOrder, 
                                                List<Batch_Log__c> batchLogs, 
                                                Database.BatchableContext batch) 
    {
        RCR_Contract__c newServiceOrder = existingServiceOrder.clone(false, true, false, false);        
        
//        for(RCR_CBMS_Contract__c cbms : [SELECT Other_Risks_Description__c,
//                                                Health_Universal_precautions__c,
//                                                Health_Seizures__c,
//                                                Health_Other__c,
//                                                Health_Diabetic__c,
//                                                Health_Asthmatic__c,
//                                                Health_Allergies__c,
//                                                Environ_Remote_Location__c,
//                                                Environ_Pets__c,
//                                                Environ_Other__c,
//                                                Environ_Long_Grass__c,
//                                                Confidential__c,
//                                                Confidential_Other__c,
//                                                Behaviour_Other__c,
//                                                Behaviour_Client_Agressive__c,
//                                                Behaviour_Carer_Agressive__c,
//                                                Behaviour_2_Person_Visit__c,
//                                                Region__c,
//                                                Service_Coordinator_Team__c,
//                                                Service_Coordinator_Name__c,
//                                                Service_Coordinator_Phone__c
//                                        FROM    RCR_CBMS_Contract__c
//                                        WHERE   ID  = :existingServiceOrder.RCR_CBMS_Contract__c])
//        {
//            newServiceOrder.Risk_Oth_Det__c = cbms.Other_Risks_Description__c;
//            newServiceOrder.Risk_Univ_Prec__c = cbms.Health_Universal_precautions__c;
//            newServiceOrder.Risk_Seizures__c = cbms.Health_Seizures__c;
//            newServiceOrder.Risk_Hlth_Oth__c = cbms.Health_Other__c;
//            newServiceOrder.Risk_Diabetic__c = cbms.Health_Diabetic__c;
//            newServiceOrder.Risk_Asthmatic__c = cbms.Health_Asthmatic__c;
//            newServiceOrder.Risk_Allergies__c = cbms.Health_Allergies__c;
//            newServiceOrder.Risk_Remote__c = cbms.Environ_Remote_Location__c;
//            newServiceOrder.Risk_Pets__c = cbms.Environ_Pets__c;
//            newServiceOrder.Risk_Env_Oth__c = cbms.Environ_Other__c;
//            newServiceOrder.Risk_Grass__c = cbms.Environ_Long_Grass__c;
//            newServiceOrder.Risk_Conf_Issues__c = cbms.Confidential__c;
//            newServiceOrder.Risk_Add_Oth__c = cbms.Confidential_Other__c;
//            newServiceOrder.Risk_Behav_Oth__c = cbms.Behaviour_Other__c;
//            newServiceOrder.Risk_Client_Aggr__c = cbms.Behaviour_Client_Agressive__c;
//            newServiceOrder.Risk_Carer_Aggr__c = cbms.Behaviour_Carer_Agressive__c;
//            newServiceOrder.Risk_2_Pers_Visit__c = cbms.Behaviour_2_Person_Visit__c;
//            newServiceOrder.CBMS_Region__c = cbms.Region__c;
//            newServiceOrder.CBMS_Team__c = cbms.Service_Coordinator_Team__c;
//            newServiceOrder.CBMS_Service_Coordinator__c = cbms.Service_Coordinator_Name__c;
//            newServiceOrder.CBMS_Service_Coordinator_Phone__c = cbms.Service_Coordinator_Phone__c;
//
//            if(Mode == RunMode.Rollover)
//            {
//                // Move comments from Comments field to Service to be provided field.
//                newServiceOrder.Service_to_be_provided__c = newServiceOrder.Comments__c;
//                newServiceOrder.Comments__c = null;
//            }
//        }
        newServiceOrder.Source_Record__c = existingServiceOrder.Id;
        newServiceOrder.Client__c = getClientOrProgram(existingServiceOrder);
        newServiceOrder.Name = existingServiceOrder.Name;     
        newServiceOrder.Orig_Cost__c = existingServiceOrder.Total_Service_Order_Cost__c;    
        newServiceOrder.Visible_To_Service_Providers__c = false;
        newServiceOrder.CSA_Acceptance_Date__c = null;
        newServiceOrder.CSA_Accepted_On_Recall__c = null;
        newServiceOrder.CSA_Bulk_Approved__c = false;
        newServiceOrder.CSA_Status__c = null;
        newServiceOrder.CSA_Status_Recall__c = null;
        newServiceOrder.Approver_User_ID__c = null;
        newServiceOrder.Approver_User_ID_Recall__c = null;
        newServiceOrder.Approver_User_Name__c = null;
        newServiceOrder.Approver_User_Name_Recall__c = null;
        newServiceOrder.Approved_Adhoc_Service_Cost__c = null;
        newServiceOrder.Approved_Scheduled_Service_Cost__c = null;
        newServiceOrder.Date_Approved__c = null;
        newServiceOrder.Status__c = APS_PicklistValues.RCRContract_Status_New;
        newServiceOrder.Recommendation__c = null;
        newServiceOrder.Recommendation_Reason__c = null;
        newServiceOrder.Panel_Date__c = null;
        newServiceOrder.Is_Cloning__c = true;
        newServiceOrder.Schedule__c = null;
        newServiceOrder.Allocation_Total__c = 0.0;
        newServiceOrder.Allocation_Total_Approved__c = 0.0;
        newServiceOrder.Allocation_Total_Not_Approved__c = 0.0;
        newServiceOrder.Allocation_Total_Not_Approved_Once_Off__c = 0.0;
        newServiceOrder.Allocation_Total_Recurrent__c = 0.0;
        newServiceOrder.Emergency_Respite__c = false;
        newServiceOrder.New_Service_Rate_Variation_Required__c = false;
        if(Mode == RunMode.RolloverApproved || Mode == RunMode.RequestApproved)
        {       
            NewEndDate = existingServiceOrder.End_Date__c;
        }   
        newServiceOrder.Original_End_Date__c = NewEndDate;

//        if(existingServiceOrder.RCR_CBMS_Contract__c == null)
//        {
//            newServiceOrder.CBMS_Region__c = existingServiceOrder.CBMS_Region__c;
//            newServiceOrder.CBMS_Team__c = existingServiceOrder.CBMS_Team__c;
//            newServiceOrder.CBMS_Service_Coordinator__c = existingServiceOrder.CBMS_Service_Coordinator__c;
//            newServiceOrder.CBMS_Service_Coordinator_Phone__c = existingServiceOrder.CBMS_Service_Coordinator_Phone__c;
//        }
        if(Mode == RunMode.RolloverApproved)
        {       
            newServiceOrder.Rollover_Status__c = null;
            newServiceOrder.Rollover_Rejection_Reason__c = null;
            newServiceOrder.RecordTypeId = A2HCUtilities.getRecordType('RCR_Contract__c', APSRecordTypes.RCRServiceOrder_ServiceOrder).ID;
        }   
        else if(Mode == RunMode.RequestApproved)
        {
            newServiceOrder.RecordTypeId = A2HCUtilities.getRecordType('RCR_Contract__c', APSRecordTypes.RCRServiceOrder_ServiceOrder).ID;                          
            newServiceOrder.OwnerId = existingServiceOrder.OwnerId;
            newServiceOrder.Status__c = APS_PicklistValues.RCRContract_Status_Approved;
            newServiceOrder.CSA_Acceptance_Date__c = Date.today();
            newServiceOrder.CSA_Status__c = APS_PicklistValues.RCRContractCSAStatus_Accepted; //APS_PicklistValues.RCRContractCSAStatus_Pending_Acceptance;
            newServiceOrder.Visible_To_Service_Providers__c = true;
            newServiceOrder.Date_Approved__c = Datetime.now();
            
            // if the new request had a plan action then copy that to the new service order
            newServiceOrder.Plan_Action__c = existingServiceOrder.Plan_Action__c;
        }
        else if(Mode == RunMode.Rollover)
        {
            newServiceOrder.Rollover_Status__c = APS_PicklistValues.RCRContract_RolloverStatus_PendingBrokerageReview;  
            newServiceOrder.Funding_Source__c = APS_PicklistValues.RCRContract_FundingSource_UseOfAllocation;
            newServiceOrder.New_Request_Type__c = APS_PicklistValues.RCRContract_NewRequestType_Rollover;
//            newServiceOrder.RCR_CBMS_Contract__c = null;
            // Default value for newServiceOrder.Type__c
            if(newServiceOrder.Type__c == null)
            {
                if(existingServiceOrder.Is_IF__c)
                {
                    newServiceOrder.Type__c = APS_PicklistValues.RCRContract_Type_IndividualisedFunding;
                }
                else
                {
                    newServiceOrder.Type__c = APS_PicklistValues.RCRContract_Type_Brokerage; 
                }
            }
            if(NewEndDate != existingServiceOrder.End_Date__c)
            {
                newServiceOrder.RecordTypeId = A2HCUtilities.getRecordType('RCR_Contract__c', APSRecordTypes.RCRServiceOrder_Rollover).ID;
                newServiceOrder.Start_Date__c = existingServiceOrder.End_Date__c.addDays(1);
            }     
            for(RCR_Contract__c otherSo : [SELECT   Start_Date__c,
                                                    End_Date__c 
                                            FROM    RCR_Contract__c
                                            WHERE   Name = :newServiceOrder.Name AND
                                                    RecordType.Name != :APSRecordTypes.RCRServiceOrder_Amendment AND
                                                    RecordType.Name != :APSRecordTypes.RCRServiceOrder_NewRequest])
            {                           
                if(A2HCUtilities.dateRangesOverlap(otherSo.Start_Date__c, otherSo.End_Date__c, newServiceOrder.Start_Date__c, newServiceOrder.Original_End_Date__c))
                {
                    addBatchLog(existingServiceOrder, batchLogs, batch, null, 'Rollover Request could not be created as it overlaps an existing Service Order or Rollover Request.'); 
                    return null;
                }   
            }   
            existingServiceOrder.Rollover_Status__c = null;
        }
        insert newServiceOrder; 
        // sharing recalculation seems to be too slow, so create a share record so current user can access the new record
        SharingRule.createServiceOrderSharing(newServiceOrder, SharingRule.AccessLevel.Edit, UserInfo.getUserID());
        SharingRule.createServiceOrderSharing(existingServiceOrder, SharingRule.AccessLevel.Edit, UserInfo.getUserID());

        // Update the original Service Order with the Id of the new Service Order.
        if(existingServiceOrder.ID != null &&
                existingServiceOrder.RecordTypeId != A2HCUtilities.getRecordType('RCR_Contract__c', APSRecordTypes.RCRServiceOrder_Amendment).ID)
        {
            existingServiceOrder.Destination_Record__c = newServiceOrder.Id;
            
            // if existing service order had a plan action then remove it
            existingServiceOrder.Plan_Action__c = null;
            
            update existingServiceOrder;
        }
        
        cloneBreakPeriods(existingServiceOrder, 
                                newServiceOrder, 
                                existingServiceOrder.Start_Date__c.daysBetween(newServiceOrder.Start_Date__c)); 
        
        /* commented out ME 9/10/2014 - funding details are handled by a trigger now
        if(Mode == RunMode.RequestApproved || Mode == RunMode.RolloverApproved)     
        {                       
            cloneFundingDetails(existingServiceOrder, 
                                    newServiceOrder);    
        }*/
        addBatchLog(existingServiceOrder, batchLogs, batch, 'Rollover Request created, Rollover Status: ' + newServiceOrder.Rollover_Status__c, null); 
        
        newServiceOrder = getServiceOrder(newServiceOrder.ID);
        
        if(Mode == RunMode.Rollover)
        {
            List<RCR_Funding_Detail__c> newFundingDetails = new List<RCR_Funding_Detail__c>{};
//            if(existingServiceOrder.RCR_CBMS_Contract__c != null)
//            {
//                RCR_Funding_Detail__c fd = new RCR_Funding_Detail__c(RCR_Service_Order__c = newServiceOrder.ID);
//                fd.Cost_Centre__c = RCR_Cost_Centre_Code__c.getInstance('ROLLOVER').Code__c;
//                fd.Start_Date__c = newServiceOrder.Start_Date__c;
//                fd.Activity_Code_Based_On_Client_Details__c = true;
//                fd.Maximum_Amount__c = 0.0; // service order has no cost yet as no services created
//                newFundingDetails.add(fd);
//            }
//            else
//            {
                Map<String, decimal> fundingAmountByActivityCode = new Map<String, decimal>();
                for(RCR_Funding_Detail__c oldFd : existingServiceOrder.RCR_Funding_Details__r)
                {
                    if(!fundingAmountByActivityCode.containsKey(oldFd.Activity_Code__c))
                    {
                        fundingAmountByActivityCode.put(oldFd.Activity_Code__c, 0.0);
                    }
                    decimal existingAmount = fundingAmountByActivityCode.get(oldFd.Activity_Code__c);
                    fundingAmountByActivityCode.put(oldFd.Activity_Code__c, (existingAmount += oldFd.Maximum_Amount__c));
                }
                for(RCR_Funding_Detail__c oldFd : existingServiceOrder.RCR_Funding_Details__r)
                {
                    if(!fundingAmountByActivityCode.containsKey(oldFd.Activity_Code__c))
                    {
                        continue;
                    }
                    RCR_Funding_Detail__c fd = new RCR_Funding_Detail__c(RCR_Service_Order__c = newServiceOrder.ID);
                    fd.Cost_Centre__c = RCR_Cost_Centre_Code__c.getInstance('ROLLOVER').Code__c;
                    fd.Start_Date__c = newServiceOrder.Start_Date__c;
                    fd.Activity_Code_Based_On_Client_Details__c = oldFd.Activity_Code_Based_On_Client_Details__c;
                    fd.Activity_Code__c = oldFd.Activity_Code__c;
                    fd.Maximum_Amount__c = 0.0; // service order has no cost yet as no services created
                    newFundingDetails.add(fd);
                    
                    fundingAmountByActivityCode.remove(oldFd.Activity_Code__c);
//                }
            }
            insert newFundingDetails;
        }
        return newServiceOrder;
    }
    
    @TestVisible
    protected void cloneServiceTypes(List<RCR_Contract_Service_Type__c> serviceTypes, Id parentID, String parentType)
    {
        List<RCR_Contract_Service_Type__c> newServiceTypes = new List<RCR_Contract_Service_Type__c>();
        for(RCR_Contract_Service_Type__c st : serviceTypes)
        {
            RCR_Contract_Service_Type__c newServiceType = st.clone(false, true);
            if(parentType == 'AdHoc')
            {
                newServiceType.RCR_Contract_Adhoc_Service__c = parentID;
            }
            else
            {
                newServiceType.RCR_Contract_Scheduled_Service__c = parentID;
            }
            newServiceTypes.add(newServiceType);
        }
        insert newServiceTypes;
    }
    
    @TestVisible
    protected void finaliseBatch(Database.BatchableContext info, String emailTemplateName)
    {       
        List<RCR_Contract__c> serviceOrders = [SELECT   ID,
                                                        Rollover_Status__c,
                                                        Destination_Record__c,
                                                        LP_Contract_Key__c,
                                                        Account__r.LP_Biller_Key__c,
                                                        Account__r.Lantern_Pay_Active__c
                                                FROM    RCR_Contract__c
                                                WHERE   ID IN :ServiceOrderIds]; 
        List<RCR_Contract__c> newServiceOrders = new List<RCR_Contract__c>();
        List<RCR_Contract__c> newLPServiceOrders = new List<RCR_Contract__c>();
        for(RCR_Contract__c so : serviceOrders)
        {
            so.Schedule__c = null;
            so.Is_Cloning__c = false;
            if(Mode == RunMode.RolloverApproved)
            {
                if(so.Rollover_Status__c != APS_PicklistValues.RCRContract_RolloverStatus_BulkApproved)
                {
                    so.Rollover_Status__c = APS_PicklistValues.RCRContract_RolloverStatus_Complete;
                }
            }
            else if(Mode == RunMode.Rollover  || Mode == RunMode.RequestApproved)
            {
                if(so.Destination_Record__c != null)
                {
                    newServiceOrders.add(new RCR_Contract__c(ID = so.Destination_Record__c, 
                                                            Schedule__c = null));
                }
                if(so.Account__r.LP_Biller_Key__c != null &&
                        so.Account__r.Lantern_Pay_Active__c)
                {
                    newLPServiceOrders.add(so);
                }
            }
        }
        DMLAsSystem.updateRecords(serviceOrders);
        DMLAsSystem.updateRecords(newServiceOrders);
        if(Mode == RunMode.RequestApproved && !newLPServiceOrders.isEmpty())
        {
            LPAddContractQueueable lpQueuable = new LPAddContractQueueable(newServiceOrders);
            System.enqueueJob(lpQueuable);
        }
        
        // delete ervice orders that were created with errors
        if(Mode == RunMode.Rollover)
        {
            Set<ID> serviceOrderErrorIDs = new Set<ID>();
            for(Batch_Log__c b : [SELECT    Record_Id__c,
                                            Error__c
                                    FROM    Batch_Log__c
                                    WHERE   Name = :info.getJobId()])
            {
                // can't filter on long text fields so have to get them all
                if(String.isNotBlank(b.Error__c))
                {
                    serviceOrderErrorIDs.add(ID.valueOf(b.Record_Id__c));
                }
            }   
            try
            {                                   
                delete [SELECT  ID
                        FROM    RCR_Contract__c
                        WHERE   Source_Record__c IN :serviceOrderErrorIDs AND
                                RecordType.Name = :APSRecordTypes.RCRServiceOrder_Rollover];
            }
            catch(Exception ex)
            // ignore any problems here
            {}
        }
        sendJobCompletedEmails(info.getJobId(), emailTemplateName);
    }
    
    @TestVisible
    protected void reparentBatchLogs(Database.BatchableContext batch)
    {
        ApexJobId = batch.getJobID();
        List<Batch_Log__c> batchlogs = [SELECT  ID
                                        FROM    Batch_Log__c
                                        WHERE   Name = :prevJobId];
        for(Batch_Log__c b : batchlogs) 
        {
            b.Name = batch.getJobID();
        }   
        update batchlogs;                   
    }
    
    @TestVisible
    protected void setServiceDates(RCR_Contract__c oldServiceOrder, RCR_Contract__c newServiceOrder, sObject oldService, sObject newService)
    {
        Date oldServiceStartDate = (Date)(oldService.get('Start_Date__c'));
        Date oldServiceEndDate = (Date)(oldService.get('End_Date__c'));
        Date newEndDate;
        Date newStartDate;
        if(oldServiceOrder.Start_Date__c == oldServiceStartDate &&
                oldServiceOrder.End_Date__c == oldServiceEndDate)
        {
            newStartDate = newServiceOrder.Start_Date__c;
            newEndDate = newServiceOrder.End_Date__c;           
        }               
        else
        {       
            setRolloverComment(newService, 'Dates and quantities may not match the original.');
            Integer serviceLength = oldServiceStartDate.daysBetween(oldServiceEndDate); 
            Integer startDateIncrement = oldServiceOrder.Start_Date__c.daysBetween(oldServiceStartDate);
            newStartDate = newServiceOrder.Start_Date__c.addDays(startDateIncrement);                   
            newEndDate = newStartDate.addDays(serviceLength);
        
            if(newStartDate < newServiceOrder.Start_Date__c)
            {
                newStartDate = newServiceOrder.Start_Date__c;       
                setRolloverComment(newService, 'Start Date has been adjusted to fall within the Service Order.');
            }   
            if(newEndDate > newServiceOrder.End_Date__c)
            {
                newEndDate = newServiceOrder.End_Date__c;           
                setRolloverComment(newService, 'End Date has been adjusted to fall within the Service Order.');
            }
        }
        newService.put('Start_Date__c', newStartDate);
        newService.put('Original_End_Date__c', newEndDate);     
    }
    
    @TestVisible
    protected void setRolloverComment(sObject newService, String msg)
    {
        if(mode != RunMode.Rollover)
        {
            return;
        }
        String comment = (String)newService.get('Rollover_Comment__c');
        comment = comment == null ? msg : (comment += '\n' + msg);          
        newService.put('Rollover_Comment__c', comment);
    }
    
    @TestVisible
    protected void handleError(RCR_Contract__c existingServiceOrder, 
                                        RCR_Contract__c newServiceOrder,
                                        System.savePoint sp,
                                        List<Batch_Log__c> batchLogs, 
                                        Database.BatchableContext batch,
                                        Exception ex,
                                        String messagePrefix)
    {
        if(sp != null)
        {
            Database.rollback(sp);
        }
        if(ex != null)
        {
            addBatchLog(existingServiceOrder, batchLogs, batch, null, messagePrefix + ': ' + A2HCException.getExceptionMessage(ex, 255));
        }
    }
    
    @TestVisible
    protected void addBatchLog(RCR_Contract__c existingServiceOrder, 
                                        List<Batch_Log__c> batchLogs, 
                                        Database.BatchableContext batch,
                                        String msg,
                                        String errMsg)
    {
        if(batch != null)
        {
            errMsg = (errMsg != null && errMsg.length() > 255) ? errMsg.substring(0, 255) : errMsg;
            batchLogs.add(new Batch_Log__c(Name = batch.getJobID(), 
                                        Record_Id__c = (existingServiceOrder == null ? '': existingServiceOrder.Id), 
                                        Record_Name__c = (existingServiceOrder == null ? '': existingServiceOrder.Name),
                                        Message__c = msg,
                                        Error__c = errMsg));
        }
    }
    
    @TestVisible
    private void cloneFundingDetails(RCR_Contract__c oldServiceOrder, 
                                                RCR_Contract__c newServiceOrder)
    {
        List<RCR_Funding_Detail__c> fundingdetails = new List<RCR_Funding_Detail__c>();
        for(RCR_Funding_Detail__c fd : oldServiceOrder.RCR_Funding_Details__r)
        {
            RCR_Funding_Detail__c fundingdetail = fd.clone(false, true);
            fundingdetail.RCR_Service_Order__c = newServiceOrder.ID;
            fundingdetails.add(fundingdetail);
        }
        insert fundingdetails;
    }   

    @TestVisible
    private void cloneBreakPeriods(RCR_Contract__c oldServiceOrder, 
                                                RCR_Contract__c newServiceOrder, 
                                                Integer rolloverIncrement)
    {
        List<RCR_Service_Order_Break_Period__c> breakPeriods = new List<RCR_Service_Order_Break_Period__c>();
        for(RCR_Service_Order_Break_Period__c bp : oldServiceOrder.RCR_Service_Order_Break_Periods__r)
        {
            RCR_Service_Order_Break_Period__c breakPeriod = bp.clone(false, true);
            breakPeriod.Start_Date__c = bp.Start_Date__c.addDays(rolloverIncrement);
            breakPeriod.End_Date__c = bp.End_Date__c.addDays(rolloverIncrement);
            breakPeriod.RCR_Service_Order__c = newServiceOrder.ID;
            breakPeriods.add(breakPeriod);
        }
        insert breakPeriods;
    }   
    
    @TestVisible
    private void sendJobCompletedEmails(String rolloverBatchId, String templateName) 
    {
        if(templateName != null)
        {
            ID templateID = [SELECT t.ID
                                FROM    EmailTemplate t
                                WHERE   t.DeveloperName = :templateName].ID;
    
            for(ID userId : UserIds)
            {
                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                message.setTemplateId(templateID);
                message.setTargetObjectId(userId);
                message.setSaveAsActivity(false);
                message.setWhatId(rolloverBatchId);
                message.setSenderDisplayName(templateName.replace('_', ' '));
                message.setReplyTo('noreply@eReferrals.dfc.sa.gov.au');
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { message }, false);
            }    
        }
        delete [SELECT  ID
                FROM    Batch_Log__c
                WHERE   Name = :rolloverBatchId];         
    }
}