@isTest
private class TestRCRRolloverEditExtension
{
    public static TestMethod void myTestMethod() 
    {   
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        RCR_Contract__c c = [SELECT Id,
                                    Client__c,
                                    Start_Date__c,
                                    Original_End_Date__c,
                                    Level__c,
                                    Type__c,
                                    Funding_Source__c,
                                    Service_Coordinator__c
                             FROM RCR_Contract__c
                             LIMIT 1];
    
        RCRRolloverEditExtension r = new RCRRolloverEditExtension(new ApexPages.StandardController(c));             
        //system.debug(r.RolloverAllocations);
        r.addRolloverAllocation();
        r.deleteAllocation();
        r.calcRolloverAllocationFYEAmount();
        r.saveRollover();
        r.saveAndContinue();
                
        Test.StopTest();
    }
}