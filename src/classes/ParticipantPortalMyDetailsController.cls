public with sharing class ParticipantPortalMyDetailsController extends A2HCPageBase
{
    public ParticipantPortalMyDetailsController()
    {}

    public ParticipantPortalMyDetailsController(ApexPages.StandardController c)
    {}

    public PageReference Redirect()
    {
        try
        {
            Id contactId;
            for (User u : [SELECT Id,
                                  ContactId
                           FROM User
                           WHERE Id = :UserInfo.getUserId()])
            {
                contactId = u.ContactId;
            }                           
                                
            for (Referred_Client__c c : [SELECT Id                                                
                                         FROM Referred_Client__c
                                         WHERE Participant_Portal_Contact__c = :contactId
                                         LIMIT 1])
            {
                PageReference ref = Page.ParticipantPortalMyDetails;
                ref.getParameters().put('id', c.Id);
                
                return ref;
            }                                         
                    
            return A2HCException.formatException('No participant record found');                    
        }
        catch(Exception e)
        {
            return A2HCException.formatException(e);
        }                                               
    }

    public Referred_Client__c ThisClient
    {
        get
        {
            if(ThisClient == null)
            {
                ThisClient = [SELECT ID,
                                    Name,
                                    Current_Activity_Status__c
                            FROM    Referred_Client__c
                            WHERE   Participant_Portal_Contact__c = :CurrentUser.ContactID
                            LIMIT 1];
            }
            return ThisClient;
        }
        private set;
    }
}