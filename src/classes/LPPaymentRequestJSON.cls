public class LPPaymentRequestJSON
{
    public String id;	//fd3b8c72-b0b6-4c5c-bd69-10ee3ca03f2f
    public Integer created;	//1518405650
    public cls_data data;
    public String type;	//claiming.invoice.processed
    public class cls_data
    {
        public String paymentUri;	//https://api.sandbox.lanternpay.com/payments?directEntryFileUKey=468aff68-4f00-481f-9f01-8352414f44ab
    }

    public static LPPaymentRequestJSON parse(String json){
        return (LPPaymentRequestJSON) System.JSON.deserialize(json, LPPaymentRequestJSON.class);
    }
}