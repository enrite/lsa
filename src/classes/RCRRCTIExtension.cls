public with sharing class RCRRCTIExtension {

    public RCRRCTIExtension(ApexPages.StandardController controller)
    {
        // can't group by status, so need four queries rather than two
        AggregateResult ar = [SELECT    SUM(Total_GST__c)  GST,
                                                SUM(Total__c)  tot,
                                                SUM(Total_ex_GST__c)  totExGST
                                        FROM    RCR_Invoice_Item__c
                                        WHERE   RCR_Invoice__c = :(controller.getID()) AND
                                                Status__c = :APS_PicklistValues.RCRInvoiceItem_Status_Approved AND
                                                Total_GST__c <> 0];
        gstVal = (decimal)ar.get('GST');
        totalExGSTTaxableVal = (decimal)ar.get('totExGST');
        TotalTaxableVal = (decimal)ar.get('tot');

        ar = [SELECT    SUM(Total_GST__c)  GST,
                        SUM(Total__c)  tot,
                        SUM(Total_ex_GST__c)  totExGST
                FROM    RCR_Invoice_Item__c
                WHERE   RCR_Invoice__c = :(controller.getID()) AND
                        Status__c = :APS_PicklistValues.RCRInvoiceItem_Status_Rejected AND
                        Total_GST__c <> 0];
        failedGSTVal = (decimal)ar.get('GST');
        totalFailedExGSTTaxableVal = (decimal)ar.get('totExGST');
        TotalFailedTaxableVal = (decimal)ar.get('tot');

        ar = [SELECT    SUM(Total__c)  tot
                FROM    RCR_Invoice_Item__c
                WHERE   RCR_Invoice__c = :(controller.getID()) AND
                        Total_GST__c = 0 AND
                        Status__c = :APS_PicklistValues.RCRInvoiceItem_Status_Approved];
        totalNonTaxableVal = (decimal)ar.get('tot');

        ar = [SELECT    SUM(Total__c)  tot
                FROM    RCR_Invoice_Item__c
                WHERE   RCR_Invoice__c = :(controller.getID()) AND
                        Total_GST__c = 0 AND
                        Status__c = :APS_PicklistValues.RCRInvoiceItem_Status_Rejected];
        TotalFailedNonTaxableVal = (decimal)ar.get('tot');
    }
    
    private decimal totalExGSTTaxableVal
    {
        get
        {
            if(totalExGSTTaxableVal == null)
            {
                totalExGSTTaxableVal = 0.0;
            }
            return totalExGSTTaxableVal;
        }
        set; 
    }
    private decimal gstVal
    {
        get
        {
            if(gstVal == null)
            {
                gstVal = 0.0;
            }
            return gstVal;
        }
        set; 
    }
    private decimal failedGSTVal
    {
        get
        {
            if(failedGSTVal == null)
            {
                failedGSTVal = 0.0;
            }
            return failedGSTVal;
        }
        set; 
    }
    
    private decimal totalFailedExGSTTaxableVal
    {
        get
        {
            if(totalFailedExGSTTaxableVal == null)
            {
                totalFailedExGSTTaxableVal = 0.0;
            }
            return totalFailedExGSTTaxableVal;
        }
        set; 
    }
    
    public decimal TotalNonTaxableVal
    {
        get
        {
            if(TotalNonTaxableVal == null)
            {
                TotalNonTaxableVal = 0.0;
            }
            return TotalNonTaxableVal;
        }
        private set; 
    }
    
    public decimal TotalFailedNonTaxableVal
    {
        get
        {
            if(TotalFailedNonTaxableVal == null)
            {
                TotalFailedNonTaxableVal = 0.0;
            }
            return TotalFailedNonTaxableVal;
        }
        private set; 
    }
    
    public decimal TotalTaxableVal
    {
        get
        {
            if(TotalTaxableVal == null)
            {
                TotalTaxableVal = 0.0;
            }
            return TotalTaxableVal;
        }
        private set; 
    }
    
    public decimal TotalFailedTaxableVal
    {
        get
        {
            if(TotalFailedTaxableVal == null)
            {
                TotalFailedTaxableVal = 0.0;
            }
            return TotalFailedTaxableVal;
        }
        private set; 
    }
    
    public String TotalExGSTTaxable
    {
        get
        {
            return A2HCUtilities.decimalToCurrency(totalExGSTTaxableVal);
        }
    }
    public String GST
    {
        get
        {
            return A2HCUtilities.decimalToCurrency(gstVal);
        }
    }
    public String TotalTaxable
    {
        get
        {
            return A2HCUtilities.decimalToCurrency(TotalTaxableVal);
        }
    }

    public String TotalNonTaxable
    {
        get
        {
            return A2HCUtilities.decimalToCurrency(totalNonTaxableVal);
        }
    }

    public String TotalFailedTaxable 
    {
        get
        {
            return A2HCUtilities.decimalToCurrency(TotalFailedTaxableVal);
        }
    }

    public String FailedGST
    {
        get
        {
            return A2HCUtilities.decimalToCurrency(failedGSTVal);
        }
    }

    public String TotalFailedExGSTTaxable
    {
        get
        {
            return A2HCUtilities.decimalToCurrency(totalFailedExGSTTaxableVal);
        }
    }

    public String TotalFailedNonTaxable
    {
        get
        {
            return A2HCUtilities.decimalToCurrency(TotalFailedNonTaxableVal);
        }
    }

    public String TotalPayable
    {
        get
        {
            return A2HCUtilities.decimalToCurrency(TotalTaxableVal + totalNonTaxableVal);
        }
    }
    
    public String TotalRejected
    {
        get
        {
            return A2HCUtilities.decimalToCurrency(TotalFailedTaxableVal + TotalFailedNonTaxableVal);
        }
    }

    public String RecipientName
    {
        get
        {
            return Settings.get('Recipient_Name');
        }
        private set;
    }

    public String RecipientUnit
    {
        get
        {
            return Settings.get('Recipient_Unit');
        }
        private set;
    }

    public String RecipientAddress1
    {
        get
        {
            return Settings.get('Recipient_Address1');
        }
        private set;
    }

    public String RecipientAddress2
    {
        get
        {
            return Settings.get('Recipient_Address2');
        }
        private set;
    }

    public String RecipientAddress3
    {
        get
        {
            return Settings.get('Recipient_Address3');
        }
        private set;
    }

    public String RecipientABN
    {
        get
        {
            return Settings.get('Recipient_ABN');
        }
        private set;
    }

    private Map<String, String> Settings
    {
        get
        {
            if(Settings == null)
            {
                Settings = new Map<String, String>();

                for(RCR_RCTI_Settings__c description : [SELECT  Name,
                                                Description__c
                                        FROM    RCR_RCTI_Settings__c])
                {
                    Settings.put(description.Name, description.Description__c);
                }
            }
            return Settings;
        }
        private set;
    }

    @IsTest//(SeeAllData=true)
    static void test1()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        RCR_Invoice__c invoice = [SELECT Id FROM RCR_Invoice__c LIMIT 1];
        RCRRCTIExtension ext = new RCRRCTIExtension(new Apexpages.Standardcontroller(invoice));

        system.debug(ext.TotalExGSTTaxable);
        system.debug(ext.GST);
        system.debug(ext.TotalTaxable);
        system.debug(ext.TotalNonTaxable);
        system.debug(ext.TotalFailedTaxable);
        system.debug(ext.FailedGST);
        system.debug(ext.TotalFailedexGSTTaxable);
        system.debug(ext.TotalFailedNonTaxable);

        system.debug(ext.RecipientName);
        system.debug(ext.RecipientUnit);
        system.debug(ext.RecipientAddress1);
        system.debug(ext.RecipientAddress2);
        system.debug(ext.RecipientAddress3);
        
        Test.StopTest();
    }
}