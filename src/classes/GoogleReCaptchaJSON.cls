public class GooglereCaptchaJSON
{
    public Boolean success;

    public static GooglereCaptchaJSON parse(String jsonStr)
    {
        return (GooglereCaptchaJSON) JSON.deserialize(jsonStr, GooglereCaptchaJSON.class);
    }
}