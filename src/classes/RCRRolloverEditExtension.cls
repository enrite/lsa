public with sharing class RCRRolloverEditExtension
{
	public RCRRolloverEditExtension(ApexPages.StandardController ctrlr)
    {
    	if(!Test.isRunningTest())
    	{
    		ctrlr.addFields(new String[]{'End_Date__c'});
    	}
    	rollover = (RCR_Contract__c)ctrlr.getRecord();
    	controller = ctrlr;
    }
    
    private RCR_Contract__c rollover;
    private ApexPages.StandardController controller;
    
//    public List<IF_Personal_Budget__c> RolloverAllocations
//    {
//    	get
//    	{
//    		if(RolloverAllocations == null)
//    		{
//    			RolloverAllocations = [SELECT   ID,
//		                                        Name,
//		                                        Amount__c,
//		                                        FYE_Amount__c,
//		                                        Allocation_Source__c,
//		                                        Allocation_Type__c,
//		                                        Status__c,
//		                                        Reason__c,
//		                                        Region__c,
//		                                        Service_Type__c,
//		                                        RCR_Service_Order__c,
//		                                        Client__c,
//		                                        Financial_Year__c,
//		                                        Financial_Year__r.Name
//		                                FROM    IF_Personal_Budget__c
//		                                WHERE   RCR_Service_Order__c = :rollover.ID AND
//		                                        RCR_Service_Order__c != null
//		                                ORDER BY Financial_Year__r.Name];
//    		}
//    		return RolloverAllocations;
//    	}
//    	set;
//    }
    
    public void deleteAllocation()
    {
//    	try
//	 	{
//	 		Integer rowIndex = A2HCUtilities.tryParseInteger(getParameter('rowIndex'));
//			deleteChildRecord(RolloverAllocations[rowIndex].ID, true);
//         	RolloverAllocations.remove(rowIndex);
//	 	}
//	 	catch(Exception ex)
//	 	{
//	 		A2HCException.formatException(ex);
//	 	}
    }
    
    public void addRolloverAllocation()
	{
//	 	try
//	 	{
//	 		 IF_Personal_Budget__c alloc = new IF_Personal_Budget__c(RCR_Service_Order__c = rollover.ID,
//                                                    Status__c = APS_PicklistValues.IFAllocation_Status_New,
//                                                    Client__c = rollover.Client__c,
//                                                    Reason__c = APS_PicklistValues.IFAllocation_Reason_RolloverTopUp);
//             RolloverAllocations.add(alloc);
//	 	}
//	 	catch(Exception ex)
//	 	{
//	 		A2HCException.formatException(ex);
//	 	}
	}
	
	public void calcRolloverAllocationFYEAmount()
    {
//    	try
//    	{
//    		throwTestError();
//    		RCRServiceOrderRequestController.calcFYEAmountForAllocations(rollover,
//    																	RolloverAllocations[A2HCUtilities.tryParseInteger(getParameter('rowIndex'))]);
//    	}
//        catch (Exception ex)
//        {
//            A2HCException.formatException(ex);
//        }
    }
	
	public PageReference saveRollover()
	{
		 if(!save())
		 {
		 	return null;
		 }
 		 return new PageReference('/' + rollover.ID);
	}
	
	public PageReference saveAndContinue()
	{
 		 save();
 		 return null;
	}
	
	private Boolean save()
	{
		Savepoint sp = Database.setSavepoint();
	 	try
	 	{
	 		 Boolean valid = isRequiredFieldValid(rollover.Start_Date__c, 'Start Date', true);
	 		 valid = isRequiredFieldValid(rollover.Original_End_Date__c, 'End Date', valid);
	 		 valid = isRequiredFieldValid(rollover.Level__c, 'Level', valid);
	 		 valid = isRequiredFieldValid(rollover.Type__c, 'Type', valid);
	 		 valid = isRequiredFieldValid(rollover.Funding_Source__c, 'Funding Source', valid);
	 		 valid = isRequiredFieldValid(rollover.Service_Coordinator__c, 'Service Coordinator', valid);
//	 		 for(IF_Personal_Budget__c alloc : RolloverAllocations)
//	 		 {
//	 		 	valid = isRequiredFieldValid(alloc.Allocation_Type__c, 'Allocation Type', valid);
//	 		 	valid = isRequiredFieldValid(alloc.Allocation_Source__c, 'Allocation Source', valid);
//	 		 	valid = isRequiredFieldValid(alloc.Service_Type__c, 'Allocation Service Type', valid);
//	 		 	valid = isRequiredFieldValid(alloc.Amount__c, 'Allocation Amount', valid);
//	 		 	valid = isRequiredFieldValid(alloc.FYE_Amount__c, 'Allocation FYE Amount', valid);
//	 		 }
			 if(!valid)
			 {
			 	return false;
			 }
	 		 RCRServiceOrderRequestController.setManagersAndDirector(rollover);
	 		 upsert rollover;
	 		 rollover = RCRContractExtension.getContract(rollover.ID);
//	 		 for(IF_Personal_Budget__c alloc : RolloverAllocations)
//	 		 {
//	 		 	alloc.RCR_Service_Order__c = rollover.ID;
//	 		 }
//	 		 TriggerBypass.IFAllocation = true;
//	 		 upsert RolloverAllocations;
	 		 return true;
	 	}
	 	catch(Exception ex)
	 	{
	 		A2HCException.formatExceptionAndRollback(sp, ex);
	 		return false;
	 	}	
	}
	
	
}