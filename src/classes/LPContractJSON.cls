public class LPContractJSON
{
    public String contractNumber;
    public String programCode;
    public String billerKey;
    public String startDate;
    public String endDate;
    //public String budget;
    public Preauthorizations[] preauthorizations;

    public class Preauthorizations
    {
        public String contractLineNumber;
        public String preauthorizationNumber;
        public String itemCode;
        public String description;
        public Decimal unitPrice;
        public String quantity;
        public String hours;
        public String total;
        public String budget;
    }

    public String getJSON(RCR_Contract__c contract, Map<ID, RCR_Service__c> servicesByID, Lantern_Pay__c settings, Boolean isAmendment)
    {
        contractNumber = contract.Name;
        programCode = settings.Program_Code__c;
        billerKey = contract.Account__r.LP_Biller_Key__c;
        startDate = LPRESTInterface.getDateString(contract.Start_Date__c);
        endDate = LPRESTInterface.getDateString(contract.End_Date__c);

        preauthorizations = new List<LPContractJSON.Preauthorizations>();
        for(RCR_Contract_Scheduled_Service__c css : contract.RCR_Contract_Scheduled_Services__r)
        {
            LPContractJSON.Preauthorizations preAuth = new LPContractJSON.Preauthorizations();
            preAuth.contractLineNumber = css.Name;
            preAuth.preauthorizationNumber = css.Name;
            preAuth.itemCode = css.Standard_Service_Code__c;
            preAuth.description = css.Standard_Service_Code_Description__c;

            if(css.Use_Negotiated_Rates__c)
            {
                preAuth.unitPrice = css.Negotiated_Rate_Standard__c;
            }
            else
            {
                preAuth.unitPrice = null;
                RCR_Service__c service = servicesByID.get(css.RCR_Service__c);
                if(service != null)
                {
                    for(RCR_Service_Rate__c rate : service.RCR_Service_Rates__r)
                    {
                        if(A2HCUtilities.dateRangesOverlap(css.Start_Date__c, css.End_Date__c, rate.Start_Date__c, rate.End_Date__c))
                        {
                            preAuth.unitPrice = rate.Rate__c;
                            break;
                        }
                    }
                }
            }

            Decimal qty = 0;
//            Decimal qtyUsed = css.Quantity_Used__c == null ? 0.0 : css.Quantity_Used__c;
//            if(qtyUsed < css.Total_Quantity__c)
//            {
//                qty = css.Total_Quantity__c - qtyUsed;
//            }
//            // allow for contracts where quantity used may be greater than total quantity due to billing method
//            else
            if(css.Remaining__c > 0 && preAuth.unitPrice > 0)
            {
                qty = css.Remaining__c / preAuth.unitPrice;
            }
            preAuth.quantity = LPRESTInterface.getFormattedDecimal(qty);
            preauthorizations.add(preAuth);
        }
        return JSON.serialize(this);
    }
}