public without sharing class LPRejectBatch extends LPAuthoriseRejectInvoiceBatchBase
{
    public LPRejectBatch(Set<ID> lpItemsForRejection)
    {
        super(lpItemsForRejection, LPRESTInterface.LPINVOICE_APPROVAL_REJECT);
    }

    public LPRejectBatch(Set<ID> lpItemsForRejection, Set<ID> lpPendingItemsForApproval)
    {
        super(lpItemsForRejection, lpPendingItemsForApproval, LPRESTInterface.LPINVOICE_APPROVAL_REJECT);
    }
}