public class PermissionSetAuditBatch implements Database.Batchable<SObject>, Database.Stateful
{
	public PermissionSetAuditBatch(DateTime pLastRun) 
    {
		lastRun = pLastRun;
	}

    private DateTime lastRun;
    private ID permissionSetRtId = User_Audit__c.sObjectType.getDescribe().getRecordTypeInfosByName().get('Permission Set').getRecordTypeId();
	
	/**
	 * @description gets invoked when the batch job starts
	 * @param context contains the job ID
	 * @returns the record set as a QueryLocator object that will be batched for execution
	 */ 
	public Database.QueryLocator start(Database.BatchableContext context) 
    {
        return Database.getQueryLocator([SELECT ID,
                                                User__c,
                                                Permission_Set_ID__c,
                                                Permission_Set_Assignment_ID__c,
                                                Permission_Set_Name__c,
                                                Updated__c	
                                        FROM    User_Audit__c 
                                        WHERE   RecordTypeId = :permissionSetRtId AND
                                                Grant__c = 'Granted'
                                        ORDER BY Updated__c DESC]);
	}

	/**
	 * @description gets invoked when the batch job executes and operates on one batch of records. Contains or calls the main execution logic for the batch job.
	 * @param context contains the job ID
	 * @param scope contains the batch of records to process.
	 */ 
   	public void execute(Database.BatchableContext context, List<User_Audit__c> scope) 
    {
        List<User_Audit__c> newAudits = new List<User_Audit__c>();
	    for(User_Audit__c ua : scope)
        {
            // check if there is already a "removed" audit record for this permission set
            for(User_Audit__c removedUa : [SELECT   ID
                                            FROM    User_Audit__c 
                                            WHERE   RecordTypeId = :permissionSetRtId AND 
                                                    Permission_Set_ID__c = :ua.Permission_Set_ID__c AND
                                                    Grant__c = 'Removed' AND 
                                                    User__c = :ua.User__c AND 
                                                    Updated__c > :ua.Updated__c
                                            LIMIT 1])
            {
                continue;
            }
            ID permissionSetAssignmentID = Id.valueOf(ua.Permission_Set_Assignment_ID__c);
            if([SELECT COUNT()
                FROM    PermissionSetAssignment 
                WHERE   ID = :permissionSetAssignmentID] == 0)
            {
                User_Audit__c audit = new User_Audit__c(RecordTypeId = permissionSetRtId);
                audit.User__c = ua.User__c;
                audit.Permission_Set_Assignment_ID__c = ua.Permission_Set_Assignment_ID__c;
                audit.Permission_Set_ID__c = ua.Permission_Set_ID__c;
                audit.Permission_Set_Name__c = ua.Permission_Set_Name__c;
                audit.Grant__c = 'Removed';
                audit.Updated__c = lastRun;
                audit.Updated_On__c = String.valueOf(lastRun);
                newAudits.add(audit);
            }
        }
        insert newAudits;
	}
	
	/**
	 * @description gets invoked when the batch job finishes. Place any clean up code in this method.
	 * @param context contains the job ID
	 */ 
	public void finish(Database.BatchableContext context) {
		
	}
}