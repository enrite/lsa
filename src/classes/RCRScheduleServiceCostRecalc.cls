global class RCRScheduleServiceCostRecalc implements Schedulable
{
	global void execute(SchedulableContext SC) 
    {
        RCRBatchUpdateScheduledServiceTotalCost batch = new RCRBatchUpdateScheduledServiceTotalCost();  
        batch.ScheduleID = SC.getTriggerID();
        batch.runMode = RCRBatchUpdateScheduledServiceTotalCost.Mode.ServiceOrderRollover;
        ID batchprocessid = Database.executeBatch(batch, Batch_Size__c.getInstance('Rate_Recalculation').Records__c.IntValue());
    }
}