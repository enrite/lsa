public virtual class AttachmentUploadController extends A2HCPageBase
{
    public AttachmentUploadController()
    {
        refreshControlId = getParameter('cntrId');
    }
    
    public String getAccept()
    {
        return getParameter('acc');
    }

    public ID ParentId
    {
        get;
        set;
    }

    public String ReturnURL
    {
        get;
        set;
    }
    
    public Attachment attachment 
    {
        get 
        {
            if (attachment == null)
            {
                attachment = new Attachment();
            }
            return attachment;
        }
        set;
    }
    
    public String refreshControlId
    {
        get;
        set;
    }
 
    public virtual PageReference upload()
    {
        try 
        {
            attachment.OwnerId = UserInfo.getUserId();
            attachment.ParentId = ParentId == null ? getParameter('id') : ParentId;
            attachment.Description = getParameter('Q');
            attachment.IsPrivate = false;
            insert attachment;
            A2HCException.formatException(ApexPages.severity.INFO, 'Attachment uploaded successfully');

            if(String.isBlank(ReturnURL) || String.isBlank(ParentId))
            {
                return null;
            }
            PageReference pg = new PageReference(ReturnURL);
            pg.getParameters().put('id', ParentId); 
            return pg;
        } 
        catch (Exception e) 
        {
            return A2HCException.formatException(e);
        } 
        finally 
        {
            attachment = new Attachment(); 
        }
    }
}