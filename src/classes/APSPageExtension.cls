// only used to expose A2HCPageBase methods to pages using
// standard controllers
public with sharing class APSPageExtension
    extends A2HCPageBase
{
    public APSPageExtension(ApexPages.StandardController c)
    {}

    @IsTest//(SeeAllData=true)
    private static void test1()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        APSPageExtension ext = new APSPageExtension(new ApexPages.StandardController([SELECT ID FROM Account LIMIT 1]));        
        
        Test.StopTest();
    }
}