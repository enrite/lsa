@isTest
public class TestFormEditExtension
{                    
    static testMethod void testSINF()
    {
        TestLoadFormData data = new TestLoadFormData();
                                           
        Form__c f = new Form__c();
        f.Client__c = data.Client.Id;
    
        Test.setCurrentPage(Page.SevereInjuryNotificationEdit);
        ApexPages.CurrentPage().getParameters().put('RecordType', data.RecordTypes.get('Severe_Injury_Notification').Id);
        
        FormEditExtension ext = new FormEditExtension(new ApexPages.StandardController(f));       
        
        ext.SaveOverride();
        
        ext = new FormEditExtension(new ApexPages.StandardController([SELECT Id FROM Form__c LIMIT 1]));
        
        ext.AttachDocument();
        ext.File = Blob.valueOf('test');
        ext.FileName = 'test';
        ext.AttachDocument();
        
        system.debug(ext.AttachedDocuments);
        
        ext.AttachmentId = [SELECT Id FROM Attachment LIMIT 1].Id;
        ext.DeleteDocument();  
        
        system.debug(ext.FieldsRequired);              
    }
    
    static testMethod void testMedicalCertificate()
    {
        TestLoadFormData data = new TestLoadFormData();
                                           
        Form__c f = new Form__c();
        f.Client__c = data.Client.Id;
        f.Signature_Date__c = Date.today();
    
        Test.setCurrentPage(Page.SevereInjuryNotificationEdit);
        ApexPages.CurrentPage().getParameters().put('RecordType', data.RecordTypes.get('Medical_Certificate').Id);
        
        FormEditExtension ext = new FormEditExtension(new ApexPages.StandardController(f));
        ext.Signature = 'test';
                
        ext.SaveOverride();
        
        ext = new FormEditExtension(new ApexPages.StandardController([SELECT Id FROM Form__c LIMIT 1]));
    }
    
    static testMethod void testPreAccidentInformation()
    {
        TestLoadFormData data = new TestLoadFormData();
                                           
        Form__c f = new Form__c();
        f.Client__c = data.Client.Id;
    
        Test.setCurrentPage(Page.SevereInjuryNotificationEdit);
        ApexPages.CurrentPage().getParameters().put('RecordType', data.RecordTypes.get('Pre_Accident_Information').Id);
        
        FormEditExtension ext = new FormEditExtension(new ApexPages.StandardController(f));        
        
        ext.SaveOverride();
    }
    
    static testMethod void testFIMScoreSheet()
    {
        TestLoadFormData data = new TestLoadFormData();
                                           
        Form__c f = new Form__c();
        f.Client__c = data.Client.Id;
        f.Date_of_assessment__c = Date.today();
    
        Test.setCurrentPage(Page.SevereInjuryNotificationEdit);
        ApexPages.CurrentPage().getParameters().put('RecordType', data.RecordTypes.get('FIM_Score_Sheet').Id);
        
        FormEditExtension ext = new FormEditExtension(new ApexPages.StandardController(f));
        ext.Signature = 'test';        
        
        ext.SaveOverride();
        
        ext = new FormEditExtension(new ApexPages.StandardController([SELECT Id FROM Form__c LIMIT 1]));
    }
    
    static testMethod void testDischargeRehabiliationForm()
    {
        TestLoadFormData data = new TestLoadFormData();
                                           
        Form__c f = new Form__c();
        f.Client__c = data.Client.Id;
        f.Signature_Date__c = Date.today();
    
        Test.setCurrentPage(Page.DischargeRehabilitationEdit);
        ApexPages.CurrentPage().getParameters().put('RecordType', data.RecordTypes.get('Discharge_Rehabilitation_Form').Id);
        
        FormEditExtension ext = new FormEditExtension(new ApexPages.StandardController(f));
        ext.Signature = 'test';
                
        ext.SaveOverride();
        
        ext = new FormEditExtension(new ApexPages.StandardController([SELECT Id FROM Form__c LIMIT 1]));
    }
    
    static testMethod void testHomeModificationPreliminaryScoping()
    {
        TestLoadFormData data = new TestLoadFormData();
                                           
        Form__c f = new Form__c();
        f.Client__c = data.Client.Id;
        f.Signature_Date__c = Date.today();
    
        Test.setCurrentPage(Page.HomeModificationEdit);
        ApexPages.CurrentPage().getParameters().put('RecordType', data.RecordTypes.get('Home_Modification_Preliminary_Scoping').Id);
        
        FormEditExtension ext = new FormEditExtension(new ApexPages.StandardController(f));
        ext.Signature = 'test';
                
        ext.SaveOverride();
        
        ext = new FormEditExtension(new ApexPages.StandardController([SELECT Id FROM Form__c LIMIT 1]));
    }
    
    static testMethod void testAppFormOther()
    {
        TestLoadFormData data = new TestLoadFormData();
                                           
        Form__c f = new Form__c();
        f.Client__c = data.Client.Id;
        f.Signature_Date__c = Date.today();
        f.First_Name__c = 'test';
        f.Surname__c = 'test';
        f.Street__c = 'test';
        f.Suburb__c = 'test';
        f.State__c = 'test';
        f.Postcode__c = 'test';
        f.Home_Phone__c = '123456789';
        f.SAPOL_Vehicle_Collision_Report_Number__c = 'test';
        f.Date_of_Accident__c = Date.today();
        f.Time_of_Accident_hours__c = '13';
        f.Time_of_Accident_minutes__c = '00';
        f.Location_of_Accident__c = 'test';
        f.Injured_person_s_part_in_accident__c = 'test';                
        f.Injured_person_s_description_of_accident__c = 'test';        
        f.Applicant_s_injuries__c = 'test';
        f.Accident_happened_while_working__c = 'Yes';
    
        Test.setCurrentPage(Page.SevereInjuryNotificationEdit);
        ApexPages.CurrentPage().getParameters().put('RecordType', data.RecordTypes.get('Application_Form_from_Other').Id);
        ApexPages.CurrentPage().getParameters().put('PreSubmit', '1');
        
        FormEditExtension ext = new FormEditExtension(new ApexPages.StandardController(f));
        ext.Signature = 'test';        
                
        ext.SaveOverride();
        
        ext.AddMotorVehicle();
        ext.AddMotorVehicle();
        ext.AddWitness();
        ext.AddWitness();
        
        ext.SaveOverride();
        
        for (String key : ext.MotorVehicles.keySet())
        {
            ext.SelectedMotorVehicleKey = key;
            ext.DeleteMotorVehicle();
        }
        
        for (String key : ext.Witnesses.keySet())
        {
            ext.SelectedWitnessKey = key;
            ext.DeleteWitness();
        }
        
        ext = new FormEditExtension(new ApexPages.StandardController([SELECT Id FROM Form__c LIMIT 1]));        
    }
    
    static testMethod void testAppFormInsurer()
    {
        TestLoadFormData data = new TestLoadFormData();
                                           
        Form__c f = new Form__c();
        f.Client__c = data.Client.Id;
        f.Signature_Date__c = Date.today();
        f.First_Name__c = 'test';
        f.Surname__c = 'test';
        f.Street__c = 'test';
        f.Suburb__c = 'test';
        f.State__c = 'test';
        f.Postcode__c = 'test';
        f.Home_Phone__c = '123456789';
        f.SAPOL_Vehicle_Collision_Report_Number__c = 'test';
        f.Date_of_Accident__c = Date.today();
        f.Time_of_Accident_hours__c = '13';
        f.Time_of_Accident_minutes__c = '13';
        f.Location_of_Accident__c = 'test';
        f.Injured_person_s_part_in_accident__c = 'test';                
        f.Injured_person_s_description_of_accident__c = 'test';        
        f.Applicant_s_injuries__c = 'test';
        f.Accident_happened_while_working__c = 'Yes';
    
        Test.setCurrentPage(Page.SevereInjuryNotificationEdit);
        ApexPages.CurrentPage().getParameters().put('RecordType', data.RecordTypes.get('Application_Form_from_Insurer').Id);
        
        FormEditExtension ext = new FormEditExtension(new ApexPages.StandardController(f));
        ext.Signature = 'test';        
        
        ext.AddMotorVehicle();
        ext.AddMotorVehicle();
        ext.AddWitness();
        
        for (Form_Detail__c fd : ext.MotorVehicles.values())
        {
            fd.Driver_s_Name__c = 'Test';
            break;
        }
                        
        ext.SaveOverride();
                
        ext = new FormEditExtension(new ApexPages.StandardController([SELECT Id FROM Form__c LIMIT 1]));
    }
}