@isTest
private class TestRCRServiceOrderAcceptanceExtension 
{
    static testMethod void myUnitTest1() 
    {
        TestLoadData.loadRCRData();
                
        Test.StartTest();
    
        RCR_Contract__c contract = [SELECT  r.Id,
                                            r.Name,
                                            r.CCMS_ID__c,
                                            //r.RCR_CBMS_Request_For_Service__c,
                                            r.Start_Date__c,
                                            r.End_Date__c,
                                            r.Original_End_Date__c,
                                            r.Cancellation_Date__c,
                                            r.Account__c,
                                            r.Account__r.Name,
                                            r.Level__c,
                                            r.Comments__c,
                                            r.Internal_Comments__c,
                                            //r.RCR_CBMS_Contract__r.Contract_Total__c,
                                            //r.RCR_CBMS_Contract__r.Name,
                                            //r.Region__c,
                                            r.Data_Migration_Status__c,
                                            r.Migration_User__c,
                                            r.Roll_Over_Service_Order__c,
                                            r.Compensable__c,
                                            r.Total_Service_Order_Cost__c
                                   FROM     RCR_Contract__c r
                                   LIMIT 1];
                                   
        Test.setCurrentPage(Page.RCRServiceOrderAcceptance);
        ApexPages.currentPage().getParameters().put('id', contract.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(new RCR_Contract__c());
        RCRServiceOrderAcceptanceExtension ext = new RCRServiceOrderAcceptanceExtension(sc);
        
        system.debug(ext.AuthorisedToAcceptCSA);
        system.debug(ext.RCRContract);
        system.debug(ext.ProviderAcceptance);
        
        ext.rejectCSA();
        ext.accept();
        ext.savePDF();
        
        ext.forceTestError = true;  
        ext.savePDF();
        ext.createServiceQuery();
        ext.cancelOverride();
        ext.rejectCSA(); 
        ext.accept();
        ext.acceptRollovers();
        ext.acceptServiceRateVariation();
        
        Test.StopTest();
    }
    
    static testMethod void myUnitTest2() 
    {
        TestLoadData.loadRCRData();
                
        Test.StartTest();
        
        RCR_Contract__c contract = [SELECT  r.Id,
                                            r.Name,
                                            r.CCMS_ID__c,
                                           /// r.RCR_CBMS_Request_For_Service__c,
                                            r.Start_Date__c,
                                            r.End_Date__c,
                                            r.Original_End_Date__c,
                                            r.Cancellation_Date__c,
                                            r.Account__c,
                                            r.Account__r.Name,
                                            r.Level__c,
                                            r.Comments__c,
                                            r.Internal_Comments__c,
                                           // r.RCR_CBMS_Contract__r.Contract_Total__c,
                                           // r.RCR_CBMS_Contract__r.Name,
                                            //r.Region__c,
                                            r.Data_Migration_Status__c,
                                            r.Migration_User__c,
                                            r.Roll_Over_Service_Order__c,
                                            r.Compensable__c,
                                            r.Total_Service_Order_Cost__c
                                   FROM     RCR_Contract__c r
                                   LIMIT 1];
                                   
        Test.setCurrentPage(Page.RCRServiceOrderAcceptance);
        ApexPages.currentPage().getParameters().put('id', contract.Id);
        ApexPages.currentPage().getParameters().put('type', 'Cancellation');
        ApexPages.StandardController sc = new ApexPages.StandardController(new RCR_Contract__c());
        RCRServiceOrderAcceptanceExtension ext = new RCRServiceOrderAcceptanceExtension(sc);
        
        system.debug(ext.AuthorisedToAcceptCSA);
        system.debug(ext.RCRContract);
        system.debug(ext.ProviderAcceptance);
        
        ext.rejectCSA();
        ext.accept();
        ext.savePDF();
        ext.createServiceQuery();
        ext.cancelOverride();
        
        Test.StopTest();
    }
    
    static testMethod void myUnitTest3() 
    {
        TestLoadData.loadRCRData();
                
        Test.StartTest();
    
        RCR_Contract__c contract = [SELECT  r.Id,
                                            r.Name,
                                            r.CCMS_ID__c,
                                          //  r.RCR_CBMS_Request_For_Service__c,
                                            r.Start_Date__c,
                                            r.End_Date__c,
                                            r.Original_End_Date__c,
                                            r.Cancellation_Date__c,
                                            r.Account__c,
                                            r.Account__r.Name,
                                            r.Level__c,
                                            r.Comments__c,
                                            r.Internal_Comments__c,
                                           // r.RCR_CBMS_Contract__r.Contract_Total__c,
                                            //r.RCR_CBMS_Contract__r.Name,
                                            //r.Region__c,
                                            r.Data_Migration_Status__c,
                                            r.Migration_User__c,
                                            r.Roll_Over_Service_Order__c,
                                            r.Compensable__c,
                                            r.Total_Service_Order_Cost__c
                                   FROM     RCR_Contract__c r
                                   //WHERE    Name = 'TestSO'
                                   LIMIT 1];
                                   
        contract.RecordTypeId = A2HCUtilities.getRecordType('RCR_Contract__c', APSRecordTypes.RCRServiceOrder_Rollover).Id;
        update contract;                      
                                   
        Test.setCurrentPage(Page.RCRServiceOrderAcceptance);
        ApexPages.currentPage().getParameters().put('id', contract.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(contract);
        RCRServiceOrderAcceptanceExtension ext = new RCRServiceOrderAcceptanceExtension(sc);
        
        ext.acceptRollovers();
        
        Test.StopTest();
    }

    static testMethod void myUnitTest4() 
    {
        TestLoadData.loadRCRData();
                
        Test.StartTest();
    
        RCR_Contract__c serviceOrder = [SELECT  r.Id,
                                            r.Name,
                                            r.CCMS_ID__c,
                                           // r.RCR_CBMS_Request_For_Service__c,
                                            r.Start_Date__c,
                                            r.End_Date__c,
                                            r.Original_End_Date__c,
                                            r.Cancellation_Date__c,
                                            r.Account__c,
                                            r.Account__r.Name,
                                            r.Level__c,
                                            r.Comments__c,
                                            r.Internal_Comments__c,
                                           // r.RCR_CBMS_Contract__r.Contract_Total__c,
                                          //  r.RCR_CBMS_Contract__r.Name,
                                            //r.Region__c,
                                            r.Data_Migration_Status__c,
                                            r.Migration_User__c,
                                            r.Roll_Over_Service_Order__c,
                                            r.Compensable__c,
                                            r.Total_Service_Order_Cost__c,
                                            (SELECT Id,
                                                    RCR_Service__c,
                                                    Total_Cost__c
                                             FROM   RCR_Contract_Scheduled_Services__r)
                                   FROM     RCR_Contract__c r
                                   //WHERE    Name = 'TestSO'
                                   LIMIT 1];
                                   
        serviceOrder.RecordTypeId = A2HCUtilities.getRecordType('RCR_Contract__c', APSRecordTypes.RCRServiceOrder_Rollover).Id;
        update serviceOrder; 
        
        RCR_Service_Rate__c rate = [SELECT  Id
                                    FROM    RCR_Service_Rate__c
                                    WHERE   RCR_Service__c = :serviceOrder.RCR_Contract_Scheduled_Services__r[0].RCR_Service__c
                                    ORDER BY Start_Date__c desc
                                    LIMIT 1];       
        
        Account acct = [SELECT Id FROM Account LIMIT 1];
        RCR_Service_Rate_Variation__c rateVariation = new RCR_Service_Rate_Variation__c();
        rateVariation.Account__c = acct.Id;
        insert rateVariation;    
        
        RCR_Service_Rate_Audit__c rateAudit = new RCR_Service_Rate_Audit__c();
        rateAudit.RCR_Service_Rate_Variation__c = rateVariation.id;
        rateAudit.RCR_Contract_Scheduled_Service__c = serviceOrder.RCR_Contract_Scheduled_Services__r[0].Id;
        rateAudit.RCR_Service_Order__c = serviceOrder.Id;
        rateAudit.RCR_Service_Rate__c = rate.Id;
        rateAudit.Recalculation_ID__c = 10000000;
        rateAudit.Scheduled_Service_Value_Before__c = serviceOrder.RCR_Contract_Scheduled_Services__r[0].Total_Cost__c;
        rateAudit.Service_Order_Value_Before__c = serviceOrder.Total_Service_Order_Cost__c;
        rateAudit.Proposed_Rate_Activation__c = true;
        insert rateAudit;       
                                   
        Test.setCurrentPage(Page.RCRServiceOrderAcceptance);
        ApexPages.currentPage().getParameters().put('id', serviceOrder.Id);
        ApexPages.currentPage().getParameters().put('type', 'RateVariation');
        ApexPages.currentPage().getParameters().put('variationID', rateVariation.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(new RCR_Contract__c());
        RCRServiceOrderAcceptanceExtension ext = new RCRServiceOrderAcceptanceExtension(sc);
        
        ext.acceptServiceRateVariation();
        ext.savePDF();
        
        Test.StopTest();
    }
}