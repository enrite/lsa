public class RCRApprovedServiceTypesExtension
	extends A2HCPageBase
{
	public RCRApprovedServiceTypesExtension(ApexPages.StandardController controller)
	{
		AccountProgramCategoryId = getParameter('srvcId');

		if(AccountProgramCategoryId == null)
		{
			AccountProgramCategoryId = getParameter('retURL');
			if(AccountProgramCategoryId != null)
			{
				AccountProgramCategoryId = AccountProgramCategoryId.replace('/', '');
			}
		}

		AccountProgramCategory = [SELECT	r.RCR_Program__c,
											r.RCR_Program_Category__c,
											r.Name,
											r.Id,
											r.Account__c
								  FROM		RCR_Account_Program_Category__c r
								  WHERE		r.Id = :AccountProgramCategoryId];
	}

	public String AccountProgramCategoryId
	{
		get;
		set;
	}


	public RCR_Account_Program_Category__c AccountProgramCategory
	{
		get;
		private set;
	}


	public List<ServiceTypeWrapper> ServiceTypes
	{
		get
		{
			if(ServiceTypes == null)
			{
				ServiceTypes = new List<ServiceTypeWrapper>();

				for(RCR_Program_Category_Service_Type__c t : [SELECT Name,
																	(SELECT	Id,
																	        Name,
																	        Approved__c
																	  FROM	RCR_Approved_Service_Types__r
																	  WHERE RCR_Account_Program_Category__r.Account__c = :AccountProgramCategory.Account__c)
															  FROM	  RCR_Program_Category_Service_Type__c
															  WHERE	  RCR_Program_Category__c = :AccountProgramCategory.RCR_Program_Category__c
															  ORDER BY Name])
				{
					if(t.RCR_Approved_Service_Types__r.size() > 0)
					{
						ServiceTypes.add(new ServiceTypeWrapper(true, t, t.RCR_Approved_Service_Types__r[0].Approved__c));
					}
					else
					{
						ServiceTypes.add(new ServiceTypeWrapper(false, t, false));
					}
				}
			}
			return ServiceTypes;
		}
		private set;
	}


	public PageReference saveOverride()
	{
		try
		{
			for(ServiceTypeWrapper servType : ServiceTypes)
			{
				RCR_Approved_Service_Type__c st;
				if(servType.ServiceType.RCR_Approved_Service_Types__r.size() > 0)
				{
					st = servType.ServiceType.RCR_Approved_Service_Types__r[0];
				}
				else
				{
					st = new RCR_Approved_Service_Type__c();
					st.RCR_Account_Program_Category__c = AccountProgramCategory.Id;
				}
				st.RCR_Program_Category_Service_Type__c = servType.ServiceType.Id;
				st.Approved__c = servType.Approved;
				upsert st;
			}
			ApexPages.Pagereference ref = new ApexPages.Pagereference('/' + AccountProgramCategoryId);
	    	ref.setRedirect(true);
	    	return ref;
		}
		catch (Exception ex)
		{
			return A2HCException.formatException(ex);
		}
	}


	public PageReference cancelOverride()
    {
    	try
    	{
    		ApexPages.Pagereference ref = new ApexPages.Pagereference('/' + AccountProgramCategoryId);
	    	ref.setRedirect(true);
    		return ref;
    	}
    	catch(Exception ex)
    	{
    		return A2HCException.formatException(ex);
    	}
    }

	public class ServiceTypeWrapper
    {
    	public ServiceTypeWrapper(boolean pSelected, RCR_Program_Category_Service_Type__c pServiceType, boolean pApproved)
    	{
    		Selected = pSelected;
    		ServiceType = pServiceType;
    		Approved = pApproved;
    	}

    	public boolean Selected
    	{
    		get;
    		set;
    	}

    	public boolean Approved
    	{
    		get;
    		set;
    	}

    	public RCR_Program_Category_Service_Type__c ServiceType
    	{
    		get;
    		set;
    	}
    }
}