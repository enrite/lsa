@isTest
private class LSAEmailContractExtensionTest {
	@isTest(SeeAllData=true)
	static void emailContractExtensionTest()
	{
		Test.startTest();
		Account acc = new Account(Name='test');
		insert acc;

		Contact c = new Contact(LastName='test1',AccountId=acc.id,Email='test@test.com');
		insert c;

		RCR_Contract__c contract = new RCR_Contract__c(Name='Test',Account__c=acc.Id);
		insert contract;

		ApexPages.StandardController sc = new ApexPages.StandardController(contract);
		LSAEmailContractExtension controller = new LSAEmailContractExtension(sc);

		controller.contactId = c.Id;
		System.debug(controller.Contacts);
		controller.email();
		System.assertEquals(ApexPages.getMessages()[0].getDetail() , 'The Contract has been sent successfully to the selected contact.');
		Test.stopTest();
	}
}