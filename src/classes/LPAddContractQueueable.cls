public class LPAddContractQueueable implements Queueable, Database.AllowsCallouts
{
    public LPAddContractQueueable(List<RCR_Contract__c> contracts)
    {
        this.contracts = contracts;
    }

    private List<RCR_Contract__c> contracts;

    public void execute(QueueableContext param1)
    {
        LPRESTInterface.addContracts(contracts);
    }
}