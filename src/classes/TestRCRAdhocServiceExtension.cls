/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestRCRAdhocServiceExtension
{
    @isTest//(SeeAllData=true)
    static void myUnitTest()
    {       
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        TestLoadData.loadAdhocServices();
    
        RCR_Contract__c contract = TestRCRReconcileBatch.getServiceOrder(); 

        RCR_Contract_Adhoc_Service__c adHocServ = contract.RCR_Contract_Adhoc_Services__r[0];
       
        Test.setCurrentPage(Page.RCR_Adhoc_Service);
        // Get the existing Adhoc Service
        ApexPages.currentPage().getParameters().put('id', adHocServ.ID);
        RCRAdhocServiceExtension ext = new RCRAdhocServiceExtension(new ApexPages.StandardController(adHocServ));
        
        system.assert(ext.ReadOnly != null);
        system.assert(ext.SubPrograms != null);
        
        
        ext.saveOverride();

        ext.AdhocService.Amount__c = 100.00;
        ext.AdhocService.Start_Date__c = contract.Start_Date__c;
        ext.AdhocService.Original_End_Date__c = contract.End_Date__c;

        ext.ServiceTypesController = new RCRContractServiceTypeController(); 
        ext.ServiceTypesController.ParentObject = ext.AdhocService;
        ext.ServiceTypesController.addContractServiceType();
        ext.ServiceTypesController.SelectedProgram = [SELECT   Id
                                                    FROM     RCR_Program__c
                                                    WHERE    ID IN (SELECT  RCR_Program__c
                                                                    FROM   RCR_Program_Category__c)
                                                    LIMIT 1].ID;

        ext.ServiceTypesController.SelectedProgramCategory = [SELECT    Id
                                                            FROM      RCR_Program_Category__c
                                                            WHERE     RCR_Program__c = :ext.ServiceTypesController.SelectedProgram AND
                                                                    ID IN (SELECT  RCR_Program_Category__c
                                                                            FROM    RCR_Program_Category_Service_Type__c)
                                                            LIMIT 1].ID;
        ext.ServiceTypesController.addContractServiceType();

        ext.ServiceTypesController.SelectedServiceType = [SELECT  Id
                                                            FROM    RCR_Program_Category_Service_Type__c
                                                            WHERE   RCR_Program_Category__c = :ext.ServiceTypesController.SelectedProgramCategory
                                                            LIMIT 1].ID;
        ext.ServiceTypesController.addContractServiceType();

        ext.saveOverride();
        ext.cancelOverride();

        

        ApexPages.currentPage().getParameters().remove('id');
        ApexPages.currentPage().getParameters().put('retURL', '/' + contract.ID);
        ext = new RCRAdhocServiceExtension(new ApexPages.StandardController(new RCR_Contract_Adhoc_Service__c()));

        ext.saveOverride();

        ext.AdhocService.Amount__c = 100.00;
        ext.AdhocService.Start_Date__c = contract.Start_Date__c;
        ext.AdhocService.Original_End_Date__c = contract.End_Date__c;
        ext.AdhocService.RCR_Account_Program_Category__c = adHocServ.RCR_Account_Program_Category__c;
        ext.AdhocService.RCR_Sub_Program__c = adHocServ.RCR_Sub_Program__c;
        ext.saveOverride();
        
        ext.AdhocService.Amount__c = -100.00;
        ext.saveOverride();
        
        ext.ServiceTypesController = new RCRContractServiceTypeController();
        ext.ServiceTypesController.ContractServiceTypes.clear();
        ext.saveOverride();
        Test.stopTest();

    }
    
    @isTest//(SeeAllData=true)
    static void testError()
    {     
        TestLoadData.loadRCRData();
        
        Test.StartTest();
        
        TestLoadData.loadAdhocServices();
              
        RCR_Contract__c contract = TestRCRReconcileBatch.getServiceOrder(); 

        RCR_Contract_Adhoc_Service__c adHocServ = contract.RCR_Contract_Adhoc_Services__r[0];
        
        Test.setCurrentPage(Page.RCR_Adhoc_Service);
        // Get the existing Adhoc Service
        ApexPages.currentPage().getParameters().put('id', adHocServ.ID);
        RCRAdhocServiceExtension ext = new RCRAdhocServiceExtension(new ApexPages.StandardController(adHocServ));
        
        ext.forceTestError = true;
        
        try
        {
            ext.saveOverride();
        }
        catch(Exception ex)
        {}
         
        try
        {
            ext.cancelOverride();
        }
        catch(Exception ex)
        {}
        try
        {
            ext.AdhocService = new RCR_Contract_Adhoc_Service__c();
            ext.saveOverride();
        }
        catch(Exception ex)
        {}   
        
        Test.StopTest();   
    }
    
}