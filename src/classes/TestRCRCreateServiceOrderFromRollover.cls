@isTest//(seeAllData=true)
private class TestRCRCreateServiceOrderFromRollover 
{
    static testMethod void myUnitTest() 
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        RCR_Contract__c so = [SELECT Id FROM RCR_Contract__c LIMIT 1 /*WHERE Name = 'TestSO' LIMIT 1*/];
                        
        RCRCreateServiceOrderFromRollover cntrl = new RCRCreateServiceOrderFromRollover(new ApexPages.Standardcontroller(new RCR_Contract__c()));
        Test.setCurrentPage(Page.RCR);
        ApexPages.currentPage().getParameters().put('id', so.Id);
        cntrl.CreateServiceOrder();
        
        cntrl.forceTestError = true;
        cntrl.CreateServiceOrder();
        
        Test.stopTest();
    }
}