/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestEmailManager 
{
    @IsTest
    static void test1()
    {
        EmailManager em = new EmailManager();
        User u = [SELECT ID,
                        Email
                FROM    User
                WHERE   IsActive = true AND
                        //UserRole.Name LIKE 'RCR%' AND
                        UserType = 'Standard'
                        LIMIT 1];
        insert new Service_Query__c(Assigned_To__c = u.ID); 
        
        Set<ID> sqUsers = new Set<ID>();        
        List<Service_Query__c> queries = new List<Service_Query__c>();
        for(Service_Query__c sq : [SELECT   ID,
                                            Assigned_To__c,
                                            Assigned_To__r.Email,
                                            CreatedByID
                                    FROM    Service_Query__c
                                    WHERE   Assigned_To__c IN (SELECT ID
                                                            FROM    User
                                                            WHERE   IsActive = true AND
                                                                UserType = 'Standard')
                                    LIMIT 10])
        {
            sqUsers.add(sq.Assigned_To__c);
            queries.add(sq);
        }

        Map<ID, User> usersByID = new Map<ID, User>([SELECT     ID,
                                                                Email
                                                        FROM    User
                                                        WHERE   IsActive = true AND
                                                                UserType = 'Standard' AND
                                                            ID IN :sqUsers]);                                   
        em.sendUpdatedServiceQueryEmails(queries, usersByID);
        em.sendInternalServiceQueryEmails(queries);
    }
    
    @IsTest
    static void test2()
    {
        EmailManager em = new EmailManager();
        
        List<User> portalUsers = [SELECT    ID,
                                            Email,
                                            Name
                                    FROM    User
                                    WHERE   IsActive = true AND
                                            UserType = 'CspLiteportal'
                                    LIMIT 5];                                   
        for(RCR_Contract__c serviceOrder : [SELECT ID,
                                                Name,
                                                Submited_For_Approval_User_ID__c,
                                                CCMS_ID__c,
                                                Approver_User_Name__c,
                                                Approver_User_ID__c,
                                                Account__r.Name,
                                                Client__r.Name,
                                                Client__r.Title__c,
                                                Client__r.Full_Name__c,
                                                Client__r.CCMS_ID__c,
//                                                RCR_CBMS_Contract__r.Client__r.Full_Name__c,
//                                                RCR_CBMS_Contract__r.Client__r.Title__c,
                                                Group_Program__c,
                                                RecordTypeId                                            
                                        FROM    RCR_Contract__c
                                        WHERE   Submited_For_Approval_User_ID__c != null
                                        LIMIT 1])
        {
            em.sendCSAAcceptanceNotifications(portalUsers, serviceOrder);
            
            em.sendSubmitterApprovalGrantedNotification(serviceOrder);
            em.sendSubmitterApprovalRejectedNotification(serviceOrder);
            em.sendNoCSAApproversForAccountNotification(serviceOrder);
            em.sendSubmitterServiceProviderAcceptanceNotification(serviceOrder, APS_PicklistValues.RCRContractCSAStatus_Accepted, portalUsers[0]);
            em.sendSubmitterServiceProviderAcceptanceNotification(serviceOrder, 'test', portalUsers[0]); 
            em.sendCSAAcceptanceNotifications(portalUsers, new List<RCR_Contract__c>{serviceOrder});
            em.sendNoCSAApproversForAccountNotification(new List<RCR_Contract__c>{serviceOrder}, UserInfo.getUserId());
        }
        EmailManager.sendApprovalProcessComplete(new String[]{'test@test.com'}, 'test', 1, 1);
    }
    
    @IsTest
    static void test3()
    {
        EmailManager em = new EmailManager();                                   
        for(RCR_Invoice__c invoice : [SELECT ID                                             
                                        FROM    RCR_Invoice__c
                                        LIMIT 1])
        {
            em.sendSubmitInvoiceAsyncComplete(invoice);         
        }
        
        String[] emailAddresses = new String[]{'test@test.com'};
        EmailManager.sendApprovalProcessComplete(emailAddresses, 'test', 1, 1);
        EmailManager.sendHTMLMessage(emailAddresses,  'test',  'test',  'test',  emailAddresses[0]);    
        EmailManager.sendHTMLMessage((ID)UserInfo.getUserID(), 'test',  'test',  'test',  emailAddresses[0]);
        EmailManager.sendPlainTextMessage(UserInfo.getUserID(), 'test',  'test',  'test',  emailAddresses[0]);
        EmailManager.sendPlainTextMessage(emailAddresses, 'test',  'test',  'test',  emailAddresses[0]);
    }
    
    @IsTest
    static void test4()
    {
        EmailManager em = new EmailManager();                                   
        em.sendSubmitterApprovalGrantedNotification(new RCR_Contract__c());
        em.sendServiceOrderBrokerageCompleted(new Task());
        em.sendServiceOrderRequestReviwed(new RCR_Contract__c());
        em.sendServiceOrderReviewStatusUpdated(new Task());
        em.sendSubmitInvoiceAsyncComplete(new RCR_Invoice__c());
        /*em.sendSubmitInvoiceAsyncComplete([SELECT ID 
                                            FROM AsyncApexJob 
                                            LIMIT 1].ID);*/
    }
    
    @IsTest
    static void test5()
    {
        EmailManager em = new EmailManager();   
        for(RCR_Contract__c so : [SELECT Group_Program__c,
                                        Client__r.Name,
                                        Client__r.Title__c,
                                        Client__r.CCMS_ID__c
                                FROM    RCR_Contract__c
                                WHERE   Group_Program__c != null
                                LIMIT 1])
        {                   
            em.getSOClientName(so);
        }
        for(RCR_Contract__c so : [SELECT    Group_Program__c,
                                            Client__r.Name,
                                            Client__r.Title__c,
                                            Client__r.CCMS_ID__c
                                    FROM    RCR_Contract__c
                                    WHERE   Client__r.Name != null AND
                                            Group_Program__c = null
                                    LIMIT 1])
                        
        {                   
            em.getSOClientName(so);
        }
        for(RCR_Contract__c so : [SELECT Group_Program__c,
                                        Client__r.Name,
                                        Client__r.Title__c,
                                        Client__r.CCMS_ID__c
                                FROM    RCR_Contract__c
                                WHERE   Client__r.Name = null AND
                                        Group_Program__c = null
                                LIMIT 1])   
        {                   
            em.getSOClientName(so);
        }
    }
    
    @IsTest
    static void test6()
    {
        EmailManager em = new EmailManager();   
        List<User> portalUsers = [SELECT    ID,
                                            Email,
                                            Name,
                                            Contact.AccountID
                                    FROM    User
                                    WHERE   IsActive = true AND
                                            UserType = 'CspLiteportal' AND
                                            Contact.AccountID != null
                                    LIMIT 5];   
        
        for(User u : portalUsers)
        {       
            RCR_Service_Rate_Variation__c rateVariation = new RCR_Service_Rate_Variation__c();
            rateVariation.Account__c = u.Contact.AccountID;
            insert rateVariation;
            em.sendRateVariationNotifications(portalUsers, rateVariation);
        }                           
        
    }
        
    @IsTest
    static void test7()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
        
        Account acc = [SELECT ID FROM ACCOUNT LIMIT 1];
      
        Contact cnt = new Contact (FirstName='Joe',LastName='Schmoe',AccountId=acc.id);
        insert cnt;
      
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduserxyz@testorg.com', Contact = cnt);
        insert u;
        
        List<User> pusers = new List<User> { u };
        
        RCR_Contract__c c = [SELECT Id,
                                    Name,
                                    Group_Program__c,
                                    Client__r.Title__c,
                                    Client__r.Name,
                                    Client__r.CCMS_ID__c,
                                    Approver_User_Name__c,
                                    Submited_For_Approval_User_ID__c,
                                    Account__c
                             FROM RCR_Contract__c
                             LIMIT 1];
                
        EmailManager em = new EmailManager(); 
        
        em.sendParticipationPlanManualAcceptance(c);
        em.sendCSAAcceptanceNotifications(pusers, c);       
        em.sendCSAAcceptanceNotifications(pusers, new List<RCR_Contract__c> { c });               
        em.sendSubmitterApprovalRejectedNotification(c);
        em.sendNoCSAApproversForAccountNotification(c);
        em.sendNoCSAApproversForAccountNotification(new List<RCR_Contract__c> { c }, UserInfo.getUserId());
        em.sendSubmitterServiceProviderAcceptanceNotification(c, 'Accepted', u);    
                
        Test.StopTest();                      
    }        
}