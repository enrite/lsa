global without sharing class UserUtilities 
{
    private static User currentUser;

    public static Map<String, UserRole> RolesByDeveloperName
    {
        get;
        set;
    }

    public static Map<Id, Set<Id>> RoleUsers 
    { 
        get;
        set;
    }
    
    public static Map<Id, Set<Id>> RoleSubordinateUsers 
    { 
        get;
        set;
    }

    public static Map<Id, Set<Id>> RoleAndSuperiorUsers
    {
        get;
        set;
    }
    
    public static Map<Id, Set<Id>> GroupUsers 
    { 
        get;
        set;
    }
    
    public static Map<Id, Set<Id>> UserGroups  
     { 
        get;
        set;
    }
    
    public static Map<Id, Set<Id>> UserQueues 
     { 
        get;
        set;
    }

    public static User ManagerASDUser
    {
        get
        {
            if(ManagerASDUser== null)
            {
                ManagerASDUser = getUserFromRole('Manager_ASD');
            }
            return ManagerASDUser;
        }
        set;
    }

    public static User getUserFromRole(String roleDeveloperName)
    {
        return getUser(null, roleDeveloperName);
    }

    public static Boolean isAdminUser()
    {
        return getCurrentUser().Profile.Name == 'System Administrator';
    }

    public static User getUser(ID userID)
    {
        return getUser(userID, null);
    }

    public static User getCurrentUser()
    {
        if(currentUser == null)
        {
            currentUser = getUser(UserInfo.getUserId());
        }
        return currentUser;
    }
    
    private static User getUser(ID userID, String roleDeveloperName)
    {
        return [SELECT  ID,
                        Name,
                        Username,
                        UserType,
                        UserRoleId,
                        Title,
                        FullName__c,
                        Street,
                        State,
                        PostalCode,
                        Phone,
                        Fax,                        
                        MobilePhone,
                        LastName,
                        IsActive,
                        FirstName,
                        Email,
                        ContactId,
                        CommunityNickname,
                        Grant_Disclaimer__c,
                        Alias,
                        AccountId,
                        AboutMe,
                        A2HC_Site__c,
                        Account_ID__c,
                        Account_Name__c,
                        Account_Billing_Address__c,
                        Account_Phone__c,
                        Account_Fax__c,
                        Account_ABN__c,
                        RCR_Service_Provider_Code__c,
                        User_Type__c,
                        Authorised_To_Accept_CSA__c,
                        Preferred_Team_Manager__c,
                        Service_Planner_Delegation__c,
                        Quality_Reviewer__c,
                        ProfileId,
                        Profile.Name,
                        UserRole.Name,
                        UserRole.DeveloperName,
                        ManagerId,
                        Manager.Name
                FROM    User    
                WHERE   ID = :(userID == null ? '~' : userID) OR
                        (UserRole.DeveloperName = :(roleDeveloperName == null ? '~' : roleDeveloperName) AND
                        IsActive = true)
                LIMIT 1];
    }
    
//    public static User getA2HCOfficer(Boolean anyOfficer, String userName, String firstName, String lastName)
//    {
//        User a2hcOfficer;
//        if(anyOfficer)
//        {
//            a2hcOfficer = [SELECT   u.Username,
//                                    u.UserType,
//                                    u.UserRoleId,
//                                    u.ProfileId,
//                                    u.Name,
//                                    u.LastName,
//                                    u.IsActive,
//                                    u.Id,
//                                    u.FirstName,
//                                    u.Email,
//                                    //u.A2HC_eReferral_Enabled__c,
//                                    u.ContactId,
//                                    u.Alias,
//                                    u.A2HC_Site__c
//                            FROM    User u
//                            WHERE   u.IsActive = true AND
//                                    u.UserRoleId IN (SELECT ID
//                                                    FROM    UserRole
//                                                    WHERE   Name = 'A2HC Officer') AND
//                                    u.ProfileId IN (SELECT  p.ID
//                                                    FROM    Profile p
//                                                    WHERE   p.Name = 'A2HC Officer')
//                            LIMIT 1];
//        }
//        else
//        {
//            a2hcOfficer = [SELECT   u.Username,
//                                    u.UserType,
//                                    u.UserRoleId,
//                                    u.ProfileId,
//                                    u.Name,
//                                    u.LastName,
//                                    u.IsActive,
//                                    u.Id,
//                                    u.FirstName,
//                                    u.Email,
//                                    //u.A2HC_eReferral_Enabled__c,
//                                    u.ContactId,
//                                    u.Alias,
//                                    u.A2HC_Site__c
//                            FROM    User u
//                            WHERE   u.ProfileId IN (SELECT  p.ID
//                                                    FROM    Profile p
//                                                    WHERE   p.Name = 'A2HC Officer') AND
//                                    (
//                                        u.Username = :userName OR
//                                        (u.FirstName = :firstName AND
//                                        u.LastName = :lastName)
//                                    )];
//        }
//
//        return a2hcOfficer;
//    }
    
    public static Boolean isUserInPermissionSet(String permissionSetName, ID userID, Boolean isAdmin)
    {
        return isAdmin ? true : ([SELECT COUNT()
                                        FROM    PermissionSetAssignment 
                                        WHERE   PermissionSet.Name = :permissionSetName AND
                                                AssigneeId = :userID] > 0);
    }
    
    public static Map<string, boolean> accessablePages
    {
        get
        {
            if(accessablePages == null)
            {
                Set<string> pageIDs = new Set<string>();
                accessablePages = new Map<string, boolean>();
                
                for(PermissionSet ps : [SELECT  Id, 
                                                Name, 
                                                (SELECT Id, 
                                                        setupentityid
                                                 FROM   SetupEntityAccessItems 
                                                 WHERE  setupentityid IN (SELECT Id 
                                                                          FROM   ApexPage))
                                        FROM    PermissionSet
                                        WHERE   ID IN (SELECT   PermissionSetID
                                                       FROM     PermissionSetAssignment
                                                       WHERE    AssigneeId = :UserInfo.getUserID())])
                {
                    for(SetupEntityAccess sea : ps.SetupEntityAccessItems)
                    {
                        pageIDs.add(sea.setupentityid);
                    }
                }
                Boolean isAdmin = getUser(UserInfo.getUserId()).Profile.Name == 'System Administrator';
                for(ApexPage p : [SELECT id,
                                         Name 
                                  FROM   ApexPage])
                {
                    
                    accessablePages.put(p.Name, pageIDs.contains(p.Id) || isAdmin);
                }
            }
            return accessablePages;
        }
        private set;
    }
    
    public static Group getGroup(String groupDeveloperName)
    {
        return getGroup(groupDeveloperName, false);
    }
    
    public static Group getQueue(String groupDeveloperName)
    {
        return getGroup(groupDeveloperName, true);
    }

    public static Boolean isUserInGroupByID(String groupID, ID userId)
    {
        if(GroupUsers == null)
        {
            getUserGroups();
        }
        return GroupUsers.containsKey(groupID) && GroupUsers.get(groupID).contains(userId);
    }
    
    public static Boolean isUserInRoleByID(String roleID, ID userId, Boolean includeSubordinates)
    {
        if(RoleUsers == null || RoleSubordinateUsers == null)
        {
            getUserGroups();
        }
        
        if(includeSubordinates)
        {
            return RoleSubordinateUsers.containsKey(roleID) && RoleSubordinateUsers.get(roleID).contains(userId);
        }
        else
        {
            return RoleUsers.containsKey(roleID) && RoleUsers.get(roleID).contains(userId);
        }
        return false;
    }
    
    public static Boolean isUserInGroup(String groupDeveloperName, ID userId)
    {
        if(GroupUsers == null)
        {
            getUserGroups();
        }
        Group grp = getGroup(groupDeveloperName);
        return grp != null && GroupUsers.containsKey(grp.ID) && GroupUsers.get(grp.ID).contains(userId);
    }
    
    public static Boolean isUserInRole(String roleDeveloperName, ID userId, Boolean includeSubordinates)
    {
        if(RoleUsers == null || RoleSubordinateUsers == null)
        {
            getUserGroups();
        }
        
        for(UserRole rle : [SELECT  ID
                                FROM    UserRole
                                WHERE   DeveloperName = :roleDeveloperName])
        {
            if(includeSubordinates)
            {
                return RoleSubordinateUsers.containsKey(rle.ID) && RoleSubordinateUsers.get(rle.ID).contains(userId);
            }
            else
            {
                return RoleUsers.containsKey(rle.ID) && RoleUsers.get(rle.ID).contains(userId);
            }
        }
        return false;
    }

    public static Boolean userHasRole(String roleDeveloperName, ID userId)
    {
        if(RoleAndSuperiorUsers == null)
        {
            getUserGroups();
        }
        UserRole rle = RolesByDeveloperName.get(roleDeveloperName);
system.debug(rle);
        if(rle != null)
        {
system.debug(RoleAndSuperiorUsers.get(rle.ID));
            return RoleAndSuperiorUsers.get(rle.ID).contains(userId);
        }
        return false;
    }
    
    public static Boolean isPrefixForID(Id sfdcId, String prefix) 
    {
        if(sfdcId == null || prefix == null || prefix.length() != 3) 
        {
            return false;
        } 
        else 
        {
            return String.valueOf(sfdcId).startsWith(prefix);
        }
    }
    
    private static void getUserGroups()
    {       
        final String USERPREFIX = User.SObjectType.getDescribe().getKeyPrefix();
        RolesByDeveloperName = new Map<String, UserRole>();
        RoleUsers = new Map<Id, Set<Id>>();
        RoleSubordinateUsers = new Map<Id, Set<Id>>();
        RoleAndSuperiorUsers = new Map<Id, Set<Id>>();
        GroupUsers = new Map<Id, Set<Id>>();
        UserGroups = new Map<Id, Set<Id>>();
        UserQueues = new Map<Id, Set<Id>>();
        
        Set<Id> userIds;
        Set<Id> queueIds;
        Id subordinateId;
        
        Map<Id, UserRole> roles = new Map<Id, UserRole>(
                            [SELECT Id,
                                    Name,
                                    DeveloperName,
                                    ParentRoleId,
                                    (SELECT Id,
                                            Name
                                    FROM    Users
                                    WHERE   IsActive = true)
                            FROM     UserRole
                            ORDER BY ParentRoleId NULLS LAST]);
        Map<Id, Group> groups = new Map<Id, Group>(
                                [SELECT Id,
                                        Name,
                                        DeveloperName,
                                        Type,
                                        RelatedId,
                                        (SELECT UserOrGroupId
                                        FROM    GroupMembers)
                                FROM    Group]);
                                
        for(UserRole uRole : roles.values())
        {
            RolesByDeveloperName.put(uRole.DeveloperName, uRole);
            //Add the User Ids of the current role
            RoleUsers.put(uRole.Id, (new Map<Id, User>(uRole.Users)).keySet());
            //Loop throuh and populate the role and subordinate Map
            subordinateId = null;
            UserRole role = uRole;
            while(role != null)
            {
                if(!RoleSubordinateUsers.containsKey(role.Id))
                {
                    RoleSubordinateUsers.put(role.Id, new Set<Id>());
                }
                userIds = RoleSubordinateUsers.get(role.Id);
                if(RoleSubordinateUsers.containsKey(subordinateId)) 
                {
                    userIds.addAll(RoleSubordinateUsers.get(subordinateId));
                }
                for(User user : role.Users) 
                {
                    userIds.add(user.Id);
                }
                subordinateId = role.Id;
                role = roles.get(role.ParentRoleId);
            }
        }
        Map<ID, Set<ID>> roleAndParentRolesById = new Map<ID, Set<ID>>();
        for(UserRole role : roles.values())
        {
            roleAndParentRolesById.put(role.ID, getParentRoleSet(roles.values(), role));
        }
        for(ID roleID : roleAndParentRolesById.keySet())
        {
            Set<ID> roleUserIds = new Set<ID>();
            roleUserIds.addAll(RoleUsers.get(roleID));
            for(ID parentRoleId : roleAndParentRolesById.get(roleID))
            {
                roleUserIds.addAll(RoleUsers.get(parentRoleId));
            }
            RoleAndSuperiorUsers.put(roleID, roleUserIds);
        }

        List<Id> groupIdStack;
        Group grp;
        for(Id groupId : groups.keySet())
        {
            groupIdStack = new List<Id> { groupId };
            userIds = new Set<Id>();
            do 
            {
                grp = groups.get(groupIdStack.remove(0));
                //This is extra cautious, it should not be possible to get a null value
                if(grp == null) 
                {
                    continue;
                }
                if(grp.RelatedId != null) 
                {
                    if(grp.Type == 'Role') 
                    {
                        userIds.addAll(RoleUsers.get(grp.RelatedId));
                    } 
                    else if(grp.Type == 'RoleAndSubordinates') 
                    {
                        userIds.addAll(RoleSubordinateUsers.get(grp.RelatedId));
                    }
                } 
                else 
                {
                    for(GroupMember member : grp.GroupMembers)
                    {
                        if(isPrefixForID(member.UserOrGroupId, USERPREFIX)) 
                        {
                            userIds.add(member.UserOrGroupId);
                        } 
                        else 
                        {
                            //When the Id is not a user Id it is reliably a group Id, that we can add to the stack for processing
                            groupIdStack.add(member.UserOrGroupId);
                        }
                    }
                }
            } while(!groupIdStack.isEmpty());
            
            if(userIds.size() > 0) 
            {
                GroupUsers.put(groupId, userIds);
                for(Id userId : userIds) 
                {
                    if(!UserGroups.containsKey(userId)) 
                    {
                        UserGroups.put(userId, new Set<Id>());
                    }
                    UserGroups.get(userId).add(groupId);
                }
            }
        }
        for(Id userId : UserGroups.keySet()) 
        {
            if(!UserQueues.containsKey(userId)) 
            {
                UserQueues.put(userId, new Set<Id>());
            }
            for(Id groupId : UserGroups.get(userId)) 
            {
                grp = groups.get(groupId);
                if(grp.Type == 'Queue' && grp.QueueSobjects.size() > 0) 
                {
                    UserQueues.get(userId).addAll((new Map<Id, QueueSobject>(grp.QueueSobjects)).keySet());
                }
            }
        }
    }

    private static Set<ID> getParentRoleSet(List<UserRole> roles, UserRole role)
    {
        Set<ID> roleIds = new Set<ID>{role.ID};
        for(UserRole r : roles)
        {
            if(role.ParentRoleId == r.ID)
            {
                roleIds.add(r.ID);
                roleIds.addAll(getParentRoleSet(roles, r));

            }
        }
        return roleIds;
    }

    private static Group getGroup(String groupDeveloperName, Boolean isQueue)
    {
        for(Group g : [SELECT   Id, 
                                Name,                        
                                Email,
                                Type, 
                                DoesSendEmailToMembers,
                                DoesIncludeBosses, 
                                DeveloperName
                        FROM    Group
                        WHERE   DeveloperName = :groupDeveloperName AND
                                Type LIKE :(isQueue ? 'Queue' : '%')])
        {
            return g;
        }
        return null;
    }

    public static List<User> getUsersInRoles(List<String> roles)
    {
        return [SELECT  ID 
                FROM    User 
                WHERE   UserRole.Name IN :roles AND 
                        IsActive = true];
    }

    public static Set<ID> getUserIdsInGroup(String groupDeveloperName)
    {
        Group g = getGroup(groupDeveloperName);
        if(g == null)
        {
            return new Set<ID>();
        }
        getUserGroups();
        return GroupUsers.get(g.ID);
    }

    // Returns a List of the Ids of all RecordTypes
    // available to the running user for a given SOBject type
    public static List<ID> GetAvailableRecordTypeIdsForSObject(Schema.SObjectType objType) {
        List<ID> Ids = new List<ID>();
        List<RecordTypeInfo> infos = objType.getDescribe().getRecordTypeInfos();
        // If there are 2 or more RecordTypes...
        if (infos.size() > 1) {
            for (RecordTypeInfo i : infos) {
               if (i.isAvailable() 
               // Ignore the Master Record Type, whose Id always ends with 'AAA'.
               // We check the Id because Name can change depending on the user's language.
                && !String.valueOf(i.getRecordTypeId()).endsWith('AAA'))
                    Ids.add(i.getRecordTypeId());
            }
        } 
        // Otherwise there's just the Master record type,
        // so add it in, since it MUST always be available
        else Ids.add(infos[0].getRecordTypeId());
        return Ids;
    }
    
    

    @ISTEst
    private static void test1()
    {
        system.debug(UserUtilities.getUser(UserInfo.getUserID()));
        
        for(PermissionSet p : [SELECT Name FROM PermissionSet LIMIT 1])
        {
            system.debug(UserUtilities.isUserInPermissionSet(p.Name, UserInfo.getUserID(), true));
            system.debug(UserUtilities.isUserInPermissionSet(p.Name, UserInfo.getUserID(), false));
        }
        for(UserRole r : [SELECT ID, DeveloperName FROM UserRole LIMIT 1])
        {
            system.debug(UserUtilities.isUserInRole(r.DeveloperName, UserInfo.getUserID(), true));
            system.debug(UserUtilities.isUserInRoleById(r.ID, UserInfo.getUserID(), false));
        }
        
        for(Group g : [SELECT ID,  Name FROM Group LIMIT 1])
        {
            system.debug(UserUtilities.isUserInGroup(g.Name, UserInfo.getUserID()));
            system.debug(UserUtilities.isUserInGroupbyId(g.ID, UserInfo.getUserID()));
        }
        
        //system.debug(UserUtilities.getA2HCOfficer(true, null, null, null));
    }
}