public without sharing class PortalRequestExtension extends PageControllerBase
{
    public PortalRequestExtension(ApexPages.StandardController controller)
    {
        SelectedUserType = 'New User';
    }
    
    transient Attachment theFile = new Attachment();

    public String RecordType
    { 
        get
        {
            if (RecordType == null)
            {
                RecordType = ApexPages.CurrentPage().getParameters().get('rt') != null ? ApexPages.CurrentPage().getParameters().get('rt') : 'RCR';                            
            }
            return RecordType;
        }
        set;
    }
    
    public List<SelectOption> UserTypes 
    {
    	get
    	{
    		if(UserTypes == null)
    		{
    			UserTypes = new List<SelectOption>();
    			UserTypes.add(new SelectOption('New User', 'New User'));
    			UserTypes.add(new SelectOption('Existing User', 'Existing User'));
    		}
    		return UserTypes;
    	}
    	private set;
    }
    
    public string SelectedUserType
    {
    	get;
    	set;
    }
    
    
    public string UserName
    {
    	get;
    	set
    	{
    		RequestUserForm = null;
    		UserName = value;
    	}
    }
    
    
    public boolean CSAAuthoriser
    {
    	get;
    	set;
    }
    
    
    public Attachment file
    {
        get
        {
        	if(theFile == null)
        	{
        		theFile = new Attachment();
        	}
            return theFile;
        }
        set
        {
        	theFile = value == null ? new Attachment() : value;	
        }
    }
    
    
    public Portal_Request_User_Form__c RequestUserForm
    {
    	get
    	{
    		if(RequestUserForm == null)
    		{
    			
    			RequestUserForm = new Portal_Request_User_Form__c();
    			
    			if(SelectedUserType == 'Existing User')
    			{
    				if(!string.isBlank(UserName))
    				{
	    				for(Portal_Request_User_Form__c r : [SELECT	p.User__c, 
	        													p.Status__c, 
	        													p.RecordTypeId, 
	        													p.Phone_Number__c, 
	        													p.Organisation_Name__c, 
	        													p.New_User__c, 
	        													p.Name, 
	        													p.Last_Name__c, 
	        													p.First_Name__c, 
	        													p.Email__c, 
	        													p.CSA_Authoriser__c, 
	        													p.Account__c 
	        											 FROM	Portal_Request_User_Form__c p
	        											 WHERE	p.Username__c = :UserName
	        											 LIMIT	1])
		        		{
		        			RequestUserForm = r;
		        		}
    				}
    			}
    		}
    		return RequestUserForm;
    	} 
    	private set;
    }
    

    public PageReference saveOverride()
    {
        try
        {
  	
        	if((SelectedUserType == 'Existing User' && !CSAAuthoriser) ||
        	   (SelectedUserType == 'Existing User' && CSAAuthoriser && (file.name == '' || file.name == null)))
        	{
        		return A2HCException.formatException('To update an existing user with CSA Authorisation approval you must tick the CSA Authoriser checkbox and provide the CSA Authorisation Letter.');
        	}
        	
        	if(SelectedUserType == 'Existing User' && RequestUserForm.Id == null)
        	{     		
        		return A2HCException.formatException('This User Name was not found. Please check your user name or create a new user.');
        	}

            RequestUserForm.RecordTypeId = [SELECT Id
	                                       FROM RecordType
	                                       WHERE SObjectType = 'Portal_Request_User_Form__c'
	                                       AND Name = :RecordType].Id;            
	                                       
            RequestUserForm.New_User__c = SelectedUserType == 'New User' ? true : false;
            RequestUserForm.CSA_Authoriser__c = CSAAuthoriser ? true : false;
            RequestUserForm.Status__c = 'Submitted'; 
   
            upsert RequestUserForm;
            //cont.save();
            
            // If there is a file link it to this Portal Request User Form record
            // and insert.
            if(file.Name != Null)
            {
	            file.ParentId = RequestUserForm.Id;
	            insert file;
            }

            try
            {
                Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
                request.setObjectId(RequestUserForm.Id);
                Approval.process(request);
            }
            catch(Exception ex)
            {
            	if(ex.getMessage().contains('Cannot submit object already in process'))
            	{
            		throw new A2HCException('Record is pending approval and cannot be submitted again at this time.');
            	}
            	return A2HCException.formatException(ex);
            }

            PageReference pg = Page.PortalRequestSuccess;
            return pg.setRedirect(true);
        }
        catch(Exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }
 
    
    public void refresh()
    {
    	RequestUserForm = null;
    }
    
    
    @IsTest(SeeAllData=true)
    static void myUnitTest()
    {
    	for(Portal_Request_User_Form__c r : [SELECT	p.User__c, 
													p.Status__c, 
													p.RecordTypeId, 
													p.Phone_Number__c, 
													p.Organisation_Name__c, 
													p.New_User__c, 
													p.Name, 
													p.Last_Name__c, 
													p.First_Name__c, 
													p.Email__c, 
													p.CSA_Authoriser__c, 
													p.Account__c 
											 FROM	Portal_Request_User_Form__c p
											 LIMIT	10])
		{
			PortalRequestExtension ext = new PortalRequestExtension(new ApexPages.Standardcontroller(r));
			ext.RequestUserForm = r;
			system.debug(ext.UserTypes);
			system.debug(ext.file);
			ext.SelectedUserType = 'Existing User';
			system.debug(ext.SelectedUserType);
			ext.UserName = r.Email__c;
			system.debug(ext.UserName);
			system.debug(ext.RecordType);
			system.debug(ext.RequestUserForm);
			ext.saveOverride();
			ext.refresh();
		}
    }
}