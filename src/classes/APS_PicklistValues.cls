public class APS_PicklistValues
{
    public static String Profile_CompanionCard = 'Companion Card';
    public static String Profile_RCRPortal= 'RCR Portal';
    public static String Profile_RCRPortalNonSubmit= 'RCR Portal (Non Submit)';

    public static String Role_RCRBrokerageOfficer = 'RCR Brokerage Officer';

    public static String Account_DCSI_RCR_Programs = 'DCSI - RCR Programs';
    public static String Account_A2HC = 'A2HC';
    public static String Account_CompanionCard = 'Companion Card';
    public static String Account_DCSI_IF = 'DCSI IF Account';

    public static String NeedsIdentification_NeedMeals_Other2 = 'Other';

    public static String NeedsIdentification_CarerAvailability_HasOneOrMoreCarers = 'Has one or more carers';
    public static String NeedsIdentification_CarerAvailability_HasNoCarers = 'Has no carers';

    public static String NeedsIdentification_ClosedDuringOrFollowing_ANINeedsIdentification = 'ANI (Needs Identification)';
    public static String NeedsIdentification_ClosedDuringOrFollowing_Registration = 'Registration';

    public static String NeedsIdentification_CanYouWalk_WithSomeHelp = 'With some help';

    public static String NeedsIdentification_DurationAlone_ForSomeButNotAllTime = 'For some, but not all of time';
    public static String NeedsIdentification_DurationAlone_NotAtAllConstantSupervision = 'Not at all, needs constant supervision';

    public static String Contact_AccommodationSetting_PublicPlaceTemporaryShelter = 'Public place/temporary shelter';

    public static String eReferral_AcceptanceStatus_Withdrawn = 'Withdrawn';

    public static String RCRCBMSContract_ContractType_Recurrent = 'Recurrent';

    public static String Consent_Status_Active = 'Active';
    public static String Consent_Status_Closed = 'Closed';

    public static String PEF_Status_Active = 'Active';
    public static String PEF_Status_Closed = 'Closed';

    public static String RestrictivePractice_Status_Active = 'Active';
    public static String RestrictivePractice_Status_Closed = 'Closed';

    public static String RCRGroupAgreement_GrantPaidVia_RCR = 'RCR';
    
    public static String RCRGroupAgreement_RolloverStatus_Review = 'Review';

    public static string RCRRegion_Name_IFStage2 = 'IF Stage 2';

    // RCR Service Order
    public static String RCRContractCSAStatus_Pending_Approval = 'Pending Approval';
    public static String RCRContractCSAStatus_Pending_Acceptance = 'Pending Acceptance';
    public static String RCRContractCSAStatus_Rejected = 'Rejected';
    public static String RCRContractCSAStatus_Accepted = 'Accepted';

    public static String RCRContract_Status_PendingApproval = 'Pending Approval';
    public static String RCRContract_Status_AllocationPendingApproval = 'Allocation Pending Approval';
    public static String RCRContract_Status_AllocationApproved = 'Allocation - Approved';
    public static String RCRContract_Status_AllocationRejected = 'Allocation - Rejected';
    public static String RCRContract_Status_New = 'New';
    public static String RCRContract_Status_Approved = 'Approved';
    public static String RCRContract_Status_PendingCancellation = 'Pending Cancellation';
    public static String RCRContract_Status_Rejected = 'Rejected';
    public static String RCRContract_Status_CBMSApproved = 'CBMS Approved';
    // the following arenot picklist values as such but is set programatically
    public static String RCRContract_Status_PendingAcceptance = 'Pending Acceptance';
    public static String RCRContract_Status_ParticipationPlanAccepted = 'Participation Plan Accepted';

    public static String RCRContract_FundingCommitment_OnceOff = 'Once Off';
    public static String RCRContract_FundingCommitment_Recurrent = 'Recurrent';

    public static String RCRContract_FundingSource_RegionalBudget ='Regional Budget';
    public static String RCRContract_FundingSource_PTIC = 'PTIC';
    public static String RCRContract_FundingSource_UseOfAllocation = 'Use of Allocation';
    public static String RCRContract_FundingSource_Rollover = 'Rollover';
    public static String RCRContract_FundingSource_DayOptions = 'Day Options';
    public static String RCRContract_FundingSource_DirectContract = 'Direct Contract';

    public static String RCRContract_RolloverStatus_ReadyForAllocation ='Ready for Approval';
    public static String RCRContract_RolloverStatus_Review ='Review';
    public static String RCRContract_RolloverStatus_RolloverNotRequired ='Rollover Not required';
    public static String RCRContract_RolloverStatus_PendingBrokerageReview = 'Pending Brokerage Review';
    public static String RCRContract_RolloverStatus_BrokerageReviewed = 'Brokerage Reviewed';
    public static String RCRContract_RolloverStatus_PendingCoordReview = 'Pending Coordinator Review';
    public static String RCRContract_RolloverStatus_PendingAdjustment = 'Pending Adjustment';
    public static String RCRContract_RolloverStatus_Complete ='Approved';
    public static String RCRContract_RolloverStatus_Queued ='Queued';
    public static String RCRContract_RolloverStatus_BulkApproved = 'Bulk Approved';
    public static String RCRContract_RolloverStatus_BulkRejected = 'Bulk Rejected';
    public static String RCRContract_RolloverStatus_Rejected ='Rejected';
    public static String RCRContract_RolloverStatus_PendingApproval = 'Pending Approval';

    public static String RCRContract_Recommendation_Approve = 'Approve';
    public static String RCRContract_Recommendation_ApproveWithAmendments = 'Approve with Amendments';
    public static String RCRContract_Recommendation_NotToApprove = 'Not to Approve';

    public static string RCRContract_RolloverRecommend_ApproveOnceOff = 'Approve Once Off';
    public static string RCRContract_RolloverRecommend_ApproveRecurrent = 'Approve Recurrent';
    public static string RCRContract_RolloverRecommend_ReduceServices = 'Reduce Services';
    public static string RCRContract_RolloverRecommend_IdentifyLowerCostProv = 'Identify Lower Cost Provider';

    public static string RCRContract_Type_IndividualisedFunding = 'Individualised Funding';
    public static string RCRContract_Type_Brokerage = 'Brokerage';
    public static string RCRContract_Type_DirectContract = 'Direct Contract';
    //public static string RCRContract_Type_IFStage2 = 'IF Stage 2';

    public static String RCRContract_IFStage_Stage1 = 'Stage 1';
    public static String RCRContract_IFStage_Stage2 = 'Stage 2';
    public static String RCRContract_IFStage_Stage3 = 'Stage 3';

    public static string RCRContract_NewRequestType_Rollover = 'Rollover';

    // RCR Scheduled Service Flexibility Options
    public static String RCRContractScheduledService_Flexibilty_WithinThePeriod = 'Flexible within a Period';
    public static String RCRContractScheduledService_Flexibilty_AcrossTheSucceedingPeriod = 'Flexible across a Period';
    public static String RCRContractScheduledService_Flexibilty_Accumulative = 'Flexible across Scheduled Service Date Range';

    // RCR Invoice Statuses
    public static String RCRInvoice_Status_Loaded = 'Loaded';
    public static String RCRInvoice_Status_ReconciliationInProgress = 'Reconciliation In Progress';
    public static String RCRInvoice_Status_ReconciledErrors = 'Reconciled - Errors';
    public static String RCRInvoice_Status_ReconciledNoErrors = 'Reconciled - No Errors';
    public static String RCRInvoice_Status_PendingApproval = 'Pending Approval';
    public static String RCRInvoice_Status_Approved = 'Approved';
    public static String RCRInvoice_Status_PaymentProcessed = 'Payment Processed';

    // RCR Invoice Item Statuses
    public static String RCRInvoiceItem_Status_Loaded = 'Loaded';
    public static String RCRInvoiceItem_Status_PendingApproval = 'Pending Approval';
    public static String RCRInvoiceItem_Status_Approved = 'Approved';
    public static String RCRInvoiceItem_Status_Rejected = 'Rejected';

    // RCR Public Holiday Options
    public static String RCRContractScheduledService_PublicHolidaysNotAllowed_UseStandardRates = 'No, normal scheduled services are not contracted on Public Holidays';
    public static String RCRContractScheduledService_PublicHolidaysNotAllowed_UseNoPublicHolidays = 'No, Public Holidays are excluded';
    public static String RCRContractScheduledService_PublicHolidaysNotAllowed_UsePublicHolidayRates = 'Yes, normal scheduled services continue and are paid at Public Holiday Rates';
    public static String RCRContractScheduledService_PublicHolidaysNotAllowed_UseExceptions = 'Special arrangements have been agreed for Public holidays or other days, replacing normal schedule services';

    public static String Schedule_WeeksInPeriod_ServiceDateRange = 'Service Date Range';
    public static String Schedule_WeeksInPeriod_1Week = '1 Week';
    public static String Schedule_WeeksInPeriod_2Week = '2 Weeks';
    public static String Schedule_WeeksInPeriod_3Week = '3 Weeks';
    public static String Schedule_WeeksInPeriod_4Week = '4 Weeks';
    public static String Schedule_WeeksInPeriod_Monthly = 'Monthly';


    public static String Note_ClientDetailsReport = 'Client Details Report';

    public static String ServiceQuery_Status_Lodged = 'Lodged';
    public static String ServiceQuery_Status_InProgress = 'In Progress';

    // Individualised Funding
    public static String IFAllocation_Status_New = 'New';
    public static String IFAllocation_Status_Approved = 'Approved';
    public static String IFAllocation_Status_PendingApproval = 'Pending Approval';
    public static String IFAllocation_Status_Rejected = 'Rejected';
    public static String IFAllocation_Status_Archived = 'Archived';

    public static String IFAllocation_Type_Recurrent = 'Recurrent';
    public static String IFAllocation_Type_OnceOff = 'Once Off';

    public static String IFAllocation_Source_DayOptions = 'Day Options';
    public static String IFAllocation_Source_IndividualSupport = 'Individual Support';
    //public static String IFAllocation_Source_Regional = 'Regional';
    public static string IFAllocation_Source_PTICIndividualSupport = 'PTIC - Individual Support';
    public static String IFAllocation_Source_Regional = 'Regional - Individual Support';
    public static String IFAllocation_Source_IFStage2DayOptions = 'IF Stage 2 - Day Options';
    public static String IFAllocation_Source_IFStage2Other = 'IF Stage 2 - Other';
    public static string IFAllocation_Source_ENU = 'ENU';

    public static String IFAllocation_Reason_InitialAllocation = 'Initial Allocation';
    public static string IFAllocation_Reason_Increase = 'Increase';
    public static string IFAllocation_Reason_Decrease = 'Decrease';
    public static string IFAllocation_Reason_Adjustment = 'Adjustment to Initial Allocation';
    public static String IFAllocation_Reason_Rollover = 'Rollover';
    public static String IFAllocation_Reason_RolloverTopUp = 'Rollover Top Up';


    public static String IFAllocation_ApprovalCriteria_AutomaticApprove = 'Automatic Approve';
    public static String IFAllocation_ApprovalCriteria_AutomaticReject = 'Automatic Reject';
    public static String IFAllocation_ApprovalCriteria_BulkRollover = 'Bulk Rollover';

    public static String IFAllocation_IFStage_Stage1 = '1';
    public static String IFAllocation_IFStage_Stage2 = '2';
    public static String IFAllocation_IFStage_Stage3 = '3';

    public static String IF_PlanStatus_Draft = 'Draft';
    public static String IF_PlanStatus_RevbyClient = 'Review by Client';
    public static String IF_PlanStatus_RevbyFac = 'Review by Facilitator';
    public static String IF_PlanStatus_RevbyTeamLead = 'Review by Team Leader';
    public static String IF_PlanStatus_RevbyManger = 'Review by Manager';
    public static String IF_PlanStatus_RevbyDirector = 'Review by Director';
    public static String IF_PlanStatus_RevbyEnPanel = 'Review by Enablement Panel';
    public static String IF_PlanStatus_Approved = 'Approved';
    public static String IF_PlanStatus_NotApproved = 'Not Approved';
    public static String IF_PlanStatus_Cancelled = 'Cancelled';

    public static String IF_FacilitatorRecomended_Recommended = 'Recommended';
    public static String IF_FacilitatorRecomended_NotRecommended = 'Not Recommended';
    public static String IF_FacilitatorRecomended_RolloverApproval = 'Rollover Approval';

    public static String IF_TeamLeaderDecision_Approved = 'Approved (Amendments Only)';
    public static String IF_TeamLeaderDecision_Recommended = 'Recommended to Manager';
    public static String IF_TeamLeaderDecision_Referred = 'Refer to Manager';
    public static String IF_TeamLeaderDecision_Rejected = 'Rejected';

    public static String IF_ManagerApproval_Approved = 'Approved';
    public static String IF_ManagerApproval_Referred = 'Refer to Director';
    public static String IF_ManagerApproval_Rejected = 'Rejected';

    public static String IF_DirectorApproval_Approved = 'Approved';
    public static String IF_DirectorApproval_Referred = 'Refer to Enablement Panel';
    public static String IF_DirectorApproval_Rejected = 'Rejected';

    public static String IF_PlanItem_ServiceType_PerCare = 'Personal Care Support Services';
    public static String IF_PlanItem_ServiceType_General = 'General Support Services';
    public static String IF_PlanItem_ServiceType_Other = 'Other';

    public static String IF_Client_AdminType_Direct = 'Directly manage';
    public static String IF_Client_AdminType_HelpDirect = 'Someone to help you directly manage';
    public static String IF_Client_AdminType_Agency = 'An agency to help manage the budget';
    public static String IF_Client_AdminType_AgencyService = 'An agency to arrange the services and manage the budget';
    public static String IF_Client_AdminType_Gov = 'Leave everything as it is, Government to continue to arrange & pay for your services';

    public static String IF_Acquittal_Status_Draft = 'Draft';
    public static String IF_Acquittal_Status_Rejected = 'Rejected';
    public static String IF_Acquittal_Status_Submitted = 'Submitted';
    public static String IF_Acquittal_Status_Approved = 'Approved';

    public static String IF_Acquittal_Period_Qtr1 = 'Jul - Sep';
    public static String IF_Acquittal_Period_Qtr2 = 'Oct - Dec';
    public static String IF_Acquittal_Period_Qtr3 = 'Jan - Mar';
    public static String IF_Acquittal_Period_Qtr4 = 'Apr - Jun';
    public static String IF_Acquittal_Period_Annual = 'Annual';

    public static String RCRServiceOrderRateCalculation_Period_1Week = 'Weekly';
    public static String RCRServiceOrderRateCalculation_Period_2Weeks = 'Fortnightly';
    public static String RCRServiceOrderRateCalculation_Period_Monthly = 'Monthly';
    public static String RCRServiceOrderRateCalculation_Period_ServiceDateRange = 'Over Whole Contract';

    public static String RCRServiceOrderRateCalculation_RateType_OfficeHours = 'Weekday (before 6PM)';
    public static String RCRServiceOrderRateCalculation_RateType_AfterHours = 'Weekday (after 6PM)';
    public static String RCRServiceOrderRateCalculation_RateType_Saturday = 'Saturday';
    public static String RCRServiceOrderRateCalculation_RateType_Sunday = 'Sunday';
    public static String RCRServiceOrderRateCalculation_RateType_PassiveNights = 'Passive Nights';
    public static String RCRServiceOrderRateCalculation_RateType_OvernightCare = 'Overnight Care';

    public static String Task_Subject_RequestReviewBySeniorCoordinator = 'Request Review by Senior Coordinator';
    public static String Task_Subject_PleaseCompleteTheServiceRequest = 'Please complete the Service Request';

    public static String Task_Status_NotStarted = 'Not Started';
    public static String Task_Status_Completed = 'Completed';


    public static String PlanApproval_CEDecision_Approved = 'Approved';
    public static String PlanApproval_PlanUpdateRequired_Yes = 'Yes';

    public static String StaffAvailability_Status_Available = 'Available';

    public static String ParticipantPlan_PlanStatus_Approved = 'Approved';
    public static String ParticipantPlan_PlanStatus_Amendment = 'Amendment';

}