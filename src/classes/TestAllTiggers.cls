/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against 
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestAllTiggers 
{
    @IsTest//(SeeAllData=true)
    static void myUnitTest() 
    {
        TestLoadData.loadRCRData();
    
        Test.StartTest();
       
       Account a = [SELECT ID
                    FROM    Account 
                    LIMIT 1];
       a.Name = 'test';
       upsert a;

       RCR_Invoice__c inv = new RCR_Invoice__c(Account__c = a.ID, Invoice_Number__c = 'test');
       insert inv;
       
       RCR_Invoice_Item__c item = new  RCR_Invoice_Item__c(RCR_Invoice__c = inv.ID);
       insert item;
       
       item.Total__c = 2;
       update item;
       
       delete item;
       
       delete inv;
       /*
       RCR_CBMS_Contract__c cbmsContract = [SELECT ID,
                                                    End_Date__c
                                            FROM    RCR_CBMS_Contract__c
                                            WHERE   //Cancellation_Date__c != null AND
                                                    ID IN (SELECT RCR_CBMS_Contract__c
                                                            FROM    RCR_Contract__c
                                                            WHERE   Total_Scheduled_Service_Cost__c > 0)
                                            LIMIT 1];
        cbmsContract.Cancellation_Date__c = cbmsContract.End_Date__c.addDays(-10);
        update  cbmsContract;
        */
        try
        {
            RCR_Contract_Adhoc_Service__c ahs = [SELECT ID
                                                FROM    RCR_Contract_Adhoc_Service__c
                                                LIMIT 1];
            delete ahs;  
        }
        catch(Exception ex)
        {}
        
        try
        {
            RCR_Contract_Scheduled_Service__c css = [SELECT ID
                                                FROM    RCR_Contract_Scheduled_Service__c
                                                LIMIT 1];
            delete css;  
        }
        catch(Exception ex)
        {}  
        
        Service_Query__c sq = new Service_Query__c();
        insert sq;
        
        sq.Contact_Name__c = 'test';
        update sq;  
        
        delete [SELECT ID
                FROM    RCR_Service_Rate__c
                WHERE   RCR_Service__c NOT IN (SELECT RCR_Service__c
                                                FROM    RCR_Contract_Scheduled_Service__c)
                LIMIT 1];   
           /*     
        delete [SELECT ID
                FROM    RCR_Contract__c
                WHERE   ID NOT IN (SELECT RCR_Service_Order__c
                                    FROM    RCR_Invoice_Item__c)
                AND     ID NOT IN (SELECT RCR_Service_Order__c
                                    FROM IF_Personal_Budget__c)
                AND     RecordType.Name != :APSRecordTypes.RCRServiceOrder_Rollover
                LIMIT 1];  
             */   
        for(User u : [SELECT ID
                    FROM    User
                    LIMIT 1])  
        {
            update u;
        }       
       
       RCR_Contract__c rcrc1 = [SELECT Id
                               FROM RCR_Contract__c
                               LIMIT 1];  
       RCR_Contract__c rcrc2 = rcrc1.clone(false, true);                  
       insert rcrc2;                                           
       delete rcrc2;
       
       /*IFClientPortalRequest__c req = new IFClientPortalRequest__c();
       insert req;
       update req;*/
       
//       RCR_CBMS_Contract__c cbms = new RCR_CBMS_Contract__c();
//       insert cbms;
//       update cbms;
          
         Test.StopTest();                                      
    }
    
    @IsTest//(SeeAllData=true)
    static void test2() 
    {
        TestLoadData.loadRCRData();
    
        Test.StartTest();
    
        User u = [SELECT ID
                        FROM    User
                        /*WHERE   IsActive = true AND
                                Profile.Name = 'RCR DCSI User'*/
                        LIMIT 1];
                                
        RCR_Directorate__c d = new RCR_Directorate__c(Name = 'test');
        insert d;
        
        d.Director__c = u.ID;
        update d;
        
        RCR_Region__c r = new RCR_Region__c(Name = 'test', RCR_Directorate__c = d.ID);
        insert r;
        
        r.Regional_Manager__c = u.ID;
        update r;
        
        RCR_Office__c regOff = new RCR_Office__c(Name = 'test', RCR_Region__c = r.ID);
        insert regOff;
        
        RCR_Office_Team__c regOffTeam = new RCR_Office_Team__c(Name = 'test', RCR_Office__c = regOff.ID);
        insert regOffTeam;
        
        regOffTeam.Team_Manager__c = u.ID;
        update regOffTeam;
        
        RCR_Contract__c so = TestRCR.getTestContract();
        
        RCR_Group_Agreement__c ga = new RCR_Group_Agreement__c();
        ga.Account__c = so.Account__c;
        ga.Grant_Paid_Via__c = 'Test';
        ga.Grant_Amount__c = 10.0;
        ga.Start_Date__c = so.Start_Date__c;
        ga.End_Date__c =so.End_Date__c;
        insert ga;
        
        TriggerBypass.RCRServiceOrder = true;
        so.RCR_Group_Agreement__c = ga.id;
        ga.Grant_Amount__c = 11.0;
        update ga;
        
        so.RecordTypeId = A2HCUtilities.getRecordType('RCR_Contract__c', APSRecordTypes.RCRServiceOrder_NewRequest).ID;
        so.Other_Service_Provider_Considered_1__c = [SELECT Id FROM Account LIMIT 1].Id;
        so.Other_Service_Provider_Considered_2__c = [SELECT Id FROM Account LIMIT 1].Id;
        update so;
        update ga;
        
        so.RecordTypeId = A2HCUtilities.getRecordType('RCR_Contract__c', APSRecordTypes.RCRServiceOrder_Amendment).ID;
        update so;
        update ga;
        
        Test.StopTest();
    }

    @IsTest
    static void test3()
    {

        Referred_Client__c Client = new Referred_Client__c(Given_Name__c = 'Test',
                Family_Name__c = 'Test',
                Date_of_Birth__c = Date.today(),
                Address__c = 'Test',
                Suburb__c = 'Test',
                State__c = 'Test',
                Postcode__c = 'Test',
                Phone__c = '012345678',
                SAPOL_Vehicle_Collision_Report_Number__c = 'Test',
                Time_of_accident_hours__c = '13',
                Time_of_accident_minutes__c = '00',
                Date_of_accident__c = Date.today(),
                Allocated_to__c = UserInfo.getUserId());
        insert Client;

        Plan__c p = new Plan__c();
        p.Client__c = Client.Id;
        p.RecordTypeId = SObjectType.Plan__c.getRecordTypeInfosByName().get('MyPlan').RecordTypeId;
        p.Plan_Status__c = 'Draft';
        insert p;

        Plan_Approval__c plApp = new Plan_Approval__c(Participant_Plan__c = p.id);
        insert plApp;

        Plan_Action__c pa = new Plan_Action__c(Participant_Plan__c = p.id, Plan_Approval__c = plApp.ID);
        insert pa;

        delete plApp;
        
        WHOQOL_Questionnaire__c q = new WHOQOL_Questionnaire__c(Client__c = Client.ID);
        insert q;
        
        //update q;
        //delete q;
    }




}