public virtual class RCRClientSummary extends FinancialYearAllocation
{
    private transient List<RCR_Contract__c> crntServiceOrders;
    private transient List<RCR_Contract__c> expdServiceOrders;
    private transient List<RCR_Contract__c> crntSORequests;
    private transient Map<string, List<RCR_Contract__c>> relatedList;
    //private transient Map<string, List<RCR_>> relatedScheduledServices;
    private Integer recCountSO = 0;
    private Integer recCountExpiredSO = 0;
    private Integer recCountR = 0;

    public RCRClientSummary(ApexPages.StandardController cntrl)
    {
        super(cntrl, 100);
        controller = cntrl;
        serviceOrder.Client__c = controller.getID();
    }

    private ApexPages.StandardController controller;

    public Integer RecordsCountSO
    {
        get {
            return recCountSO;
        }
    }

    public Integer RecordsCountExpiredSO
    {
        get {
            return recCountExpiredSO;
        }
    }

    public Integer RecordsCountR
    {
        get {
            return recCountR;
        }
    }

    public List<RCR_Contract__c> CurrentServiceOrders
    {
        get
        {
            crntServiceOrders = new List<RCR_Contract__c>();
            Integer offst = QueryOffset;

            Map<Id, RCR_Contract__c> serviceOrders = new Map<Id, RCR_Contract__c>();
            if (getParameter('planId') != null)
            {
                serviceOrders = new Map<Id, RCR_Contract__c> ([SELECT Id
                FROM RCR_Contract__c
                WHERE Plan_Action__r.Participant_Plan__c = :getParameter('planId')]);

            }
            else if (getParameter('planApprovalId') != null)
            {
                serviceOrders = new Map<Id, RCR_Contract__c> ([SELECT Id
                FROM RCR_Contract__c
                WHERE Plan_Action__r.Plan_Approval__c = :getParameter('planApprovalId')]);

            }
            else
            {
                serviceOrders = new Map<Id, RCR_Contract__c> ([SELECT Id
                FROM RCR_Contract__c
                WHERE   Client__c = :controller.getID()
                AND     RecordType.Name IN (:APSRecordTypes.RCRServiceOrder_ServiceOrder,:APSRecordTypes.RCRServiceOrder_ServiceOrderCBMS)
                AND Status__c = 'Approved'
                AND Start_Date__c <= :Date.today()
                AND End_Date__c >= :Date.today()
                AND Created_by_Adhoc_Payment__c = false]);
            }

            TotalRecords = [SELECT  COUNT()
            FROM    RCR_Contract__c
            WHERE   Client__c = :controller.getID()
            AND     Id IN :serviceOrders.keySet()
            AND     RecordType.Name IN (:APSRecordTypes.RCRServiceOrder_ServiceOrder,:APSRecordTypes.RCRServiceOrder_ServiceOrderCBMS)
            AND Status__c = 'Approved'
            AND Start_Date__c <= :Date.today()
            AND End_Date__c >= :Date.today()
            AND Created_by_Adhoc_Payment__c = false];

            recCountSO = TotalRecords;

            crntServiceOrders  = [SELECT    ID,
                    Name,
                    Client__c,
                    Status__c,
                    Request_Amount__c,
                    Start_Date__c,
                    End_Date__c,
                    CSA_Status__c,
                    Level__c,
                    Total_Service_Order_Cost__c,
                    Total_Submitted__c,
                    RecordTypeID,
                    RecordType.Name,
                    Account__r.Name,
                    Original_End_Date__c,
                    Cancellation_Date__c,
                    Funding_Source__c,
                    Account__c,
                    Source_Record__c,
                    Destination_Record__c,
                    Plan_Action__r.Participant_Plan__r.Name,
                    Plan_Action__r.Name,
                    Plan_Action__r.Participant_Plan__c,
                    Plan_Action__c,
                    Plan_Action__r.Approved__c,
            (SELECT  ID,
                    Name,
                    Service_Code__c,
                    Service_Description__c,
                    Service_Types__c,
                    Start_Date__c,
                    End_Date__c,
                    Total_Cost__c,
                    Total_Submitted__c,
                    Remaining__c,
                    Standard_Service_Code__c
            FROM    RCR_Contract_Scheduled_Services__r)
            FROM    RCR_Contract__c
            WHERE   Client__c = :controller.getID()
            AND     Id IN :serviceOrders.keySet()
            AND     Client__c != NULL
            AND RecordType.Name IN (:APSRecordTypes.RCRServiceOrder_ServiceOrder,:APSRecordTypes.RCRServiceOrder_ServiceOrderCBMS)
            AND Status__c = 'Approved'
            AND Start_Date__c <= :Date.today()
            AND End_Date__c >= :Date.today()
            AND Created_by_Adhoc_Payment__c = false
            ORDER BY End_Date__c DESC
            LIMIT  :PageSize
            OFFSET  :offst];

            relatedList = new Map<string, List<RCR_Contract__c>>();
            for (RCR_Contract__c so :  crntServiceOrders)
            {
                if(!relatedList.containsKey(so.ID))
                {
                    relatedList.put(so.ID, new List<RCR_Contract__c>());
                }
                relatedList.get(so.ID).add(so);
            }

            return crntServiceOrders;
        }
        private set;
    }

    private Map<String, LPMemberContractsJSON.cls_lpcontracts> LPContractsByLPContractNumber
    {
        get
        {
            if(LPContractsByLPContractNumber == null)
            {
                LPRESTInterface lpInterface = new LPRESTInterface();
                LPContractsByLPContractNumber = new Map<String, LPMemberContractsJSON.cls_lpcontracts>();
                Referred_Client__c client = (Referred_Client__c)controller.getRecord();
                if(client.LP_Member_Key__c != null && client.LP_Member_Date__c != null)
                {
                    for(LPMemberContractsJSON.cls_lpcontracts lpContract : lpInterface.getMemberContracts((Referred_Client__c) controller.getRecord()))
                    {
                        LPContractsByLPContractNumber.put(lpContract.contractNumber, lpContract);
                    }
                }
            }
            return LPContractsByLPContractNumber;
        }
        set;
    }

    public Map<String, LPMemberContractsJSON.cls_lpcontracts> LPContractsBySOName
    {
        get
        {
            if(LPContractsBySOName == null)
            {
                LPContractsBySOName = getLPContractMap(CurrentServiceOrders);
            }
            return LPContractsBySOName;
        }
        private set;
    }

    public Map<String, LPMemberContractsJSON.cls_lpcontracts> LPExpContractsBySOName
    {
        get
        {
            if(LPExpContractsBySOName == null)
            {
                LPExpContractsBySOName = getLPContractMap(ExpiredServiceOrders);
            }
            return LPExpContractsBySOName;
        }
        private set;
    }

    private Map<String, LPMemberContractsJSON.cls_lpcontracts> getLPContractMap(List<RCR_Contract__c> serviceOrders)
    {
        Map<String, LPMemberContractsJSON.cls_lpcontracts> lpContractMap = new Map<String, LPMemberContractsJSON.cls_lpcontracts>();
        for(RCR_Contract__c so : serviceOrders)
        {
            if(LPContractsByLPContractNumber.containsKey((so.Name)))
            {
                lpContractMap.put(so.Name, LPContractsByLPContractNumber.get(so.Name));
            }
            else
            {
                LPMemberContractsJSON.cls_lpcontracts lpContract = new LPMemberContractsJSON.cls_lpcontracts();
                lpContract.preauthorizations = new List<LPMemberContractsJSON.cls_preauthorizations>();
                lpContract.budget = new LPMemberContractsJSON.cls_budget();
                lpContractMap.put(so.Name, lpContract);
            }
        }
        return lpContractMap;
    }

    public Map<String, LPMemberContractsJSON.cls_preauthorizations> LPPreAuthorisationsByCSSName
    {
        get
        {
            if(LPPreAuthorisationsByCSSName == null)
            {
                LPPreAuthorisationsByCSSName = getLPPreAuthorisations(CurrentServiceOrders);
            }
            return LPPreAuthorisationsByCSSName;
        }
        private set;
    }

    public Map<String, LPMemberContractsJSON.cls_preauthorizations> LPExpPreAuthorisationsByCSSName
    {
        get
        {
            if(LPExpPreAuthorisationsByCSSName == null)
            {
                LPExpPreAuthorisationsByCSSName = getLPPreAuthorisations(ExpiredServiceOrders);
            }
            return LPExpPreAuthorisationsByCSSName;
        }
        private set;
    }

    private Map<String, LPMemberContractsJSON.cls_preauthorizations> getLPPreAuthorisations(List<RCR_Contract__c> serviceOrders)
    {
        Map<String, LPMemberContractsJSON.cls_preauthorizations> lpPreAuthByCssName = new Map<String, LPMemberContractsJSON.cls_preauthorizations>();
        for(RCR_Contract__c so : serviceOrders)
        {
            LPMemberContractsJSON.cls_lpcontracts lpContract = LPContractsBySOName.get(so.Name);
            for(RCR_Contract_Scheduled_Service__c css : so.RCR_Contract_Scheduled_Services__r)
            {
                if(lpContract != null && lpContract.preauthorizations != null)
                {
                    for(LPMemberContractsJSON.cls_preauthorizations lpPreAuth : lpContract.preauthorizations)
                    {
                        if(lpPreAuth.preauthorizationNumber == css.Name)
                        {
                            lpPreAuthByCssName.put(css.Name, lpPreAuth);
                            break;
                        }
                    }
                }
                if(!lpPreAuthByCssName.containsKey(css.Name))
                {
                    LPMemberContractsJSON.cls_preauthorizations tempPreAuth = new LPMemberContractsJSON.cls_preauthorizations();
                    tempPreAuth.preauthorizationNumber = css.Name;
                    tempPreAuth.budget = new LPMemberContractsJSON.cls_budget();
                    tempPreAuth.item = new LPMemberContractsJSON.cls_item();
                    lpPreAuthByCssName.put(css.Name, tempPreAuth);
                }
            }
        }
        return lpPreAuthByCssName;
    }

    public List<RCR_Contract__c> ExpiredServiceOrders
    {
        get
        {
            ExpiredServiceOrders = new List<RCR_Contract__c>();
            Integer offst = QueryOffset;

            Map<Id, RCR_Contract__c> serviceOrders = new Map<Id, RCR_Contract__c>();
            if (getParameter('planId') != null)
            {
                serviceOrders = new Map<Id, RCR_Contract__c> ([SELECT Id
                FROM RCR_Contract__c
                WHERE Plan_Action__r.Participant_Plan__c = :getParameter('planId')]);

            }
            else if (getParameter('planApprovalId') != null)
            {
                serviceOrders = new Map<Id, RCR_Contract__c> ([SELECT Id
                FROM RCR_Contract__c
                WHERE Plan_Action__r.Plan_Approval__c = :getParameter('planApprovalId')]);

            }
            else
            {
                serviceOrders = new Map<Id, RCR_Contract__c> ([SELECT Id
                FROM RCR_Contract__c
                WHERE   Client__c = :controller.getID()
                AND     RecordType.Name IN (:APSRecordTypes.RCRServiceOrder_ServiceOrder,:APSRecordTypes.RCRServiceOrder_ServiceOrderCBMS)
                AND (Status__c != 'Approved'
                OR Start_Date__c > :Date.today()
                OR End_Date__c < :Date.today())
                AND Created_by_Adhoc_Payment__c = false]);
            }

            TotalRecords = [SELECT  COUNT()
            FROM    RCR_Contract__c
            WHERE   Client__c = :controller.getID()
            AND     Id IN :serviceOrders.keySet()
            AND     RecordType.Name IN (:APSRecordTypes.RCRServiceOrder_ServiceOrder,:APSRecordTypes.RCRServiceOrder_ServiceOrderCBMS)
            AND (Status__c != 'Approved'
            OR Start_Date__c > :Date.today()
            OR End_Date__c < :Date.today())
            AND Created_by_Adhoc_Payment__c = false];

            recCountExpiredSO = TotalRecords;

            expdServiceOrders = [SELECT    ID,
                    Name,
                    Client__c,
                    Status__c,
                    Request_Amount__c,
                    Start_Date__c,
                    End_Date__c,
                    CSA_Status__c,
                    Level__c,
                    Total_Service_Order_Cost__c,
                    Total_Submitted__c,
                    RecordTypeID,
                    RecordType.Name,
                    Account__r.Name,
                    Original_End_Date__c,
                    Cancellation_Date__c,
                    Funding_Source__c,
                    Account__c,
                    Source_Record__c,
                    Destination_Record__c,
                    Plan_Action__r.Participant_Plan__r.Name,
                    Plan_Action__r.Name,
                    Plan_Action__r.Participant_Plan__c,
                    Plan_Action__c,
                    Plan_Action__r.Approved__c,
            (SELECT  ID,
                    Name,
                    Service_Code__c,
                    Service_Description__c,
                    Service_Types__c,
                    Start_Date__c,
                    End_Date__c,
                    Total_Cost__c,
                    Total_Submitted__c,
                    Remaining__c,
                    Standard_Service_Code__c
            FROM    RCR_Contract_Scheduled_Services__r)
            FROM    RCR_Contract__c
            WHERE   Client__c = :controller.getID()
            AND     Id IN :serviceOrders.keySet()
            AND     Client__c != NULL
            AND RecordType.Name IN (:APSRecordTypes.RCRServiceOrder_ServiceOrder,:APSRecordTypes.RCRServiceOrder_ServiceOrderCBMS)
            AND (Status__c != 'Approved'
            OR Start_Date__c > :Date.today()
            OR End_Date__c < :Date.today())
            AND Created_by_Adhoc_Payment__c = false
            ORDER BY End_Date__c DESC
            LIMIT  :PageSize
            OFFSET  :offst];

            relatedList = new Map<string, List<RCR_Contract__c>>();
            for (RCR_Contract__c so :  expdServiceOrders)
            {
                if(!relatedList.containsKey(so.ID))
                {
                    relatedList.put(so.ID, new List<RCR_Contract__c>());
                }
                relatedList.get(so.ID).add(so);
            }

            return expdServiceOrders;
        }
        set;
    }

    public List<RCR_Contract__c> CurrentSORequests
    {
        get
        {
            crntSORequests= new List<RCR_Contract__c>();
            Integer offst = QueryOffset;

            Map<Id, RCR_Contract__c> serviceOrders = new Map<Id, RCR_Contract__c>();
            if (getParameter('planId') != null)
            {
                serviceOrders = new Map<Id, RCR_Contract__c> ([SELECT Id
                FROM RCR_Contract__c
                WHERE Plan_Action__r.Participant_Plan__c = :getParameter('planId')]);

            }
            else if (getParameter('planApprovalId') != null)
            {
                serviceOrders = new Map<Id, RCR_Contract__c> ([SELECT Id
                FROM RCR_Contract__c
                WHERE Plan_Action__r.Plan_Approval__c = :getParameter('planApprovalId')]);

            }
            else
            {
                serviceOrders = new Map<Id, RCR_Contract__c> ([SELECT Id
                FROM RCR_Contract__c
                WHERE   Client__c = :controller.getID()
                AND     Status__c = 'New'
                AND     RecordType.Name NOT IN (:APSRecordTypes.RCRServiceOrder_ServiceOrder,
                        :APSRecordTypes.RCRServiceOrder_ServiceOrderCBMS)]);
            }

            TotalRecords = [SELECT  COUNT()
            FROM    RCR_Contract__c
            WHERE   Client__c = :controller.getID()
            AND     Id IN :serviceOrders.keySet()
            AND     Status__c = 'New'
            AND     RecordType.Name NOT IN (:APSRecordTypes.RCRServiceOrder_ServiceOrder,
                    :APSRecordTypes.RCRServiceOrder_ServiceOrderCBMS)
            AND Created_by_Adhoc_Payment__c = false];

            recCountR = TotalRecords;

            crntSORequests = [SELECT    ID,
                    Name,
                    Client__c,
                    Status__c,
                    Request_Amount__c,
                    Start_Date__c,
                    End_Date__c,
                    CSA_Status__c,
                    Level__c,
                    Total_Service_Order_Cost__c,
                    Total_Submitted__c,
                    RecordTypeID,
                    RecordType.Name,
                    Account__r.Name,
                    Original_End_Date__c,
                    Cancellation_Date__c,
                    Funding_Source__c,
                    Account__c,
                    Plan_Action__r.Participant_Plan__r.Name,
                    Plan_Action__r.Name,
                    Plan_Action__r.Participant_Plan__c,
                    Plan_Action__c,
                    Plan_Action__r.Approved__c,
            (SELECT  ID,
                    Name,
                    Service_Code__c,
                    Service_Description__c,
                    Service_Types__c,
                    Start_Date__c,
                    End_Date__c,
                    Total_Cost__c,
                    Total_Submitted__c,
                    Remaining__c,
                    Standard_Service_Code__c
            FROM    RCR_Contract_Scheduled_Services__r)
            FROM    RCR_Contract__c
            WHERE   Client__c = :controller.getID()
            AND     Id IN :serviceOrders.keySet()
            AND     Client__c != NULL
            AND     Status__c = 'New'
            AND RecordType.Name NOT IN (:APSRecordTypes.RCRServiceOrder_ServiceOrder,
                    :APSRecordTypes.RCRServiceOrder_ServiceOrderCBMS)
            AND Created_by_Adhoc_Payment__c = false
            ORDER BY End_Date__c DESC
            LIMIT  :PageSize
            OFFSET  :offst];

            Records = crntSORequests;
            return crntSORequests;
        }
        private set;
    }

    public PageReference newRequest()
    {
        try{
            String pageHeaderReferer = ApexPages.currentPage().getHeaders().get('Referer');
            PageReference pg = new PageReference('/apex/RCRDirectContractRequestEdit?clientId='+serviceOrder.Client__c +'&retURL='+
                    EncodingUtil.urlEncode(pageHeaderReferer,'UTF-8'));
            pg.setRedirect(true);
            return pg;
        }
        catch(Exception ex)
        {
            return null;
        }
    }

    public void syncLPContracts()
    {
        try
        {
            LPRESTInterface lpInterface = new LPRESTInterface();
            for(RCR_Contract__c serviceOrder : LPRESTInterface.getContracts([SELECT    ID
            FROM    RCR_Contract__c
            WHERE   Client__c = :controller.getId() AND
            RecordType.Name = :APSRecordTypes.RCRServiceOrder_ServiceOrder AND
            Cancellation_Date__c = null AND
            LP_Contract_Key__c != null]))
            {
                lpInterface.amendContract(serviceOrder, false);
            }
            lpInterface.saveRecords();
            CurrentServiceOrders = null;
            ExpiredServiceOrders = null;
            LPContractsByLPContractNumber = null;
            LPContractsBySOName = null;
            LPExpContractsBySOName = null;
            LPPreAuthorisationsByCSSName = null;
            LPExpPreAuthorisationsByCSSName = null;
        }
        catch(Exception ex)
        {
            A2HCException.formatException(ex);
        }
    }
}