public with sharing class CaseNotesReportExtension
{
    public CaseNotesReportExtension(ApexPages.StandardController c)
    {
        record = (Referred_Client__c)c.getRecord();
        
        DateParams = new Schedule__c();
        ShowNotes = true;
        ShowEmails = true;
    }

    public Referred_Client__c record;
    public Schedule__c DateParams { get; set; }
    public Boolean ShowNotes { get; set; }
    public Boolean ShowEmails { get; set; }
    public String AttachmentURL { get; set; }
    
    public void Print()
    {   
        setPageParameters(Page.CaseNotesReportPDF);
    }
    
    public void CSV()
    {   
        setPageParameters(Page.CaseNotesReportCSV);
    }

    private void setPageParameters(PageReference pg)
    {
        AttachmentURL = null;
        pg.getParameters().put('id', record.Id);
        pg.getParameters().put('from', (DateParams.Start_Date__c != null ? DateParams.Start_Date__c.format() : ''));
        pg.getParameters().put('to', (DateParams.End_Date__c != null ? DateParams.End_Date__c.format() : ''));
        pg.getParameters().put('notes', String.valueOf(ShowNotes));
        pg.getParameters().put('emails', String.valueOf(ShowEmails));
        AttachmentURL = pg.getURL();
    }
}