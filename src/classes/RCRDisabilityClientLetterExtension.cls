public with sharing class RCRDisabilityClientLetterExtension extends A2HCPageBase
{
	public RCRDisabilityClientLetterExtension(ApexPages.StandardController controller)
	{
		this.contract = (RCR_Contract__c)controller.getRecord();
		
		List<Id> cssIDs = new List<Id>();
		
		for(RCR_Contract_Scheduled_Service__c css : [SELECT Id,
															Start_Date__c,
															End_Date__c,
															Service_Types_Desc__c,
															RCR_Service__r.Name,
															RCR_Service__r.Description__c,
															(SELECT Id,
																	Name,
                                                                    Service_Type__c,
                                                                    Quantity__c
                                                             FROM	RCR_Contract_Service_Types__r)
													 FROM	RCR_Contract_Scheduled_Service__c
													 WHERE	RCR_Contract__c = :contract.Id])
		{
			ScheduledServices.add(css);
			cssIDs.add(css.Id);
		}
		
		for(RCR_Contract_AdHoc_Service__c ahs : [SELECT	Id,
														Start_Date__c,
														End_Date__c,
														Comment__c
												 FROM	RCR_Contract_AdHoc_Service__c
												 WHERE	RCR_Contract__c = :contract.Id])
		{
			AdHocServices.add(ahs);
		}
	
		for(Schedule__c schedule : [SELECT	Name,
											RCR_Contract_Scheduled_Service__c,
											Quantity_Per_Period__c,
											Number_of_weeks_in_period__c
									FROM	Schedule__c
									WHERE	Name IN :cssIDs])
		{
			if(!SchedulesByID.containsKey(schedule.Name))
			{
				SchedulesByID.put(schedule.Name, schedule);
			}
		}
	}
	
	private final RCR_Contract__c contract;
	
	@TestVisible
	private List<RCR_Contract_Scheduled_Service__c> ScheduledServices
	{
		get
		{
			if(ScheduledServices == null)
			{
				ScheduledServices = new List<RCR_Contract_Scheduled_Service__c>();
			}
			return ScheduledServices;
		}
		private set;
	}
	
	@TestVisible
	private List<RCR_Contract_AdHoc_Service__c> AdHocServices
	{
		get
		{
			if(AdHocServices == null)
			{
				AdHocServices = new List<RCR_Contract_AdHoc_Service__c>();
			}
			return AdHocServices;
		}
		private set;
	}
	
	public Map<Id, Schedule__c> SchedulesByID
	{
		get
		{
			if(SchedulesByID == null)
			{
				SchedulesByID = new Map<Id, Schedule__c>();
			}
			return SchedulesByID;
		}
		private set;
	}
	
	public List<ClientServiceWrapper> ClientServices
	{
		get
		{
			if(ClientServices == null)
			{
				ClientServices = new List<ClientServiceWrapper>();
			
				for(RCR_Contract_Scheduled_Service__c css : ScheduledServices)
				{		
					ClientServices.add(new ClientServiceWrapper(css, null, SchedulesByID)); 
				}
				
				for(RCR_Contract_AdHoc_Service__c ahs : AdHocServices)
				{
					ClientServices.add(new ClientServiceWrapper(null, ahs, null));
				}
			}
			return ClientServices;
		}
		private set;
	}
	
	/*
	css.RCR_Service__r.Name + ' (' + css.RCR_Service__r.Description__c + ')', 
																css.Start_Date__c, 
																css.End_Date__c, 
																css.Service_Types_Desc__c,
																SchedulesByID.get(css.Id).Quantity_Per_Period__c,
																SchedulesByID.get(css.Id).Number_of_weeks_in_period__c
*/																
	
	public class ClientServiceWrapper
	{
		public ClientServiceWrapper(RCR_Contract_Scheduled_Service__c pScheduledService, RCR_Contract_AdHoc_Service__c pAdHocService, Map<Id, Schedule__c> pSchedulesByID)
		{
			ScheduledService = pScheduledService;
			AdHocService = pAdHocService;
			SchedulesByID = pSchedulesByID;
		}
		
		public RCR_Contract_Scheduled_Service__c ScheduledService
        {
            get;
            private set;
        }
        
        public RCR_Contract_AdHoc_Service__c AdHocService
        {
            get;
            private set;
        }
        
        public Map<Id, Schedule__c> SchedulesByID
        {
        	get;
        	private set;
        }
		
		public string Service
		{
			get
			{
				return AdHocService == null ? ScheduledService.RCR_Service__r.Name + ' (' + ScheduledService.RCR_Service__r.Description__c + ')' : AdHocService.Comment__c;
			}
		}
		
		public Date StartDate
		{
			get
			{
				return AdHocService == null ? ScheduledService.Start_Date__c : AdHocService.Start_Date__c;
			}
		}
		
		public Date EndDate
		{
			get
			{
				return AdHocService == null ? ScheduledService.End_Date__c : AdHocService.End_Date__c;
			}
		}
		
		public string ServiceTypes
		{
			get
			{
				return AdHocService == null ? ScheduledService.Service_Types_Desc__c : AdHocService.Service_Types_Desc__c;
			}
		}
		
		public decimal QuantityPerPeriod
		{
			get
			{
				if(AdHocService != null)
                {
                    return AdHocService.Qty__c;
                }

                QuantityPerPeriod = 0.0;
                for(RCR_Contract_Service_Type__c cst : ScheduledService.RCR_Contract_Service_Types__r)
                {
                    QuantityPerPeriod += cst.Quantity__c == null ? 0.0 : cst.Quantity__c;
                }

                return QuantityPerPeriod;
			}
			private set;
		}
		
		public string WeeksInPeriod
		{
			get
			{
				if(AdHocService != null)
                {
                    return 'Service Date Range';
                }
					
				if(!SchedulesByID.containsKey(ScheduledService.Id))
				{
					return '';
				}

				return SchedulesByID.get(ScheduledService.Id).Number_Of_Weeks_In_Period__c;
			}
		}
	}
	
	
}