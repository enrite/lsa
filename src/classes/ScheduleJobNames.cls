public class ScheduleJobNames 
{
  public static String CreateRolloverRequest = 'CreateRolloverRequests';
  public static String ApproveRolloverRequest = 'ApproveRolloverRequests';
  public static String ApproveNewRequest = 'ApproveNewRequest';
  public static String UpdateSchedContractCost = 'UpdateSchedContractCost';
  //public static String CSAApproval = 'CSAApproval';
  public static String InvoiceReconciliation = 'InvoiceReconciliation';
}