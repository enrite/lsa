public class ScheduleDayWrapper
{
	public ScheduleDayWrapper(Date d)
	{
		day = d;
		dailySchedule = new List<ScheduleDayItemWrapper>();
	}

	public Date day
	{
		get;
		set;
	}

	public List<ScheduleDayItemWrapper> dailySchedule
	{
		get;
		set;
	}
}