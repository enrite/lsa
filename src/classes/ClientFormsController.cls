public with sharing class ClientFormsController
{
    public ClientFormsController()
    {    
    }

    public Referred_Client__c Client { get; set; }
    
    public List<Form> Forms
    {
        get
        {
            try
            {
                if (Forms == null)
                {
                    Forms = new List<Form>();
                    
                    // add the forms that are attached to the client
                    for (Form__c f : [SELECT Id,
                                             Name,
                                             RecordType.Name,
                                             Status__c,
                                             Hospital_Facility__c,
                                             Hospital_Facility__r.Name,
                                             Date_of_accident__c,
                                             CreatedDate
                                      FROM Form__c
                                      WHERE Client__c = :Client.Id])
                    {
                        Forms.add(new Form(f));
                    }                       
                    
                    // find the case notes that are flagged for displayed
                    Map<Id, Case_Notes__c> caseNotes = new Map<Id, Case_Notes__c> ([SELECT Id,
                                                                                           Resulting_From__c
                                                                                    FROM Case_Notes__c
                                                                                    WHERE Client__c = :Client.Id
                                                                                    AND Display_on_Application_Assessment__c = true]);
                                                                                    
                    // find the attachments for those case notes                                                                                
                    for (Attachment a : [SELECT Id,
                                                Name,
                                                ParentId,
                                                CreatedDate
                                         FROM Attachment
                                         WHERE ParentId IN :caseNotes.keySet()])
                    {
                        Forms.add(new Form(a, caseNotes.get(a.ParentId)));
                    }                                     
                
                    Forms.sort();
                }
                return Forms;
            }
            catch(Exception e)
            {
                A2HCException.formatException(e);
                return null;
            }
        }
        set;    
    }
    
    public class Form implements Comparable
    {            
        public Form(Form__c f)
        {
            RecordId = f.Id;
            Name = f.Name;
            Type = f.RecordType.Name;
            Status = f.Status__c;
            Hospital = f.Hospital_Facility__r.Name;
            DateOfAccident = (f.Date_of_Accident__c != null ? f.Date_of_accident__c.format() : null);
            CreatedDate = f.CreatedDate; 
        }
        
        public Form(Attachment a, Case_Notes__c c)
        {
            RecordId = a.Id;
            Name = a.Name;
            Type = c.Resulting_From__c;
            Status = 'Received';
            Hospital = 'N/A';
            DateOfAccident = 'N/A';
            CreatedDate = a.CreatedDate;
        }
    
        public String RecordId { get; set; }
        public String Name { get; set; }
        public String Type { get; set; }
        public String Status { get; set; }
        public String Hospital { get; set; }
        public String DateOfAccident { get; set; }
        public Datetime CreatedDate { get; set; }
        
        public Integer compareTo(Object compareTo) 
        {
            Form compareToForm = (Form)compareTo;
            
            // The return value of 0 indicates that both elements are equal.         
            Integer returnValue = 0;
            if (CreatedDate > compareToForm.CreatedDate) 
            {
                // Set return value to a negative value.         
                returnValue = -1;
            } 
            else if (CreatedDate < compareToForm.CreatedDate) 
            {
                // Set return value to a positive value.         
                returnValue = 1;
            }            
            return returnValue;       
        }
    }
}