public with sharing class LPBulkLoadContractsController
{
    Decimal batchSize = Lantern_Pay__c.getOrgDefaults().Bulk_load_batch_size__c;
    Integer intBatchSize = batchSize == null ? 20 : batchSize.intValue();

    public List<LPBulkLoadParticipantsController.LPBulkLoadWrapper> ContractsToLoad
    {
        get
        {
            if(ContractsToLoad == null)
            {
                ContractsToLoad = new List<LPBulkLoadParticipantsController.LPBulkLoadWrapper>();
                for(RCR_Contract__c so : [SELECT   ID,
                                                    Name,
                                                    Client_Name__c,
                                                    Start_Date__c,
                                                    End_Date__c,
                                                    Total_Service_Order_Cost__c,
                                                    Total_Submitted__c,
                                                    Account__r.Name,
                                                    Client__c,
                                                    Client__r.Client_ID__c,
                                                    Client__r.LP_Ready_to_Integrate__c,
                                                    Client__r.LP_Registered__c,
                                                    Client__r.LP_Member_Key__c,
                                                    Client__r.LP_Member_Status__c
                                            FROM    RCR_Contract__c
                                            WHERE   Recordtype.DeveloperName = 'Service_Order' AND
                                                    LP_Contract_Key__c = null AND
                                                    Account__r.Lantern_Pay_Active__c = true AND
                                                    End_Date__c >= :Date.today().addDays(-90) AND
                                                    Cancellation_Date__c = null AND
                                                    LP_Do_Not_Send__c = false
                                            ORDER BY LastModifiedDate
                                            LIMIT :intBatchSize])
                {
                    ContractsToLoad.add(new LPBulkLoadParticipantsController.LPBulkLoadWrapper(so));
                }
            }
            return ContractsToLoad;
        }
        set;
    }

    public Boolean ShowParticipantButton
    {
        get
        {
            Boolean show = false;
            for(LPBulkLoadParticipantsController.LPBulkLoadWrapper wrapper : ContractsToLoad)
            {
                if(wrapper.ServiceOrder.Client__r.LP_Ready_to_Integrate__c)
                {
                    show = true;
                    break;
                }
            }
            return show;
        }
    }

    public void sendToLP()
    {
        try
        {
            if(ShowParticipantButton)
            {
                A2HCException.formatMessage('Some participants have not been registered with LanternPay, Register Participants first.');
            }
            List<RCR_Contract__c> contracts = new List<RCR_Contract__c>();
            for(LPBulkLoadParticipantsController.LPBulkLoadWrapper wrapper : ContractsToLoad)
            {
                if(wrapper.Selected)
                {
                    contracts.add(wrapper.ServiceOrder);
                }
            }
            if(contracts.isEmpty())
            {
                A2HCException.formatMessage('No records have been selected');
                return;
            }
            contracts = LPRESTInterface.getContracts(contracts);
            Map<ID, RCR_Service__c> servicesByID = LPRESTInterface.getContractServices(contracts);
            Map<ID, Referred_Client__c> clientsById = new Map<ID, Referred_Client__c>([SELECT ID,
                                                                                                Given_Name__c,
                                                                                                Family_Name__c,
                                                                                                Client_ID__c,
                                                                                                Date_Of_Birth__c,
                                                                                                Current_Status__c,
                                                                                                Current_Status_Date__c,
                                                                                                LP_Member_Key__c,
                                                                                                LP_Member_Status__c,
                                                                                                LP_Registered__c,
                                                                                                CreatedDate
                                                                                        FROM    Referred_Client__c
                                                                                        WHERE   ID IN :LPRESTInterface.getClientIds(contracts)]);
            List<LP_API_Error__c> errors = new List<LP_API_Error__c>();
            LPRESTInterface lpInterface = new LPRESTInterface();
            Integer count = 0;
            Boolean limitReached = false;
            for(RCR_Contract__c contract : contracts)
            {
                try
                {
                    Referred_Client__c client = clientsById.get(contract.Client__c);
                    if(client.LP_Registered__c)
                    {
                        lpInterface.addContract(contract, client, servicesByID);
                        count = Limits.getCallouts();
                        if(count >= Limits.getLimitCallouts() - 5)
                        {
                            limitReached = true;
                            break;
                        }
                    }
                }
                catch(Exception ex)
                {
                    errors.add(LPRESTInterface.logError('Bulk load contracts', ex.getStackTraceString(), null, null, true));
                }
            }
            lpInterface.saveRecords();
            upsert errors;
            if(limitReached)
            {
                A2HCException.formatMessage('A maximum of ' + String.valueOf(count) + ' records can be sent to LanternPay in one transaction, refresh the page and send another batch.');
            }
            A2HCException.formatMessage(String.valueOf(count) + ' contracts sent to LanternPay. The contract keys will be populated as LanternPay responds.');
        }
        catch(Exception ex)
        {
            A2HCException.formatException(ex);
        }
    }

    public void sendParticipantToLP()
    {
        try
        {
            Set<ID> clientIdsToSend = new Set<ID>();
            for(LPBulkLoadParticipantsController.LPBulkLoadWrapper wrapper : ContractsToLoad)
            {
                if(wrapper.Selected && wrapper.ServiceOrder.Client__r.LP_Ready_to_Integrate__c)
                {
                    clientIdsToSend.add(wrapper.ServiceOrder.Client__c);
                }
            }
            if(clientIdsToSend.isEmpty())
            {
                A2HCException.formatMessage('All participants are already registered with LanternPay');
                return;
            }

            List<Referred_Client__c> participants = LPRESTInterface.getClients(clientIdsToSend);
            LPRESTInterface lpInterface = new LPRESTInterface();
            for(Referred_Client__c participant : participants)
            {
                lpInterface.addMember(participant);
            }
            lpInterface.saveRecords();
            A2HCException.formatMessage(String.valueOf(participants.size()) + ' participants sent to LanternPay. Their participant keys will be populated as LanternPay responds.');
        }
        catch(Exception ex)
        {
            A2HCException.formatException(ex);
        }
    }
}