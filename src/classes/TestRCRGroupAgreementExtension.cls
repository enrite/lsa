@isTest
private class TestRCRGroupAgreementExtension
{
    public static TestMethod void myTestMethod() 
    {   
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        RCR_Group_Agreement__c g = new RCR_Group_Agreement__c();
    
        RCRGroupAgreementExtension ext = new RCRGroupAgreementExtension(new ApexPages.StandardController(g));             
        system.debug(ext.ServiceOrders);
        system.debug(ext.Requests);
        ext.submitWithdrawl();
        
        Test.StopTest();
    }
}