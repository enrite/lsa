public with sharing class RCRAccountAvailableForBulkApprovals extends A2HCPageBase
{
    public List<Account> AccountsWithRollovers
    {
        get
        {
            if(AccountsWithRollovers == null)
            { 
                AccountsWithRollovers = new List<Account>();
                Set<Id> accountIDs = new Set<Id>();
                
                for(Account a : [SELECT ID,
                                        Name,
                                        Available_for_Bulk_Rollover_Approvals__c
                                 FROM   Account
                                 WHERE  ID IN (SELECT   c.Account__c
                                                FROM    RCR_Contract__c c
                                                WHERE   c.RecordType.Name = :APSRecordTypes.RCRSERVICEORDER_ROLLOVER
                                                AND     c.Rollover_Status__c = :APS_PicklistValues.RCRContract_RolloverStatus_ReadyForAllocation
                                                AND     (c.Status__c = null OR
                                                        c.Status__c = :APS_PicklistValues.RCRContract_Status_New)
                                                AND     c.Destination_Record__c = null
                                                AND     c.Schedule__c = null)
                                 ORDER BY Name])
                {
                    AccountsWithRollovers.add(a);
                    accountIDs.add(a.Id);
                    flagValueOnLoad.put(a.Id, a.Available_for_Bulk_Rollover_Approvals__c);
                }
                
                for(Account a : [SELECT ID,
                                        Name,
                                        Available_for_Bulk_Rollover_Approvals__c
                                 FROM   Account
                                 WHERE  Available_for_Bulk_Rollover_Approvals__c = true])
                {
                    if(!accountIDs.contains(a.Id))
                    {
                        AccountsWithRollovers.add(a);
                        flagValueOnLoad.put(a.Id, a.Available_for_Bulk_Rollover_Approvals__c);
                    }
                }
                
                AccountsWithRollovers.sort();
            }                 
            return AccountsWithRollovers;
        }
        private set;
    }
    
    
    private Map<id, boolean> flagValueOnLoad
    {
        get
        {
            if(flagValueOnLoad == null)
            {
                flagValueOnLoad = new Map<Id, boolean>();
            }
            return flagValueOnLoad;
        }
        private set;
    }
    
    
    public PageReference cancel()
    {
        try
        {
            return Page.RCR;
        }
        catch(Exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }

    
    public PageReference makeAccountsAvailable()
    {
        try
        {
            List<Account> accountsToUpdate = new List<Account>();
            for(Account a : AccountsWithRollovers)
            {
                if(a.Available_for_Bulk_Rollover_Approvals__c != flagValueOnLoad.get(a.Id))
                {
                    accountsToUpdate.add(a);
                }
            }
            
            update accountsToUpdate;
            
            AccountsWithRollovers = null;
            flagValueOnLoad = null;
            return null;
        }
        catch(Exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }
    
    @isTest(seealldata = true)
    static void test1()
    {
        RCRAccountAvailableForBulkApprovals ext = new RCRAccountAvailableForBulkApprovals();
        
        system.debug(ext.AccountsWithRollovers);
        system.debug(ext.flagValueOnLoad);        
        ext.cancel();
        ext.makeAccountsAvailable();
    }
}