public class LPPaymentDetailJSON
{
    public String paymentDate;
    public Decimal amount;
    public links links;
    public list<payment> payments;

    public class self
    {
        public String href;
    }

    public class payment
    {
        public String billerKey;
        public Decimal amount;
        public String requestedDate;
        public List<contribution> contributions;
        public String paymentReference;
    }

    public class curies
    {
        public Boolean templated;
        public String href;
        public String name;
    }

    public class contribution
    {
        public String invoiceNumber;
        public String claimKey;
        public Decimal amount;
        public String memberKey;
    }

    public class links
    {
        public curies curies;
        public self self;
    }

    public static LPPaymentDetailJSON parse(String jsonString)
    {
        system.debug(jsonString);
        if(jsonString != null)
        {
            jsonString = jsonString.replace('"date":', '"paymentDate":');
            jsonString = jsonString.replace('"_links":', '"links":');
        }
        system.debug(jsonString);
        return (LPPaymentDetailJSON) System.JSON.deserialize(jsonString, LPPaymentDetailJSON.class);
    }

    public void createPayments(LPRESTInterface interfaceInstance)
    {
        Map<String, Referred_Client__c> participantsByLPKey = new Map<String, Referred_Client__c>();
        Map<String, RCR_Invoice_Item__c> invoiceItemsByClaimKey = new Map<String, RCR_Invoice_Item__c>();
        for(LPPaymentDetailJSON.payment payment : payments)
        {
            for(LPPaymentDetailJSON.contribution contribution : payment.contributions)
            {
                invoiceItemsByClaimKey.put(contribution.claimKey, null);
                participantsByLPKey.put(contribution.memberKey, null);
            }
        }
        for(Referred_Client__c client : [SELECT ID,
                                                Name,
                                                LP_Member_Key__c
                                        FROM    Referred_Client__c
                                        WHERE   LP_Member_Key__c IN :participantsByLPKey.keySet() AND
                                                LP_Member_Key__c != null])
        {
            participantsByLPKey.put(client.LP_Member_Key__c, client);
        }

        for(RCR_Invoice_Item__c invItem : [SELECT ID,
                                                LP_Claim_Key__c,
                                                RCR_Invoice__c
                                        FROM    RCR_Invoice_Item__c
                                        WHERE   LP_Claim_Key__c IN :invoiceItemsByClaimKey.keySet() AND
                                                LP_Claim_Key__c != null])
        {
            invoiceItemsByClaimKey.put(invItem.LP_Claim_Key__c, invItem);
        }
        for(LPPaymentDetailJSON.payment payment : payments)
        {
            for(LPPaymentDetailJSON.contribution contribution : payment.contributions)
            {
                Referred_Client__c participant = participantsByLPKey.get(contribution.memberKey);
                RCR_Invoice_Item__c invItem = invoiceItemsByClaimKey.get(contribution.claimKey);
                LP_Confirmed_Payment__c lpPayment = new LP_Confirmed_Payment__c();
                if(links != null && links.self != null)
                {
                    lpPayment.LP_Detail_URL__c = links.self.href;
                    URL selfURL = new URL(links.self.href);
                    String qry = selfURL.getQuery();
                    String fileKey = null;
                    if(qry != null)
                    {
                        for(String s : qry.split('&'))
                        {
                            if(s != null)
                            {
                                String[] parts = s.split('=');
                                if(parts != null && parts.size() == 2 && parts[0] == 'directEntryFileUKey')
                                {
                                    fileKey = parts[1];
                                }
                            }
                        }
                    }
                    lpPayment.LP_Direct_Entry_File_Key__c = fileKey;
                }
                if(invItem == null)
                {
                    lpPayment.Match_Status__c = LPRESTInterface.LPPAYMENT_MATCHSTATUS_NOTMATCHED;
                }
                else
                {
                    lpPayment.Match_Status__c = LPRESTInterface.LPPAYMENT_MATCHSTATUS_MATCHED;
                    lpPayment.Activity_Statement__c = invItem.RCR_Invoice__c;
                    lpPayment.Activity_Statement_Item__c = invItem.ID;
                }
                lpPayment.Payment_Reference__c = payment.paymentReference;
                lpPayment.Biller_Key__c = payment.billerKey;
                lpPayment.Total_Payment_Amount__c = payment.amount;
                lpPayment.Participant__c = participant == null ? null : participant.ID;
                lpPayment.Requested_Date__c = LPRESTInterface.parseDate(payment.requestedDate);
                lpPayment.Invoice_Number__c = contribution.invoiceNumber;
                lpPayment.Paid_Amount__c = contribution.amount;
                lpPayment.Member_Key__c = contribution.memberKey;
                lpPayment.Claim_Key__c = contribution.claimKey;
                lpPayment.Date__c = LPRESTInterface.parseDate(paymentDate);
                interfaceInstance.lpPayments.add(lpPayment);
            }
        }
    }
}