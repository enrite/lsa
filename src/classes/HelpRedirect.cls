public class HelpRedirect {

    public HelpRedirect() 
    {
    }
        
    public PageReference redirect()
    {
        PageReference page;           
        page = new PageReference('http://inside.dfc.sa.gov.au/teams/app/default.aspx');
                        
        return page.setRedirect(true);        
    }
}