public class LPMemberContractsJSON
{
    public cls_embedded embedded;
    public class cls_embedded
    {
        public cls_lpcontracts[] lpcontracts;
    }

    public class cls_lpcontracts
    {
        public cls_state state;
        public cls_biller biller;
        public String startDate {get;set;}//2018-07-10
        public String endDate {get;set;} //2019-03-15
        public String formattedStartDate
        {
            get
            {
                return convertLPDateString(startDate);
            }
        }
        public String formattedEndDate
        {
            get
            {
                return convertLPDateString(endDate);
            }
        }
        public cls_budget budget {get;set;}
        public String contractNumber {get;set;}//LSA07436
        public cls_preauthorizations[] preauthorizations {get;set;}
        public String title;	//Multiple Services
        private String convertLPDateString(String s)
        {
            Date dt = LPRESTInterface.parseDate(s);
            return dt == null ? '' : dt.format();
        }
    }
    class cls_state {
        public Integer value;	//3
        public String displayName;	//Terminated
    }
    class cls_biller {
        public String billerKey;	//b42ff7f3-a1bc-0629-2fcf-8a3d2bc1e7a8
        public String name;	//One Rehabilitation Service
    }
    public class cls_budget {
        public Double total {get;set;}	//817.2
        public Decimal spent {get;set;}	//0
        public Double unspent {get;set;}	//817.2
    }
    public class cls_preauthorizations {
        public cls_budget budget {get;set;}
        public cls_quantity quantity;
        public cls_hours hours;
        public cls_item item {get;set;}
        public String description {get;set;}	//LSA-SPA - 2515
        public String preauthorizationNumber {get;set;}	//CSA000034485
    }
    class cls_quantity {
        public Decimal total;	//4.5
        public Decimal used;	//0
        public Decimal remaining;	//4.5
    }
    class cls_hours {
        public String total;	//0:00
        public String used;	//0:00
        public String remaining;	//0:00
    }
    public class cls_item {
        public String key;	//ed923246-205e-4405-be19-accd92c1d9aa
        public String code {get;set;}	//2515
        public String name {get;set;}	//Speech Pathology Assessment
        public Decimal unitPrice;	//181.6
    }

    public List<cls_lpcontracts> getContracts()
    {
        if(embedded != null && embedded.lpcontracts != null)
        {
            return embedded.lpcontracts;
        }
        return new List<cls_lpcontracts>();
    }

    public static LPMemberContractsJSON parse(String jsonString)
    {
        jsonString = jsonString.replace('_embedded', 'embedded').replace('lp:contracts', 'lpcontracts');
        LPMemberContractsJSON jsonInstance = (LPMemberContractsJSON) System.JSON.deserialize(jsonString, LPMemberContractsJSON.class);
        if(jsonInstance.embedded != null && jsonInstance.embedded.lpcontracts != null)
        for(Integer i = jsonInstance.embedded.lpcontracts.size() - 1; i >= 0; i--)
        {
            cls_lpcontracts contract = jsonInstance.embedded.lpcontracts[i];
            if(contract.state.value != 2) //Active
            {
                jsonInstance.embedded.lpcontracts.remove(i);
            }
        }
        return jsonInstance;
    }
}