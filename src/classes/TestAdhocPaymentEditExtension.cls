@isTest
public class TestAdhocPaymentEditExtension
{                   
    static testMethod void myTestMethod()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
        
        Account acc = [SELECT Id,
                              Masterpiece_Payment_Required__c 
                       FROM Account 
                       LIMIT 1];
                       
        acc.Masterpiece_Payment_Required__c = true;
        update acc;                       
        Referred_Client__c client = TestLoadData.getTestClient();

        Adhoc_Payment__c a = new Adhoc_Payment__c();
        a.Name = 'Test123456789012';
        a.Account__c = acc.Id;
        //a.Participant__c = [SELECT Id FROM Referred_Client__c LIMIT 1].Id;
        a.Participant__c = client.ID;
        a.Date_of_Transaction__c = client.Current_Status_Date__c.addDays(1);
        a.GL_Code__c = [SELECT Id FROM RCR_Program_Category_Service_Type__c LIMIT 1].Id;
        a.Amount__c = 10;
        a.GST__c = 1;        
        
        AdhocPaymentEditExtension ext = new AdhocPaymentEditExtension(new ApexPages.StandardController(a));
        ext.SaveAndNew();

        // test the trigger
        a.Approved__c = true;
        a.Submitter__c = UserInfo.getUserId();
        a.Approver__c = UserInfo.getUserId(); 
        
        try
        {
            upsert a;
        }
        catch (Exception e)
        {}
        try
        {
            a.Approver__c = 'test';
            a.Submitter__c = 'test';
            upsert a;
        }
        catch (Exception e)
        {}
        
        /*a = new Adhoc_Payment__c();
        a.Name = '1234567890123Test';
        a.Account__c = acc.Id;
        a.Participant__c = [SELECT Id FROM Referred_Client__c LIMIT 1].Id;
        a.Date_of_Transaction__c = Date.today().addDays(-1);
        a.GL_Code__c = [SELECT Id FROM RCR_Program_Category_Service_Type__c LIMIT 1].Id;
        a.Amount__c = 10;
        a.GST__c = 1;        
        insert a;
        a.Approved__c = true;
        a.Submitter__c = 'x';
        a.Approver__c = 'y';
        update a;*/
        
        Test.StopTest();
    }
}