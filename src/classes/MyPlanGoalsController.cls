public with sharing class MyPlanGoalsController
{
    public MyPlanGoalsController()
    {
        FindPlanGoals();
    }
        
    public Plan__c Plan { get; set; }

    public List<String> PlanGoalsKeys { get; set; }
    public Map<String, Plan_Goal__c> PlanGoals { get; set; }
    
    public void FindPlanGoals()        
    {
        try
        {
            PlanGoalsKeys = new List<String>();
            PlanGoals = new Map<String, Plan_Goal__c>();
            
            for (Plan_Goal__c g : [SELECT Id,
                                          Name,
                                          Discussion__c
                                   FROM Plan_Goal__c
                                   WHERE Participant_Plan__c = :Plan.Id
                                   ORDER BY Order__c ASC NULLS LAST])
            {
                String key = A2HCUtilities.GenerateKey(15);
                
                PlanGoalsKeys.add(key);
                PlanGoals.put(key, g);
            }                                                   
        }
        catch(Exception e)
        {
            A2HCException.formatException(e);
        }            
    } 
}