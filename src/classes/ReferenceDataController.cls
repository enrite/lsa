public class ReferenceDataController
{
    public String SelectedPrefix { get; set; }

    public List<String> Objects
    {
        get
        {
            if (Objects == null)
            {
                //Objects = new List<SelectOption>();
                //Objects.add(new SelectOption('', ''));
                Objects = new List<String>();
                
                Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
                List<String> objectNames = new List<String>();
                Map<String, Schema.DescribeSObjectResult> objectDescribes = new Map<String, Schema.DescribeSObjectResult>();
                
                for (String objectName : gd.keySet())
                {     
                    Schema.DescribeSObjectResult dsor = gd.get(objectName).getDescribe(); 
                    
                    if (dsor.isCustom() && !dsor.isCustomSetting())
                    {                    
                        objectNames.add(dsor.getLabel());
                        objectDescribes.put(dsor.getLabel(), dsor);                    
                    }
                }   
                
                objectNames.sort();
                
                for (String o : objectNames)
                {                                                        
                    //Objects.add(new SelectOption(objectDescribes.get(o).getKeyPrefix(), objectDescribes.get(o).getLabel() + ' (' + objectDescribes.get(o).getName() + ')'));                
                    Objects.add('<a href="/' + objectDescribes.get(o).getKeyPrefix() + '">' + objectDescribes.get(o).getLabel()/* + ' (' + objectDescribes.get(o).getName() + ')'*/ + '</a>');                          
                }
            }
            return Objects;
        }
        set;   
    }
}