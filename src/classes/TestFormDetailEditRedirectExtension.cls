@isTest
public class TestFormDetailEditRedirectExtension
{                    
    static testMethod void myTestMethod()
    {
        TestLoadFormData data = new TestLoadFormData();
                                           
        Form__c f = new Form__c();         
        insert f;
        
        Form_Detail__c ce = new Form_Detail__c(Form_Application_Assessment__c = f.Id);        
                
        Test.setCurrentPage(Page.FormDetailEditRedirect);
        ApexPages.CurrentPage().getParameters().put('RecordType', data.RecordTypes.get('Chief_Executive_Application_Assessment').Id);
        
        FormDetailEditRedirectExtension ext = new FormDetailEditRedirectExtension(new ApexPages.StandardController(ce));               
        ext.Redir(); 
        
        Form_Detail__c asd = new Form_Detail__c(Form_Application_Assessment__c = f.Id,
                                                RecordTypeId = data.RecordTypes.get('Manager_ASD_Application_Assessment').Id,
                                                Recommended_to_Chief_Executive_CE__c = true,
                                                Application_Consistent_with_LSS_Rules__c = true,
                                                Legal_Tests__c = true,             
                                                Medical_Tests__c = true);
                                                
        ApexPages.CurrentPage().getParameters().remove('RecordType');                                                
                                                
        ext = new FormDetailEditRedirectExtension(new ApexPages.StandardController(asd));               
        ext.Redir();                                                
                                                
        insert asd;
        
        ext = new FormDetailEditRedirectExtension(new ApexPages.StandardController(asd));               
        ext.Redir(); 
        
        ApexPages.CurrentPage().getParameters().put('RecordType', data.RecordTypes.get('Chief_Executive_Application_Assessment').Id);
        
        ext = new FormDetailEditRedirectExtension(new ApexPages.StandardController(ce));               
        ext.Redir();  
        
        Form_Detail__c ir = new Form_Detail__c(RecordTypeId = data.RecordTypes.get('Internal_Review').Id, Form_Application_Assessment__c = f.Id);                              
        ApexPages.CurrentPage().getParameters().put('RecordType', data.RecordTypes.get('Internal_Review').Id);        
        ext = new FormDetailEditRedirectExtension(new ApexPages.StandardController(ir));               
        ext.Redir();    
        
        Form_Detail__c dcr = new Form_Detail__c(RecordTypeId = data.RecordTypes.get('District_Court_Review').Id, Form_Application_Assessment__c = f.Id);                            
        ApexPages.CurrentPage().getParameters().put('RecordType', data.RecordTypes.get('District_Court_Review').Id);        
        ext = new FormDetailEditRedirectExtension(new ApexPages.StandardController(dcr));               
        ext.Redir();
        
        Form_Detail__c mv = new Form_Detail__c(RecordTypeId = data.RecordTypes.get('Motor_Vehicle').Id, Form_Motor_Vehicle__c = f.Id);                            
        ApexPages.CurrentPage().getParameters().put('RecordType', data.RecordTypes.get('Motor_Vehicle').Id);        
        ext = new FormDetailEditRedirectExtension(new ApexPages.StandardController(mv));               
        ext.Redir();
        
        insert mv;
        ext = new FormDetailEditRedirectExtension(new ApexPages.StandardController(mv));               
        ext.Redir();                                                            
    }
}