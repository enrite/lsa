/**
 * Created by me on 19/03/2019.
 */

public class SatalystOutboundMessage
{
    @future(Callout=true)
    public static void sendSatalystMessage(ID planId)
    {
        sendSatalystMessageImmediate(planId);
    }

    public static void sendSatalystMessageImmediate(ID planId)
    {
        HttpRequest request = new HttpRequest();
        request.setEndpoint('callout:Satalyst_Bot');
        request.setTimeout(Satalyst_Bot__c.getOrgDefaults().Timeout__c.intValue());
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Accept', 'application/json');
        String body = SatalystPlanJSON.serialisePlan(new Plan__c(ID = planId));
        system.debug(body);
        request.setBody(body);
        HttpResponse response = (new Http()).send(request);
        system.debug(response.getStatusCode());
        if(response.getStatusCode() != 200)
        {
            throw new A2HCException(response.getStatusCode() + ': ' + response.getStatus() + '\n'  + response.getBody());
        }
    }
}