/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@IsTest//(SeeAllData=true) 
private class TestRCRDirectorApprovalController 
{
    static testMethod void test1()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        RCR_Contract__c so = [SELECT ID,
                                    Start_Date__c
                            FROM    RCR_Contract__c
                            /*WHERE   Name = 'TestSO'*/
                            LIMIT 1];
                            
        so.RecordTypeID = A2HCUtilities.getRecordType('RCR_Contract__c', APSRecordTypes.RCRServiceOrder_NewRequest).ID;
        so.Panel_Date__c =  so.Start_Date__c.addDays(1);
        update so;
                                
        so = RCRContractExtension.getContract(so.ID);
        
        Test.setCurrentPage(Page.RCR);
        ApexPages.currentPage().getParameters().put('startDate', so.Start_Date__c.format()); 
        ApexPages.currentPage().getParameters().put('endDate', so.End_Date__c.format());
        RCRDirectorApprovalController ext = new RCRDirectorApprovalController();
        ext.getRequests(APSRecordTypes.RCRServiceOrder_NewRequest);

        system.debug(ext.Requests);
        system.debug(ext.PendingApprovalWorkItems); 

        for(RCRDirectorApprovalController.RequestWrapper wrap : ext.Requests)
        {
            
            system.assert(wrap.SelectedCriteriaFieldSet != null);
            system.assert(wrap.ServiceRequest != null);
            
            system.debug(wrap.SelectedCriteriaFieldSet);
    
            for(RCRDirectorApprovalController.AllocationWrapper aw : wrap.ServiceAllocations.values())
            {
                system.assert(aw.ServiceType != null);
                system.assert(aw.RecurrentAlloc != null);
                system.assert(aw.OnceOffAlloc != null);
                system.assert(aw.RecurrentFYEAlloc != null);
            }
        }
        
        ext.printReport();
        
        Test.StopTest();
    }
    
     
    @IsTest//(SeeAllData=true)
    private static void test2()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        RCR_Contract__c so = [SELECT    Id
                                FROM    RCR_Contract__c
                                //WHERE   Name = 'TestSO'
                                LIMIT 1];
        so = RCRContractExtension.getContract(so.ID);
        
        so.RecordTypeId = A2HCUtilities.getRecordType('RCR_Contract__c', APSRecordTypes.RCRServiceOrder_NewRequest).ID;
        so.Panel_Date__c = so.End_Date__c;
        update so;
        
        Date sd = so.Start_Date__c;
        Date ed = so.End_Date__c;
        
        PageReference pg = Page.RCR;
        pg.getParameters().put('startDate', sd.format());
        pg.getParameters().put('endDate', ed.format());
        
        Test.setCurrentPage(pg);
        
        RCRDirectorApprovalController c = new RCRDirectorApprovalController();
        
        system.assert(c.ApprovalDecisionOpts != null); 
                
        c.getRequests(APSRecordTypes.RCRServiceOrder_NewRequest);
        for(RCRDirectorApprovalController.RequestWrapper w : c.Requests)
        {
            system.debug(w.CurrentSituationNotDefined);
            system.debug(w.SelectedCriteriaFieldSet);
            system.debug(w.getFieldsetCheckedLabels(sObjectType.RCR_Contract__c.fieldSets.Criteria)); 
        }
        
        RCRDirectorApprovalController.ApprovalDecisionWrapper wrap = new RCRDirectorApprovalController.ApprovalDecisionWrapper('test', 'test');
        system.debug(wrap.RequestId);
        system.debug(wrap.Decision);
        
        so.RecordTypeId = A2HCUtilities.getRecordType('RCR_Contract__c', APSRecordTypes.RCRServiceOrder_Amendment).ID;
        so.Funding_Source__c = APS_PicklistValues.RCRContract_FundingSource_UseOfAllocation;
        so.Status__c = APS_PicklistValues.RCRContract_Status_New;
        update so;
        
        Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
        request.setObjectId(so.ID);
        Approval.process(request, false); 
        
        c.getRequests(APSRecordTypes.RCRServiceOrder_Amendment);
        
        
        system.assert(c.Amendments != null);
        system.assert(c.Requests != null);               
        
        c.ApproveReject();
        
        c.Amendments = null;
        c.Requests = null;
        
        Test.StopTest();
    }
    
    @IsTest//(SeeAllData=true)
    private static void testException()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        RCRDirectorApprovalController c = new RCRDirectorApprovalController();
        c.forceTestError = true;
        try
        {
            c.getRequests(null);
        }
        catch(Exception ex){}
        try
        {
            c.ApproveReject();
        }
        catch(Exception ex){}
        try
        {
            c.printReport();
        }
        catch(Exception ex){}
        
        Test.StopTest();
    }
    
    @IsTest//(SeeAllData=true)
    private static void testWrapper()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        RCRDirectorApprovalController.AllocationWrapper w = new RCRDirectorApprovalController.AllocationWrapper('test');
        system.assert(w.ServiceType != null);
        system.assert(w.RecurrentAlloc == 0.0);
        system.assert(w.RecurrentFYEAlloc == 0.0);
        system.assert(w.OnceOffAlloc == 0.0);
        system.assert(w.OnceOffFYEAlloc == 0.0);
        
        Test.StopTest();
    }
    
    @IsTest//(SeeAllData=true)
    private static void testServiceAllocations()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        RCRDirectorApprovalController c = new RCRDirectorApprovalController();
        
        RCR_Contract__c so = [SELECT    ID  
                                FROM    RCR_Contract__c
                                /*WHERE   Status__c = :APS_PicklistValues.RCRContract_Status_Approved AND
                                        ID IN (SELECT   RCR_Service_Order__c
                                                FROM    IF_Personal_Budget__c
                                                WHERE   Allocation_Type__c = :APS_PicklistValues.IFAllocation_Type_OnceOff)*/
                                LIMIT 1];
        so = RCRContractExtension.getContract(so.ID);
        
        RCRDirectorApprovalController.RequestWrapper w = new RCRDirectorApprovalController.RequestWrapper(so, new ProcessInstanceWorkitem(), sObjectType.RCR_Contract__c.fieldSets.Criteria);
        w.setServiceAllocations();
        
        so = [SELECT    ID
                FROM    RCR_Contract__c
                /*WHERE   Status__c = :APS_PicklistValues.RCRContract_Status_Approved AND
                        ID IN (SELECT   RCR_Service_Order__c
                                FROM    IF_Personal_Budget__c
                                WHERE   Allocation_Type__c = :APS_PicklistValues.IFAllocation_Type_Recurrent)*/
                LIMIT 1];       
        so = RCRContractExtension.getContract(so.ID);
        
        w = new RCRDirectorApprovalController.RequestWrapper(so, new ProcessInstanceWorkitem(), sObjectType.RCR_Contract__c.fieldSets.Criteria);
        c.PendingApprovalWorkItems.put(w.ServiceRequest.Id, new ProcessInstanceWorkitem());
        w.setServiceAllocations();
        w.ApprovalDecision = 'Approved';
        c.getWorkItemRequest(w);
        w.ApprovalDecision = 'Rejected';
        c.getWorkItemRequest(w);
        w.ApprovalDecision = 'Test';
        c.getWorkItemRequest(w);
        
        Test.StopTest();
    }
}