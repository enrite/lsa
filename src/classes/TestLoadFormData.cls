@isTest
public class TestLoadFormData 
{    
    public TestLoadFormData()
    {
        ServiceProvider = new Account(Name = 'Test');
        insert ServiceProvider;
        
        Contact c = new Contact(AccountId = ServiceProvider.Id, 
                                      LastName = 'Test');
        insert c;
                        
        PortalUser = new User(ContactId = c.Id,
                              Username = 'portal.user@lsa.com',
                              Email = 'portal.user@lsa.com',
                              LastName = 'Test',
                              Alias = 'Test',
                              ProfileID = [SELECT Id FROM Profile WHERE Name = 'LSA Community User'].Id,                              
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US',
                              TimeZoneSidKey = 'Australia/Adelaide',
                              EmailEncodingKey='UTF-8');
        insert PortalUser;
    
        Client = new Referred_Client__c(Given_Name__c = 'Test',
                                        Family_Name__c = 'Test',
                                        Date_of_Birth__c = Date.today(),
                                        Address__c = 'Test',
                                        Suburb__c = 'Test',
                                        State__c = 'Test',
                                        Postcode__c = 'Test',
                                        Phone__c = '012345678',
                                        SAPOL_Vehicle_Collision_Report_Number__c = 'Test',
                                        Time_of_accident_hours__c = '13',
                                        Time_of_accident_minutes__c = '00',
                                        Date_of_accident__c = Date.today(),
                                        Allocated_to__c = UserInfo.getUserId());        
        insert Client; 
        
        ParentGuardian = new Participant_Contact__c(Participant__c = Client.Id,
                                                    RecordTypeId = RecordTypes.get('Parent_or_Guardian').Id,
                                                    First_Name__c = 'Test',
                                                    Surname__c = 'Test');
        
        insert ParentGuardian;
        
        RCR_Office_Team__c ot = new RCR_Office_Team__c(Name = 'LSA');
        insert ot;               
    }

    public Referred_Client__c Client;
    public Participant_Contact__c ParentGuardian;
    public Account ServiceProvider;
    public User PortalUser;

    public Map<String, RecordType> RecordTypes
    {
        get
        {
            if (RecordTypes == null)
            {
                RecordTypes = new Map<String, RecordType>();
                
                for (RecordType rt : [SELECT Id,
                                             Name,
                                             DeveloperName
                                      FROM RecordType
                                      WHERE sObjectType IN ('Form__c', 'Form_Detail__c', 'Participant_Contact__c', 'Plan_Approval__c', 'Plan__c', 'Plan_Action__c')])
                {
                    recordTypes.put(rt.DeveloperName, rt);
                }  
            }
            return RecordTypes;    
        }
        set;
    }

}