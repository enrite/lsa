/**
 * Created by USer on 08/09/2016.
 */

public with sharing class GrantPublicMilestoneController
{
    public GrantPublicMilestoneController()
    {}

    private transient Map<String, List<Grant_Application__c>> tApplicationsByGrant;

    public Map<String, List<Grant_Application__c>> ApplicationsByGrant
    {
        get
        {
            if(tApplicationsByGrant == null)
            {
                tApplicationsByGrant = new Map<String, List<Grant_Application__c>>();
                for(Grant_Application__c ga : [SELECT   ID,
                                                        Grant_Round__c,
                                                        Grant_Round__r.Name,
                                                        Name_of_Project__c,
                                                        Name_of_Applicant_Organisation__c,
                                                        Brief_Project_Summary__c,
                                                        (SELECT     ID,
                                                                    Name,
                                                                    Due_Date__c,
                                                                    Description__c,
                                                                    Progress_Update__c,
                                                                    Make_Public__c
                                                        FROM        Grant_Milestones__r
                                                        ORDER BY Due_Date__c)
                                                FROM    Grant_Application__c
                                                WHERE   Status__c = 'Approved'
                                                ORDER BY Grant_Round__r.Open_Date__c])
                {
                    if(!tApplicationsByGrant.containsKey(ga.Grant_Round__r.Name))
                    {
                        tApplicationsByGrant.put(ga.Grant_Round__r.Name, new List<Grant_Application__c>());
                    }
                    tApplicationsByGrant.get(ga.Grant_Round__r.Name).add(ga);
                }
            }
            return tApplicationsByGrant;
        }
    }
}