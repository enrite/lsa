@isTest
public class TestParticipantPortalMyRequestsList
{                    
    static testMethod void myTestMethod()
    {            
        TestLoadFormData data = new TestLoadFormData();
                 
        Test.StartTest();
        
        Request__c r = new Request__c();
        r.Client__c = data.Client.Id;
        r.Request_Details__c = 'test';
        insert r;
                                
        ParticipantPortalMyRequestsList c = new ParticipantPortalMyRequestsList();
        system.debug(c.setCon);                                                                      
        c.getRequests();
        
        Test.StopTest();
    }
}