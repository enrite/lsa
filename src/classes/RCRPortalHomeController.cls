public with sharing class RCRPortalHomeController
    extends A2HCPageBase
{
    public String ServiceOrderNumber
    {
        get;
        set;
    }
    
    public List<RCR_Invoice__c> UnreconciledInvoices
    {
        get
        {
            if(UnreconciledInvoices == null)
            {
                UnreconciledInvoices = [SELECT  ID,
                                                Name,
                                                Status__c,
                                                Invoice_Number__c,
                                                Total__c,
                                                Total_Reconciled__c,
                                                Total_Failed_Reconciliation__c,
                                                CreatedBy.Name
                                        FROM    RCR_Invoice__c
                                        WHERE   RecordType.Name = :APSRecordTypes.RCRInvoice_Created AND
                                                Created_By_Portal_User__c = 'Y'
                                        ORDER BY CreatedDate DESC
                                        LIMIT 6];
            }
            return UnreconciledInvoices;
        }
        private set;
    }
    
    public List<RCR_Contract__c> ServiceOrdersForAcceptance
    {
        get
        {
            if(ServiceOrdersForAcceptance == null)
            {
                ServiceOrdersForAcceptance = [SELECT    Id,
                                                    Name,
                                                    Level__c,
                                                    Start_Date__c,
                                                    End_Date__c,
                                                    Total_Service_Order_Cost__c,
                                                    Group_Program__c,
                                                    Client_Name__c,
                                                    (SELECT ID 
                                                    FROM    Source_Records__r 
                                                    WHERE   RecordType.Name = :APSRecordTypes.RCRServiceOrder_Amendment)
                                          FROM      RCR_Contract__c
                                          WHERE     Visible_To_Service_Providers__c = true
                                          AND       (RecordType.Name = :APSRecordTypes.RCRServiceOrder_ServiceOrder OR
                                                    RecordType.Name = :APSRecordTypes.RCRServiceOrder_ServiceOrderCBMS)
                                          AND       RCR_Cancellation_Date__c = null
                                          AND       CSA_Status__c = :APS_PicklistValues.RCRContractCSAStatus_Pending_Acceptance];
            }
            return ServiceOrdersForAcceptance;
        }
        private set;
    }
    

    public List<RCR_Service_Rate_Variation__c> ServiceRateVariations
    {
        get
        {
            if(ServiceRateVariations == null)
            {
                ServiceRateVariations = [SELECT Id,
                                                Name
                                         FROM   RCR_Service_Rate_Variation__c
                                         WHERE  Approved_By__c != null
                                         AND    Accepted_By__c = null];
            }
            return ServiceRateVariations;
        }
        private set;
    }

    
    public List<RCR_Contract__c> ServiceOrdersForCancellation
    {
        get
        {
            if(ServiceOrdersForCancellation == null)
            {
                ServiceOrdersForCancellation = [SELECT  Id,
                                                        Name,
                                                        Level__c,
                                                        Start_Date__c,
                                                        End_Date__c,
                                                        Total_Service_Order_Cost__c,
                                                        Group_Program__c,
                                                        Client_Name__c,
                                                        (SELECT ID 
                                                        FROM    Source_Records__r 
                                                        WHERE   RecordType.Name = :APSRecordTypes.RCRServiceOrder_Amendment)
                                              FROM      RCR_Contract__c
                                              WHERE     Visible_To_Service_Providers__c = true
                                              AND       (RecordType.Name = :APSRecordTypes.RCRServiceOrder_ServiceOrder OR
                                                        RecordType.Name = :APSRecordTypes.RCRServiceOrder_ServiceOrderCBMS)
                                              AND       Status__c = :APS_PicklistValues.RCRContract_Status_Approved
                                              AND       RCR_Cancellation_Date__c != null
                                              AND       Cancellation_Approval_Date__c != null
                                              AND       Cancellation_Approver__c != null
                                              AND       Cancellation_Acknowledged_Date__c = null];
            }
            return ServiceOrdersForCancellation;
        }
        private set;
    }   
    
    public List<RCR_Contract__c> ServiceOrdersSearchResults
    {
        get
        {
            if(ServiceOrdersSearchResults == null)
            {
                ServiceOrdersSearchResults = new List<RCR_Contract__c>();
            }
            return ServiceOrdersSearchResults;
        }
        private set;
    }

    public List<RCR_Invoice__c> SubmittedInvoices
    {
        get
        {
            if(SubmittedInvoices == null)
            {
                SubmittedInvoices = [SELECT ID,
                                            Name,
                                            Status__c,
                                            Invoice_Number__c,
                                            Total__c,
                                            Total_Reconciled__c,
                                            Total_Failed_Reconciliation__c,
                                            CreatedBy.Name
                                    FROM    RCR_Invoice__c
                                    WHERE   RecordType.Name = :APSRecordTypes.RCRInvoice_Submitted
                                    ORDER BY LastModifiedDate DESC
                                    LIMIT 6];
            }
            return SubmittedInvoices;
        }
        private set;
    }

    public List<Service_Query__c> ServiceQueries
    {
        get
        {
            if(ServiceQueries == null)
            {
                ServiceQueries = [SELECT    ID,
                                            Name,
                                            Contact_Name__c,
                                            Status__c,
                                            Service_Query_Type__c,
                                            RCR_Service_Order__c,
                                            RCR_Service_Order__r.Name,
                                            Statement_Number__c,
                                            CreatedDate,
                                            CreatedBy.Name
                                    FROM    Service_Query__c
                                    WHERE   Status__c = :APS_PicklistValues.ServiceQuery_Status_Lodged OR
                                            Status__c = :APS_PicklistValues.ServiceQuery_Status_InProgress
                                    ORDER BY CreatedDate DESC
                                    LIMIT 11];
            }
            return ServiceQueries;
        }
        private set;
    }
    
    public List<RCR_Contract__c> ServiceRequests
    {
        get
        {
            if(ServiceRequests == null)
            {
                ServiceRequests = [SELECT   ID,
                                            Name,                                           
                                            Client_Name__c
                                    FROM    RCR_Contract__c
                                    WHERE   RecordType.Name = :APSRecordTypes.RCRServiceOrder_NewRequest
                                    ORDER BY CreatedDate DESC
                                    LIMIT 11];
            }
            return ServiceRequests;
        }
        private set;
    }

    public List<ServiceWrapper> Services
    {
        get
        {
            if(Services == null)
            {
                Services = new List<ServiceWrapper>();
                for(RCR_Service__c s : [SELECT  ID,
                                                Name,
                                                Description__c,
                                                Day_of_Week__c,
                                                (SELECT ID,
                                                        Rate__c,
                                                        Public_Holiday_Rate__c,
                                                        Start_Date__c,
                                                        End_Date__c
                                                FROM    RCR_Service_Rates__r
                                                WHERE   Start_Date__c <= :Date.today() AND
                                                        End_Date__c >= :Date.today()
                                                )
                                        FROM    RCR_Service__c
                                        ORDER BY Name])
                {
                    Services.add(new ServiceWrapper(s));
                }
            }
            return Services;
        }
        private set;
    }
    
    public void searchServiceOrders()
    {
        try
        {
            ServiceOrdersSearchResults = [SELECT    ID,
                                                    Name,
                                                    Start_Date__c,
                                                    End_Date__c,
                                                    Cancellation_Date__c,
                                                    Total_Service_Order_Cost__c,
                                                    Comments__c,
                                                    Group_Program__c,
                                                    Client_Name__c
                                            FROM    RCR_Contract__c
                                            WHERE   Name LIKE :(ServiceOrderNumber + '%') AND
                                                    Visible_to_Service_Providers__c = true AND                                              
                                                    (RecordType.Name = :APSRecordTypes.RCRServiceOrder_ServiceOrder OR
                                                     RecordType.Name = :APSRecordTypes.RCRServiceOrder_ServiceOrderCBMS)
                                             LIMIT 11];
        }
        catch(Exception ex)
        {
            A2HCException.formatException(ex);
        }
    }

    public class ServiceWrapper
    {
        public ServiceWrapper(RCR_Service__c pService)
        {
            Service = pService;
            if(pService.RCR_Service_Rates__r.size() == 0)
            {
                CurrentRate = new RCR_Service_Rate__c();
            }
            else
            {
                CurrentRate = pService.RCR_Service_Rates__r[0];
            }
        }

        public RCR_Service__c Service
        {
            get;
            private set;
        }

        public RCR_Service_Rate__c CurrentRate
        {
            get;
            private set;
        }
    }
}