/**
 * An apex page controller that exposes the site login functionality
 */
global class SiteLoginController 
	extends A2HCPageBase
{
    global String username {get; set;}
    global String password {get; set;}

    global PageReference login() {
		String startUrl = '/home/home.jsp'; 
		return Site.login(username, password, startUrl);
    }
    
    global SiteLoginController () 
    {
    	String user = getParameter('un');
    	if(user != null)
    	{
    		username = user;    		
    	}
    }
    
    global static testMethod void testSiteLoginController () {
        // Instantiate a new controller with all parameters in the page
        SiteLoginController controller = new SiteLoginController ();
        controller.username = 'cpa_provider@hotmail.com';
        controller.password = 'eReferral1'; 
		
		System.debug('login:' + Site.isLoginEnabled());
		
		PageReference page = controller.login();
        System.assert(page == null);                           
    }    
}