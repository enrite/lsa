@isTest
private class TestRCRParticipantReviewExtension
{
    public static TestMethod void myTestMethod1() 
    {        
        TestLoadData.loadRCRData();
        
        Test.StartTest();
        
        TestLoadData.loadAdhocServices();
        
        RCR_Contract__c c = [SELECT Id
                             FROM RCR_Contract__c
                             LIMIT 1];
                   
        RCRParticipantReviewExtension ext = new RCRParticipantReviewExtension(new ApexPages.StandardController(c));                   
        system.debug(ext.AllContractServiceTypesByCssID);
        system.debug(ext.Programs);
        system.debug(ext.DefaultFlexibility);
        system.debug(ext.ServiceOrder);
        system.debug(ext.mySched);
        system.debug(ext.ScheduleServiceWrappers);
        system.debug(ext.Services);       
        
        Test.StopTest();
    }
    
    public static TestMethod void myTestMethod2() 
    {        
        TestLoadData.loadRCRData();
        
        Test.StartTest();
        
        TestLoadData.loadAdhocServices();
        
        RCR_Contract__c c = [SELECT Id
                             FROM RCR_Contract__c
                             LIMIT 1];
                   
        RCRParticipantReviewExtension ext = new RCRParticipantReviewExtension(new ApexPages.StandardController(c));                         
        ext.createAmendment();
        //ext.saveOverride();
        ext.acceptPlan();
        
        Test.StopTest();
    }       
}