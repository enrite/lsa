public with sharing class SelectRecordTypeController
{
    public SelectRecordTypeController()
    {
        FindRecordTypes();
    }

    public String SelectedRecordType { get; set; }
    public List<RecordType> RecordTypes { get; set; }
    public List<SelectOption> RecordTypeOptions  { get; set; }
        
    private void FindRecordTypes()
    {   
        RecordTypes = new List<RecordType>();    
        RecordTypeOptions = new List<SelectOption>();
            
        String objType = ApexPages.CurrentPage().getParameters().get('o');
        
        String recordTypeIds = ApexPages.CurrentPage().getParameters().get('rid');
        if (recordTypeIds != null)
        {
            recordTypeIds = '\'' + recordTypeIds.replace(';', '\',\'') + '\'';
        }
        
        String recordTypeNames = ApexPages.CurrentPage().getParameters().get('rn');
        if (recordTypeNames != null)
        {
            recordTypeNames = '\'' + recordTypeNames.replace(';', '\',\'') + '\'';
        }
    
        // set up the query
        String q = 'SELECT Id, Name, DeveloperName, Description ';
        q += 'FROM RecordType ';
        q += 'WHERE sObjectType = \'' + objType + '\' ';
        
        // if the ids parameter has been supplied then we'll filter on that                
        if (recordTypeIds != null)
        {
            q += 'AND Id IN (' + recordTypeIds + ') ';
        }
        // otherwise if the names parameter has been supplied then we'll filter on that
        else if (recordTypeNames != null)
        {
            q += 'AND DeveloperName IN (' + recordTypeNames + ') ';                    
        }
        
        q += 'ORDER BY Name';

        system.debug('q: ' + q);
        // query for the record types
        Map<String, RecordType> queried = new Map<String, RecordType>();                                
        for (RecordType rt : Database.query(q))
        {
            // if the ids parameter has been supplied then map by id
            if (recordTypeIds != null)
            {
                queried.put(rt.Id, rt);
            }
            // if the names parameter has been supplied then map by name
            else if (recordTypeNames != null)
            {
                queried.put(rt.DeveloperName, rt);
            }                                
            // otherwise add to the record types list in the order the query found the records
            else    
            {
                RecordTypes.add(rt);
                RecordTypeOptions.add(new SelectOption(rt.Id, rt.Name));    
                
                // default to the first record
                if (SelectedRecordType == null)                    
                {
                    SelectedRecordType = rt.Id;
                }                                        
            }                                                                                                   
        }  
        
        // if we're filtering on id or name then loop through the respective parameter and add to the list
        String param;
        if (recordTypeIds != null)                
        {
            param = ApexPages.CurrentPage().getParameters().get('rid');
        }
        else if (recordTypeNames != null)
        {
            param = ApexPages.CurrentPage().getParameters().get('rn');
        }
        
        // if we have a parameter to filter by then do so
        if (param != null)
        {
            // loop through the entries in the parameter
            for (String p : param.split(';', -1))
            {
                RecordTypes.add(queried.get(p));
                RecordTypeOptions.add(new SelectOption(queried.get(p).Id, queried.get(p).Name));     
                
                // default to the first record
                if (SelectedRecordType == null)                    
                {
                    SelectedRecordType = queried.get(p).Id;
                }
            }                    
        }                 
    }

    public PageReference ClickContinue()
    {
        try
        {
            // create the pagereference we'll be redirecting to
            PageReference ref = new PageReference('/' + ApexPages.CurrentPage().getParameters().get('oid') + '/e');
            ref.getParameters().put('RecordType', SelectedRecordType);
            
            // add other parameters that were passed to this page
            for (String k : ApexPages.CurrentPage().getParameters().keySet())
            {
                // if it's any of the parameters for this page then skip
                if (k == 'o'
                    || k == 'oid'
                    || k == 'rid'
                    || k == 'rn')
                {
                    continue;
                }                    
            
                ref.getParameters().put(k, ApexPages.CurrentPage().getParameters().get(k));
            }                        

            system.debug('ref: ' + ref);
            return ref;
        }
        catch(Exception e)
        {
            return A2HCException.formatException(e);
        }    
    }

    public PageReference Cancel()
    {
        try
        {
            return new PageReference(ApexPages.CurrentPage().getParameters().get('retURL'));
        }
        catch(Exception e)
        {
            return A2HCException.formatException(e);
        }    
    }

}