public class RCRServiceOrderFormulaReplacement
{
    public static Map<String, QueueSobject> Queues
    {
        get
        {
            if(Queues == null)
            {
                Queues = new Map<String, QueueSobject>();
                for(QueueSobject q : [SELECT Id,
                                                Queue.Name,
                                                Queue.Id
                                        FROM    QueueSobject])
                {
                    Queues.put(q.Queue.Name, q);
                }
            }
            return Queues;
        }
        private set;
    }


    public static void Replace(List<RCR_Contract__c> serviceOrders, Map<Id, RCR_Contract__c> oldValues)
    {                               
//        Map<Id, RecordType> recordTypes = new Map<Id, RecordType> ([SELECT Id,
//                                                                           Name
//                                                                    FROM RecordType]);
//
//        Map<String, QueueSobject> queues = new Map<String, QueueSobject>();
//        for (QueueSobject q : [SELECT Id,
//                                      Queue.Name,
//                                      Queue.Id
//                               FROM QueueSobject])
//        {
//            queues.put(q.Queue.Name, q);
//        }
            
        Set<Id> clientIds = new Set<Id>();
        Set<Id> accountIds = new Set<Id>();
        Set<Id> rcrOfficeTeamIds = new Set<Id>();
        Set<Id> sourceRecordIds = new Set<Id>();
        Set<Id> planActionIds = new Set<Id>();
        Map<ID, List<RCR_Contract_Scheduled_Service__c>> planActionCSSByPlanActionId = new Map<ID, List<RCR_Contract_Scheduled_Service__c>>();
        Map<ID, Decimal> planActionsValuesById = new Map<ID, Decimal>();
        Map<ID, Plan_Action__c> planActions = new Map<Id, Plan_Action__c>();
        Boolean findMaxSeqNum = false;

        //RecordType lSAParticipantRecordType = A2HCUtilities.getRecordType('Referred_Client__c', APSRecordTypes.Client_LSAParticipant );
        RecordTypeInfo lSAParticipantRecordType = Referred_Client__c.SObjectType.getDescribe().getRecordTypeInfosByName().get(APSRecordTypes.Client_LSAParticipant);
        RecordTypeInfo newRequestRecordType = RCR_Contract__c.SObjectType.getDescribe().getRecordTypeInfosByName().get(APSRecordTypes.RCRServiceOrder_NewRequest);
        for (RCR_Contract__c so : serviceOrders)
        {
            // if the service order has not changed then skip this record
            if (so.Id != null
                && so == oldValues.get(so.Id))
            {
                continue;
            }
        
            /* Other_Service_Providers_Required validation rule
            NOT(ISPICKVAL(Funding_Source__c, 'Direct Contract')) && 
            ((ISNEW() || DATEVALUE(CreatedDate) > DATE(2013,11,24)) && 
            RecordType.Name == 'New Request' && 
            NOT(ISBLANK(Account__c)) && 
            NOT(No_Other_Providers_Servicing_This_Area__c) && 
            (ISBLANK(Other_Service_Provider_Considered_1__c) || 
            ISBLANK(Other_Service_Provider_Considered_2__c)))
            */
            if (so.Funding_Source__c != 'Direct Contract'
                && (so.Id == null || so.CreatedDate > Date.newInstance(2013, 11, 24))
                //&& recordTypes.get(so.RecordTypeId).Name == 'New Request'
                && so.RecordTypeId == newRequestRecordType.getRecordTypeId()
                && so.Account__c != null
                && !so.No_Other_Providers_Servicing_This_Area__c
                && (so.Other_Service_Provider_Considered_1__c == null || so.Other_Service_Provider_Considered_2__c == null))
            {
                so.addError('Both Other Service Provider Considered are required.');
            }
            
            clientIds.add(so.Client__c);            
            
            if (so.Is_IF__c == null || so.Masterpiece_Vendor_Code__c == null)
            {    
                accountIds.add(so.Account__c);                
            }
            
            if (so.Service_Order_Number__c == null)
            {
                findMaxSeqNum = true;
            } 
            
            rcrOfficeTeamIds.add(so.RCR_Office_Team__c);
            
            if (so.Id == null || so.Source_Record__c != oldValues.get(so.Id).Source_Record__c)
            {
                sourceRecordIds.add(so.Source_Record__c);
            }
            
//            if (so.Id == null
//                || so.Plan_Action__c != oldValues.get(so.Id).Plan_Action__c
//                || so.Plan_Action_Estimated_Cost_Update__c != oldValues.get(so.Id).Plan_Action_Estimated_Cost_Update__c
//                || so.Plan_Action_Total_Service_Order_Cost__c != oldValues.get(so.Id).Plan_Action_Total_Service_Order_Cost__c
            if(so.Plan_Action__c != null)
            {
                planActionIds.add(so.Plan_Action__c);
            }
        }
        
        Map<Id, Referred_Client__c> clients = new Map<Id, Referred_Client__c>();        
        if (clientIds.size() > 0)
        {
            clients = new Map<Id, Referred_Client__c> ([SELECT Id,
                                                               Address__c,
                                                               IF_Number__c,
                                                               Allocated_to__c,
                                                               RecordTypeId,
                                                                RecordType.Name
                                                        FROM Referred_Client__c
                                                        WHERE Id IN :clientIds]);
        }                                                                                    
        
        Map<Id, Account> accounts = new Map<Id, Account>();
        if (accountIds.size() > 0)
        {
            accounts = new Map<Id, Account> ([SELECT Id,
                                                     Available_To__c,
                                                     Is_IF__c,
                                                     Masterpiece_Vendor_Code__c
                                              FROM Account
                                              WHERE Id IN :accountIds]); 
        }                                                               

        Map<Id, RCR_Office_Team__c> rcrOfficeTeams = new Map<Id, RCR_Office_Team__c>();
        if (rcrOfficeTeamIds.size() > 0)
        {
            rcrOfficeTeams = new Map<Id, RCR_Office_Team__c> ([SELECT Id,
                                                                      Regional_Manager__c,
                                                                      Team_Manager_Name__c
                                                               FROM RCR_Office_Team__c
                                                               WHERE Id IN :rcrOfficeTeamIds]); 
        }                                                                                           
        
        Map<Id, RCR_Contract__c> sourceRecords = new Map<Id, RCR_Contract__c>();
        if (sourceRecordIds.size() > 0)
        {
            sourceRecords = new Map<Id, RCR_Contract__c> ([SELECT Id,
                                                                  Total_Adhoc_Activity_Cost__c,
                                                                  Total_Scheduled_Service_Cost__c
                                                           FROM RCR_Contract__c
                                                           WHERE Id IN :sourceRecordIds]);      
        }
 system.debug(planActionIds);
        if (planActionIds.size() > 0)
        {
            planActions = new Map<Id, Plan_Action__c> ([SELECT Id,
                                                               Estimated_Cost__c,
                                                               Approved__c
                                                        FROM    Plan_Action__c
                                                        WHERE   Id IN :planActionIds]);

           for(RCR_Contract_Scheduled_Service__c css : [SELECT ID,
                                                                RCR_Contract__r.Plan_Action__c,
                                                                Total_Cost__c
                                                        FROM    RCR_Contract_Scheduled_Service__c
                                                        WHERE   RCR_Contract__r.Plan_Action__c IN :planActionIds AND
                                                                RCR_Contract__r.RecordType.Name = 'Service Order'])
            {
                if(!planActionCSSByPlanActionId.containsKey(css.RCR_Contract__r.Plan_Action__c))
                {
                    planActionCSSByPlanActionId.put(css.RCR_Contract__r.Plan_Action__c, new List<RCR_Contract_Scheduled_Service__c>());
                }
                planActionCSSByPlanActionId.get(css.RCR_Contract__r.Plan_Action__c).add(css);
            }
        }
                                                                    
        // find the max service order number that exists
        Integer seqNum = 0;
        if (findMaxSeqNum)
        {
            for (RCR_Contract__c so : [SELECT Id,
                                              Manual_Sequence_Number__c
                                       FROM RCR_Contract__c 
                                       WHERE Manual_Sequence_Number__c != null
                                       ORDER BY Manual_Sequence_Number__c DESC
                                       LIMIT 1])
            {
                seqNum = Integer.valueOf(so.Manual_Sequence_Number__c);
            }                                                      
        }                                                    

        for (RCR_Contract__c so : serviceOrders)
        {     
            // if the service order has not changed then skip this record
            if (so.Id != null
                && so == oldValues.get(so.Id))
            {
                continue;
            }
         
            // Client_Address__c: Client__r.Address__c
            so.Client_Address__c = (clients.containsKey(so.Client__c) ? clients.get(so.Client__c).Address__c : null);
            so.Service_Planner__c = (clients.containsKey(so.Client__c) ? clients.get(so.Client__c).Allocated_to__c : null);
                         
            // Is_IF__c: Account__r.Is_IF__c
            if (so.Is_IF__c == null)
            {
                so.Is_IF__c = (accounts.containsKey(so.Account__c) ? accounts.get(so.Account__c).Is_IF__c : false);
            }
        
            // Masterpiece_Vendor_Code__c: IF(Is_IF__c && NOT(ISBLANK(Vendor_Code__c)), Vendor_Code__c, Account__r.Masterpiece_Vendor_Code__c)
            if (so.Masterpiece_Vendor_Code__c == null)
            {
                so.Masterpiece_Vendor_Code__c = (so.Is_IF__c && so.Vendor_Code__c != null ? so.Vendor_Code__c : (accounts.containsKey(so.Account__c) ? accounts.get(so.Account__c).Masterpiece_Vendor_Code__c : null));
            }
                    
            // Regional_Manager_Name__c: RCR_Office_Team__r.Regional_Manager__c
            so.Regional_Manager_Name__c = (rcrOfficeTeams.containsKey(so.RCR_Office_Team__c) ? rcrOfficeTeams.get(so.RCR_Office_Team__c).Regional_Manager__c : null);
        
            // Team_Manager_Name__c: RCR_Office_Team__r.Team_Manager_Name__c            
            so.Team_Manager_Name__c = (rcrOfficeTeams.containsKey(so.RCR_Office_Team__c) ? rcrOfficeTeams.get(so.RCR_Office_Team__c).Team_Manager_Name__c : null);            
        
            // Original_Adhoc_Service_Cost__c: Source_Record__r.Total_Adhoc_Activity_Cost__c
            if (so.Source_Record__c == null)
            {
                so.Original_Adhoc_Service_Cost__c = null;
            }
            else
            {                      
                so.Original_Adhoc_Service_Cost__c = (sourceRecords.containsKey(so.Source_Record__c) ? sourceRecords.get(so.Source_Record__c).Total_Adhoc_Activity_Cost__c : so.Original_Adhoc_Service_Cost__c);
            }
            
            // Original_Scheduled_Service_Cost__c: Source_Record__r.Total_Scheduled_Service_Cost__c
            if (so.Source_Record__c == null)
            {
                so.Original_Scheduled_Service_Cost__c = null;
            }
            else
            {
                so.Original_Scheduled_Service_Cost__c = (sourceRecords.containsKey(so.Source_Record__c) ? sourceRecords.get(so.Source_Record__c).Total_Scheduled_Service_Cost__c : so.Original_Scheduled_Service_Cost__c);
            }

            // sequence number replacement
            if (so.Manual_Sequence_Number__c == null)
            {
                seqNum++;
                so.Manual_Sequence_Number__c = seqNum;                
                
                // all service orders need to be prefixed with LSA
                System.debug('clients: ' + clients);
                String soPrefix = 'LSA';
                if(!Test.isRunningTest() && clients.get(so.Client__c).RecordType.Name == APSRecordTypes.Client_ManagedClient)
                {
                    soPrefix = 'EXT';
                }
                so.Service_Order_Number__c = soPrefix + ('00000' + String.valueOf(so.Manual_Sequence_Number__c)).right(5);
                if (so.Name == null)
                {
                    so.Name = so.Service_Order_Number__c;
                }
            } 
            
//            so.Record_Type_Name__c =  recordTypes.get(so.RecordTypeId).Name;
            so.Record_Type_Name__c = RCR_Contract__c.SObjectType.getDescribe().getRecordTypeInfosById().get(so.RecordTypeId).getName();

            /* Manual CSA Acceptance workflow
            RecordType.Name == 'Service Order' && 
            ISPICKVAL(CSA_Status__c, 'Pending Acceptance') && 
            NOT(ISBLANK(Date_Approved__c)) && 
            ISCHANGED(CSA_Acceptance_Date__c) && 
            NOT(ISBLANK(CSA_Acceptance_Date__c))   
            */ 
            if (so.Record_Type_Name__c == 'Service Order'
                && so.CSA_Status__c == 'Pending Acceptance'
                && so.Date_Approved__c != null
                && so.CSA_Acceptance_Date__c != null
                && oldValues != null
                && oldValues.containsKey(so.Id)
                && so.CSA_Acceptance_Date__c != oldValues.get(so.Id).CSA_Acceptance_Date__c)
            {
                //so.Date_Approved__c = Datetime.now();
                so.CSA_Status__c = 'Accepted';
                so.Visible_to_Service_Providers__c = true;
            }
            
            /* Withdrawn workflow
            ISBLANK(PRIORVALUE(Requested_Withdrawl_Date__c)) && NOT(ISBLANK(Requested_Withdrawl_Date__c))
            */
            if (oldValues != null
                && oldValues.containsKey(so.Id)
                && oldValues.get(so.Id).Requested_Withdrawl_Date__c == null
                && so.Requested_Withdrawl_Date__c != null)
            {
                so.OwnerId = queues.get('RCR Service Order Withdrawal').Queue.Id;   
            }
            // populate the plan action estimated cost field
            if (so.Id == null
                || so.Plan_Action__c != oldValues.get(so.Id).Plan_Action__c
                || so.Plan_Action_Estimated_Cost_Update__c != oldValues.get(so.Id).Plan_Action_Estimated_Cost_Update__c
                || so.Plan_Action_Total_Service_Order_Cost__c != oldValues.get(so.Id).Plan_Action_Total_Service_Order_Cost__c
                || planActions.containsKey(so.Plan_Action__c))
            {
                so.Plan_Action_Estimated_Cost__c = 0;
                so.Plan_Action_Total_Service_Order_Cost__c = 0;
                List<RCR_Contract_Scheduled_Service__c> paCSS = planActionCSSByPlanActionId.get(so.Plan_Action__c);
                Plan_Action__c pa = planActions.get(so.Plan_Action__c);
                if(pa != null && paCSS != null)
                {
                    Decimal total = 0;
                    for(RCR_Contract_Scheduled_Service__c css : paCSS)
                    {
                        total += css.Total_Cost__c;
                    }
                    so.Plan_Action_Total_Service_Order_Cost__c = total;
                    so.Plan_Action_Estimated_Cost__c = pa.Estimated_Cost__c;
                    so.Plan_Action_Approved__c = pa.Approved__c;

                    planActionsValuesById.put(pa.ID, total);
                }
            }
        }
system.debug(planActionsValuesById);
        if(!planActionsValuesById.isEmpty() && Trigger.isUpdate)
        {
            ServiceOrderFuture.updatePlanActionTotalServiceOrderCost(planActionsValuesById, Trigger.newMap.keySet());
        }
    
    }

}