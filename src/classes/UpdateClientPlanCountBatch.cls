/**
 * Created by cain on 31/05/2016.
 */

public  class UpdateClientPlanCountBatch
        implements Database.Batchable<sObject>
{
    public Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator([SELECT Id, 
                                                (SELECT ID 
                                                FROM    Plans__r 
                                                WHERE   (
                                                            Plan_Status__c = :APS_PicklistValues.ParticipantPlan_PlanStatus_Approved OR 
                                                            Plan_Status__c = :APS_PicklistValues.ParticipantPlan_PlanStatus_Amendment
                                                        ) 
                                                        AND Plan_End_Date__c > TODAY
                                                LIMIT 1) 
                                        FROM    Referred_Client__c]);
    }

    public void execute(Database.BatchableContext bc,  List<Referred_Client__c> scope)
    {
        for(Referred_Client__c  c : scope)
        {
            c.Has_Participant_Plan__c = !c.Plans__r.isEmpty();
        }
        Database.update(scope, false);
    }

    public void finish(Database.BatchableContext bc)
    {

    }
}