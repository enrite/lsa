public with sharing class LSAContractPDFExtension
{
    public LSAContractPDFExtension(ApexPages.StandardController c)
    {
        record = (RCR_Contract__c)c.getRecord();
    }

    private RCR_Contract__c record;
    
    public List<Service> Services
    {
        get
        {
            Services = new List<Service>();
            Set<String> cssIds = new Set<String>();
            Set<Id> serviceIds = new Set<Id>();
                            
            for (RCR_Contract_Scheduled_service__c css : [SELECT Id,
                                                                 RCR_Service__c,
                                                                 RCR_Service__r.Name,
                                                                 RCR_Service__r.Description__c,
                                                                 RCR_Service__r.Display_Gazetted_Rates__c,
                                                                 Use_Negotiated_Rates__c,
                                                                 Negotiated_Rate_Standard__c,
                                                                 Negotiated_Rate_Public_Holidays__c,
                                                                 Start_Date__c,
                                                                End_Date__c,
                                                                Original_End_Date__c,
                                                                 Total_Quantity__c,
                                                                 Total_Cost__c                                                                
                                                          FROM RCR_Contract_Scheduled_service__c
                                                          WHERE RCR_Contract__c = :record.Id
                                                          ORDER BY Start_Date__c])
            {
                Services.add(new Service(css));
                cssIds.add(css.Id);
                serviceIds.add(css.RCR_Service__c);            
            }                                      
            
            Map<Id, Schedule__c> scheds = new Map<Id, Schedule__c>();
            for (Schedule__c s : [SELECT Id,
                                         Name,
                                         Number_of_weeks_in_period__c,
                                         Quantity_Per_Period__c
                                  FROM Schedule__c
                                  WHERE Name IN :cssIds])
            {
                scheds.put(s.Name, s);
            }                                  
            
            Map<Id, RCR_Service__c> svcs = new Map<Id, RCR_Service__c> ([SELECT Id,
                                                                                (SELECT Rate__c,
                                                                                        Start_Date__c,
                                                                                        End_Date__c
                                                                                 FROM   RCR_Service_Rates__r)                                                                                 
                                                                         FROM RCR_Service__c r
                                                                         WHERE Id IN :serviceIds]);                                             
                    
            for (Service s : Services)
            {
                if (scheds.containsKey(s.ScheduledService.Id))
                {
                    s.Schedule = scheds.get(s.ScheduledService.Id);
                }
            
                if (svcs.containsKey(s.ScheduledService.RCR_Service__c) && !s.ScheduledService.Use_Negotiated_Rates__c)
                {
                    for (RCR_Service_Rate__c sr : svcs.get(s.ScheduledService.RCR_Service__c).RCR_Service_Rates__r)
                    {
                        if (sr.Start_Date__c <= s.ScheduledService.Original_End_Date__c && sr.End_Date__c >= s.ScheduledService.Start_Date__c)
                        {
                            s.ServiceRate = sr;
                            break;
                        }
                    }  
                }                        
            }  
            
            return Services;                                                       
        }
        set;
    }
    
    public String ProgramSupportOfficer
    {
        get
        {
            for (User u : [SELECT Id,
                                  Name
                           FROM User
                           WHERE Id IN (SELECT UserOrGroupId
                                        FROM GroupMember
                                        WHERE Group.DeveloperName = 'Program_Support_Officers')
                           LIMIT 1])
            {
                return u.Name;
            }
            
            return null;        
        }        
    }

//    private Date SDSDate
//    {
//        get
//        {
//            return Date.today().toStartOfMonth().addMonths(1);
//        }
//    }
//
//    public String SDSStartDate
//    {
//        get
//        {
//            return ScheduledService.Start_Date__c.addMonths(1);
//        }
//    }

    private class Service
    {
        public Service(RCR_Contract_Scheduled_Service__c css)
        {
            ScheduledService = css;
            Schedule = new Schedule__c();            
            ServiceRate = new RCR_Service_Rate__c();
            if(css.Use_Negotiated_Rates__c)
            {
                ServiceRate.Rate__c = css.Negotiated_Rate_Standard__c;
            }
        }
        
        public RCR_Contract_Scheduled_Service__c ScheduledService { get; set; }
        public Schedule__c Schedule { get; set; }
        public RCR_Service_Rate__c ServiceRate { get; set; }

        public String StartDate
        {
            get
            {
                return (ScheduledService == null || ScheduledService.Start_Date__c == null) ? '' : ScheduledService.Start_Date__c.addMonths(1).format();
            }
        }

        public String EndDate
        {
            get
            {
                return (ScheduledService == null || ScheduledService.End_Date__c == null) ? '' : ScheduledService.End_Date__c.format();
            }
        }

        public String Period
        {
            get
            {
                if(Schedule == null || Schedule.Number_of_weeks_in_period__c == null)
                {
                    return '';
                }
                else if(Schedule.Number_of_weeks_in_period__c == '1 Week')
                {
                    return 'Weekly';
                }
                else if(Schedule.Number_of_weeks_in_period__c == '2 Weeks')
                {
                    return 'Fortnightly';
                }
                else if(Schedule.Number_of_weeks_in_period__c == '3 Weeks')
                {
                    return '3 Weekly';
                }
                else if(Schedule.Number_of_weeks_in_period__c == '4 Weeks')
                {
                    return '4 Weekly';
                }
                else
                {
                    return Schedule.Number_of_weeks_in_period__c;
                }
            }
        }

    }
}