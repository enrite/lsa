public virtual class TriggerBase 
{
	public TriggerBase(Map<ID, sObject> pTriggerNewMap, 
						Map<ID, sObject> pTriggerOldMap)
	{
		isUpdate = true;
	}
	
	public TriggerBase(Map<ID, sObject> pTriggerNewMap)
	{
		isInsert = true;
	}
	
	protected Boolean isUpdate = false;
	protected Boolean isInsert = false;
	protected Boolean isDelete = false;
}