public class LPAuthoriseInvoiceJSON
{
    public claim[] claims;

    public class claim
    {
        public String claimKey;
        public String action;
    }

    public static String serialise(RCR_Invoice__c invoice, String action)
    {
        LPAuthoriseInvoiceJSON invAuthJSON = new LPAuthoriseInvoiceJSON();
        invAuthJSON.claims = new List<LPAuthoriseInvoiceJSON.claim>();
        for(RCR_Invoice_Item__c item : invoice.RCR_Invoice_Items__r)
        {
            LPAuthoriseInvoiceJSON.claim claim = new LPAuthoriseInvoiceJSON.claim();
            claim.claimKey = item.LP_Claim_Key__c;
            claim.action = action;
            invAuthJSON.claims.add(claim);
        }
        return JSON.serialize(invAuthJSON);
    }
}