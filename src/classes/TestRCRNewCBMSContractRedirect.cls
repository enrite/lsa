@isTest
private class TestRCRNewCBMSContractRedirect 
{
    private static testmethod void test()
    {
        RCRNewCBMSContractRedirect c = new RCRNewCBMSContractRedirect(new ApexPages.StandardController(new RCR_Contract__c()));
        c.redir(); 
        
        c.forceTestError = true;
        try
        {
        	c.redir();
        }
        catch(Exception ex)
		{
			system.assert(ex instanceof A2HCException); 
		}	
    }
}