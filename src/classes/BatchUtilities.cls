public without sharing class BatchUtilities 
{		
	public static Boolean isScheduleWaiting(String scheduleID)	
	{
		if(scheduleID == null)
		{
			return false;
		}
		for(CronTrigger ct : [SELECT 	State,
										NextFireTime
						         FROM   CronTrigger
						         WHERE	Id = :(ID.valueOf(scheduleID))])
         {
         	// if waiting, allow a buffer of 5(?) seconds or so
         	Integer waitTolerance = Batch_Size__c.getInstance('Schedule_Wait_Tolerance').Records__c.intValue();
         	if(ct.State == 'WAITING' && ct.NextFireTime.addSeconds(waitTolerance) > DateTime.now()) 
         	{
         		return true;
         	}
         }
         return false;  
	}
	
	public static String getNextScheduleID(Schedulable schedulableObject, Type batchableObjectType, String scheduleName)
	{
		return getNextScheduleID(schedulableObject, batchableObjectType, scheduleName, null);
	}
	
	public static String getNextScheduleID(Schedulable schedulableObject, 
												Type batchableObjectType, 
												String scheduleName, 
												Integer addMinutes)
	{
		String scheduleID = getNextScheduleID(batchableObjectType.getName());
		// If scheduleID is still null we couldn't find an existing schedule that hadn't already been executed. Need to create a new one.
        if(scheduleID == null)
        {
        	DateTime n = DateTime.now();
            Integer min = n.minute() + (addMinutes == null ? 0 : addMinutes);
            Integer hour = n.hour();
            String day = n.day().format();
            String month = n.month().format();
            String year =  n.year().format();
            // Determine Schedule delay time interval (minutes).
            min = getAdjustedMinute(getRunningJobs(batchableObjectType.getName()), min + (addMinutes == null ? 0 : addMinutes));
            if(min > 59)
            {
            	min = Math.min(59, Math.abs(60 - min)); 
            	hour++;
            }           
            // Build cron string and create Schedule.
            String cronStr = '0 ' + min.format() + ' ' + hour.format() + ' ' + day + ' ' + month + ' ? ' + year.replace(',', '');
			scheduleID = Test.isRunningTest() ? 'test' : system.schedule(scheduleName + DateTime.now().getTime().format().replace(',', ''), cronStr, schedulableObject);

			Schedule_Batch__c sb = new Schedule_Batch__c(Schedule__c = scheduleID, Batch_Class_Name__c = batchableObjectType.getName());
			upsert sb Schedule_Batch__c.sObjectType.Schedule__c; 	
        } 
        return scheduleID.length() > 15 ? scheduleID.subString(0, 15) : scheduleID;
	}
	
	public static String getNextScheduleID(Database.Batchable<sObject> batchableObject, 
												String batchableObjectName, 
												String scheduleName, 
												Integer addMinutes,
												Integer batchSize)
	{
//		object o = Type.forName(batchableObjectName);
//		system.debug(o);
//		system.debug(o.class);
//		system.debug(batchableObject.class);
//		if(!(Type.forName(batchableObjectName) instanceof Database.Batchable<sObject>))
//		{
//			throw new A2HCException('Class name does not match batchable object type.');
//		}
		String scheduleID = getNextScheduleID(batchableObjectName);
    	if(scheduleID == null)
        {
			scheduleID = System.scheduleBatch(batchableObject, 
												scheduleName + DateTime.now().getTime().format().replace(',', ''), 
												getAdjustedMinute(getRunningJobs(batchableObjectName), (addMinutes == null ? 0 : addMinutes)), 
												batchSize); 
																							
			Schedule_Batch__c sb = new Schedule_Batch__c(Schedule__c = scheduleID, Batch_Class_Name__c = batchableObjectName);
			upsert sb Schedule_Batch__c.sObjectType.Schedule__c; 	
        }
        return scheduleID.length() > 15 ? scheduleID.subString(0, 15) : scheduleID;
	}
	
	public static void cleanUpSchedule(String scheduleID)
	{
		try
		{
			List<Schedule_Batch__c> srb = [SELECT  Id
			                                FROM    Schedule_Batch__c
			                                WHERE   Schedule__c = :scheduleID OR
			                                		CreatedDate < :Date.today().addDays(-7)
	                                		LIMIT 100];
		    delete srb;			 
		}
		catch(Exception ex)
		{}		
	}
	
	private static ID getNextScheduleID(String batchableObjectName)
	{
		String scheduleID = null;
		Set<String> schedules = new Set<string>();
			
    	// Look for any existing Schedules 
    	for(Schedule_Batch__c s : [SELECT Schedule__c,
										 Case_safe_schedule_ID__c,
										 Batch_Job_ID__c
		 						  FROM	 Schedule_Batch__c
		 						  WHERE	 Batch_Class_Name__c = :batchableObjectName])
		{			
			schedules.add(s.Case_safe_schedule_ID__c);
		}
		for(CronTrigger c : [SELECT Id,
    								CronExpression,
    								TimesTriggered,
    								NextFireTime,
    								PreviousFireTime,
    								State
                             FROM   CronTrigger
                             WHERE	Id IN :schedules AND
                             		State = 'WAITING'
                             ORDER BY NextFireTime DESC])
        {
        	// If the schedule hasn't been executed yet get its ID.
			// Only get the Id of the latest WAITING Schedule
			string cronID = string.valueOf(c.Id);
			if(cronID.length() == 18)
			{
				cronID = cronID.subString(0, 15);
			}
			scheduleID = cronID;
        }
        return scheduleID;
	}
	private static Integer getRunningJobs(String batchableObjectName)
	{
		return  [SELECT 	COUNT()
				   FROM 	AsyncApexJob
				   WHERE 	ApexClass.Name = :batchableObjectName
				   AND		Status IN ('Processing', 'Preparing')];
	}
	
	private static Integer getAdjustedminute(Integer runningJobs, Integer currentMinute)
	{
		if(runningJobs > 5)
        {
        	currentMinute = currentMinute + Batch_Size__c.getInstance('RCR_Invoice_Schedule_Long_Interval').Records__c.intValue();
        }
        else if(runningJobs > 3)
        {
        	currentMinute = currentMinute + Batch_Size__c.getInstance('RCR_Invoice_Schedule_Medium_Interval').Records__c.intValue();
        }
        else
        {
        	currentMinute = currentMinute + Batch_Size__c.getInstance('RCR_Invoice_Schedule_Short_Interval').Records__c.intValue();
        }
        return currentMinute;
	}
}