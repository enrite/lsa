public with sharing class PlanApprovalExtension extends A2HCPageBase
{
    public PlanApprovalExtension(ApexPages.StandardController ctrlr)
    {
        controller = ctrlr;
        planApproval = (Plan_Approval__c) ctrlr.getRecord();
        planId = planApproval.Participant_Plan__c;
        if(planApproval.ID == null)
        {
            for(String s : ApexPages.currentPage().getParameters().keySet())
            {
                if(s != null && s.endsWith('_lkid'))
                {
                    planId = getParameter(s);
                    break;
                }
            }
        }
        ApprovalInProgress = false;
        for(Plan_Approval__c pa : [SELECT  ID
                                    FROM    Plan_Approval__c
                                    WHERE   Participant_Plan__c = :planId AND
                                            ID != :planApproval.ID AND
                                            Plan_Approval_Date__c = null])
        {
            ApprovalInProgress = true;
        }
        for(Plan__c pl : [SELECT ID,
                                Plan_Status__c,
                                Estimated_Plan_Costs__c,
                                Do_not_create_Amendment__c,
                                RecordType.DeveloperName
                        FROM    Plan__c
                        WHERE   ID = :planId])
        {
            approvalPlan = pl;
        }
        showDelegationFields();
    }

    private ApexPages.StandardController controller;
    private Plan_Approval__c planApproval;
    private ID planId;
    private Plan__c approvalPlan;

    private final String RECOMMENDED_TO_DELEGATE = 'Recommended to Delegate';
    private final String RECOMMENDED_TO_MANAGER_ASD = 'Recommended to Manager ASD';
    private final String REFERRED_BACK_TO_SERVICE_PLANNER = 'Referred back to Service Planner';
    private final String REFERRED_BACK_TO_LEAD_SERVICE_PLANNER = 'Referred back to Delegate';
    private final String APPROVED = 'Approved';
    private final String NOT_APPROVED = 'Not Approved';

    private final String RTDEV_SERVICE_PLANNER_RECOMMENDATION = 'Service_Planner_Recommendation';
    private final String RTDEV_LEAD_SERVICE_PLANNER_RECOMMENDATION = 'Lead_Service_Planner_Recommendation';
    private final String RTDEV_MANAGER_ASD_RECOMMENDATION = 'Manager_ASD_Recommendation';
    private final String RTDEV_CHIEF_EXECUTIVE_APPROVAL = 'Chief_Executive_Approval';

    private final String QUALITY_ATTENDANTCARE = 'Attendant Care & Support';
    private final String QUALITY_HOMEMODIFICATIONS = 'Home Modifications';

    public Plan_Approval_Delegation__c UserDelegation
    {
        get
        {
            if(UserDelegation == null)
            {
                UserDelegation = new Plan_Approval_Delegation__c();
                for(Plan_Approval_Delegation__c pad :[SELECT Id,
                                                            Name,
                                                            Limit__c,
                                                            Role__c
                                                    FROM    Plan_Approval_Delegation__c
                                                    WHERE   Role__c = :CurrentUser.UserRole.Name AND
                                                            Current__c = true
                                                    LIMIT 1])
                {
                    UserDelegation = pad;
                }
            }
            return UserDelegation;
        }
        set;
    }

    private Map<String, RecordType> PlanApprovalRecordTypes
    {
        get
        {
            if(PlanApprovalRecordTypes == null)
            {
                PlanApprovalRecordTypes = getRecordTypes(Plan_Approval__c.SObjectType);
            }
            return PlanApprovalRecordTypes;
        }
        set;
    }

    private Map<String, RecordType> PlanActionRecordTypes
    {
        get
        {
            if(PlanActionRecordTypes == null)
            {
                PlanActionRecordTypes = getRecordTypes(Plan_Action__c.SObjectType);
            }
            return PlanActionRecordTypes;
        }
        set;
    }

    private Map<String, RecordType> getRecordTypes(SObjectType objType)
    {
        Map<String, RecordType> recTypes = new Map<String, RecordType>();
        for(RecordType rt : [SELECT Id,
                                    Name,
                                    DeveloperName
                            FROM RecordType
                            WHERE sObjectType = :objType.getDescribe().getName()])
        {
            recTypes.put(rt.DeveloperName, rt);
        }
        return recTypes;
    }

    public Boolean ApprovalInProgress
    {
        get;
        private set;
    }

    public Boolean ApprovalStage
    {
        get {
            return planApproval.RecordType.DeveloperName == RTDEV_LEAD_SERVICE_PLANNER_RECOMMENDATION ||
                    planApproval.RecordType.DeveloperName == RTDEV_MANAGER_ASD_RECOMMENDATION ||
                    planApproval.RecordType.DeveloperName == RTDEV_CHIEF_EXECUTIVE_APPROVAL;
        }
    }

    public Boolean ShowPlannerDelegation
    {
        get
        {
            if(WithinApprovalLevel)
            {
                return true;
            }
            if(approvalPlan.Plan_Status__c == 'Draft' && approvalPlan.RecordType.DeveloperName == 'MyPlan')
            {
                return true;
            }
            return false;//CurrentUser.Service_Planner_Delegation__c;
        }
    }

    public Boolean WithinApprovalLevel
    {
        get
        {
            return CurrentUser.Service_Planner_Delegation__c && UserDelegation.Limit__c != null && approvalPlan.Estimated_Plan_Costs__c <= UserDelegation.Limit__c;
        }
    }

    public Boolean ShowAttCareQualityApproval
    {
        get
        {
            return CurrentUser.Quality_Reviewer__c != null && CurrentUser.Quality_Reviewer__c.contains(QUALITY_ATTENDANTCARE);
        }
    }

    public Boolean ShowHomeModQualityApproval
    {
        get
        {
            return CurrentUser.Quality_Reviewer__c != null && CurrentUser.Quality_Reviewer__c.contains(QUALITY_HOMEMODIFICATIONS);
        }
    }

    public String SelectedServicePlannerApprovalOption
    {
        get;
        set;
    }

    public List<SelectOption> ServicePlannerApprovalOptions
    {
        get
        {
            if(ServicePlannerApprovalOptions == null)
            {
                ServicePlannerApprovalOptions = new List<SelectOption>();
                ServicePlannerApprovalOptions.add(new SelectOption('', '--None'));
                Boolean addApproved = true;
                if(approvalPlan.Plan_Status__c == 'Draft' && approvalPlan.RecordType.DeveloperName == 'MyPlan')
                {
                    addApproved = false;
                }
                if(addApproved)
                {
                    ServicePlannerApprovalOptions.add(new SelectOption(APPROVED, APPROVED));
                }
                ServicePlannerApprovalOptions.add(new SelectOption(RECOMMENDED_TO_DELEGATE, RECOMMENDED_TO_DELEGATE));
            }
            return ServicePlannerApprovalOptions;
        }
        private set;
    }

    public List<Plan_Action__c> NewPlanActions
    {
        get {
            if(NewPlanActions == null)
            {
                planId = planId == null ? planApproval.Participant_Plan__c : planId;
                NewPlanActions = [SELECT ID,
                                        Name,
                                        Plan_Approval__c,
                                        Ready_to_Approve__c,
                                        Pre_Defined_Services__c,
                                        Item_Description__c,
                                        Estimated_Cost__c,
                                        Amendment_Details__c,
                                        Total_Service_Order_Cost__c,
                                        Approved__c,
                                        Home_Mod_Quality_Recommendation__c,
                                        Att_Care_Support_Quality_Recommendation__c,
                                        Provider__r.Name,
                                        RecordTypeId,
                                        RecordType.Name,
                                        RecordType.DeveloperName
                                FROM    Plan_Action__c
                                WHERE   (Plan_Approval__c = :planApproval.ID AND
                                        Plan_Approval__c != null)
                                        OR
                                        (Participant_Plan__c = :planId AND
                                        Approved__c = false AND
                                        Ready_to_Approve__c = false)];
            }
            return NewPlanActions;
        }
        set;
    }

    public List<Plan_Action__c> PlanActionsForApproval
    {
        get {
            if(PlanActionsForApproval == null)
            {
                PlanActionsForApproval = getPlanActions();
            }
            return PlanActionsForApproval;
        }
        set;
    }

    public Boolean AttendantCare
    {
        get;
        private set;
    }

    public Boolean HomeModifications
    {
        get;
        private set;
    }

    public void showDelegationFields()
    {
        AttendantCare = false;
        HomeModifications = false;
        for(Plan_Action__c pa : getSelectedPlanActions())
        {
            if(pa.RecordTypeId == PlanActionRecordTypes.get('Attendant_Care_and_Support_MyPlan').ID ||
                    pa.RecordTypeId == PlanActionRecordTypes.get('Attendant_Care_and_Support').ID)
            {
                AttendantCare = true;
            }
            if(pa.Pre_Defined_Services__c == QUALITY_HOMEMODIFICATIONS)
            {
                HomeModifications = true;
            }
        }
system.debug('AttendantCare ' + AttendantCare);
system.debug('HomeModifications ' + HomeModifications);
    }

    public PageReference savePlanner() {
        Savepoint sp = Database.setSavepoint();
        try
        {
            Boolean actionSelected = false;
            for(Plan_Action__c pa : NewPlanActions)
            {
                if(pa.Ready_to_Approve__c)
                {
                    actionSelected = true;
                    break;
                }
            }
            if(!actionSelected)
            {
                return A2HCException.formatExceptionAndRollback(sp, 'At least one plan action must be selected');
            }

            showDelegationFields();

            Boolean complete = (ShowPlannerDelegation && String.isNotBlank(SelectedServicePlannerApprovalOption)) ||
                    (!ShowPlannerDelegation && planApproval.Service_Planner_Complete__c);
//            Boolean complete = !ShowPlannerDelegation && planApproval.Service_Planner_Complete__c;
            if(complete &&
                    (planApproval.RecordType.DeveloperName == null || planApproval.RecordType.DeveloperName == RTDEV_SERVICE_PLANNER_RECOMMENDATION))
            {
                Boolean updatePlan = false;
                for(Plan__c plan : [SELECT Id,
                                            Plan_Status__c
                                    FROM    Plan__c
                                    WHERE   Id = :planApproval.Participant_Plan__c
                                    AND     Do_not_create_Amendment__c = false])
                {
                    if(plan.Plan_Status__c == APPROVED)
                    {
                        plan.Plan_Status__c = 'Amendment';
                        updatePlan = true;
                    }
                    else if(plan.Plan_Status__c != 'Draft' && plan.Plan_Status__c != 'Amendment')
                    {
                        plan.Plan_Status__c = 'Draft';
                        updatePlan = true;
                    }
                    if(updatePlan)
                    {
                        update plan;
                    }
                }
            }
            if(complete && String.isEmpty(planApproval.Plan_Update_Required__c))
            {
                return A2HCException.formatExceptionAndRollback(sp, 'You must indicate if a Plan Update is Required.');
            }

            if(planApproval.ID == null)
            {
                planApproval.RecordTypeId = PlanApprovalRecordTypes.get(RTDEV_SERVICE_PLANNER_RECOMMENDATION).ID;// Plan_Approval__c.SObjectType.getDescribe().getRecordTypeInfosByName().get('Service Planner Recommendation').getRecordTypeId();
            }

            if(complete)
            {
                planApproval.Service_Planner_Complete__c = true;
            }
            setNextApprover();

            processApproval();

            upsert planApproval;
            if(complete)
            {
                PlanActionFuture.createApprovalAudits(new Set<ID>{planApproval.ID});
            }
            for(Plan_Action__c pa : NewPlanActions)
            {
                pa.Plan_Approval__c = pa.Ready_to_Approve__c ? planApproval.ID : null;
            }
            TriggerBypass.PlanAction = true;
            update NewPlanActions;
            TriggerBypass.PlanAction = false;

            return new PageReference('/' + planApproval.ID);
        }
        catch(Exception ex)
        {
            return A2HCException.formatExceptionAndRollback(sp, ex);
        }
    }

    public PageReference saveApproval()
    {
        Map<ID, Plan_Action__c> originalPlanActionsById = new Map<ID, Plan_Action__c>(getPlanActions());
        Savepoint sp = Database.setSavepoint();
        try
        {
            Boolean finalApproval =
//                    (planApproval.RecordType.DeveloperName == RTDEV_LEAD_SERVICE_PLANNER_RECOMMENDATION &&
//                            planApproval.Lead_Service_Planner_Complete__c == APPROVED)
//                    ||
                    (planApproval.RecordType.DeveloperName == RTDEV_MANAGER_ASD_RECOMMENDATION &&
                                    planApproval.Manager_ASD_Complete__c == APPROVED)
                    ||
                    (planApproval.RecordType.DeveloperName == RTDEV_CHIEF_EXECUTIVE_APPROVAL &&
                            planApproval.Chief_Executive_Decision__c == APPROVED);

            processApproval();
            for(Plan_Action__c pa : PlanActionsForApproval)
            {
                if(planApproval.RecordType.DeveloperName == RTDEV_LEAD_SERVICE_PLANNER_RECOMMENDATION &&
                        planApproval.Lead_Service_Planner_Complete__c == RECOMMENDED_TO_MANAGER_ASD)
                {
                    if(String.isNotBlank(planApproval.Home_Mod_Quality_Recommendation__c))
                    {
                        planApproval.Home_Mod_Quality_Recommend_Date__c = Datetime.now();
                        planApproval.Home_Mod_Quality_Recommend_By__c = CurrentUser.ID;

                        pa.Home_Mod_Quality_Recommendation__c = pa.Home_Mod_Quality_Recommendation__c == null ? '' : (pa.Home_Mod_Quality_Recommendation__c + '<br/>');
                        pa.Home_Mod_Quality_Recommendation__c += Date.today().format() + ' - ' + planApproval.Home_Mod_Quality_Recommendation__c;
                    }
                    if(String.isNotBlank(planApproval.Att_Care_Support_Quality_Recommendation__c))
                    {
                        planApproval.Att_Care_Support_Quality_Recommend_Date__c = Datetime.now();
                        planApproval.Att_Care_Support_Recommend_By__c = CurrentUser.ID;
                        pa.Att_Care_Support_Quality_Recommendation__c = pa.Att_Care_Support_Quality_Recommendation__c == null ? '' : (pa.Att_Care_Support_Quality_Recommendation__c + '<br/>');
                        pa.Att_Care_Support_Quality_Recommendation__c += Date.today().format() + ' - ' + planApproval.Att_Care_Support_Quality_Recommendation__c;
                    }
                }
                else if(!pa.Ready_to_Approve__c && originalPlanActionsById.get(pa.ID).Ready_to_Approve__c)
                {
                    planApproval.Approval_Audit__c = planApproval.Approval_Audit__c == null ? '' : planApproval.Approval_Audit__c;
                    planApproval.Approval_Audit__c += '\n' + pa.Name + ' - removed by ' + CurrentUser.Name + ' ' + Date.today().format();
                    pa.Plan_Approval__c = null;
                }
            }
            if(finalApproval)
            {
                PlanActionFuture.createApprovalAudits(new Set<ID>{planApproval.ID});
            }
            TriggerBypass.PlanAction = true;
            update PlanActionsForApproval;
            TriggerBypass.PlanAction = false;
            update planApproval;

            return new PageReference('/' + planApproval.ID);
        }
        catch(Exception ex)
        {
            for(Plan_Action__c pa : PlanActionsForApproval)
            {
                if(originalPlanActionsById.containsKey(pa.ID))
                {
                    pa.Approved__c = originalPlanActionsById.get(pa.ID).Approved__c;
                }
                else
                {
                    pa.Approved__c = false;
                }
            }
            return A2HCException.formatExceptionAndRollback(sp, ex);
        }
    }

    private void setNextApprover()
    {
        if(planApproval.Service_Planner_Complete__c && SelectedServicePlannerApprovalOption != APPROVED)
        {
            planApproval.Assigned_To__c = CurrentUser.ManagerId;
            for(User u : [SELECT ID,
                                Quality_Reviewer__c
                        FROM    User
                        WHERE   IsActive = true AND
                                Quality_Reviewer__c != null])
            {
                if(AttendantCare || (AttendantCare && HomeModifications))
                {
                    if(u.Quality_Reviewer__c.contains(QUALITY_ATTENDANTCARE))
                    {
                        planApproval.Assigned_To__c = u.ID;
                        break;
                    }
                }
                else if(HomeModifications)
                {
                    if(u.Quality_Reviewer__c.contains(QUALITY_HOMEMODIFICATIONS))
                    {
                        planApproval.Assigned_To__c = u.ID;
                        break;
                    }
                }
            }
        }
    }

    private List<Plan_Action__c> getPlanActions() {
        return [SELECT ID,
                        Name,
                        Ready_to_Approve__c,
                        Plan_Approval__c,
                        Pre_Defined_Services__c,
                        Item_Description__c,
                        Estimated_Cost__c,
                        Amendment_Details__c,
                        Total_Service_Order_Cost__c,
                        Approved__c,
                        Service_Planner_Conflict_of_Interest__c,
                        MSD_Conflict_Process_Complete__c,
                        Home_Mod_Quality_Recommendation__c,
                        Att_Care_Support_Quality_Recommendation__c,
                        LastModifiedDate,
                        Provider__r.Name,
                        RecordTypeId,
                        RecordType.Name,
                        RecordType.DeveloperName
                FROM Plan_Action__c
                WHERE Plan_Approval__c = :planApproval.ID AND
                Plan_Approval__c != null];
    }

    private void processApproval()
    {
        List<Plan_Action__c> planActions = new List<Plan_Action__c>();
        Decimal planActionTotal = 0;
       // Boolean delegatedApproval = CurrentUser.Service_Planner_Delegation__c && SelectedServicePlannerApprovalOption == APPROVED;
        for(Plan_Action__c pa : getSelectedPlanActions())
        {
            if(pa.Ready_to_Approve__c)
            {
                planActions.add(pa);
                planActionTotal += (pa.Total_Service_Order_Cost__c == null ? 0 : pa.Total_Service_Order_Cost__c);
            }
        }

        Plan_Approval__c currentState = new Plan_Approval__c();
        for(Plan_Approval__c pa : [SELECT ID,
                                            Service_Planner_Complete__c,
                                            Chief_Executive_Decision__c
                                    FROM    Plan_Approval__c
                                    WHERE   ID = :planApproval.ID])
        {
            currentState = pa;
        }

        planApproval.Amendment_Details__c = '';
        //Boolean hasConflict = false;
        for(Plan_Action__c act : planActions)
        {
            if(act.Amendment_Details__c != null)
            {
                planApproval.Amendment_Details__c += act.Amendment_Details__c + '\n\n';
            }

            if(planApproval.RecordTypeId == PlanApprovalRecordTypes.get(RTDEV_SERVICE_PLANNER_RECOMMENDATION).Id
                    && planApproval.Service_Planner_Complete__c
                    && currentState.Service_Planner_Complete__c)
            {
                if(act.Service_Planner_Conflict_of_Interest__c && !act.MSD_Conflict_Process_Complete__c)
                {
                    throw new A2HCException('The Plan cannot be recommended until all Potential Conflicts of Interest have been completed.');
                    //hasConflict = true;
                }
            }
        }

system.debug('WithinApprovalLevel:'+WithinApprovalLevel)   ;
        if(WithinApprovalLevel)
        {
            delegatedApproval(currentState);
        }
        else
        {
            undelegatedApproval(currentState);
        }
    }

    private void delegatedApproval(Plan_Approval__c currentState)
    {
        // service planner complete
        if(planApproval.RecordTypeId == null || planApproval.RecordTypeId == PlanApprovalRecordTypes.get(RTDEV_SERVICE_PLANNER_RECOMMENDATION).Id)
        {
            if(SelectedServicePlannerApprovalOption == APPROVED && CurrentUser.UserRole.Name == 'Service Planners')
            {
                setDelegatedApprovalFields('Service_Planner_Approval', currentState);
            }
            else
            {
                undelegatedApproval(currentState);
            }
        }
        else if(planApproval.RecordTypeId == PlanApprovalRecordTypes.get(RTDEV_LEAD_SERVICE_PLANNER_RECOMMENDATION).Id)
        {
            if(planApproval.Lead_Service_Planner_Complete__c == APPROVED && CurrentUser.UserRole.Name == 'Lead Service Planner')
            {
                setDelegatedApprovalFields('Lead_Service_Planner_Approval', currentState);
            }
            else
            {
                undelegatedApproval(currentState);
            }
        }
        else if(planApproval.RecordTypeId == PlanApprovalRecordTypes.get(RTDEV_MANAGER_ASD_RECOMMENDATION).Id)
        {
            if(planApproval.Manager_ASD_Complete__c == APPROVED && CurrentUser.UserRole.Name == 'Manager ASD')
            {
                setDelegatedApprovalFields('Manager_Service_Planning_Approval', currentState);
            }
            else
            {
                undelegatedApproval(currentState);
            }
        }
        else
        {
            undelegatedApproval(currentState);
        }
    }

    private void setDelegatedApprovalFields(String recordTypeDevName, Plan_Approval__c currentState)
    {
        String roleDisplay = CurrentUser.UserRole.Name;
        if(roleDisplay == 'Manager ASD')
        {
            roleDisplay = 'Senior Delegate';
        }
        else if(roleDisplay == 'Lead Service Planner')
        {
            roleDisplay = 'Delegate';
        }
        planApproval.RecordTypeId = PlanApprovalRecordTypes.get(recordTypeDevName).Id;
        planApproval.Plan_Approval_Date__c = Date.today();
        planApproval.Chief_Executive_Comments__c = 'N/A approved at the ' + roleDisplay + ' level';
        planApproval.Assigned_To__c = null;

        setPlanStatus(APPROVED);
    }

    private void undelegatedApproval(Plan_Approval__c currentState)
    {
        // service planner complete
        if(planApproval.RecordTypeId == null || planApproval.RecordTypeId == PlanApprovalRecordTypes.get(RTDEV_SERVICE_PLANNER_RECOMMENDATION).Id)
        {
            if(planApproval.Service_Planner_Complete__c)
            {
                planApproval.RecordTypeId = PlanApprovalRecordTypes.get(RTDEV_LEAD_SERVICE_PLANNER_RECOMMENDATION).Id;
                planApproval.Lead_Service_Planner_Complete__c = null;
            }
        }
        // lead service planner complete
        else if(planApproval.RecordTypeId == PlanApprovalRecordTypes.get(RTDEV_LEAD_SERVICE_PLANNER_RECOMMENDATION).Id)
        {
            if(planApproval.Lead_Service_Planner_Complete__c == REFERRED_BACK_TO_SERVICE_PLANNER)
            {
                planApproval.RecordTypeId = PlanApprovalRecordTypes.get(RTDEV_SERVICE_PLANNER_RECOMMENDATION).Id;
                planApproval.Service_Planner_Complete__c = false;
                planApproval.Assigned_To__c = null;
            }
            else if(planApproval.Lead_Service_Planner_Complete__c == RECOMMENDED_TO_MANAGER_ASD)
            {
                planApproval.RecordTypeId = PlanApprovalRecordTypes.get(RTDEV_MANAGER_ASD_RECOMMENDATION).Id;
                planApproval.Manager_ASD_Complete__c = null;
            }
        }
        // manager ASD complete
        else if(planApproval.RecordTypeId == PlanApprovalRecordTypes.get(RTDEV_MANAGER_ASD_RECOMMENDATION).Id)
        {
            if(planApproval.Manager_ASD_Complete__c == REFERRED_BACK_TO_SERVICE_PLANNER)
            {
                planApproval.RecordTypeId = PlanApprovalRecordTypes.get(RTDEV_SERVICE_PLANNER_RECOMMENDATION).Id;
                planApproval.Service_Planner_Complete__c = false;
            }
            else if(planApproval.Manager_ASD_Complete__c == REFERRED_BACK_TO_LEAD_SERVICE_PLANNER)
            {
                planApproval.RecordTypeId = PlanApprovalRecordTypes.get(RTDEV_LEAD_SERVICE_PLANNER_RECOMMENDATION).Id;
                planApproval.Lead_Service_Planner_Complete__c = null;
            }
            else if(planApproval.Manager_ASD_Complete__c == 'Recommended to Chief Executive')
            {
                planApproval.RecordTypeId = PlanApprovalRecordTypes.get(RTDEV_CHIEF_EXECUTIVE_APPROVAL).Id;
                planApproval.Chief_Executive_Decision__c = null;
            }
            else if(planApproval.Manager_ASD_Complete__c == APPROVED)
            {
                planApproval.addError('Estimated plan cost exceeds your delegation level');
            }
        }
        // chief executive decision
        else if(planApproval.RecordTypeId == PlanApprovalRecordTypes.get(RTDEV_CHIEF_EXECUTIVE_APPROVAL).Id)
        {
            String planStatus = null;
            if(planApproval.Chief_Executive_Decision__c == REFERRED_BACK_TO_SERVICE_PLANNER)
            {
                planApproval.RecordTypeId = PlanApprovalRecordTypes.get(RTDEV_SERVICE_PLANNER_RECOMMENDATION).Id;
                planApproval.Service_Planner_Complete__c = false;
            }
            else if(planApproval.Chief_Executive_Decision__c == 'Referred back to Lead Service Planner' ||
                    planApproval.Chief_Executive_Decision__c == REFERRED_BACK_TO_LEAD_SERVICE_PLANNER)
            {
                planApproval.RecordTypeId = PlanApprovalRecordTypes.get(RTDEV_LEAD_SERVICE_PLANNER_RECOMMENDATION).Id;
                planApproval.Lead_Service_Planner_Complete__c = null;
            }
            else if(planApproval.Chief_Executive_Decision__c == 'Referred back to Senior Delegate')
            {
                planApproval.RecordTypeId = PlanApprovalRecordTypes.get(RTDEV_MANAGER_ASD_RECOMMENDATION).Id;
                planApproval.Manager_ASD_Complete__c = null;
            }
            else if(planApproval.Chief_Executive_Decision__c == NOT_APPROVED)
            {
                planStatus = NOT_APPROVED;
            }
            else if(planApproval.Chief_Executive_Decision__c == APPROVED &&
                    planApproval.Chief_Executive_Decision__c != currentState.Chief_Executive_Decision__c)
            {
                planApproval.Plan_Approval_Date__c = Date.today();
                planStatus = APPROVED;
            }
            if(planStatus != null && approvalPlan.Plan_Status__c != planStatus)
            {
                setPlanStatus(planStatus);
            }
        }
    }

    private void setPlanStatus(String status)
    {
        approvalPlan.Plan_Status__c = status;
        update approvalPlan;

        if(status == APPROVED)
        {
            List<Plan_Action__c> planActions = new List<Plan_Action__c>();
            for(Plan_Action__c pa : getSelectedPlanActions())
            {
                if(pa.Ready_to_Approve__c)
                {
                    pa.Approved__c = true;
                    pa.Recently_Updated__c = true;
                    planActions.add(pa);
                }
            }
            update planActions;
        }
    }

    private List<Plan_Action__c> getSelectedPlanActions()
    {
        List<Plan_Action__c> planActions = new List<Plan_Action__c>();
        for(Plan_Action__c pa : ((planApproval.Id == null || planApproval.RecordType.DeveloperName == RTDEV_SERVICE_PLANNER_RECOMMENDATION) ? NewPlanActions : PlanActionsForApproval))
        {
            if(pa.Ready_to_Approve__c)
            {
                planActions.add(pa);
            }
        }
        return planActions;
    }
 }