public class InvoiceBatchCompletedEmailController 
{
	public InvoiceBatchCompletedEmailController()
	{
	}
	
	public Id BatchID
	{
		get;set;
	}
	
	public Id ScheduleID
	{
		get;
		set;
	}
	
	
	public Id UserID
	{
		get;
		set;
	}
	

	public string SalesforceURL
	{
		get
		{
			return URL.getSalesforceBaseUrl().toExternalForm();
		}
		private set;
	}
	
	
	public List<RCR_Invoice__c> AllInvoices
	{
		get
		{
			if(AllInvoices == null)
			{
				AllInvoices = new List<RCR_Invoice__c>();
			}
			return AllInvoices;
		}
		private set;
	}
	
	
	public List<RCR_Invoice__c> getInvoices()
	{
		return [SELECT	Id,
						Invoice_Number__c,
						Status__c
				FROM	RCR_Invoice__c
				WHERE	(Schedule__c = :ScheduleID OR Case_safe_schedule_ID__c = :ScheduleID)
				AND		Reconciliation_User__c = :UserID
				ORDER By Name];
	}
	
	
	static testMethod void test1()
	{
		InvoiceBatchCompletedEmailController emailCntr = new InvoiceBatchCompletedEmailController();
		
		// Need a valid ID so insert a Batch Log record to generate an Id. Can use if for all Id properties.
		Batch_Log__c log = new Batch_Log__c(Name = '11111', Record_Id__c = 'Test', Record_Name__c = 'Test', Message__c = 'Testing');
		insert log;
		
		RCR_Invoice__c i = new RCR_Invoice__c(Invoice_Number__c = '1', Schedule__c = log.Id, Reconciliation_User__c = UserInfo.getUserId());
		insert i;
		
		// Set class properties
		emailCntr.BatchID = log.Id;
		emailCntr.ScheduleID = log.Id;
		emailCntr.UserID = log.Id;
		emailCntr.SalesforceURL = 'www.google.com.au';
		
		system.debug(emailCntr.BatchID);
		system.debug(emailCntr.ScheduleID);
		system.debug(emailCntr.UserID);
		system.debug(emailCntr.SalesforceURL);
		system.debug(emailCntr.AllInvoices);
		system.debug(emailCntr.getInvoices());
	}
}