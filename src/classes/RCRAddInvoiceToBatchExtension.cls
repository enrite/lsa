public with sharing class RCRAddInvoiceToBatchExtension
    extends A2HCPageBase
{
    public RCRAddInvoiceToBatchExtension(ApexPages.StandardController controller)
    {
        invoice = [SELECT   ID,
                            Status__c,
                            Total_Reconciled__c,
                            RCTI_Agreement__c,
                            RCTI_Generated__c,
                            (SELECT Id,
                                    Activity_Date__c,
                                    Total_ex_GST__c,
                                    Total_GST__c,
                                    RCR_Service_Order__c
                             FROM   RCR_Invoice_Items__r)
                    FROM    RCR_Invoice__c
                    WHERE   ID = :(controller.getID())];

        Valid = invoice.Total_Reconciled__c != 0.0;
        if(!Valid)
        {
            A2HCException.formatException(ApexPages.Severity.WARNING, 'Activity statements with no reconciled items cannot be processed.');
        }
        else
        {
            Valid = invoice.Status__c == APS_PicklistValues.RCRInvoiceItem_Status_Approved;
            if(!Valid)
            {
                A2HCException.formatException(ApexPages.Severity.WARNING, 'Activity statement must be approved before it can be added to a batch.');
            }
            else
            {
                Valid = ((invoice.RCTI_Agreement__c == 'Yes' && invoice.RCTI_Generated__c != null) ||
                            invoice.RCTI_Agreement__c == 'No');
                if(!Valid)
                {
                    A2HCException.formatException(ApexPages.Severity.WARNING, 'Activity statement must have an RCTI before it can be added to a batch.');
                }
            }
        }
    }
    
    public RCRAddInvoiceToBatchExtension()
    {
        
    }

    private RCR_Invoice__c invoice;

    public Boolean Valid
    {
        get;
        private set;
    }

    public String Description
    {
        get;
        set;
    }

    public List<RCR_Invoice_Batch__c> OpenBatches
    {
        get
        {
            if(OpenBatches == null)
            {
                OpenBatches = [SELECT   ID,
                                        Name,
                                        Description__c,
                                        Approved_By__c,
                                        Approved_Date__c
                                FROM    RCR_Invoice_Batch__c
                                WHERE   Batch_Number__c = null AND
                                        MP_File_Name__c = null];
            }
            return OpenBatches;
        }
        private set;
    }

    public PageReference addToBatch()
    {
        try
        {
            String batchID = getParameter('batchID');
            
            RCR_Invoice_Batch__c batch = new RCR_Invoice_Batch__c(id = batchID);
            batch.Approved_By__c = null;
            batch.Approved_date__c = null;
            update batch;
            
            invoice.RCR_Invoice_Batch__c = batchID;
            update invoice;

            //ApexPages.Action act = new ApexPages.Action('{!list}');
            //return act.invoke();

            PageReference ref = new PageReference('/apex/RCRInvoiceBatch');
            ref.getParameters().put('id',batch.Id);
            return ref;
        }
        catch (Exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }

    public PageReference newBatch()
    {
        Savepoint sp = Database.setSavepoint();
        try
        {
            if(A2HCUtilities.isStringNullOrEmpty(Description))
            {
                return A2HCException.formatException('Description is required for a new batch.');
            }
            
            RCR_Invoice_Batch__c batch = new RCR_Invoice_Batch__c(Description__c = Description);
            insert batch;

            invoice.RCR_Invoice_Batch__c = batch.ID;
            update invoice;

            //ApexPages.Action act = new ApexPages.Action('{!list}');
            //return act.invoke();

            PageReference ref = new PageReference('/apex/RCRInvoiceBatch');
            ref.getParameters().put('id',batch.Id);
            return ref;
        }
        catch (Exception ex)
        {
            return A2HCEXception.formatExceptionAndRollback(sp, ex);
        }
    }

    @IsTest//(SeeAllData=true)
    static void test1()
    {
        TestLoadData.loadRCRData();

        Test.StartTest();

        RCR_Invoice__c invoice = [SELECT    ID,
                                            Status__c,
                                            Total_Reconciled__c,
                                            (SELECT Code__c,
                                                    Service_Order_Number__c,
                                                    Activity_Date__c,
                                                    GST__c,
                                                    Quantity__c,
                                                    Rate__c,
                                                    Reconciled_Service__c,
                                                    Total__c,
                                                    Status__c
                                             FROM   RCR_Invoice_Items__r)   
                                    FROM    RCR_Invoice__c
                                    //WHERE   Status__c = 'Reconciled - No Errors'
                                    LIMIT 1];
                                                         
        for(RCR_Invoice_Item__c item : invoice.RCR_Invoice_Items__r)
        {
            system.debug(item);
        }                                   
                                
        system.debug('Total Reconciled: ' + invoice.Total_Reconciled__c);                                   

        RCRAddInvoiceToBatchExtension ext = new RCRAddInvoiceToBatchExtension(new ApexPages.Standardcontroller(invoice));
        ext.addToBatch();

        system.debug(ext.Valid);

        system.debug(ext.OpenBatches);

        system.debug(ext.Description);

        ext.Description = null;
        ext.newBatch();

        ext.Description = 'blah';
        ext.newBatch();

        Test.StopTest();
    }
}