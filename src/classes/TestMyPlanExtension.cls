@isTest
public class TestMyPlanExtension
{                   
    static testMethod void myTestMethod()
    {   
        TestLoadFormData data = new TestLoadFormData();
                 
        Test.StartTest();
                                                           
        Plan__c p = new Plan__c();
        p.Client__c = data.Client.Id;
        p.RecordTypeId = data.RecordTypes.get('MyPlan').Id;
        p.Plan_Status__c = 'Draft';
        insert p;
        
        Plan_Action__c pa = new Plan_Action__c();
        pa.Participant_Plan__c = p.Id;
        pa.RecordTypeId = data.RecordTypes.get('Attendant_Care_and_Support').Id;
        insert pa;
        
        Plan_Goal__c g = new Plan_Goal__c();
        g.Participant_Plan__c = p.Id;
        insert g;               
                                     
        MyPlanExtension ext = new MyPlanExtension(new ApexPages.StandardController(p));
        ext.AddPlanGoal();
        ext.AddPlanGoal();
        
        ext.PlanGoalKey = ext.PlanGoalsKeys[0];
        ext.MoveDown();
        ext.MoveUp();
        ext.MoveBottom();
        ext.MoveTop();
        
        ext.SaveOverride();        
        ext.RemovePlanGoal();
        ext.SaveOverride(); 
                       
        Test.StopTest();        
    }
}