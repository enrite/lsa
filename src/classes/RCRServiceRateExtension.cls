public with sharing class RCRServiceRateExtension extends A2HCPageBase
{
    public RCRServiceRateExtension(ApexPages.Standardcontroller controller)
    {
        String rateId = getParameter('Id');
        isClone =  getParameter('clone') == '1';
        if(rateId != null)
        {
            RCRServiceRate = [SELECT r.Id,
                                    r.Name,
                                    r.RCR_Service__c,
                                    r.RCR_Service__r.Name,
                                    r.Start_Date__c,
                                    r.End_Date__c,
                                    r.Rate__c,
                                    r.Public_Holiday_Rate__c,
                                    r.Proposed_Rate_Standard__c,
                                    r.Proposed_Rate_Public_Holidays__c,
                                    r.Recalculate_Service_Order_Value__c
                          FROM      RCR_Service_Rate__c r
                          WHERE     r.Id = :rateId];
        }
        else
        {
            String retURL = getParameter('retURL');
            if(retURL != null)
            {
                RCRServiceRate = new RCR_Service_Rate__c(RCR_Service__c = retURL.replace('/', ''));
            }
        }
        originalRate = RCRServiceRate.Rate__c;
        originalPublicHolidayRate = RCRServiceRate.Public_Holiday_Rate__c;
    }
    
    private Boolean isClone = false;
    public RCR_Service_Rate__c RCRServiceRate
    {
        get
        {
            if(RCRServiceRate == null)
            {
                RCRServiceRate = new RCR_Service_Rate__c();
            }
            return RCRServiceRate;
        }
        set;
    }


    private decimal originalRate;
    private decimal originalPublicHolidayRate;


    public PageReference saveOverride()
    {
        try
        {
            if(isClone)
            {
                RCRServiceRate = RCRServiceRate.clone(false, true);
            }
                
            if(valid())
            {               
                upsert RCRServiceRate;
                if(RCRServiceRate.Recalculate_Service_Order_Value__c)
                {
                    updateScheduledServices(Batch_Size__c.getInstance('Rate_Recalculation').Records__c.IntValue(), false);
                }
                return getReturnPage(RCRServiceRate.ID, RCRServiceRate.RCR_Service__c).setRedirect(true);
            }
            else
            {
                return null;
            }
        }
        catch (Exception e)
        {
            RCRServiceRate.Recalculate_Service_Order_Value__c = false;
            RCRServiceRate.Rate__c = originalRate;
            RCRServiceRate.Public_Holiday_Rate__c = originalPublicHolidayRate;
            return A2HCException.formatException(e);
        }
    }
    
    
    public PageReference activateNegotiatedRates()
    {
        try
        {
            
            if(RCRServiceRate.Proposed_Rate_Standard__c == null || RCRServiceRate.Proposed_Rate_Public_Holidays__c == null)
            {
                return A2HCException.formatException('Both the \'Proposed Rate (Standard)\' and the \'Proposed Rate (Public Holidays)\' must be provided when Activate Proposed Rates is used.');
            }
            if(!RCRServiceRate.Recalculate_Service_Order_Value__c)
            {
                return A2HCException.formatException('You must set the \'Recalculate Scheduled Service Values\' checkbox when activating proposed rates.');
            }
            
            RCRServiceRate.Rate__c = RCRServiceRate.Proposed_Rate_Standard__c;
            RCRServiceRate.Public_Holiday_Rate__c = RCRServiceRate.Proposed_Rate_Public_Holidays__c;
            RCRServiceRate.Proposed_Rate_Standard__c = null;
            RCRServiceRate.Proposed_Rate_Public_Holidays__c = null;
            
            if(valid())
            {
                upsert RCRServiceRate;
                updateScheduledServices(Batch_Size__c.getInstance('Rate_Recalculation').Records__c.IntValue(), true);

                return getReturnPage(RCRServiceRate.ID, RCRServiceRate.RCR_Service__c).setRedirect(true);
            }
            else
            {
                return null;
            }
            
            return null;            
        }
        catch (Exception ex)
        {
            RCRServiceRate.Recalculate_Service_Order_Value__c = false;
            RCRServiceRate.Rate__c = originalRate;
            RCRServiceRate.Public_Holiday_Rate__c = originalPublicHolidayRate;
            return A2HCException.formatException(ex);
        }
    }
    

    public PageReference cancelOverride()
    {
        try
        {
            return getReturnPage(RCRServiceRate.ID, RCRServiceRate.RCR_Service__c).setRedirect(true);
        }
        catch (Exception e)
        {
            return A2HCException.formatException(e);
        }
    }

    private boolean valid()
    {
        boolean isValid = true;
        
        if(RCRServiceRate.End_Date__c <= RCRServiceRate.Start_Date__c)
        {
            A2HCException.formatException('End date must be later than start date.');
            isValid = false;
        }
        
        // Rate effective periods must not overlap each other.
        for(RCR_Service_Rate__c rate : [SELECT  r.Id,
                                                r.Name,
                                                r.Start_Date__c,
                                                r.End_Date__c
                                         FROM   RCR_Service_Rate__c r
                                         WHERE  r.Id != :RCRServiceRate.Id
                                         AND    r.RCR_Service__c = :RCRServiceRate.RCR_Service__c])
        {
            if(A2HCUtilities.dateRangesOverlap(rate.Start_Date__c, rate.End_Date__c, RCRServiceRate.Start_Date__c, RCRServiceRate.End_Date__c))
            {
                A2HCException.formatException('Date range overlaps an existing Service Rate for this Service.');
                isValid = false;
                break; 
            }
        }
        return isValid;
    }

    public void updateScheduledServices(integer batchSize, boolean proposedRateActivation)
    {
        if(batchSize == null)
        {
            batchSize = 20;
        }
        RCRBatchUpdateScheduledServiceTotalCost batch = new RCRBatchUpdateScheduledServiceTotalCost();
        batch.Rate = RCRServiceRate;
        batch.OriginalStandardRate = originalRate;
        batch.OriginalPublicHolidayRate = originalPublicHolidayRate;
        batch.runMode = RCRBatchUpdateScheduledServiceTotalCost.Mode.SingleRateUpdate;
        batch.ProposedRateActivation = proposedRateActivation;
        ID batchprocessid = Database.executeBatch(batch, batchSize);
    }
    
    @IsTest
    private static void test1()
    {system.debug('debug1:' + [SELECT Id, Name, Minister_for_Disabilities__c FROM RCR_Client_Service_Agreement__c]);
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        RCR_Service__c service = [SELECT Id,
                                         (SELECT    Id, 
                                                    RCR_Service__c,
                                                    Start_Date__c,
                                                    End_Date__c
                                           FROM     RCR_Service_Rates__r
                                           WHERE    Proposed_Rate_Standard__c != null)
                                  FROM   RCR_Service__c
                                  /*WHERE  Account__r.Name = 'TestAccount'
                                  AND    Name = 'TESTCODE3'*/
                                  LIMIT 1];
        
        List<RCR_Service_Rate__c> existingRates = new List<RCR_Service_Rate__c>();
        existingRates.addAll(service.RCR_Service_Rates__r);
        
        Date maxEndDate = Date.newInstance(1900, 1, 1);
                
        for(RCR_Service_Rate__c r : existingRates)
        {       
            if(r.End_Date__c > maxEndDate)
            {           
                maxEndDate = r.End_Date__c;
            }
        }
        
        RCR_Service_Rate__c newRate = new RCR_Service_Rate__c();
        newRate.RCR_Service__c = service.Id;
        newRate.Start_Date__c = maxEndDate.addDays(1);
        newRate.End_Date__c = maxEndDate.addYears(1);
        newRate.Rate__c = 0.2;
        newRate.Public_Holiday_Rate__c = 0.4;
        newRate.Proposed_Rate_Standard__c = 0.25;
        newRate.Proposed_Rate_Public_Holidays__c = 0.45;
        newRate.Recalculate_Service_Order_Value__c = true;
        insert newRate;

        Test.setCurrentPage(Page.RCRInvoiceUpload);
        ApexPages.currentPage().getParameters().put('retURL', newRate.RCR_Service__c);
            
        RCRServiceRateExtension ext = new RCRServiceRateExtension(new ApexPages.Standardcontroller(newRate));
        ApexPages.currentPage().getParameters().put('id', newRate.Id);
        ext = new RCRServiceRateExtension(new ApexPages.Standardcontroller(newRate));
        
        ext.activateNegotiatedRates();

        ext.RCRServiceRate = null;
        System.debug(ext.RCRServiceRate);
        ext.cancelOverride();
        ext.saveOverride();

        ext.RCRServiceRate.Start_Date__c = date.today();
        ext.RCRServiceRate.End_Date__c = date.today();

        ext.saveOverride();
        ext.RCRServiceRate.End_Date__c = date.today().addDays(1);
        ext.RCRServiceRate.RCR_Service__c = newRate.RCR_Service__c;
        ext.RCRServiceRate.Rate__c = 1.0;

        ext.saveOverride();
        ext.RCRServiceRate.Start_Date__c = date.today().addYears(5);
        ext.RCRServiceRate.End_Date__c = ext.RCRServiceRate.Start_Date__c.addDays(10);
        ext.saveOverride();

        ext.updateScheduledServices(20, true);
        
        Test.StopTest();
    }
}