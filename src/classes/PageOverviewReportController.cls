public with sharing class PageOverviewReportController {
	Milestone1_Project__c record;

    public List<Milestone1_Milestone__c> UpcomingTaskCompletion
    {
    	get{
    		if(UpcomingTaskCompletion==null)
    		{
    			UpcomingTaskCompletion = [SELECT Kickoff__c, Deadline__c, Percentage_Complete__c, Name, OwnerId 
    			FROM Milestone1_Milestone__c where Project__c =: record.Id and Include_on_Gantt_Chart__c = true
                order by Kickoff__c, Deadline__c, Name];
    		}
    		return UpcomingTaskCompletion;
    	}set;
    }

    public List<string> Teams {
        get{
            List<string> options = new List<string>(); 
            Schema.sObjectType sobject_type = Milestone1_Milestone__c.getSObjectType(); 
            Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); 
            Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); 
            List<Schema.PicklistEntry> pick_list_values = field_map.get('Team__c').getDescribe().getPickListValues(); 
            for (Schema.PicklistEntry a : pick_list_values) { 
                options.add(a.getLabel());  
            }
            return options; 
        }set;
    }

    public Map<String,Decimal> TasksComplete
    {
        get{
            if(TasksComplete==null)
            {
                TasksComplete = new Map<String, Decimal>();
                for(string s : Teams)
                {
                    TasksComplete.put(s,0.00);
                }

                List<AggregateResult> results = [SELECT AVG(Percentage_Complete__c) percentage, Team__c 
                from Milestone1_Milestone__c where Project__c =: record.Id
                group by Team__c];
                for(AggregateResult result : results)
                {
                    TasksComplete.put((String)result.get('Team__c') , ((Decimal)result.get('percentage')).setScale(2));
                }
            }
            return TasksComplete;
        }set;
    }

    public string TimelineData
    {
        get{
            if(TimelineData == null)
            {
                List<Milestone1_Milestone__c> milestones = [SELECT Name, Owner.Name, Kickoff__c, Deadline__c from Milestone1_Milestone__c where Project__c =: record.Id and Highlight_to_Board__c = true];
                List<object[]> data = new List<object[]>();
                for(Milestone1_Milestone__c m : milestones){
                    data.add(new object[]{m.Owner.Name, m.Name, m.Kickoff__c, m.Deadline__c});
                }
                TimelineData = JSON.serialize(data);
            }return TimelineData;
        }set;
    }

    public PageOverviewReportController(ApexPages.StandardController con) {
        record = (Milestone1_Project__c)con.getRecord();
    }
}