public class SignatureComponentController 
{
    @RemoteAction
    public static String saveSignature(String signatureBody, String parentId) 
    {
        try
        {            
            Attachment a = new Attachment();
            a.ParentId = parentId;
            a.Body = EncodingUtil.base64Decode(signatureBody);
            a.ContentType = 'image/png';
            a.Name = 'Signature Capture.png';
            insert a;
            
            return null;            
        }
        catch(Exception e)
        {
            return JSON.serialize(e);
        }        
    }    
}