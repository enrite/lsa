public with sharing class PlanActionViewRedirectExtension {
    Plan_Action__c record;

    public PlanActionViewRedirectExtension(ApexPages.StandardController con) {
        record = (Plan_Action__c)con.getRecord();
    }

    public PageReference Redir()
    {
        try
        {
            PageReference pg;
            String referer = ApexPages.currentPage().getHeaders().get('referer');
            if (referer != null && referer.toLowerCase().contains('participantportal'))
            {
                pg = Page.ParticipantPortalPlanAction;
            }
            else
            {
                pg = new PageReference('/' + record.Id);

                pg.getParameters().put('nooverride', '1');
            }

            // add the parameters for this page to the new page reference
            for (String k : ApexPages.CurrentPage().getParameters().keySet())
            {
                if (k != 'save_new' && k != '_CONFIRMATIONTOKEN')
                {
                    pg.getParameters().put(k, ApexPages.CurrentPage().getParameters().get(k));
                }
            }

            pg.setRedirect(true);
            return pg;
        }
        catch(Exception e)
        {
            return A2HCException.formatException(e);
        }
    }
}