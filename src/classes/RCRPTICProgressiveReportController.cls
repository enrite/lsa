public with sharing class RCRPTICProgressiveReportController extends A2HCPageBase 
{
//    public RCRPTICProgressiveReportController()
//    {
//        initialise();
//        getSimpleCounts();
//    }
//
//    public date StartDate
//    {
//        get;
//        private set;
//    }
//
//    public date EndDate
//    {
//        get;
//        private set;
//    }
//
//    public List<CountWrapper> ClientAndGroupCount
//    {
//        get
//        {
//            if(ClientAndGroupCount == null)
//            {
//                ClientAndGroupCount = new List<CountWrapper>();
//
//                AggregateResult ar;
//
//                ar = [SELECT    COUNT_DISTINCT(Group_Program__c) cnt
//                       FROM     RCR_Contract__c
//                       WHERE    (RecordType.Name = :APSRecordTypes.RCRServiceOrder_NewRequest OR
//                                 RecordType.Name = :APSRecordTypes.RCRServiceOrder_Amendment)
//                       AND      Funding_Source__c = :APS_Picklistvalues.RCRContract_FundingSource_PTIC
//                       AND      Group_Program__c != null
//                       AND      Group_Program__c != ''
//                       AND      Allocation_Approved_Date__c >= :StartDate
//                       AND      Allocation_Approved_Date__c <= :EndDate];
//
//                ClientAndGroupCount.add(new CountWrapper('Groups (unique count of groups)', (integer)ar.get('cnt')));
//
//                ar = [SELECT    COUNT_DISTINCT(Client__c) cnt
//                       FROM     RCR_Contract__c
//                       WHERE    (RecordType.Name = :APSRecordTypes.RCRServiceOrder_NewRequest OR
//                                 RecordType.Name = :APSRecordTypes.RCRServiceOrder_Amendment)
//                       AND      Funding_Source__c = :APS_Picklistvalues.RCRContract_FundingSource_PTIC
//                       AND      Client__c != null
//                       AND      Allocation_Approved_Date__c >= :StartDate
//                       AND      Allocation_Approved_Date__c <= :EndDate];
//
//                ClientAndGroupCount.add(new CountWrapper('Clients (unique count of clients)', (integer)ar.get('cnt')));
//            }
//            return ClientAndGroupCount;
//        }
//        private set;
//    }
//
//
//    public List<CountWrapper> ClientAgeGroups
//    {
//        get
//        {
//            if(ClientAgeGroups == null)
//            {
//                integer zeroToFiveCount = 0;
//                integer sixToFourteenCount = 0;
//                integer fifteenToTwentyFiveCount = 0;
//                integer twentySixToFourtyFiveCount = 0;
//                integer fourtySixToSixtyFourCount = 0;
//                integer sixtyFivePlusCount = 0;
//                integer unknownAgeCount = 0;
//
//                Set<Id> clientIDs = new Set<Id>();
//
//                for(RCR_Contract__c c : [SELECT Id,
//                                                Name,
//                                                Client__c,
//                                                Client_Age_at_Start_Date__c
//                                         FROM   RCR_Contract__c
//                                         WHERE  (RecordType.Name = :APSRecordTypes.RCRServiceOrder_NewRequest OR
//                                                 RecordType.Name = :APSRecordTypes.RCRServiceOrder_Amendment)
//                                         AND    Funding_Source__c = :APS_Picklistvalues.RCRContract_FundingSource_PTIC
//                                         And    Client__c != null
//                                         AND    Allocation_Approved_Date__c >= :StartDate
//                                         AND    Allocation_Approved_Date__c <= :EndDate])
//                {
//                    if(!clientIDs.contains(c.Client__c))
//                    {
//                        if(c.Client_Age_at_Start_Date__c == null)
//                        {
//                            unknownAgeCount += 1;
//                        }
//                        else if(c.Client_Age_at_Start_Date__c <= 5)
//                        {
//                            zeroToFiveCount += 1;
//                        }
//                        else if(c.Client_Age_at_Start_Date__c >= 6 && c.Client_Age_at_Start_Date__c <= 14)
//                        {
//                            sixToFourteenCount += 1;
//                        }
//                        else if(c.Client_Age_at_Start_Date__c >= 15 && c.Client_Age_at_Start_Date__c <= 25)
//                        {
//                            fifteenToTwentyFiveCount += 1;
//                        }
//                        else if(c.Client_Age_at_Start_Date__c >= 26 && c.Client_Age_at_Start_Date__c <= 45)
//                        {
//                            twentySixToFourtyFiveCount += 1;
//                        }
//                        else if(c.Client_Age_at_Start_Date__c >= 46 && c.Client_Age_at_Start_Date__c <= 64)
//                        {
//                            fourtySixToSixtyFourCount += 1;
//                        }
//                        else if(c.Client_Age_at_Start_Date__c >= 65)
//                        {
//                            sixtyFivePlusCount += 1;
//                        }
//                        clientIDs.add(c.Client__c);
//                    }
//                }
//
//                ClientAgeGroups = new List<CountWrapper>();
//                ClientAgeGroups.add(new CountWrapper('0 - 5 years', zeroToFiveCount));
//                ClientAgeGroups.add(new CountWrapper('6 - 14 years', sixToFourteenCount));
//                ClientAgeGroups.add(new CountWrapper('15 - 25 years', fifteenToTwentyFiveCount));
//                ClientAgeGroups.add(new CountWrapper('26 - 45 years', twentySixToFourtyFiveCount));
//                ClientAgeGroups.add(new CountWrapper('46 - 64 years', fourtySixToSixtyFourCount));
//                ClientAgeGroups.add(new CountWrapper('65 years & over', sixtyFivePlusCount));
//                ClientAgeGroups.add(new CountWrapper('Unknown age', unknownAgeCount));
//            }
//            return ClientAgeGroups;
//        }
//        private set;
//    }
//
//    public List<CountWrapper> ClientDisabilities
//    {
//        get
//        {
//            if(ClientDisabilities == null)
//            {
//                AggregateResult[] ar = [SELECT  Count(Id) cnt,
//                                                Primary_Disability__c
//                                        FROM    Referred_Client__c
//                                        WHERE   Id IN ( SELECT  Client__c
//                                                        FROM    RCR_Contract__c
//                                                        WHERE   (RecordType.Name = :APSRecordTypes.RCRServiceOrder_NewRequest OR
//                                                                 RecordType.Name = :APSRecordTypes.RCRServiceOrder_Amendment)
//                                                        AND     Funding_Source__c = :APS_Picklistvalues.RCRContract_FundingSource_PTIC
//                                                        AND     Allocation_Approved_Date__c >= :StartDate
//                                                        AND     Allocation_Approved_Date__c <= :EndDate)
//                                        GROUP BY Primary_Disability__c
//                                        ORDER BY Primary_Disability__c desc];
//
//                ClientDisabilities = new List<CountWrapper>();
//
//                for(AggregateResult result : ar)
//                {
//                    string disability = (string)result.get('Primary_Disability__c');
//
//                    if(disability == '' || disability == null)
//                    {
//                        disability = 'Unknown';
//                    }
//
//                    ClientDisabilities.add(new CountWrapper(disability, (integer)result.get('cnt')));
//                }
//            }
//            return ClientDisabilities;
//        }
//        private set;
//    }
//
//    public List<CountWrapper> RequestTypesCount
//    {
//        get
//        {
//            if(RequestTypesCount == null)
//            {
//                RequestTypesCount = new List<CountWrapper>();
//
//                for(AggregateResult ar : [SELECT    COUNT(Id) cnt,
//                                                    New_Request_Type__c
//                                           FROM     RCR_Contract__c
//                                           WHERE    (RecordType.Name = :APSRecordTypes.RCRServiceOrder_NewRequest OR
//                                                     RecordType.Name = :APSRecordTypes.RCRServiceOrder_Amendment)
//                                           AND      Funding_Source__c = :APS_Picklistvalues.RCRContract_FundingSource_PTIC
//                                           AND      Allocation_Approved_Date__c >= :StartDate
//                                           AND      Allocation_Approved_Date__c <= :EndDate
//                                           GROUP BY New_Request_Type__c
//                                           ORDER BY New_Request_Type__c])
//                {
//                    string requestType = (string)ar.get('New_Request_Type__c');
//                    if(string.isBlank(requestType))
//                    {
//                        requestType = 'Undefined';
//                    }
//                    RequestTypesCount.add(new CountWrapper(requestType, (integer)ar.get('cnt')));
//                }
//            }
//            return RequestTypesCount;
//        }
//        private set;
//    }
//
//    public List<CountWrapper> FundingCriteria
//    {
//        get
//        {
//            if(FundingCriteria == null)
//            {
//                FundingCriteria = new List<CountWrapper>();
//            }
//            return FundingCriteria;
//        }
//        private set;
//    }
//
//    public List<CountWrapper> Background
//    {
//        get
//        {
//            if(Background == null)
//            {
//                Background = new List<CountWrapper>();
//            }
//            return Background;
//        }
//        private set;
//    }
//
//
////
//
//
//    public Map<string, decimal> Totals
//    {
//        get
//        {
//            if(Totals == null)
//            {
//                // Initialise Totals to zero.
//                Totals = new Map<string, decimal>();
//                Totals.put('OnceOffCount', 0.0);
//                Totals.put('RecurrentCount', 0.0);
//                Totals.put('OnceOffCost', 0.0);
//                Totals.put('RecurrentCost', 0.0);
//                Totals.put('ServicesCost', 0.0);
//                Totals.put('OnceOffAlloc', 0.0);
//                Totals.put('OnceOffFYEAlloc', 0.0);
//                Totals.put('RecurrentAlloc', 0.0);
//                Totals.put('RecurrentFYEAlloc', 0.0);
//                Totals.put('TotalAlloc', 0.0);
//                Totals.put('TotalFYEAlloc', 0.0);
//            }
//            return Totals;
//        }
//        private set;
//    }
//
//    private void initialise()
//    {
//        try
//        {
//            StartDate = Date.parse(getParameter('startDate'));
//            EndDate = Date.parse(getParameter('endDate'));
//        }
//        catch(Exception ex)
//        {
//            A2HCException.formatException(ex);
//        }
//    }
//
//    @TestVisible
//    private void getSimpleCounts()
//    {
//        integer homelessCount = 0;
//        integer riskOfHomelessCount = 0;
//        integer familiesSATransitionCount = 0;
//        integer novitaCount = 0;
//        integer transferCount = 0;
//        integer noCriteriaEnteredCount = 0;
//        integer behaviouralIssuesCount = 0;
//        integer deteriorationConditionCount = 0;
//        integer carerIssuesCount = 0;
//        integer hospitalDischargeCount = 0;
//        integer ministerialImperativeCount = 0;
//        integer ngoSectorRequestCount = 0;
//        integer accommServicesRequestCount = 0;
//        integer noBackgroundEnteredCount = 0;
//
//        AggregateResult[] ar = [SELECT  Count(Id) cnt,
//                                        Crit_Homeless__c,
//                                        Crit_Risk_Homeless__c,
//                                        Crit_FamSA__c,
//                                        Crit_Novita__c,
//                                        Crit_Transfer__c,
//                                        Background_Behavioural_Issues__c,
//                                        Background_Deteriorating_Condition__c,
//                                        Background_Carer_Issues__c,
//                                        Background_Hospital_Discharge__c,
//                                        Background_Ministerial_Imperative__c,
//                                        Background_NGO_Sector_Request__c,
//                                        Background_Accomm_Services_Request__c
//                                FROM    RCR_Contract__c
//                                WHERE   (RecordType.Name = :APSRecordTypes.RCRServiceOrder_NewRequest OR
//                                         RecordType.Name = :APSRecordTypes.RCRServiceOrder_Amendment)
//                                AND     Funding_Source__c = :APS_Picklistvalues.RCRContract_FundingSource_PTIC
//                                AND     Allocation_Approved_Date__c >= :StartDate
//                                AND     Allocation_Approved_Date__c <= :EndDate
//                                GROUP BY Crit_Homeless__c,
//                                        Crit_Risk_Homeless__c,
//                                        Crit_FamSA__c,
//                                        Crit_Novita__c,
//                                        Crit_Transfer__c,
//                                        Background_Behavioural_Issues__c,
//                                        Background_Deteriorating_Condition__c,
//                                        Background_Carer_Issues__c,
//                                        Background_Hospital_Discharge__c,
//                                        Background_Ministerial_Imperative__c,
//                                        Background_NGO_Sector_Request__c,
//                                        Background_Accomm_Services_Request__c];
//
//        for(AggregateResult result : ar)
//        {
//            if((boolean)result.get('Crit_Homeless__c'))
//            {
//                homelessCount += (integer)result.get('cnt');
//            }
//            if((boolean)result.get('Crit_Risk_Homeless__c'))
//            {
//                riskOfHomelessCount += (integer)result.get('cnt');
//            }
//            if((boolean)result.get('Crit_FamSA__c'))
//            {
//                familiesSATransitionCount += (integer)result.get('cnt');
//            }
//            if((boolean)result.get('Crit_Novita__c'))
//            {
//                novitaCount += (integer)result.get('cnt');
//            }
//            if((boolean)result.get('Crit_Transfer__c'))
//            {
//                transferCount += (integer)result.get('cnt');
//            }
//            if((boolean)result.get('Background_Behavioural_Issues__c'))
//            {
//                behaviouralIssuesCount += (integer)result.get('cnt');
//            }
//            if((boolean)result.get('Background_Deteriorating_Condition__c'))
//            {
//                deteriorationConditionCount += (integer)result.get('cnt');
//            }
//            if((boolean)result.get('Background_Carer_Issues__c'))
//            {
//                carerIssuesCount += (integer)result.get('cnt');
//            }
//            if((boolean)result.get('Background_Hospital_Discharge__c'))
//            {
//                hospitalDischargeCount += (integer)result.get('cnt');
//            }
//            if((boolean)result.get('Background_Ministerial_Imperative__c'))
//            {
//                ministerialImperativeCount += (integer)result.get('cnt');
//            }
//            if((boolean)result.get('Background_NGO_Sector_Request__c'))
//            {
//                ngoSectorRequestCount += (integer)result.get('cnt');
//            }
//            if((boolean)result.get('Background_Accomm_Services_Request__c'))
//            {
//                accommServicesRequestCount += (integer)result.get('cnt');
//            }
//            if(!(boolean)result.get('Crit_Homeless__c') &&
//                !(boolean)result.get('Crit_Risk_Homeless__c') &&
//                !(boolean)result.get('Crit_FamSA__c') &&
//                !(boolean)result.get('Crit_Novita__c') &&
//                !(boolean)result.get('Crit_Transfer__c'))
//            {
//                noCriteriaEnteredCount += (integer)result.get('cnt');
//            }
//            if(!(boolean)result.get('Background_Behavioural_Issues__c') &&
//                !(boolean)result.get('Background_Deteriorating_Condition__c') &&
//                !(boolean)result.get('Background_Carer_Issues__c') &&
//                !(boolean)result.get('Background_Hospital_Discharge__c') &&
//                !(boolean)result.get('Background_Ministerial_Imperative__c') &&
//                !(boolean)result.get('Background_NGO_Sector_Request__c') &&
//                !(boolean)result.get('Background_Accomm_Services_Request__c'))
//            {
//                noBackgroundEnteredCount += (integer)result.get('cnt');
//            }
//        }
//
//        FundingCriteria.add(new CountWrapper('Homeless', homelessCount));
//        FundingCriteria.add(new CountWrapper('Risk of Homelessness', riskOfHomelessCount));
//        FundingCriteria.add(new CountWrapper('Families SA Transition', familiesSATransitionCount));
//        FundingCriteria.add(new CountWrapper('Novita Transition', novitaCount));
//        FundingCriteria.add(new CountWrapper('Interstate Transfer', transferCount));
//        FundingCriteria.add(new CountWrapper('No funding criteria recorded', noCriteriaEnteredCount));
//
//        Background.add(new CountWrapper('Behavioural Issues', behaviouralIssuesCount));
//        Background.add(new CountWrapper('Deteriorating Condition', deteriorationConditionCount));
//        Background.add(new CountWrapper('Carer Issues', carerIssuesCount));
//        Background.add(new CountWrapper('Hospital Discharge', hospitalDischargeCount));
//        Background.add(new CountWrapper('Ministerial Imperative', ministerialImperativeCount));
//        Background.add(new CountWrapper('NGO Sector Request', ngoSectorRequestCount));
//        Background.add(new CountWrapper('Accommodation Services Request', accommServicesRequestCount));
//        Background.add(new CountWrapper('No background recorded', noBackgroundEnteredCount));
//    }
//
//
//    public class CountWrapper
//    {
//        public CountWrapper(string pValueCounted, integer pCount)
//        {
//            ValueCounted = pValueCounted;
//            Count = pCount;
//        }
//
//        public string ValueCounted
//        {
//            get;
//            private set;
//        }
//
//        public integer Count
//        {
//            get;
//            private set;
//        }
//    }
//
//
//    public class ServicesWrapper
//    {
//        public ServicesWrapper(string pServiceType)
//        {
//            ServiceType = pServiceType;
//            OnceOffCount = 0;
//            RecurrentCount = 0;
//            OnceOffCost = 0.0;
//            OnceOffFYECost = 0.0;
//            RecurrentCost = 0.0;
//            RecurrentFYECost = 0.0;
//        }
//
//        public string ServiceType
//        {
//            get;
//            private set;
//        }
//
//        public integer OnceOffCount
//        {
//            get;
//            set;
//        }
//
//        public integer RecurrentCount
//        {
//            get;
//            private set;
//        }
//
//        public integer ServiceCount
//        {
//            get
//            {
//                return OnceOffCount + RecurrentCount;
//            }
//        }
//
//        public decimal OnceOffCost
//        {
//            get;
//            private set;
//        }
//
//        public decimal OnceOffFYECost
//        {
//            get;
//            private set;
//        }
//
//        public decimal RecurrentCost
//        {
//            get;
//            private set;
//        }
//
//        public decimal RecurrentFYECost
//        {
//            get;
//            private set;
//        }
//
//        public decimal TotalServiceCost
//        {
//            get
//            {
//                return RecurrentCost + OnceOffCost;
//            }
//        }
//
//        public decimal TotalServiceFYECost
//        {
//            get
//            {
//                return OnceOffFYECost + RecurrentFYECost;
//            }
//        }
//    }
//

}