public class LPInvoiceRequestJSON
{
    public String id;	//fd3b8c72-b0b6-4c5c-bd69-10ee3ca03f2f
    public Integer created;	//1518405650
    public cls_data data;
    public String type;	//claiming.invoice.processed
    public class cls_data
    {
        public String memberNumber;	//qwerty
        public String membershipNumber;	//qwerty
        public String programCode;	//ndis
        public String invoiceUri;	//https://api.sandbox.lanternpay.com/invoices/3eeadfca-3cc8-4f01-90f0-0f81bd4328b5
    }
    public static LPInvoiceRequestJSON parse(String json){
        return (LPInvoiceRequestJSON) System.JSON.deserialize(json, LPInvoiceRequestJSON.class);
    }

//    static testMethod void testParse() {
//        String json=		'{'+
//                '    "id": "fd3b8c72-b0b6-4c5c-bd69-10ee3ca03f2f",'+
//                '    "created": 1518405650,'+
//                '    "data": {'+
//                '      "memberNumber": "qwerty",'+
//                '      "programCode": "ndis",'+
//                '      "invoiceUri": "https://api.sandbox.lanternpay.com/invoices/3eeadfca-3cc8-4f01-90f0-0f81bd4328b5"'+
//                '    },'+
//                '    "type": "claiming.invoice.processed"'+
//                '}';
//        fromJSON obj = parse(json);
//        System.assert(obj != null);
//    }
}