@isTest
public class TestDeliveryPlanInformationExtension
{                   
    static testMethod void myTestMethod()
    {
        TestLoadFormData data = new TestLoadFormData();
        
        Plan__c p = new Plan__c();
        p.Client__c = data.Client.Id;
        p.RecordTypeId = data.RecordTypes.get('MyPlan').Id;
        p.Plan_Status__c = 'Draft';
        insert p;
        
        Plan_Action__c pa = new Plan_Action__c();
        pa.Participant_Plan__c = p.Id;
        pa.RecordTypeId = data.RecordTypes.get('Attendant_Care_and_Support').Id;
        insert pa;
         
        Test.StartTest();         
        
        DeliveryPlanInformationExtension ext = new DeliveryPlanInformationExtension(new ApexPages.StandardController(p));
        system.debug(ext.PlanAction);
        
        Test.StopTest();        
    }
}