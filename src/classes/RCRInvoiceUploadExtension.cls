public with sharing class RCRInvoiceUploadExtension extends A2HCPageBase
// note there is no sharing as invoices uploaded by other users
// from the same account may delete and upload the same invoice file
{
    public RCRInvoiceUploadExtension()
    {
        String invoiceID = getParameter('id');
        if(invoiceID != null)
        {
            InvoiceUpload = [SELECT     r.Name,
                                    r.Id,
                                    r.File_Name__c,
                                    r.Date__c,
                                    r.Description__c,
                                    (SELECT i.ID,
                                            i.Name,
                                            i.Date__c,
                                            i.Invoice_Number__c
                                    FROM    RCR_Invoices__r i)
                            FROM    RCR_Invoice_Upload__c   r
                            WHERE   ID = :invoiceID];
        }
    }

    private integer exceptionCount = 0;
    private integer queryLimit = 0;
    private integer heapLimit = 0;
   // private integer scriptLimit = 0;
    private integer invoiceCount = 0;

    private transient List<RCR_Invoice__c> tPreviousInvoiceUploads;

    private final String UPLOADED_INVOICE = 'Uploaded Invoice';
    private final String DEFAULT_LIMIT_MSG = 'Too many activity items in this file. Reducing the number of line items below %0 may be successful.';

    private Map<String, String> errorMsgs
    {
        get
        {
            if(errorMsgs == null)
            {
                errorMsgs = new Map<String, String>();
                for(RCR_Error_Message__c msg : [SELECT  Name,
                                                Message__c
                                        FROM    RCR_Error_Message__c])
                {
                    errorMsgs.put(msg.Name, msg.Message__c);
                }
            }
            return errorMsgs;
        }
        private set;
    }

    public Attachment file
    {
        get
        {
            if(file == null)
            {
                file = new Attachment();
            }
            return file;
        }
        set;
    }

    public List<RCR_Invoice__c> PreviousInvoiceUploads
    {
        get
        {
            if(tPreviousInvoiceUploads == null)
            {
                tPreviousInvoiceUploads = [SELECT   ID,
                                                    Name,
                                                    Invoice_Number__c,
                                                    CreatedDate,
                                                    Date__c,
                                                    Status__c,
                                                    OwnerID,
                                                    Total_Ex_GST__c,
                                                    (SELECT ID 
                                                    FROM Attachments
                                                    WHERE Description = :UPLOADED_INVOICE)
                                            FROM    RCR_Invoice__c
                                            ORDER BY CreatedDate DESC];
            }
            return tPreviousInvoiceUploads;
        }
    }

    public Account SPAccount
    {
        get
        {
            if(SPAccount == null && SelectedAccount != null)
            {
                SPAccount = [SELECT ID,
                                    Allow_CSV_Statements__c,
                                    Allow_Manual_Statements__c,
                                    Allow_PDF_Statements__c
                            FROM    Account
                            WHERE   ID = :SelectedAccount];
            }
            return SPAccount;
        }
        private set;
    }

    public RCR_Invoice__c PDFInvoiceUpload
    {
        get
        {
            if(PDFInvoiceUpload == null)
            {
                PDFInvoiceUpload = new RCR_Invoice__c();
            }
            return PDFInvoiceUpload;
        }
        set;
    }

    public RCR_Invoice_Upload__c InvoiceUpload
    {
        get
        {
            if(InvoiceUpload == null)
            {
                InvoiceUpload = new RCR_Invoice_Upload__c();
            }
            return InvoiceUpload;
        }
        set;
    }

    public List<SelectOption> RCRProviderList
    {
        get
        {
            if(RCRProviderList == null)
            {
                if(InPortal)
                {
                    RCRProviderList = new List<SelectOption>();
                    RCRProviderList.add(new SelectOption(CurrentUser.Account_ID__c, CurrentUser.Account_Name__c));
                }
                else
                {
                    ServiceProviderExtension se = new ServiceProviderExtension();
                    RCRProviderList = se.RCRProviderList;
                }
            }
            return RCRProviderList;
        }
        private set;
    }

    public String SelectedAccount
    {
        get
        {
            if(InPortal)
            {
                SelectedAccount = CurrentUser.Account_ID__c;
            }
            return SelectedAccount;
        }
        set;
    }

    public Pagereference cancel()
    {
        try
        {
            return getReturnPageFromURL();
        }
        catch(Exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }

    public PageReference confirmSave()
    {
        PageReference pg = null;
        Savepoint sp = Database.setSavepoint();
        try
        {
            file = [SELECT  ID,
                            Body
                    FROM    Attachment
                    WHERE   ParentID = :InvoiceUpload.ID];

            if(!parseCSV(true))
            {
                Database.rollback(sp);
                InvoiceUpload = InvoiceUpload.clone(false);
                file = file.clone(false, false);
            }
            else
            {
                pg = Page.RCRUploadSummary;
                pg.getParameters().put('id', InvoiceUpload.ID);
                pg.setredirect(true);
            }

            return pg;
        }
        catch(Exception ex)
        {
            Database.rollback(sp);
            InvoiceUpload = InvoiceUpload.clone(false);
            file = file.clone(false, false);
            if(ex instanceof System.ListException)
            {
                return A2HCException.formatException(ErrorMsgs.get('Upload_Index_Out_Of_Bounds'));
            }
            return A2HCException.formatException(ex);
        }
    }
    public PageReference uploadPDF()
    {
        Savepoint sp = Database.setSavepoint();
        try
        {
            Boolean valid = true;
            if(SelectedAccount == null)
            {
                A2HCException.formatException('Account is required.');
                file.Name = null;
                valid = false;
            }
            if(file.Name == null)
            {
                A2HCException.formatException('File is required.');
                file.Name = null;
                valid = false;
            }
            if(PDFInvoiceUpload.Invoice_Number__c == null)
            {
                A2HCException.formatException('Statement number is required.');
                file.Name = null;
                valid = false;
            }
            if(PDFInvoiceUpload.Date__c == null)
            {
                A2HCException.formatException('Date is required.');
                file.Name = null;
                valid = false;
            }
            if(!valid)
            {
                return null;
            }

            if(PDFInvoiceUpload.Account__c == null)
            {
                PDFInvoiceUpload.Account__c = SelectedAccount;
            }
            if(PDFInvoiceUpload.ID != null)
            {
                delete PDFInvoiceUpload.Attachments;
            }
            insert PDFInvoiceUpload;

            file.ParentId = PDFInvoiceUpload.ID;
            file.Description = UPLOADED_INVOICE;
            insert file;

            Task t = new Task();
            t.WhatId = PDFInvoiceUpload.ID;
            t.ActivityDate = Date.today();
            t.Subject = 'Statement uploaded by ' + CurrentUser.Account_Name__c;
            t.Assigned_to_Group__c = 'Finance';
            insert t;


            A2HCException.formatMessage('File uploaded.');
            PageReference pg = Page.SPPortalStatementUpload;
            return pg.setRedirect(true);
        }
        catch(Exception ex)
        {
            Database.rollback(sp);
            PDFInvoiceUpload = PDFInvoiceUpload.clone(false);
            file = new Attachment();
            return A2HCException.formatException(ex);
        }
    }

    public Pagereference saveOverride()
    {
        Savepoint sp = Database.setSavepoint();
        try
        {
            Boolean valid = true;
            if(SelectedAccount == null)
            {
                A2HCException.formatException('Account is required.');
                file.Name = null;
                valid = false;
            }
            if(file.Name == null)
            {
                A2HCException.formatException('File is required.');
                file.Name = null;
                valid = false;
            }
            if(!valid)
            {
                return null;
            }

            if(InvoiceUpload.ID != null)
            {
                delete InvoiceUpload.Attachments;
            }
            InvoiceUpload.File_Name__c = file.Name;
            InvoiceUpload.Account__c = SelectedAccount;
            InvoiceUpload.OwnerId = UserInfo.getUserId();
            upsert InvoiceUpload;

            file.ParentId = InvoiceUpload.ID;
            insert file;

            PageReference pg = null;
            if(!parseCSV(false))
            {
                Database.rollback(sp);
                InvoiceUpload = InvoiceUpload.clone(false);
                file = new Attachment();
            }
            else
            {
                pg = Page.RCRUploadSummary;
                pg.getParameters().put('id', InvoiceUpload.ID);
                pg.setredirect(false);
            }
            
            return pg;
        }
        catch(Exception ex)
        {
            Database.rollback(sp);
            InvoiceUpload = InvoiceUpload.clone(false);
            file = new Attachment();
            if(ex instanceof System.ListException)
            {
                return A2HCException.formatException(ErrorMsgs.get('Upload_Index_Out_Of_Bounds'));
            }
            return A2HCException.formatException(ex);
        }
    }

    private Boolean parseCSV(Boolean confirmReplaceInvoices)
    {
        final Integer SERVICE_PROVIDER_CODE_INDEX = 0;
        final Integer SERVICE_ORDER_INDEX = 1;
        final Integer CLIENT_NAME_INDEX = 2;
        final Integer INVOICE_DATE_INDEX = 3;
        final Integer INVOICE_STARTDATE_INDEX = 4;
        final Integer INVOICE_ENDDATE_INDEX = 5;
        final Integer INVOICE_NUM_INDEX = 6;
        final Integer SERVICE_DATE_INDEX = 7;
        final Integer SERVICE_CODE_INDEX = 8;
        final Integer GL_CODE_INDEX = 9;
        final Integer SERVICE_QTY_INDEX = 10;
        final Integer SERVICE_RATE_INDEX = 11;
        final Integer SERVICE_GST_INDEX = 12;
        final Integer SERVICE_EXGST_INDEX = 13;
        final Integer SERVICE_TOTAL_INDEX = 14;
        final Integer UNIQUE_IDENTIFIER_INDEX = 15;
        final Integer LAST_COLUMN_INDEX = UNIQUE_IDENTIFIER_INDEX + 1;
//        final Integer SERVICE_TYPE_INDEX = 16;

        Map<String, List<RCR_Invoice_Item__c>> lineItems = new Map<String, List<RCR_Invoice_Item__c>>();
        Map<String, RCR_Invoice__c> newInvoices = new Map<String, RCR_Invoice__c>();
        List<RCR_Invoice__c> invoicesToDelete = new List<RCR_Invoice__c>();
        String accountCode = null;
        string fileBody = '';
        Integer lineCount = 2;
        Integer invoiceCount = 0;
        decimal decVal = 0.00;
        Date dateVal = null;

        exceptionCount = 0;

        try
        {
            queryLimit = Batch_Size__c.getInstance('RCR_Upload_Query_Tolerance').Records__c.intValue();
            heapLimit = Batch_Size__c.getInstance('RCR_Upload_Heap_Tolerance').Records__c.intValue();
            //scriptLimit = Batch_Size__c.getInstance('RCR_Upload_Script_Tolerance').Records__c.intValue();
        }
        catch(Exception ex)
        {
            A2HCException.formatException('Tolerance settings not defined.');
            return false;
        }

        try
        {
            fileBody = file.body.toString();
        }
        catch(Exception ex)
        {
            // If unable to convert the blob to a string dont throw an unfriendly error.
            // The user will still get the File Unreadable error because the file could not be split into fields.
        }

        CSVParser parser = new CSVParser(fileBody, true, LAST_COLUMN_INDEX);
        // Read line into string array.
        String[] invFields = parser.readLine();
        if(invFields == null)
        {
            // CSV Parser could not split the line into the field string array, or there was no line to read.
            A2HCException.formatException(ErrorMsgs.get('File_Unreadable'));
            return false;
        }
        if(invFields.size() == 0)
        {
            // CSV Parser could not split the line into the field string array, or there was no line to read.
            A2HCException.formatException('File does not contain any readable lines.');
            return false;
        }


        if(InPortal)
        {
            accountCode = CurrentUser.RCR_Service_Provider_Code__c;
        }
        else
        {
            for(Account a : [SELECT a.RCR_Service_Provider_Code__c
                            FROM    Account a
                            WHERE   a.ID = :SelectedAccount AND
                                    a.Available_to__c includes ('RCR')])
            {
                accountCode = a.RCR_Service_Provider_Code__c;
                break;
            }
        }
        if(accountCode == null || accountCode != invFields[SERVICE_PROVIDER_CODE_INDEX])
        {
            A2HCException.formatException('Line ' + lineCount.format() + ': Service Provider Code ' + invFields[SERVICE_PROVIDER_CODE_INDEX] + ' is invalid' + (InPortal ? '' : ' for the selected service provider') + '.');
            return false;
        }

        integer queryCount = Limits.getQueries();
        while (invFields != null && invFields.size() > 0 && !A2HCUtilities.isStringNullOrEmpty(invFields[SERVICE_PROVIDER_CODE_INDEX]))
        {
            if(accountCode != invFields[SERVICE_PROVIDER_CODE_INDEX])
            {
                A2HCException.formatException('Line ' + lineCount.format() + ': Service Provider Code ' + invFields[SERVICE_PROVIDER_CODE_INDEX] + ' is invalid.');
                return false;
            }
            if(A2HCUtilities.isTrimmedStringNullOrEmpty(invFields[INVOICE_NUM_INDEX]))
            {
                A2HCException.formatException('Line ' + lineCount.format() + ': Statement number is a required field .');
                return false;
            }
            RCR_Invoice__c invoice = null;
            String invoiceNumber = invFields[INVOICE_NUM_INDEX];
            if(invoiceNumber.length() > 12)
            {
                A2HCException.formatException('Line ' + lineCount.format() + ': Statement number ' + invFields[INVOICE_NUM_INDEX] + ' exceeds the maximum of 12 characters.');
                return false;
            }
            if(newInvoices.containsKey(invFields[INVOICE_NUM_INDEX]))
            {
                invoice = newInvoices.get(invFields[INVOICE_NUM_INDEX]);
            }
            else
            {
                invoice = new RCR_Invoice__c(RCR_Invoice_Upload__c = InvoiceUpload.Id,
                                            OwnerId = InvoiceUpload.OwnerId,
                                            Invoice_Number__c = invFields[INVOICE_NUM_INDEX],
                                            Date__c = parseDate(invFields[INVOICE_DATE_INDEX], 'Invoice Date', false, lineCount),
                                            Start_Date__c = parseDate(invFields[INVOICE_STARTDATE_INDEX], 'Invoice Start Date', true, lineCount),
                                            End_Date__c = parseDate(invFields[INVOICE_ENDDATE_INDEX], 'Invoice End Date', true, lineCount),
                                            Account__c = SelectedAccount);
                if(nearLimits(lineCount, null, queryCount++, newInvoices.size()))
                {
                    return false;
                }
                for(RCR_Invoice__c existingInvoice :
                                        [SELECT     Id,
                                                    Status__c,
                                                    RecordType.Name,
                                                    Created_By_Portal_User__c
                                            FROM    RCR_Invoice__c
                                            WHERE   Invoice_Number__c = :invoiceNumber
                                            AND     Account__c = :SelectedAccount])
                {
                    if(existingInvoice.RecordType.Name == 'Submitted')
                    {
                        A2HCException.formatException('Line ' + lineCount.format() + ': Activity statement ' + invFields[INVOICE_NUM_INDEX] + ' has already been submitted for payment.');
                        return false;
                    }
                    if(existingInvoice.Status__c == APS_PicklistValues.RCRInvoice_Status_ReconciliationInProgress)
                    {
                        A2HCException.formatException('Line ' + lineCount.format() + ': Activity statement ' + invFields[INVOICE_NUM_INDEX] + ' currently being processed.');
                        return false;
                    }
                    if(InPortal && existingInvoice.Created_By_Portal_User__c != 'Y')
                    {
                        A2HCException.formatException('Line ' + lineCount.format() + ': Activity statement ' + invFields[INVOICE_NUM_INDEX] + ' was created internally by DCSI and cannot be overwritten.');
                        return false;
                    }
                    else
                    {
                        // Otherwise delete the Invoice so it can be overwritten with a new version.
                        invoicesToDelete.add(existingInvoice);
                    }
                }
                // cant put in a list as we need the ID for the line items
                if(invoice.Invoice_Number__c != null)
                {
                    newInvoices.put(invoice.Invoice_Number__c, invoice);
                    lineItems.put(invoice.Invoice_Number__c, new List<RCR_Invoice_Item__c>());
                }
            }   
            String uniqueID = invFields.size() <= UNIQUE_IDENTIFIER_INDEX ? '' : invFields[UNIQUE_IDENTIFIER_INDEX];          
//            String serviceType = invFields.size() <= SERVICE_TYPE_INDEX ? '' : invFields[SERVICE_TYPE_INDEX];
            RCR_Invoice_Item__c lineItem = new RCR_Invoice_Item__c( Service_Order_Number__c = invFields[SERVICE_ORDER_INDEX],
                                                                    Activity_Date__c = parseDate(invFields[SERVICE_DATE_INDEX], 'Activity Date', false, lineCount),
                                                                    Code__c = invFields[SERVICE_CODE_INDEX],
                                                                    Quantity__c = parseDecimal(invFields[SERVICE_QTY_INDEX], 'Quantity', lineCount).setScale(2),
                                                                    Rate__c = parseDecimal(invFields[SERVICE_RATE_INDEX], 'Rate', lineCount).setScale(2),
                                                                    GST__c = parseDecimal(invFields[SERVICE_GST_INDEX], 'GST', lineCount).setScale(2),
                                                                    Total__c = parseDecimal(invFields[SERVICE_TOTAL_INDEX], 'Total', lineCount).setScale(2),
                                                                    Unique_Identifier__c = uniqueID,
                                                                    RCR_Invoice__c = invoice.Id,
                                                                    Bulk_Upload__c = true,
                                                                    Client_Name_As_Loaded__c = invFields[CLIENT_NAME_INDEX],
                                                                    GL_Code__c = invFields[GL_CODE_INDEX]);
            if(exceptionCount == 0)
            {
                lineItems.get(invoice.Invoice_Number__c).add(lineItem);
            }
            if(nearLimits(lineCount, null, null, newInvoices.size()) || exceptionCount > 10)
            {
                return false;
            }
            invFields = parser.readLine();
            lineCount++;
            system.debug(lineCount);
        }
        parser = null;

        if(exceptionCount > 0 || nearLimits(lineCount, invoicesToDelete.size(), null, newInvoices.size()))
        {
            return false;
        }
        delete invoicesToDelete;

        if(nearLimits(lineCount, newInvoices.size(), null, newInvoices.size()))
        {
            return false;
        }
        insert newInvoices.values();

        Integer lineItemsSoFar = 0;
        for(string key : lineItems.keySet())
        {
            for(RCR_Invoice_Item__c li : lineItems.get(key))
            {
                li.RCR_Invoice__c = newInvoices.get(key).ID;
            }
            lineItemsSoFar = lineItemsSoFar + lineItems.get(key).size();
            if(nearLimits(lineCount, lineItemsSoFar, null, newInvoices.size()))
            {
                return false;
            }
            upsert lineItems.get(key);
        }
        lineItems = null;
        return true;
    }

    private decimal parseDecimal(String str, String fieldName, Integer lineNum)
    {
        decimal decVal = A2HCUtilities.tryParseDecimal(str);
        if(decVal != null)
        {
            return decVal;
        }
        else
        {
            exceptionCount++;
            if(fieldName == 'Rate')
            {
                A2HCException.formatException('Line ' + lineNum.format() + ': ' + errorMsgs.get('Invoice_Upload_Invalid_Rate'));
            }
            else
            {
                A2HCException.formatException('Line ' + lineNum.format() + ': ' + fieldName + ': ' + str + ' is not a valid number.');
            }
            return 0;
        }
    }

    private Date parseDate(String str, String fieldName, Boolean allowNull, Integer lineNum)
    {
        if(allowNull && A2HCUtilities.isStringNullOrEmpty(str))
        {
            return null;
        }
        Date dateVal = A2HCUtilities.tryParseDate(str);
        if(dateVal != null)
        {
            return dateVal;
        }
        else
        {
            exceptionCount++;
            A2HCException.formatException('Line ' + lineNum.format() + ': ' + fieldName + ': ' + str + ' is not a valid date.');
            return null;
        }
    }

    private Boolean nearLimits(Integer itemCount, Integer dmlRows, Integer queries, Integer invoiceCount)
    {
        String msg = null;
        dmlRows = dmlRows == null ? 0 : dmlRows;
        queries = queries == null ? Limits.getQueries() : queries;

        if(Limits.getQueries() >= (Limits.getLimitQueries() - queryLimit))
        {
            msg = 'Error 1:Too many activity statements in this file. Reducing the number of activity statements below ' + (invoiceCount + 1).format()  + ' may be successful.';
        }
        else if(Limits.getDMLRows() + itemCount >= Limits.getLimitDMLRows())
        {
            msg = 'Error 2:Too many lines in this file. Reducing the number of lines below ' + itemCount.format() + ' may be successful.';
        }
        else if(//Limits.getScriptStatements() >= (Limits.getLimitScriptStatements() - scriptLimit) ||
                Limits.getHeapSize() >= (Limits.getLimitHeapSize() - heapLimit))
        {
            msg = 'Error ' + (itemCount == null ? '5:' : '4:') + DEFAULT_LIMIT_MSG.replace('%0', (itemCount == null ? '3000' : itemCount.format()));
        }
        if(msg != null)
        {
            A2HCException.formatException(msg);
            return true;
        }
        return false;
    }
    
    /*
    @IsTest(SeeAllData=true)
    static void  test1()
    {
        User usr;
        Account acc;
        
        for(Contact c : [SELECT Id,
                                AccountId
                         FROM   Contact
                         WHERE  Id IN (SELECT ContactId
                                        FROM    User
                                        WHERE   UserType = 'CspLitePortal' AND
                                        IsActive = true)
                        AND     AccountID IN (SELECT Account__c FROM RCR_Invoice__c WHERE  
                                                ( Status__c = 'Loaded' OR
                                                 Status__c LIKE 'Reconciled%') AND
                                        RCR_Invoice_Upload__c != null)
                        LIMIT 1])
        {
            acc = [SELECT RCR_Service_Provider_Code__c,
                          (SELECT   Name
                           FROM     RCR_Contracts__r
                           LIMIT 1)
                   FROM   Account
                   WHERE  Id = :c.AccountId];
                   
            usr = [SELECT Id
                    FROM    User
                    WHERE   ContactId = :c.Id];
        }
        
        System.runAs(usr)
        {           
            doTest(acc, false);
        }
    }
    */
    
    @IsTest//(SeeAllData=true)
    static void  test2()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        Account acc = [SELECT RCR_Service_Provider_Code__c,
                                (SELECT Name
                                FROM    RCR_Contracts__r
                                LIMIT 1)
                        FROM    Account
                        WHERE   RCR_service_Provider_code__c != null AND
                                ID IN (SELECT Account__c
                                        FROM    RCR_Contract__c)                                        
                        LIMIT 1];
                        
        doTest(acc, false);
        
        Test.StopTest();
    }
    
    @IsTest//(SeeAllData=true)
    static void  test3()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        Account acc = [SELECT RCR_Service_Provider_Code__c,
                                (SELECT Name
                                FROM    RCR_Contracts__r
                                LIMIT 1)
                        FROM    Account
                        WHERE   RCR_service_Provider_code__c != null AND
                                ID IN (SELECT Account__c
                                        FROM    RCR_Contract__c)                                        
                        LIMIT 1];
                        
        doTest(acc, true);
        
        Test.StopTest();
    }
    
    @IsTest//(SeeAllData=true)
    static void  test4()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        Account acc = [SELECT RCR_Service_Provider_Code__c,
                                (SELECT Name
                                FROM    RCR_Contracts__r
                                LIMIT 1)
                        FROM    Account
                        WHERE   RCR_service_Provider_code__c != null AND
                                ID IN (SELECT Account__c
                                        FROM    RCR_Contract__c)                                        
                        LIMIT 1];
                        
        doTest(acc, true);
        
        Test.StopTest();
    }
    
    private static void doTest(Account acc, Boolean invalidCSVFile)
    {
        
    
        RCR_Contract__c contract = acc.RCR_Contracts__r[0];
                    
        RCR_Invoice__c inv = [SELECT ID,
                                    Invoice_number__c,
                                    RCR_Invoice_Upload__c
                            FROM    RCR_Invoice__c
                            /*WHERE  ( Status__c = 'Loaded' OR
                                     Status__c LIKE 'Reconciled%') AND
                                        RCR_Invoice_Upload__c != null*/
                            LIMIT 1];

        Test.setCurrentPage(Page.RCRInvoiceUpload);
        ApexPages.currentPage().getParameters().put('id', inv.RCR_Invoice_Upload__c);
        RCRInvoiceUploadExtension ext = new RCRInvoiceUploadExtension();
        
        ext.InvoiceUpload = null;
        system.debug(ext.InvoiceUpload);

        ext.file = null;
        system.debug(ext.file);

        ext.InvoiceUpload = null;
        system.debug(ext.InvoiceUpload);

        ext.SelectedAccount = null;
        system.debug(ext.SelectedAccount);
        
        system.debug(ext.RCRProviderList);

        system.debug(ext.InPortal);

        ext.ErrorMsgs = null;
        system.debug(ext.ErrorMsgs);

        String csv;
        
        if(invalidCSVFile)
        {
            csv= 'Program,Contract Number,n' +
                        acc.RCR_Service_Provider_Code__c + ',' + contract.Name + ',,\n' + 
                        acc.RCR_Service_Provider_Code__c + ',' + contract.Name + ',,01/08/2011,01/07/2011,31/07/2011';
        }
        else
        {
            csv= 'Program,Contract Number,Client Name,Invoice Date,Invoice Start Date,Invoice End Date,Invoice Number,Service Date,Code,Qty,Rate,GST %,Total Ex Gst,Total inc GST\n' +
                        acc.RCR_Service_Provider_Code__c + ',' + contract.Name + ',,01/08/2011,01/07/2011,31/07/2011,' + inv.Invoice_number__c + ',62222,28/07/2011,HL FT LEVEL 2,1,82.95,8.30,82.95,91.25\n' +
                        acc.RCR_Service_Provider_Code__c + ',' + contract.Name + ',,01/08/2011,01/07/2011,31/07/2011,' + inv.Invoice_number__c + ',29/07/2011,HL FT LEVEL 2,1,82.95,8.30,82.95,91.25';
        }
        Attachment att = new Attachment();
        att.Name = 'test';
        att.Description = 'test';
        att.Body = Blob.valueOf(csv);
        ext.file = att;
        ext.file.Name = 'test';
        
        ext.SelectedAccount = acc.ID;
        ext.saveOverride();
        
        ext.confirmSave();
        
        ext.SelectedAccount = null;
        ext.saveOverride();
        
        ext.cancel();
    }
}