public with sharing class MCPortalClientController 
{ 
    private transient List<Referred_Client__c> tParticipants;

    public String ParticipantSearch 
    {
        get;
        set;
    }

    public List<Referred_Client__c> Participants
    {
        get
        {
            if(tParticipants == null)
            {
                tParticipants = new List<Referred_Client__c>();
            }
            return tParticipants;
        }
        set;
    }

    public void search()
    {
    	try
    	{
    		String clientName = (String.isBlank(ParticipantSearch) ? '' : ('%' + ParticipantSearch)) + '%';
    		tParticipants = [SELECT 	ID,
    								    Name
    					    FROM 		Referred_client__c 
    					    WHERE 		Name LIKE :clientName 
						    ORDER BY	Name];
    	}
    	catch(Exception ex)
    	{
    		A2HCException.formatException(ex);
    	}
    }
}