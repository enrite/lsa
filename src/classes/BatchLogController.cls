public class BatchLogController 
{
	public BatchLogController()
	{
	}
	
	public String BatchID
	{
		get;
		set;
	}
		
	public string SalesforceURL
	{
		get
		{
			return URL.getSalesforceBaseUrl().toExternalForm();
		}
		private set;
	}
	
	public Map<String, List<Batch_Log__c>> SucessLogEntriesByName
	{
		get
		{
			if(SucessLogEntriesByName == null)
			{
				SucessLogEntriesByName = new Map<String, List<Batch_Log__c>>(); 				
				for(Batch_Log__c log : [SELECT	b.Id,
											   	b.Name,
											   	b.Record_Id__c,
											   	b.Record_Name__c,
											   	b.Message__c,
											   	Error__c
									    FROM	Batch_Log__c b
									    WHERE	b.Name = :BatchID
									    ORDER BY b.Record_Name__c, 
									    		SystemModStamp])
				{			
					if(String.isBlank(log.Error__c))
					{
						// don't log messages for these, just the service order name is enough							
						if(log.Message__c != null && log.Message__c.contains('Rollover Status: Review'))
						{						
							if(!ReviewLogEntriesByName.containsKey(log.Record_Name__c))
							{
								ReviewLogEntriesByName.put(log.Record_Name__c, new List<Batch_Log__c>());
							}
						}
						else if(!ReviewLogEntriesByName.containsKey(log.Record_Name__c))
						{
							if(!SucessLogEntriesByName.containsKey(log.Record_Name__c))
							{
								SucessLogEntriesByName.put(log.Record_Name__c, new List<Batch_Log__c>());
							}
						} 
					}
					else
					{
						if(!ErrorLogEntriesByName.containsKey(log.Record_Name__c))
						{
							ErrorLogEntriesByName.put(log.Record_Name__c, new List<Batch_Log__c>());
						}
						ErrorLogEntriesByName.get(log.Record_Name__c).add(log);
					}
				}
				for(String s : ReviewLogEntriesByName.keySet())
				{
					SucessLogEntriesByName.remove(s);
				}
				for(String s : ErrorLogEntriesByName.keySet())
				{
					SucessLogEntriesByName.remove(s);
					ReviewLogEntriesByName.remove(s);
				}
			}
			return SucessLogEntriesByName;
		}
		private set;
	}
	
	public Map<String, List<Batch_Log__c>> ErrorLogEntriesByName
	{
		get
		{
			if(ErrorLogEntriesByName == null)
			{
				ErrorLogEntriesByName = new Map<String, List<Batch_Log__c>>();
				// force a get
				system.debug(SucessLogEntriesByName);
			}
			return ErrorLogEntriesByName;
		}
		private set;
	}
	
	public Map<String, List<Batch_Log__c>> ReviewLogEntriesByName
	{
		get
		{
			if(ReviewLogEntriesByName == null)
			{
				ReviewLogEntriesByName = new Map<String, List<Batch_Log__c>>();
				// force a get
				system.debug(SucessLogEntriesByName);
			}
			return ReviewLogEntriesByName;
		}
		private set;
	}
	
	public List<String> SuccessLogNames
	{
		get
		{
			if(SuccessLogNames == null)
			{
				SuccessLogNames = new List<String>(SucessLogEntriesByName.keySet());
				SuccessLogNames.sort();
			}
			return SuccessLogNames;
		}
		private set;
	}
	
	public List<String> ErrorLogNames
	{
		get
		{
			if(ErrorLogNames == null)
			{
				ErrorLogNames = new List<String>(ErrorLogEntriesByName.keySet());
				ErrorLogNames.sort();
			}
			return ErrorLogNames;
		}
		private set;
	}
	
	public List<String> ReviewLogNames
	{
		get
		{
			if(ReviewLogNames == null)
			{
				ReviewLogNames = new List<String>(ReviewLogEntriesByName.keySet());
				ReviewLogNames.sort();
			}
			return ReviewLogNames;
		}
		private set;
	}
	
	static testMethod void test1()
	{
		BatchLogController blc = new BatchLogController();
		String logId = 'Test';
		Batch_Log__c log = new Batch_Log__c(Name = logId, Record_Id__c = 'Test', Record_Name__c = 'Review', Message__c = 'Rollover Status: Review', Error__c = null);
		insert log;
		
		Batch_Log__c log2 = new Batch_Log__c(Name = logId, Record_Id__c = 'Test', Record_Name__c = 'Error', Message__c = 'Testing', Error__c = 'Test');
		insert log2;
		
		log2 = new Batch_Log__c(Name = logId, Record_Id__c = 'Test', Record_Name__c = 'Both', Message__c = 'Testing', Error__c = 'Test');
		insert log2;
		
		log2 = new Batch_Log__c(Name = logId, Record_Id__c = 'Test', Record_Name__c = 'Success', Message__c = 'Testing', Error__c = null);
		insert log2;
		
		blc.BatchID = logId;
		blc.SalesforceURL = 'www.google.com.au';
		
		system.debug(blc.BatchID);
		system.debug(blc.SalesforceURL);

		
		system.debug(blc.SuccessLogNames); 
		system.debug(blc.ErrorLogNames); 
		system.debug(blc.ReviewLogNames); 
	}
}