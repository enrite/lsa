public with sharing class RCRCompensableStatementPDFController extends A2HCPageBase 
{
//    public RCRCompensableStatementPDFController()
//    {
//        string compensableStatementId = getParameter('id');
//
//        if(CompensableStatementId != null)
//        {
//            getCompensableStatement(compensableStatementId);
//        }
//    }
//
//    public RCR_Compensable_Statement__c CompensableStatement
//    {
//        get;
//        private set;
//    }
//
//    private void getCompensableStatement(string compensableStatementID)
//    {
//        CompensableStatement = [SELECT  id,
//                                        Name,
//                                        Date__c,
//                                        Masterpiece_Invoice_Date__c,
//                                        Masterpiece_Invoice_Number__c,
//                                        Notes__c,
//                                        Client__c,
//                                        Client__r.Name,
//                                        Client__r.Title__c,
//                                        Client__r.Date_Of_Birth__c,
//                                        Client__r.Compensable_Billing_Attention__c,
//                                        Client__r.Compensable_Billing_Name__c,
//                                        Client__r.Compensable_Billing_Address__c,
//                                        Client__r.Compensable_Solicitor_Reference__c,
//                                        Client__r.Compensable_Claim_Number__c,
//                                        Sum_Total_Inc_GST__c,
//                                        (SELECT id,
//                                                Item__c,
//                                                Start_Date__c,
//                                                End_Date__c,
//                                                Quantity__c,
//                                                Rate__c,
//                                                Total_Ex_GST__c,
//                                                Total_GST__c,
//                                                Total_Inc_GST__c,
//                                                Service_Provider__c,
//                                                Service_Provider__r.Name
//                                         FROM   RCR_Compensable_Statement_Items__r)
//                                 FROM   RCR_Compensable_Statement__c
//                                 WHERE  Id = :compensableStatementID];
//    }
//
//    @IsTest//(SeeAllData=true)
//    private static void test()
//    {
//        TestLoadData.loadRCRData();
//
//        Test.StartTest();
//
//        Date d = Date.newInstance(2013,8,9);
//
//        Account acc = [SELECT   Id
//                       FROM     Account
//                       //WHERE    Name = 'TestAccount'
//                       LIMIT 1];
//
//        Referred_Client__c client = [SELECT Id
//                                     FROM   Referred_Client__c
//                                     //WHERE  Family_Name__c = 'Test'
//                                     LIMIT 1];
//
//        RCR_Compensable_Statement__c statement = new RCR_Compensable_Statement__c();
//        statement.Date__c = d;
//        statement.Masterpiece_Invoice_Date__c = d;
//        statement.Masterpiece_Invoice_Number__c = 'Testing 123';
//        statement.Notes__c = 'Hello World.';
//        statement.Client__c = client.Id;
//        insert statement;
//
//        RCR_Compensable_Statement_Item__c ci = new RCR_Compensable_Statement_Item__c();
//        ci.Item__c = 'Case Management';
//        ci.Start_Date__c = d;
//        ci.End_Date__c = d.addMonths(1);
//        ci.Quantity__c =  10;
//        ci.Rate__c = 10.0;
//        ci.Total_GST__c = 10.0;
//        ci.Service_Provider__c = acc.Id;
//        ci.RCR_Compensable_Statement__c = statement.Id;
//        insert ci;
//
//        PageReference pg = Page.RCRCompensableStatementPDF;
//        pg.getParameters().put('id', statement.Id);
//        Test.setCurrentPage(pg);
//
//        RCRCompensableStatementPDFController c = new RCRCompensableStatementPDFController();
//        system.debug(c.CompensableStatement);
//
//        Test.StopTest();
//    }
}