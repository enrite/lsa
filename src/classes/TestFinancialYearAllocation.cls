@isTest
private class TestFinancialYearAllocation
{
    @IsTest
    static void test1()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        RCR_Contract__c c = [SELECT Id
                             FROM RCR_Contract__c
                             LIMIT 1];
    
        FinancialYearAllocation fya = new FinancialYearAllocation(new ApexPages.StandardController(c));
        system.debug(fya.ServiceOrderPeriod);
        system.debug(fya.RequestClientServiceOrders);
        system.debug(fya.OtherClientRollovers);
        system.debug(fya.AllocationsByFinancialYear);
        system.debug(fya.AllocationsForServiceOrderPeriod);
        system.debug(fya.AllAllocationsForServiceOrderPeriod);
        system.debug(fya.ServiceOrderFinancialYears);
        system.debug(fya.FinancialYears);        
        system.debug(fya.ServiceOrderCost);
        system.debug(fya.RecurrentAllocationAmount);
        system.debug(fya.AllocationAvailable);
        system.debug(fya.RequestsNotYetApproved);
        system.debug(fya.TotalServiceOrderCost);
        system.debug(fya.TotalAllocationCost);
        system.debug(fya.TotalRequestCostNotApproved);
        system.debug(fya.TotalAllocationFYECost);
        system.debug(fya.TotalAvailableAllocation);
        system.debug(fya.FinancialYearsByID);
        fya.getSubmittedServiceOrders();
        
        FinancialYearAllocation.FinancialYearAllocationRow fyar = new FinancialYearAllocation.FinancialYearAllocationRow('test1', 'test1');
        FinancialYearAllocation.FinancialYearAllocationRow fyar2 = new FinancialYearAllocation.FinancialYearAllocationRow('test2', 'test2');
        List<FinancialYearAllocation.FinancialYearAllocationRow> fyars = new List<FinancialYearAllocation.FinancialYearAllocationRow>();
        fyars.add(fyar);
        fyars.add(fyar2);
        fyars.sort();
        
        system.debug(fyar.AllocationAvailable);
        system.debug(fyar.ServiceOrderCostString);
        system.debug(fyar.AllocationCostString);
        system.debug(fyar.AllocationAvailableString);
        system.debug(fyar.AllocationFYECostString);
        system.debug(fyar.RequestCostNotApprovedString);                
    
        Test.StopTest();
    }
}