public class RCRClientServiceAgreementExtension extends A2HCPageBase
{
    public RCRClientServiceAgreementExtension(ApexPages.StandardController controller) 
    {
        try
        {
            ContractId = getParameter('id');
            ContractServiceTotalQuantity = 0.00;
            InitializeViewModel();
            getContract();            
        }
        catch(exception ex)
        {
            A2HCException.formatException(ex);
        }
    }

    public PageReference displaySimple()
    {
        try
        {
            Boolean redirect = true;
            if(!RCRContract.Account__r.Available_To__c.contains('IF'))
            {
                redirect = RCRContract.Account__r.RCR_Date_Accepted_To_Provider_Panel__c  == null || 
                            RCRContract.Account__r.RCR_Date_Accepted_To_Provider_Panel__c > Date.today(); 
                if(redirect)
                {
                    PageReference pg = Page.RCRSimpleAgreementPDF;
                    pg.getParameters().put('id', ContractId);
                    return pg.setRedirect(false);
                }
            }
//            else
//            {
//                 PageReference pg = Page.IFClientAgreementFiles;
//                 pg.getParameters().put('id', ContractId);
//                 return pg.setRedirect(false);
//            }
                              
            return null;
        }
        catch(Exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }

    private void getContract()
    {
        RCRContract = [SELECT   Name,
                                Comments__c,
                                Start_Date__c,
                                End_Date__c,
                                Funding_Source__c,
                                Total_Service_Order_Cost__c,
                                CCMS_ID__c,
                                Group_Program__c,
                                CSA_Status__c,
                                CSA_Acceptance_Date__c,
                                Approver_User_Name__c,
                                Approver_User_ID__c,
                                Date_Approved__c,
                                Service_to_be_provided__c,
                                Client_Outcomes__c,
                                Health_Care_Provided__c,
                                Risk_Carer_Aggr__c,
                                Risk_Client_Aggr__c,
                                Risk_Asthmatic__c,
                                Risk_Diabetic__c,
                                Risk_2_Pers_Visit__c,
                                Risk_Conf_Issues__c,
                                Risk_Oth_Det__c,
                                Risk_Grass__c,
                                Risk_Behav_Oth__c,
                                Risk_Hlth_Oth__c,
                                Risk_Env_Oth__c,
                                Risk_Add_Oth__c,
                                Risk_Pets__c,
                                Risk_Remote__c, 
                                Risk_Allergies__c, 
                                Risk_Seizures__c,  
                                Risk_Univ_Prec__c,                              
//                                CBMS_Region__c,
//                                CBMS_Team__c,
//                                CBMS_Service_Coordinator__c,
//                                CBMS_Service_Coordinator_Phone__c,
//                                CBMS_Contract_Type__c,
                                RecordType.Name,
                                Account__r.ABN__c,
                                Account__r.Name,
                                Account__r.Phone,
                                Account__r.BillingStreet,
                                Account__r.BillingCity,
                                Account__r.BillingState,
                                Account__r.BillingPostalCode,
                                Account__r.Contact_First_Name__c,
                                Account__r.Contact_Surname__c,
                                Account__r.Fax,
                                Account__r.RCR_Date_Accepted_To_Provider_Panel__c,
                                Account__r.Available_To__c,
                                Client_Address__c, 
                                Client__r.Name,
                                Client_Name__c,
                                Client__r.CCMS_ID__c,
                                Client__r.Title__c,
                                Client_Suburb__c,    
                                Client_State__c,  
                                Client_Postcode__c, 
                                Client_Phone__c,      
                                Client__r.Date_Of_Birth__c,  
                                Service_Coordinator__c,
                                Service_Coordinator__r.Name,
                                Service_Coordinator__r.Phone,              
                                RCR_Office_Team__r.Name,
                                RCR_Office_Team__r.Region__c,      
//                                RCR_CBMS_Contract__r.Region__c,
//                                RCR_CBMS_Contract__r.Service_Description__c,
//                                RCR_CBMS_Contract__r.Outcomes_Outputs_for_client__c,
//                                RCR_CBMS_Contract__r.Client__c,
                                (SELECT Id,
                                        Name,
                                        Authorised_Person__c,
                                        Authorised_Person__r.Name,
                                        Contract_Manager_Fax__c,
                                        Contract_Manager_Name__c,
                                        Contract_Manager_Phone__c,
                                        Acceptance_Status__c,
                                        Date_Accepted_Rejected__c,
                                        Position__c,
                                        RCR_Service_Order__c,
                                        Staff_Member_Mobile__c,
                                        Staff_Member_Name__c,
                                        Staff_Member_Phone__c
                              FROM      RCR_Service_Order_Acceptances__r
                              ORDER BY  LastModifiedDate desc
                              LIMIT 1)
                        FROM    RCR_Contract__c
                        WHERE   Id = :ContractId
                        LIMIT 1];                     
    }


    public string ContractId
    {
        get;
        set;
    }
    
    public RCR_Contract__c RCRContract
    {
        get;
        set;
    }
    
    public string CSAStatus
    {
        get
        {
            return RCRContract.CSA_Status__c;
        }
    }

    public string ServiceOrderNumber
    {
        get
        {
            return RCRContract.Name;
        }
    }

    public Referred_Client__c Client
    {
        get
        {
            if(Client == null)
            {
                Client = [SELECT r.Id,
                                 r.Name,
                                 r.Year_of_Birth__c,
                                 r.Work_Phone__c,
                                 r.Title__c,
                                 r.Suburb__c,
                                 r.Status__c,
                                 r.State__c,
                                 r.Sex__c,
                                 r.Sector_Client_ID__c,
                                 r.SLK__c,
                                 r.PostCode__c,
                                 r.Phone__c,
                                 r.Mobile__c,
                                 r.Main_language_spoken_at_home__c,
                                 r.Indigenous_Status__c,
                                 r.Given_Name__c,
                                 r.Full_Name__c,
                                 r.Family_Name__c,
                                 r.Date_Of_Birth__c,
                                 r.Date_Client_Registered__c,
                                 r.DSDC_ID__c,
                                 r.DOB_Estimated__c,
                                 r.Country_of_Birth__c,
                                 r.Client_ID__c,
                                 r.CCMS_ID__c,
                                 r.Age__c,
                                 r.Address__c,
                                 r.Account__c
                          FROM   Referred_Client__c r
                          WHERE  Id = :RCRContract.Client__c];
            }
            return Client;
        }
        set;
    }

    public string ClientDateOfBirth
    {
        get
        {
            DateTime dob = RCRContract.Client__r.Date_Of_Birth__c; 
            return (dob == null)?'':dob.format('dd/MM/yyyy');
        }
    }


    public string ClientRegion
    {
        get
        {
            //return RCRContract.RecordType.Name == 'Service Order - CBMS' ? ((RCRContract.Client__r.Name == null) ? '' : RCRContract.RCR_CBMS_Contract__r.Region__c) :
            return RCRContract.RCR_Office_Team__r.Region__c;
        }
    }


    public Decimal ContractServiceTotalQuantity
    {
        get;
        set;
    }

    public DateTime CurrentDate
    {
        get
        {
            return datetime.now();
        }
    }

    public DateTime ProposalSignedBy
    {
        get
        {
            return datetime.now().addDays(30);
        }
    }

    public String ClientServiceAgreementParticulars
    {
        get
        {
            return RCR_Client_Service_Agreement__c.getValues('CSA').Client_Service_Agreement_Particulars__c;
        }
    }

    public String MinisterForDisabilities
    {
        get
        {
            return RCR_Client_Service_Agreement__c.getValues('CSA').Minister_for_Disabilities__c;
        }
    }


    public String MinistersContractManagerName
    {
        get
        {
            return RCR_Client_Service_Agreement__c.getValues('CSA').Minister_s_Contract_Manger__c;
        }
    }

    public String MinistersContractManagerPosition
    {
        get
        {
            return RCR_Client_Service_Agreement__c.getValues('CSA').Ministers_Contract_Manager_Position__c;
        }
    }    

    public String ContractManagerPhone 
    {
        get
        {
            return RCR_Client_Service_Agreement__c.getValues('CSA').Contract_Manager_Phone__c;
        }
    }

    public String ContractManagerFax
    {
        get
        {
            return RCR_Client_Service_Agreement__c.getValues('CSA').Contract_Manager_Fax__c;
        }
    }

    public Decimal PublicLiabilityInsurance
    {
        get
        {
            return RCR_Client_Service_Agreement__c.getValues('CSA').Public_Liability_Insurance__c;
        }
    }


    public string ServiceDescription
    {
        get
        {
            //return RCRContract.RecordType.Name == 'Service Order - CBMS' ? RCRContract.RCR_CBMS_Contract__r.Service_Description__c : RCRContract.Service_to_be_provided__c;
            return RCRContract.Service_to_be_provided__c;
        }
    }

    public string OutcomesForClient
    {
        get
        {
            //return RCRContract.RecordType.Name == 'Service Order - CBMS' ? RCRContract.RCR_CBMS_Contract__r.Outcomes_Outputs_for_client__c : RCRContract.Client_Outcomes__c;
            return RCRContract.Client_Outcomes__c;
        }
    }

    public Decimal PublicIndemnityInsurance
    {
        get
        {
            return RCR_Client_Service_Agreement__c.getValues('CSA').Public_Indemnity_Insurance__c;
        }
    }

    public List<ContractServiceTypeViewModel> ContractServiceTypes
    {
        get;
        set;
    }


    public List<ContractServiceTypeViewModel> ContractSheduledServices
    {
        get;
        set;
    }
    
    
    public List<PeriodBreakdownViewModel> ContractPeriodBreakdowns
    {
        get
        {
            if(ContractPeriodBreakdowns == null)
            {
                ContractPeriodBreakdowns = new List<PeriodBreakdownViewModel>();
            }
            return ContractPeriodBreakdowns;
        }
        set;
    }
    
    public List<RateBreakdownViewModel> ContractRateBreakdowns
    {
        get
        {
            if(ContractRateBreakdowns == null)
            {
                ContractRateBreakdowns = new List<RateBreakdownViewModel>();
            }
            return ContractRateBreakdowns;
        }
        set;
    }
    

    private void initializeViewModel()
    {
        Map<Id, RCR_Contract_Service_Type__c> serviceTypesMap = new Map<Id, RCR_Contract_Service_Type__c>();
        ContractServiceTypes = new List<ContractServiceTypeViewModel>();
        ContractSheduledServices = new List<ContractServiceTypeViewModel>();

        for(RCR_Contract_Scheduled_Service__c css : [SELECT Id,
                                                            Name,
                                                            Start_Date__c,
                                                            End_Date__c,
                                                            RCR_Service__r.Name,
                                                            RCR_Service__r.Description__c,
                                                            Flexibility__c,
                                                            Total_Quantity__c,
                                                            Total_Cost__c,
                                                            (SELECT Id,
                                                                    Name,
                                                                    Start_Date__c,
                                                                    End_Date__c,
                                                                    Quantity__c,
                                                                    Rate__c,
                                                                    Total__c,
                                                                    Public_Holiday__c
                                                              FROM  RCR_Scheduled_Service_Breakdowns_By_Rate__r
                                                              ORDER BY Start_Date__c),
                                                             (SELECT Id,
                                                                     Name,
                                                                     Service_Type__c,
                                                                     Quantity__c
                                                              FROM   RCR_Contract_Service_Types__r)
                                                      FROM  RCR_Contract_Scheduled_Service__c
                                                      WHERE RCR_Contract__c = :ContractId])
        {
            ContractPeriodBreakdowns.add(new PeriodBreakdownViewModel(css, null));
            ContractRateBreakdowns.add(new RateBreakdownViewModel(css, null));
        }
        
        for(RCR_Contract_AdHoc_Service__c ahs : [SELECT Id,
                                                        Name,
                                                        Start_Date__c,
                                                        End_Date__c,
                                                        Qty__c,
                                                        Comment__c,
                                                        Rate__c,
                                                        Amount__c
                                                 FROM   RCR_Contract_AdHoc_Service__c
                                                 WHERE  RCR_Contract__c = :ContractId])
        {
            ContractPeriodBreakdowns.add(new PeriodBreakdownViewModel(null, ahs));
            ContractRateBreakdowns.add(new RateBreakdownViewModel(null, ahs));
        }
                                                                    
                                                                    

        for(RCR_Contract_Service_Type__c serviceType : [SELECT  id,
                                                                RCR_Program_Category_Service_Type__c,
                                                                RCR_Program_Category_Service_Type__r.Name,
                                                                RCR_Program__r.Name,
                                                                Quantity__c,
                                                                RCR_Contract_Scheduled_Service__r.Id,
                                                                RCR_Contract_Scheduled_Service__r.RCR_Contract__c,
                                                                RCR_Contract_Scheduled_Service__r.RCR_Service__r.Name,
                                                                RCR_Contract_Scheduled_Service__r.RCR_Service__r.Description__c,
                                                                RCR_Contract_Scheduled_Service__r.Start_Date__c,
                                                                RCR_Contract_Scheduled_Service__r.End_Date__c,
                                                                RCR_Contract_Scheduled_Service__r.Flexibility__c,
                                                                RCR_Contract_Scheduled_Service__r.Total_Cost__c,
                                                                RCR_Contract_Adhoc_Service__r.Id,
                                                                RCR_Contract_Adhoc_Service__r.Comment__c,
                                                                RCR_Contract_Adhoc_Service__r.Start_Date__c,
                                                                RCR_Contract_Adhoc_Service__r.End_Date__c,
                                                                RCR_Contract_Adhoc_Service__r.Amount__c
                                                        FROM    RCR_Contract_Service_Type__c
                                                        where RCR_Contract_Scheduled_Service__r.RCR_Contract__c = :ContractId
                                                        OR RCR_Contract_Adhoc_Service__r.RCR_Contract__c = :ContractId])
        {
            ContractServiceTypes.add(new ContractServiceTypeViewModel(ContractServiceTypes.size(), serviceType));
            ContractServiceTotalQuantity += serviceType.Quantity__c==null?0.00:serviceType.Quantity__c;

            if(!serviceTypesMap.containsKey(serviceType.RCR_Contract_Scheduled_Service__r.Id))
            {
                serviceTypesMap.put(serviceType.RCR_Contract_Scheduled_Service__r.Id, serviceType);
            }
            else
            {
                if(serviceTypesMap.get(serviceType.RCR_Contract_Scheduled_Service__r.Id).Quantity__c == null)
                {
                    serviceTypesMap.get(serviceType.RCR_Contract_Scheduled_Service__r.Id).Quantity__c = serviceType.Quantity__c;
                }
                else
                {
                    serviceTypesMap.get(serviceType.RCR_Contract_Scheduled_Service__r.Id).Quantity__c += (serviceType.Quantity__c==null?0:serviceType.Quantity__c);
                }
            }
        }

        for(RCR_Contract_Service_Type__c serviceType : serviceTypesMap.values())
        {
            ContractSheduledServices.add(new ContractServiceTypeViewModel(ContractSheduledServices.size(), serviceType));
        }
    }
    
    
    public class PeriodBreakdownViewModel
    {
        public PeriodBreakdownViewModel(RCR_Contract_Scheduled_Service__c pScheduledService, RCR_Contract_AdHoc_Service__c pAdHocService)
        {
            ScheduledService = pScheduledService;
            AdHocService = pAdHocService;
        }
        
        public RCR_Contract_Scheduled_Service__c ScheduledService
        {
            get;
            private set;
        }
        
        public RCR_Contract_AdHoc_Service__c AdHocService
        {
            get;
            private set;
        }
        
        public string ServiceDescription
        {
            get
            {
                return AdHocService == null ? ScheduledService.RCR_Service__r.Description__c + ' (' + ScheduledService.RCR_Service__r.Name + ')' : AdHocService.Comment__c;
            }
        }
        
        public date StartDate
        {
            get
            {
                return AdHocService == null ? ScheduledService.Start_Date__c : AdHocService.Start_Date__c;
            }
        }
        
        public date EndDate
        {
            get
            {
                return AdHocService == null ? ScheduledService.End_Date__c : AdHocService.End_Date__c;
            }
        }
        
        public string Period
        {
            get
            {
                if(AdHocService != null)
                {
                    return 'Service Date Range';
                }

                Schedule__c schedule;

                for(Schedule__c s : [SELECT Number_Of_Weeks_In_Period__c
                                    FROM    Schedule__c
                                    WHERE   Name = :ScheduledService.Id
                                    AND     Start_Date__c = :ScheduledService.Start_Date__c
                                    AND     End_Date__c = :ScheduledService.End_Date__c
                                    LIMIT 1])
                {
                    schedule = s;
                }

                if(schedule == null)
                {
                    return '';
                }

                return schedule.Number_Of_Weeks_In_Period__c;
            }
        }

        
        public decimal PeriodUnits
        {
            get
            {
                if(AdHocService != null)
                {
                    return AdHocService.Qty__c;
                }

                PeriodUnits = 0.0;
                for(RCR_Contract_Service_Type__c cst : ScheduledService.RCR_Contract_Service_Types__r)
                {
                    PeriodUnits += cst.Quantity__c == null ? 0.0 : cst.Quantity__c;
                }

                return PeriodUnits;
            }
            private set;
        }
        
        public string Flexibility
        {
            get
            {
                return AdHocService == null ? ScheduledService.Flexibility__c : 'Flexible across a date range';
            }
        }
        
        public decimal TotalUnits
        {
            get
            {
                return AdHocService == null ? ScheduledService.Total_Quantity__c : AdHocService.Qty__c;
            }
        }
        
        public decimal TotalCost
        {
            get
            {
                return AdHocService == null ? ScheduledService.Total_Cost__c : AdHocService.Amount__c;
            }
        }
    }    
        
        
    public class RateBreakdownViewModel
    {
        public RateBreakdownViewModel(RCR_Contract_Scheduled_Service__c pScheduledService, RCR_Contract_AdHoc_Service__c pAdHocService)
        {
            ScheduledService = pScheduledService;
            AdHocService = pAdHocService;
        }
        
        public RCR_Contract_Scheduled_Service__c ScheduledService
        {
            get;
            private set;
        }
        
        public RCR_Contract_AdHoc_Service__c AdHocService
        {
            get;
            private set;
        }
        
        public string ServiceDescription
        {
            get
            {
                return AdHocService == null ? ScheduledService.RCR_Service__r.Description__c + ' (' + ScheduledService.RCR_Service__r.Name + ')' : AdHocService.Comment__c;
            }
        }

        public List<RCR_Scheduled_Service_Breakdown_By_Rate__c> ScheduledServiceBreakdownsByRate
        {
            get
            {
                if(ScheduledServiceBreakdownsByRate == null)
                {
                    ScheduledServiceBreakdownsByRate = new List<RCR_Scheduled_Service_Breakdown_By_Rate__c>();
                    if(ScheduledService != null)
                    {
                        for(RCR_Scheduled_Service_Breakdown_By_Rate__c breakdown : ScheduledService.RCR_Scheduled_Service_Breakdowns_By_Rate__r)
                        {
                            ScheduledServiceBreakdownsByRate.add(breakdown);
                        }
                    }
                }
                return ScheduledServiceBreakdownsByRate;
            }
            private set;
        }

    }    

    public class ContractServiceTypeViewModel
    {
        RCR_Contract_Service_Type__c contractSheduledService;
        Boolean isAdHoc = false;

        public ContractServiceTypeViewModel(integer pRowIndx, RCR_Contract_Service_Type__c pContractSheduledService)
        {
            RowIndx = pRowIndx;
            contractSheduledService = pContractSheduledService;
            isAdHoc = (pContractSheduledService.RCR_Contract_Scheduled_Service__r.Id == null);
            Quantity = pContractSheduledService.Quantity__c;
        }

        public string ServiceType
        {
            get
            {
                return contractSheduledService.RCR_Program_Category_Service_Type__r.Name;
            }
        }

        public string Program
        {
            get
            {
                return contractSheduledService.RCR_Program__r.Name;
            }
        }

        public Decimal Quantity
        {
            get;
            set;
        }

        public string Name
        {
            get
            {
                return isAdHoc ? contractSheduledService.RCR_Contract_Adhoc_Service__r.Comment__c : contractSheduledService.RCR_Contract_Scheduled_Service__r.RCR_Service__r.Name;
            }
        }


        public string ServiceDescription
        {
            get
            {
                return isAdHoc ? contractSheduledService.RCR_Contract_Adhoc_Service__r.Comment__c : contractSheduledService.RCR_Contract_Scheduled_Service__r.RCR_Service__r.Description__c + ' (' + contractSheduledService.RCR_Contract_Scheduled_Service__r.RCR_Service__r.Name + ')';
            }
        }

        public Date StartDate
        {
            get
            {
                return isAdHoc?contractSheduledService.RCR_Contract_Adhoc_Service__r.Start_Date__c:contractSheduledService.RCR_Contract_Scheduled_Service__r.Start_Date__c;
            }
        }

        public Date EndDate
        {
            get
            {
                return isAdHoc?contractSheduledService.RCR_Contract_Adhoc_Service__r.End_Date__c:contractSheduledService.RCR_Contract_Scheduled_Service__r.End_Date__c;
            }
        }

        public string Flexibility
        {
            get
            {
                return isAdHoc ? 'Flexible across a date range' : contractSheduledService.RCR_Contract_Scheduled_Service__r.Flexibility__c;
            }
        }

        public string Period
        {
            get
            {
                if(isAdHoc)
                {
                    return '';
                }

                Schedule__c schedule;

                for(Schedule__c s : [SELECT Number_Of_Weeks_In_Period__c
                                    FROM    Schedule__c
                                    WHERE   Name = :contractSheduledService.RCR_Contract_Scheduled_Service__r.Id
                                    AND     Start_Date__c = :contractSheduledService.RCR_Contract_Scheduled_Service__r.Start_Date__c
                                    AND     End_Date__c = :contractSheduledService.RCR_Contract_Scheduled_Service__r.End_Date__c
                                    LIMIT 1])
                {
                    schedule = s;
                }

                if(schedule == null)
                {
                    return '';
                }

                return schedule.Number_Of_Weeks_In_Period__c;
            }
        }

        public Decimal TotalCost
        {
            get
            {
                return isAdHoc ? contractSheduledService.RCR_Contract_Adhoc_Service__r.Amount__c : contractSheduledService.RCR_Contract_Scheduled_Service__r.Total_Cost__c;
            }
        }

        public integer RowIndx
        {
            get;
            set;
        }

        public Decimal NormalUnits
        {
            get
            {
                if(isAdHoc)
                {
                    return null;
                }
                return (ContractSheduledService.Quantity__c == null ? 0 : ContractSheduledService.Quantity__c - PublicHolidayUnits);
            }
        }

        public Decimal PublicHolidayUnits
        {
            get
            {
                if(isAdHoc)
                {
                    return null;
                }

                AggregateResult[] sumHolidays = [SELECT SUM(public_Holiday_Quantity__c) Quantity
                                                 FROM   RCR_Scheduled_Service_Exception__c
                                                 WHERE  RCR_Contract_Scheduled_Service__c = :ContractSheduledService.RCR_Contract_Scheduled_Service__r.Id];

                Decimal publicHolidays = sumHolidays[0].get('Quantity')== null ? 0.00 : (Decimal)sumHolidays[0].get('Quantity');

                return publicHolidays;
            }
            set;
        }
    }

    @isTest//(SeeAllData=true)
    public static void test1()
    {
        TestLoadData.loadRCRData();
    
        Test.StartTest();
        
        TestLoadData.loadAdhocServices();
         
        Account a = [SELECT Id,
                            RCR_Date_Accepted_To_Provider_Panel__c
                     FROM Account
                     LIMIT 1];
                     
        a.RCR_Date_Accepted_To_Provider_Panel__c = null;
        update a;                              
                                         
        RCR_Contract__c contract = [SELECT  ID,
                                                Name,
                                                Account__c,
                                               // RCR_CBMS_Contract__c,
                                                Level__c,
                                                Start_Date__c,
                                                End_Date__c,
                                                Original_End_Date__c,
                                                Roll_Over_Service_Order__c,
                                                RecordTypeId
                                        FROM    RCR_Contract__c                                      
                                        WHERE   Account__r.RCR_Date_Accepted_To_Provider_Panel__c = null AND
                                                Client__c != null AND
                                                ID IN (SELECT   RCR_Contract__c
                                                        FROM    RCR_Contract_Scheduled_Service__c) AND
                                                ID IN (SELECT   RCR_Contract__c
                                                        FROM    RCR_Contract_Adhoc_Service__c)
                                        LIMIT 1];

        ApexPages.currentPage().getParameters().put('id', contract.Id);
        RCRClientServiceAgreementExtension ext = new  RCRClientServiceAgreementExtension(new ApexPages.Standardcontroller(contract));
        for(ContractServiceTypeViewModel cst : ext.ContractServiceTypes)
        {
            system.debug(cst.ServiceType);
            system.debug(cst.Program);
            system.debug(cst.Name);
            system.debug(cst.Quantity);
            system.debug(cst.StartDate);
            system.debug(cst.EndDate);
            system.debug(cst.Flexibility);
            system.debug(cst.TotalCost);
            system.debug(cst.PublicHolidayUnits);
            system.debug(cst.NormalUnits);
            system.debug(cst.RowIndx);
            system.debug(cst.Period); 
        }
        
        ext.displaySimple();
        
        system.debug(ext.CSAStatus);
        system.debug(ext.ServiceOrderNumber);
        system.debug(ext.Client);
        system.debug(ext.ClientDateOfBirth);
        system.debug(ext.ClientRegion);
        system.debug(ext.ContractServiceTotalQuantity);
        system.debug(ext.CurrentDate);
        system.debug(ext.ProposalSignedBy);
        system.debug(ext.ClientServiceAgreementParticulars);
        system.debug(ext.MinisterForDisabilities);
        system.debug(ext.MinistersContractManagerName);
        system.debug(ext.MinistersContractManagerPosition);
        system.debug(ext.ContractManagerPhone);
        system.debug(ext.ContractManagerFax);
        system.debug(ext.PublicLiabilityInsurance);
        system.debug(ext.ServiceDescription);
        system.debug(ext.OutcomesForClient);
        system.debug(ext.PublicIndemnityInsurance);   
        
        for(PeriodBreakdownViewModel pbvw : ext.ContractPeriodBreakdowns)
        {           
            system.debug(pbvw.ScheduledService);
            system.debug(pbvw.AdHocService);
            system.debug(pbvw.ServiceDescription);
            system.debug(pbvw.StartDate);
            system.debug(pbvw.EndDate);
            system.debug(pbvw.Period);
            system.debug(pbvw.PeriodUnits);
            system.debug(pbvw.Flexibility);
            system.debug(pbvw.TotalUnits);
            system.debug(pbvw.TotalCost);
        }
        
        for(RateBreakdownViewModel rbvw : ext.ContractRateBreakdowns)
        {
            system.debug(rbvw.ScheduledService);
            system.debug(rbvw.AdHocService);
            system.debug(rbvw.ServiceDescription);
            for(RCR_Scheduled_Service_Breakdown_By_Rate__c ssbr : rbvw.ScheduledServiceBreakdownsByRate)
            {
                system.debug(ssbr.Start_Date__c);
            }
        } 
        
        Test.StopTest();                               
    }
    
    @isTest//(SeeAllData=true)
    public static void test2()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();   
        
        TestLoadData.loadAdhocServices();     

        RCR_Contract__c contract = [SELECT  ID,
                                                Name,
                                                Account__c,
                                               // RCR_CBMS_Contract__c,
                                                Level__c,
                                                Start_Date__c,
                                                End_Date__c,
                                                Original_End_Date__c,
                                                Roll_Over_Service_Order__c,
                                                RecordTypeId
                                        FROM    RCR_Contract__c
                                        WHERE   Account__r.RCR_Date_Accepted_To_Provider_Panel__c != null AND
                                                Client__c != null AND
                                                ID IN (SELECT   RCR_Contract__c
                                                        FROM    RCR_Contract_Scheduled_Service__c)/* AND
                                                ID IN (SELECT   RCR_Contract__c
                                                        FROM    RCR_Contract_Adhoc_Service__c)*/
                                        LIMIT 1];
                                        
        ApexPages.currentPage().getParameters().put('id', contract.Id);
        RCRClientServiceAgreementExtension ext = new  RCRClientServiceAgreementExtension(new ApexPages.Standardcontroller(contract));
        for(ContractServiceTypeViewModel cst : ext.ContractServiceTypes)
        {
            system.debug(cst.ServiceType);
            system.debug(cst.Program);
            system.debug(cst.Name);
            system.debug(cst.Quantity);
            system.debug(cst.StartDate);
            system.debug(cst.EndDate);
            system.debug(cst.Flexibility);
            system.debug(cst.TotalCost);
            system.debug(cst.PublicHolidayUnits);
            system.debug(cst.NormalUnits);
            system.debug(cst.RowIndx);
            system.debug(cst.Period);   
            system.debug(cst.ServiceDescription);      
        }
        
        ext.displaySimple();
        
        system.debug(ext.CSAStatus);
        system.debug(ext.ServiceOrderNumber);
        system.debug(ext.Client);
        system.debug(ext.ClientDateOfBirth);
        system.debug(ext.ClientRegion);
        system.debug(ext.ContractServiceTotalQuantity);
        system.debug(ext.CurrentDate);
        system.debug(ext.ProposalSignedBy);
        system.debug(ext.ClientServiceAgreementParticulars);
        system.debug(ext.MinisterForDisabilities);
        system.debug(ext.MinistersContractManagerName);
        system.debug(ext.MinistersContractManagerPosition);
        system.debug(ext.ContractManagerPhone);
        system.debug(ext.ContractManagerFax);
        system.debug(ext.PublicLiabilityInsurance);
        system.debug(ext.ServiceDescription);
        system.debug(ext.OutcomesForClient);
        system.debug(ext.PublicIndemnityInsurance);   
        
        for(PeriodBreakdownViewModel pbvw : ext.ContractPeriodBreakdowns)
        {           
            system.debug(pbvw.ScheduledService);
            system.debug(pbvw.AdHocService);
            system.debug(pbvw.ServiceDescription);
            system.debug(pbvw.StartDate);
            system.debug(pbvw.EndDate);
            system.debug(pbvw.Period);
            system.debug(pbvw.PeriodUnits);
            system.debug(pbvw.Flexibility);
            system.debug(pbvw.TotalUnits);
            system.debug(pbvw.TotalCost);
        }
        
        for(RateBreakdownViewModel rbvw : ext.ContractRateBreakdowns)
        {
            system.debug(rbvw.ScheduledService);
            system.debug(rbvw.AdHocService);
            system.debug(rbvw.ServiceDescription);
            for(RCR_Scheduled_Service_Breakdown_By_Rate__c ssbr : rbvw.ScheduledServiceBreakdownsByRate)
            {
                system.debug(ssbr.Start_Date__c);
            }
        }
        
        Test.StopTest(); 
    }        
}