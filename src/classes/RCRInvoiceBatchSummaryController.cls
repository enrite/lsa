public with sharing class RCRInvoiceBatchSummaryController
    extends A2HCPageBase
{
	public RCRInvoiceBatchSummaryController() {}
	
	static testMethod void myUnitTest()
    {
    	RCRInvoiceBatchSummaryController ext = new RCRInvoiceBatchSummaryController();
    }
    /*public RCRInvoiceBatchSummaryController()
    {
        String invoiceID = getParameter('id');
		if(invoiceID != null)
		{
			InvoiceBatch = [SELECT 	r.Name,
									r.Id,
									r.File_Name__c,
									r.Date__c,
									r.Batch_Number__c,
									//r.RCR_Contract__r.RCR_Program__r.Name,
									//r.RCR_Contract__r.RCR_Program__r.Account__r.Name,
									(SELECT	i.ID,
											i.Name,
											i.Start_Date__c,
											i.End_Date__c,
											i.Invoice_Number__c,
											i.Status__c,
											i.Total__c
									FROM	RCR_Invoices__r i)
							FROM	RCR_Invoice_Batch__c	r
							WHERE 	ID = :invoiceID];
        }
    }

	public RCR_Invoice_Batch__c InvoiceBatch
    {
    	get
    	{
    		if(InvoiceBatch == null)
    		{
    			InvoiceBatch = new RCR_Invoice_Batch__c();
    		}
    		return InvoiceBatch;
    	}
    	set;
	}
    public Date EndDate
    {
        get;
        set;
    }

    public Date StartDate
    {
        get;
        set;
    }

    public String OrgName
    {
        get;
        set;
    }

    public PageReference reconcile()
    {
        RCRReconcileInvoice recInvoice = new RCRReconcileInvoice();
        for(RCR_Invoice__c invoice : InvoiceBatch.RCR_Invoices__r)
        {
            recInvoice.Invoice = invoice;
            recInvoice.reconcile();
        }

        return null;
    }*/
}