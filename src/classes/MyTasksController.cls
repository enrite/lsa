public with sharing class MyTasksController {
    private List<ProcessInstance> processInstances;
    public final Map<String,Id> recordTypes;

    private final String TEAM = 'Team - ';
    private final String OTHER = TEAM + 'Other';
    private final String PSC = 'PSC';

    public MyTasksController() {
        recordTypes = Utils.GetRecordTypeIdsByDeveloperName(Task.getSObjectType());
    }

    public Id TaskRecordType{
        get{
            if(TaskRecordType== null)
            {
                TaskRecordType = recordTypes.get('Query');
            }
            return TaskRecordType;
        }set;
    }

    public List<Task> AdminstrationPSOTasks{
        get{
            if(AdminstrationPSOTasks==null)
            {
                AdminstrationPSOTasks = [SELECT Id,
                                                Subject,
                                                CreatedDate,
                                                Status,
                                                ActivityDate,
                                                Response__c,
                                                Participant_Request_Details__c,
                                                WhatId,
                                                WhoId,
                                                Description,
                                                Priority,
                                                Category__c,
                                                Assigned_to_Group__c,
                                                What.Name,
                                                OwnerID
                                        FROM     Task
                                        WHERE    Assigned_to_Group__c IN ('PSO', :PSC) AND
                                                OwnerID = :Default_Task_User__c.getInstance().User_Id__c AND
                                                IsClosed = false AND
                                                IsRecurrence = false AND
                                                (ActivityDate < :Date.Today().addMonths(3) OR ActivityDate = null)
                                        ORDER BY CreatedDate];
            }
            return AdminstrationPSOTasks;
        }set;
    }

    public List<String> AdminstrationPSOTasksByManagerKeySet
    {
        get
        {
            if(AdminstrationPSOTasksByManagerKeySet == null)
            {
                Set<String> keys = new Set<String>(AdminstrationPSOTasksByManager.keySet());

                // make sure Other and PSC are last in list
                Boolean containsOther = keys.remove(OTHER);
                Boolean containsPSC = keys.remove(PSC);
                AdminstrationPSOTasksByManagerKeySet = new List<String>(keys);
                AdminstrationPSOTasksByManagerKeySet.sort();
                if(containsOther)
                {
                    AdminstrationPSOTasksByManagerKeySet.add(OTHER);
                }
                if(containsPSC)
                {
                    AdminstrationPSOTasksByManagerKeySet.add(PSC);
                }
            }
            return AdminstrationPSOTasksByManagerKeySet;
        }
        private set;
    }

    public Map<String, List<TaskViewModel>> AdminstrationPSOTasksByManager
    {
        get
        {
            if(AdminstrationPSOTasksByManager == null)
            {
                AdminstrationPSOTasksByManager = new Map<String, List<TaskViewModel>>();
                Set<ID> relatedIds = new Set<ID>();
                Map<String, List<Task>> tasksByManager = new Map<String, List<Task>>();
                Map<ID, Task> tasksById = new Map<ID, Task>(AdminstrationPSOTasks);
                Set<ID> psoCordinatorTaskIds =  new Set<ID>();
                Set<ID> managerTaskIds =  new Set<ID>();
                for(Task t : tasksById.values())
                {
                    if(t.Assigned_to_Group__c == PSC)
                    {
                        psoCordinatorTaskIds.add(t.ID);
                    }
                    else if(t.WhatId != null)
                    {
                        relatedIds.add(t.WhatId);
                    }
                }
                if(!psoCordinatorTaskIds.isEmpty())
                {
                    List<Task> pscTasks = new List<Task>();
                    for(ID tskId : psoCordinatorTaskIds)
                    {
                        Task t = tasksById.get(tskId);
                        addTasksByManager(tasksByManager, new List<Task>{t}, tasksById, PSC);
                    }
                }
                for(Plan_Action__c pa : [SELECT ID,
                                                Participant_Plan__r.Client__r.Allocated_to__r.Name,
                                                Participant_Plan__r.Client__r.Allocated_to__r.Manager.Name,
                                                (SELECT ID
                                                FROM    Tasks
                                                WHERE   ID IN :tasksById.keySet()
                                                ORDER BY CreatedDate)
                                        FROM    Plan_Action__c
                                        WHERE   ID IN :relatedIds])
                {
                    String manager = pa.Participant_Plan__r.Client__r.Allocated_to__r.Manager.Name == null ? pa.Participant_Plan__r.Client__r.Allocated_to__r.Name :
                            pa.Participant_Plan__r.Client__r.Allocated_to__r.Manager.Name;
                    addTasksByManager(tasksByManager, pa.Tasks, tasksById, manager);
                }
                for(Plan_Approval__c pap : [SELECT ID,
                                                    Participant_Plan__r.Client__r.Allocated_to__r.Name,
                                                    Participant_Plan__r.Client__r.Allocated_to__r.Manager.Name,
                                                    (SELECT ID
                                                    FROM    Tasks
                                                    WHERE   ID IN :tasksById.keySet()
                                                    ORDER BY CreatedDate)
                                        FROM    Plan_Approval__c
                                        WHERE   ID IN :relatedIds])
                {
                    String manager = pap.Participant_Plan__r.Client__r.Allocated_to__r.Manager.Name == null ? pap.Participant_Plan__r.Client__r.Allocated_to__r.Name :
                            pap.Participant_Plan__r.Client__r.Allocated_to__r.Manager.Name;
                    addTasksByManager(tasksByManager, pap.Tasks, tasksById, manager);
                }
                for(RCR_Invoice__c inv : [SELECT ID,
                                                Service_Order__r.Client__r.Allocated_to__r.Name,
                                                Service_Order__r.Client__r.Allocated_to__r.Manager.Name,
                                                (SELECT ID
                                                FROM    Tasks
                                                WHERE   ID IN :tasksById.keySet()
                                                ORDER BY CreatedDate)
                                        FROM    RCR_Invoice__c
                                        WHERE   ID IN :relatedIds])
                {
                    String manager = inv.Service_Order__r.Client__r.Allocated_to__r.Manager.Name == null ? inv.Service_Order__r.Client__r.Allocated_to__r.Name :
                            inv.Service_Order__r.Client__r.Allocated_to__r.Manager.Name;
                    addTasksByManager(tasksByManager, inv.Tasks, tasksById, manager);
                }
                for(Referred_Client__c part : [SELECT ID,
                                                    Allocated_to__r.Name,
                                                    Allocated_to__r.Manager.Name,
                                                    (SELECT ID
                                                    FROM    Tasks
                                                    WHERE   ID IN :tasksById.keySet()
                                                    ORDER BY CreatedDate)
                                                FROM    Referred_Client__c
                                                WHERE   ID IN :relatedIds])
                {
                    String manager = part.Allocated_to__r.Manager.Name == null ? part.Allocated_to__r.Name :
                            part.Allocated_to__r.Manager.Name;
                    addTasksByManager(tasksByManager, part.Tasks, tasksById, manager);
                }
                for(Plan__c plan : [SELECT ID,
                                            Client__r.Allocated_to__r.Name,
                                            Client__r.Allocated_to__r.Manager.Name,
                                            (SELECT ID
                                            FROM    Tasks
                                            WHERE   ID IN :tasksById.keySet()
                                            ORDER BY CreatedDate)
                                    FROM    Plan__c
                                    WHERE   ID IN :relatedIds])
                {
                    String manager = plan.Client__r.Allocated_to__r.Manager.Name == null ? plan.Client__r.Allocated_to__r.Name :
                            plan.Client__r.Allocated_to__r.Manager.Name;
                    addTasksByManager(tasksByManager, plan.Tasks, tasksById, manager);
                }
                for(Case_Notes__c cn : [SELECT ID,
                                                Client__r.Allocated_to__r.Name,
                                                Client__r.Allocated_to__r.Manager.Name,
                                                (SELECT ID
                                                FROM    Tasks
                                                WHERE   ID IN :tasksById.keySet()
                                                ORDER BY CreatedDate)
                                        FROM    Case_Notes__c
                                        WHERE   ID IN :relatedIds])
                {
                    String manager = cn.Client__r.Allocated_to__r.Manager.Name == null ? cn.Client__r.Allocated_to__r.Name :
                            cn.Client__r.Allocated_to__r.Manager.Name;
                    addTasksByManager(tasksByManager, cn.Tasks, tasksById, manager);
                }
                for(RCR_Contract__c so : [SELECT    ID,
                                                    Client__r.Allocated_to__r.Name,
                                                    Client__r.Allocated_to__r.Manager.Name,
                                                    (SELECT ID
                                                    FROM    Tasks
                                                    WHERE   ID IN :tasksById.keySet()
                                                    ORDER BY CreatedDate)
                                            FROM    RCR_Contract__c
                                            WHERE   ID IN :relatedIds])
                {
                    String manager = so.Client__r.Allocated_to__r.Manager.Name == null ? so.Client__r.Allocated_to__r.Name :
                            so.Client__r.Allocated_to__r.Manager.Name;
                    addTasksByManager(tasksByManager, so.Tasks, tasksById, manager);
                }

                if(!tasksById.isEmpty())
                {
                    tasksByManager.put(OTHER, tasksById.values());
                }
                for(String key : tasksByManager.keyset())
                {
                    List<TaskViewModel> tvmList = new List<TaskViewModel>();
                    for(Task t : tasksByManager.get(key))
                    {
                        tvmList.add(new TaskViewModel(t));
                    }
                    tvmList.sort();
                    AdminstrationPSOTasksByManager.put(key, tvmList);
                }
            }
            return AdminstrationPSOTasksByManager;
        }
        private set;
    }

    private void addTasksByManager(Map<String, List<Task>> tasksByManager, List<Task> tasks, Map<Id, Task> tasksById, String managerName)
    {
        if(managerName == null)
        {
            return;
        }
        String key = managerName == PSC ? PSC : (TEAM + managerName);
        if(!tasksByManager.containsKey(key))
        {
            tasksByManager.put(key, new List<Task>());
        }
        for(Task t : tasks)
        {
            tasksByManager.get(key).add(tasksById.get(t.ID));
            tasksById.remove(t.ID);
        }
    }

    public List<TaskViewModel> AdminstrationFinanceTasks{
        get{
            if(AdminstrationFinanceTasks==null)
            {
                List<TaskViewModel> tasks = new List<TaskViewModel>();

                for(Task t : [SELECT Id, Subject, CreatedDate, Status, ActivityDate, Response__C, Participant_Request_Details__c, WhatId, WhoId, Description,
                                    What.Name
                           FROM     Task 
                           WHERE    Assigned_to_Group__c = 'Finance' AND
                                    OwnerID = :Default_Task_User__c.getInstance().User_Id__c AND 
                                    IsClosed = false AND
                                    IsRecurrence = false AND
                                    (ActivityDate < :Date.Today().addMonths(3) OR ActivityDate = null)])
                {
                    tasks.add(new TaskViewModel(t));
                }

                tasks.sort();
                AdminstrationFinanceTasks = tasks;
            }
            return AdminstrationFinanceTasks;
        }set;
    } 

    public List<TaskViewModel> MyTasks{
        get{
            if(MyTasks==null)
            {
                List<TaskViewModel> tasks = new List<TaskViewModel>();

                for(Task t : [SELECT Id,
                                    Subject,
                                    CreatedDate,
                                    Status,
                                    ActivityDate,
                                    Response__c,
                                    Participant_Request_Details__c,
                                    WhatId,
                                    WhoId,
                                    Description,
                                    What.Name
                           FROM     Task
                           where    OwnerId = :UserInfo.getUserId() AND
                                    IsClosed = false AND
                                    IsRecurrence = false AND
                                    (ActivityDate < :Date.Today().addMonths(3) OR ActivityDate = null) AND
                                    WhatId NOT IN (SELECT ID
                                                FROM    RCR_Invoice_Item__c)])
                {
                    tasks.add(new TaskViewModel(t));
                }

                tasks.sort();
                MyTasks = tasks;
            }
            return MyTasks;
        }set;
    } 



    public List<ProcessInstanceWrapper> Batches{
        get{
            if(Batches==null){
                Batches = new List<ProcessInstanceWrapper>();
                Map<Id, ProcessInstance> targetProcessInstances  = getTargetProcessIntances(RCR_Invoice_Batch__c.SObjectType.getDescribe().getKeyPrefix());
                List<RCR_Invoice_Batch__c> rcrInvoiceBatches = [Select Id, Name, CreatedById, Batch_Total_inc_GST__c From RCR_Invoice_Batch__c where id in : targetProcessInstances.keySet()];
                for(RCR_Invoice_Batch__c batch : rcrInvoiceBatches)
                {
                    Batches.add(new ProcessInstanceWrapper(batch, targetProcessInstances.get(batch.id)));
                }  
                Batches.sort();  
            }
            return Batches;
        }set;
    }

    public List<ProcessInstanceWrapper> ServiceOrders{
        get{
            if(ServiceOrders==null){
                ServiceOrders = new List<ProcessInstanceWrapper>();
                Map<Id, ProcessInstance> targetProcessInstances = getTargetProcessIntances(RCR_Contract__c.SObjectType.getDescribe().getKeyPrefix());
                List<RCR_Contract__c> rcrServiceOrders = [Select Id, Name, Client__c, Client__r.Name, CreatedById, Amendment_Details__c, Total_Service_Order_Cost__c, Orig_Cost__c, Account__c From RCR_Contract__c where id in : targetProcessInstances.keySet()];    
                for(RCR_Contract__c serviceOrder : rcrServiceOrders)
                {
                    ServiceOrders.add(new ProcessInstanceWrapper(serviceOrder, targetProcessInstances.get(serviceOrder.id)));
                }  
                ServiceOrders.sort();
            }
            return ServiceOrders;
        }set;
    }

    public List<ProcessInstanceWrapper> AdhocPayments{
        get{
            if(AdhocPayments==null){
                AdhocPayments = new List<ProcessInstanceWrapper>();
                Map<Id, ProcessInstance> targetProcessInstances = getTargetProcessIntances(Adhoc_Payment__c.SObjectType.getDescribe().getKeyPrefix());
                List<Adhoc_Payment__c> adhpays = [SELECT Id, 
                                                        Account__c, 
                                                        NonGST_Expense_Description__c,
                                                        NonGST_Amount__c, 
                                                        NonGST_GL_Code__c, 
                                                        Participant__c,
                                                        Expense_Description__c,
                                                        GL_Code__c, 
                                                        Amount__c,
                                                        GSTAmount__c,
                                                        Total_Amount__c,
                                                        Participant__r.Name, 
                                                        CreatedById, 
                                                        Name
                                                    FROM Adhoc_Payment__c
                                                    WHERE ID IN :targetProcessInstances.keySet()];
                for(Adhoc_Payment__c adhocPayment : adhpays)
                {
                  AdhocPayments.add(new ProcessInstanceWrapper(adhocPayment, targetProcessInstances.get(adhocPayment.id)));
                }  
                AdhocPayments.sort();
            }
            return AdhocPayments;
        }set;
    }

    public List<ProcessInstanceWrapper> Expenses{
        get{
            if(Expenses==null){
                Expenses = new List<ProcessInstanceWrapper>();
                Map<Id, ProcessInstance> targetProcessInstances  = getTargetProcessIntances(External_Expense__c.SObjectType.getDescribe().getKeyPrefix());
                List<External_Expense__c>  exps = [SELECT   Id, 
                                                            Name, 
                                                            CreatedById, 
                                                            Month__c, 
                                                            Year__c, 
                                                            Statement_Total_Amount__c 
                                                    FROM    External_Expense__c 
                                                    WHERE   ID IN :targetProcessInstances.keySet()];    
                for(External_Expense__c expense : exps)
                {
                    Expenses.add(new ProcessInstanceWrapper(expense, targetProcessInstances.get(expense.id)));
                }  
                Expenses.sort();
            }
            return Expenses;
        }set;
    }

    public List<Form_Detail__c> AssessmentTasks{
        get{
            if(AssessmentTasks == null)
            {
                List<Id> recordTypeIds = UserUtilities.GetAvailableRecordTypeIdsForSObject(Form_Detail__c.SObjectType);
                AssessmentTasks = [SELECT Id, 
                                        Name, 
                                        CreatedDate,
                                        Form_Application_Assessment__r.Client__c,
                                        Form_Application_Assessment__r.Client__r.Name,
                                        Form_Application_Assessment__r.Client__r.Allocated_to__c,
                                        OwnerId,
                                        RecordType.Name
                                    FROM Form_Detail__c
                                    where   (
                                                (RecordType.DeveloperName like '%Application_Assessment%'
                                                 and (not Status__c  like '%Accepted%')) 
                                                OR 
                                                (RecordType.DeveloperName = 'Lifetime_Assessment_Start'
                                                 and Service_Planner_Complete__c = false) 
                                                OR
                                                (RecordType.DeveloperName = 'Lifetime_Assessment_CE_Approval'
                                                 and Status__c = null) 
                                            )
                                            and RecordTypeId in : recordTypeIds
                                    order by CreatedDate];
            }
            return AssessmentTasks;
        }set;
    }


    public List<Plan_Approval__c> PlanApprovalTasks{
        get{
            if(PlanApprovalTasks == null)
            {
                List<Id> recordTypeIds = UserUtilities.GetAvailableRecordTypeIdsForSObject(Plan_Approval__c.SObjectType);
                PlanApprovalTasks = [SELECT Id, 
                                            Name,
                                            CreatedDate,
                                            Participant_Plan__r.RecordType.Name,
                                            Participant_Plan__r.Plan_Start_Date__c,
                                            Participant_Plan__r.Plan_End_Date__c,
                                            Participant_Plan__r.Estimated_Plan_Costs__c,
                                            Plan_Approval_Date__c, 
                                            Participant_Plan__r.Plan_Status__c,
                                            Participant_Plan__r.Client_Name__c, 
                                            Participant_Plan__r.Client__r.Allocated_to__c, 
                                            OwnerId,
                                            RecordType.Name,
                                            RecordType.DeveloperName,
                                            Assigned_To__c,
                                            Assigned_To__r.Name
                                    FROM    Plan_Approval__c
                                    WHERE   (NOT Chief_Executive_Decision__c like '%Approved%')
                                    AND     RecordType.DeveloperName != 'Service_Planner_Approval'
                                    AND     RecordType.DeveloperName != 'Lead_Service_Planner_Approval'
                                    AND     RecordType.DeveloperName != 'Manager_Service_Planning_Approval'
                                    AND     RecordTypeId in : recordTypeIds
                                    order by CreatedDate];
            }
            return PlanApprovalTasks;
        }set;
    }

    public List<SdsWrapper> SelfDirectedSupportApprovals{
        get{
            if(SelfDirectedSupportApprovals == null){
                Id userId = UserInfo.getUserId();
                SelfDirectedSupportApprovals = new List<SdsWrapper>();
                Set<Id> invoiceIds = new Set<Id>();
                for (RCR_Invoice_Item__c item : [
                        SELECT ID, Client__r.Name, RCR_Invoice__r.Id, RCR_Invoice__r.Start_Date__c, RCR_Invoice__r.End_Date__c, RCR_Invoice__r.Invoice_Number__c, RCR_Invoice__r.Total_ex_GST__c, RCR_Invoice__r.Client_Name__c,RCR_Invoice__r.Name,RCR_Service_Order__r.Client_Name__c,RCR_Service_Order__r.Client__c
                        FROM RCR_Invoice_Item__c
                        WHERE RCR_Invoice__r.Status__c='Reconciled - No Errors'
                        AND RCR_Invoice__r.RecordType.DeveloperName ='Created'
                        AND RCR_Invoice__r.SDS__c = true
                        AND (RCR_Service_Order__r.Service_Planner__c = :userId OR
                             RCR_Contract_Scheduled_Service__r.RCR_Contract__r.Client__r.Allocated_To__c = :userId)
                        ORDER BY RCR_Service_Order__r.Client_Name__c ASC, RCR_Invoice__r.Invoice_Number__c DESC]) {

                    RCR_Invoice__c invoice = item.RCR_Invoice__r;
                    if (!invoiceIds.contains(invoice.Id)) {
                        SelfDirectedSupportApprovals.add(new SdsWrapper(invoice, item.RCR_Service_Order__r.Client__c, item.RCR_Service_Order__r.Client_Name__c));
                        invoiceIds.add(invoice.Id);
                    }

                }
            }
            return SelfDirectedSupportApprovals;
        }
        set;
    }

    public List<ParticipantWrapper> MyParticipants
    {
        get
        {
            if(MyParticipants == null)
            {
                MyParticipants = new List<ParticipantWrapper>();
                List<Referred_Client__c> participants = [SELECT ID,
                                                            Full_Name__c,
                                                            Current_Status__c,
                                                            Current_Status_Date__c,
                                                            (SELECT ID,
                                                                    Plan_End_Date__c,
                                                                    RecordType.Name
                                                            FROM    Plans__r
                                                            ORDER BY Plan_Start_Date__c DESC
                                                            LIMIT 1),
                                                            (SELECT ID
                                                            FROM RCR_Activity_Statement_Items__r)
                                                    FROM    Referred_Client__c
                                                    WHERE   Allocated_to__c = :UserInfo.getUserId() AND
                                                            Current_Participant__c = true
                                                    ORDER BY Family_Name__c];

                Map<ID, Set<ID>> taskIdsByParticipantId = new Map<ID, Set<ID>>();
                Map<ID, ID> participantIdByItemId = new Map<ID, ID>();
                for(Referred_Client__c part : participants)
                {
                    taskIdsByParticipantId.put(part.ID, new Set<ID>());
                    for(RCR_Invoice_Item__c item : part.RCR_Activity_Statement_Items__r)
                    {
                        participantIdByItemId.put(item.Id, part.ID);
                    }
                }

                for(Task t : [SELECT ID,
                                    WhatId,
                                    RCR_Service_Order__r.Client__c,
                                    RCR_Contract_Scheduled_Service__r.RCR_Contract__r.Client__c,
                                    Plan_Action__r.Client__c
                            FROM    Task
                            WHERE   IsClosed = false AND
                                    (WhatId IN :participants OR
                                    RCR_Service_Order__r.Client__c IN :participants OR
                                    RCR_Contract_Scheduled_Service__r.RCR_Contract__r.Client__c IN :participants OR
                                    Plan_Action__r.Client__c IN :participants)])
                {
                    if(taskIdsByParticipantId.containsKey(t.WhatId))
                    {
                        taskIdsByParticipantId.get(t.WhatId).add(t.ID);
                    }
                    if(taskIdsByParticipantId.containsKey(t.RCR_Service_Order__r.Client__c))
                    {
                        taskIdsByParticipantId.get(t.RCR_Service_Order__r.Client__c).add(t.ID);
                    }
                    if(taskIdsByParticipantId.containsKey(t.RCR_Contract_Scheduled_Service__r.RCR_Contract__r.Client__c))
                    {
                        taskIdsByParticipantId.get(t.RCR_Contract_Scheduled_Service__r.RCR_Contract__r.Client__c).add(t.ID);
                    }
                    if(taskIdsByParticipantId.containsKey(t.Plan_Action__r.Client__c))
                    {
                        taskIdsByParticipantId.get(t.Plan_Action__r.Client__c).add(t.ID);
                    }
                }

                for(Task t : [SELECT    ID,
                                        WhatId,
                                        OwnerID
                                FROM    Task
                                WHERE   IsClosed = false AND
                                        WhatId IN (SELECT ID
                                                    FROM    RCR_Invoice_Item__c
                                                    WHERE   Client__c IN :participants)])
                {
                    ID participantId = participantIdByItemId.get(t.WhatID);
                    if(participantId != null)
                    {
                        taskIdsByParticipantId.get(participantId).add(t.ID);
                    }
                }

                for(Referred_Client__c part : participants)
                {
                    MyParticipants.add(new ParticipantWrapper(part, taskIdsByParticipantId.get(part.ID).size()));
                }
            }
            return MyParticipants;
        }
        private set;
    }

    public class TaskViewModel implements Comparable
    {
        public Task Task {get;set;}
//        public string ObjectType {get;set;}

        public TaskViewModel(Task task)
        {
            this.Task = task;
        }

        public Integer compareTo(Object compareTo) {
            TaskViewModel compareToTask = (TaskViewModel)compareTo;

            Integer returnValue = 0;
            if (Task.CreatedDate > compareToTask.Task.CreatedDate) {
                returnValue = 1;
            } else if (Task.CreatedDate < compareToTask.Task.CreatedDate) {
                returnValue = -1;
            }
            
            return returnValue;        
        }
    } 

    
    private List<ProcessInstance> getProcessInstances()
    {
        if(processInstances == null)
        {
            ID userID = UserInfo.getUserID();
            processInstances = new List<ProcessInstance>();
            for ( ProcessInstance pi: [SELECT Id, Status, TargetObjectId, CreatedDate, (SELECT Id, ActorId FROM Workitems)
                       FROM ProcessInstance where Status IN ('Pending','Hold','Reassigned','NoResponse')] ) {
                    for (ProcessInstanceWorkitem piwi : pi.Workitems) {
                        if(piwi.ActorID == userID ||
                                UserUtilities.isUserInGroupByID(piwi.ActorID, userID) ||
                                UserUtilities.isUserInRoleByID(piwi.ActorID, userID, true)) 
                        {
                            processInstances.add(pi);
                        }   
                    }      
                }
        }
        return processInstances;
    }

    private Map<Id, ProcessInstance> getTargetProcessIntances(String keyPrefix)
    {
        Map<Id, ProcessInstance> targetProcessInstances = new Map<Id, ProcessInstance>();
        for ( ProcessInstance pi: getProcessInstances()) {
            if(!string.valueOf(pi.TargetObjectId).startsWith(keyPrefix))continue;
            targetProcessInstances.put(pi.TargetObjectId, pi);      
        }
        return targetProcessInstances;
    }

    /*
     * Name                 : ProcessInstanceWrapper
     * Object               : ProcessIntance
     * Requirement          :
     * Refer classes        :
     * Author               :
     * Create Date          :
     * Modify History       :
     *
     */
    public class ProcessInstanceWrapper implements Comparable {
        public SObject TargetObject{get;set;}
        public ProcessInstance Process{get;set;}

        public String ApprovalURL
        {
            get
            {
                if(ApprovalURL == null)
                {
                    List<ProcessInstanceWorkitem> workItemList = [SELECT Id FROM ProcessInstanceWorkitem WHERE processInstance.TargetObjectId = :TargetObject.Id];

                    if(workItemList.size() > 0)
                    {
                        ApprovalURL ='https://'+ System.URL.getSalesforceBaseUrl().getHost() + '/p/process/ProcessInstanceWorkitemWizardStageManager?id=' + workItemList[0].id;
                    }
                }
                return ApprovalURL;
            }
            set;
        }
        ProcessInstanceWrapper(SObject so, ProcessInstance pi)
        {
            this.TargetObject = so;
            this.Process = pi;
        }

        public Integer compareTo(Object compareTo) {
            ProcessInstanceWrapper compareToPi = (ProcessInstanceWrapper)compareTo;
            
            Integer returnValue = 0;
            if (Process.CreatedDate > compareToPi.Process.CreatedDate) {
                returnValue = 1;
            } else if (Process.CreatedDate < compareToPi.Process.CreatedDate) {
                returnValue = -1;
            }
            
            return returnValue;       
        }
    }

    public class SdsWrapper {
        public Id clientId {get;set;}
        public String clientName {get;set;}
        public RCR_Invoice__c invoice {get;set;}

        public SdsWrapper(RCR_Invoice__c inv, Id cId, String cname) {
            this.invoice = inv;
            this.clientId = cId;
            this.clientName = cname;
        }
    }

    public class ParticipantWrapper
    {
        public Referred_Client__c Participant {get;set;}
        public Plan__c LatestPlan {get;set;}
        public Integer OpenTasks{get;set;}

        public ParticipantWrapper(Referred_Client__c part, Integer openTasks)
        {
            this.Participant = part;
            this.OpenTasks = openTasks;
            LatestPlan = new Plan__c();
            for(Plan__c p : part.Plans__r)
            {
                LatestPlan = p;
            }
        }
    }
}