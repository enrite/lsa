public class GoogleReCaptcha
{
    private final String SECRET = Recaptcha__c.getInstance().Secret_Key__c;

    public Boolean verify(String captchaResponse)
    {
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://www.google.com/recaptcha/api/siteverify');
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        String body = 'secret=' + SECRET + '&response=' + captchaResponse;
        request.setHeader('Content-Length', String.valueOf(body.length()));
        request.setBody(body);
        Http http = new Http();
        GooglereCaptchaJSON result;
        if(Test.isRunningTest())
        {
            result = new GooglereCaptchaJSON();
            result.success = captchaResponse == 'Pass';
        }
        else
        {
            HttpResponse response = http.send(request);
            result = GooglereCaptchaJSON.parse(response.getBody());
        }
        return result.success;
    }


}