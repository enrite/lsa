public without sharing class ParticipantPortalNoSharing
{
    public static List<Adhoc_Payment__c> getParticipantPayments(User u)
    {
        return [SELECT //ID,
                        Name,
                        Date_of_Transaction__c,
                        Account_Name__c,
                        Expense_Description__c,
                        NonGST_Expense_Description__c,
                        Total_Amount__c
                FROM    Adhoc_Payment__c
                WHERE   Approved__c = true AND
                        Participant__c IN (SELECT ID
                                            FROM    Referred_Client__c
                                            WHERE   Participant_Portal_Contact__c = :u.ContactId)
                ORDER BY Date_of_Transaction__c DESC];
    }
}