@isTest
public class TestGenerateLetterController
{                    
    static testMethod void myTestMethod()
    {
        TestLoadFormData data = new TestLoadFormData();
                             
        Test.setCurrentPage(Page.GenerateLetter);                             
        ApexPages.CurrentPage().getParameters().put('pg', 'AcceptedLifetimePDF');
        ApexPages.CurrentPage().getParameters().put('id', data.Client.Id);
        ApexPages.CurrentPage().getParameters().put('pages', '1');
        ApexPages.CurrentPage().getParameters().put('parent', data.Client.Id);
        ApexPages.CurrentPage().getParameters().put('file', 'test');
        ApexPages.CurrentPage().getParameters().put('desc', 'test');
        ApexPages.CurrentPage().getParameters().put('email', 'test@test.com');
        ApexPages.CurrentPage().getParameters().put('subject', 'test');
        ApexPages.CurrentPage().getParameters().put('bdy', 'test');        
                                           
        GenerateLetterController ctr = new GenerateLetterController();
        ctr.GenerateLetter();
    }        
}