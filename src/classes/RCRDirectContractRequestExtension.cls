public with sharing class RCRDirectContractRequestExtension extends A2HCPageBase
{
    public RCRDirectContractRequestExtension(ApexPages.StandardController controller)
    {
        StandardController = controller;

        initialise();
    }

    @TestVisible private ApexPages.StandardController StandardController;
    @TestVisible private final String NEW_REQUEST = APSRecordTypes.RCRServiceOrder_NewRequest;
    @TestVisible private final String PARAMETER_NEW_AMENDMENT = 'newAmendment';
    @TestVisible private final String PARAMETER_APPROVED = 'planActionApproved';
    @TestVisible private transient List<SelectOption> directorateOptionList;
    @TestVisible private transient List<SelectOption> regionOptionList;
    @TestVisible private transient List<SelectOption> officeOptionList;
    @TestVisible private transient List<SelectOption> officeTeamOptionList;

    public RCR_Contract__c NewRequest
    {
        get;
        set;
    }

    public string SelectedDirectorate
    {
        get;
        set;
    }

    public string SelectedRegion
    {
        get;
        set;
    }

    public string SelectedOffice
    {
        get;
        set;
    }

    public string SelectedOfficeTeam
    {
        get;
        set;
    }

    public Boolean IsAmendment
    {
        get
        {
            return getParameter(PARAMETER_NEW_AMENDMENT) == '1' || NewRequest.RecordType.DeveloperName == 'Amendment';
        }
    }

    public List<SelectOption> Directorates
    {
        get
        {
            if(directorateOptionList == null)
            {
                directorateOptionList = getSelectListFromObjectList([SELECT ID,
                                                                    Name
                                                            FROM    RCR_Directorate__c
                                                            WHERE   Active__c = true
                                                            ORDER BY Name], true);
            }
            return directorateOptionList;
        }
    }

    public List<SelectOption> Regions
    {
        get
        {
            if(regionOptionList == null)
            {
                regionOptionList = getSelectListFromObjectList([SELECT   ID,
                                                                Name
                                                        FROM    RCR_Region__c
                                                        WHERE   Active__c = true AND
                                                                RCR_Directorate__c = :SelectedDirectorate
                                                        ORDER BY Name], true);
            }
            return regionOptionList;
        }
    }

    public List<SelectOption> Offices
    {
        get
        {
            if(officeOptionList == null)
            {
                officeOptionList = getSelectListFromObjectList([SELECT   ID,
                                                                Name
                                                        FROM    RCR_Office__c
                                                        WHERE   Active__c = true AND
                                                                RCR_Region__c = :SelectedRegion
                                                        ORDER BY Name], true);
            }
            return officeOptionList;
        }
    }

    public List<SelectOption> OfficeTeams
    {
        get
        {
            if(officeTeamOptionList == null)
            {
                officeTeamOptionList = getSelectListFromObjectList([SELECT  ID,
                                                                    Name
                                                            FROM    RCR_Office_Team__c
                                                            WHERE   Active__c = true
                                                            ORDER BY Name], true);
            }
            return officeTeamOptionList;
        }
    }

    public RCR_Contract__c getServiceOrder(String serviceOrderID)
    {
        try
        {
            return RCRContractExtension.getContract(serviceOrderID);
        }
        catch(Exception ex)
        {
            throw new A2HCException('Record could not be found.');
        }
    }

    public Boolean ValidAmendment
    {
        get 
        {
            if(ValidAmendment == null)
            {
                ValidAmendment = true;
            }
            return ValidAmendment;
        }
        private set;
    }

    private void initialise()
    {
        String serviceOrderId = getParameter('id');
        Id clientID = getParameter('clientID');
        Id planActionID = getParameter('planActionID');
        boolean isApproved = getParameter(PARAMETER_APPROVED) == '1';
        boolean isAmendment = getParameter(PARAMETER_NEW_AMENDMENT) == '1';

        if(serviceOrderId != null)
        {
            NewRequest = getServiceOrder(serviceOrderId);
            SelectedDirectorate = NewRequest.RCR_Office_Team__r.RCR_Office__r.RCR_Region__r.RCR_Directorate__c;
            SelectedRegion = NewRequest.RCR_Office_Team__r.RCR_Office__r.RCR_Region__c;
            SelectedOffice = NewRequest.RCR_Office_Team__r.RCR_Office__c;
            SelectedOfficeTeam = NewRequest.RCR_Office_Team__c;
        }
        else
        {
            NewRequest = new RCR_Contract__c();
            if(clientID != null)
            {
                NewRequest.Client__c = clientID;
            }
            if (planActionID != null)
            {
                NewRequest.Plan_Action__c = planActionID;  
                NewRequest.Plan_Action_Approved__c = isApproved;
                SelectPlanAction();                
            }
            
            NewRequest.Status__c = APS_PicklistValues.RCRContract_Status_New;
            NewRequest.Funding_Source__c = APS_PicklistValues.RCRContract_FundingSource_DirectContract;
            NewRequest.Type__c = APS_PicklistValues.RCRContract_Type_DirectContract;
            NewRequest.Level__c = '1';
            NewRequest.RecordTypeId = A2HCUtilities.getRecordType('RCR_Contract__c', NEW_REQUEST).ID;
        }

        if(isAmendment)
        {
            try
            {
                for(RCR_Contract__c c : [SELECT ID
                                        FROM    RCR_Contract__c
                                        WHERE   Name = :NewRequest.Name AND
                                                RecordType.Name = :APSRecordTypes.RCRServiceOrder_Amendment AND
                                                ID != :serviceOrderId AND
                                                (Status__c != :APS_PicklistValues.RCRContract_Status_Approved AND
                                                Status__c != :APS_PicklistValues.RCRContract_Status_Rejected)
                                        LIMIT 1])
                {
                    ValidAmendment = false;
                    throw new A2HCException('Another Amendment is in progress for this Service Order.');
                }

                for(RCR_Contract__c c : [SELECT ID
                                        FROM    RCR_Contract__c
                                        WHERE   RecordType.Name = :APSRecordTypes.RCRServiceOrder_ServiceOrder AND
                                                ID = :serviceOrderId AND
                                                ID IN (SELECT   RCR_Service_Order__c
                                                        FROM    RCR_MP_Service_Order_Payment__c
                                                        WHERE   RCR_Invoice_Batch__r.Approved_Date__c = null AND
                                                                RCR_Invoice_Batch__r.Lantern_Pay_Batch__c = false)
                                        LIMIT 1])
                {
                    ValidAmendment = false;
                    throw new A2HCException('This Service Order has an approved activity statement that is part of an unapproved payment batch.<br/>' +
                            'The statement must be removed from the batch OR the batch approved before this service order can be amended.');
                }
                NewRequest = NewRequest.clone(false, true);

                NewRequest.Source_Record__c = serviceOrderId;
                NewRequest.Status__c = APS_PicklistValues.RCRContract_Status_New;
                NewRequest.Schedule__c = null;
                NewRequest.Request_Date__c = null;
                NewRequest.Alloc_Change__c = false;
                NewRequest.Risk_Univ_Prec__c = true;
                NewRequest.Allocation_Total_Approved__c = 0.0;
                NewRequest.Allocation_Total_Not_Approved__c = 0.0;
                NewRequest.Orig_Cost__c = NewRequest.Total_Service_Order_Cost__c;
                NewRequest.New_Request_Type__c = null;
                NewRequest.Panel_Date__c = null;
                NewRequest.Recommendation__c = null;
                NewRequest.Recommendation_Reason__c = null;
                NewRequest.Brokerage_Officer__c = null;
                NewRequest.Approval_Process__c = null;                
                NewRequest.OwnerId = UserInfo.getUserId();
                NewRequest.RecordTypeID = A2HCUtilities.getRecordType('RCR_Contract__c', APSRecordTypes.RCRServiceOrder_Amendment).ID;
                
                if (NewRequest.Plan_Action__c != null)
                {
                    SelectPlanAction(true);
                }
            }
            catch(Exception ex)
            {
                A2HCException.formatException(ex);
                return;
            }
        }
    }
    
    /*
    public Boolean RehabTherapyTermsApply
    {
        get
        {
            if (RehabTherapyTermsApply == null)
            {
                RehabTherapyTermsApply = false;
                
                for (RCR_Contract_Service_Type__c cst : [SELECT Id                                                                    
                                                         FROM RCR_Contract_Service_Type__c 
                                                         WHERE RCR_Contract_Scheduled_Service__r.RCR_Contract__c = :NewRequest.Id
                                                         AND RCR_Program_Category_Service_Type__r.RCR_Program_Category__r.RCR_Program__r.Name = 'Rehabilitation'
                                                         LIMIT 1])
                {
                    RehabTherapyTermsApply = true;
                }                                                                  
            }
            return RehabTherapyTermsApply;
        }
        set;        
    }
    
    public Boolean HomeMaintenance
    {
        get
        {                            
            for (RCR_Contract_Service_Type__c cst : [SELECT Id                                                                    
                                                     FROM RCR_Contract_Service_Type__c 
                                                     WHERE RCR_Contract_Scheduled_Service__r.RCR_Contract__c = :NewRequest.Id
                                                     AND RCR_Program_Category_Service_Type__r.RCR_Program_Category__r.RCR_Program__r.Name = 'Home Maintenance'
                                                     LIMIT 1])
            {
                return true;
            }                                                                  
            
            return false;
        }
    }*/
    
    public void SelectPlanAction()
    {
        SelectPlanAction(false);
    }
    
    public void SelectPlanAction(Boolean amendmentDetailsOnly)
    {
        try
        {
            // if this is a new request then populate some fields from the plan action
            if (NewRequest.Id == null)
            {
                // get values from plan action if we have one
                for (Plan_Action__c pa : [SELECT Id,
                                                 Provider__c,
                                                 Pre_Defined_Services__c,
                                                 Item_Description__c,
                                                 Services_to_be_Provided__c,
                                                 Plan_Start_Date__c,
                                                 Plan_End_Date__c,
                                                 Amendment_Details__c,
                                                 RecordType.Name,
                                                 Approved__c,
                                                 Additional_Care_Instructions__c
                                          FROM Plan_Action__c
                                          WHERE Id = :NewRequest.Plan_Action__c])
                {
                    if (!amendmentDetailsOnly)
                    {
                        NewRequest.Account__c = pa.Provider__c;
                        NewRequest.Service_to_be_provided__c = (pa.Pre_Defined_Services__c != null ? pa.Pre_Defined_Services__c + '\n' : '');
                        NewRequest.Service_to_be_provided__c += (pa.Item_Description__c != null ? pa.Item_Description__c : '');
                        NewRequest.Service_to_be_provided__c += (pa.Services_to_be_Provided__c != null ? pa.Services_to_be_Provided__c : '');
                        NewRequest.Plan_Action_Approved__c = pa.Approved__c;
                        //NewRequest.Start_Date__c = pa.Plan_Start_Date__c;
                        //NewRequest.Original_End_Date__c = pa.Plan_End_Date__c;
                        NewRequest.Attendant_Care_Support__c = (pa.RecordType.Name == 'Attendant Care and Support' ? 'Yes' : 'No');                      
                    }
                    NewRequest.Amendment_Details__c = pa.Amendment_Details__c;
                    NewRequest.Additional_Care_Instructions__c = pa.Additional_Care_Instructions__c;

                    if(pa.RecordType.Name == 'MyWay Service')
                    {
                        NewRequest.Start_Date__c = pa.Plan_Start_Date__c;
                        NewRequest.Original_End_Date__c = pa.Plan_End_Date__c;
                        NewRequest.Service_Delivery_Location__c = 'Not Applicable';
                        NewRequest.New_Request_Type__c = 'New Service';
                    }
                }                                                      
            }        
        }
        catch(Exception ex)
        {
            A2HCException.formatException(ex);           
        }       
    }
    
    public String PlanStatus
    {
        get
        {
            for (Plan_Action__c pa : [SELECT Id,
                                             Participant_Plan__r.Plan_Status__c
                                      FROM Plan_Action__c
                                      WHERE Id = :NewRequest.Plan_Action__c])
            {
                return pa.Participant_Plan__r.Plan_Status__c;
            }
            
            return null;                                      
        }
    }

    public PageReference saveOverride()
    {
        Savepoint sp = Database.setSavepoint();
        try
        {
            boolean isAmendment = getParameter(PARAMETER_NEW_AMENDMENT) == '1';
            for(Plan_Action__c pa : [SELECT ID,
                                            Plan_Start_Date__c,
                                            Plan_End_Date__c
                                    FROM    Plan_Action__c
                                    WHERE   ID = :NewRequest.Plan_Action__c])
            {
                if(NewRequest.Start_Date__c < pa.Plan_Start_Date__c ||
                        NewRequest.Original_End_Date__c > pa.Plan_End_Date__c)
                {
                    system.debug(NewRequest.Start_Date__c +'|'+ pa.Plan_Start_Date__c);
                    system.debug(NewRequest.Original_End_Date__c +'|'+ pa.Plan_End_Date__c);
                    throw new A2HCException('Service order start and end dates must be within the linked plan\'s start and end dates');
                }
            }
            upsert NewRequest;

            NewRequest = getServiceOrder(NewRequest.Id);

            if(isAmendment)
            {
                List<sObject> newRelatedRecords = new List<sObject>();

                RCR_Contract__c origServiceOrder = getServiceOrder(NewRequest.Source_Record__c);
//                origServiceOrder.Plan_Action__c = null;
//                update origServiceOrder;
                                                
                Map<ID, ID> newIDsByOldIds = new Map<ID, ID>();
                List<RCR_Contract_Scheduled_Service__c> serviceOrderCss =  [SELECT     ID,
                                                                                    Name,
                                                                                    Start_Date__c,
                                                                                    End_Date__c,
                                                                                    Original_End_Date__c,
                                                                                    Flexibility__c,
                                                                                    Expenses__c,
                                                                                    Service_Types__c,
                                                                                    RCR_Service__c,
                                                                                    Service_Code__c,
                                                                                    Total_Quantity__c,
                                                                                    Total_Cost__c,
                                                                                    Exception_Count__c,
                                                                                    Use_Negotiated_Rates__c,
                                                                                    Negotiated_Rate_Standard__c,
                                                                                    Negotiated_Rate_Public_Holidays__c,
                                                                                    Public_holidays_not_allowed__c,
                                                                                    Funding_Commitment__c,
                                                                                    Source_Record__c,
                                                                                    Client__c,
                                                                                    Comment__c,
                                                                                    Internal_Comments__c,
                                                                                    GL_Category__c,
                                                                                    Standard_Service_Code__c,
                                                                                    Standard_Service_Code_Description__c,
                                                                                    RCR_Service__r.Name,
                                                                                    RCR_Service__r.Description__c,
                                                                                    RCR_Service__r.Day_of_Week__c,
                                                                                    RCR_Service__r.CodeAndDescription__c,
                                                                                    RCR_Contract__r.Name
                                                                            FROM   RCR_Contract_Scheduled_Service__c
                                                                            WHERE   RCR_Contract__c = :origServiceOrder.ID];
                for(RCR_Contract_Scheduled_Service__c css : serviceOrderCss)
                {
                    css.Source_Record__c = css.ID;
                }
//                // set this before cloning
//                for(RCR_Contract_Scheduled_Service__c css : origServiceOrder.RCR_Contract_Scheduled_Services__r)
//                {
//                    css.Source_Record__c = css.ID;
//                }
                List<RCR_Contract_Scheduled_Service__c> newCssList = A2HCUtilities.cloneRelatedList(NewRequest,
                                                                                                    serviceOrderCss,
                                                                                                    RCR_Contract_Scheduled_Service__c.RCR_Contract__c.getDescribe(),
                                                                                                    true);

                for(RCR_Contract_Scheduled_Service__c newCss : newCssList)
                {
                    newIDsByOldIds.put(newCss.Source_Record__c, newCss.ID);
                }

//                for(Integer i = 0; i < origServiceOrder.RCR_Contract_Scheduled_Services__r.size(); i++)
//                {
//                    newIDsByOldIds.put(origServiceOrder.RCR_Contract_Scheduled_Services__r[i].ID, newCssList[i].ID);
//                    newCssList[i].Source_Record__c = origServiceOrder.RCR_Contract_Scheduled_Services__r[i].ID;
//                }
                                

                /* Funding Details are now handled automatically by a trigger so we should be able to comment this out
                newRelatedRecords.addAll(A2HCUtilities.cloneRelatedList(NewRequest,
                                                                            origServiceOrder.RCR_Funding_Details__r,
                                                                            RCR_Funding_Detail__c.RCR_Service_Order__c.getDescribe(),
                                                                            false));
                */                                                                            
                newRelatedRecords.addAll(A2HCUtilities.cloneRelatedList(NewRequest,
                                                                            origServiceOrder.RCR_Service_Order_Rate_Calculations__r,
                                                                            RCR_Service_Order_Rate_Calculation__c.RCR_Service_Order__c.getDescribe(),
                                                                            false));
                newRelatedRecords.addAll(A2HCUtilities.cloneRelatedList(NewRequest,
                                                                            origServiceOrder.RCR_Service_Order_Break_Periods__r,
                                                                            RCR_Service_Order_Break_Period__c.RCR_Service_Order__c.getDescribe(),
                                                                            false));

                DescribeFieldResult scheduleFieldDescResult = Schedule_Period__c.Schedule__c.getDescribe();
                for(Schedule__c oldSchedule : [SELECT   Name ,
                                                        s.Start_Date__c,
                                                        s.End_Date__c,
                                                        s.Schedule_Recurrence_Pattern__c,
                                                        s.Number_of_weeks_in_period__c,
                                                        s.Quantity_Per_Period__c,
                                                        (SELECT Schedule__c,
                                                                Week_Number__c,
                                                                Week_Of_Month__c,
                                                                Monday__c,
                                                                Tuesday__c,
                                                                Wednesday__c,
                                                                Thursday__c,
                                                                Friday__c,
                                                                Saturday__c,
                                                                Sunday__c,
                                                                Total_Quantity__c
                                                         FROM   Schedule_Periods__r
                                                         ORDER BY Week_Number__c)
                                                FROM    Schedule__c s
                                                WHERE   Name IN :newIDsByOldIds.keySet()])
                {
                    Schedule__c newSchedule = oldSchedule.clone(false, true);
                    newSchedule.Name = newIDsByOldIds.get(oldSchedule.Name);
                    insert newSchedule;
                    newRelatedRecords.addAll(A2HCUtilities.cloneRelatedList(newSchedule,
                                                                            oldSchedule.Schedule_Periods__r,
                                                                            scheduleFieldDescResult,
                                                                            false));
                }
                for(RCR_Contract_Scheduled_Service__c css : [SELECT ID,
                                                                    (SELECT Id,
                                                                            Date__c,
                                                                            Public_Holiday_Quantity__c,
                                                                            Standard_Quantity__c,
                                                                            Description__c,
                                                                            RCR_Contract_Scheduled_Service__c
                                                                    FROM    RCR_Scheduled_Service_Exceptions__r),
                                                                    (SELECT Id,
                                                                            RCR_Program__c,
                                                                            RCR_Contract_Adhoc_Service__c,
                                                                            RCR_Contract_Scheduled_Service__c,
                                                                            RCR_Program_Category_Service_Type__c,
                                                                            Quantity__c
                                                                     FROM   RCR_Contract_Service_Types__r)
                                                                FROM    RCR_Contract_Scheduled_Service__c
                                                                WHERE   ID IN :newIDsByOldIds.keySet()])
                {
                     newRelatedRecords.addAll(A2HCUtilities.cloneRelatedList(new RCR_Contract_Scheduled_Service__c(ID = newIDsByOldIds.get(css.ID)),
                                                                            css.RCR_Scheduled_Service_Exceptions__r,
                                                                            RCR_Scheduled_Service_Exception__c.RCR_Contract_Scheduled_Service__c.getDescribe(),
                                                                            false));
                     newRelatedRecords.addAll(A2HCUtilities.cloneRelatedList(new RCR_Contract_Scheduled_Service__c(ID = newIDsByOldIds.get(css.ID)),
                                                                            css.RCR_Contract_Service_Types__r,
                                                                            RCR_Contract_Service_Type__c.RCR_Contract_Scheduled_Service__c.getDescribe(),
                                                                            false));
                }
                insert newRelatedRecords;
                
                // update to new css records so the triggers are run a final time
                update newCssList;
            }

            PageReference pg = new PageReference('/' + NewRequest.Id);
            return pg;
        }
        catch(Exception ex)
        {
            return A2HCException.formatExceptionAndRollback(sp, ex);
        }
    }

    public PageReference submitForApproval()
    {
        try
        {
            if(!isValid())
            {
                return null;
            }

            for(RCR_Contract__c so : [SELECT ID,
                                            Client__r.Allocated_to__c
                                    FROM    RCR_Contract__c
                                    WHERE   ID = :NewRequest.ID AND
                                            Owner.IsActive = false])
            {
                NewRequest.OwnerId = so.Client__r.Allocated_to__c;
                TriggerBypass.RCRServiceOrder = true;
                update NewRequest;
                TriggerBypass.RCRServiceOrder = false;
            }
            submitForApproval(NewRequest.Id, 'New Request - Direct Contract', false);
            return null;
        }
        catch(Exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }

    public void getRegions()
    {
        try
        {
            throwTestError();

            NewRequest.Director__c = null;
            for(RCR_Directorate__c dir : [SELECT    Director__c
                                            FROM    RCR_Directorate__c
                                            WHERE   ID = :SelectedDirectorate])
            {
                NewRequest.Director__c = dir.Director__c;
            }
            SelectedRegion = null;
            regionOptionList = null;
            getOffices();
            NewRequest.Regional_Manager__c = null;
        }
        catch(Exception ex)
        {
            A2HCException.formatException(ex);
        }
    }

    public void getOffices()
    {
        try
        {
            throwTestError();

            SelectedOffice = null;
            NewRequest.RCR_Office_Team__c = null;
            NewRequest.Team_Manager__c = null;
            officeOptionList = null;

            getTeams();
            for(RCR_Region__c reg : [SELECT Regional_Manager__c,
                                            Regional_Manager__r.Name
                                    FROM    RCR_Region__c
                                    WHERE   ID = :SelectedRegion])
            {
                NewRequest.Regional_Manager__c = reg.Regional_Manager__c;
            }
        }
        catch(Exception ex)
        {
            A2HCException.formatException(ex);
        }
    }

    public void getTeams()
    {
        try
        {
            throwTestError();

            NewRequest.RCR_Office_Team__c = null;
            officeTeamOptionList = null;
            NewRequest.Team_Manager__c = null;
            if(OfficeTeams.size() == 2)
            {
                // only 1 valid selection, so just set it
                NewRequest.RCR_Office_Team__c = OfficeTeams[1].getValue();
                getTeamManager();
            }
        }
        catch(Exception ex)
        {
            A2HCException.formatException(ex);
        }
    }

    public void getTeamManager()
    {
        try
        {
            throwTestError();

            NewRequest.Team_Manager__c = null;
            ID teamId = NewRequest.RCR_Office_Team__c;
            for(RCR_Office_Team__c ot : [SELECT  Team_Manager__c,
                                                 Team_Manager__r.Name
                                        FROM     RCR_Office_Team__c
                                        WHERE    ID = :teamId])
            {
                NewRequest.Team_Manager__c = ot.Team_Manager__c;
            }
        }
        catch(Exception ex)
        {
            A2HCException.formatException(ex);
        }
    }


    private Boolean isValid()
    {
        Boolean valid = isRequiredFieldValid(NewRequest.Client__c, 'Client/Program', true);
        valid = isRequiredFieldValid(NewRequest.Level__c, 'Level', valid);
        valid = isRequiredFieldValid(NewRequest.Start_Date__c, 'Start Date', valid);
        valid = isRequiredFieldValid(NewRequest.Original_End_Date__c, 'End Date', valid);
        //valid = isRequiredFieldValid(NewRequest.Service_Coordinator__c, 'Service Coordinator', valid);
        //valid = isRequiredFieldValid(NewRequest.Regional_Manager__c, 'Region', valid);
        //valid = isRequiredFieldValid(NewRequest.Team_Manager__c, 'Team', valid);
        valid = isRequiredFieldValid(NewRequest.Type__c, 'Type', valid);
        valid = isRequiredFieldValid(NewRequest.Funding_Source__c, 'Funding Source', valid);
        valid = isRequiredFieldValid(NewRequest.New_Request_Type__c, 'New Request Type', valid);
        //valid = isRequiredFieldValid(NewRequest.Service_to_be_provided__c, 'Service to be Provided', valid);
        valid = isRequiredFieldValid(NewRequest.Attendant_Care_Support__c, 'Attendant Care & Support', valid);
        valid = isRequiredFieldValid(NewRequest.Service_Delivery_Location__c, 'Service Delivery Location', valid);
        
        if(valid)
        {
            if(NewRequest.Total_Service_Order_Cost__c <= 0.0)
            {
                valid = false;
                A2HCException.formatException('You cannot submit a ' +  NewRequest.RecordType.Name + ' for Approval if it has no cost.');
            }            
        }

        // if the amendment has an approved plan action then the amounts must match
        if(valid)
        {
            if (NewRequest.RecordType.Name == APSRecordTypes.RCRServiceOrder_Amendment || NewRequest.RecordType.Name == APSRecordTypes.RCRServiceOrder_NewRequest)
            {
                Boolean found = false;
                for (Plan_Action__c pa : [SELECT Id,
                                                Estimated_Cost__c,
                                                Approved__c,
                                                Plan_Start_Date__c,
                                                Plan_End_Date__c,
                                                (SELECT Total_Service_Order_Cost__c,
                                                        Owner.IsActive,
                                                        RecordType.Name
                                                FROM    RCR_Service_Orders__r
                                                WHERE   RecordType.Name = :APSRecordTypes.RCRServiceOrder_ServiceOrder AND 
                                                        Total_Service_Order_Cost__c != null)
                                          FROM  Plan_Action__c
                                          WHERE Id = :NewRequest.Plan_Action__c])
                {
                    found = true;
                    if(!pa.Approved__c)
                    {
                        valid = false;
                        A2HCException.formatException( 'This ' + NewRequest.RecordType.Name + ' cannot be approved as is not linked to an approved Plan Action.');
                    }
                    if(!A2HCUtilities.isDateBetween(NewRequest.Start_Date__c, pa.Plan_Start_Date__c, pa.Plan_End_Date__c) ||
                            !A2HCUtilities.isDateBetween(NewRequest.End_Date__c, pa.Plan_Start_Date__c, pa.Plan_End_Date__c))
                    {
                        valid = false;
                        A2HCException.formatException( 'This ' + NewRequest.RecordType.Name + ' start and end dates are not within the approved Plan Action date range.');
                    }
                    if (NewRequest.Total_Service_Order_Cost__c > pa.Estimated_Cost__c || pa.Estimated_Cost__c == null)
                    {
                        valid = false;
                        A2HCException.formatException('The Total Service Order Cost for an ' + NewRequest.RecordType.Name + ' must not be greater than the Estimated Cost of the approved Plan Action.');
                    }
                    if(valid)
                    {
                        pa.Total_Service_Order_Cost__c = 0.0;
                        Decimal thisCost = NewRequest.Total_Service_Order_Cost__c;
                        if(NewRequest.RecordType.Name == APSRecordTypes.RCRServiceOrder_Amendment)
                        {
                            thisCost -= NewRequest.Orig_Cost__c;
                        }
                        for(RCR_Contract__c so : pa.RCR_Service_Orders__r)
                        {
                            pa.Total_Service_Order_Cost__c += so.Total_Service_Order_Cost__c;
                        }
                        if(pa.Total_Service_Order_Cost__c + thisCost > pa.Estimated_Cost__c)
                        {
                            valid = false;
                            String costs = String.valueOf(pa.Total_Service_Order_Cost__c + thisCost);
                            A2HCException.formatException('The Total Service Order Cost of this ' + NewRequest.RecordType.Name + ' ($' + String.valueOf(thisCost) + ')' + 
                                                            ' and other Service Orders ($' + pa.Total_Service_Order_Cost__c + ') for the approved Plan Action exceed its Estimated Cost ($' +  String.valueOf(pa.Estimated_Cost__c) + ').');
                        }
                        if(valid)
                        {
                            TriggerBypass.PlanAction = true;
                            update pa;
                        }
                    }
                }
                if(!found)
                {
                    A2HCException.formatException('This ' + NewRequest.RecordType.Name + ' cannot be approved as is not linked to an approved Plan Action.');
                    valid = false;
                }
            }                                      
        }
        return valid;
    }
}