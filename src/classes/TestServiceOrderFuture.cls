/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestServiceOrderFuture {

    @IsTest
    private static void test1()
    {
        TestLoadData.loadRCRData();
                
        Test.StartTest();
    
        RCR_Contract__c so = getContract();          
        
        RCR_Service_Order_Break_Period__c bp = new  RCR_Service_Order_Break_Period__c(RCR_Service_Order__c = so.ID);
        bp.Start_Date__c = so.Start_Date__c;
        bp.End_Date__c = so.End_Date__c;    
        insert bp;

        Set<ID> soIds = null;
        ServiceOrderFuture.submitForApprovalImmediate(soIds); 
        soIds = new Set<ID>{so.ID};
        ServiceOrderFuture.submitForApprovalImmediate(soIds);  
        
        ServiceOrderFuture.submitForApproval(soIds);
        
        ServiceOrderFuture.submitForApproval(new List<ID>(soIds));     
        
        ServiceOrderFuture.submitServiceOrdersForApproval(null, 'test', null, true);
        
        ServiceOrderFuture.submitServiceOrdersForApproval(new List<ID>(soIds), 'test', new List<ID>(soIds), true);
        
        ServiceOrderFuture.submitRolloversForApproval(new List<ID>(soIds));
        
        Test.StopTest();
    }
    
//    @IsTest
//    private static void test2()
//    {
//        TestLoadData.loadRCRData();
//
//        Test.StartTest();
//
//        for(RCR_Contract__c so : [SELECT     ID
//                                    FROM     RCR_Contract__c
//                                    LIMIT 1])
//        {
//            RCR_Contract__c serviceOrder = RCRServiceOrderRequestController.getServiceOrder(so.ID);
//            ServiceOrderFuture.checkServiceOrderAllocation(serviceOrder, false);
//        }
//
//        Test.StopTest();
//    }

    @IsTest
    private static void test3()
    {
        TestLoadData.loadRCRData();
                
        Test.StartTest();
    
        RCR_Contract__c so = getContract();        
        ServiceOrderFuture.createServiceOrderFromRequest(so.ID);
        
        Test.StopTest();
    }
    /*
    @IsTest
    private static void test4a()
    {
        TestLoadData.loadRCRData();
                
        Test.StartTest();
    
        RCR_Contract__c so = getContract();        
        
        RCR_Contract__c amendment = new RCR_Contract__c();        
        amendment.Name = so.Name;   
        amendment.RecordTypeID = A2HCUtilities.getRecordType('RCR_Contract__c', APSRecordTypes.RCRServiceOrder_Amendment).ID;   
        amendment.Source_Record__c = so.ID;  
        insert amendment;     
        
        A2HCUtilities.copyAllFields(so, amendment, new Set<String>{'Destination_Record__c', 'Source_Record__c', 'OwnerID', 'RecordTypeID'});
        amendment.Name = 'TestS2';
        amendment.RCR_CBMS_Contract__c = null;
        update amendment;
                        
        ServiceOrderFuture.copyServiceOrder(so, amendment);                
        
        Test.StopTest();
    }*/
    
    @IsTest
    private static void test4b()
    {
        TestLoadData.loadRCRData();
                
        Test.StartTest();
    
        RCR_Contract__c so = getContract();        
        
        RCR_Contract__c amendment = new RCR_Contract__c();        
        amendment.Name = so.Name;   
        amendment.RecordTypeID = A2HCUtilities.getRecordType('RCR_Contract__c', APSRecordTypes.RCRServiceOrder_Amendment).ID;   
        amendment.Source_Record__c = so.ID;  
        insert amendment;     
        
        A2HCUtilities.copyAllFields(so, amendment, new Set<String>{'Destination_Record__c', 'Source_Record__c', 'OwnerID', 'RecordTypeID'});
        amendment.Name = 'TestS2';
//        amendment.RCR_CBMS_Contract__c = null;
        update amendment;
                        
        ServiceOrderFuture.setDestination(so.ID, amendment.ID);
        ServiceOrderFuture.mergeAmendment(amendment.ID);        
        
        Test.StopTest();
    }           
    
    @IsTest
    private static void test6()
    {
        TestLoadData.loadRCRData();
                
        Test.StartTest();
    
        Financial_Year__c fy = [SELECT ID,
                                        Start_Date__c,
                                        End_Date__c
                                FROM    Financial_Year__c
                                LIMIT 1];
        ServiceOrderFuture.getApportionedAmountForFinancialYear(fy, fy.Start_Date__c.addDays(-100), fy.End_Date__c.addDays(100), 100.00);
        
        Test.StopTest();
    }
    
     @IsTest
    private static void test7()
    {       
        TestLoadData.loadRCRData();
                
        Test.StartTest();
    
        RCR_Contract__c so = getContract(); 
        
        // need to adjust dates to 2013 FY
        Date stDate = Date.newInstance(2013, 7, 1);
        Date endDate = Date.newInstance(2014, 6, 30);
        
        /*RCR_CBMS_Contract__c cbmsContract = new RCR_CBMS_Contract__c(ID = so.RCR_CBMS_Contract__c);
        cbmsContract.End_Date__c = endDate;
        update cbmsContract;*/
        
        so.Start_Date__c = stDate;
        update so;
        
        for(RCR_Contract_Scheduled_Service__c css : so.RCR_Contract_Scheduled_Services__r)
        {
            css.Original_End_Date__c = endDate;
            css.Start_Date__c = stDate;
        }
        update so.RCR_Contract_Scheduled_Services__r;
        
        for(RCR_Contract_Adhoc_Service__c ahs : so.RCR_Contract_Adhoc_Services__r)
        {
            ahs.Original_End_Date__c = endDate;
            ahs.Start_Date__c = stDate;
        }
        update so.RCR_Contract_Adhoc_Services__r;
        
        so.Original_End_Date__c = endDate;
        update so;
        
        so = RCRContractExtension.getContract(so.ID);
                
                
        Financial_Year__c fy = [SELECT ID,
                                        Start_Date__c
                                FROM    Financial_Year__c
                                WHERE   Start_Date__c = :A2HCUtilities.getStartOfFinancialYear(so.Start_Date__c)];
                                
        
//        IF_Personal_Budget__c alloc = new IF_Personal_Budget__c(Client__c = [SELECT Id FROM Referred_Client__c LIMIT 1].Id, Financial_Year__c = fy.ID);
//        alloc.Allocation_Type__c = APS_PicklistValues.IFAllocation_Type_Recurrent;
//        alloc.Allocation_Source__c = APS_PicklistValues.IFAllocation_Source_IndividualSupport;
//        alloc.Amount__c = 50.00;
//        alloc.FYE_Amount__c =  50.00;
//        insert alloc;
//
//        List<IF_Personal_Budget__c> currentAllocations = new List<IF_Personal_Budget__c>();
//
//        IF_Personal_Budget__c alloc2 = new IF_Personal_Budget__c(Client__c = [SELECT Id FROM Referred_Client__c LIMIT 1].Id, Financial_Year__c = fy.ID);
//        alloc2.Allocation_Type__c = APS_PicklistValues.IFAllocation_Type_Recurrent;
//        alloc2.Allocation_Source__c = APS_PicklistValues.IFAllocation_Source_IndividualSupport;
//        alloc2.Amount__c = 1.00;
//        alloc2.FYE_Amount__c = 1.00;
//        currentAllocations.add(alloc2);
//
//        IF_Personal_Budget__c alloc3 = new IF_Personal_Budget__c(Client__c = [SELECT Id FROM Referred_Client__c LIMIT 1].Id, Financial_Year__c = fy.ID);
//        alloc3.Allocation_Type__c = APS_PicklistValues.IFAllocation_Type_Recurrent;
//        alloc3.Allocation_Source__c = APS_PicklistValues.IFAllocation_Source_IndividualSupport;
//        alloc3.Amount__c = 1.00;
//        alloc3.FYE_Amount__c = 1.00;
//        currentAllocations.add(alloc3);
//
//        TriggerByPass.IFAllocation = true;
//        insert currentAllocations;
//        currentAllocations = new List<IF_Personal_Budget__c>();
//
//        IF_Personal_Budget__c alloc4 = new IF_Personal_Budget__c(Client__c = [SELECT Id FROM Referred_Client__c LIMIT 1].Id, Financial_Year__c = fy.ID);
//        alloc4.Allocation_Type__c = APS_PicklistValues.IFAllocation_Type_Recurrent;
//        alloc4.Allocation_Source__c = APS_PicklistValues.IFAllocation_Source_IndividualSupport;
//        alloc4.Amount__c = -1.00;
//        alloc4.FYE_Amount__c = -1.00;
//        currentAllocations.add(alloc4);
//
//        IF_Personal_Budget__c alloc5 = new IF_Personal_Budget__c(Client__c = [SELECT Id FROM Referred_Client__c LIMIT 1].Id, Financial_Year__c = fy.ID);
//        alloc5.Allocation_Type__c = APS_PicklistValues.IFAllocation_Type_Recurrent;
//        alloc5.Allocation_Source__c = APS_PicklistValues.IFAllocation_Source_IndividualSupport;
//        alloc5.Amount__c = 1.00;
//        alloc5.FYE_Amount__c = 1.00;
//        currentAllocations.add(alloc5);
//
//        ServiceOrderFuture.checkServiceOrderAllocation(so, currentAllocations);
//
        Test.stopTest();
    }
   
   
    
    @IsTest
    private static void test8()
    {
        TestLoadData.loadRCRData();
                
        Test.StartTest();
    
        RCR_Contract__c so = getContract();        
        so.RecordTypeId = A2HCUtilities.getRecordType('RCR_Contract__c', APSRecordTypes.RCRServiceOrder_Rollover).ID;
        so.Status__c = APS_PicklistValues.IFAllocation_Status_PendingApproval;
        update so;
        
        ServiceOrderFuture.createServiceOrderFromRollover(so.ID);
        
        Test.StopTest();
    }
    
    @IsTest
    private static void test9()
    {
        TestLoadData.loadRCRData();
                
        Test.StartTest();
    
        RCR_Contract__c so = getContract();
        so.RecordTypeId = A2HCUtilities.getRecordType('RCR_Contract__c', APSRecordTypes.RCRServiceOrder_Rollover).ID;
        update so;
        
//        IF_Personal_Budget__c alloc = new IF_Personal_Budget__c(RCR_Service_Order__c = so.ID, Client__c = [SELECT Id FROM Referred_Client__c LIMIT 1].Id);
//        alloc.Financial_Year__c = [SELECT ID
//                                    FROM    Financial_Year__c
//                                    WHERE   Start_Date__c = :A2HCUtilities.getStartOfFinancialYear(so.Start_Date__c)].ID;
//        alloc.Allocation_Type__c = APS_PicklistValues.IFAllocation_Type_OnceOff;
//        alloc.Allocation_Source__c = APS_PicklistValues.IFAllocation_Source_DayOptions;
//        alloc.Amount__c = 1.00;
//        alloc.FYE_Amount__c = 1.00;
//        alloc.Status__c = APS_PicklistValues.IFAllocation_Status_New;
//        insert alloc;
//
//        ServiceOrderFuture.submitAllocationsForApproval(new Set<ID>{so.ID});
        
        Test.StopTest();
    } 
     
    @IsTest
    private static void test9AndAHalf()
    {
        TestLoadData.loadRCRData();
                
        Test.StartTest();
    
        RCR_Contract__c so = getContract();
        so.RecordTypeId = A2HCUtilities.getRecordType('RCR_Contract__c', APSRecordTypes.RCRServiceOrder_Rollover).ID;
        update so;
                
//        IF_Personal_Budget__c alloc = new IF_Personal_Budget__c(RCR_Service_Order__c = so.ID, Client__c = [SELECT Id FROM Referred_Client__c LIMIT 1].Id);
//        alloc.Financial_Year__c = [SELECT ID
//                                    FROM    Financial_Year__c
//                                    WHERE   Start_Date__c = :A2HCUtilities.getStartOfFinancialYear(so.Start_Date__c)].ID;
//        alloc.Allocation_Type__c = APS_PicklistValues.IFAllocation_Type_OnceOff;
//        alloc.Allocation_Source__c = APS_PicklistValues.IFAllocation_Source_DayOptions;
//        alloc.Amount__c = 1.00;
//        alloc.FYE_Amount__c = 1.00;
//
//        insert alloc;
//        alloc.Status__c = APS_PicklistValues.IFAllocation_Status_PendingApproval;
//        update alloc;
//
//        ServiceOrderFuture.submitAllocationsForApproval(new Set<ID>{so.ID});
        
        Test.StopTest();
    }
    
    @IsTest
    private static void test10()
    {
        TestLoadData.loadRCRData();
                
        Test.StartTest();
    
        RCR_Contract__c so = getContract();
        
        ServiceOrderFuture.getFinancialYearAllocations(so.Client__c, true);
        
        ServiceOrderFuture.getFinancialYearAllocations(so.Client__c, so, Date.today());
        
        Test.StopTest();
    }
    
    
    private static RCR_Contract__c getContract()
    {           
        RCR_Contract__c so = [SELECT    ID
                                FROM    RCR_Contract__c                                
                                LIMIT 1];
        return RCRContractExtension.getContract(so.ID);                
    }
}