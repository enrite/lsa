public class RCRInvoiceSchedule
{
    @TestVisible
    private Map<String, String> errorMsgs
    {
        get
        {
            if(errorMsgs == null)
            {
                errorMsgs = new Map<String, String>();
                
                for(RCR_Error_Message__c msg : [SELECT  Name,
                                                Message__c
                                        FROM    RCR_Error_Message__c])
                {
                    errorMsgs.put(msg.Name, msg.Message__c);
                }
            }
            return errorMsgs;
        }
        private set;
    }

    public void scheduleReconciliation(List<RCR_Invoice__c> Invoices)
    {
        scheduleReconciliation(Invoices, false);
    }
    
    public void scheduleReconciliation(List<RCR_Invoice__c> Invoices, Boolean useNew)
    {
        if(Invoices.size() == 0)
        {
            return;
        }
        
        Set<ID> invoiceIDs = new Set<ID>(); 
        for(RCR_Invoice__c invoice : Invoices)
        {
            invoiceIDs.add(invoice.ID);
            if(!invoice.Override_validation__c)
            {
                decimal lowestTotal = 0.0;
                decimal highestTotal = 0.0;
                for(RCR_Invoice_Item__c item : invoice.RCR_Invoice_Items__r) 
                {
                    if(item.Total__c < lowestTotal)
                    {
                        lowestTotal = item.Total__c;
                    }
                    if(item.Total__c > highestTotal)
                    {
                        highestTotal = item.Total__c;
                    }
                }               
                if(highestTotal > 0 && lowestTotal < 0)
                {
                    for(RCR_Invoice_Item__c item : invoice.RCR_Invoice_Items__r)
                    {
                        item.Error__c = 'A mixture of debit and credit transactions are not allowed in the same activity statement.';
                    }
                    update invoice.RCR_Invoice_Items__r;
                    
                    A2HCException.formatWarning('Activity statement ' + invoice.Invoice_Number__c + ' contains a mixture of debit and credit transactions.');
                    continue;
                }
            }
        }       
        
        // check invoice has not been submitted by another user     
        Map<ID, RCR_Invoice__c> invoicesAlreadyProcessedByID  = new Map<ID, RCR_Invoice__c>(                                                            
                                                            [SELECT ID
                                                            FROM    RCR_Invoice__c
                                                            WHERE   ID IN :invoiceIDs AND
                                                                    RecordType.Name != 'Created']);
        String scheduleID;
        RCRCreateReconcileBatch invoiceBatch = new RCRCreateReconcileBatch(useNew);
        if(useNew)
        {
            scheduleID = BatchUtilities.getNextScheduleID(invoiceBatch, RCRReconcileBatchNew.class, 'Invoice ReconciliationNew_');
        }
        else
        {
            scheduleID = BatchUtilities.getNextScheduleID(invoiceBatch, RCRReconcileBatch.class, 'Invoice Reconciliation_');
        }

        for(RCR_Invoice__c invoice : Invoices)
        { 
            if(!invoicesAlreadyProcessedByID.containsKey(invoice.ID))
            {
                invoice.Schedule__c = scheduleID;
                invoice.RecordTypeId = A2HCUtilities.getRecordType('RCR_Invoice__c', 'Processing').Id; 
            }
        }
        update Invoices;
    }
    

}