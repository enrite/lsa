public with sharing class LSAContractPDFRedirectExtension
{
    public LSAContractPDFRedirectExtension(ApexPages.StandardController c)
    {
        record = (RCR_Contract__c)c.getRecord();
    }

    private RCR_Contract__c record;
    
    public PageReference Redirect()
    {
        PageReference ref = Page.LSAContractPDF;
        for(Plan_Action__c pa : [SELECT ID
                                FROM    Plan_Action__c
                                WHERE   ID = :record.Plan_Action__c AND
                                        RecordType.DeveloperName = 'MyWay_Service'])
        {
            ref = Page.LSAMyWayContractPDF;
        }
        if(record.Attendant_Care_Support__c == 'Yes')
        {
            ref = Page.LSAContractAttendantCarePDF;
            if(record.Plan_Action__r.Participant_Plan__r.RecordType.DeveloperName == 'MyPlan'
                    && record.Plan_Action__r.RecordType.DeveloperName == 'Attendant_Care_and_Support')
            {
                ref = Page.DeliveryPlanPDF;
                ref.getParameters().put('id', record.Id);

                // if both the service order and plan are approved then attach the PDF to the service order
                if(record.RecordType.DeveloperName == 'Service_Order'
                        && record.Plan_Action__r.Participant_Plan__r.Plan_Status__c == 'Approved')
                {
                    Attachment a = new Attachment();
                    a.ParentId = record.Id;
                    a.Name = 'Delivery_Plan_' + Date.today().format() + '.pdf';
                    if(!Test.isRunningTest())
                    {
                        a.Body = ref.getContentAsPDF();
                        insert a;
                    }
                }
            }
        }
         
        // add the parameters for this page to the new page reference
        for (String k : ApexPages.CurrentPage().getParameters().keySet())
        {
            if(ref.getUrl().toLowerCase().contains('lsacontractpdf') && k == 'pages')
            {
                continue;
            }
            if (k != 'save_new' && k != '_CONFIRMATIONTOKEN')
            {     
                ref.getParameters().put(k, ApexPages.CurrentPage().getParameters().get(k));           
            }
        } 
                               
        return ref;
    }
}