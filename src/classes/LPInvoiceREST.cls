@RestResource(UrlMapping='/LPInvoiceREST/*')
global without sharing class LPInvoiceREST
{
    @HttpPost
    global static void createInvoice()
    {
        try
        {
            if(!LPRESTInterface.getAuthorisationKey(RestContext.request))
            {
                RestContext.response.statusCode = 403;
                return;
            }
            Blob body = RestContext.request.requestBody;
            system.debug(body.toString());
            LPInvoiceRequestJSON invoiceRequest = LPInvoiceRequestJSON.parse(body.toString());
            system.debug(invoiceRequest);
            if(invoiceRequest.type == 'claiming.invoice.processed' ||
                    invoiceRequest.type == 'claiming.invoice.received' ||
                    invoiceRequest.type == 'claiming.invoice.cancelled')
            {
                LPRESTInterface lpInterface = new LPRESTInterface();
                lpInterface.addInvoice(invoiceRequest);
                lpInterface.saveRecords();
                if(invoiceRequest.type != 'claiming.invoice.cancelled' &&
                        !lpInterface.statementsByInvoiceNo.isEmpty())
                {
                    lpInterface.reconcileInvoices();
                }
            }
            RestContext.response.statusCode = 200;
        }
        catch(Exception ex)
        {
            RestContext.request.requestBody = Blob.valueOf('Error: ' + ex.getStackTraceString());
            RestContext.response.statusCode = 500;
        }
    }
}