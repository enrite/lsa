public class LPMemberInvoicesJSON
{
    public cls_links links;
    public cls_embedded embedded;

    class cls_links
    {
        public cls_self self;
    }

    class cls_self
    {
        public String href;    //https://api.lanternpay.com/invoices/67747b4b-ed13-0fa4-3c0b-d54512e6b517
    }

    class cls_curies
    {
        public String name;    //lp
        public String href;    //https://docs.lanternpay.com/reference#{rel}
        public boolean templated;
    }

    class cls_embedded
    {
        public cls_lpinvoices[] lpinvoices;
    }

    public class cls_lpinvoices
    {
        public cls_links links;
        public String invoiceKey;    //67747b4b-ed13-0fa4-3c0b-d54512e6b517
        public String submittedDate;    //2018-12-03T23:52:24.377
        public String invoiceDate;    //2018-12-04T00:00:00
        public String invoiceNumber;    //11177
        public Double total;    //426.6
        public cls_biller biller;
    }
    class cls_biller
    {
        public String billerKey;    //b42ff7f3-a1bc-0629-2fcf-8a3d2bc1e7a8
        public String name;    //One Rehabilitation Service
    }

    public List<cls_lpinvoices> getInvoices()
    {
        if(embedded != null && embedded.lpinvoices != null)
        {
            return embedded.lpinvoices;
        }
        return new List<cls_lpinvoices>();
    }

    public static LPMemberInvoicesJSON parse(String jsonString)
    {
        jsonString = jsonString.replace('_embedded','embedded').replace('_links', 'links').replace('lp:invoices', 'lpinvoices');
        return (LPMemberInvoicesJSON) System.JSON.deserialize(jsonString, LPMemberInvoicesJSON.class);
    }
}