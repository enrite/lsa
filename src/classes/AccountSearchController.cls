public class AccountSearchController extends A2HCPageBase
{
    public AccountSearchController()
    {

    }
    
    
    public String SearchName
    {
        get;
        set;
    }
    
    public String SearchType
    {
        get;
        set;
    }
    
    public String SearchServiceType
    {
        get;
        set;
    }
    
    public String SearchLocation
    {
        get;
        set;
    }
    
    public List<AccountWrapper> Accounts
    {
        get
        {
            if(Accounts== null)
            {
                Accounts = new List<AccountWrapper>();
            }
            return Accounts;
        }
        set;
    }

    public Boolean SearchDone
    {
        get
        {
            if(SearchDone == null)
            {
                SearchDone = false;
            }
            return SearchDone;
        }
        set;
    }

    public List<SelectOption> AccountTypes
    {
        get
        {
            if(AccountTypes == null)
            {
                AccountTypes = new List<SelectOption>();

                SelectOption firstOption = new SelectOption('', '');
                AccountTypes.add(firstOption);
                Schema.DescribeFieldResult statusFieldDescription = Account.Account_Type__c.getDescribe();
                for (Schema.Picklistentry picklistEntry : statusFieldDescription.getPicklistValues())
                {

                    AccountTypes.add(new SelectOption(pickListEntry.getValue(), pickListEntry.getLabel()));
                }
            }
             return AccountTypes;
        }
        private set;
    }

    public PageReference doSearch()
    {
        try
        {
            SearchName = A2HCUtilities.escapeSearchString(SearchName);
            SearchType = A2HCUtilities.escapeSearchString(SearchType);
            SearchServiceType = A2HCUtilities.escapeSearchString(SearchServiceType);
            SearchLocation = A2HCUtilities.escapeSearchString(SearchLocation);
            
            Set<Id> accountIDs = new Set<Id>();
            Set<Id> contactIDs = new Set<Id>();
            Map<Id, List<Contact>> contactsByAcctId = new Map<Id, List<Contact>>();
            Accounts = new List<AccountWrapper>();

            string msg = validateInputParameters();
            if(msg != null)
            {
                return A2HCException.formatException(msg);
            }
            
            string qryStr = '';
            qryStr = 'SELECT Id, ';
            qryStr +=       'Name, ';
            qryStr +=       'Account_Type__c, ';
            qryStr +=       'Service_Type__c, ';
            qryStr +=       'BillingStreet, ';
            qryStr +=       'BillingCity, ';
            qryStr +=       'BillingState, ';
            qryStr +=       'BillingPostalCode, ';
            qryStr +=       'Phone ';
            qryStr += 'FROM Account ';
            qryStr += 'WHERE ';

            string whereClause = '';
            if(!string.isBlank(SearchName))
            {
                whereClause = ' Name LIKE \'%' + SearchName + '%\' ';
            }
            if(!string.isBlank(SearchType))
            {
                if(string.isBlank(whereClause))
                {
                    whereClause = ' Account_Type__c = \'' + SearchType + '\' ';
                }
                else
                {
                    whereClause += ' AND Account_Type__c =  \'' + SearchType + '\' ';
                }

            }
            if(!string.isBlank(SearchServiceType))
            {
                if(string.isBlank(whereClause))
                {
                    whereClause = ' Service_Type__c LIKE \'%' + SearchServiceType + '%\' ';
                }
                else
                {
                    whereClause += ' AND Service_Type__c LIKE \'%' + SearchServiceType + '%\' ';
                }
            }
            if(!string.isBlank(SearchLocation))
            {
                if(string.isBlank(whereClause))
                {
                    whereClause = ' BillingCity LIKE \'%' + SearchLocation + '%\' ';
                }
                else
                {
                    whereClause += ' AND BillingCity LIKE \'%' + SearchLocation + '%\' ';
                }

            }
            qryStr += whereClause + ' ORDER BY Name';

            System.debug('qryStr: ' + qryStr);
            for(Account acc : Database.query(qryStr))
            {
                Accounts.add(new AccountWrapper(acc));
                accountIDs.add(acc.Id);
            }
            
             for(Contact c : [SELECT  Name,
                                     AccountId,
                                    MobilePhone,
                                    AHPRA_Registration_Number__c,
                                    Non_AHPRA_Qualification_check__c,
                                    Police_Check__c,
                                    Working_with_Children_Screening__c
                            FROM    Contact
                            WHERE   AccountId IN :accountIDs
                            ORDER BY Name])
            {
                if(!contactsByAcctId.containsKey(c.AccountId))
                {
                    contactsByAcctId.put((c.AccountId), new List<Contact>());
                }
                contactsByAcctId.get((c.AccountId)).add(c);
            }
            
            for(AccountWrapper aw : Accounts)
            {
                if(contactsByAcctId.containsKey(aw.Account.Id))
                {
                    aw.addContacts(contactsByAcctId.get(aw.Account.Id));
                }
            }

            SearchDone = true;
            return null;
        }
        catch(Exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }
    
    
    private string validateInputParameters()
    {
        string msg;

        if( string.isBlank(SearchName) &&
              string.isBlank(SearchType) &&
              string.isBlank(SearchServiceType) &&
              string.isBlank(SearchLocation) )
        {
            msg = 'When searching for Acccounts you must provide some search criteria.';
        }

        return msg;
    }
    
    public class AccountWrapper
    {
        public AccountWrapper(Account pAccount)
        {
            Account = pAccount;
        }

        public Account Account
        {
            get;
            private set;
        }

        public String AccAddress
        {
            get
            {
                String addr = '';
                if(!string.isBlank(Account.BillingStreet))
                {
                    addr = Account.BillingStreet + ' ' ;
                }
                if(!string.isBlank(Account.BillingCity))
                {
                    addr += Account.BillingCity + ' ' ;
                }
                if(!string.isBlank(Account.BillingState))
                {
                    addr +=  Account.BillingState + ' ' ;
                }
                if(!string.isBlank(Account.BillingPostalCode))
                {
                    addr += Account.BillingPostalCode;
                }
                AccAddress = addr;
                return AccAddress;
            }
            private set;
        }

        public List<Contact> Contacts
        {
            get
            {
                if(Contacts == null)
                {
                    Contacts = new List<Contact>();
                }
                return Contacts;
            }
            private set;
        }

        @testVisible
        public void addContacts(List<Contact> pContacts)
        {
            Contacts.addAll(pContacts);
        }
    }
}