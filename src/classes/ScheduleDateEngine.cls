public class ScheduleDateEngine
{
    public ScheduleDateEngine(Map<Id, string> idNamesMap, Date pStartDate, Date pEndDate)
    {
        ParentNameByID = idNamesMap;
        startDate = pStartDate;
        endDate = pEndDate;
    }

    public ScheduleDateEngine(Id pParentId, Date pStartDate, Date pEndDate)
    {
        initialize(new List<Id>{pParentId}, pStartDate, pEndDate);
    }

    public ScheduleDateEngine(List<Id> pParentIds, Date pStartDate, Date pEndDate)
    {
        initialize(pParentIds, pStartDate, pEndDate);
    }

    public ScheduleDateEngine(Id pParentId)
    {
        ParentNameByID = new Map<Id, string>();
        ParentNameByID.put(pParentId, pParentId);        
        for(Schedule__c s : [SELECT Start_Date__c,
                                    End_Date__c
                            FROM    Schedule__c
                            WHERE   Name = :pParentId])
        {
            startDate = s.Start_Date__c;
            endDate = s.End_Date__c;
        }
    }
    
    public ScheduleDateEngine(Schedule__c schedule)
    {
        querySchedules = false;
        Schedules.add(schedule); 
        startDate = schedule.Start_Date__c;
        endDate = schedule.End_Date__c;      
    }

    private Date startDate;
    private Date endDate;
    private Boolean querySchedules = true;

    private Map<Id, string> ParentNameByID
    {
        get;
        set;
    }

    // get a list of ScheduleDayWrapper instances
    // only really used for calendar display
    public List<ScheduleDayWrapper> ScheduleCalendarDays
    {
        get
        {
            if(ScheduleCalendarDays == null)
            {
                ScheduleCalendarDays = new List<ScheduleDayWrapper>();
                setScheduleDays();
            }
            return ScheduleCalendarDays;
        }
        private set;
    }

    public Map<String, Map<Date, decimal>> ScheduleParentLoadByCode
    {
        get
        {
            if(ScheduleParentLoadByCode == null)
            {
                ScheduleParentLoadByCode = new Map<String, Map<Date, decimal>>();
                setParentScheduleDays();
            }
            return ScheduleParentLoadByCode;
        }
        private set;
    }

    // get a map of dates the schedule falls on and the quantities for that date
    public Map<Date, decimal> ScheduleQuantityByDay
    {
        get
        {
            if(ScheduleQuantityByDay == null)
            {
                ScheduleQuantityByDay = new Map<Date, decimal>();
                for(Schedule__c s : Schedules)
                {
                    if(s.Number_of_weeks_in_period__c == 'Service Date Range')
                    {
                        continue;
                    }
                    ScheduleQuantityByDay.putAll(getScheduleDates(s));
                }
            }
            return ScheduleQuantityByDay;
        }
        private set;
    }

    public Map<String, List<Date>> DatesByScheduleParentCode
    {
        get
        {
            if(DatesByScheduleParentCode == null)
            {
                DatesByScheduleParentCode = new Map<String, List<Date>>();
                for(Schedule__c s : Schedules)
                {
                    DatesByScheduleParentCode.put(ParentNameByID.get(s.Name), getDates(s));
                }
            }
            return DatesByScheduleParentCode;
        }
        private set;
    }   
    
    public static Integer getNumberOfWeeksFromDescription(String weekDescription)
    {
        if(weekDescription == '1 Week')
        {
            return 1;
        }
        else if(weekDescription == '2 Weeks')
        {
            return 2;
        }
        else if(weekDescription == '3 Weeks')
        {
            return 3;
        }
        else if(weekDescription == '4 Weeks')
        {
            return 4;
        }
        return 0;
    }
    
    public static List<Schedule__c> getSchedules(Set<ID> parentIDs)
    {
        return [SELECT  Name,
                        Schedule_Recurrence_Pattern__c,
                        Number_of_weeks_in_period__c,
                        Start_Date__c,
                        End_Date__c,
                        Quantity_Per_Period__c,
                        (SELECT    Monday__c,
                                   Tuesday__c,
                                   Wednesday__c,
                                   Thursday__c,
                                   Friday__c,
                                   Saturday__c,
                                   Sunday__c,
                                   Week_Number__c,
                                   Week_Of_Month__c
                         FROM      Schedule_Periods__r
                         ORDER BY Week_Number__c)
                FROM    Schedule__c
                WHERE   Name IN :parentIDs];
    }

    public Date getSchedulePeriodServiceStartDate(String parentCode)
    {
        List<Date> dates = new List<Date>();
        if(DatesByScheduleParentCode.get(parentCode) != null)
        {
            dates.addAll(DatesByScheduleParentCode.get(parentCode));
        }
        return A2HCUtilities.getEarliestDate(dates);
    }

    public Date getSchedulePeriodEndDate(String parentCode)
    {
        return A2HCUtilities.getLatestDate(DatesByScheduleParentCode.get(parentCode));
    }


    public Date getSchedulePeriodStartDate(Date activityDate, SchedulePeriod period)
    {
        //*****************************************************************************
        //* This function returns the first date in the current cycle of the schedule. *
        //******************************************************************************
        if(Schedules.size() == 0)
        {
            throw new A2HCException('No schedule found.');
        }
        else if(Schedules.size() > 1)
        {
            throw new A2HCException('Multiple schedules found.');
        }
        
        return getSchedulePeriodStartDate(Schedules[0], activityDate, period);
    }
    
    
    public Date getSchedulePeriodStartDate(Schedule__c schedule, Date activityDate, SchedulePeriod period)
    {
    
        // In a Service Date Range schedule the Start of the Period is the Start Date of the Schedule
        // which should always be the same as the Start Date of the Scheduled Service.
        if(schedule.Number_of_weeks_in_period__c == 'Service Date Range')
        {
            return Schedules[0].Start_Date__c;
        }
        // In a monthly schedule the start of the current period will always be the first of the month.
        if(schedule.Number_of_weeks_in_period__c == 'Monthly')
        {
            if(period == schedulePeriod.Previous)
            {
                // Start of last month.
                return activityDate.addMonths(-1).toStartOfMonth();
            }
            else if(period == SchedulePeriod.Subsequent)
            {
                // Start of next month.
                return activityDate.addMonths(1).toStartOfMonth();
            }
            else
            {
                // Start of current month.
                return activityDate.toStartOfMonth();
            }
        }

        // In a weekly schedule we need to loop through all the iterations of the schedule periods up until
        // the current schedule period then return the first date of that period.
        Integer weeksInPeriod = getNumberOfWeeksFromDescription(schedule.Number_of_weeks_in_period__c);
        Date tempDate = schedule.Start_Date__c.toStartOfWeek();
        Date retDate = tempDate;
        while(tempDate <= activityDate)
        {
            tempDate = tempDate.addDays(7 * weeksInPeriod);
            if(tempDate > activityDate)
            {
                // If tempDate > referenceDate we have gone one iteration too many. Go back one!
                retDate = tempDate.addDays(-7 * weeksInPeriod);
            }
        }

        // Modify the retDate if we need the Previous or Subsequent period.
        if(period == SchedulePeriod.Previous)
        {
            retDate = retDate.addDays(-7 * weeksInPeriod);
        }
        else if(period == SchedulePeriod.Subsequent)
        {
            retDate = retDate.addDays(7 * weeksInPeriod);
        }
        return retDate;
    }

    public Date getSchedulePeriodEndDate(Date activityDate, SchedulePeriod period)
    {
        if(Schedules.size() == 0)
        {
            throw new A2HCException('No schedule found.');
        }
        else if(Schedules.size() > 1)
        {
            throw new A2HCException('Multiple schedules found.');
        }
        // In a Service Date Range schedule the End of the Period is the End Date of the Schedule
        // which should always be the same as the End Date of the Scheduled Service.
        if(Schedules[0].Number_of_weeks_in_period__c == 'Service Date Range')
        {
            return Schedules[0].End_Date__c;
        }

        Date tempDate = getSchedulePeriodStartDate(activityDate, period);
        if(Schedules[0].Number_of_weeks_in_period__c == 'Monthly')
        {
            tempDate = tempDate.addMonths(1).addDays(-1);
        }
        else
        {
            Integer weeksInPeriod = getNumberOfWeeksFromDescription(Schedules[0].Number_of_weeks_in_period__c);
            tempDate = tempDate.addDays(7 * weeksInPeriod) - 1;
        }
        return tempDate;
    }

    public List<Schedule__c> Schedules
    {
        get
        {
            if(Schedules == null)
            {
                if(!querySchedules)
                {
                    Schedules = new  List<Schedule__c>();
                }
                else
                {
                    Schedules = getSchedules(ParentNameByID.keySet());
                }
            }
            return Schedules;
        }
        private set;
    }
    
    public Map<Date, RCR_Scheduled_Service_Exception__c> ScheduledServiceExceptions
    {
        get
        {
            if(ScheduledServiceExceptions == null)
            {
                ScheduledServiceExceptions = new Map<Date, RCR_Scheduled_Service_Exception__c>();
                for(RCR_Scheduled_Service_Exception__c ex : [SELECT Date__c,
                                                                    Description__c,
                                                                    Standard_Quantity__c,
                                                                    Public_Holiday_Quantity__c
                                                             FROM   RCR_Scheduled_Service_Exception__c
                                                             WHERE  RCR_Contract_Scheduled_Service__c IN :ParentNameByID.keySet()])
                {
                    ScheduledServiceExceptions.put(ex.Date__c, ex);
                }
            }
            return ScheduledServiceExceptions;
        }
        set;
    }
    
    
    public Map<String, Map<Date, RCR_Scheduled_Service_Exception__c>> exceptionsByServiceCode
    {
        get
        { 
            if(exceptionsByServiceCode == null)
            {
                exceptionsByServiceCode = new Map<String, Map<Date, RCR_Scheduled_Service_Exception__c>>();             
                for(RCR_Contract_Scheduled_Service__c css : [SELECT r.Service_Code__c,
                                                                    r.Service_Description__c,
                                                                    r.Id,
                                                                    r.RCR_Contract__r.Name,
                                                                    (SELECT Date__c,
                                                                            Description__c,
                                                                            Standard_Quantity__c,
                                                                            Public_Holiday_Quantity__c
                                                                     FROM   RCR_Scheduled_Service_Exceptions__r)
                                                             FROM   RCR_Contract_Scheduled_Service__c r
                                                             WHERE  r.Id IN :ParentNameByID.keySet()])
                {
                    if(!exceptionsByServiceCode.containsKey(css.Service_Code__c))
                    {
                        exceptionsByServiceCode.put(css.Service_Code__c, new Map<date, RCR_Scheduled_Service_Exception__c>());
                    }                   
                    for(RCR_Scheduled_Service_Exception__c excep : css.RCR_Scheduled_Service_Exceptions__r)
                    {
                        if(!exceptionsByServiceCode.get(css.Service_Code__c).containsKey(excep.Date__c))
                        {
                            exceptionsByServiceCode.get(css.Service_Code__c).put(excep.Date__c, excep);
                        }
                    }
                }
            }
            return exceptionsByServiceCode;
        }
        private set;
    }    

    private void setScheduleDays()
    {
        //***********************************************************************************************
        //*                             ***** WEEKLY SCHEDULE PROCESSING *****                          *
        //*                                                                                             *
        //* STEP 1: Detemine the pattern that the Schedule determines that services will be provided    *
        //*         as an array of integers which are the number of days after the beginning of the     *
        //*         cycle.
        //* STEP 2: Determine the number of weeks in the Schedule Cycle                                 *
        //* STEP 3: Locate the start of the first cycle of the schedule to be included in the results.  *
        //* STEP 4: Create an array of Dates that are the dates that the schedule pattern has determined*
        //*         are the dates on which the event will occur.                                        *
        //* STEP 5: Loop through each date in the period if the date is contained in our list of        *
        //*         scheduled service dates add the scheduled service to that day for display in the    *
        //*         calendar.                                                                           *
        //***********************************************************************************************
        Map<Date, ScheduleDayWrapper> calendarDaysByDate = new Map<Date, ScheduleDayWrapper>();
        for(Schedule__c schedule : Schedules)
        {
            // A list of dates is not applicable for a Service Date Range type of service.
            if(schedule.Number_of_weeks_in_period__c == 'Service Date Range')
            {
                continue;
            }
            // Gets list of dates applicable to this Scheduled Service for the date range given.
            Map<Date, decimal> scheduleDates = getScheduleDates(schedule);
            Date currentDate = startDate;
            while(currentDate <= endDate)
            {
                ScheduleDayWrapper schedDayWrapper;
                if(!calendarDaysByDate.containsKey(currentDate))
                {
                     schedDayWrapper = new ScheduleDayWrapper(currentDate); 
                }
                else
                {
                    schedDayWrapper = calendarDaysByDate.get(currentDate);
                }
                if(scheduleDates.containsKey(currentDate))
                {
                    schedDayWrapper.dailySchedule.add(new ScheduleDayItemWrapper(schedule.Name, ParentNameByID.get(schedule.Name), scheduleDates.get(currentDate)));
                }
                else if(exceptionsByServiceCode.containsKey(ParentNameByID.get(schedule.Name)))
                {
                    if(exceptionsByServiceCode.get(ParentNameByID.get(schedule.Name)).containsKey(currentDate))
                    {
                        schedDayWrapper.dailySchedule.add(new ScheduleDayItemWrapper(schedule.Name, ParentNameByID.get(schedule.Name), exceptionsByServiceCode.get(ParentNameByID.get(schedule.Name)).get(currentDate).Public_Holiday_Quantity__c));
                    }
                }
                calendarDaysByDate.put(currentDate, schedDayWrapper);
                currentDate = currentDate.addDays(1);
            }
        }        
        ScheduleCalendarDays.addAll(calendarDaysByDate.values());
    }

    private void setParentScheduleDays()
    {
        for(Schedule__c schedule : Schedules)
        {
            if(schedule.Number_of_weeks_in_period__c == 'Service Date Range')
            {
                continue;
            }
            String parentCode = ParentNameByID.get(schedule.Name);
            if(ScheduleParentLoadByCode.containsKey(parentCode))
            {
                ScheduleParentLoadByCode.get(parentCode).putAll(getScheduleDates(schedule));
            }
            else
            {
                ScheduleParentLoadByCode.put(parentCode, getScheduleDates(schedule));
            }
        }
    }

    private Map<Date, decimal> getScheduleDates(Schedule__c s)
    {
        Map<Date, decimal> scheduleDates = new Map<Date, decimal>();
        if(s.Schedule_Recurrence_Pattern__c == 'Periodically')
        {
            // Monthly and Weekly scheduling.
            if(s.Number_of_weeks_in_period__c != 'Monthly')
            {
                scheduleDates = getWeeklyDates(s);
            }
            else
            {
                scheduleDates = getMonthlyDates(s);
            }
        }
        return scheduleDates;
    }

    private Map<Date, decimal> getMonthlyDates(Schedule__c s)
    {
        Map<Date, decimal> scheduleDates = new Map<Date, decimal>();
        Date thisDate = startDate;      
        while(thisDate <= endDate)
        {                   
            // Dont get dates outside of schedule.
            if(thisDate > s.End_Date__c)
            {
                break;
            }
            if(thisDate < s.Start_Date__c)
            {
                thisDate = thisDate.addDays(1);
                continue;
            }                       
            long weekNumber;
            string dayOfWeek = A2HCUtilities.getDayOfWeek(Datetime.newInstance(thisDate, Time.newInstance(12, 0, 0, 0)));
            weekNumber = (thisDate.day()/7.0).round(System.roundingMode.UP);

            for(Schedule_Period__c sp : s.Schedule_Periods__r)
            {
                if(sp.Week_Number__c.longValue() <> weekNumber)
                {
                    continue;
                }
                if(sp.Monday__c > 0.0 && dayOfWeek == 'Monday')
                {
                    scheduleDates.put(thisDate, sp.Monday__c);
                }
                if(sp.Tuesday__c > 0.0 && dayOfWeek == 'Tuesday')
                {
                    scheduleDates.put(thisDate, sp.Tuesday__c);
                }
                if(sp.Wednesday__c > 0.0 && dayOfWeek == 'Wednesday')
                {
                    scheduleDates.put(thisDate, sp.Wednesday__c);
                }
                if(sp.Thursday__c > 0.0 && dayOfWeek == 'Thursday')
                {
                    scheduleDates.put(thisDate, sp.Thursday__c);
                }
                if(sp.Friday__c > 0.0 && dayOfWeek == 'Friday')
                {
                    scheduleDates.put(thisDate, sp.Friday__c);
                }
                if(sp.Saturday__c > 0.0 && dayOfWeek == 'Saturday')
                {
                    scheduleDates.put(thisDate, sp.Saturday__c);
                }
                if(sp.Sunday__c > 0.0 && dayOfWeek == 'Sunday')
                {
                    scheduleDates.put(thisDate, sp.Sunday__c);
                }
            }
            thisDate = thisDate.addDays(1);
        }
        return scheduleDates;
    }

    private Map<Date, decimal> getWeeklyDates(Schedule__c schedule)
    {
        Date patternStartDate;
        Map<Date, decimal> scheduleDates = new Map<Date, decimal>();
        Map<integer, decimal> occurrencePattern = new Map<integer, decimal>();
        for(Schedule_Period__c p : schedule.Schedule_Periods__r)
        {
            if(p.Sunday__c > 0.0)
            {
                occurrencePattern.put(0 + (7 * (p.Week_Number__c.intValue() - 1)), p.Sunday__c);
            }
            if(p.Monday__c > 0.0)
            {
                occurrencePattern.put(1 + (7 * (p.Week_Number__c.intValue() - 1)), p.Monday__c);
            }
            if(p.Tuesday__c > 0.0)
            {
                occurrencePattern.put(2 + (7 * (p.Week_Number__c.intValue() - 1)), p.Tuesday__c);
            }
            if(p.Wednesday__c > 0.0)
            {
                occurrencePattern.put(3 + (7 * (p.Week_Number__c.intValue() - 1)), p.Wednesday__c);
            }
            if(p.Thursday__c > 0.0)
            {
                occurrencePattern.put(4 + (7 * (p.Week_Number__c.intValue() - 1)), p.Thursday__c);
            }
            if(p.Friday__c > 0.0)
            {
                occurrencePattern.put(5 + (7 * (p.Week_Number__c.intValue() - 1)), p.Friday__c);
            }
            if(p.Saturday__c > 0.0)
            {
                occurrencePattern.put(6 + (7 * (p.Week_Number__c.intValue() - 1)), p.Saturday__c);
            }
        }
        //******************
        //***** STEP 2 *****
        //******************
        integer intMod = getNumberOfWeeksFromDescription(schedule.Number_of_weeks_in_period__c);
        //******************
        //***** STEP 3 *****
        //******************
        Date currentPatternStartDate = getCurrentPatternStartDate(schedule, startDate, intMod);
        //******************
        //***** STEP 4 *****
        //******************
        integer patternCount = schedule.Start_Date__c.toStartOfWeek().daysBetween(endDate) / (intMod * 7);
        for(integer i = 0; i <= patternCount; i++)
        {
            Date d;
            for(integer j : occurrencePattern.keySet())
            {
                d = currentPatternStartDate.addDays(j + (i * intMod * 7));
                if(d >= startDate && d >= schedule.Start_Date__c)
                {
                    if(d > endDate || d > schedule.End_Date__c)
                    {
                        continue;
                    }
                    scheduleDates.put(d, occurrencePattern.get(j));
                }
            }
            if(d > endDate)
            {
                continue;
            }
        }
        return scheduleDates;
    }

    private Date getCurrentPatternStartDate(Schedule__c schedule, Date referenceDate, Integer weeksInPeriod)
    {
        if(schedule.Number_of_weeks_in_period__c == APS_PicklistValues.Schedule_WeeksInPeriod_ServiceDateRange)// 'Service Date Range')
        {
            return schedule.Start_Date__c;
        }   
        Date currentPatternStartDate = schedule.Start_Date__c.toStartOfWeek();
        Date tempDate = currentPatternStartDate;
        while(tempDate <= startDate)
        {
            tempDate = tempDate.addDays(7 * weeksInPeriod);
            if(tempDate <= startDate)
            {
                currentPatternStartDate = tempDate;
            }
        }
        return currentPatternStartDate;
    }
    
    private void initialize(List<Id> pParentIds, Date pStartDate, Date pEndDate)
    {
        ParentNameByID = new Map<Id, string>();
        for(ID recordId : pParentIds)
        {
            if(!ParentNameByID.containsKey(recordId))
            {
                ParentNameByID.put(recordId, recordId);
            }
        }
        startDate = pStartDate;
        endDate = pEndDate;
    }

    private List<Date> getDates(Schedule__c s)
    {
        List<Date> datesList = new List<Date>();
        if(s.Schedule_Recurrence_Pattern__c == 'Periodically')
        {
            if(s.Number_of_weeks_in_period__c != 'Monthly' && s.Start_Date__c == null)
            {
                throw new A2HCException('Cant calculate recurring dates without a start date');
            }
            if(startDate < s.Start_Date__c)
            {
                startDate = s.Start_Date__c;
            }
            if(s.End_Date__c != null && endDate > s.End_Date__c)
            {
                endDate = s.End_Date__c;
            }
            Date tmpDate = startDate;
            while(tmpDate <= endDate)
            {
                long weekNumber;
                string dayOfWeek = A2HCUtilities.getDayOfWeek(Datetime.newInstance(tmpDate, Time.newInstance(12, 0, 0, 0)));
                if(s.Number_of_weeks_in_period__c == 'Monthly')
                {
                    weekNumber = (tmpDate.day() / 7.0).round(System.roundingMode.UP);
                }
                else
                {
                    weekNumber = (s.Start_Date__c.daysBetween(tmpDate) / 7.0).round(System.roundingMode.DOWN);
                    weekNumber = Math.mod(weekNumber, 4) + 1;
                }
                for(Schedule_Period__c p : s.Schedule_Periods__r)
                {
                    if(p.Week_Number__c.longValue() <> weekNumber)
                    {
                        continue;
                    }
                    if((p.Monday__c > 0.0 && dayOfWeek == 'Monday') ||
                            (p.Tuesday__c > 0.0 && dayOfWeek == 'Tuesday') ||
                            (p.Wednesday__c > 0.0 && dayOfWeek == 'Wednesday') ||
                            (p.Friday__c > 0.0 && dayOfWeek == 'Friday') ||
                            (p.Saturday__c > 0.0 && dayOfWeek == 'Saturday') ||
                            (p.Sunday__c > 0.0 && dayOfWeek == 'Sunday'))
                    {
                        datesList.add(tmpDate);
                    }
                }
                tmpDate = tmpDate.addDays(1);
            }
        }
        return datesList;
    }
    
    @IsTest(SeeAllData=true)
    private static void test1()
    {
        RCR_Contract_Scheduled_Service__c css = null;
        
        for(Schedule__c schedule : [SELECT  Name
                                    FROM    Schedule__c
                                    WHERE   Number_of_weeks_in_period__c != 'Service Date Range'
                                    LIMIT 100])
        {
            for(RCR_Contract_Scheduled_Service__c c : [SELECT   ID,
                                                                Name,
                                                                Start_Date__c,
                                                                End_Date__c
                                                        FROM    RCR_Contract_Scheduled_Service__c
                                                        WHERE   Total_Cost__c > 0 AND 
                                                                ID = :schedule.Name])
            {
                css = c;
            }
            if(css != null)
            {
                break;
            }
        }
        if(css == null)
        {
            return;
        }
        
        ScheduleDateEngine dtEngine = new ScheduleDateEngine(css.ID);
        
        dtEngine.getSchedulePeriodEndDate(css.Start_Date__c, SchedulePeriod.Current);
        dtEngine.getSchedulePeriodEndDate(css.Start_Date__c, SchedulePeriod.Previous);
        
        dtEngine.getSchedulePeriodEndDate(css.ID);
        
        dtEngine.getSchedulePeriodServiceStartDate(css.ID);
        
        dtEngine.getSchedulePeriodStartDate(css.Start_Date__c, SchedulePeriod.Current);
        dtEngine.getSchedulePeriodStartDate(css.Start_Date__c, SchedulePeriod.Previous);
        dtEngine.getSchedulePeriodStartDate(dtEngine.Schedules[0], css.Start_Date__c, SchedulePeriod.Current);
        
        ScheduleDateEngine.getNumberOfWeeksFromDescription('1 Week');
        ScheduleDateEngine.getNumberOfWeeksFromDescription('2 Weeks');
        ScheduleDateEngine.getNumberOfWeeksFromDescription('3 Weeks'); 
        ScheduleDateEngine.getNumberOfWeeksFromDescription('4 Weeks');
        ScheduleDateEngine.getNumberOfWeeksFromDescription('test');
        
        system.debug(dtEngine.ScheduleCalendarDays);
        
        system.debug(dtEngine.ScheduledServiceExceptions);
        
        system.debug(dtEngine.ScheduleParentLoadByCode);
        
        system.debug(dtEngine.ScheduleQuantityByDay);
        
        system.debug(dtEngine.Schedules);
        
        system.debug(dtEngine.exceptionsByServiceCode);
        
        
        Map<Id, string> idNamesMap = new Map<Id, string>();
        idNamesMap.put(css.ID, css.Name);       
        dtEngine = new ScheduleDateEngine(idNamesMap, css.Start_Date__c, css.End_Date__c);
        
        dtEngine = new ScheduleDateEngine(css.ID, css.Start_Date__c, css.End_Date__c);
        
        List<Id> pParentIds = new Id[] {css.ID};
        dtEngine = new ScheduleDateEngine(pParentIds, css.Start_Date__c, css.End_Date__c);
        
        //dtEngine = new ScheduleDateEngine(dtEngine.Schedules[0]);
    }
    
    @IsTest//(SeeAllData=true)
    private static void test2()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
        
        Schedule__c schedule = [SELECT  Name
                                FROM    Schedule__c
                                //WHERE   Number_of_weeks_in_period__c = 'Service Date Range'
                                LIMIT 1];

        RCR_Contract_Scheduled_Service__c css = null;       
        for(RCR_Contract_Scheduled_Service__c c : [SELECT   ID,
                                                            Name,
                                                            Start_Date__c,
                                                            End_Date__c
                                                    FROM    RCR_Contract_Scheduled_Service__c
                                                    WHERE   ID = :schedule.Name
                                                    LIMIT 1])
        {
            css = c;
        }
        
        if(css == null)
        {
            return;
        }
        
        ScheduleDateEngine dtEngine = new ScheduleDateEngine(css.ID);
        
        dtEngine.getSchedulePeriodStartDate(dtEngine.Schedules[0], css.Start_Date__c, SchedulePeriod.Current);
        dtEngine.getSchedulePeriodEndDate(css.Start_Date__c, SchedulePeriod.Current);
        
        system.debug(dtEngine.ScheduleCalendarDays);
        
        system.debug(dtEngine.ScheduledServiceExceptions);
        
        system.debug(dtEngine.ScheduleParentLoadByCode);
        
        system.debug(dtEngine.ScheduleQuantityByDay);
        
        system.debug(dtEngine.Schedules);
        
        system.debug(dtEngine.exceptionsByServiceCode);
        
        Test.StopTest();
    }
    
    @IsTest//(SeeAllData=true)
    private static void test3()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
        
        Schedule__c schedule = [SELECT  Name
                                FROM    Schedule__c
                                //WHERE   Number_of_weeks_in_period__c = 'Monthly'
                                LIMIT 1];

        RCR_Contract_Scheduled_Service__c css = null;       
        for(RCR_Contract_Scheduled_Service__c c : [SELECT   ID,
                                                            Name,
                                                            Start_Date__c,
                                                            End_Date__c
                                                    FROM    RCR_Contract_Scheduled_Service__c
                                                    WHERE   ID = :schedule.Name
                                                    LIMIT 1])
        {
            css = c;
        }
        
        if(css == null)
        {
            return;
        }
        
        ScheduleDateEngine dtEngine = new ScheduleDateEngine(css.ID);
        
        dtEngine.getSchedulePeriodStartDate(dtEngine.Schedules[0],css.Start_Date__c, SchedulePeriod.Current);
        dtEngine.getSchedulePeriodEndDate(css.Start_Date__c, SchedulePeriod.Current);
        
        system.debug(dtEngine.ScheduleCalendarDays);
        
        system.debug(dtEngine.ScheduledServiceExceptions);
        
        system.debug(dtEngine.ScheduleParentLoadByCode);
        
        system.debug(dtEngine.ScheduleQuantityByDay);
        
        system.debug(dtEngine.Schedules);
        
        system.debug(dtEngine.exceptionsByServiceCode);
        
        Test.StopTest();
    }
}