public class TriggerBypass 
{
	public static Boolean RCRServiceOrder = false;
	public static Boolean RCRContractScheduledService = false;
	public static Boolean IFAllocation = false;
	public static Boolean PlanAction = false;
    public static Boolean RCRInvoice = false;
    public static Boolean RCRInvoiceItem = false;
    public static Boolean Attachment = false;
    public static Boolean PlanApproval = false;
    public static Boolean Plan = false;
    public static Boolean Client = false;

    public static Integer PlanRecursion = 0;
    public static Integer PlanActionRecursion = 0;
    public static Integer PlanApprovalRecursion = 0;
}