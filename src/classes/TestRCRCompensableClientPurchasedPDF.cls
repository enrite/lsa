@isTest//(seeAllData=true)
private class TestRCRCompensableClientPurchasedPDF 
{
    static testMethod void myUnitTest() 
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        RCR_Invoice_Item__c ii = [Select i.RCR_Service_Order__r.Account__c, i.Activity_Date__c, i.RCR_Service_Order__r.Client__c from RCR_Invoice_Item__c i 
                                    /*where i.RCR_Invoice__r.Status__c = 'Payment Processed'*/
                                    LIMIT 1];
        system.debug(ii.RCR_Service_Order__r.Account__c +'***********************'+ ii.RCR_Service_Order__r.Client__c + '*****' + ii.Activity_Date__c);
        Referred_Client__c rc = [Select id from Referred_Client__c r 
                                    /*where r.id =: ii.RCR_Service_Order__r.Client__c  */
                                    LIMIT 1];
        
        ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(rc);
        system.debug(controller.getId()+'**************');
        RCRCompensableClientPurchasedPDF ext = new RCRCompensableClientPurchasedPDF(controller);

        string datestring =  ii.Activity_Date__c.format();
        
        string tempString = 'temp';
        Date tempDate = date.valueOf('2013-06-01 12:00:00');
        decimal tempDecimal = 1;
        RCRCompensableClientPurchasedPDF.AllServices allS = new RCRCompensableClientPurchasedPDF.AllServices(tempString);
        RCRCompensableClientPurchasedPDF.ServiceTotal serT = new RCRCompensableClientPurchasedPDF.ServiceTotal(tempDecimal, tempDecimal, tempDecimal);
        RCRCompensableClientPurchasedPDF.ServiceDetail serD = new RCRCompensableClientPurchasedPDF.ServiceDetail(tempString, tempDate, tempDate, tempDate, tempDate, tempString, tempDecimal, tempDecimal, tempDecimal, tempString, tempDecimal, tempString);
  
        ext.SelectedCompensableOption = 'YesMaybe';
        system.debug(ext.setCompensableSearchOpts());
        ext.SelectedCompensableOption = 'Maybe';
        system.debug(ext.setCompensableSearchOpts());
        ext.SelectedCompensableOption = 'Yes';
        system.debug(ext.setCompensableSearchOpts());
        ext.SelectedCompensableOption = '';
        system.debug(ext.setCompensableSearchOpts());
        
        system.debug(ext.StartDate);
        system.debug(ext.EndDate);
        
        ext.StartDate = datestring;
        ext.EndDate = datestring;
        
        system.debug(ext.doSearch());
        system.debug(ext.PurchasedServiceTotals);
        system.debug(ext.PurchasedServiceDetails);
        system.debug(ext.PurchasedServices);
        
        Test.StopTest();
    }
    
    static testMethod void myUnitTest2() 
    {
        ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(new Referred_Client__c());
        system.debug(controller.getId()+'**************');
        RCRCompensableClientPurchasedPDF ext = new RCRCompensableClientPurchasedPDF(controller);
        system.debug(ext.doSearch());
    }
}