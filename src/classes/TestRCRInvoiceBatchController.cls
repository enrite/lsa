/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestRCRInvoiceBatchController 
{
    @IsTest//(SeeAllData=true) 
    static void test0()
    {
        TestLoadData.loadRCRData();
    
        Test.StartTest();
    
        RCR_Invoice_Batch__c batch = getBatch();
                                        
//        delete [SELECT  ID
//                    FROM    RCR_Service_Order_Payment__c
//                    WHERE   RCR_Invoice_Batch__c = :batch.ID];
//
        Batch.Approved_By__c = null;
        Batch.Approved_Date__c = null;   
        Batch.Batch_Number__c = null;       
        update Batch;                               
                                                                                   
        Test.setCurrentPage(new PageReference('/apex/RCRInvoiceBatch'));

        RCRInvoiceBatchController ext = new RCRInvoiceBatchController();

        ext = new RCRInvoiceBatchController(new ApexPages.Standardcontroller(new RCR_Invoice_Batch__c()));        

        ApexPages.currentPage().getParameters().put('batchId', batch.ID);
        ApexPages.currentPage().getParameters().put('control', 'Account');        
        ext = new RCRInvoiceBatchController(new ApexPages.Standardcontroller(batch));
        
        ext.first();
        system.debug('************* SrchInvoices.size(): ' + ext.SrchInvoices.size());
        
        ext.searchInvoices();
        
        for(integer j = 0; j < ext.SrchInvoices.size(); j++)
        {
            ext.SrchInvoices[j].Selected = true;
        }
        ext.addInvoices();

        //ext.next();
        ext.last();
        //ext.previous();
        ext.first();

        ext.SelectedAccount = batch.RCR_Invoices__r[0].Account__c;
        system.debug(ext.SelectedAccount);

        ext.InvoiceNumber = batch.RCR_Invoices__r[0].Invoice_Number__c;
        system.debug(ext.InvoiceNumber);

        ext.InvoiceDate = Date.today().format();
        system.debug(ext.InvoiceDate);

        system.debug(ext.GridController);
        system.debug(ext.BatchInvoices);

        system.debug(ext.NumInvoices);
        system.debug(ext.HasMasterpieceExports);
        system.debug(ext.TotalGST);
        system.debug(ext.TotalexGST);
        system.debug(ext.TotalincGST);
        system.debug(ext.TotalFailed);

        ApexPages.currentPage().getParameters().put('removeID', ext.BatchInvoices[0].ID);
        ext.removeInvoice();  
        
         ext.Batch.Description__c = null;
        ext.saveBatch();
        
        ext.Batch.Description__c = 'test';
        ext.saveBatch();   
        
        ext.resetForm();
        
        ext.cancelOverride();  
        
        ext.Batch.Batch_Number__c = 1234;
        update ext.Batch;
        ext.redir();
        
        ApexPages.currentPage().getParameters().put('inv', batch.RCR_Invoices__r[0].Invoice_Number__c);
        ext = new RCRInvoiceBatchController(new ApexPages.Standardcontroller(batch));
        Test.stopTest();
    }
    
    @IsTest//(SeeAllData=true) 
    static void test1()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        RCR_Invoice_Batch__c batch = getBatch();
                                        
//        delete [SELECT  ID
//                    FROM    RCR_Service_Order_Payment__c
//                    WHERE   RCR_Invoice_Batch__c = :batch.ID];
                    
        Batch.Approved_By__c = null;
        Batch.Approved_Date__c = null;   
        Batch.Batch_Number__c = null;       
        update Batch;                               
                                                                                    
        Test.setCurrentPage(Page.RCRInvoiceBatch);     

        ApexPages.currentPage().getParameters().put('batchId', batch.ID);
        ApexPages.currentPage().getParameters().put('control', 'Account');        
        RCRInvoiceBatchController ext = new RCRInvoiceBatchController(new ApexPages.Standardcontroller(batch));
        
        for(integer j = 0; j < ext.SrchInvoices.size(); j++)
        {
            ext.SrchInvoices[j].Selected = true;
        }
        ext.addInvoices();

        ext.processPayment();

        ApexPages.currentPage().getParameters().put('inv', batch.RCR_Invoices__r[0].Invoice_Number__c);
        ext = new RCRInvoiceBatchController(new ApexPages.Standardcontroller(batch));
        Test.stopTest();
    }
    
    @IsTest//(SeeAllData=true) 
    static void test1AndAHalf()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        RCR_Invoice_Batch__c batch = getBatch();
                                        
//        delete [SELECT  ID
//                    FROM    RCR_Service_Order_Payment__c
//                    WHERE   RCR_Invoice_Batch__c = :batch.ID];
//
        Batch.Approved_By__c = null;
        Batch.Approved_Date__c = null;   
        Batch.Batch_Number__c = null;       
        update Batch;                               
                                                                                    
        Test.setCurrentPage(Page.RCRInvoiceBatch);     

        ApexPages.currentPage().getParameters().put('batchId', batch.ID);
        ApexPages.currentPage().getParameters().put('control', 'Account');        
        RCRInvoiceBatchController ext = new RCRInvoiceBatchController(new ApexPages.Standardcontroller(batch));
        
        for(integer j = 0; j < ext.SrchInvoices.size(); j++)
        {
            ext.SrchInvoices[j].Selected = true;
        }
        ext.addInvoices();
        
        ext.printAndCreatePaymentRecords();

        ApexPages.currentPage().getParameters().put('inv', batch.RCR_Invoices__r[0].Invoice_Number__c);
        ext = new RCRInvoiceBatchController(new ApexPages.Standardcontroller(batch));
        Test.stopTest();
    }
    
    @IsTest//(SeeAllData=true) 
    static void test2()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        RCR_Invoice_Batch__c batch = getBatch();
                                        
        RCRInvoiceBatchController ext = new RCRInvoiceBatchController(new ApexPages.Standardcontroller(new RCR_Invoice_Batch__c()));
        
        ext = new RCRInvoiceBatchController(new ApexPages.Standardcontroller(batch));
                                                
        system.assert(ext.PageController != null);
        
        system.assert(ext.ServiceOrdersByName != null);
        
        ext.SelectedProcess = null;
        system.assert(ext.SelectedProcess != null);
        
        system.assert(ext.BatchInvoices != null);
        
        system.assert(ext.ErrorMsgs != null);
        
        Test.StopTest();
    }
    
    @IsTest//(SeeAllData=true) 
    static void test3()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        RCR_Invoice_Batch__c batch = getBatch();
                                        
        RCRInvoiceBatchController ext = new RCRInvoiceBatchController(new ApexPages.Standardcontroller(batch));
        
        ext.forceTestError = true;
                    
        try
        {
            ext.resetForm();            
        }
        catch(Exception ex)
        {}
        try
        {
            ext.checkForPayment();          
        }
        catch(Exception ex)
        {}
        try
        {
            ext.redir();            
        }
        catch(Exception ex)
        {}
        try
        {
            ext.searchInvoices();           
        }
        catch(Exception ex)
        {}
        try
        {
            ext.addInvoices();          
        }
        catch(Exception ex)
        {}
        try
        {
            ext.submitInvoices();           
        }
        catch(Exception ex)
        {}
        try
        {
            ext.removeInvoice();            
        }
        catch(Exception ex)
        {}
        try
        {
            ext.processPayment();           
        }
        catch(Exception ex)
        {}
        try
        {
            ext.recreateMPFile();           
        }
        catch(Exception ex)
        {}
        try
        {
            ext.printAndCreatePaymentRecords();         
        }
        catch(Exception ex)
        {}
        try
        {
            ext.saveBatch();            
        }
        catch(Exception ex)
        {}
        try
        {
            ext.cancelOverride();           
        }
        catch(Exception ex)
        {}
        
        Test.StopTest();
    }
    
    @IsTest//(SeeAllData=true) 
    static void test4()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        RCR_Invoice_Batch__c batch = getBatch();
                                        
        RCRInvoiceBatchController ext = new RCRInvoiceBatchController(new ApexPages.Standardcontroller(batch));
        
        ext.redir();
        
        Test.setCurrentPage(Page.RCRInvoiceBatch);
        ApexPages.currentPage().getParameters().put('recMPfile', '1');
        ext.redir();
        
        ext.SelectedProcess = 'Submit';
        ext.searchInvoices();
        
        ext.SelectedProcess = 'Batch';
        ext.searchInvoices();

        for(RCRInvoiceBatchController.RCRInvoiceWrapper w : ext.SrchInvoices)
        {
            w.Selected = true;
        }
        ext.submitInvoices();   
        
        ext.processPayment();
        
        ID soID = [SELECT   RCR_Service_Order__c
                    FROM    RCR_Invoice_Item__c
                    WHERE   RCR_Invoice__r.RCR_Invoice_Batch__c = :Batch.Id AND
                            RCR_Service_Order__c != null
                    LIMIT 1].RCR_Service_Order__c;
                    
        RCR_Funding_Detail__c fd = new RCR_Funding_Detail__c(RCR_Service_Order__c = soID);
        fd.Activity_Code__c = 'test';
        fd.Cost_Centre__c = null;
        insert fd;  
        
        fd = new RCR_Funding_Detail__c(RCR_Service_Order__c = soID);
        fd.Activity_Code__c = 'test';
        fd.Cost_Centre__c = 'test';
        fd.Activity_Code_Based_On_Client_Details__c = true;
        insert fd;          
        ext.processPayment();
        
        ext.createActivityStatementItemFundingRecords(); 
        
        try
        {
            ext.createServiceOrderPayments(false);
        }
        catch(Exception ex){}
        
        ext.Batch.Sent_Date__c = null;
        
        try
        {
            ext.createServiceOrderPayments(false);
        }
        catch(Exception ex){}
        try
        {
            ext.recreateMPFile();   
        }
        catch(Exception ex){}
        
        Test.StopTest();
    }
    
    @IsTest//(SeeAllData=true) 
    static void test5()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        RCR_Invoice_Batch__c batch = getBatch();
                                        
        RCRInvoiceBatchController ext = new RCRInvoiceBatchController(new ApexPages.Standardcontroller(batch));
        
        ext.SelectedProcess = 'Batch';
        ext.searchInvoices();
        
        RCR_MP_Service_Order_Payment__c p = new RCR_MP_Service_Order_Payment__c();
        p.Total__c = 1.00;
        p.RCR_Invoice__c = ext.BatchInvoices[0].ID; 
        p.RCR_Invoice_Batch__c = ext.Batch.ID;
        p.Invoice_Start_Date__c = Date.today().addDays(-1); 
        p.Invoice_Number__c = 'test'; 
        p.Invoice_End_Date__c = Date.today().addDays(1);  
        p.GST_Amount__c = 1.00;
        p.Cost_Centre_Code__c = 'test';
        p.Batch_Sent_Date__c = Date.today().addDays(2); 
        p.Activity_Code__c = 'test';
        insert p;
        
        ext.generateMasterPieceAttachment(ext.getMasterpieceInvoices());    
        ext.generateMasterPieceAttachment(ext.getMasterpieceInvoices());    
        
        Test.StopTest();
    }
    
    @IsTest
    static void test6()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
        
        RCR_Invoice_Batch__c batch = getBatch();
        batch.Description__c = 'aaa';
        update batch;
                                        
        RCRInvoiceBatchController ext = new RCRInvoiceBatchController(batch.Id);
        system.debug(ext.PaymentInvoiceCount);
        system.debug(ext.Batch);
        ext.checkForPayment();
        ext.processPayment();
        ext.validForPayment();
        ext.invoiceListChanged();
        try { ext.splitItemCostAccordingToFundingDetails(new RCR_Invoice_Item__c(), new List<RCR_Funding_Detail__c>(), new List<RCR_Activity_Statement_Item_Funding__c>(), new Map<ID, decimal>(), 1); } catch(Exception e) { }
        ext.initialise(batch.Id, false);
        
        ext.AddSignedCharacter(null, true);
        ext.AddSignedCharacter('1', false);
        ext.AddSignedCharacter('2', false);
        ext.AddSignedCharacter('3', false);
        ext.AddSignedCharacter('4', false);
        ext.AddSignedCharacter('5', false);
        ext.AddSignedCharacter('6', false);
        ext.AddSignedCharacter('7', false);
        ext.AddSignedCharacter('8', false);
        ext.AddSignedCharacter('9', false);
        ext.AddSignedCharacter('0', false);
        
        Test.StopTest();
    }
    
    private static RCR_Invoice_Batch__c getBatch()
    {                               
        return [SELECT    ID,
                            (SELECT     Account__c,
                                    Invoice_Number__c
                            FROM    RCR_Invoices__r)
                    FROM    RCR_Invoice_Batch__c
                    /*WHERE   Batch_Number__c != null AND
                            ID IN (SELECT   RCR_Invoice_Batch__c
                                    FROM    RCR_Invoice__c
                                    WHERE   Status__c = :APS_PicklistValues.RCRInvoice_Status_PaymentProcessed) */
                    LIMIT 1];
    }
}