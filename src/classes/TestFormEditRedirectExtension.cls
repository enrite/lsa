@isTest
public class TestFormEditRedirectExtension
{                    
    static testMethod void myTestMethod()
    {
        TestLoadFormData data = new TestLoadFormData();
                                           
        Form__c f = new Form__c(); 
        f.Medical_Specialist_Signed__c = true;       
    
        Test.setCurrentPage(Page.FormEditRedirect);
        ApexPages.CurrentPage().getParameters().put('RecordType', data.RecordTypes.get('Severe_Injury_Notification').Id);
        
        FormEditRedirectExtension ext = new FormEditRedirectExtension(new ApexPages.StandardController(f));               
        ext.Redir(); 
        
        f.Medical_Specialist_Signed__c = false;       
        ext = new FormEditRedirectExtension(new ApexPages.StandardController(f));               
        ext.Redir(); 
        
        ApexPages.CurrentPage().getParameters().put('RecordType', 'test');
        
        ext = new FormEditRedirectExtension(new ApexPages.StandardController(f));               
        ext.Redir(); 
        
        System.runAs(data.PortalUser)               
        {
            f.Status__c = 'Submitted';
        
            ext = new FormEditRedirectExtension(new ApexPages.StandardController(f));               
            ext.Redir();
            
            system.debug(ext.InCommunity); 
        }
    }
}