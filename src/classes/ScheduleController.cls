public with sharing class ScheduleController extends ComponentControllerBase
{
    public ScheduleController()
    {
    }
    
    public ScheduleController(ID pParentRecordID, String pSelectedFlexibility)
    {
        ParentRecordID = pParentRecordID;
        SelectedFlexibility = pSelectedFlexibility;
    }

    public Id ParentRecordID
    {
        get;
        set;
    }

 
    public String SelectedFlexibility
    {
        get;
        set;
    }

    public Boolean Expenses
    {
        get;
        set;
    }

    public string SelectedRecurrencePattern
    {
        get
        {
            if(SelectedRecurrencePattern == null)
            {
                SelectedRecurrencePattern = 'Periodically';
            }
            return SelectedRecurrencePattern;
        }
        set;
    }


    public List<SelectOption> NumberOfWeeks
    {
        get
        {
            if(NumberOfWeeks == null)
            {
                NumberOfWeeks = new List<SelectOption>();
                
                NumberOfWeeks.add(new SelectOption('1 Week', '1 Week'));
                NumberOfWeeks.add(new SelectOption('2 Weeks', '2 Weeks'));
                NumberOfWeeks.add(new SelectOption('3 Weeks', '3 Weeks'));
                NumberOfWeeks.add(new SelectOption('4 Weeks', '4 Weeks'));
                NumberOfWeeks.add(new SelectOption('Monthly', 'Monthly'));
                
                if(SelectedFlexibility == APS_PicklistValues.RCRContractScheduledService_Flexibilty_Accumulative)
                {
                    NumberOfWeeks.add(new SelectOption('Service Date Range', 'Service Date Range'));
                }
            }
            return NumberOfWeeks;
        }
        set;
    }


    public Schedule__c Schedule
    {
        get
        {
            if(Schedule == null)
            {
                Schedule = new Schedule__c();
                if(ParentRecordID != null)
                {
                    // Retrieve EXISTING Schedule
                    for(Schedule__c sch : [SELECT  Name,
                                                    Schedule_Recurrence_Pattern__c,
                                                    Number_of_weeks_in_period__c,
                                                    Quantity_Per_Period__c,
                                                    (SELECT    Monday__c,
                                                               Tuesday__c,
                                                               Wednesday__c,
                                                               Thursday__c,
                                                               Friday__c,
                                                               Saturday__c,
                                                               Sunday__c,
                                                               Week_Number__c,
                                                               Week_Of_Month__c
                                                     FROM      Schedule_Periods__r
                                                     ORDER BY Week_Number__c)
                                            FROM    Schedule__c
                                            WHERE   Name = :ParentRecordID])
                    {
                        Schedule = sch;
                        SelectedRecurrencePattern = Schedule.Schedule_Recurrence_Pattern__c;
                    }
                }
                SchedulePeriods = getSchedulePeriods();
            }
            return Schedule;
        }
        set;
    }

    public List<Schedule_Period__c> getSchedulePeriods()
    {
        List<Schedule_Period__c> temp = new List<Schedule_Period__c>(); 
        for(integer i = 0; i < getNumWeeks(); i++)
        {
            temp.add(new Schedule_Period__c(Week_Number__c = i + 1,
                                            Sunday__c = 0.0,
                                            Monday__c = 0.0,
                                            Tuesday__c = 0.0,
                                            Wednesday__c = 0.0,
                                            Thursday__c = 0.0,
                                            Friday__c = 0.0,
                                            Saturday__c = 0.0));
        }
        system.debug(Schedule.Id);
        system.debug(temp.size());
        SchedulePeriods = new List<Schedule_Period__c>();
        for(Schedule_Period__c p :[SELECT  Monday__c,
                                           Tuesday__c,
                                           Wednesday__c,
                                           Thursday__c,
                                           Friday__c,
                                           Saturday__c,
                                           Sunday__c,
                                           Week_Number__c,
                                           Week_Of_Month__c
                                   FROM    Schedule_Period__c
                                   WHERE   Schedule__c = :Schedule.Id
                                   ORDER BY Week_Number__c])
        {
            system.debug(p);
            if(temp.size() >= p.Week_Number__c)
            {
                temp[p.Week_Number__c.intValue() - 1] = p;
            }
        }
        return temp;
    }


    public List<Schedule_Period__c> SchedulePeriods
    {
        get;
        set;
    }

    // Used to set the number of rows in the Schedule Periods List for a Weekly Schedule.
    public PageReference setSchedulePeriods()
    {
        SchedulePeriods = getSchedulePeriods();
        return null;
    }

    public void setSchedulePeriods2()
    {
        SchedulePeriods = getSchedulePeriods();
    }

    public decimal getScheduleQuantityPerPeriod()
    {
        decimal qty = 0.0;
        for(Schedule_Period__c p : SchedulePeriods)
        {
            qty += (p.Sunday__c == null ? 0.0 : p.Sunday__c) +
                        (p.Monday__c == null ? 0.0 : p.Monday__c) +
                        (p.Tuesday__c == null ? 0.0 : p.Tuesday__c) +
                        (p.Wednesday__c == null ? 0.0 : p.Wednesday__c) +
                        (p.Thursday__c == null ? 0.0 : p.Thursday__c) +
                        (p.Friday__c == null ? 0.0 : p.Friday__c) +
                        (p.Saturday__c == null ? 0.0 : p.Saturday__c);
        }
        return qty;
    }

    // Save the Schedule and related Schedule Period records.
    public void saveSchedule(Id schedServiceID, Date startDate, Date endDate)
    {
        if(ParentRecordID == null)
        {
            if(schedServiceID == null)
            {
                throw new A2HCException('Parent record is null');
            }
            // Must be a new schedule! Use the passed in ID instead.
            //ParentRecordID = schedServiceID;
        }       
        ParentRecordID = schedServiceID;

        Schedule.Name = ParentRecordID;
        Schedule.Schedule_Recurrence_Pattern__c = SelectedRecurrencePattern;
        Schedule.Start_Date__c = startDate;
        Schedule.End_Date__c = endDate;
        upsert Schedule;    

        SchedulePeriods = SchedulePeriods.deepClone(false);
        // Remove SchedulePeriods that dont need to be saved.
        for(integer i = SchedulePeriods.size() - 1; i >= 0; i--)
        {
            system.debug(SchedulePeriods[i].Monday__c);
            if(SchedulePeriods[i].Sunday__c == 0.0 &&
               SchedulePeriods[i].Monday__c == 0.0 &&
               SchedulePeriods[i].Tuesday__c == 0.0 &&
               SchedulePeriods[i].Wednesday__c == 0.0 &&
               SchedulePeriods[i].Thursday__c == 0.0 &&
               SchedulePeriods[i].Friday__c == 0.0 &&
               SchedulePeriods[i].Saturday__c == 0.0)
            {
                SchedulePeriods.remove(i);
            }
            else
            {
                SchedulePeriods[i].Schedule__c = Schedule.Id;
                SchedulePeriods[i].Sunday__c = validatePeriodDay(SchedulePeriods[i].Sunday__c);
                SchedulePeriods[i].Monday__c = validatePeriodDay(SchedulePeriods[i].Monday__c);
                SchedulePeriods[i].Tuesday__c = validatePeriodDay(SchedulePeriods[i].Tuesday__c);
                SchedulePeriods[i].Wednesday__c = validatePeriodDay(SchedulePeriods[i].Wednesday__c);
                SchedulePeriods[i].Thursday__c = validatePeriodDay(SchedulePeriods[i].Thursday__c);
                SchedulePeriods[i].Friday__c = validatePeriodDay(SchedulePeriods[i].Friday__c);
                SchedulePeriods[i].Saturday__c = validatePeriodDay(SchedulePeriods[i].Saturday__c);
            }
        }

        if(SchedulePeriods.size() == 0)
        {
            // Must have at least 1 Schedule_Period__c even if its empty.
            SchedulePeriods.add(new Schedule_Period__c(Schedule__c = Schedule.Id,
                                                       Sunday__c = 0.0,
                                                       Monday__c = 0.0,
                                                       Tuesday__c = 0.0,
                                                       Wednesday__c = 0.0,
                                                       Thursday__c = 0.0,
                                                       Friday__c = 0.0,
                                                       Saturday__c = 0.0));
        }   
        upsert SchedulePeriods;
        Set<ID> periodIds = new Set<ID>();
        for(Schedule_Period__c sp : SchedulePeriods)
        {
            periodIds.add(sp.ID);
        }
        // Delete any existing Schedule Period records and insert the updated ones.
        delete([SELECT  ID
                FROM    Schedule_Period__c
                WHERE   Schedule__c = :Schedule.Id AND
                        ID NOT IN :periodIds]);
                                
        setSchedulePeriods();
    }

    private decimal validatePeriodDay(decimal dayValue)
    {
        if(dayValue == null)
        {
            dayValue = 0.0;
        }
        else if(dayValue < 0)
        {
            throw new A2HCException('Time table values must be greater than or equal to 0.');
        }
        return dayValue;
    }

    private integer getNumWeeks()
    {
        // Must always return at least 1 so that we have one row in
        // when creating a new Schedule.
        integer num = 1;

        // DAY OPTIONS PROTOTYPE - commented out if statement
        //if(SelectedFlexibility != APS_PicklistValues.RCRContractScheduledService_Flexibilty_Accumulative)
        //{
        if(Schedule.Number_of_weeks_in_period__c == '2 Weeks')
        {
            num = 2;
        }
        else if(Schedule.Number_of_weeks_in_period__c == '3 Weeks')
        {
            num = 3;
        }
        else if(Schedule.Number_of_weeks_in_period__c == '4 Weeks')
        {
            num = 4;
        }
        else if(Schedule.Number_of_weeks_in_period__c == 'Monthly')
        {
            num = 5;
        }
        //}
        return num;
    }

    
}