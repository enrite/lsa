public with sharing class RCRServiceQueryExtension extends A2HCPageBase
{
    public RCRServiceQueryExtension(ApexPages.StandardController controller)
    {
        Query = (Service_Query__c)controller.getRecord();
        Query.RCR_Service_Order__c = getParameter('serviceOrder');
        Query.CSA_Related__c = getParameter('CSARelated') == 'true' ? true : false;
        Query.RecordTypeId = A2HCUtilities.getRecordType('Service_Query__c', 'RCR Open').Id;
    }
    
    public Service_Query__c Query
    {
        get;
        set;
    }
    
    public PageReference saveOverride()
    {
        try
        {
            string retURL;
            retURL = getParameter('retURL');
            
            string baseURL = URL.getSalesforceBaseUrl().toExternalForm();
            
            upsert Query;

            if(retURL != null)
            {
                PageReference pg = new PageReference(getParameter('retURL'));
                return pg.setRedirect(true);
            }
            else
            {
                return null;
            }
        }
        catch(Exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }
    
    @IsTest//(SeeAllData=true)
    private static void test1()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        RCR_Contract__c serviceOrder = [SELECT  Id,
                                                Name
                                        FROM    RCR_Contract__c
                                        /*WHERE   Name = 'TestSO'*/
                                        LIMIT 1];
                                        
        string baseURL = [SELECT Service_Provider_Portal__c FROM BaseURL__c].Service_Provider_Portal__c;                                        

        PageReference pg = Page.RCRServiceQuery;
        pg.getParameters().put('serviceOrder', serviceOrder.Id);
        pg.getParameters().put('CSARelated', 'true');
        pg.getParameters().put('retURL', baseURL + 'apex/RCRServiceOrderAcceptance?id=' + serviceOrder.Id);
        
        RCRServiceQueryExtension ext = new RCRServiceQueryExtension(new ApexPages.StandardController(new Service_Query__c()));
        
        system.debug(ext.Query);
        ext.saveOverride();
        
        Test.StopTest();
    }
}