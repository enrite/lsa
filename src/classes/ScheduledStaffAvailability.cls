global class ScheduledStaffAvailability implements Schedulable 
{   
    // this should run on a Monday morning
    global void execute(SchedulableContext ctx) 
    {            
        RecordType rt = [SELECT Id
                         FROM RecordType
                         WHERE sObjectType = 'Staff_Availability__c'
                         AND DeveloperName = 'Staff_Availability'];
    
        List<Staff_Availability__c> availabilities = new List<Staff_Availability__c>();
   
        // loop through the staff records with their absences for the next week
        for (User u : [SELECT Id,
                              Name,
                              MobilePhone,
                              (SELECT Id,
                                      Start_Date__c,
                                      End_Date__c,
                                      Absence_Type__c
                               FROM Staff_Availability__r
                               WHERE RecordType.Name = 'Staff Absences'
                               AND Start_Date__c < :Date.today().addDays(7)
                               AND End_Date__c >= :Date.today())
                       FROM User
                       WHERE CompanyName = 'LSA'
                       ORDER BY Name])
        {                        
            Staff_Availability__c sa = new Staff_Availability__c();
            sa.RecordTypeId = rt.Id;
            sa.Staff_Member__c = u.Id;
            sa.Week_Commencing__c = Date.today();
            sa.Monday__c = true;
            sa.Tuesday__c = true;           
            sa.Wednesday__c = true;
            sa.Thursday__c = true;
            sa.Friday__c = true;            
            
            // check each day of the week 
            for (Date d = Date.today(); d <= Date.today().addDays(4); d = d + 1)
            {                                                
                for (Staff_Availability__c a : u.Staff_Availability__r)
                {
                    // does the day fall in an absence
                    if (d >= a.Start_Date__c && d <= a.End_Date__c)
                    {
                        Integer dayOfWeek = math.mod(Date.newinstance(1900, 1, 1).daysBetween(d), 7);  
                        String comment = a.Absence_Type__c + ' until ' + a.End_Date__c.format();
                        
                        if (dayOfWeek == 0) { sa.Monday__c = false; sa.Monday_Comments__c = comment; }
                        else if (dayOfWeek == 1) { sa.Tuesday__c = false; sa.Tuesday_Comments__c = comment; } 
                        else if (dayOfWeek == 2) { sa.Wednesday__c = false; sa.Wednesday_Comments__c = comment; } 
                        else if (dayOfWeek == 3) { sa.Thursday__c = false; sa.Thursday_Comments__c = comment; } 
                        else if (dayOfWeek == 4) { sa.Friday__c = false; sa.Friday_Comments__c = comment; } 
                    }
                }
            }
            
            availabilities.add(sa);            
        }
        
        insert availabilities;                                                                     
    }          
}