public with sharing class CaseNotesReportPDFExtension
{
    public CaseNotesReportPDFExtension(ApexPages.StandardController c)
    {
        if(!Test.isRunningTest())
        {
            c.addFields(new List<String>{'Client_Id__c'});
        }
        record = (Referred_Client__c)c.getRecord();  
        Apexpages.currentPage().getHeaders().put('content-disposition', 'attachment; filename=CaseNotesReport_' + record.Client_Id__c + '.pdf');                
    }

    private transient List<ReportRow> tReportRows;

    public Referred_Client__c record;    
    
    public Date FromDate { get { if (ApexPages.CurrentPage().getParameters().get('from') == '') { return null; } return Date.parse(ApexPages.CurrentPage().getParameters().get('from')); } }    
    public Date ToDate { get { if (ApexPages.CurrentPage().getParameters().get('to') == '') { return null; } return Date.parse(ApexPages.CurrentPage().getParameters().get('to')); } }   
    public Boolean ShowNotes { get { return (ApexPages.CurrentPage().getParameters().get('notes') == 'true'); } }    
    public Boolean ShowEmails { get { return (ApexPages.CurrentPage().getParameters().get('emails') == 'true'); } }
    
    public List<Case_Notes__c> Notes
    {
        get
        {                              
            return [SELECT Id,
                           Note_Date__c,
                           Resulting_From__c,
                           Category__c,
                           Note__c,
                           Relating_to_Contact__r.First_Name__c,
                           Relating_to_Contact__r.Surname__c,
                           (SELECT ID,
                                    Name,
                                    Description,
                                    CreatedDate
                            FROM    Attachments
                            ORDER BY CreatedDate DESC)
                    FROM Case_Notes__c
                    WHERE Client__c = :record.Id
                    AND Note_Date__c >= :(FromDate != null ? FromDate : Date.newInstance(1900, 1, 1))
                    AND Note_Date__c <= :(ToDate != null ? ToDate : Date.newInstance(2100, 1, 1)) 
                    ORDER BY Note_Date__c DESC];        
        }        
    }
    
    public List<Notification__c> Emails
    {
        get
        {
            return [SELECT Id,
                           Date_Received__c,
                            Email_Date__c,
                           Received_From__c,
                           Email_Subject__c,
                           Email_Body__c
                    FROM Notification__c
                    WHERE Client__c = :record.Id
                    AND ((Email_Date__c >= :(FromDate != null ? FromDate : Date.newInstance(1900, 1, 1))
                        AND Email_Date__c <= :(ToDate != null ? ToDate : Date.newInstance(2100, 1, 1)))
                        OR
                        (Email_Date__c = null AND
                        Date_Received__c >= :(FromDate != null ? FromDate : Date.newInstance(1900, 1, 1))
                        AND Date_Received__c <= :(ToDate != null ? ToDate : Date.newInstance(2100, 1, 1))))
                    ORDER BY Email_Date__c DESC];
        }        
    }

    public List<ReportRow> ReportRows
    {
        get
        {
            if(tReportRows == null)
            {
                tReportRows = new List<ReportRow>();
                if(ShowNotes)
                {
                    for(Case_Notes__c n : Notes)
                    {
                        tReportRows.add(new ReportRow(n));
                    }
                }
                if(ShowEmails)
                {
                    for(Notification__c e : Emails)
                    {
                        tReportRows.add(new ReportRow(e));
                    }
                }
                tReportRows.sort();
            }
            return tReportRows;
        }
    }

    public class ReportRow implements Comparable
    {
        public ReportRow(Case_Notes__c pNote)
        {
            Note = pNote;
            dateKey = Note.Note_Date__c;
        }

        public ReportRow(Notification__c pEmail)
        {
            Email = pEmail;
            dateKey = Email.Email_Date__c == null ? Email.Date_Received__c : Email.Email_Date__c;
        }

        private final Integer EXCEL_MAX_CELL_LENGTH = 32767;

        public Integer compareTo(Object obj) 
        {
            Date dt = ((ReportRow)obj).dateKey;
            if(dateKey > dt)
            {
                return 1;
            }
            else if(dateKey < dt)
            {
                return -1;
            }
            return 0;
        }

        public Date dateKey
        {
            get;
            set;
        }

        public String DateString
        {
            get
            {
                return dateKey.format();
            }
        }

        public String NoteBody
        {
            get
            {
                String s = Email == null ? Note.Note__c : Email.Email_Body__c;
                return s == null ? null : s.escapeCsv().replaceAll('\n', ' ').replaceAll('\r', '').left(EXCEL_MAX_CELL_LENGTH);
            }
        }

        public Case_Notes__c Note
        {
            get;
            private set;
        }

        public Notification__c Email
        {
            get;
            private set;
        }
    }       
}