@RestResource(UrlMapping='/LPPaymentREST/*')
global without sharing class LPPaymentREST
{
    @HttpPost
    global static void createPayment()
    {
        try
        {
            if(!LPRESTInterface.getAuthorisationKey(RestContext.request))
            {
                RestContext.response.statusCode = 403;
                return;
            }
            Blob body = RestContext.request.requestBody;
            system.debug(body.toString());
            LPPaymentRequestJSON paymentRequest = LPPaymentRequestJSON.parse(body.toString());
            system.debug(paymentRequest);
            if(paymentRequest.type == 'settlements.payments.scheduled')
            {
                LPRESTInterface lpInterface = new LPRESTInterface();
                lpInterface.createPayments(paymentRequest);
                lpInterface.saveRecords();
            }
            RestContext.response.statusCode = 200;
        }
        catch(Exception ex)
        {
            RestContext.response.statusCode = 500;
            RestContext.response.responseBody = Blob.valueOf(ex.getStackTraceString());
        }
    }
}