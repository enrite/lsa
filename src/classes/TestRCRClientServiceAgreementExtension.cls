/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestRCRClientServiceAgreementExtension {

        @isTest//(SeeAllData=true)
    public static void test1()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        TestLoadData.loadAdhocServices();
    
        Account a = [SELECT Id,
                            RCR_Date_Accepted_To_Provider_Panel__c
                     FROM Account
                     LIMIT 1];
                     
        a.RCR_Date_Accepted_To_Provider_Panel__c = null;
        update a;                     
    
        RCR_Contract__c contract = [SELECT  ID,
                                                Name,
                                                Account__c,
//                                                RCR_CBMS_Contract__c,
                                                Level__c,
                                                Start_Date__c,
                                                End_Date__c,
                                                Original_End_Date__c,
                                                Roll_Over_Service_Order__c,
                                                RecordTypeId
                                        FROM    RCR_Contract__c
                                        WHERE   Account__r.RCR_Date_Accepted_To_Provider_Panel__c = null AND
                                                Client__c != null AND
                                                ID IN (SELECT   RCR_Contract__c
                                                        FROM    RCR_Contract_Scheduled_Service__c) AND
                                                ID IN (SELECT   RCR_Contract__c
                                                        FROM    RCR_Contract_Adhoc_Service__c)
                                        LIMIT 1];
                                        
        ApexPages.currentPage().getParameters().put('id', contract.Id);
        RCRClientServiceAgreementExtension ext = new  RCRClientServiceAgreementExtension(new ApexPages.Standardcontroller(contract));
        for(RCRClientServiceAgreementExtension.ContractServiceTypeViewModel cst : ext.ContractServiceTypes)
        {
            system.debug(cst.ServiceType);
            system.debug(cst.Program);
            system.debug(cst.Name);
            system.debug(cst.Quantity);
            system.debug(cst.StartDate);
            system.debug(cst.EndDate);
            system.debug(cst.Flexibility);
            system.debug(cst.TotalCost);
            system.debug(cst.PublicHolidayUnits);
            system.debug(cst.NormalUnits);
            system.debug(cst.RowIndx);
            system.debug(cst.Period); 
        }
        
        ext.displaySimple();
        
        system.debug(ext.CSAStatus);
        system.debug(ext.ServiceOrderNumber);
        system.debug(ext.Client);
        system.debug(ext.ClientDateOfBirth);
        system.debug(ext.ClientRegion);
        system.debug(ext.ContractServiceTotalQuantity);
        system.debug(ext.CurrentDate);
        system.debug(ext.ProposalSignedBy);
        system.debug(ext.ClientServiceAgreementParticulars);
        system.debug(ext.MinisterForDisabilities);
        system.debug(ext.MinistersContractManagerName);
        system.debug(ext.MinistersContractManagerPosition);
        system.debug(ext.ContractManagerPhone);
        system.debug(ext.ContractManagerFax);
        system.debug(ext.PublicLiabilityInsurance);
        system.debug(ext.ServiceDescription);
        system.debug(ext.OutcomesForClient);
        system.debug(ext.PublicIndemnityInsurance);   
        
        for(RCRClientServiceAgreementExtension.PeriodBreakdownViewModel pbvw : ext.ContractPeriodBreakdowns)
        {           
            system.debug(pbvw.ScheduledService);
            system.debug(pbvw.AdHocService);
            system.debug(pbvw.ServiceDescription);
            system.debug(pbvw.StartDate);
            system.debug(pbvw.EndDate);
            system.debug(pbvw.Period);
            system.debug(pbvw.PeriodUnits);
            system.debug(pbvw.Flexibility);
            system.debug(pbvw.TotalUnits);
            system.debug(pbvw.TotalCost);
        }
        
        for(RCRClientServiceAgreementExtension.RateBreakdownViewModel rbvw : ext.ContractRateBreakdowns)
        {
            system.debug(rbvw.ScheduledService);
            system.debug(rbvw.AdHocService);
            system.debug(rbvw.ServiceDescription);
            for(RCR_Scheduled_Service_Breakdown_By_Rate__c ssbr : rbvw.ScheduledServiceBreakdownsByRate)
            {
                system.debug(ssbr.Start_Date__c);
            }
        }  
        
        Test.StopTest();                              
    }
    
    @isTest//(SeeAllData=true)
    public static void test2()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
        
        TestLoadData.loadAdhocServices();
            
        RCR_Contract__c contract = [SELECT  ID,
                                                Name,
                                                Account__c,
//                                                RCR_CBMS_Contract__c,
                                                Level__c,
                                                Start_Date__c,
                                                End_Date__c,
                                                Original_End_Date__c,
                                                Roll_Over_Service_Order__c,
                                                RecordTypeId
                                        FROM    RCR_Contract__c
                                        WHERE   Account__r.RCR_Date_Accepted_To_Provider_Panel__c != null AND
                                                Client__c != null AND
                                                ID IN (SELECT   RCR_Contract__c
                                                        FROM    RCR_Contract_Scheduled_Service__c) AND
                                                ID IN (SELECT   RCR_Contract__c
                                                        FROM    RCR_Contract_Adhoc_Service__c)
                                        LIMIT 1];
                                        
        ApexPages.currentPage().getParameters().put('id', contract.Id);
        RCRClientServiceAgreementExtension ext = new  RCRClientServiceAgreementExtension(new ApexPages.Standardcontroller(contract));
        for(RCRClientServiceAgreementExtension.ContractServiceTypeViewModel cst : ext.ContractServiceTypes)
        {
            system.debug(cst.ServiceType);
            system.debug(cst.Program);
            system.debug(cst.Name);
            system.debug(cst.Quantity);
            system.debug(cst.StartDate);
            system.debug(cst.EndDate);
            system.debug(cst.Flexibility);
            system.debug(cst.TotalCost);
            system.debug(cst.PublicHolidayUnits);
            system.debug(cst.NormalUnits);
            system.debug(cst.RowIndx);
            system.debug(cst.Period);   
            system.debug(cst.ServiceDescription);      
        }
        
        ext.displaySimple();
        
        system.debug(ext.CSAStatus);
        system.debug(ext.ServiceOrderNumber);
        system.debug(ext.Client);
        system.debug(ext.ClientDateOfBirth);
        system.debug(ext.ClientRegion);
        system.debug(ext.ContractServiceTotalQuantity);
        system.debug(ext.CurrentDate);
        system.debug(ext.ProposalSignedBy);
        system.debug(ext.ClientServiceAgreementParticulars);
        system.debug(ext.MinisterForDisabilities);
        system.debug(ext.MinistersContractManagerName);
        system.debug(ext.MinistersContractManagerPosition);
        system.debug(ext.ContractManagerPhone);
        system.debug(ext.ContractManagerFax);
        system.debug(ext.PublicLiabilityInsurance);
        system.debug(ext.ServiceDescription);
        system.debug(ext.OutcomesForClient);
        system.debug(ext.PublicIndemnityInsurance);   
        
        for(RCRClientServiceAgreementExtension.PeriodBreakdownViewModel pbvw : ext.ContractPeriodBreakdowns)
        {           
            system.debug(pbvw.ScheduledService);
            system.debug(pbvw.AdHocService);
            system.debug(pbvw.ServiceDescription);
            system.debug(pbvw.StartDate);
            system.debug(pbvw.EndDate);
            system.debug(pbvw.Period);
            system.debug(pbvw.PeriodUnits);
            system.debug(pbvw.Flexibility);
            system.debug(pbvw.TotalUnits);
            system.debug(pbvw.TotalCost);
        }
        
        for(RCRClientServiceAgreementExtension.RateBreakdownViewModel rbvw : ext.ContractRateBreakdowns)
        {
            system.debug(rbvw.ScheduledService);
            system.debug(rbvw.AdHocService);
            system.debug(rbvw.ServiceDescription);
            for(RCR_Scheduled_Service_Breakdown_By_Rate__c ssbr : rbvw.ScheduledServiceBreakdownsByRate)
            {
                system.debug(ssbr.Start_Date__c);
            }
        }
        
        Test.StopTest();
    }
}