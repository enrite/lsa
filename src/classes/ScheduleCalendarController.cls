public abstract class ScheduleCalendarController extends A2HCPageBase
{ 
    public ScheduleCalendarController()
    {
    	ServiceOrder = [SELECT	Start_Date__c,
								End_Date__c,
                                (SELECT     ID,
		                                    Start_Date__c,
		                                    End_Date__c,
		                                    RCR_Service_Order__c 
		                        FROM        RCR_Service_Order_Break_Periods__r)
						FROM	RCR_Contract__c
						WHERE	Id = :getParameter('Id')];
    	
    	// If the current date is outside of the Service Order date range
    	// set the calendar to either the start or end of the Service Order.
    	// Otherwise the calendar will default to the current month.								
    	if(serviceOrder.Start_Date__c > Date.today())
    	{
    		Year = serviceOrder.Start_Date__c.year();
    		Month = serviceOrder.Start_Date__c.month();
    	}
    	else if(serviceOrder.End_Date__c < Date.today())
    	{
    		Year = serviceOrder.End_Date__c.year();
    		Month = serviceOrder.End_Date__c.month();
    	}
    	SelectedServiceOrderMonth = '1/' + (Month < 10 ? '0': '') + string.valueOf(Month) + '/' + string.valueOf(Year);
    }
    
    private RCR_Contract__c ServiceOrder;
     
    public List<ScheduleDayItemWrapper> IdNamesList
    {
        get
        {
            if(IdNamesList == null)
            {
                IdNamesList = new List<ScheduleDayItemWrapper>();
            }
            return IdNamesList;
        }
        set;
    }   
 
    public List<SelectOption> ServiceOrderMonths
    {
    	get
    	{
    		if(ServiceOrderMonths == null)
    		{
    			ServiceOrderMonths = new List<SelectOption>();
    			
    			Date tempDate = ServiceOrder.Start_Date__c.toStartOfMonth();
    			integer tempYear = tempDate.year();
    			
    			SelectOption option = new SelectOption('', '<optgroup label=\'' + tempYear.format().replace(',','') + '\'></optgroup>');
        		option.setEscapeItem(false);
				ServiceOrderMonths.add(option);
    			
    			do
    			{
    				if(tempYear != tempDate.year())
    				{
    					tempYear = tempDate.year();
    				
	    				option = new SelectOption('', '<optgroup label=\'' + tempYear.format().replace(',', '') + '\'></optgroup>');
		        		option.setEscapeItem(false);
	    				ServiceOrderMonths.add(option);
    				}
    				
    				string month = Datetime.newInstance(tempDate.year(), tempDate.month(), 1).format('MMMMM');
    				
    				ServiceOrderMonths.add(new SelectOption(tempDate.format(), month));
    				
    				tempDate = tempDate.addMonths(1);
    			}
    			while(tempDate <= ServiceOrder.End_Date__c);
    		}
    		return ServiceOrderMonths;
    	}
    	private set;
    }
    
    
    public string SelectedServiceOrderMonth
    {
    	get;
    	set;
    }
    
    
    public String Today
    {
        get
        {
            return String.valueOf(Date.today());
        }
    }

    
    public integer Month
    {
        get
        {
            if(Month == null)
            {
                Month = Date.today().month();
            }
            return Month;
        }
        set;
    }


    public integer Year
    {
        get
        {
            if(Year == null)
            {
                Year = Date.today().year();
            }
            return Year;
        }
        set;
    }
    

    public String MonthName
    {
        get
        {
            return Datetime.newInstance(Year, Month, 1).format('MMMMM');
        }
    }


    public string StrYear
    {
        get
        {
            return Year.format().replace(',', '');
        }
    }
    
    
    public List<Id> SchedServicesIDs
    {
    	get
    	{
    		if(SchedServicesIDs == null)
    		{
    			SchedServicesIDs = new List<Id>();

    			for(ScheduleDayItemWrapper i : IdNamesList)
    			{
    				SchedServicesIDs.add(i.RecordId);
    			}
    		}
    		return SchedServicesIDs;
    	}
    	private set;
    }


	public List<Id> adHocIDs
	{
		get
		{
			if(adHocIDs == null)
			{
				adHocIDs = new List<Id>();
			}
			return adHocIDs;
		}
		set;
	}
	

    public Boolean ShowHolidayMessage
    {
    	get
    	{
    		if(ShowHolidayMessage == null)
    		{
    			ShowHolidayMessage = false;
    			for(ScheduleForWeek wk : Weeks)
    			{
    				if(ShowHolidayMessage)
    				{
    					break;
    				}									
    				for(ScheduleForDay d : wk.Days)
    				{	
						if(d.DayHasException)
						{
							ShowHolidayMessage = true;
							break;
						}
						if(d.ScheduleDayItems != null)
						{
							for(ScheduleDayItemWrapper item : d.ScheduleDayItems)
							{
								if(d.IsPublicHoliday && item.PublicHolidaysAtStandardRate)
								{
									if(item.AvailableForPublicHoliday)
									{
										ShowHolidayMessage = true;
										break;
									}
								}
							}
						}
    				}
    			}
    		}
    		return ShowHolidayMessage;
    	}
    	set;
    }

    public List<ScheduleForWeek> Weeks
    {
        get
        {
            if(Weeks == null)
            {
                Weeks = new List<ScheduleForWeek>();
                Map<Id, string> idNameMap = new Map<Id, string>();
                Map<Id, PeriodTemplate> periodTemplates = new Map<Id, PeriodTemplate>();

                for(ScheduleDayItemWrapper i :IdNamesList)
                {
                    idNameMap.put(i.recordId, i.label);
                }
                Date startDate = Date.newInstance(Year, Month, 1);
                Date endDate = startDate.addMonths(1).addDays(-1);
                ScheduleDateEngine dateEngine = new ScheduleDateEngine(idNameMap, startDate, endDate);
                for(Schedule__c schedule : dateEngine.Schedules)
                {
                	for(ScheduleDayItemWrapper i : IdNamesList)
                	{
                		if(schedule.Name == i.recordId)
                		{
                			date periodStartDate = dateEngine.getSchedulePeriodStartDate(schedule, startDate, SchedulePeriod.Current);   
                			integer periodLength = ScheduleDateEngine.getNumberOfWeeksFromDescription(schedule.Number_of_weeks_in_period__c) * 7;

                			periodTemplates.put(schedule.Name, new PeriodTemplate(schedule.Start_Date__c.toStartOfWeek(), periodLength, i.Label));
                		}
                	}
                }
                
                // get the first Sunday, possibly from the previous month
                Date calendarStartDate = Date.newInstance(Year, Month, 1).toStartOfWeek();
                
                // now calculate how many days to leave blank before we get to the first day of the month
                Date d = calendarStartDate;
                integer offset = 0;
                while(d.month() != Month)
                {
                    offset++;
                    d = d.addDays(1);
                }
                
                // calculate how many week rows to display
                integer calendarDays = calendarStartDate.daysBetween(d.addDays(date.daysInMonth(Year, Month)));
                integer weeksInMonth = 5;
                if(calendarDays > 35)
                {
                	weeksInMonth = 6;
                }

                Map<Date, List<ScheduleDayItemWrapper>> itemMap = new Map<Date, List<ScheduleDayItemWrapper>>();
                for(ScheduleDayWrapper dWrap : dateEngine.ScheduleCalendarDays)
                {
                	integer i = 0;
                	for(ScheduleDayItemWrapper itemWrap : dWrap.dailySchedule)
                	{
                		itemWrap.colour = dataSeriesColours.get(itemWrap.recordId);
                		i++;
                	}
                    itemMap.put(dWrap.day, dWrap.dailySchedule);
                }

                for(integer i = 1; i <= weeksInMonth; i++)
                {
                    this.Weeks.add(new ScheduleForWeek(i,
                    									Month,
                    									Year,
                    									offset,
                    									itemMap,
                    									publicHolidays,
                    									ExceptionsByServiceCode,
                    									BreakPeriods,
                    									ServiceCodesForPublicHolidays,
                    									ServiceCodesForPublicHolidaysAtStandardRatesOnly,
                    									periodTemplates));
                }
            }
            return Weeks;
        }
        private set
        {
        	Weeks = value;
        	if(value == null)
        	{
        		ShowHolidayMessage = null;
        	}
        }
    }

    public List<ScheduledServiceAccrossDateRangeWrapper> AccrossDateRangeScheduledServices
    {
    	get
    	{
    		if(AccrossDateRangeScheduledServices == null)
    		{
    			AccrossDateRangeScheduledServices = new List<ScheduledServiceAccrossDateRangeWrapper>();
    			Date startDate = Date.newInstance(Year, Month, 1);
                Date endDate = startDate.addMonths(1).addDays(-1);
                for(RCR_Contract_Scheduled_Service__c css : [SELECT Name,
                													Start_Date__c,
                													End_Date__c,
                													Total_Cost__c,
                													Service_Code__c,
                													Service_Description__c,
                													Service_Types_Desc__c
                											 FROM	RCR_Contract_Scheduled_Service__c
                											 WHERE	Id IN :SchedServicesIDs])
                {
                	Schedule__c schedule = [SELECT 	Id,
                									Name,
                									Number_of_weeks_in_period__c
        									FROM 	Schedule__c
        									WHERE 	Name = :css.Id];

                	if(schedule != null && schedule.Number_of_weeks_in_period__c == 'Service Date Range' &&
                	   A2HCUtilities.dateRangesOverlap(startDate, endDate, css.Start_Date__c, css.End_Date__c))
                	{
                		AccrossDateRangeScheduledServices.add(new ScheduledServiceAccrossDateRangeWrapper(css, true));
                	}
                }
    		}
    		return AccrossDateRangeScheduledServices;
    	}
    	set;
    }

    public List<AdHocServiceWrapper> AdHocServices
    {
    	get
    	{
    		if(AdHocServices == null)
    		{
    			AdHocServices = new List<AdHocServiceWrapper>();
    			Date startDate = Date.newInstance(Year, Month, 1);
                Date endDate = startDate.addMonths(1).addDays(-1);
    			for(RCR_Contract_Adhoc_Service__c ahs : [SELECT Name,
    															Start_Date__c,
    															End_Date__c,
    															Qty__c,
    															Amount__c,
    															Rate__c,
    															Comment__c,
    															Service_Types__c
    													 FROM	RCR_Contract_Adhoc_Service__c
    													 WHERE	Id IN :adHocIDs])
    			{
    				if(A2HCUtilities.dateRangesOverlap(startDate, endDate, ahs.Start_Date__c, ahs.End_Date__c))
    				{
    					AdHocServices.add(new AdHocServiceWrapper(ahs, true));
    				}
    			}
    		}
    		return AdHocServices;
    	}
    	set;
    }

    private Set<Date> publicHolidays
    {
    	get
    	{
	    	if(publicHolidays == null)
	    	{
	    		publicHolidays = new Set<Date>();

	    		for(Holiday h : [SELECT  ActivityDate
								  FROM	 Holiday
								  WHERE	 IsAllDay = true])
	    		{
	    			publicHolidays.add(h.ActivityDate);
	    		}
	    	}
	    	return publicHolidays;
    	}
    	private set;
    }
	
	@TestVisible
    private Map<string, string> DataSeriesColours
    {
    	get
    	{
    		if(dataSeriesColours == null)
    		{
    			dataSeriesColours = new Map<string, string>();
    			List<CalendarColours__c> coloursList = new List<CalendarColours__c>();
    			for(CalendarColours__c c : [SELECT Name, Colour__c FROM CalendarColours__c])
    			{
    				coloursList.add(c);
    			}
    			integer namesIndx = 0;
    			integer colourIndx = 0;
    			while(namesIndx < IdNamesList.size())
    			{
    				dataSeriesColours.put(IdNamesList[namesIndx].recordId, coloursList[colourIndx].Colour__c);
    				colourIndx++;
    				if(colourIndx >= coloursList.size())
    				{
    					colourIndx = 0;
    				}
    				namesIndx++;
    			}
    		}
    		return dataSeriesColours;
    	}
    	set;
    }
	
	@TestVisible
    private Map<string, Map<date, RCR_Scheduled_Service_Exception__c>> ExceptionsByServiceCode
    {
    	get
    	{
    		if(ExceptionsByServiceCode == null)
    		{
    			ExceptionsByServiceCode = new Map<string, Map<date, RCR_Scheduled_Service_Exception__c>>();
    			for(RCR_Contract_Scheduled_Service__c css : [SELECT r.Service_Code__c,
																 	r.Service_Description__c,
													           		r.Id,
													           		Public_Holidays_Not_Allowed__c,
													           		r.RCR_Contract__r.Name,
													           		(SELECT Date__c,
													           				Description__c,
													           				Standard_Quantity__c,
													           				Public_Holiday_Quantity__c
													           		 FROM   RCR_Scheduled_Service_Exceptions__r)
														     FROM   RCR_Contract_Scheduled_Service__c r
														     WHERE  r.Id IN :SchedServicesIDs])
				{
					if(!ExceptionsByServiceCode.containsKey(css.Service_Code__c))
					{
						ExceptionsByServiceCode.put(css.Service_Code__c, new Map<date, RCR_Scheduled_Service_Exception__c>());
					}

					for(RCR_Scheduled_Service_Exception__c excep : css.RCR_Scheduled_Service_Exceptions__r)
					{
						if(!ExceptionsByServiceCode.get(css.Service_Code__c).containsKey(excep.Date__c))
						{
							ExceptionsByServiceCode.get(css.Service_Code__c).put(excep.Date__c, excep);
						}
					}
					if(css.Public_Holidays_Not_Allowed__c == APS_PicklistValues.RCRContractScheduledService_PublicHolidaysNotAllowed_UseStandardRates || 
						 css.Public_Holidays_Not_Allowed__c == APS_PicklistValues.RCRContractScheduledService_PublicHolidaysNotAllowed_UseExceptions ||
						 css.Public_Holidays_Not_Allowed__c == APS_PicklistValues.RCRContractScheduledService_PublicHolidaysNotAllowed_UsePublicHolidayRates)
					{
						ServiceCodesForPublicHolidays.add(css.Service_Code__c);
					}
					if(css.Public_Holidays_Not_Allowed__c == APS_PicklistValues.RCRContractScheduledService_PublicHolidaysNotAllowed_UseStandardRates)
					{
						ServiceCodesForPublicHolidaysAtStandardRatesOnly.add(css.Service_Code__c);
					}
				}
    		}
    		return ExceptionsByServiceCode;
    	}
    	private set;
    }
    
    @TestVisible
    private List<RCR_Service_Order_Break_Period__c> BreakPeriods
    {
    	get
    	{
    		if(BreakPeriods == null)
    		{
    			BreakPeriods = new List<RCR_Service_Order_Break_Period__c>();
    			
    			for(RCR_Service_Order_Break_Period__c breakPeriod : ServiceOrder.RCR_Service_Order_Break_Periods__r)
    			{
    				BreakPeriods.add(breakPeriod);
    			}
    		}
    		return BreakPeriods;
    	}
    	private set;
    }
    
	@TestVisible
    private Set<String> ServiceCodesForPublicHolidays
    {
    	get
    	{
    		if(ServiceCodesForPublicHolidays == null)
    		{
    			ServiceCodesForPublicHolidays = new Set<String>();
    		}
    		return ServiceCodesForPublicHolidays;
    	}
    	set;
    }
    
    @TestVisible
    private Set<String> ServiceCodesForPublicHolidaysAtStandardRatesOnly
    {
    	get
    	{
    		if(ServiceCodesForPublicHolidaysAtStandardRatesOnly == null)
    		{
    			ServiceCodesForPublicHolidaysAtStandardRatesOnly = new Set<string>();
    		}
    		return ServiceCodesForPublicHolidaysAtStandardRatesOnly;
    	}
    	private set;
    }

    public PageReference getNextMonth()
    {
    	try
    	{
			if((Month + 1) > 12)
			{
				Month = 1;
				Year = Year + 1;
			}
			else
			{
				Month = Month + 1;
			}
			Weeks = null;
			AccrossDateRangeScheduledServices = null;
			AdHocServices = null;			
			SelectedServiceOrderMonth = '1/' + (Month < 10 ? '0': '') + string.valueOf(Month) + '/' + string.valueOf(Year);			
	    	return null;
    	}
    	catch(Exception ex)
    	{
    		return A2HCException.formatException(ex);
    	}
    }

    public PageReference getPreviousMonth()
    {
    	try
    	{
			if((Month -1) < 1)
			{
				Month = 12;
				Year = Year - 1;
			}
			else
			{
				Month = Month - 1;
			}
			Weeks = null;
			AccrossDateRangeScheduledServices = null;
			AdHocServices = null;		
			SelectedServiceOrderMonth = '1/' + (Month < 10 ? '0': '') + string.valueOf(Month) + '/' + string.valueOf(Year);			
	    	return null;
    	}
    	catch(Exception ex)
    	{
    		return A2HCException.formatException(ex);
    	}
    }
        
    public PageReference jumpToMonth()
    {
    	try
    	{
    		Year = Date.parse(SelectedServiceOrderMonth).year();
    		Month = Date.parse(SelectedServiceOrderMonth).month();
    		Weeks = null;
			AccrossDateRangeScheduledServices = null;
			AdHocServices = null;
			
    		return null;
    	}
    	catch(Exception ex)
    	{
    		return A2HCException.formatException(ex);
    	}
    }
    
    public PageReference cancel()
    {
    	try
    	{
    		String retURL = getParameter('retURL');
    		if(retURL != null)
    		{
    			PageReference pg = new PageReference(retURL);
    			return pg.setRedirect(true);
    		}
    		return null;
    	}
    	catch(Exception ex)
    	{
    		return A2HCException.formatException(ex);
    	}
    }

    public class ScheduleForWeek
    {
        public ScheduleForWeek(integer weekNum, integer month, integer year, integer offset,
        						Map<Date, List<ScheduleDayItemWrapper>> itemMap,
        						Set<Date> publicHolidays,
        						Map<string, Map<date, RCR_Scheduled_Service_Exception__c>> exceptionsByServiceCode,
        						List<RCR_Service_Order_Break_Period__c> breakPeriods,
        						Set<string> serviceCodesForPublicHolidays,
        						Set<string> ServiceCodesForPublicHolidaysAtStandardRatesOnly,
        						Map<Id, PeriodTemplate> pPeriodTemplates)
        {
            WeekNumber = weekNum;
            for(integer i = 1; i <= 7; i++)
            {
                this.Days.add(new ScheduleForDay(i,
                								weekNum,
                								month,
                								year,
                								offset,
                								itemMap,
                								publicHolidays,
                								exceptionsByServiceCode,
                								breakPeriods,
                								serviceCodesForPublicHolidays,
                								ServiceCodesForPublicHolidaysAtStandardRatesOnly,
                								pPeriodTemplates));
            }
        }

        public integer WeekNumber
        {
            get;
            set;
        }

        public List<ScheduleForDay> Days
        {
            get
            {
                if(Days == null)
                {
                    Days = new List<ScheduleForDay>();
                }
                return Days;
            }
            set;
        }
    }

    public class ScheduleForDay
    {
        public ScheduleForDay(Integer dayOfWeek, 
        						Integer week, 
        						Integer month, 
        						Integer year, 
        						Integer offset,
        						Map<Date, List<ScheduleDayItemWrapper>> itemMap,
        						Set<date> publicHolidays,
        						Map<string, Map<date, RCR_Scheduled_Service_Exception__c>> exceptionsByServiceCode,
        						List<RCR_Service_Order_Break_Period__c> breakPeriods,
        						Set<string> serviceCodesForPublicHolidays,
        						Set<string> ServiceCodesForPublicHolidaysAtStandardRatesOnly,
        						Map<Id, PeriodTemplate> pPeriodTemplates)
        {
        	IsPublicHoliday = false;
        	DayHasException = false;
            Date d = Date.newInstance(year, month, dayOfWeek + ((week - 1) * 7));
            Date startOfWeek = d.toStartOfWeek();
            if(week == 1 && d.day() <= offset || d.addDays(-offset).month() > month)
            {
                d = null;
            }
            else
            {
                d = d.addDays(-offset);
                boolean isBreak = false;
                
                for(RCR_Service_Order_Break_Period__c breakPeriod : breakPeriods)
                {
                	if(A2HCUtilities.isDateBetween(d, breakPeriod.Start_Date__c, breakPeriod.End_Date__c))
                	{
                		isBreak = true;
                		break;
                	}
                }
                
                // Only set ScheduleDayItems if the date does not appear in a break period.
                if(!isBreak)
                {
                	ScheduleDayItems = itemMap.get(d);
                }
                
				if(ScheduleDayItems != null)
				{
                	// Modify ScheduledDayItems to show values for exceptions.
	                for(ScheduleDayItemWrapper item : ScheduleDayItems)
	                {
	                	item.PublicHolidaysAtStandardRate = ServiceCodesForPublicHolidaysAtStandardRatesOnly.contains(item.Label);
	                	item.AvailableForPublicHoliday = serviceCodesForPublicHolidays.contains(item.Label);  	
	                	
	                	if(ExceptionsByServiceCode.containsKey(item.Label))
	                	{
	                		if(ExceptionsByServiceCode.get(item.Label).containsKey(d))
	                		{
	                			RCR_Scheduled_Service_Exception__c e = ExceptionsByServiceCode.get(item.Label).get(d);
	                			item.IsException = true;
	                			DayHasException = true;
	                			item.Quantity = e.Standard_Quantity__c;
	                			item.PublicHolidayQuantity = e.Public_Holiday_Quantity__c;
	                			IsPublicHoliday = true;
	                		}
	                	}
	                }
				}
				for(PeriodTemplate tmpl : pPeriodTemplates.values())
	            {
	            	integer shadingSwitch = 0;
	            	if(tmpl.PeriodLength > 0)
	            	{
						integer dayDiff = tmpl.ScheduledServiceStartDate.daysBetween(d);
						decimal dec = ((dayDiff +1) * 1.0).divide(tmpl.PeriodLength, 10, system.roundingMode.CEILING);
						long periodNumber = dec.round(System.roundingMode.CEILING);
						shadingSwitch = Math.mod(periodNumber.intValue(), 2);
	            	}
	            	if(shadingSwitch == 0)
	            	{
	            		ClassNames.add(tmpl.ServiceCode + '|' + 'ourShaded');
	            	}
	            	else
	            	{
	            		ClassNames.add(tmpl.ServiceCode + '|' + 'altShaded');
	            	}
	            }
            }
            DayDate = d;
            IsPublicHoliday = IsPublicHoliday ? true : publicHolidays.contains(d);
        }

        public integer Day
        {
            get
            {
                if(DayDate == null)
                {
                    return null;
                }
                else
                {
                    return DayDate.day();
                }
            }
            set;
        }

        public Date DayDate
        {
            get;
            set;
        }

        public Boolean IsPublicHoliday
		{
			get;
			set;
		}

		public Boolean DayHasException
		{
			get;
			private set;
		}


        public List<ScheduleDayItemWrapper> ScheduleDayItems
        {
            get;
            set;
        }


        public List<string> ClassNames
    	{
    		get
    		{
    			if(ClassNames == null)
    			{
    				ClassNames = new List<string>();
    			}
				return ClassNames;
    		}
    		private set;
    	}
    }


    public class ScheduledServiceAccrossDateRangeWrapper
    {
    	public ScheduledServiceAccrossDateRangeWrapper(RCR_Contract_Scheduled_Service__c pSchedService, boolean pDisplayed)
    	{
    		SchedService = pSchedService;
    		Displayed = pDisplayed;
    	}

    	public RCR_Contract_Scheduled_Service__c SchedService
    	{
    		get;
    		private set;
    	}

    	public boolean Displayed
    	{
    		get;
    		private set;
    	}
    }


    public class AdHocServiceWrapper
    {
    	public AdHocServiceWrapper(RCR_Contract_Adhoc_Service__c pAdHocService, boolean pDisplayed)
    	{
    		AdHocService = pAdHocService;
    		Displayed = pDisplayed;
    	}

    	public RCR_Contract_Adhoc_Service__c AdHocService
    	{
    		get;
    		private set;
    	}

    	public boolean Displayed
    	{
    		get;
    		set;
    	}
    }


    public class PeriodTemplate
    {
    	public PeriodTemplate(Date pScheduledServiceStartDate, integer pPeriodLengh, string pServiceCode)
    	{
    		ScheduledServiceStartDate = pScheduledServiceStartDate;
    		PeriodLength =pPeriodLengh;
    		ServiceCode = pServiceCode;
    	}


    	public Date ScheduledServiceStartDate
    	{
    		get;
    		private set;
    	}

    	public integer PeriodLength
    	{
    		get;
    		private set;
    	}

    	public string ServiceCode
    	{
    		get;
    		private set;
    	}
    }

   
}