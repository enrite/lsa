/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestRCRActivityStatementDetailExtension {

    /*@IsTest(SeeAllData=true)
    static void test1()
    {
        List<Holiday> holidays = [SELECT  ActivityDate
                                  FROM    Holiday
                                  WHERE	  IsAllDay = true
                                  order by ActivityDate];
        Date firstHoliday = holidays[0].ActivityDate;
        Date lastHoliday = holidays[holidays.size() - 1].ActivityDate;

        RCR_Contract__c contract = null;
        RCR_Contract_Scheduled_Service__c css = null; 
        for(RCR_Contract__c c : [SELECT ID,
                                            Name,
                                            Account__c,
                                            RCR_CBMS_Contract__c,
                                            Level__c,
                                            Start_Date__c,
                                            End_Date__c,
                                            Original_End_Date__c,
                                            Roll_Over_Service_Order__c,
                                            RecordTypeId,
                                            (SELECT ID,
                                                    Start_Date__c,
                                                    End_Date__c
                                            FROM    RCR_Contract_Scheduled_Services__r
                                            WHERE   Total_Cost__c > 0)
                                    FROM    RCR_Contract__c
                                    WHERE   Name = 'TestSO'
                                    AND		Account__r.Name = 'TestAccount'])
        {
            if(c.Start_Date__c < c.End_Date__c)
            {
                for(RCR_Contract_Scheduled_Service__c cs : c.RCR_Contract_Scheduled_Services__r)
                {
                    if(cs.Start_Date__c < cs.End_Date__c)
                    {
                        css = cs;
                        contract = c;
                        break;
                    }
                }
            }
            if(contract != null)
            {
                break;
            }
        }
		
		if(css == null)
		{
			return;
		}
        css.Public_holidays_not_allowed__c = APS_PicklistValues.RCRContractScheduledService_PublicHolidaysNotAllowed_UseExceptions;
        update css;

        RCRActivityStatementDetailExtension ext = new RCRActivityStatementDetailExtension(new ApexPages.Standardcontroller(contract));
        Test.setCurrentPage(Page.RCRActivityStatementDetail);
        ApexPages.currentPage().getParameters().put('id', contract.Id);
        ApexPages.currentPage().getParameters().put('soid', contract.Id);
        ext = new RCRActivityStatementDetailExtension(new ApexPages.Standardcontroller(contract));

        ext.saveOverride();
        ext.saveAndGenerateRows();

        ext.Invoice.Invoice_Number__c = 'test 10';
        ext.Invoice.Start_Date__c = contract.Start_Date__c.addDays(-10);
        ext.Invoice.Start_Date__c = contract.End_Date__c.addDays(10);
        ext.saveOverride();

        ext.Invoice.Start_Date__c = contract.Start_Date__c;
        ext.Invoice.Start_Date__c = contract.End_Date__c;
        ext.saveOverride();

        for(Holiday hol : holidays)
        {
            if(A2HCUtilities.isDateBetween(hol.ActivityDate, ext.Invoice.Start_Date__c, ext.Invoice.End_Date__c))
            {
                RCR_Scheduled_Service_Exception__c ex = new RCR_Scheduled_Service_Exception__c(RCR_Contract_Scheduled_Service__c = css.ID);
                ex.Standard_Quantity__c = 0.0;
                ex.Public_Holiday_Quantity__c = 2.0;
                ex.Date__c = hol.ActivityDate;
                insert ex;
            }
        }

        Test.startTest();
 
        ext.generateRows();
        Integer numberOfContracts = ext.ScheduledServices.size() > 5 ? 5 : ext.ScheduledServices.size(); 
        for(Integer i=0; i < numberOfContracts; i++)
        {
            ext.RowIndex = ext.ScheduledServices[i].RowIndexDisplay;
            ext.ScheduledServices[i].SelectedService = ext.ServiceList[ext.ServiceList.size() - 1].getValue();
system.debug(ext.ScheduledServices[i].SelectedService);            
            ext.ScheduledServices[i].SelectedDatePeriod = ext.ScheduledServices[i].PeriodStartDateDisplay.replace('/', '-') + '~' + ext.ScheduledServices[i].PeriodEndDateDisplay.replace('/', '-');
system.debug(ext.ScheduledServices[i].SelectedDatePeriod);              
            ext.calculateSchedRates();
            ext.ScheduledServices[i].ActualQuantity = 1.0;
            
        }
        numberOfContracts = ext.AdhocServices.size() > 5 ? 5 : ext.AdhocServices.size(); 
        for(Integer i=0; i < numberOfContracts; i++)
        {
            ext.RowIndex = ext.AdhocServices[i].RowIndexDisplay;
            ext.calculateAdHocRates();
            ext.AdhocServices[i].ActualQuantity = 1.0;
            ext.AdhocServices[i].SelectedService = ext.ServiceList[ext.ServiceList.size() - 1].getValue();
        }
		
		ext.AdhocDateList = null;
		ext.ScheduledDateList = null;
        ext.addSchRow();
        
        for(SelectOption opt : ext.ScheduledDateList)
        {
            if(A2HCUtilities.isStringNullOrEmpty(opt.getValue()))
            {
                continue;
            }
            String[] dateStrings = opt.getValue().split('~');
            Date stDt =  Date.parse(dateStrings[0].replace('-', '/'));
            Date endDt = Date.parse(dateStrings[1].replace('-', '/'));
            for(Holiday hol : holidays)
            {
                if(A2HCUtilities.isDateBetween(hol.ActivityDate, stDt, endDt))
                {
                    ext.calculateSchedRates();
                    ext.ScheduledServices[ext.ScheduledServices.size() - 1].SelectedDatePeriod = opt.getValue();
                    ext.ScheduledServices[ext.ScheduledServices.size() - 1].SelectedService = ext.ServiceList[1].getValue();
                    break;
                }
            }
        }
        ext.RowIndex = ext.ScheduledServices[ext.ScheduledServices.size() - 1].RowIndexDisplay;
        ext.calculateSchedDays();
        ext.ScheduledServices[ext.ScheduledServices.size() - 1].ActualQuantity = 2.0;

        ext.saveOverride();

        system.debug(ext.ServiceOrder);

        ext.RowIndex = null;
        system.debug(ext.RowIndex);

        ext.RateType = null;
        system.debug(ext.RateType);

        system.debug(ext.Invoice);
        system.debug(ext.ServicesByName);       

		ext.AdhocDateList = null;
		ext.ScheduledDateList = null;
        ext.addAdhocRow();
        system.debug(ext.AdhocServices.size());
        ext.calculateAdHocRates();

        system.debug(ext.ScheduledDateList);
        system.debug(ext.AdhocDateList);

        ext.saveOverride();
        ext.saveAndReconcile();

        ext.AdhocDateList = null;
        ext.ScheduledDateList  = null;
        ext.addAdhocRow();

        ApexPages.currentPage().getParameters().put('invid', ext.Invoice.Id);
        ApexPages.currentPage().getParameters().put('id', contract.Id);
        ext = new RCRActivityStatementDetailExtension(new ApexPages.Standardcontroller(contract));
		
		numberOfContracts = ext.ScheduledServices.size() > 5 ? 5 : ext.ScheduledServices.size(); 
        for(Integer i=0; i < numberOfContracts; i++)
        {
            ext.RowIndex = ext.ScheduledServices[i].RowIndexDisplay;
            ext.ScheduledServices[i].SelectedDatePeriod = null;
            ext.calculateSchedRates();
            ext.calculateSchedDays();
            ext.deleteRow();
            break;
        }
        numberOfContracts = ext.AdhocServices.size() > 5 ? 5 : ext.AdhocServices.size();
        for(Integer i=0; i < numberOfContracts; i++)
        {
            ext.RowIndex = ext.AdhocServices[i].RowIndexDisplay;
            ext.deleteRow();
            break;
        }
        ext.saveOverride();
        
        system.debug(ext.InvoiceStartText);
        system.debug(ext.InvoiceEndText);

        Test.stopTest();
    }

    @IsTest(SeeAllData=true)
    static void test2()
    {
        List<Holiday> holidays = [SELECT  ActivityDate
                                  FROM    Holiday
                                  WHERE	  IsAllDay = true
                                  ORDER BY ActivityDate];
        Date firstHoliday = holidays[0].ActivityDate;
        Date lastHoliday = holidays[holidays.size() - 1].ActivityDate;

        RCR_Contract__c contract = null;
        RCR_Contract_Scheduled_Service__c css = null;
        for(RCR_Contract__c c : [SELECT ID,
                                            Name,
                                            Account__c,
                                            RCR_CBMS_Contract__c,
                                            Level__c,
                                            Start_Date__c,
                                            End_Date__c,
                                            Original_End_Date__c,
                                            Roll_Over_Service_Order__c,
                                            RecordTypeId,
                                            (SELECT ID,
                                                    Start_Date__c,
                                                    End_Date__c
                                            FROM    RCR_Contract_Scheduled_Services__r
                                            WHERE   Total_Cost__c > 0)
                                    FROM    RCR_Contract__c
                                    WHERE   ID IN (SELECT RCR_Contract__c
                                                    FROM    RCR_Contract_Scheduled_Service__c
                                                    WHERE   Total_Cost__c > 0) AND
                                            ID IN (SELECT RCR_Contract__c
                                                    FROM    RCR_Contract_Adhoc_Service__c
                                                    WHERE   Amount__c > 0) AND
                                            Start_Date__c > : firstHoliday AND
                                            Start_Date__c < :lastHoliday
                                    LIMIT 200])
        {
            if(c.Start_Date__c < c.End_Date__c)
            {
                for(RCR_Contract_Scheduled_Service__c cs : c.RCR_Contract_Scheduled_Services__r)
                {
                    if(cs.Start_Date__c < cs.End_Date__c)
                    {
                        css = cs;
                        contract = c;
                        break;
                    }
                }
            }
            if(contract != null)
            {
                break;
            }
        }
		
		if(css == null)
		{
			return;
		}
        css.Public_holidays_not_allowed__c = APS_PicklistValues.RCRContractScheduledService_PublicHolidaysNotAllowed_UseExceptions;
        update css;

        RCRActivityStatementDetailExtension ext = new RCRActivityStatementDetailExtension(new ApexPages.Standardcontroller(contract));
        Test.setCurrentPage(Page.RCRActivityStatementDetail);
        ApexPages.currentPage().getParameters().put('id', contract.Id);
        ext = new RCRActivityStatementDetailExtension(new ApexPages.Standardcontroller(contract));
        ext.Invoice.Invoice_Number__c = 'Fred testxxxxxxxxxx';
        ext.Invoice.Start_Date__c = contract.Start_date__c;
        ext.Invoice.End_Date__c = contract.End_Date__c;
        ext.saveOverride();


        ext.generateRows();
		Integer numberOfContracts = ext.ScheduledServices.size() > 5 ? 5 : ext.ScheduledServices.size(); 
        for(Integer i=0; i < numberOfContracts; i++)
        {
            ext.RowIndex = ext.ScheduledServices[i].RowIndexDisplay;
            ext.ScheduledServices[i].ActualQuantity = 1.0;
            ext.ScheduledServices[i].SelectedService = ext.ScheduledServices[i].Services[1].getValue();
        }
        numberOfContracts = ext.AdhocServices.size() > 5 ? 5 : ext.AdhocServices.size(); 
        for(Integer i=0; i < numberOfContracts; i++)
        {
            ext.RowIndex = ext.AdhocServices[i].RowIndexDisplay;

            ext.ScheduledServices[i].ActualQuantity = 1.0;
            ext.AdhocServices[i].SelectedService = ext.AdhocServices[i].Services[1].getValue();
        }

        ext.saveOverride();
		
		numberOfContracts = ext.ScheduledServices.size() > 5 ? 5 : ext.ScheduledServices.size(); 
        for(Integer i=0; i < numberOfContracts; i++)
        {
            ext.RowIndex = ext.ScheduledServices[i].RowIndexDisplay;
            ext.ScheduledServices[i].SelectedDatePeriod = null;
            ext.calculateSchedRates();
            ext.calculateSchedDays();
            ext.deleteRow();
            break;
        }
        numberOfContracts = ext.AdhocServices.size() > 5 ? 5 : ext.AdhocServices.size(); 
        for(Integer i=0; i < numberOfContracts; i++)
        {
            ext.RowIndex = ext.AdhocServices[i].RowIndexDisplay;
            ext.deleteRow();
            break;
        }
        
        ext.ServiceOrder = null;
        system.debug(ext.ServiceOrder);
    }
    
    
    @IsTest(SeeAllData=true)
    static void test3()
	{
		RCR_Contract__c contract = [SELECT ID
        							FROM	RCR_Contract__c
        							LIMIT 1];
		RCRActivityStatementDetailExtension ext = new RCRActivityStatementDetailExtension(new ApexPages.Standardcontroller(contract));
        Test.setCurrentPage(Page.RCRActivityStatementDetail);

        ApexPages.currentPage().getParameters().put('id', contract.Id);
	    ext.cancelOverride();
	    
	    ApexPages.currentPage().getParameters().put('soid', contract.Id);
	    ext.cancelOverride();
	}*/
}