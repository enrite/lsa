@isTest
public with sharing class TestPaymentAuthorityBatch
{
    public static TestMethod void myTestMethod()
    {
        TestLoadData.loadRCRData();


        Date startDt = date.newInstance(Date.today().year(),1,1).addYears(-2);
        Date endDt = startDt.addYears(1).addDays(-1);
        Map<String, RecordType> rtMap = new Map<String, RecordType>();

        for (RecordType rt : [SELECT Id,
                                    Name,
                                    DeveloperName
                            FROM RecordType
                            WHERE sObjectType IN ('Form__c', 'Form_Detail__c', 'Participant_Contact__c', 'Plan_Approval__c', 'Plan__c', 'Plan_Action__c')])
        {
            rtMap.put(rt.DeveloperName, rt);
        }

        RCR_Contract__c con = [SELECT Id, Client__c FROM RCR_Contract__c WHERE Name = '1225'];

        Plan__c p = new Plan__c();
        p.Client__c = TestLoadData.getTestClient().Id;
        p.RecordTypeId = rtMap.get('MyPlan').Id;
        p.Plan_Status__c = 'Draft';
        insert p;

        Plan_Action__c pa = new Plan_Action__c();
        pa.Client__c = TestLoadData.getTestClient().Id;
        pa.Participant_Plan__c = p.Id;
        pa.RecordTypeId = rtMap.get('MyWay_Service').Id;
        pa.Approved__c = true;
        insert pa;

        Plan_Goal__c g = new Plan_Goal__c();
        g.Participant_Plan__c = p.Id;
        insert g;

        system.debug('con.clent: ' + con.Client__c);
        system.debug('pa.clent: ' + con.Client__c);
        con.Plan_Action__c = pa.Id;
        update con;

        Test.StartTest();

        PaymentAuthorityBatch batch = new PaymentAuthorityBatch();
        Database.executeBatch(batch, 10);

        Test.StopTest();
    }


}