public with sharing class AdhocPaymentEditExtension extends A2HCPageBase
{
    public AdhocPaymentEditExtension(ApexPages.StandardController c)
    {
        controller = c;
        if(!Test.isRunningTest())
        {
            controller.addFields(new List<String>{Adhoc_Payment__c.Approval_Process_Step__c.getDescribe().getName()});
        }
        record = (Adhoc_Payment__c)c.getRecord();
        setAcceptedDate();
        FindGLCodes();
    }

    private ApexPages.StandardController controller;
    private Adhoc_Payment__c record;            
    public List<SelectOption> GLCodes { get; set; }

    public Boolean InApprovalProcess
    {
        get
        {
            return ApprovalProcessWorkItem.ID != null;
        }
    }

    public Boolean ServicePlannerApproval
    {
        get
        {
            return record.Approval_Process_Step__c == 'Service Planner' && UserIsAssignedApprover;
        }
    }

    public Boolean UserIsAssignedApprover
    {
        get
        {
            if(UserIsAssignedApprover == null)
            {
                UserIsAssignedApprover = false;
                for(User u : [SELECT   ID
                                FROM    User
                                WHERE   (ID = :ApprovalProcessWorkItem.ActorId OR
                                        DelegatedApproverId = :ApprovalProcessWorkItem.ActorId) AND
                                        ID = :UserInfo.getUserId()])
                {
                    UserIsAssignedApprover = true;
                }
            }
            return UserIsAssignedApprover;
        }
        private set;
    }

    public ProcessInstanceWorkitem ApprovalProcessWorkItem
    {
        get
        {
             if(ApprovalProcessWorkItem == null)
             {
                 ApprovalProcessWorkItem = new ProcessInstanceWorkitem();
                 for(ProcessInstanceWorkitem pwi : [SELECT   ID,
                                                            ActorId,
                                                            Actor.Name
                                                     FROM    ProcessInstanceWorkitem
                                                     WHERE   ProcessInstance.TargetObjectId = :controller.getId()])
                 {
                     ApprovalProcessWorkItem = pwi;
                 }
             }
            return ApprovalProcessWorkItem;
        }
        set;
    }

    public Date AcceptedDate
    {
        get;
        private set;
    }

    public void setAcceptedDate()
    {
        String partId = getParameter('partId');
        if(partId == null)
        {
            partId = record.Participant__c;
        }
        for(Referred_Client__c c : [SELECT Current_Status_Date__c
                                    FROM    Referred_Client__c
                                    WHERE   Id = :partId])
        {
            AcceptedDate = c.Current_Status_Date__c;
        }
    }
    
    public void FindGLCodes()
    {
        try
        {
            GLCodes = new List<SelectOption>();
            GLCodes.add(new SelectOption('', '--None--'));
      
            for (RCR_Approved_Service_Type__c ast : [SELECT Id,
                                                            RCR_Account_Program_Category__r.Account__r.Finance_Code__c,
                                                            RCR_Program_Category_Service_Type__c,
                                                            RCR_Program_Category_Service_Type__r.Name,
                                                            RCR_Program_Category_Service_Type__r.Finance_Code_GG__c,
                                                            RCR_Program_Category_Service_Type__r.Finance_Code_NSAG__c
                                                     FROM RCR_Approved_Service_Type__c 
                                                     WHERE RCR_Account_Program_Category__r.Account__c = :record.Account__c
                                                     ORDER BY RCR_Program_Category_Service_Type__r.Name])
            {    
                GLCodes.add(new SelectOption(ast.RCR_Program_Category_Service_Type__c, ast.RCR_Program_Category_Service_Type__r.Name));                            
            }                                                                                                           
        }    
        catch(Exception e)
        {
            A2HCException.formatException(e);
        }    
    }

    public PageReference SaveOverride()
    {
        try
        {
            if(!validate())
            {
                return null;
            }
            //upsert record;
            controller.save();
            String retURL = ApexPages.CurrentPage().getParameters().get('retURL');
            PageReference ref = new PageReference(String.isBlank(retURL) ? ('/' + record.ID) : retURL);
            return ref;
        }
        catch(Exception e)
        {
            return A2HCException.formatException(e);
        }
    }
    
    public PageReference SaveAndNew()
    {
        try
        {
            if(!validate())
            {
                return null;
            }
            //upsert record;
            controller.save();
                                                                        
            PageReference ref = new PageReference('/' + Adhoc_Payment__c.sObjectType.getDescribe().getKeyPrefix() + '/e');    
            if (ApexPages.CurrentPage().getParameters().containsKey('retURL'))
            {
                ref.getParameters().put('retURL', ApexPages.CurrentPage().getParameters().get('retURL'));
            }                       
            
            return ref;
        }
        catch(Exception e)
        {
            return A2HCException.formatException(e);
        }    
    }

    private Boolean validate()
    {
        for(Adhoc_Payment__c ah : [SELECT   ID
                                    FROM    Adhoc_Payment__c
                                    WHERE   Name = :record.Name AND
                                            ID != :record.ID])
        {
            A2HCException.formatException('An adhoc payment already exists with this ID');
            return false;
        }
        for(RCR_Invoice__c inv : [SELECT ID
                                FROM    RCR_Invoice__c
                                WHERE   Invoice_Number__c = :record.Name.left(12) AND
                                        Account__c = :record.Account__c])
        {
            A2HCException.formatException('An activity statement already exists with this ID');
            return false;
        }
        return true;
    }
}