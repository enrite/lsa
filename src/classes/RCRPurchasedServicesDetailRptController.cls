public with sharing class RCRPurchasedServicesDetailRptController extends A2HCPageBase 
{
	public RCRPurchasedServicesDetailRptController()
	{
	}
	
	private String hCompOption;
    
    public String hrefCompensableOption
    {
    	get { return hCompOption;}
    	private set;
    }       
	
	public Referred_Client__c Client
    {
        get
        {
            if(Client == null)
            {
                Client = new Referred_Client__c();
            }
            return Client;
        }
        set;
    }
    
    public List<SelectOption> CompensableOptions
    {
    	get
    	{
    		if(CompensableOptions == null)
    		{
    			CompensableOptions = getSelectListFromPicklist(RCR_Contract__c.Compensable__c.getDescribe(), true);
    		}
    		return CompensableOptions;
    	}
    	private set;
    }
    
    
    public String StartDate
    {
    	get;
    	set;
    }
    
    
    public String EndDate
    {
    	get;
    	set;
    }
    
    public decimal GrandTotalExGST
    {
    	get;
    	private set;
    }
    
    public decimal GrandTotalGST 
    {
    	get;
    	private set;
    }
    
    public decimal GrandTotalIncGST
    {
    	get;
    	private set;
    }
    
    public String SelectedCompensableOption
    {
    	get;
    	set;
    }

    public List<ServiceDetail> PurchasedServiceDetails
    {
    	get
    	{
    		if(PurchasedServiceDetails == null)
    		{
    			PurchasedServiceDetails = new List<ServiceDetail>();
    		}
    		return PurchasedServiceDetails;
    	}
    	private set;
    }
    
    public PageReference doSearch()
    {   	
    	try
    	{
    		PurchasedServiceDetails = null;
    		GrandTotalExGST = 0.0;
    		GrandTotalGST = 0.0;
    		GrandTotalIncGST = 0.0;
    		
    		StartDate = StartDate.trim().length() == 0 ? null : StartDate;
    		EndDate = EndDate.trim().length() == 0 ? null : EndDate;
    		
    		Boolean valid =  isRequiredFieldValid(StartDate,  'Start Date', true);
    		valid =  isRequiredFieldValid(EndDate,  'End Date', true);
    		valid =  isRequiredFieldValid(Client.Id,  'Client', true);
    		if(!valid)
    		{
    			return null;
    		}

    		Set<String> compensableSearchOpts = setCompensableSearchOpts();    		  		    
	    	for(AggregateResult ar : [SELECT 	i.RCR_Invoice__r.Invoice_Number__c invNumber,
												i.RCR_Invoice__r.Date__c	invDate,
												i.RCR_Invoice__r.Start_Date__c startDate,
												i.RCR_Invoice__r.End_Date__c endDate,
												i.RCR_Invoice__r.Batch_Sent_Date__c sentDate,
												i.RCR_Invoice__r.RCR_Invoice_Batch__c,
												i.RCR_Service_Order__r.Compensable__c compensable,
												SUM(i.Total_ex_GST__c)	totalExGST,
	    										SUM(i.Total_GST__c) 	totalGST,
	    										SUM(i.Total__c)			totalIncGST
	    							  FROM		RCR_Invoice_Item__c i
	    							  WHERE		i.Activity_Date__c >= :Date.parse(StartDate)
	    							  AND		i.Activity_Date__c <= :Date.parse(EndDate)
	    							  AND		i.RCR_Service_Order__r.Compensable__c IN :CompensableSearchOpts
	    							  AND		i.RCR_Service_Order__r.Client__c = :Client.Id
	    							  AND		i.RCR_Invoice__r.Status__c = :APS_PicklistValues.RCRInvoice_Status_PaymentProcessed
	    							  AND		i.Status__c = :APS_PicklistValues.RCRInvoiceItem_Status_Approved
	    							  GROUP BY	i.RCR_Invoice__r.Invoice_Number__c,
												i.RCR_Invoice__r.Date__c,
												i.RCR_Invoice__r.Start_Date__c,
												i.RCR_Invoice__r.End_Date__c,
												i.RCR_Invoice__r.Batch_Sent_Date__c,
												i.RCR_Invoice__r.RCR_Invoice_Batch__c,
												i.RCR_Service_Order__r.Compensable__c])
	    	{
	    		PurchasedServiceDetails.add(new ServiceDetail((String)ar.get('invNumber'),
	    													  (Date)ar.get('invDate'),
	    													  (Date)ar.get('startDate'),
	    													  (Date)ar.get('endDate'),
	    													  (Date)ar.get('sentDate'),
	    													  (String)ar.get('compensable'),
	    													  (decimal)ar.get('totalExGST'), 
	    													  (decimal)ar.get('totalGST'), 
	    													  (decimal)ar.get('totalIncGST')));
	    													  
				GrandTotalExGST += (decimal)ar.get('totalExGST');
				GrandTotalGST += (decimal)ar.get('totalGST');
				GrandTotalIncGST += (decimal)ar.get('totalIncGST');	    													  
	    	}
	    	
	    	if(PurchasedServiceDetails.size() > 0)
	    	{
	    		return null;
	    	}
	    	else
	    	{
	    		return A2HCException.formatMessage('No results found for the Search Criteria you entered.');
	    	}
    	}
    	catch(Exception ex)
    	{
    		return A2HCException.formatException(ex);
    	}
    }
    
    public PageReference getClient()
    {
    	try
    	{
	        String clientId = getParameter('clientid');
	        
	        Client = [SELECT  ID,
	                        Name,
	                        Year_of_Birth__c,
	                        Work_Phone__c,
	                        Title__c,
	                        Suburb__c,
	                        Status__c,
	                        State__c,
	                        Sex__c,
	                        Sector_Client_ID__c,
	                        PostCode__c,
	                        Phone__c,
	                        Mobile__c,
	                        Main_language_spoken_at_home__c,
	                        Indigenous_Status__c,
	                        Given_Name__c,
	                        Full_Name__c,
	                        Family_Name__c,
	                        Date_Of_Birth__c,
	                        Date_Client_Registered__c,
	                        DOB_Estimated__c,
	                        Country_of_Birth__c,
	                        Client_ID__c,
	                        CCMS_ID__c,
	                        Age__c,
	                        Address__c,
	                        Account__c
	                FROM    Referred_Client__c
	                WHERE   ID = :clientId];
	                
			PurchasedServiceDetails = null;
			return null;
    	}
    	catch(Exception ex)
    	{
    		return A2HCException.formatException(ex);
    	}
    }
    
    private Set<String> setCompensableSearchOpts()
    {
		Set<String> compensableSearchOpts = new Set<String>();  
		if(SelectedCompensableOption == '' || SelectedCompensableOption == null)
		{
			compensableSearchOpts.add('');
			compensableSearchOpts.add('Yes');
			compensableSearchOpts.add('Maybe');
			hCompOption = '';
		}
		else if(SelectedCompensableOption == 'Yes')
		{
			compensableSearchOpts.add('Yes');
			hCompOption = 'Yes';
		}
		else if(SelectedCompensableOption == 'Maybe')
		{
			compensableSearchOpts.add('Maybe');
			hCompOption = 'Maybe';
		}
		else if(SelectedCompensableOption == 'Yes Maybe')
		{
			compensableSearchOpts.add('Yes');
			compensableSearchOpts.add('Maybe');
			hCompOption = '';
		}
		return compensableSearchOpts;
    }
    
    
    public class ServiceDetail
    {
    	public ServiceDetail(String pInvNumber, 
    							Date pInvDate, 
    							Date pStartDate, 
    							Date pEndDate, 
    							Date pSentDate, 
    							String pCompensable, 
    							decimal pTotalExGST, 
    							decimal pTotalGST, 
    							decimal pTotalIncGST)
    	{
    		InvoiceNumber = pInvNumber;
    		InvoiceDate = pInvDate == null ? null : pInvDate.format();
    		InvoiceStartDate = pStartDate == null ? null : pStartDate.format();
    		InvoiceEndDate = pEndDate == null ? null : pEndDate.format();
    		BatchSentDate = pSentDate == null ? null : pSentDate.format();
    		Compensable = pCompensable;
    		TotalExGST = pTotalExGST;
    		TotalGST = pTotalGST;
    		TotalIncGST = pTotalIncGST;
    	}
    	
    	public String InvoiceNumber
    	{
    		get;
    		private set;
    	}
    	
    	public String InvoiceDate
    	{
    		get;
    		private set;
    	}
    	
    	public String InvoiceStartDate
    	{
    		get;
    		private set;
    	}
    	
    	public String InvoiceEndDate
    	{
    		get;
    		private set;
    	}
    	
    	public String BatchSentDate
    	{
    		get;
    		private set;
    	}
    	
    	public String Compensable
    	{
    		get;
    		private set;
    	}
    	
    	public decimal TotalExGST
    	{
    		get;
    		private set;
    	}
    	
    	public decimal TotalGST
    	{
    		get;
    		private set;
    	}
    	
    	public decimal TotalIncGST
    	{
    		get;
    		private set;
    	}
    }
}