public class RCRServiceExtension extends A2HCPageBase
{
    public RCRServiceExtension(ApexPages.StandardController controller)
    {
        ProgramServiceId = getParameter('srvcId');

        if(ProgramServiceId == null)
        {
            ProgramServiceId = getParameter('retURL');
            if(ProgramServiceId != null)
            {
                ProgramServiceId = ProgramServiceId.replace('/', '');
            }
        }

        ProgramService = [SELECT Id,
                                 Name,
                                 Account__c
                          FROM   RCR_Service__c
                          WHERE  Id = :ProgramServiceId];

        getServices();
    }

    public RCRServiceExtension()
    {
    }

    public String ProgramServiceId
    {
        get;
        set;
    }


    public RCR_Service__c ProgramService
    {
        get;
        private set;
    }


    public string SelectedServiceIDs
    {
        get;
        set;
    }


    public string UnSelectedServiceIDs
    {
        get;
        set;
    }

    public Map<String, List<ApprovedServiceTypeWrapper>> ProgramApprovedServiceTypes
    {
        get
        {
            if(ProgramApprovedServiceTypes == null)
            {
                ProgramApprovedServiceTypes = new Map<String, List<ApprovedServiceTypeWrapper>>();
            }
            return ProgramApprovedServiceTypes;
        }
        private set;
    }

    public List<ApprovedServiceTypeWrapper> NDAApprovedServiceTypes
    {
        get
        {
            if(NDAApprovedServiceTypes == null)
            {
                NDAApprovedServiceTypes = new List<ApprovedServiceTypeWrapper>();
            }
            return NDAApprovedServiceTypes;
        }
        private set;
    }


    public List<ApprovedServiceTypeWrapper> HACCApprovedServiceTypes
    {
        get
        {
            if(HACCApprovedServiceTypes == null)
            {
                HACCApprovedServiceTypes = new List<ApprovedServiceTypeWrapper>();
            }
            return HACCApprovedServiceTypes;
        }
        private set;
    }


    public PageReference SaveOverride()
    {
        try
        {
            for (String key : ProgramApprovedServiceTypes.keySet())
            {
                for(ApprovedServiceTypeWrapper approvedServiceType : ProgramApprovedServiceTypes.get(key))
                {
                    if(approvedServiceType.Selected && approvedServiceType.ServiceType.RCR_Approved_Service_Type_Services__r.size() == 0)
                    {
                        RCR_Approved_Service_Type_Service__c s = new RCR_Approved_Service_Type_Service__c();
                        s.RCR_Approved_Service_Type__c = approvedServiceType.ServiceType.Id;
                        s.RCR_Service__c = ProgramService.Id;
    
                        insert s;
                    }
                    else if(!approvedServiceType.Selected && approvedServiceType.ServiceType.RCR_Approved_Service_Type_Services__r.size() > 0)
                    {
                        for(RCR_Approved_Service_Type_Service__c s : approvedServiceType.ServiceType.RCR_Approved_Service_Type_Services__r)
                        {
                            delete s;
                        }
                    }
                }
           }
        
            /*
            for(ApprovedServiceTypeWrapper approvedServiceType : NDAApprovedServiceTypes)
            {
                if(approvedServiceType.Selected && approvedServiceType.ServiceType.RCR_Approved_Service_Type_Services__r.size() == 0)
                {
                    RCR_Approved_Service_Type_Service__c s = new RCR_Approved_Service_Type_Service__c();
                    s.RCR_Approved_Service_Type__c = approvedServiceType.ServiceType.Id;
                    s.RCR_Service__c = ProgramService.Id;

                    insert s;
                }
                else if(!approvedServiceType.Selected && approvedServiceType.ServiceType.RCR_Approved_Service_Type_Services__r.size() > 0)
                {
                    for(RCR_Approved_Service_Type_Service__c s : approvedServiceType.ServiceType.RCR_Approved_Service_Type_Services__r)
                    {
                        delete s;
                    }
                }
            }

            for(ApprovedServiceTypeWrapper approvedServiceType : HACCApprovedServiceTypes)
            {
                if(approvedServiceType.Selected && approvedServiceType.ServiceType.RCR_Approved_Service_Type_Services__r.size() == 0)
                {
                    RCR_Approved_Service_Type_Service__c s = new RCR_Approved_Service_Type_Service__c();
                    s.RCR_Approved_Service_Type__c = approvedServiceType.ServiceType.Id;
                    s.RCR_Service__c = ProgramService.Id;

                    insert s;
                }
                else if(!approvedServiceType.Selected && approvedServiceType.ServiceType.RCR_Approved_Service_Type_Services__r.size() > 0)
                {
                    for(RCR_Approved_Service_Type_Service__c s : approvedServiceType.ServiceType.RCR_Approved_Service_Type_Services__r)
                    {
                        delete s;
                    }
                }
            }
            */

            ApexPages.Pagereference ref = new ApexPages.Pagereference('/' + ProgramServiceId);
            ref.setRedirect(true);
            return ref;
        }
        catch(Exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }

    public PageReference cancelOverride()
    {
        try
        {
            ApexPages.Pagereference ref = new ApexPages.Pagereference('/' + ProgramServiceId);
            ref.setRedirect(true);
            return ref;
        }
        catch(Exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }

    public static RCR_Service__c getRCRService(String ServiceID)
    {
         return [SELECT r.ID,
                        r.Name,
                        r.Day_Of_Week__c,
                        r.Description__c,
                        (SELECT Start_Date__c,
                                Rate__c,
                                Public_Holiday_Rate__c,
                                Name,
                                End_Date__c,
                                RCR_Service__c
                         FROM   RCR_Service_Rates__r)
                 FROM   RCR_Service__c r
                 WHERE  r.Id = :ServiceID];
    }
    
    @TestVisible
    private void getServices()
    {
        for(RCR_Approved_Service_Type__c servType : [SELECT r.Id,
                                                            r.RCR_Program_Category_Service_Type__r.RCR_Program_Category__r.RCR_Program__r.Name,
                                                            r.RCR_Program_Category_Service_Type__r.RCR_Program_Category__r.Name,
                                                            r.RCR_Program_Category_Service_Type__r.Name,
                                                            (SELECT a.Id,
                                                                    a.RCR_Service__c
                                                             FROM   RCR_Approved_Service_Type_Services__r a
                                                             WHERE  a.RCR_Service__c = :ProgramService.Id)
                                                     FROM   RCR_Approved_Service_Type__c r
                                                     WHERE  r.RCR_Account_Program_Category__c IN (SELECT Id
                                                                                                  FROM   RCR_Account_Program_Category__c
                                                                                                  WHERE  Account__c = :ProgramService.Account__c)
                                                     ORDER BY r.RCR_Program_Category_Service_Type__r.Name,
                                                              r.RCR_Program_Category_Service_Type__r.RCR_Program_Category__r.Name])
        {
            if (!ProgramApprovedServiceTypes.containsKey(servType.RCR_Program_Category_Service_Type__r.RCR_Program_Category__r.RCR_Program__r.Name))
            {
                ProgramApprovedServiceTypes.put(servType.RCR_Program_Category_Service_Type__r.RCR_Program_Category__r.RCR_Program__r.Name, new List<ApprovedServiceTypeWrapper>());             
            }
        
            if(servType.RCR_Approved_Service_Type_Services__r.size() > 0)
            {
                ProgramApprovedServiceTypes.get(servType.RCR_Program_Category_Service_Type__r.RCR_Program_Category__r.RCR_Program__r.Name).add(new ApprovedServiceTypeWrapper(true, servType));
            }
            else
            {
                ProgramApprovedServiceTypes.get(servType.RCR_Program_Category_Service_Type__r.RCR_Program_Category__r.RCR_Program__r.Name).add(new ApprovedServiceTypeWrapper(false, servType));
            }
        
            /*
            if(servType.RCR_Program_Category_Service_Type__r.RCR_Program_Category__r.RCR_Program__r.Name == 'NDA')
            {
                if(servType.RCR_Approved_Service_Type_Services__r.size() > 0)
                {
                    NDAApprovedServiceTypes.add(new ApprovedServiceTypeWrapper(true, servType));
                }
                else
                {
                    NDAApprovedServiceTypes.add(new ApprovedServiceTypeWrapper(false, servType));
                }
            }
            else if(servType.RCR_Program_Category_Service_Type__r.RCR_Program_Category__r.RCR_Program__r.Name == 'HACC')
            {
                if(servType.RCR_Approved_Service_Type_Services__r.size() > 0)
                {
                    HACCApprovedServiceTypes.add(new ApprovedServiceTypeWrapper(true, servType));
                }
                else
                {
                    HACCApprovedServiceTypes.add(new ApprovedServiceTypeWrapper(false, servType));
                }
            }*/
        }
    }


    public class ApprovedServiceTypeWrapper
    {
        public ApprovedServiceTypeWrapper(boolean pSelected, RCR_Approved_Service_Type__c pServiceType)
        {
            Selected = pSelected;
            ServiceType = pServiceType;
        }

        public boolean Selected
        {
            get;
            set;
        }

        public RCR_Approved_Service_Type__c ServiceType
        {
            get;
            set;
        }
    }

    
}