public with sharing class ParticipantFormsExtension
{
    public ParticipantFormsExtension(ApexPages.StandardController c)
    {
        record = (Plan__c)c.getRecord();
    }    

    private Plan__c record;

    public List<Form__c> Forms
    {
        get
        {
            if (Forms == null)
            {
                Forms = [SELECT Id,
                                Name,
                                RecordType.Name,
                                First_Name__c,
                                Surname__c,
                                Date_of_Birth__c,
                                CreatedDate, 
                                This_application_is_being_completed_by__c
                         FROM Form__c
                         WHERE Client__c = :record.Client__c
                         ORDER BY CreatedDate DESC];
            }
            return Forms;
        }
        set;
    }
        
    
}