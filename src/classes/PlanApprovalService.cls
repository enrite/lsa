global without sharing class PlanApprovalService {
    public PlanApprovalService() {
    }

    webservice static String selectByPlanId(string PlanId)
    {
        List<Plan_Approval__c> planApprovals = [SELECT ID,
                                                        Service_Planner_Complete__C,
                                                        Lead_Service_Planner_Complete__c,
                                                        Lead_Service_Planner_Recommendation__c,
                                                        Service_Planner_Recommendation__c,
                                                        Manager_ASD_Complete__c,
                                                        Manager_ASD_Recommendation__c
                                                FROM    Plan_Approval__c
                                                WHERE   Participant_Plan__c = :PlanId AND
                                                        (
                                                                RecordType.DeveloperName = 'Manager_ASD_Recommendation' OR
                                                                (
                                                                    RecordType.DeveloperName = 'Chief_Executive_Approval' AND
                                                                    Plan_Approval_Date__c = null
                                                                )
                                                        )];

        if(planApprovals.size()==0) 
            return null;
            
        Plan_Approval__c planApproval = planApprovals[0];
        planApproval.RecordTypeId = Utils.GetRecordTypeIdsByDeveloperName(Plan_Approval__c.getSObjectType()).get('Service_Planner_Recommendation');
        planApproval.Service_Planner_Complete__c = false;
        planApproval.Lead_Service_Planner_Complete__c = null;
        planApproval.Manager_ASD_Complete__c = null; 

        planApproval.Lead_Service_Planner_Recommendation__c = null; 
        planApproval.Service_Planner_Recommendation__c = null; 
        planApproval.Manager_ASD_Recommendation__c  = null;
        
        return JSON.serialize(planApproval);                                
    }

    webservice static String recall(string planApproval){
        Plan_Approval__c approval = (Plan_Approval__c)JSON.deserialize(planApproval,Type.forName('Plan_Approval__c'));
        
        try{
            upsert approval;
        }
        catch(Exception ex)
        {
            return ex.getMessage();
        }

        return '';
    }

    webservice static String recallApproval(String planId)
    {
        try
        {
            Id rtId = Utils.GetRecordTypeIdsByDeveloperName(Plan_Approval__c.getSObjectType()).get('Service_Planner_Recommendation');
            List<Plan_Approval__c> approvals = [SELECT ID
                                                FROM    Plan_Approval__c
                                                WHERE   Participant_Plan__c = :planId AND
                                                        Participant_Plan__c != null AND
                                                        RecordType.DeveloperName != 'Service_Planner_Recommendation' AND
                                                        Plan_Approval_Date__c = null];
            for(Plan_Approval__c approval : approvals)
            {
                approval.RecordTypeId = rtId;
                approval.Service_Planner_Complete__c = false;
                approval.Lead_Service_Planner_Complete__c = null;
                approval.Manager_ASD_Complete__c = null;
                approval.Lead_Service_Planner_Recommendation__c = null;
                approval.Service_Planner_Recommendation__c = null;
                approval.Manager_ASD_Recommendation__c  = null;
            }
            update approvals;
        }
        catch(Exception ex)
        {
            return ex.getMessage();
        }
        return '';
    }
}