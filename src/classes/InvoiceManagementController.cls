public with sharing class InvoiceManagementController extends A2HCPageBase
{ 
    private transient List<Email_To_Invoice__c> tEmailInvoices;
    private transient List<Adhoc_Payment__c> tAdhocPayments;
    private transient List<RCR_Invoice__c> tActivityStatements;
   // private transient List<SelectOption> tAdhocParticipants;

    public String SelectedStatus 
    {
        get;
        set;
    }

    public String SelectedUser
    {
        get;
        set;
    }

    public String SelectedParticipant 
    {
        get;
        set;
    }

    public String DateFrom
    {
        get
        {
            if(DateFrom == null)
            {
                DateFrom = getFinYrDate('Start');
                System.debug('DateFrom: ' + DateFrom);
            }
            return DateFrom;
        }
        set;
    }

    public String DateTo
    {
        get
        {
            if(DateTo == null)
            {
                DateTo = getFinYrDate('End');
                System.debug('DateTo: ' + DateTo);
            }
            return DateTo;
        }
        set;
    }

    public List<Email_To_Invoice__c> EmailInvoices 
    {
        get 
        {
            if(tEmailInvoices== null)
            {
                tEmailInvoices = [SELECT ID,
                                         Name,
                                         CreatedDate,
                                         Email_Subject__c,
                                         RecordType.Name,
                                         Account__r.Name,
                                         Statement_Number__c,
                                         Adhoc_Payment_ID__c
                                FROM    Email_To_Invoice__c
                                WHERE   Processed__c = false 
                                ORDER BY CreatedDate];
            }
            return tEmailInvoices;
        }
    }


    public List<RCR_Invoice__c> ActivityStatements 
    {
        get 
        {
            if(tActivityStatements == null)
            {
                String status = getParameter('status');
                String userId = getParameter('user');
                String fromDate = getParameter('fromDate');
                String toDate = getParameter('toDate');

                //To handle the page entry and default the dates
                if(status == null && userId == null && fromDate == null && toDate == null)
                {
                    fromDate = getFinYrDate('Start');
                    toDate = getFinYrDate('End');
                }

                system.debug('fromDate: ' + fromDate);
                system.debug('toDate: ' + toDate);

                Date startDate = A2HCUtilities.tryParseDate(fromDate);
                Date endDate = A2HCUtilities.tryParseDate(toDate);

                loadStatements(status,userId,startDate,endDate);
            }
            return tActivityStatements;
        }
    }

    public List<Adhoc_Payment__c> AdhocPayments 
    {
        get 
        {
            if(tAdhocPayments == null)
            {
                String participantId = getParameter('partId');
                tAdhocPayments = [SELECT ID,
                                        Name,
                                        Participant__c,
                                        Participant__r.Name,
                                        Date_Of_Transaction__c,
                                        Number_of_Open_Tasks__c,
                                        Total_Amount__c,
                                        Days_in_Processing__c,
                                        Approval_Process_Step__c,
                                        Comment_for_Aged_Invoices__c,
                                        Account__r.Name                                        
                                FROM    Adhoc_Payment__c
                                WHERE   Approved__c = false AND 
                                        (Name LIKE :(String.isBlank(participantId) ? '%' : '~') OR 
                                        Participant__c = :participantId)
                                ORDER BY Date_Of_Transaction__c];

            }
            return tAdhocPayments;
        }
    }

   

    public List<SelectOption> InvoiceStatuses 
    {
        get 
        {
            if(InvoiceStatuses == null)
            {
                InvoiceStatuses = new List<SelectOption>();
                InvoiceStatuses.add(new SelectOption('', '-- All --'));
                InvoiceStatuses.add(new SelectOption('Loaded', 'Loaded'));
                InvoiceStatuses.add(new SelectOption('Reconciliation In Progress', 'Reconciliation In Progress'));
                InvoiceStatuses.add(new SelectOption('Reconciled - No Errors',  'Reconciled - No Errors'));
                InvoiceStatuses.add(new SelectOption('Reconciled - Errors', 'Reconciled - Errors'));
                InvoiceStatuses.add(new SelectOption('Submitted', 'Submitted'));
                InvoiceStatuses.add(new SelectOption('Pending Approval', 'Pending Approval'));
                InvoiceStatuses.add(new SelectOption('Approved', 'Approved'));
                InvoiceStatuses.add(new SelectOption('Rejected', 'Rejected'));
            }
            return InvoiceStatuses;
        }
        set;
    }

    public List<SelectOption> Users
    {
        get
        {
            if(Users == null)
            {
                Users = new List<SelectOption>();
                for(User u : [SELECT Id, Name FROM User WHERE UserRole.Name IN ('Finance','Program Support') ORDER BY Name])
                {
                    Users.add(new SelectOption(u.Id, u.Name));
                }
                Users.add(0, new SelectOption('', '-- All --'));
            }
            return Users;
        }
        set;
    }

    public List<SelectOption> AdhocParticipants 
    {
        get 
        {
            if(AdhocParticipants == null)
            {
                AdhocParticipants = new List<SelectOption>();
                Set<ID> participantIds = new Set<Id>();
                for(Adhoc_Payment__c ap : AdhocPayments)
                {
                    if(participantIds.contains(ap.Participant__c))
                    {
                        continue;
                    }
                    participantIds.add(ap.Participant__c);
                    AdhocParticipants.add(new SelectOption(ap.Participant__c, ap.Participant__r.Name));
                }
                AdhocParticipants.add(0, new SelectOption('', '-- All --'));
            }
            return AdhocParticipants;
        }
        set;
    }

    public void resetAdhocPayments()
    {
    }

    private String getFinYrDate(String startOrEnd)
    {
        String finYrDate;
        Date finYrDateBoundary;

        if(startOrEnd == 'Start')
        {
            finYrDateBoundary = A2HCUtilities.getStartOfFinancialYear(Date.Today());
        }
        else
        {
            finYrDateBoundary = A2HCUtilities.getEndOfFinancialYear(Date.Today());
        }

        finYrDate = finYrDateBoundary.format();
        if(finYrDate.length() == 9)
        {
            finYrDate = '0' + finYrDate;
        }

        return finYrDate;
    }
    private void loadStatements(String status, String userId, Date fromDate, Date toDate)
    {
        // Yeah its ugly but it works
        if(!String.isBlank(userId))
        {
            if(fromDate == null && toDate == null)
            {
                tActivityStatements = [SELECT   ID,
                        Name,
                        Date__c,
                        Status__c,
                        Invoice_Number__c,
                        Account__r.Name,
                        Total__c,
                        Days_in_Processing__c,
                        Number_of_Open_Tasks__c,
                        Comment_for_Aged_Invoices__c
                FROM    RCR_Invoice__c
                WHERE   Status__c != :APS_PicklistValues.RCRInvoice_Status_PaymentProcessed AND
                Status__c LIKE :(String.isBlank(status) ? '%' : status) AND
                createdById = :userId
                ORDER BY Date__c];
            }
            else
            {
                tActivityStatements = [SELECT   ID,
                        Name,
                        Date__c,
                        Status__c,
                        Invoice_Number__c,
                        Account__r.Name,
                        Total__c,
                        Days_in_Processing__c,
                        Number_of_Open_Tasks__c,
                        Comment_for_Aged_Invoices__c
                FROM    RCR_Invoice__c
                WHERE   Status__c != :APS_PicklistValues.RCRInvoice_Status_PaymentProcessed AND
                Status__c LIKE :(String.isBlank(status) ? '%' : status) AND
                createdById = :userId AND
                (Invoice_Received_Date__c >= :fromDate AND Invoice_Received_Date__c <= :toDate)
                ORDER BY Date__c];
            }

        }
        else
        {
            if(fromDate == null && toDate == null)
            {
                tActivityStatements = [SELECT   ID,
                        Name,
                        Date__c,
                        Status__c,
                        Invoice_Number__c,
                        Account__r.Name,
                        Total__c,
                        Days_in_Processing__c,
                        Number_of_Open_Tasks__c,
                        Comment_for_Aged_Invoices__c
                FROM    RCR_Invoice__c
                WHERE   Status__c != :APS_PicklistValues.RCRInvoice_Status_PaymentProcessed AND
                Status__c LIKE :(String.isBlank(status) ? '%' : status)
                ORDER BY Date__c];
            }
            else
            {
                tActivityStatements = [SELECT   ID,
                        Name,
                        Date__c,
                        Status__c,
                        Invoice_Number__c,
                        Account__r.Name,
                        Total__c,
                        Days_in_Processing__c,
                        Number_of_Open_Tasks__c,
                        Comment_for_Aged_Invoices__c
                FROM    RCR_Invoice__c
                WHERE   Status__c != :APS_PicklistValues.RCRInvoice_Status_PaymentProcessed AND
                Status__c LIKE :(String.isBlank(status) ? '%' : status) AND
                (Invoice_Received_Date__c >= :fromDate AND Invoice_Received_Date__c <= :toDate)
                ORDER BY Date__c];
            }

        }

    }
}