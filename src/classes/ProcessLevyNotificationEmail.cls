global class ProcessLevyNotificationEmail implements Messaging.InboundEmailHandler 
{ 
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) 
    {             
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
                        
        String[] split = email.htmlBody.split('<tbody>', -1);
        
        for (Integer i = 1; i < split.size(); i = i + 2)
        {
            String[] secondSplit = split[i].split('</tbody>', -1);
                                            
            Dom.Document doc = new Dom.Document();
            doc.load('<xml>' + secondSplit[0].replace('nowrap', '') + '</xml>');
            
            Payment_Collected__c pc = new Payment_Collected__c();
                        
            Integer datePosition;   
            Integer drCrPosition;     
            Integer amountPosition;            
            
            for (Dom.XMLNode tr : doc.getRootElement().getChildElements())
            {
                if (tr.getName() == 'tr')
                {                    
                    Integer j = 0;
                    for (Dom.XMLNode td : tr.getChildElements())
                    {
                        if (td.getName() == 'td')
                        {
                            for (Dom.XMLNode p : td.getChildElements())
                            {
                                if (p.getName() == 'p')
                                {
                                    for (Dom.XMLNode span : p.getChildElements())
                                    { 
                                        if (span.getName() == 'span')
                                        {
                                            if (datePosition == j)
                                            {
                                                try
                                                {
                                                    pc.Effective_Date__c = Date.parse(span.getText());
                                                }
                                                catch(Exception e) {}
                                            }
                                            else if (drCrPosition == j)
                                            {
                                                pc.Debit_Credit__c = span.getText();
                                            }
                                            else if (amountPosition == j)
                                            {
                                                try
                                                {
                                                    pc.Amount__c = Decimal.valueOf(span.getText().replace(',', ''));
                                                }
                                                catch(Exception e) {}    
                                            }
                                            else if (span.getText() == 'Effective Date')
                                            {
                                                datePosition = j;
                                            }
                                            else if (span.getText() == 'DR/CR')
                                            {
                                                drCrPosition = j;
                                            }
                                            else if (span.getText() == 'Amount')
                                            {
                                                amountPosition = j;
                                            }  
                                        }
                                    }
                                }
                            }    
                        }
                        j++;
                    }                                        
                }
            }
            
            // if we have a valid payment collected record then process it
            if (pc.Effective_Date__c != null
                && pc.Debit_Credit__c != null
                && pc.Amount__c != null)
            {
                // see if an existing record exists
                for (Payment_Collected__c exist : [SELECT Id,
                                                          Effective_Date__c,
                                                          Debit_Credit__c,
                                                          Amount__c
                                                   FROM Payment_Collected__c
                                                   WHERE Effective_Date__c = :pc.Effective_Date__c])
                {
                    exist.Effective_Date__c = pc.Effective_Date__c;
                    exist.Debit_Credit__c = pc.Debit_Credit__c;
                    exist.Amount__c = pc.Amount__c;
                    pc = exist;
                }                                                   
                
                upsert pc;
            }                
        
            /*
            if (ss.replace(',', '').replace('.', '').isNumeric())
            {
                //system.debug('debug3:' + ss);
            } */               
        }
        
        /*
        String[] split = email.htmlBody.split('>', -1);
        
        for (String s : split)
        {
            String[] secondSplit = s.split('<', -1);
            
            for (String ss : secondSplit)
            {
                //system.debug('debug2:' + ss);
            
                if (ss.replace(',', '').replace('.', '').isNumeric())
                {
                    //system.debug('debug3:' + ss);
                }    
            }
        }*/
                        
        result.success = true;
        
        return result; 
    }     
}