public with sharing class EmailToInvoiceExtension extends A2HCPageBase
{
    public EmailToInvoiceExtension(ApexPages.StandardController c)
    {
        if(!Test.isRunningTest())
        {
            c.addFields(new String[]{Email_To_Invoice__c.Process__c.getDescribe().getName(),
                    Email_To_Invoice__c.Processed__c.getDescribe().getName(),
                    Email_To_Invoice__c.Service_Order__c.getDescribe().getName()});
        }
        controller = c;
    }

    private ApexPages.StandardController controller;

    public List<SelectOption> DocumentTypes
    {
        get 
        {
            if(DocumentTypes == null)
            {
                DocumentTypes = new List<SelectOption>();
                Schema.DescribeFieldResult dfr = Email_To_Invoice__c.Document_Type__c.getDescribe();
                for(Schema.PicklistEntry ple : dfr.getPicklistValues())
                {
                    DocumentTypes.add(new SelectOption(ple.getValue(), ple.getLabel()));
                }
            }
            return DocumentTypes;
        }
        private set;
    }

    public PageReference saveOverride()
    {
       Savepoint sp = Database.setSavepoint();
        try 
        {	    
            Email_To_Invoice__c e2I = (Email_To_Invoice__c)controller.getRecord();
            Boolean valid = true;
            valid = isRequiredFieldValid(e2I.Document_Type__c, 'Document Type', valid);
            if(e2I.Document_Type__c == 'Invoice/Activity Statement')
            {
                valid = isRequiredFieldValid(e2I.Account__c, 'Service Provider', valid);
                valid = isRequiredFieldValid(e2I.Date__c, 'Date', valid);
                valid = isRequiredFieldValid(e2I.Statement_Number__c, 'Statement Number', valid);
                valid = isRequiredFieldValid(e2I.Start_Date__c, 'Start Date', valid);
                valid = isRequiredFieldValid(e2I.End_Date__c, 'End Date', valid);
            }
            else if(e2I.Document_Type__c == 'Adhoc Payment')
            {
                valid = isRequiredFieldValid(e2I.Account__c, 'Account', valid);
                valid = isRequiredFieldValid(e2I.Participant__c, 'Participant', valid);
                valid = isRequiredFieldValid(e2I.Adhoc_Payment_ID__c, 'Adhoc Payment ID', valid);
                valid = isRequiredFieldValid(e2I.Date_of_Transaction__c, 'Date of transaction', valid);
            }
            if(!valid)
            {
                return null;
            }
            controller.save();
            
            ID newRecordId = null;
            if(e2I.Process__c && !e2I.Processed__c)
            {
                if(e2I.Document_Type__c == 'Invoice/Activity Statement')
                {
                    RCR_Invoice__c invoice = new RCR_Invoice__c();
                    invoice.Account__c = e2I.Account__c;
                    invoice.Service_Order__c = e2I.Service_Order__c;
                    invoice.Date__c = e2I.Date__c;
                    invoice.Invoice_Number__c = e2I.Statement_Number__c;
                    invoice.Start_Date__c = e2I.Start_Date__c;
                    invoice.End_Date__c = e2I.End_Date__c;
                    invoice.Email_To_Invoice__c = e2I.ID;
                    insert invoice;
                    newRecordId = invoice.ID;
                }
                else if(e2I.Document_Type__c == 'Adhoc Payment')
                {
                    Adhoc_Payment__c adhocPayment = new Adhoc_Payment__c();
                    adhocPayment.Name = e2I.Adhoc_Payment_ID__c;
                    adhocPayment.Account__c = e2I.Account__c;
                    adhocPayment.Participant__c = e2I.Participant__c;
                    adhocPayment.Date_of_Transaction__c = e2I.Date_of_Transaction__c;
                    adhocPayment.Email_To_Invoice__c = e2I.ID;
                    insert adhocPayment;
                    newRecordId = adhocPayment.ID;
                }
                else
                {
                    throw new A2HCException('Document type not found');
                }
                List<Attachment> e2IAttachments = [SELECT ID,
                                                            Name,
                                                            Description,
                                                            Body,
                                                            ContentType
                                                    FROM    Attachment
                                                    WHERE   ParentID = :e2I.ID];
                List<Attachment> newAttachments = e2IAttachments.deepClone();
                for(Attachment att : newAttachments)
                {
                    att.ParentID = newRecordId;
                }
                insert newAttachments;
                List<Task> tasks = [SELECT ID 
                                    FROM    Task 
                                    WHERE   WhatID = :e2I.ID AND 
                                            Status != 'Completed'];
                for(Task t : tasks)
                {
                    t.Status = 'Completed';
                }
                update tasks;
                e2I.Processed__c = true;
            }
            String rtSuffix = '';
            if(e2I.Processed__c)
            {
                rtSuffix = ' Read Only';
            }
            e2I.RecordTypeId = A2HCUtilities.getRecordType('Email_To_Invoice__c', e2I.Document_Type__c + rtSuffix).ID;
            update e2I;

            if(newRecordId == null)
            {
                return null;
            }
            PageReference pg = new PageReference('/' + newRecordId);
            return pg;
        }
        catch(Exception e)
        {
            return A2HCException.formatExceptionAndRollback(sp, e);
        }
    }

    public static void create30DayTasks(Set<ID> relatedToIDs)
    {
        create30DayTasks(relatedToIDs, null);
    }

    public static void create30DayTasks(Set<ID> relatedToIDs, List<User> groupUsers)
    {
        if(relatedToIDs.isEmpty())
        {
            return;
        }
        List<Task> tasks = new List<Task>();
        if(groupUsers == null)
        {
            groupUsers = [SELECT     ID
                            FROM    User
                            WHERE   User.IsActive = true AND
                            ID IN (SELECT   UserOrGroupId
                                    FROM    GroupMember
                                    WHERE   Group.DeveloperName = 'Senior_Accountant')];
        }
        Task baseTask = new Task();
        baseTask.Status = 'Not Started';
        baseTask.Subject = 'Incomplete Invoice over 30 days';
        baseTask.Priority = 'Normal';
        baseTask.ActivityDate = Date.today();
        for(ID relatedToId : relatedToIDs)
        {
            for(User u : groupUsers)
            {
                Task t = baseTask.clone(false, true);
                t.WhatId = relatedToId;
                t.OwnerId = u.ID;
                tasks.add(t);
            }
            
        }
        insert tasks;
    }
}