public with sharing class ParticipantPortalPlanController
{

    private Id contactId
    {
      get{
        if(contactId==null)
        {
          User u = [SELECT Id,
                     ContactId
                     FROM User
                     WHERE Id = :UserInfo.getUserId() LIMIT 1];
          contactId = u.ContactId;
        }return contactId;
      }set;
    }

    public PageReference Redirect()
    {
        try
        {                                
            for (Plan__c p : [SELECT Id                                                
                              FROM Plan__c
                              WHERE Client__r.Participant_Portal_Contact__c = :contactId
                              AND RecordType.DeveloperName = 'Discharge_Plan'
                              ORDER BY CreatedDate DESC
                              LIMIT 1])
            {
                PageReference ref = Page.ParticipantPortalDischargePlan;
                ref.getParameters().put('id', p.Id);
                
                return ref;
            }                                         
                    
            return A2HCException.formatException('No discharge plan found');                    
        }
        catch(Exception e)
        {
            return A2HCException.formatException(e);
        }                                               
    }

    public Plan__c MyPlan
    {
      get{
        if(MyPlan == null)
        {
          List<Plan__c> plans = [SELECT Id                                                
                                FROM    Plan__c
                                WHERE   Client__r.Participant_Portal_Contact__c = :contactId
                                AND     RecordType.DeveloperName = 'MyPlan'
                                And     Plan_Status__c = 'Approved'
                                And     Plan_Start_Date__c <= Today and (Plan_End_Date__c >= Today or Plan_End_Date__c = null)
                                ORDER BY CreatedDate DESC];

          if(plans.size() == 0)
          {
            return null;
          }
          MyPlan = plans[0];
        }
        return MyPlan;
      }set;
    }

    public List<Plan__c> MyPlans
    {
      get
      {
        if(MyPlans==null)
        {
            MyPlans = [SELECT Id,
                            Name,
                            Plan_Status__c,
                            Approval_Status__c,
                            Plan_Start_Date__c,
                            Plan_End_Date__c,
                            Service_Planner_Comments__c
                        FROM Plan__c
                        WHERE Client__r.Participant_Portal_Contact__c = :contactId
                              AND RecordType.DeveloperName <> 'Discharge_Plan'
                              ORDER BY CreatedDate DESC];
        }
        return MyPlans;
      }
      set;
    }
}