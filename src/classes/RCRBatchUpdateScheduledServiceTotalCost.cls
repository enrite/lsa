global class RCRBatchUpdateScheduledServiceTotalCost 
        implements Database.Batchable<sObject>, Database.Stateful
{
    global RCRBatchUpdateScheduledServiceTotalCost()
    {
        for(RCR_Service_Rate_Audit__c ar : [SELECT  Recalculation_ID__c
                                            FROM    RCR_Service_Rate_Audit__c
                                            WHERE   Recalculation_ID__c != null
                                            ORDER BY    Recalculation_ID__c desc
                                            LIMIT 1])
        {   
            batchId = ar.Recalculation_ID__c;
        }

        batchId = batchId == null ? 1 : ++batchId;
        startTime = Datetime.now(); 
    }

    private Datetime startTime;
    private decimal batchId = null;
    public Mode RunMode
    {
        get;
        set;
    }
    
    public enum Mode
    {
        ProposedRateUpdateByNightProcess,
        SingleRateUpdate,
        MultipleRateUpdate,
        ServiceOrderRollover,
        SpecificScheduledServices,
        SpecificServiceOrder
    }
    
    @TestVisible
    private List<string> Errors
    {
        get
        {
            if(Errors == null)
            {
                Errors = new List<string>();
            }
            return Errors;
        }
        set; 
    }
            
    @TestVisible
    private Map<string, RCR_Service_Rate__c> RatesByServiceID
    {
        get
        {
            if(RatesByServiceID == null)
            {
                RatesByServiceID = new Map<string, RCR_Service_Rate__c>();
            }
            return RatesByServiceID;
        }
        set;
    }

    global RCR_Service_Rate__c Rate
    {
        get;
        set;
    }
    
    global decimal OriginalStandardRate
    {
        get;
        set;
    }
    
    global decimal OriginalPublicHolidayRate
    {
        get;
        set;
    }
    
    global List<RCR_Service_Rate__c> Rates
    {
        get
        {
            if(Rates == null)
            {
                Rates = new List<RCR_Service_Rate__c>();
            }
            return Rates;
        }
        set;
    }
    
    global boolean ProposedRateActivation
    {
        get
        {
            if(ProposedRateActivation == null)
            {
                ProposedRateActivation = false;
            }
            return ProposedRateActivation;
        }
        set;
    }
    
    global List<Id> CssIDs
    {
        get
        {
            if(cssIDs == null)
            {
                cssIDs = new List<Id>();
            }
            return cssIDs;
        }
        set;
    }
    
    global Map<Id, List<RCR_Service_Rate_Audit__c>> RateAuditRecordsByScheduledService
    {
        get
        {
            if(RateAuditRecordsByScheduledService == null)
            {
                RateAuditRecordsByScheduledService = new Map<id, List<RCR_Service_Rate_Audit__c>>();
            }
            return RateAuditRecordsByScheduledService;
        }
        set;
    }
    
    global String ScheduleID
    {
        get
        {
            if(ScheduleID != null && ScheduleID.length() > 15)
            {
                return ScheduleID.substring(0, 15);
            }
            return ScheduleID;
        }
        set;
    }

    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {

        List<Id> scheduledServiceIDs = new List<Id>();
        
        if(runMode == Mode.ProposedRateUpdateByNightProcess)
        {    
            //***********************************************
            //*     Called from Proposed Rate Night Scheduled Job     *
            //***********************************************                
            scheduledServiceIDs = new List<Id>(RateAuditRecordsByScheduledService.keySet());
        }
        else if(runMode == Mode.SingleRateUpdate)
        {
            //***********************************************
            //*     Called single rate updated     *
            //***********************************************
            RatesByServiceID.put(Rate.RCR_Service__c, Rate);
            
            for(RCR_Contract_Scheduled_Service__c css : [SELECT ID
                                                          FROM   RCR_Contract_Scheduled_Service__c r
                                                          WHERE  r.Use_Negotiated_Rates__c = false
                                                          AND    r.RCR_Service__c = :Rate.RCR_Service__c
                                                          AND    ((r.Start_Date__c <= :Rate.End_Date__c AND r.End_Date__c >= :Rate.Start_Date__c)
                                                          OR      (r.Start_Date__c >= :Rate.Start_Date__c AND r.End_Date__c <= :Rate.End_Date__c))])
            {
                scheduledServiceIDs.add(css.ID);
            }
        }
        else if(runMode == Mode.MultipleRateUpdate)
        {
            //************************************************
            //*     Called when multiple rates uploaded     *
            //************************************************      
            Set<Id> serviceIDs = new Set<Id>();
            date minStartDate;
            date maxEndDate;
                     
            minStartDate = Rates[0].Start_Date__c;
            maxEndDate = Rates[0].End_Date__c;
                     
            for(RCR_Service_Rate__c r : Rates)
            {
                serviceIDs.add(r.RCR_Service__c);
                RatesByServiceID.put(r.RCR_Service__c, r);
                   
                if(r.Start_Date__c < minStartDate)
                {
                    minStartDate = r.Start_Date__c;
                }
                if(r.End_Date__c > maxEndDate)
                {
                    maxEndDate = r.End_Date__c;
                }
            }
    
            for(RCR_Contract_Scheduled_Service__c css : [SELECT ID
                                                         FROM   RCR_Contract_Scheduled_Service__c r
                                                         WHERE  r.RCR_Service__c IN :serviceIDs
                                                         AND    ((r.Start_Date__c <= :maxEndDate AND r.End_Date__c >= :minStartDate)
                                                         OR      (r.Start_Date__c >= :minStartDate AND r.End_Date__c <= :maxEndDate))])
            {
                scheduledServiceIDs.add(css.ID);
            }
        }
        else if(runMode == Mode.ServiceOrderRollover)
        {
            //***************************************************                
            //*     Called for bulk rollover of Service Orders  *
            //***************************************************
            for(RCR_Contract_Scheduled_Service__c css : [SELECT ID
                                                         FROM   RCR_Contract_Scheduled_Service__c r
                                                         WHERE  r.Use_Negotiated_Rates__c = false
                                                         AND    RCR_Contract__r.Schedule__c = :ScheduleID
                                                         AND    RCR_Contract__r.Schedule__c != null])
            {
                scheduledServiceIDs.add(css.ID);
            }      
        } 
        else if(runMode == Mode.SpecificScheduledServices)
        {
            scheduledServiceIDs = cssIDs;
        }     
        else if(runMode == Mode.SpecificServiceOrder)
        {
            for(RCR_Contract_Scheduled_Service__c css : [SELECT     ID
                                                        FROM        RCR_Contract_Scheduled_Service__c
                                                        WHERE       RCR_Contract__r.Schedule__c = :ScheduleID
                                                         AND        RCR_Contract__r.Schedule__c != null])
            {
                scheduledServiceIDs.add(css.ID);
            }
        }
                                                                                  
        return Database.getQueryLocator([SELECT r.Name,
                                                r.Id,
                                                r.End_Date__c,
                                                r.Original_End_Date__c, 
                                                r.Flexibility__c,
                                                r.Public_holidays_not_allowed__c,
                                                r.Comment__c,
                                                Exception_Count__c,
                                                r.Total_Cost__c,
                                                r.Use_Negotiated_Rates__c,
                                                r.Negotiated_Rate_Standard__c,
                                                r.Negotiated_Rate_Public_Holidays__c,
                                                r.Start_Date__c,
                                                r.RCR_Service__c,
                                                r.RCR_Service__r.Account__c,
                                                r.RCR_Sub_Program__c,
                                                r.RCR_Contract__c,
                                                r.RCR_Contract__r.Client__c,
                                                r.RCR_Contract__r.Account__c,
                                                r.RCR_Contract__r.Level__c,
                                                r.RCR_Contract__r.Name,
//                                                r.RCR_Contract__r.RCR_CBMS_Contract__c,
//                                                r.RCR_Contract__r.Cost_Difference_RCR_to_CBMS__c,
//                                                r.RCR_Contract__r.CBMS_Service_Order_Value_at_Rate_Update__c,
                                                r.RCR_Contract__r.Total_Service_Order_Cost__c,
//                                                r.RCR_Contract__r.RCR_CBMS_Contract__r.Contract_Total__c,
                                                (SELECT Date__c,
                                                        Public_Holiday_Quantity__c,
                                                        Standard_Quantity__c,
                                                        RCR_Contract_Scheduled_Service__c
                                                FROM    RCR_Scheduled_Service_Exceptions__r
                                                ORDER BY Date__c)
                                          FROM  RCR_Contract_Scheduled_Service__c r
                                          WHERE ID IN :scheduledServiceIDs]);
    }


    global void execute(Database.BatchableContext batch, List<RCR_Contract_Scheduled_Service__c> scheduledServices)
    {
        for(RCR_Contract_Scheduled_Service__c css : scheduledServices)
        {                      
            try
            {
                processSchedService(css);
            }
            catch(Exception ex)
            {
                if(runMode == Mode.SpecificScheduledServices || runMode == Mode.SpecificServiceOrder)
                {
                    throw new A2HCException(ex);
                }
                Errors.add(css.Name + ' was not recalculated. Error: ' + ex.getMessage());
            }
        }
    }
    
    global void finish(Database.BatchableContext info)
    {
        if(Rate != null)
        { 
            Rate.Recalculate_Service_Order_Value__c = false;
            update Rate;
            if(!ProposedRateActivation)
            {
                sendBatchCompletedEmail();
            }
        }
        else if(Rates.size() > 0)
        {
            for(RCR_Service_Rate__c r : Rates)
            {
                r.Recalculate_Service_Order_Value__c = false;
            }
            update Rates;
            if(!ProposedRateActivation)
            {
                sendBatchCompletedEmail();
            }
        }
        BatchUtilities.cleanUpSchedule(ScheduleID); 
        if(ScheduleID != null)
        {
            List<RCR_Contract__c> serviceOrders = [SELECT   ID
                                                    FROM    RCR_Contract__c
                                                    WHERE   Schedule__c = :ScheduleID];
            for(RCR_Contract__c so : serviceOrders)
            {
                so.Schedule__c = null;
            }
            update serviceOrders;
        }                 
    }
    
    public void processSchedService(RCR_Contract_Scheduled_Service__c css)
    {                                                       
        List<RCR_Service_Rate_Audit__c> auditRecords = new List<RCR_Service_Rate_Audit__c>();
        if(ScheduleID != null)
        {
            // called due to change to parent CBMS contract so reset schedules
            List<Schedule__c> schedules = [SELECT   ID
                                            FROM    Schedule__c
                                            WHERE   Name = :css.ID];
            for(Schedule__c schedule : schedules)
            {
                schedule.End_Date__c = css.End_Date__c;
            }
            update schedules;                       
        }   

        if(runMode == Mode.ProposedRateUpdateByNightProcess)
        {           
            // Get existing Audit Record.
            auditRecords = RateAuditRecordsByScheduledService.get(css.Id);
        }
        else
        {
            // Create new Audit Record.
            RCR_Service_Rate_Audit__c auditRecord = new RCR_Service_Rate_Audit__c(RCR_Contract_Scheduled_Service__c = css.Id, RCR_Service_Order__c = css.RCR_Contract__c);
            auditRecord.Datestamp__c = startTime;
            auditRecord.Recalculation_ID__c = batchid.intValue();
            auditRecord.Scheduled_Service_Value_Before__c = css.Total_Cost__c;
            auditRecord.Service_Order_Value_Before__c = css.RCR_Contract__r.Total_Service_Order_Cost__c;
            auditRecord.User__c = UserInfo.getUserId();
        
            if(OriginalStandardRate != null && OriginalPublicHolidayRate != null)
            {
                auditRecord.Original_Standard_Rate__c = OriginalStandardRate;
                auditRecord.Original_Public_Holiday_Rate__c = OriginalPublicHolidayRate;
            }

            if(RatesByServiceID.containsKey(css.RCR_Service__c))
            {
                auditRecord.RCR_Service_Rate__c = RatesByServiceID.get(css.RCR_Service__c).Id;
                auditRecord.New_Standard_Rate__c = RatesByServiceID.get(css.RCR_Service__c).Rate__c;
                auditRecord.New_Public_Holiday_Rate__c = RatesByServiceID.get(css.RCR_Service__c).Public_Holiday_Rate__c;
            }
            auditRecords.add(auditRecord);
        }

        for(RCR_Service_Rate_Audit__c record : auditRecords)
        {
            record.Proposed_Rate_Activation__c = ProposedRateActivation;
        }
        
        if(runMode == Mode.ProposedRateUpdateByNightProcess || 
            runMode ==  Mode.SpecificScheduledServices ||
            runMode ==  Mode.SpecificServiceOrder //||
//           !(ProposedRateActivation &&
//             css.RCR_Contract__r.RCR_CBMS_Contract__c != null &&
//             css.RCR_Contract__r.Total_Service_Order_Cost__c == css.RCR_Contract__r.RCR_CBMS_Contract__r.Contract_Total__c)
                )
        {       
            calculateNewValues(css, auditRecords);
        }
        upsert auditRecords;
        setServiceOrderValueAtRateUpdate(css);
    }
    
    @TestVisible
    private void setServiceOrderValueAtRateUpdate(RCR_Contract_Scheduled_Service__c css)
    {
        boolean updateServiceOrder = false;
        RCR_Contract__c serviceOrder = new RCR_Contract__c(ID = css.RCR_Contract__c);
        
        if(ProposedRateActivation)
        {
            if(runMode == Mode.ProposedRateUpdateByNightProcess)
            {
//                if(css.RCR_Contract__r.Total_Service_Order_Cost__c != css.RCR_Contract__r.CBMS_Service_Order_Value_at_Rate_Update__c ||
//                    css.RCR_Contract__r.RCR_CBMS_Contract__r.Contract_Total__c != css.RCR_Contract__r.CBMS_Service_Order_Value_at_Rate_Update__c)
//                {
//                    serviceOrder.CBMS_Service_Order_Value_at_Rate_Update__c = null;
//                    updateServiceOrder = true;
//                }
            }
            else
            {
                serviceOrder.New_Service_Rate_Variation_Required__c = true;
                updateServiceOrder = true;
//                if(css.RCR_Contract__r.RCR_CBMS_Contract__c != null && css.RCR_Contract__r.Total_Service_Order_Cost__c == css.RCR_Contract__r.RCR_CBMS_Contract__r.Contract_Total__c)
//                {
//                    if(css.RCR_Contract__r.CBMS_Service_Order_Value_at_Rate_Update__c == null)
//                    {
//                        serviceOrder.CBMS_Service_Order_Value_at_Rate_Update__c = css.RCR_Contract__r.Total_Service_Order_Cost__c;
//                    }
//                }
            }
        }
        if(updateServiceOrder)
        {
            update serviceOrder;
        }
    }
    
    private void calculateNewValues(RCR_Contract_Scheduled_Service__c css, List<RCR_Service_Rate_Audit__c> auditRecords)
    {
        RCR_Service__c service = RCRServiceExtension.getRCRService(css.RCR_Service__c);
        Map<decimal, decimal> t = RCRInvoiceWebService.getTotals(service, css, true);
        decimal totalCost = 0.0;
        decimal totalQuantity = 0.0;

        for(decimal d : t.keySet())
        {
            totalCost = d;
            totalQuantity = t.get(d);
        } 

        RCRScheduledServiceExtension ext = new RCRScheduledServiceExtension(false);
        ext.Service = service;
        css.Total_Cost__c = totalCost;
        css.Total_Quantity__c = totalQuantity;
        ext.saveScheduledService(css);

        for(RCR_Service_Rate_Audit__c auditRecord : auditRecords)
        {
            auditRecord.Scheduled_Service_Value_After__c = totalCost;
            auditRecord.Service_Order_Value_After__c = RCRContractExtension.getContract(css.RCR_Contract__c).Total_Service_Order_Cost__c;
            auditRecord.Costs_Recalculated__c = startTime;
        }
    }

    private void sendBatchCompletedEmail()
    {
        string rateURL = '';         
        
        if(Rate == null && Rates.size() > 0)
        {
            rateURL = Rates[0].Id;
        }
        
        String fullRecordURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + rateURL;
        String msg = 'The Service Rates have been updated. The cost of each Scheduled Service affected by these rate changes have been recalculated.' +
                        '<br/><br/>Please logon to <a href=\"' + fullRecordURL + '\">RCR</a> to view the results of the recalculation.';
        if(Errors.size() > 0)
        {
            msg += '<br/><br/>The following Scheduled Services encountered errors:<br/>';
        }               
        for(String error : Errors)  
        {
            msg += error + '<br/>';
        }           
        EmailManager.sendHTMLMessage(ID.valueOf(UserInfo.getUserId()),  
                                        'Service Rates Updated',
                                        msg, 
                                        'Results from Scheduled Service Cost Recalculation Request', 
                                        'noreply@eReferrals.dfc.sa.gov.au');    
    }   
}