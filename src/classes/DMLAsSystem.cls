public without sharing class DMLAsSystem 
{
	// perform DML without restriction of sharing rules
    public static String[] updateRecords(List<sObject> records, Boolean allOrNone)
    {
    	return A2HCException.getExceptionMessages(Database.update(records, allOrNone));
    }
    
    public static void updateRecord(sObject record)
    {
    	update record;
    }

    public static void upsertRecords(List<sObject> records)
    {
    	upsert records;
    }
    
    public static void updateRecords(List<sObject> records)
    {
    	update records;
    }
    
    public static String[] insertRecords(List<sObject> records, Boolean allOrNone)
    {
    	return A2HCException.getExceptionMessages(Database.insert(records, allOrNone));
    }
    
    public static void deleteRecords(List<sObject> records)
    {
    	delete records;
    }
}