public with sharing class RCRMasterpieceBatchReport extends A2HCPageBase
{
    public RCRMasterpieceBatchReport(ApexPages.StandardController ctrlr)
    {
        runReport(getParameter('id'));
    }
    
    public RCRMasterpieceBatchReport(ID batchID)
    {
        runReport(batchID);
    }
    
    public RCR_Invoice_Batch__c Batch 
    {
        get;
        private set;
    }

    public String BatchName
    {
        get
        {
            if(Batch == null || Batch.ID == null)
            {
                return '';
            }
            String s = Batch.Name;
            return Batch.Lantern_Pay_Batch__c ? s.replace('RCR0', 'RCRJ') : s;
        }
    }
    
    public string RunDate
    {
        get
        {
            return Date.today().format();
        }
    }
    
    public String VendorCount
    {
        get;
        private set;
    }
    
    // this is the count of RCR invoices
    public string InvoiceCount
    {
        get;
        private set;
    }
    
    // this is the count of Masterpiece invoices
    public string VoucherCount
    {
        get;
        private set;
    }
    
    @TestVisible 
    private void runReport(ID batchID)
    { 
        try
        {
            throwTestError();
            InvoiceCount = '0';
            VendorCount = '0';
            Batch = new RCR_Invoice_Batch__c();         
            
            if(batchId != null)
            {
                Batch = [SELECT id,
                                Name,
                                MP_File_Name__c,
                                Total_Ex_GST_MP__c,
                                Total_GST_MP__c,
                                Total_Inc_GST_MP__c,
                                Lantern_Pay_Batch__c
                      FROM      RCR_Invoice_Batch__c
                      WHERE     Id = :batchId];
                      
                AggregateResult ar = [SELECT    COUNT_DISTINCT(Invoice_Number__c) 	invCnt,
                                                COUNT_DISTINCT(Vendor_Code__c) 		vendorCnt, 
                                                COUNT_DISTINCT(Voucher__c) 			voucherCnt
                                      FROM      RCR_MP_Service_Order_Payment__c 
                                      WHERE     RCR_Invoice_Batch__c = :batchId];
                                      
                InvoiceCount = String.valueOf((decimal)ar.get('invCnt'));
                VendorCount = String.valueOf((decimal)ar.get('vendorCnt'));   
                VoucherCount = String.valueOf((decimal)ar.get('voucherCnt'));                                   
            }
        }
        catch(Exception ex)
        {
            A2HCException.formatException(ex);
        }
    }
}