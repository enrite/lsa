public with sharing class RCRBatchSearchExtension
    extends A2HCPageBase
{
    public RCRBatchSearchExtension(ApexPages.StandardController controller)
    {
        String batchId = getParameter('id');
        if(batchId != null)
        {
            BatchName = [SELECT Name
                        FROM    RCR_Invoice_Batch__c
                        WHERE   ID = :batchId].Name;
            doImmediateSearch();
        }
        else if(getParameter('srch') != null)
        {
             doRecentSearch();
        }
        else
        {
            Date dt = Date.today();
            FromDate = dt.addDays(-30).format();
            ToDate = dt.format();
        }
    }

    public String SelectedAccount
    {
        get;
        set;
    }

    public String SelectedProgram
    {
        get;
        set;
    }

    public String InvoiceNumber
    {
        get;
        set;
    }

    public String ContractNumber
    {
        get;
        set;
    }

    public String FromDate
    {
        get;
        set;
    }

    public String ToDate
    {
        get;
        set;
    }

    public String BatchName
    {
        get;
        set;
    }

    public String BatchNumber
    {
        get;
        set;
    }

    public String BatchDescription
    {
        get;
        set;
    }

    public Boolean LanternPay
    {
        get;
        set;
    }

    public String SearchCriteria
    {
        get
        {
            if(SearchCriteria == null)
            {
                String s = '';
                s += (A2HCUtilities.isStringNullOrEmpty(SelectedAccount) ? '' : SelectedAccount) + '~';
                s += (A2HCUtilities.isStringNullOrEmpty(SelectedProgram) ? '' : SelectedProgram) + '~';
                s += (A2HCUtilities.isStringNullOrEmpty(InvoiceNumber) ? '' : InvoiceNumber) + '~';
                s += (A2HCUtilities.isStringNullOrEmpty(ContractNumber) ? '' : ContractNumber) + '~';
                s += (A2HCUtilities.isStringNullOrEmpty(FromDate) ? '' : FromDate) + '~';
                s += (A2HCUtilities.isStringNullOrEmpty(ToDate) ? '' : ToDate) + '~';
                s += (A2HCUtilities.isStringNullOrEmpty(BatchName) ? '' : BatchName) + '~';
                s += (A2HCUtilities.isStringNullOrEmpty(BatchNumber) ? '' : BatchNumber) + '~';
                s += (A2HCUtilities.isStringNullOrEmpty(BatchDescription) ? '' : BatchDescription) + '~';

                SearchCriteria = EncodingUtil.urlEncode(s, 'UTF-8');
            }
            return SearchCriteria;
        }
        private set;
    }
    
    transient List<InvoiceBatchWrapper> batches = new List<InvoiceBatchWrapper>();
    public  List<InvoiceBatchWrapper> InvoiceBatches
    {
        get
        {
            if(batches == null)
            {
                batches = new List<InvoiceBatchWrapper>();
            }
            return batches;
        }
        private set
        {
            batches = value;
        }
    }


    public PageReference resetForm()
    {
        InvoiceBatches = null;

        return null;
    }

    public PageReference doSearch()
    {
        try
        {
            SearchCriteria = null;
            // Clear any previous search results.
            InvoiceBatches = null;
            
            decimal batchNum = A2HCUtilities.tryParsedecimal(BatchNumber);
            List<ID> serviceOrderPaymentBatchIds = new List<ID>();
            List<ID> invoicePaymentBatchIds = new List<ID>();
            List<ID> accountPaymentBatchIds = new List<ID>();

            Datetime startDate = A2HCUtilities.tryParseDate(FromDate);
            Datetime endDate = A2HCUtilities.tryParseDate(ToDate);

            startDate = startDate == null ? Date.newInstance(1900, 1, 1) : startDate;
            endDate = endDate == null ? Date.newInstance(3000, 1, 1) : endDate.addDays(1).addSeconds(-1);
            
            if(!String.isBlank(ContractNumber))
            {
                for(RCR_Invoice__c p : [SELECT  RCR_Invoice_Batch__c
                                         FROM   RCR_Invoice__c
                                         WHERE  RCR_Invoice_Batch__c != null AND
                                                ID IN (SELECT   RCR_Invoice__c
                                                        FROM    RCR_Invoice_Item__c
                                                        WHERE   RCR_Service_Order__r.Name = :ContractNumber)])
                {
                    serviceOrderPaymentBatchIds.add(p.RCR_Invoice_Batch__c);
                }
            }
            
            if(!String.isBlank(InvoiceNumber))
            {
                for(RCR_Invoice__c p : [SELECT  RCR_Invoice_Batch__c
                                        FROM    RCR_Invoice__c
                                        WHERE   Invoice_Number__c LIKE :('%' + InvoiceNumber + '%')])
                {
                    invoicePaymentBatchIds.add(p.RCR_Invoice_Batch__c);
                }
            }   
            if(!String.isBlank(SelectedAccount))
            {
                for(RCR_Invoice__c p : [SELECT  RCR_Invoice_Batch__c
                                        FROM    RCR_Invoice__c
                                        WHERE   Account__c = :SelectedAccount])
                {
                    accountPaymentBatchIds.add(p.RCR_Invoice_Batch__c);
                }       
            }

            for(RCR_Invoice_Batch__c b : [SELECT    r.Id,
                                                    r.Name,
                                                    r.Sent_Date__c,
                                                    r.Batch_Number__c,
                                                    r.Description__c,
                                                    MP_File_Name__c,
                                                    Lantern_Pay_Batch__c,
                                                    (SELECT Total__c,
                                                            Total_Failed_Reconciliation__c,
                                                            Total_Reconciled__c
                                                     FROM   RCR_Invoices__r)
                                          FROM      RCR_Invoice_Batch__c r
                                          WHERE     (r.Sent_Date__c = null
                                                    OR
                                                    (r.Sent_Date__c >= :startDate AND
                                                    r.Sent_Date__c <= :endDate))
                                          AND       r.Name LIKE :(String.isBlank(BatchName)? '%' : ('%' + BatchName + '%'))
                                          AND       (r.Batch_Number__c = :batchNum
                                                    OR
                                                    Name LIKE :(batchNum == null ? '%' : '~'))
                                          AND       (r.Description__c = null OR
                                                    r.Description__c LIKE :(String.isBlank(BatchDescription) ? '%' : ('%' + BatchDescription + '%')))
                                          AND       (r.Id IN :invoicePaymentBatchIds
                                                    OR
                                                    Name LIKE :(String.isBlank(InvoiceNumber) ? '%' : '~'))
                                          AND       (r.Id IN :accountPaymentBatchIds
                                                    OR
                                                    Name LIKE :(String.isBlank(SelectedAccount) ? '%' : '~'))
                                          AND       (r.Id IN :serviceOrderPaymentBatchIds
                                                    OR
                                                    r.Name LIKE :(String.isBlank(ContractNumber) ? '%' : '~'))
                                         AND        (r.Lantern_Pay_Batch__c = :LanternPay OR
                                                    r.Name LIKE :(LanternPay ? '~' : '%'))
                                          ORDER BY CreatedDate desc
                                          LIMIT 500])
            {
                InvoiceBatches.add(new InvoiceBatchWrapper(b));
            }
            return null;
        }
        catch (Exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }

    private void doImmediateSearch()
    {
        try
        {
            for(RCR_Invoice_Batch__c b : [SELECT    r.Id,
                                                    r.Name,
                                                    r.Sent_Date__c,
                                                    r.Batch_Number__c,
                                                    MP_File_Name__c,
                                                    r.Description__c,
                                                    Lantern_Pay_Batch__c,
                                                    (SELECT Total__c,
                                                    Total_Failed_Reconciliation__c,
                                                    Total_Reconciled__c
                                                     FROM   RCR_Invoices__r)
                                          FROM      RCR_Invoice_Batch__c r
                                          WHERE     r.Name = :BatchName])
            {
                InvoiceBatches.add(new InvoiceBatchWrapper(b));
            }
        }
        catch (Exception ex)
        {
            A2HCException.formatException(ex);
        }
    }

    private String getParameterFromURLParameter(String s)
    {
        return A2HCUtilities.isStringNullOrEmpty(s) ? null : EncodingUtil.urlDecode(s, 'UTF-8');
    }

    private void doRecentSearch()
    {
        try
        {
            String srchCriteria = getParameter('srch');
            if(srchCriteria == null)
            {
                return;
            }

            String[] searches = srchCriteria.split('~', -1);
            if(searches.size() < 8)
            {
                return;
            }

            SelectedAccount = getParameterFromURLParameter(searches[0]);
            SelectedProgram = getParameterFromURLParameter(searches[1]);
            InvoiceNumber = getParameterFromURLParameter(searches[2]);
            ContractNumber = getParameterFromURLParameter(searches[3]);
            FromDate = getParameterFromURLParameter(searches[4]);
            ToDate = getParameterFromURLParameter(searches[5]);
            BatchName = getParameterFromURLParameter( searches[6]);
            BatchNumber = getParameterFromURLParameter(searches[7]);
            BatchDescription = getParameterFromURLParameter(searches[8]);

            doSearch();
        }
        catch (Exception ex)
        {
            A2HCException.formatException(ex);
        }
    }

    public class InvoiceBatchWrapper
    {
        public InvoiceBatchWrapper(RCR_Invoice_Batch__c pBatch)
        {
            Batch = pBatch;

            for(RCR_Invoice__c i : pBatch.RCR_Invoices__r)
            {
                Total += i.Total__c;
                TotalReconciled += i.Total_Reconciled__c;
                TotalFailed += i.Total_Failed_Reconciliation__c;
            }
        }

        public RCR_Invoice_Batch__c Batch
        {
            get;
            private set;
        }

        public decimal TotalFailed
        {
            get
            {
                if(TotalFailed == null)
                {
                    TotalFailed = 0.0;
                }
                return TotalFailed;
            }
            private set;
        }

        public decimal TotalReconciled
        {
            get
            {
                if(TotalReconciled == null)
                {
                    TotalReconciled = 0.0;
                }
                return TotalReconciled;
            }
            private set;
        }

        public decimal Total
        {
            get
            {
                if(Total == null)
                {
                    Total = 0.0;
                }
                return Total;
            }
            private set;
        }
    }
    
    @IsTest//(SeeAllData=true)
    static void myUnitTest()
    {
        TestLoadData.loadRCRData();

        Test.startTest();
        
        Account acc = [SELECT Id, Name FROM Account LIMIT 1]; 

        RCR_Invoice_Batch__c batch = [SELECT Id FROM RCR_Invoice_Batch__c LIMIT 1];
        RCRBatchSearchExtension ext = new RCRBatchSearchExtension(new ApexPages.StandardController(batch));
        Test.setCurrentPage(new PageReference('/apex/RCRBatchSearch'));
        ApexPages.currentPage().getParameters().put('id', batch.Id);
        ext = new RCRBatchSearchExtension(new ApexPages.StandardController(batch));

        ext.SelectedAccount = acc.Name;

        ext.InvoiceBatches = null;
        system.debug(ext.InvoiceBatches.size());

        ext.resetForm();

        ext.BatchName = 'test';
        ext.BatchNumber = '1234';
        ext.FromDate = '01/01/1900';
        ext.ToDate = '01/01/3000';
        ext.SelectedAccount = acc.ID;
        ext.ContractNumber = 'test';
        ext.SelectedProgram = 'test';
        ext.InvoiceNumber = 'test';
        ext.doSearch();
        
        system.debug(ext.SearchCriteria);
        
        ApexPages.currentPage().getParameters().remove('id');
        ApexPages.currentPage().getParameters().put('srch', ext.SearchCriteria);
        ext = new RCRBatchSearchExtension(new ApexPages.StandardController(batch));

        ext.BatchNumber = '666';
        ext.doSearch();

        RCRBatchSearchExtension.InvoiceBatchWrapper w = new RCRBatchSearchExtension.InvoiceBatchWrapper(batch);

        system.debug(w.Total);
        system.debug(w.Batch);
        system.debug(ext.BatchNumber);
        
        Test.stopTest();
    }
}