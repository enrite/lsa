/**
 * Created by me on 24/07/2018.
 */

public with sharing class LPBulkLoadParticipantsController
{
    public LPBulkLoadParticipantsController()
    {

    }

    Decimal batchSize = Lantern_Pay__c.getOrgDefaults().Bulk_load_batch_size__c;
    Integer intBatchSize = batchSize == null ? 20 : batchSize.intValue();

    public List<LPBulkLoadWrapper> ClientsToLoad
    {
        get
        {
            if(ClientsToLoad == null)
            {
                ClientsToLoad = new List<LPBulkLoadWrapper>();
                List<Referred_Client__c> temp = LPRESTInterface.getClients(null);
                for(Integer i = 0; i < intBatchSize; i++)
                {
                    if(temp.size() <= i)
                    {
                        break;
                    }
                    ClientsToLoad.add(new LPBulkLoadWrapper(temp[i]));
                }
            }
            return ClientsToLoad;
        }
        set;
    }

    public void sendToLP()
    {
        try
        {
            LPRESTInterface lpInterface = new LPRESTInterface();
            List<LP_API_Error__c> errors = new List<LP_API_Error__c>();
            Integer count = 0;
            Integer callOutCount = 0;
            Boolean limitReached = false;
            for(LPBulkLoadWrapper wrapper : ClientsToLoad)
            {
                if(!wrapper.Selected)
                {
                    continue;
                }
                try
                {
                    lpInterface.addMember(wrapper.Participant);
                    count++;
                    callOutCount = Limits.getCallouts();
                    if(callOutCount >= Limits.getLimitCallouts() - 5 || count >= intBatchSize)
                    {
                        limitReached = true;
                        break;
                    }
                }
                catch(Exception ex)
                {
                    errors.add(LPRESTInterface.logError('Bulk load', ex.getStackTraceString(), null, null, false));
                }
            }
            if(count == 0)
            {
                A2HCException.formatMessage('No records have been selected');
                return;
            }
            lpInterface.saveRecords();
            insert errors;
            if(limitReached)
            {
                A2HCException.formatMessage('A maximum of ' + String.valueOf(count) + ' records can be sent to LanternPay in one transaction, refresh the page and send another batch.');
            }
            A2HCException.formatMessage(String.valueOf(count) + ' participants sent to LanternPay. Their member key will be populated as LanternPay responds.');
        }
        catch(Exception ex)
        {
            A2HCException.formatException(ex);
        }
    }

    public class LPBulkLoadWrapper
    {
        public LPBulkLoadWrapper(Referred_Client__c part)
        {
            this();
            Participant = part;
        }

        public LPBulkLoadWrapper(RCR_Contract__c so)
        {
            this();
            ServiceOrder = so;
        }

        public LPBulkLoadWrapper()
        {
            Selected = false;
        }

        public Boolean Selected
        {
            get;set;
        }
        public Referred_Client__c Participant
        {
            get;set;
        }

        public RCR_Contract__c ServiceOrder
        {
            get;set;
        }
    }
}