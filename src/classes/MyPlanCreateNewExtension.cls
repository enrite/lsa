public with sharing class MyPlanCreateNewExtension
{
    public MyPlanCreateNewExtension(ApexPages.StandardController c)
    {
        record = (Plan__c)c.getRecord();    
        m_clonePlanActions = 'no';            
    }

    private Plan__c record;

    private String m_clonePlanActions;

    public String ClonePlanActions{
      get{return m_clonePlanActions;}
      set{m_clonePlanActions = value;}
    }
    
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('yes','Yes'));
        options.add(new SelectOption('no','No'));
        return options;
    }


    public PageReference Create()
    {
        try
        {
            // clone and save the record
            Plan__c thePlan = [SELECT Background_Notes__c,                                      
                                      Case_Conference_Outcomes__c,                                      
                                      Current_Daily_Activities__c,                                      
                                      Emergency__c,
                                      Emergency_Provider_Contact_Detail__c,                                                                                                                                                        
                                      Factors_impacting_on_recovery__c,
                                      How_to_Meet_Goals__c,
                                      New_Client_Information_Pack_Provided__c,
                                      Living_Arrangements__c,
                                      Medical_Issues_Other_Conditions__c,                                                                            
                                      Client__c,
                                      Participant_Emergency_Contact__c,
                                      My_Goals__c,
                                      My_Preferences_for_Service_Planner__c,
                                      Participant_Problems__c,                                    
                                      Provider_Contact_Details__c,                                                                            
                                      Service_Planner_Comments__c                                      
                               FROM Plan__c
                               WHERE Id = :record.Id];
            Plan__c newPlan = thePlan.clone(false, true);
            newPlan.Plan_Start_Date__c = record.Plan_Start_Date__c;   
            newPlan.Plan_End_Date__c = record.Plan_End_Date__c;    
            newPlan.Plan_Status__c = 'Draft';                            
            insert newPlan;
            
            // clone and save the goals
            List<Plan_Goal__c> newGoals = new List<Plan_Goal__c>();
            for (Plan_Goal__c g : [SELECT Name,
                                          Discussion__c,
                                          Order__c
                                   FROM Plan_Goal__c
                                   WHERE Participant_Plan__c = :record.Id])
            {
                Plan_Goal__c newGoal = g.clone(false, true);
                newGoal.Participant_Plan__c = newPlan.Id;
                newGoals.add(newGoal);
            }  
            insert newGoals;                                 
            
            if(m_clonePlanActions == 'yes')
            {
              // clone and save the plan actions
              List<Plan_Action__c> newPlanActions = new List<Plan_Action__c>();
              for (Plan_Action__c a : [SELECT About_Participant__c,
                                              Agency_and_Training_Requirements__c,                                                                            
                                              Client__c,
                                              Client_Agreed__c,
                                              Duties_and_Tasks__c,                                            
                                              Expected_Outcome__c,
                                              Necessary_Reasonable__c,
                                              Necessary_and_Reasonable_Criteria__c,
                                              Objectives_and_Outcomes_of_the_Service__c,
                                              Order_Deadline__c,
                                              Participant__c,                                            
                                              Participant_Preferences__c,                                            
                                              Plan_End_Date__c,                                            
                                              Plan_Record_Type__c,
                                              Plan_Start_Date__c,
                                              Pre_Defined_Services__c,
                                              RCR_Program__c,
                                              RCR_Program_Category__c,
                                              RCR_Program_Category_Service_Type__c,
                                              Provider__c,
                                              Purpose__c,
                                              Quantity__c,
                                              Quote_Attached__c,
                                              Rationale__c,
                                              Reason__c,
                                              Reference_to_LSS_Rules__c,
                                              Related_to_Vehicle_Accident_Injuries__c,
                                              Relationship_to_Goals__c,
                                              Requirements__c,
                                              Risks__c,
                                              Service_Delivery_Matters__c,
                                              Item_Description__c,
                                              Services_to_be_Provided__c,
                                              Special_Considerations_Instructions__c,
                                              Timeframe__c,
                                              Trialled__c,
                                              Who_to_Arrange__c
                                     FROM Plan_Action__c
                                     WHERE Participant_Plan__c = :record.Id])
              {
                  Plan_Action__c newPlanAction = a.clone(false, true);
                  newPlanAction.Participant_Plan__c = newPlan.Id;
                  newPlanActions.add(newPlanAction);
              }  
              insert newPlanActions;   
            }    
            
            // return to the new record                                   
            return new PageReference('/' + newPlan.Id);
        }
        catch(Exception e)
        {
            return A2HCException.formatException(e);
        }
    }
}