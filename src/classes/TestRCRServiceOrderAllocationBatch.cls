@IsTest
private class TestRCRServiceOrderAllocationBatch 
{
    @IsTest//(SeeAllData=true)
    static void test1()
    {
        TestLoadData.loadRCRData();
                
        Test.StartTest();
    
        List<RCR_Contract__c> serviceOrders = new List<RCR_Contract__c>();
        
        for(RCR_Contract__c so : [SELECT    ID,
                                            Schedule__c
                                  FROM      RCR_Contract__c
                                  LIMIT 1])
        {
            so.RecordTypeId = A2HCUtilities.getRecordType('RCR_Contract__c', APSRecordTypes.RCRServiceOrder_Rollover).ID;
            so.Schedule__c = 'MYUNITTEST';
            serviceOrders.add(so);
        }
        
        update serviceOrders;
        
        RCRServiceOrderAllocationBatch batch = new RCRServiceOrderAllocationBatch();
//        batch.ScheduleID = 'MYUNITTEST';
        ID batchprocessid = Database.executeBatch(batch, 1);
//
//        system.debug(batch.FinancialYears);
//        system.debug(batch.AllocationIdsForApproval);
//        system.debug(batch.NextYearAdjustment);
//        system.debug(batch.FollowingYearAdjustment);
        
        Test.StopTest();
    }
}