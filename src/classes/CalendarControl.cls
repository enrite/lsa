/****************************************************************
*   ccCalendar: controller for ccCalendar compnent              *
*   Renders a single or multiple YUI callendars according to    *
*   the properties set.                                         *
*   Author: Celso de Souza  *   Created: 01/06/2010             *
****************************************************************/

public class CalendarControl{

    /* A list of dates in comma spareted values in the format YYYY/MM/DD to render in the Calendar */
    public string  inStringDateListCSV {get{if(inStringDateListCSV==null)inStringDateListCSV='';return inStringDateListCSV;}set;}

    /* Enable/disable selection of past dates */
    public boolean SelectPastDates {get{if(SelectPastDates==null)SelectPastDates=false;return SelectPastDates;}set;}
    
    /* Calendar First date*/
    public date FromDate{get;set;}
    
    /* Calendar Last date*/
    public date ToDate{get;set;}    
    
    /* return the number of months between the start date and end date */
    public integer getNumberOfMonths()
    {return FromDate.monthsBetween(ToDate) + 1;}
    
    /* return the FromDate month index number */
    public integer getFromMonthIndex()
    {return FromDate.month()-1;}
    
    /* return the FromDate year*/
    public integer getFromYear()
    {return FromDate.year();}   
    
    
    /*  This function returns to the YUI Calendar the dates that are out of bound on the start of the calendars,
        E.g. FromDate is the 5th day of the month, so days from 1 to 4 are out of bound.
             if SelectPastDates is true it will bring all dates from the 1st day of the month in
             FromDate till yesterday as out of bound
        Below is how YUI expects the string. Teh date format used is YYYY/MM/DD
        YAHOO.CC.calendar.cal1.addRenderer("2009/12/01-2009/12/11", YAHOO.CC.calendar.cal1.renderOutOfBoundsDate);   */
    public string getFromDateOutOfBounds()
    {
        date nFromDate = FromDate;
        
        if(nFromDate < System.today() && !SelectPastDates)
            nFromDate = System.today();
        
        date f =  FromDate.toStartOfMonth();
    
        if(nFromDate==f)
            return '';
    
        String s =  f.year() + '/' + f.month() + '/' + f.day() + '-' + nFromDate.year() +'/'+ nFromDate.month() + '/' + (nFromDate.day()-1);
        return 'YAHOO.CC.calendar.cal1.addRenderer("' + s + '", YAHOO.CC.calendar.cal1.renderOutOfBoundsDate);';
    }

    /*  This function returns to the YUI Calendar the dates that are out of bound on the end of the calendars,
            E.g. ToDate is the 5th day of the month, so days from 6 to the last day of that month are out of bound.
        Below is how YUI expects the string. Teh date format used is YYYY/MM/DD
        YAHOO.CC.calendar.cal1.addRenderer("2009/12/01-2009/12/11", YAHOO.CC.calendar.cal1.renderOutOfBoundsDate);   */
    public string getToDateOutOfBounds()
    {
        date f =  ToDate.toStartOfMonth().addMonths(1).addDays(-1);
    
        if(ToDate==f)
            return '';
    
        String s =  ToDate.year() +'/'+ ToDate.month() + '/' + (ToDate.day()+1) + '-' + f.year() + '/' + f.month() + '/' + f.day();
        return 'YAHOO.CC.calendar.cal1.addRenderer("' + s + '", YAHOO.CC.calendar.cal1.renderOutOfBoundsDate);';
    }

    /* selectedDatesStrList is the property used by the component to render the selected dates as selected
        it uses the inStringDateListCSV property to generate the list
    */  
    public List<String> selectedDatesStrList {
        get{
        if(selectedDatesStrList==null)
            selectedDatesStrList=new List<String>(); 
        selectedDatesStrList.clear();
        if(inStringDateListCSV.length()>0){
            for (date d:convertedToList())
                selectedDatesStrList.add( d.year() + '/' + d.month() + '/' + d.day());
        }
        return selectedDatesStrList;
    }
        set;
    }
    

    /* Converts inStringDateListCSV into a List of Dates*/
    public List<Date> convertedToList()
    {
        List<Date> s = new List<Date>(); 
        if(inStringDateListCSV.length() < 1)
            return s;
                
        List<String> strDates = inStringDateListCSV.split(',');
        for(String str : strDates)
        {
            str = str.replace('/','-');
            Date d = date.valueOf(str + ' 00:00:00');
            s.add(d);
        }   
        return s;
    }
    

}