/**
 * Created by cain on 31/05/2016.
 */

global class UpdateClientPlantCount  implements Schedulable
{

    global void execute(SchedulableContext sc)
    {
        UpdateClientPlanCountBatch batch = new UpdateClientPlanCountBatch();
        ID batchprocessid = Database.executeBatch(batch, 100);
    }

}