public with sharing class RCRNewRequestController
	extends A2HCPageBase 
{
	public RCRNewRequestController()
	{
		if(!InPortal)
		{
			getItemsToApprove();
		}
	}
	
	public RCRNewRequestController(ApexPages.StandardController cntrl)
	{}
	
	public String ItemsToApproveURL
	{
		get
		{			
			if(ItemsToApproveURL == null)
			{
				Schema.DescribeSObjectResult result = ProcessInstanceWorkItem.sObjectType.getDescribe();			
				ItemsToApproveURL = (new PageReference('/' + result.getKeyPrefix())).getUrl();
			}
			return ItemsToApproveURL;
		}
		private set;
	}

	public Integer ItemsToApprove
	{
		get;
		private set;
	}
	
	public Integer DelegatedItemsToApprove
	{
		get;
		private set;
	}
	
	public Integer MyTasks
	{
		get
		{			
			if(MyTasks == null)
			{		
				MyTasks = [SELECT 	COUNT()
							FROM 	Task
							WHERE	OwnerId = :Userinfo.getUserID() AND
									IsClosed = false];
			}
			return MyTasks;
		}
		private set;
	}
	
    public String NewRequestURL
    {
        get
        {
        	if(NewRequestURL == null)
			{
	            Schema.DescribeSObjectResult result = RCR_Contract__c.sObjectType.getDescribe();
	            ID recordTypeID = A2HCUtilities.getRecordType(result.getName(), APSRecordTypes.RCRServiceOrder_NewRequestCBMS).ID;
	            PageReference pg = new PageReference('/' + result.getKeyPrefix() + '/e?RecordType=' + recordTypeID + '&retURL=' + ApexPages.currentPage().getURL());
	            NewRequestURL = pg.getUrl();
			}
			return NewRequestURL;
        }
        private set;
    }
    
    public PageReference convertToServiceOrderCBMS()
    {
    	System.Savepoint sp = Database.setSavepoint();
    	try
    	{
    		throwTestError();
    		ID serviceOrderID = getParameter('id');
			RCR_Contract__c serviceOrder = RCRServiceOrderRequestController.getServiceOrder(serviceOrderID);
			if(serviceOrder.Total_Service_Order_Cost__c == 0)
			{
				return A2HCException.formatExceptionAndRollback(sp, 'New Request has no cost and cannot be converted to a Service Order.');
			}
			serviceOrder.CSA_Status__c = null;
			serviceOrder.RecordTypeId = A2HCUtilities.getRecordType('RCR_Contract__c', APSRecordTypes.RCRServiceOrder_ServiceOrderCBMS).ID;
			serviceOrder.Record_Type_Name__c = APSRecordTypes.RCRServiceOrder_ServiceOrderCBMS;
    		update serviceOrder;
    		
//			String errMsg = ServiceOrderFuture.checkServiceOrderAllocation(serviceOrder);
//    		if(errMsg != null)
//			{
//				return A2HCException.formatExceptionAndRollback(sp, errMsg);
//			}
    		return new PageReference('/' + serviceOrderId).setRedirect(true);    		
    	}
    	catch(Exception ex)
    	{
    		return A2HCException.formatExceptionAndRollback(sp, ex);
    	}
    }
    
    private void getItemsToApprove()
    {
    	ItemsToApprove = 0;
		DelegatedItemsToApprove = 0;
		ID userID = UserInfo.getUserID();
		List<User> delegateForUsers = [SELECT ID
										FROM	User
										WHERE	DelegatedApproverId = :userID];
		for(ProcessInstanceWorkItem piwi : [SELECT ID,
													ActorID
											FROM	ProcessInstanceWorkItem])
		{			
			if(piwi.ActorID == userID ||
					UserUtilities.isUserInGroupByID(piwi.ActorID, userID) ||
					UserUtilities.isUserInRoleByID(piwi.ActorID, userID, true)) 
			{
				ItemsToApprove++;	
			}			
			for(User u : delegateForUsers)
			{
				if(piwi.ActorID == u.ID) 
				{
					DelegatedItemsToApprove++;
				}
			}
		}
    }
}