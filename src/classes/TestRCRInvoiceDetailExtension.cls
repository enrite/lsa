/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestRCRInvoiceDetailExtension {

    @IsTest//(SeeAllData=true)
    static void test1()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        RCR_Contract__c so = [SELECT ID,
                                    Account__c,
                                    Start_Date__c,
                                    End_Date__c
                            FROM    RCR_Contract__c
                            /*WHERE   Name = 'TestSO'*/
                            LIMIT 1];
                                    
        Test.setCurrentPage(Page.RCRInvoiceDetail);
        RCRInvoiceDetailExtension ext = new RCRInvoiceDetailExtension(new ApexPages.Standardcontroller(new RCR_Invoice__c())); 
        ext.saveOverride();
        
        RCR_Invoice__c invoice = TestRCRInvoiceDetailExtension.createInvoice();

        ApexPages.currentPage().getParameters().put('id', invoice.Id);
        ext = new RCRInvoiceDetailExtension(new ApexPages.Standardcontroller(invoice));
        ext.SelectedAccount = invoice.Account__c; 
        ext.saveOverride();
        
        ext.getInvoiceItems(null);
        ext.getInvoiceItems('id');
        
        ApexPages.currentPage().getParameters().put('qty', '2');
        for(Integer i = 0; i < ext.InvoiceRowItems.size(); i++)
        {
            ApexPages.currentPage().getParameters().put('rowIndex', ext.InvoiceRowItems[i].RowIndex.format());
            ext.InvoiceRowItems[i].InvoiceItem.Quantity__c = 2.0; 
        }

        ext.saveOverride();
        
        system.assert(ext.InvoiceRowItems.size() > 0);
        for(Integer i = 0; i < ext.InvoiceRowItems.size(); i++)
        {
            system.debug(ext.InvoiceRowItems[i].ServiceDescription);
            system.debug(ext.InvoiceRowItems[i].InvoiceItem);
            system.debug(ext.InvoiceRowItems[i].ReadOnly);
            system.debug(ext.InvoiceRowItems[i].Reconciled);
            system.debug(ext.InvoiceRowItems[i].IsError);
            system.debug(ext.InvoiceRowItems[i].RowIndex);
            system.debug(ext.InvoiceRowItems[i].CreatedDate);
            system.debug(ext.InvoiceRowItems[i].Services);
            system.debug(ext.InvoiceRowItems[i].AvailableForceServices);
            ext.InvoiceRowItems[i].setServices(new List<RCR_Service__c>());
            ext.InvoiceRowItems[i].setAvailableForceServicesOpts();
            ApexPages.currentPage().getParameters().put('index', i.format());  
            ext.getServices();   
        }
    
        system.assert(ext.SelectedAccount != null);
        system.assert(ext.Invoice != null);
        system.assert(RCRInvoiceDetailExtension.getAccounts() != null);

        ext.InvoiceItems = null; 

        system.assert(ext.InvoiceItems != null);
        system.assert(ext.InvoiceRowItems != null);

        ext.InvoiceRowItems = null;
        system.assert(ext.InvoiceRowItems != null); 
        
        system.assert(ext.ServiceTypeList != null);
        
        ApexPages.currentPage().getParameters().put('rowIndex', (ext.InvoiceRowItems.size() - 1).format());
        ext.editRow();
        
        ext.cancelOverride();
        
        ApexPages.currentPage().getParameters().put('rowIndex', (ext.InvoiceRowItems.size() - 1).format());
        ext.cloneRow();
        
        ApexPages.currentPage().getParameters().put('rowIndex', (ext.InvoiceRowItems.size() - 1).format());
        ext.deleteRow();
        
        ext.saveAndClose();

        ext.cancelEdit();

        Test.stopTest();                
    }
    
    @IsTest//(SeeAllData=true)
    static void test2()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        Test.setCurrentPage(Page.RCRInvoiceDetail);
        
        RCR_Contract__c so = [SELECT ID,
                                    Account__c,
                                    Start_Date__c,
                                    End_Date__c
                            FROM    RCR_Contract__c
                            /*WHERE   Name = 'TestSO'*/
                            LIMIT 1];
                            
        ApexPages.currentPage().getParameters().put('serviceOrder', so.Id);
        RCRInvoiceDetailExtension ext = new RCRInvoiceDetailExtension(new ApexPages.Standardcontroller(new RCR_Invoice__c()));
        ext.SelectedAccount = so.Account__c;
        ext.Invoice.Start_Date__c = so.Start_Date__c;
        ext.Invoice.End_Date__c = so.End_Date__c;
        ext.Invoice.Invoice_Number__c = 'Test';
        ext.saveOverride();
        
        system.assert(ext.ServicesForAccount != null);        
        
        ext.saveAndAddLines(); 
        
        for(Integer i = 0; i < ext.NewInvoiceRowItems.size() && i <= 3; i++)
        {
            ext.NewInvoiceRowItems[i].InvoiceItem.Activity_Date__c = so.Start_Date__c.addDays(1);
            ext.NewInvoiceRowItems[i].InvoiceItem.Code__c = 'TESTCODE1';
            ext.NewInvoiceRowItems[i].setAvailableForceServicesOpts();
            ApexPages.currentPage().getParameters().put('index', i.format());  
            ext.getServiceRate();   
        } 
        
        ApexPages.currentPage().getParameters().put('rowIndex', (ext.NewInvoiceRowItems.size() - 1).format());
        ext.deleteNewItem();      
        ext.resortList();
        ext.saveAndReconcile();

        Test.StopTest();
    }
    
    @IsTest//(SeeAllData=true)
    static void test3()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
        
         Test.setCurrentPage(Page.RCRInvoiceDetail);
        
        RCR_Contract__c so = [SELECT ID,
                                    Account__c,
                                    Start_Date__c,
                                    End_Date__c
                            FROM    RCR_Contract__c
                            /*WHERE   Name = 'TestSO'*/
                            LIMIT 1];
                            
        ApexPages.currentPage().getParameters().put('serviceOrder', so.Id);
        RCRInvoiceDetailExtension ext = new RCRInvoiceDetailExtension(new ApexPages.Standardcontroller(new RCR_Invoice__c()));
        ext.SelectedAccount = so.Account__c;
        ext.Invoice.Start_Date__c = so.Start_Date__c;
        ext.Invoice.End_Date__c = so.End_Date__c;
        ext.Invoice.Invoice_Number__c = 'Test';
        ext.saveOverride();
        
        ext.generateIFRows();
        
        ext.SelectedAccount = null;
        ext.isValid();
        
        ext.Invoice.Invoice_Number__c = null;
        ext.SelectedAccount = so.Account__c;
        ext.isValid();
        
        ext.Invoice.Invoice_Number__c = 'Test';
        ext.Invoice.RCR_Invoice_Upload__c = null;
        ext.Invoice.Start_Date__c = null;
        ext.isValid();
        
        ext.Invoice.Start_Date__c = so.Start_Date__c;
        ext.Invoice.End_Date__c = null;
        ext.isValid();
        
        ext.Invoice.End_Date__c = so.End_Date__c;
        ext.isValid();
        
        Test.StopTest();
    }
    
    @IsTest//(SeeAllData=true)
    static void testErrors()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        Test.setCurrentPage(Page.RCRInvoiceDetail);       
        RCR_Invoice__c invoice = TestRCRInvoiceDetailExtension.createInvoice();
        RCRInvoiceDetailExtension ext = new RCRInvoiceDetailExtension(new ApexPages.Standardcontroller(invoice));
        ext.forceTestError = true;
        
        try
        {
            ext.saveOverride();    
        }
        catch(Exception ex) 
        {
            system.assert(ex instanceof A2HCException);
        }   
        try
        {
            ext.saveAndAddLines();        

        }
        catch(Exception ex) 
        {
            system.assert(ex instanceof A2HCException);
        }   
        try
        {
            ext.saveAndClose();
        }
        catch(Exception ex) 
        {
            system.assert(ex instanceof A2HCException);
        }   
        try
        {
            ext.saveAndReconcile();  
        }
        catch(Exception ex) 
        {
            system.assert(ex instanceof A2HCException);
        }   
        try
        {
            ext.cancelOverride();    
        }
        catch(Exception ex) 
        {
            system.assert(ex instanceof A2HCException);
        }   
        try
        {
            ext.resortList();    
        }
        catch(Exception ex) 
        {
            system.assert(ex instanceof A2HCException);
        }   
        try
        {
            ext.addRows();    
        }
        catch(Exception ex) 
        {
            system.assert(ex instanceof A2HCException);
        }   
        try
        {
            ext.editRow();    
        }
        catch(Exception ex) 
        {
            system.assert(ex instanceof A2HCException);
        }   
        try
        {
            ext.deleteRow();    
        }
        catch(Exception ex) 
        {
            system.assert(ex instanceof A2HCException);
        }   
        try
        {
            ext.deleteNewItem();    
        }
        catch(Exception ex) 
        {
            system.assert(ex instanceof A2HCException);
        }   
        try
        {
            ext.cloneRow();    
        }
        catch(Exception ex) 
        {
            system.assert(ex instanceof A2HCException);
        }   
        try
        {
            ext.generateIFRows();    
        }
        catch(Exception ex) 
        {
            system.assert(ex instanceof A2HCException);
        }   
        try
        {
            ext.getServices();    
        }
        catch(Exception ex) 
        {
            system.assert(ex instanceof A2HCException);
        }   
        try
        {
            ext.getServiceRate();    
        }
        catch(Exception ex) 
        {
            system.assert(ex instanceof A2HCException);
        } 
        
        Test.StopTest();          
    }
       
    public static RCR_Invoice__c createInvoice()
    {               
        RCR_Contract__c so = [SELECT ID,
                                    Account__c,
                                    Start_Date__c,
                                    End_Date__c
                            FROM    RCR_Contract__c
                            /*WHERE   Name = 'TestSO'*/
                            LIMIT 1];
                            
        Date startDate = so.Start_Date__c;
        Date endDate = so.End_Date__c;
        Date activityDate = so.Start_Date__c.addDays(1);

        RCR_Invoice__c inv = new RCR_Invoice__c();
        inv.Account__c = so.Account__c;
        inv.RecordTypeId = A2HCUtilities.getRecordType('RCR_Invoice__c', 'Processing').Id;
        inv.Date__c = startDate;
        inv.Start_Date__c = startDate; 
        inv.End_Date__c = endDate;
        inv.Invoice_Number__c = 'TEST INV';
        insert inv;
        
        RCR_Invoice_Item__c item1 = new RCR_Invoice_Item__c();
        item1.RCR_Invoice__c = inv.Id;
        item1.Unique_Identifier__c = 'item1';
        item1.Activity_Date__c = activityDate;
        item1.Code__c = 'TESTCODE1';
        item1.GST__c = 0.01;
        item1.Quantity__c = 1.0;
        item1.Rate__c = 0.1;
        item1.Service_Order_Number__c = 'TestSO';   
        item1.Total_Item_GST__c = 0.01;
        item1.Total__c = 0.11;
        insert item1;
        
        RCR_Invoice_Item__c item2 = new RCR_Invoice_Item__c();
        item2.RCR_Invoice__c = inv.Id;
        item2.Unique_Identifier__c = 'item2';
        item2.Activity_Date__c = activityDate;
        item2.Code__c = 'EXP';
        item2.GST__c = 0.01;
        item2.Quantity__c = 1.0;
        item2.Rate__c = 0.1;
        item2.Service_Order_Number__c = 'TestSO';   
        item2.Total_Item_GST__c = 0.01;
        item2.Total__c = 0.11;
        insert item2;
                        
        return inv;                
    }
}