/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public class TestRCRReconcileBatch {
    
    private static String scheduleID
    {
        get
        {
            return '123456789ABC';
        }
    }
    
    // SeeAllData only needed to view existing reference data
    @IsTest//(SeeAllData=true)
    private static void test1()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        TestLoadData.loadAdhocServices();
    
        RCR_Contract__c so = getServiceOrder();         
        so.Visible_To_service_providers__c = true;
        so.CSA_Status__c = APS_PicklistValues.RCRContractCSAStatus_Accepted;
        update so;
        /*RCR_CBMS_Contract__c cbmsContract = new RCR_CBMS_Contract__c(ID = so.RCR_CBMS_Contract__c);
        cbmsContract.Signed_By_All_Parties__c = so.Start_Date__c;
        update cbmsContract;*/
        
        Schedule_Batch__c sb = new Schedule_Batch__c(Schedule__c = scheduleID, Batch_Class_Name__c = RCRReconcileBatch.class.getName());
        insert sb;
        
        Date activityDate = so.Start_Date__c.addDays(1);
        Date halfDayHoliday = activityDate.addDays(1);
        
        RCR_Invoice__c inv = createInvoice();
        
        RCR_Invoice_Item__c item1 = new RCR_Invoice_Item__c();
        item1.RCR_Invoice__c = inv.Id;
        item1.Unique_Identifier__c = 'item1';
        item1.Activity_Date__c = activityDate;
        item1.Code__c = 'TESTCODE1';
        item1.GST__c = 0.01;
        item1.Quantity__c = 1.0;
        item1.Rate__c = 0.1;
        item1.Service_Order_Number__c = so.Name;    
        item1.Total_Item_GST__c = 0.01;
        item1.Total__c = 0.11;
        insert item1;
        
        // Reversal - will error because the Item has not been submitted yet,
        //            but will execute some more code.
        RCR_Invoice_Item__c item2 = new RCR_Invoice_Item__c();
        item2.RCR_Invoice__c = inv.Id;
        item2.Unique_Identifier__c = 'item2';
        item2.Activity_Date__c = activityDate;
        item2.Code__c = 'TESTCODE1';
        item2.GST__c = -0.01;
        item2.Quantity__c = 1.0;
        item2.Rate__c = -0.1;
        item2.Service_Order_Number__c = so.Name;    
        item2.Total_Item_GST__c = -0.01;
        item2.Total__c = -0.11;
        insert item2;
        
        // Perform Alternate Scheduled Service Match
        RCR_Invoice_Item__c item3 = new RCR_Invoice_Item__c();
        item3.RCR_Invoice__c = inv.Id;
        item3.Unique_Identifier__c = 'item3';
        item3.Activity_Date__c = activityDate;
        item3.Period_Start_Date__c = so.Start_Date__c;
        item3.Period_End_Date__c = so.End_Date__c;
        item3.Code__c = 'TESTCODE2';
        item3.GST__c = 0.01;
        item3.Quantity__c = 1.0;
        item3.Rate__c = 0.1;
        item3.Service_Order_Number__c = so.Name;    
        item3.Total_Item_GST__c = 0.01;
        item3.Total__c = 0.11;
        insert item3;
        
        // Perform AdHoc Service Match
        RCR_Invoice_Item__c item4 = new RCR_Invoice_Item__c();
        item4.RCR_Invoice__c = inv.Id;
        item4.Unique_Identifier__c = 'item4';
        item4.Activity_Date__c = activityDate;
        item4.Code__c = 'EXP';
        item4.GST__c = 0.01;
        item4.Quantity__c = 1.0;
        item4.Rate__c = 0.1;
        item4.Service_Order_Number__c = so.Name;    
        item4.Total_Item_GST__c = 0.01;
        item4.Total__c = 0.11;
        insert item4;
        
        // Match Service Order with Different Flexibility
        RCR_Invoice_Item__c item5 = new RCR_Invoice_Item__c();
        item5.RCR_Invoice__c = inv.Id;
        item5.Unique_Identifier__c = 'item5';
        item5.Activity_Date__c = activityDate;
        item5.Code__c = 'TESTCODE3';
        item5.GST__c = 0.005;
        item5.Quantity__c = 1.0;
        item5.Rate__c = 0.05;
        item5.Service_Order_Number__c = so.Name;    
        item5.Total_Item_GST__c = 0.005;
        item5.Total__c = 0.055;
        insert item5;
        
        // Halfday Holiday
        RCR_Invoice_Item__c item6 = new RCR_Invoice_Item__c();
        item6.RCR_Invoice__c = inv.Id;
        item6.Unique_Identifier__c = 'item6';
        item6.Activity_Date__c = halfDayHoliday;
        item6.Code__c = 'TESTCODE3';
        item6.GST__c = 0.005;
        item6.Quantity__c = 1.0;
        item6.Rate__c = 0.05;
        item6.Service_Order_Number__c = so.Name;    
        item6.Total_Item_GST__c = 0.005;
        item6.Total__c = 0.055;
        insert item6;
        
        // Force Reconcile
        RCR_Invoice_Item__c item7 = new RCR_Invoice_Item__c();
        item7.RCR_Invoice__c = inv.Id;
        item7.Unique_Identifier__c = 'item7';
        item7.Activity_Date__c = activityDate;
        item7.Code__c = 'TESTCODE1';
        item7.GST__c = 0.01;
        item7.Quantity__c = 1.0;
        item7.Rate__c = 0.1;
        item7.Service_Order_Number__c = so.Name;    
        item7.Total_Item_GST__c = 0.01;
        item7.Total__c = 0.11;
        item7.Force_Reconcile_To__c = 'TESTCODE1';
        item7.Comment__c = 'Force Reconcile';
        insert item7;
        
        // Force Reconcile WITH INVALID CODE
        RCR_Invoice_Item__c item8 = new RCR_Invoice_Item__c();
        item8.RCR_Invoice__c = inv.Id;
        item8.Unique_Identifier__c = 'item8';
        item8.Activity_Date__c = activityDate;
        item8.Code__c = 'TESTCODE1';
        item8.GST__c = 0.01;
        item8.Quantity__c = 1.0;
        item8.Rate__c = 0.1;
        item8.Service_Order_Number__c = so.Name;    
        item8.Total_Item_GST__c = 0.01;
        item8.Total__c = 0.11;
        item8.Force_Reconcile_To__c = 'TESTCODEX';
        item8.Comment__c = 'Force Reconcile';
        insert item8;
        
        // INVALID SERVICE ORDER
        RCR_Invoice_Item__c item9 = new RCR_Invoice_Item__c();
        item9.RCR_Invoice__c = inv.Id;
        item9.Unique_Identifier__c = 'item9';
        item9.Activity_Date__c = activityDate;
        item9.Code__c = 'TESTCODE1';
        item9.GST__c = 0.01;
        item9.Quantity__c = 1.0;
        item9.Rate__c = 0.1;
        item9.Service_Order_Number__c = 'NOT EXIST';    
        item9.Total_Item_GST__c = 0.01;
        item9.Total__c = 0.11;
        insert item9;
        
        // Activity Date IN FUTURE
        RCR_Invoice_Item__c item10 = new RCR_Invoice_Item__c();
        item10.RCR_Invoice__c = inv.Id;
        item10.Unique_Identifier__c = 'item10';
        item10.Activity_Date__c = Date.today().addDays(1);
        item10.Code__c = 'TESTCODE1';
        item10.GST__c = 0.01;
        item10.Quantity__c = 1.0;
        item10.Rate__c = 0.1;
        item10.Service_Order_Number__c = so.Name;   
        item10.Total_Item_GST__c = 0.01;
        item10.Total__c = 0.11;
        insert item10;
        
        // INVALID GST
        RCR_Invoice_Item__c item11 = new RCR_Invoice_Item__c();
        item11.RCR_Invoice__c = inv.Id;
        item11.Unique_Identifier__c = 'item11';
        item11.Activity_Date__c = Date.today().addDays(1);
        item11.Code__c = 'TESTCODE1';
        item11.GST__c = 1.10;
        item11.Quantity__c = 1.0;
        item11.Rate__c = 0.1;
        item11.Service_Order_Number__c = so.Name;   
        item11.Total_Item_GST__c = 0.01;
        item11.Total__c = 0.11;
        insert item11;
        
        // INVALID SERVICE CODE
        RCR_Invoice_Item__c item12 = new RCR_Invoice_Item__c();
        item12.RCR_Invoice__c = inv.Id;
        item12.Unique_Identifier__c = 'item12';
        item12.Activity_Date__c = activityDate;
        item12.Code__c = 'TESTCODEX';
        item12.GST__c = 0.01;
        item12.Quantity__c = 1.0;
        item12.Rate__c = 0.1;
        item12.Service_Order_Number__c = so.Name;   
        item12.Total_Item_GST__c = 0.01;
        item12.Total__c = 0.11;
        insert item12;

        // Half Day holiday RATE ABOVE Normal Rate but UNDER Holiday Rate.
        RCR_Invoice_Item__c item13 = new RCR_Invoice_Item__c();
        item13.RCR_Invoice__c = inv.Id;
        item13.Unique_Identifier__c = 'item13';
        item13.Activity_Date__c = activityDate;
        item13.Code__c = 'TESTCODE1';
        item13.GST__c = 0.09;
        item13.Quantity__c = 9.0;
        item13.Rate__c = 0.1;
        item13.Service_Order_Number__c = so.Name;   
        item13.Total_Item_GST__c = 0.09;
        item13.Total__c = 0.99;
        insert item13;

        RCRReconcileBatch batch = new RCRReconcileBatch();
        batch.ScheduleID = scheduleID;
                
        Database.executeBatch(batch, 20);        
        
        for(RCR_Invoice_Item__c i : [SELECT r.Status__c, 
                                            r.RCR_Service_Order__r.Name, 
                                            r.RCR_Contract_Scheduled_Service__r.Name, 
                                            r.RCR_Contract_Adhoc_Service__r.Name, 
                                            r.Force_Reconcile_To__c, 
                                            r.Exceeds_Tolerance__c, 
                                            r.Error__c, 
                                            r.Code__c 
                                     FROM   RCR_Invoice_Item__c r
                                     WHERE  r.RCR_Invoice__c = :inv.Id])
        {
            system.assert(i.Error__c == null || i.Status__c != 'Approved');
        }
        
        Test.StopTest();
    }


    @IsTest//SeeAllData=true)
    private static void test2()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        TestLoadData.loadAdhocServices();
    
        // TESTING RECONCILE SINGLE ITEM
        RCR_Contract__c so = getServiceOrder(); 
        so.Visible_To_service_providers__c = true;
        so.CSA_Status__c = APS_PicklistValues.RCRContractCSAStatus_Accepted;
        update so;
        /*RCR_CBMS_Contract__c cbmsContract = new RCR_CBMS_Contract__c(ID = so.RCR_CBMS_Contract__c);
        cbmsContract.Signed_By_All_Parties__c = so.Start_Date__c;
        update cbmsContract;*/
        
        Date activityDate = so.Start_Date__c.addDays(1);
         
        RCR_Invoice__c inv = createInvoice();
        
        RCR_Invoice_Item__c item1 = new RCR_Invoice_Item__c();
        item1.RCR_Invoice__c = inv.Id;
        item1.Unique_Identifier__c = 'item1';
        item1.Activity_Date__c = activityDate;
        item1.Code__c = 'TESTCODE1';
        item1.GST__c = 0.01;
        item1.Quantity__c = 1.0;
        item1.Rate__c = 0.1;
        item1.Service_Order_Number__c = so.Name;    
        item1.Total_Item_GST__c = 0.01;
        item1.Total__c = 0.11;
        insert item1;
        
        RCR_Invoice_Item__c item2 = new RCR_Invoice_Item__c();
        item2.RCR_Invoice__c = inv.Id;
        item2.Unique_Identifier__c = 'item2';
        item2.Activity_Date__c = activityDate;
        item2.Code__c = 'TEST 2';
        item2.GST__c = 0.01;
        item2.Quantity__c = 1.0;
        item2.Rate__c = 0.1;
        item2.Service_Order_Number__c = so.Name;    
        item2.Total_Item_GST__c = 0.01;
        item2.Total__c = 0.11;
        insert item2;
        
        // over limit
        RCR_Invoice_Item__c item3 = new RCR_Invoice_Item__c();
        item3.RCR_Invoice__c = inv.Id;
        item3.Unique_Identifier__c = 'item1';
        item3.Activity_Date__c = activityDate;
        item3.Code__c = 'TESTCODE1';
        item3.GST__c = 0.00;
        item3.Quantity__c = 2000.0;
        item3.Rate__c = 0.1;
        item3.Service_Order_Number__c = so.Name;    
        item3.Total_Item_GST__c = null;
        item3.Total__c = 200.00;
        insert item3;
        
        RCR_Invoice_Item__c item4 = new RCR_Invoice_Item__c();
        item4.RCR_Invoice__c = inv.Id;
        item4.Unique_Identifier__c = 'item2';
        item4.Activity_Date__c = activityDate;
        item4.Code__c = 'EXP';
        item4.GST__c = 0.00;
        item4.Quantity__c = 2000.0;
        item4.Rate__c = 0.1;
        item4.Service_Order_Number__c = so.Name;    
        item4.Total_Item_GST__c = null;
        item4.Total__c = 200.00;
        insert item4;
        
        // Refresh the Items                  
        item1 = getItem(item1.ID);
        item2 = getItem(item2.ID); 
        item3 = getItem(item3.ID);
        item4 = getItem(item4.ID);                  
        
        RCR_Contract_Scheduled_Service__c css = [SELECT ID,
                                                        Name,
                                                        Service_Code__c,
                                                        Start_Date__c,
                                                        End_Date__c,
                                                        Flexibility__c,
                                                        Public_Holidays_Not_Allowed__c,
                                                        Total_Cost__c,
                                                        Service_Description__c,
                                                        Service_Types__c,                                                                       
                                                        RCR_Service__c,
                                                        RCR_Service__r.Name,
                                                        RCR_Contract__r.Name,
                                                        RCR_Contract__r.Level__c,
                                                        RCR_Contract__r.Start_Date__c,
                                                        RCR_Contract__r.End_Date__c,
                                                        Exception_Count__c,
                                                        Use_Negotiated_Rates__c,
                                                        Negotiated_Rate_Standard__c,
                                                        Negotiated_Rate_Public_Holidays__c
                                                 FROM   RCR_Contract_Scheduled_Service__c
                                                 WHERE  /*Service_Code__c = 'TESTCODE1'
                                                 AND*/    RCR_Contract__c = :so.ID
                                                 LIMIT 1];
        
        RCR_Contract_Adhoc_Service__c cas = [SELECT Id,
                                                    Name,
                                                    Service_Types__c,
                                                    Start_Date__c,
                                                    End_Date__c,
                                                    Amount__c
                                             FROM   RCR_Contract_Adhoc_Service__c
                                             WHERE  RCR_Contract__c = :so.ID
                                             LIMIT 1];
        
        
        RCRReconcileBatch batch = new RCRReconcileBatch();
        
        batch.reconcileSingleItem(item1, css, null);
        
        batch = new RCRReconcileBatch();
        batch.reconcileSingleItem(item2, null, cas);
        
        batch = new RCRReconcileBatch();
        batch.reconcileSingleItem(item3, css, null);
        
        batch = new RCRReconcileBatch();
        batch.reconcileSingleItem(item4, null, cas);

        // now do for ahoc servcie with rates
        batch = new RCRReconcileBatch();
        batch.loadReferenceDataMaps(item2, so);
        RCR_Service__c service = batch.services.get(item2.Code__c);
        service.No_Set_Rates__c = false;
        update service; 
        
        /*
        RCR_Service_Rate__c serviceRate = new RCR_Service_Rate__c(RCR_Service__c = service.ID);
        serviceRate.Rate__c = 1000.00;
        serviceRate.Start_Date__c = so.Start_Date__c.addMonths(12);
        serviceRate.End_Date__c = so.End_Date__c.addMonths(12);
        insert serviceRate;
        */
        
        batch = new RCRReconcileBatch();
        batch.reconcileItem(item2);
        
        system.assert(RCRReconcileBatch.Holidays != null);
        system.assert(RCRReconcileBatch.HalfDayHolidays != null);
                
        for(RCR_Invoice_Item__c i : [SELECT r.Status__c, 
                                            r.Service_Order_Number__c,
                                            r.RCR_Invoice__r.Account__c,
                                            r.RCR_Service_Order__r.Name, 
                                            r.RCR_Contract_Scheduled_Service__r.Name, 
                                            r.RCR_Contract_Adhoc_Service__r.Name, 
                                            r.Force_Reconcile_To__c, 
                                            r.Exceeds_Tolerance__c, 
                                            r.Error__c, 
                                            r.Code__c 
                                     FROM   RCR_Invoice_Item__c r
                                     WHERE  r.RCR_Invoice__c = :inv.Id])
        {
            system.debug(i.Service_Order_Number__c + ' - ' + i.RCR_Invoice__r.Account__c);
        }
        
        Test.StopTest();
    }
    
    @IsTest//(SeeAllData=true)
    private static void testBadSoName()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        TestLoadData.loadAdhocServices();
    
        // TESTING RECONCILE SINGLE ITEM
        RCR_Contract__c so = getServiceOrder(); 
        so.Visible_To_service_providers__c = true;
        so.CSA_Status__c = APS_PicklistValues.RCRContractCSAStatus_Accepted;
        update so;
        /*RCR_CBMS_Contract__c cbmsContract = new RCR_CBMS_Contract__c(ID = so.RCR_CBMS_Contract__c);
        cbmsContract.Signed_By_All_Parties__c = so.Start_Date__c;
        update cbmsContract;*/
        
        Date activityDate = so.Start_Date__c.addDays(1);
         
        RCR_Invoice__c inv = createInvoice();
        
        RCR_Invoice_Item__c item1 = new RCR_Invoice_Item__c();
        item1.RCR_Invoice__c = inv.Id;
        item1.Unique_Identifier__c = 'item1';
        item1.Activity_Date__c = activityDate;
        item1.Code__c = 'TESTCODE1';
        item1.GST__c = 0.01;
        item1.Quantity__c = 1.0;
        item1.Rate__c = 0.1;
        item1.Service_Order_Number__c = 'TestSOX';  
        item1.Total_Item_GST__c = 0.01;
        item1.Total__c = 0.11;
        insert item1;
        
        item1 = getItem(item1.ID);
        
        RCRReconcileBatch batch = new RCRReconcileBatch();
        
        batch.reconcileItem(item1);
        Test.stopTest(); 
    }
    
    @IsTest//(SeeAllData=true)
    private static void testBadSoCBMS()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        TestLoadData.loadAdhocServices();
    
        // TESTING RECONCILE SINGLE ITEM
        RCR_Contract__c so = getServiceOrder(); 
//        so.RCR_CBMS_Contract__c = null;
        update so;
                                    
        Date activityDate = so.Start_Date__c.addDays(1);
         
        RCR_Invoice__c inv = createInvoice();

        RCR_Invoice_Item__c item1 = new RCR_Invoice_Item__c();
        item1.RCR_Invoice__c = inv.Id;
        item1.Unique_Identifier__c = 'item1';
        item1.Activity_Date__c = activityDate;
        item1.Code__c = 'TESTCODE1';
        item1.GST__c = 0.01;
        item1.Quantity__c = 1.0;
        item1.Rate__c = 0.1;
        item1.Service_Order_Number__c = so.Name;    
        item1.Total_Item_GST__c = 0.01;
        item1.Total__c = 0.11;
        insert item1;
        
        item1 = getItem(item1.ID);
        
        RCRReconcileBatch batch = new RCRReconcileBatch();
       
        batch.reconcileItem(item1);
        Test.stopTest();
    }
    
    
    @IsTest//(SeeAllData=true)
    private static void testBadSoFullProc()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
        
        TestLoadData.loadAdhocServices();
        
        // TESTING RECONCILE SINGLE ITEM
        RCR_Contract__c so = getServiceOrder(); 
        so.Visible_To_service_providers__c = true;
        so.Payments_Fully_Processed__c = true;
        update so;
       /* RCR_CBMS_Contract__c cbmsContract = new RCR_CBMS_Contract__c(ID = so.RCR_CBMS_Contract__c);
        cbmsContract.Signed_By_All_Parties__c = so.Start_Date__c;
        update cbmsContract;*/

        RCR_Invoice__c inv = createInvoice();
        
        RCR_Invoice_Item__c item1 = new RCR_Invoice_Item__c();
        item1.RCR_Invoice__c = inv.Id;
        item1.Unique_Identifier__c = 'item1';
        item1.Activity_Date__c = so.Start_Date__c.addDays(1);
        item1.Code__c = 'TESTCODE1';
        item1.GST__c = 0.01;
        item1.Quantity__c = 1.0;
        item1.Rate__c = 0.1;
        item1.Service_Order_Number__c = so.Name;
        item1.Total_Item_GST__c = 0.01;
        item1.Total__c = 0.11;
        insert item1;
        
        item1 = getItem(item1.ID);        
        
        RCRReconcileBatch batch = new RCRReconcileBatch();
        
        batch.reconcileItem(item1);
        Test.stopTest();
    }
    
    @IsTest//(SeeAllData=true)
    private static void testgetSchedServ()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        TestLoadData.loadAdhocServices();
    
        // TESTING RECONCILE SINGLE ITEM
        RCR_Contract__c so = getServiceOrder(); 
        RCR_Invoice__c inv = createInvoice();
        so.Visible_To_service_providers__c = true;
        so.CSA_Status__c = APS_PicklistValues.RCRContractCSAStatus_Accepted;
        update so;
        /*RCR_CBMS_Contract__c cbmsContract = new RCR_CBMS_Contract__c(ID = so.RCR_CBMS_Contract__c);
        cbmsContract.Signed_By_All_Parties__c = so.Start_Date__c;
        update cbmsContract;*/
        
        RCR_Invoice_Item__c item = new RCR_Invoice_Item__c();
        item.RCR_Invoice__c = inv.Id;
        item.Unique_Identifier__c = 'item1';
        item.Activity_Date__c = so.Start_Date__c.addDays(1);
        item.Code__c = 'TEST 2';
        item.GST__c = 0.01;
        item.Quantity__c = 1.0;
        item.Rate__c = 0.1;
        item.Service_Order_Number__c = 'TestSO';    
        item.Total_Item_GST__c  = 0.01;
        item.Total__c = 0.11;
        insert item;
        
        item = getItem(item.ID);        
        
        RCRReconcileBatch batch = new RCRReconcileBatch();
        batch.loadReferenceDataMaps(item, so);
                
        for(RCR_Service__c service : batch.services.values())
        {
            
            batch.getScheduledService(item, service);
            //batch.getCurrentServiceRate(item, service, batch.scheduledServicesByServiceCode.get(item.Code__c));
            batch.getAdhocService(item, service); 
        }
        
        batch = new RCRReconcileBatch();        
        inv.Override_validation__c = true;
        update inv;
        
        batch.loadReferenceDataMaps(item, so);
        
        for(RCR_Service__c service : batch.services.values())
        {
            batch.getScheduledService(item, service);
            //batch.getCurrentServiceRate(item, service, batch.scheduledServicesByServiceCode.get(item.Code__c));
            batch.getAdhocService(item, service);
        }
        
        for(RCR_Service__c service : batch.services.values())
        {
            for(RCR_Contract_Scheduled_Service__c css : service.RCR_Contract_Scheduled_Services__r)
            {
                css.Flexibility__c = APS_PicklistValues.RCRContractScheduledService_Flexibilty_Accumulative;
                batch.checkPeriodTotalExceeded(item, service, css);
                
                css.Flexibility__c = APS_PicklistValues.RCRContractScheduledService_Flexibilty_WithinThePeriod;
                css.Public_holidays_not_allowed__c = APS_PicklistValues.RCRContractScheduledService_PublicHolidaysNotAllowed_UsePublicHolidayRates;
                batch.checkPeriodTotalExceeded(item, service, css);
                
                css.Flexibility__c = APS_PicklistValues.RCRContractScheduledService_Flexibilty_AcrossTheSucceedingPeriod;
                batch.checkPeriodTotalExceeded(item, service, css);
            }
        }
        
        batch = new RCRReconcileBatch();  
        batch.loadReferenceDataMaps(item, so);
        for(RCR_Service__c service : batch.services.values())
        {
            for(RCR_Contract_Scheduled_Service__c css : service.RCR_Contract_Scheduled_Services__r)
            {                          
                css.Use_Negotiated_Rates__c = true;
                batch.getCurrentServiceRate(item, service, batch.scheduledServicesByServiceCode.get(item.Code__c));
                break;
            }
        }

        Test.stopTest();
    }
    
    @IsTest//(SeeAllData=true)
    private static void testTolerance()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        TestLoadData.loadAdhocServices();
    
        RCR_Contract__c so = getServiceOrder();
        RCR_Invoice__c inv = createInvoice();
        RCR_Invoice_Item__c item = new RCR_Invoice_Item__c();
        item.RCR_Invoice__c = inv.ID;
        item.Unique_Identifier__c = 'item1';
        item.Activity_Date__c = so.Start_Date__c.addDays(1);
        item.Code__c = 'TEST 2';
        item.GST__c = 0.01;
        item.Quantity__c = 1.0;
        item.Rate__c = 0.1;
        item.Service_Order_Number__c = 'TestSO';    
        item.Total_Item_GST__c = 0.01;
        item.Total__c = 0.11;
        item.Total_Exceeded_Below_Tolerance__c = false;
        insert item;
        
        RCR_Invoice_Item__c item2 = new RCR_Invoice_Item__c();
        item2.RCR_Invoice__c = inv.ID;
        item2.RCR_Contract_Scheduled_Service__c = so.RCR_Contract_Scheduled_Services__r[0].ID;
        item2.Unique_Identifier__c = 'item1';
        item2.Activity_Date__c = so.Start_Date__c.addDays(1);
        item2.Code__c = 'TESTCODE1';
        item2.GST__c = 0.01;
        item2.Quantity__c = 1.0;
        item2.Rate__c = 0.1;
        item2.Service_Order_Number__c = 'TestSO';   
        item2.Total_Item_GST__c = 0.01;
        item2.Total__c = 0.11;
        item2.Total_Exceeded_Below_Tolerance__c = false;
        insert item2;
        
        RCR_Invoice_Item__c item3 = new RCR_Invoice_Item__c();
        item3.RCR_Invoice__c = inv.ID;
        item3.RCR_Contract_Adhoc_Service__c = so.RCR_Contract_Adhoc_Services__r[0].ID;
        item3.Unique_Identifier__c = 'item1';
        item3.Activity_Date__c = so.Start_Date__c.addDays(1);
        item3.Code__c = 'TESTCODE1';
        item3.GST__c = 0.01;
        item3.Quantity__c = 1.0;
        item3.Rate__c = 0.1;
        item3.Service_Order_Number__c = 'TestSO';   
        item3.Total_Item_GST__c = 0.01;
        item3.Total__c = 0.11;
        item3.Total_Exceeded_Below_Tolerance__c = false;
        insert item3;
           
        item = getItem(item.ID);        

        RCRReconcileBatch batch = new RCRReconcileBatch();
        batch.loadReferenceDataMaps(item, so);
                
        for(RCR_Contract_Scheduled_Service__c css : so.RCR_Contract_Scheduled_Services__r) 
        {
            //batch.getDollarsAlreadyReconciled(item, css.Start_Date__c, css.End_Date__c, css, null); 
        }       
        for(RCR_Contract_Adhoc_Service__c ahs : so.RCR_Contract_Adhoc_Services__r) 
        {
            batch.checkTolerances(item, ahs, true);
            batch.checkTolerances(item, ahs, false);
        }
        
        item.Total_Exceeded_Below_Tolerance__c = true;
        for(RCR_Contract_Scheduled_Service__c css : so.RCR_Contract_Scheduled_Services__r) 
        {
            //batch.getDollarsAlreadyReconciled(item, css.Start_Date__c, css.End_Date__c, css, null);
        }
        for(RCR_Contract_Adhoc_Service__c ahs : so.RCR_Contract_Adhoc_Services__r)
        {
            batch.checkTolerances(item, ahs, true);
            batch.checkTolerances(item, ahs, false);
        }
        Test.stopTest();
    }
    @IsTest//(SeeAllData=true)
    private static void testGST()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        TestLoadData.loadAdhocServices();
    
        RCRReconcileBatch batch = new RCRReconcileBatch();

        RCR_Invoice_Item__c item = new RCR_Invoice_Item__c();
        item.GST__c = null;
        item.Quantity__c = null;
        item.Rate__c = 0.1; 
        item.Total_Item_GST__c = null;
        item.Total__c = null;
        
        batch.checkGSTandTotal(item);
        
        item.GST__c = 0.01;
        batch.checkGSTandTotal(item);
        
        item.Quantity__c = 1.00;
        batch.checkGSTandTotal(item);
        
        item.Total__c = 0.11;
        batch.checkGSTandTotal(item);
        
        item.Total_Item_GST__c = 0.01;
        batch.checkGSTandTotal(item);
        
        item.Total_Item_GST__c = null;
        item.Rate__c = 10.0;
        item.GST__c = item.Rate__c.divide(10.0, 2, System.RoundingMode.HALF_UP) - batch.Tolerance.GST_Lower__c - 0.02; 
        batch.checkGSTandTotal(item);
        
        
        item.Rate__c = 0.1;
        item.Total__c = 0.11;
        item.Quantity__c = 1.00;
        item.GST__c = 0.01;
        item.Total__c  = (item.Quantity__c * (item.Rate__c + item.GST__c)).setScale(2, System.RoundingMode.HALF_UP) - batch.Tolerance.GST_Lower__c - 0.01;
        batch.checkGSTandTotal(item);
        
        Test.StopTest();
    }
    
    @IsTest
    private static void testMore()
    {
        RCRReconcileBatch b = new RCRReconcileBatch();
                
        try { system.debug(RCRReconcileBatch.HalfDayHolidays); } catch(Exception e) { } 
        try { b.validateScheduledService(null, null, null, null); } catch(Exception e) { } 
        try { b.getValidItemRate(null, null, null, null); } catch(Exception e) { }
        try { b.getCurrentServiceRate(null, null, null); } catch(Exception e) { } 
        try { b.validateAdHocService(null, null, null); } catch(Exception e) { } 
        try { b.getItemToBeReversed(null); } catch(Exception e) { } 
        try { b.checkPeriodTotalExceeded(null, null, null); } catch(Exception e) { } 
        try { b.checkPeriodTolerance(null, null, null, null); } catch(Exception e) { } 
        try { b.checkTotalExceeded(null, null, null); } catch(Exception e) { } 
        try { b.getDollarsForPeriod(null, null, null, null, null, null); } catch(Exception e) { } 
        try { b.getDollarsAlreadyReconciled(null); } catch(Exception e) { }         
    }
    
    //@IsTest//(SeeAllData=true)
    public static RCR_Invoice__c createInvoice()
    {            
        RCR_Contract__c so = getServiceOrder();             
        RCR_Invoice__c inv = new RCR_Invoice__c();
        inv.Account__c = so.Account__c;
        inv.RecordTypeId = A2HCUtilities.getRecordType('RCR_Invoice__c', 'Processing').Id;
        inv.Date__c = so.Start_Date__c;
        inv.Start_Date__c = so.Start_Date__c;
        inv.End_Date__c =  so.End_Date__c;
        inv.Invoice_Number__c = 'TEST INV';
        inv.Schedule__c = scheduleID;
        insert inv;
        
        return inv;                
    }
    
    //@IsTest//(SeeAllData=true)
    public static RCR_Contract__c getServiceOrder()
    {            
        return [SELECT  Id,
                         Name,
                         Start_Date__c,
                         End_Date__c,
                         CBMS_Contract_Value__c,
                         Signed_By_All_Parties__c,
                         CSA_Status__c,
                         Visible_To_Service_Providers__c,
                         Total_Service_Order_Cost__c,
                         Payments_Fully_Processed__c,                                        
                         Level__c,
                         Account__c,
//                         RCR_CBMS_Contract__c,
                         RecordTypeId,
                         RecordType.Name,
                         (SELECT ID,
                                Name,
                                Service_Code__c,
                                Start_Date__c,
                                End_Date__c,
                                Flexibility__c,
                                Public_Holidays_Not_Allowed__c,
                                Use_Negotiated_Rates__c,
                                Negotiated_Rate_Standard__c,
                                Negotiated_Rate_Public_Holidays__c,
                                Total_Cost__c,
                                RCR_Service__c,
                                RCR_Service__r.Name,
                                RCR_Contract__r.Name,
                                RCR_Contract__r.Level__c,
                                RCR_Contract__r.Start_Date__c,
                                RCR_Contract__r.End_Date__c,
                                Exception_Count__c
                          FROM   RCR_Contract_Scheduled_Services__r),
                         (SELECT ID,
                                Name,
                                Start_Date__c,
                                End_Date__c,
                                Amount__c,
                                RCR_Account_Program_Category__c,
                                RCR_Account_Program_Category__r.Name,
                                RCR_Sub_Program__c
                          FROM  RCR_Contract_Adhoc_Services__r),
                         (SELECT    Id,
                                    Maximum_Amount__c,
                                    Total_Approved__c
                          FROM      RCR_Funding_Details__r
                          ORDER By  Maximum_Amount__c desc)
                FROM    RCR_Contract__c
                //WHERE   Name = 'TestSO'
                LIMIT 1];                                                    
    }
    
    private static RCR_Invoice_Item__c getItem(String itemID)
    {           
        return [SELECT ID,
                        Name,
                        Activity_Date__c,
                        Code__c,
                        Comment__c,
                        Error__c,
                        Quantity__c,
                        Rate__c,
                        GST__c,
                        Total__c,
                        Total_GST__c,
                        Total_ex_GST__c,
                        Exceeds_Tolerance__c,
                        Override_Tolerance__c,
                        Service_Order_Number__c,
                        Unique_Identifier__c,
                        Period_Start_Date__c,
                        Period_End_Date__c,
                        Summary_AdHoc_Item__c,
                        Write_To_Log__c,
                        Force_Reconcile_To__c,
                        Reconcile_Log__c,
                        RCR_Invoice__c,
                        RCR_Invoice__r.Account__c,
                        RCR_Invoice__r.Override_validation__c
                FROM    RCR_Invoice_Item__c
                WHERE   ID = :itemID];                                       
    }
}