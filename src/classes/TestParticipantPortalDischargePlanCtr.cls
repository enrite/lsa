@isTest
public class TestParticipantPortalDischargePlanCtr
{                    
    static testMethod void myTestMethod()
    {
        TestLoadFormData data = new TestLoadFormData();
                 
        Test.StartTest();
        
        ParticipantPortalPlanController c = new ParticipantPortalPlanController();
        c.Redirect();
                                                                         
        Plan__c p = new Plan__c();
        p.Client__c = data.Client.Id;
        p.RecordTypeId = data.RecordTypes.get('Discharge_Plan').Id;
        p.Plan_Status__c = 'Draft';
        insert p;
            
        c.Redirect();
        
        Test.StopTest();
    }
}