public with sharing class PlanExpiryScheduled implements Schedulable
{
    public void execute(SchedulableContext param1)
    {
        List<Task> tasks = new List<Task>();
        List<Plan__c> plans = [SELECT   ID,
                                        Name,
                                        Client__r.Allocated_to__c,
                                        Client__r.Full_Name__c,
                                        Plan_End_Date__c,
                                        RecordType.Name,
                                        (SELECT ID
                                        FROM    Tasks
                                        WHERE   Plan_End_Date_Task__c = true)
                                FROM    Plan__c
                                WHERE   End_Date_Approaching__c = true];
        String baseURL = URL.getSalesforceBaseUrl().toExternalForm();
        for(Plan__c plan : plans)
        {
            if(plan.Tasks.isEmpty())
            {
                Task t = new Task();
                t.OwnerId = plan.Client__r.Allocated_to__c;
                t.ActivityDate = plan .Plan_End_Date__c;
                t.WhatId = plan.ID;
                t.Plan_End_Date_Task__c = true;
                t.Subject = 'Plan ending soon';
                t.Description = 'Please be aware the ' + plan.RecordType.Name + ' '  + plan.Name + ' for ' +
                        plan.Client__r.Full_Name__c + ' is ending on ' + plan.Plan_End_Date__c.format() + '. Consider commencing next plan soon.';
                tasks.add(t);
            }
        }
        insert tasks;
    }
}