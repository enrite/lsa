@isTest
public class TestLatestMedicalInformationExtension
{                   
    static testMethod void myTestMethod()
    {
        TestLoadFormData data = new TestLoadFormData();
         
        Test.StartTest();         
        
        Form__c mc = new Form__c(RecordTypeId = data.RecordTypes.get('Medical_Certificate').Id);
        mc.Client__c = data.Client.Id;
        insert mc;     
        
        LatestMedicalInformationExtension ext = new LatestMedicalInformationExtension(new ApexPages.StandardController(data.Client));
        system.debug(ext.LatestMedicalCertificate);
        
        Test.StopTest();       
    }
}