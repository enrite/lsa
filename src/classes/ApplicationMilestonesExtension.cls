/**
 * Created by USer on 07/09/2016.
 */

public with sharing class ApplicationMilestonesExtension
{
    public ApplicationMilestonesExtension(ApexPages.StandardController ctrlr)
    {
        milestone = (Grant_Milestone__c)ctrlr.getRecord();
    }

    private Grant_Milestone__c milestone;

    private transient List<Attachment> tAttachments;

    public List<Attachment> Attachments
    {
        get
        {
            if(tAttachments == null)
            {
                tAttachments = [SELECT ID,
                                        Name
                                FROM    Attachment
                                WHERE   ParentID = :milestone.ID];
            }
            return tAttachments;
        }
    }

    public PageReference saveOverride()
    {
        try
        {
            update milestone;
            PageReference pg = Page.GPProjectMilestones;
            pg.getParameters().put('id', milestone.Application__c);
            return pg;
        }
        catch(Exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }
}