@isTest
public class TestClientViewExtension
{                    
    static testMethod void myTestMethod()
    {
        TestLoadFormData data = new TestLoadFormData();
        
        Test.StartTest();
    
        Case_Notes__c cn = new Case_Notes__c(Client__c = data.Client.Id, Note_Date__c = Date.today().addDays(-1));
        insert cn;
        
        Notification__c e = new Notification__c(Client__c = data.Client.Id);
        insert e;
    
        Attachment a = new Attachment();
        a.ParentId = data.Client.Id;
        a.Name = 'test';
        a.Body = Blob.valueOf('test');
        insert a;
        
        Test.setCurrentPage(Page.ClientView);      
        ClientViewExtension ext = new ClientViewExtension(new ApexPages.StandardController(data.Client));
        ext.FindRows(1);
      
        ext.DeleteId = cn.Id;
        ext.DeleteRecord();
    
        Test.StopTest();
    }
}