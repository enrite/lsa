@isTest(seeAllData=true)
private class TestRCRSupportEmailController 
{
    static testMethod void myUnitTest() 
    {
        RCRSupportEmailController c = new RCRSupportEmailController();
        
        
        system.debug(c.ScreenIssueOptions);
        system.debug(c.SystemActionOptions);
        c.sendEmail();
        
        c.Subject = 'Hello World';
        c.MessageDetails = 'Testing';
        c.ContactPhoneNumber = '8888 8888';
        c.SelectedScreenIssue = c.ScreenIssueOptions[1].getValue();
        c.SelectSystemAction = c.SystemActionOptions[1].getValue();
        
        system.debug(c.Subject);
        system.debug(c.MessageDetails);
        system.debug(c.ContactPhoneNumber);
        system.debug(c.SelectedScreenIssue);
        system.debug(c.SelectSystemAction);
        
        c.sendEmail();
    }
}