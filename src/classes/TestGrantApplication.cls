@IsTest
public class TestGrantApplication
{
    private static testMethod void test1()
    {
        Grant_Round__c round = new Grant_Round__c();
        round.Open_Date__c = Date.today().addDays(-1);
        round.Close_Date__c = Date.today().addDays(1);
        insert round;

        Grant_Application__c application = new Grant_Application__c(Grant_Round__c = round.ID);
        application.Amount_Applied_for__c = 1.0;
        application.Brief_Project_Summary__c = 'test';
        application.Contact_Name__c = 'test';
        application.Contact_Position__c = 'test';
        application.Name_of_Project__c = 'test';
        application.Amount_Approved__c = 1.0;
        insert application;

        Grant_Milestone__c milestone = new Grant_Milestone__c(Application__c = application.ID);
        milestone.Due_Date__c = Date.today();
        milestone.Description__c = 'test';
        milestone.Milestone_Payment__c = 1.0;
        insert milestone;

        application.Status__c = 'Approved';
        application.Date_Approved__c = Date.today();
        application.Approval_Queue_Name__c = 'Tier_1';
        application.Due_Date_For_Acceptance__c = Date.today();
        update application;

    }
}