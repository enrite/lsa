global class RCRCreateReconcileBatch implements Schedulable
{
    global RCRCreateReconcileBatch(Boolean useNew)
    {
        this.useNew = useNew;
    }

    private Boolean useNew = true;

	global void execute(SchedulableContext SC) 
    {
        if(useNew)
        {
            RCRReconcileBatchNew batch = new RCRReconcileBatchNew();
            batch.ScheduleID = SC.getTriggerID();
            ID batchprocessid = Database.executeBatch(batch, Batch_Size__c.getInstance('LP_Reconcile').Records__c.IntValue());
        }
        else
        {
            RCRReconcileBatch batch = new RCRReconcileBatch();
            batch.ScheduleID = SC.getTriggerID();
            ID batchprocessid = Database.executeBatch(batch, Batch_Size__c.getInstance('RCR_Reconcile').Records__c.IntValue());
        }
    }
}