global without sharing class RCRUploadSummaryController extends A2HCPageBase
{
    public RCRUploadSummaryController(ApexPages.StandardController cont)
    {
        FileUploadId = getParameter('id');
        if(FileUploadId != null)
        {
            Invoices = getInvoices();
        }
        else
        {
            FileUpload = new RCR_Invoice_Upload__c();
        }

        for(Contact c :[SELECT  	Account.Name
                            FROM    Contact
                            WHERE   ID IN(SELECT    ContactId
                                        FROM        User
                                        WHERE       ID = :UserInfo.getUserId())])
        {
            OrgName = c.Account.Name;
        }
    }

    public RCR_Invoice_Upload__c FileUpload
    {
        get
        {
            if(FileUpload == null)
            {
                FileUpload = [SELECT    Name,
                                        File_Name__c,
                                        Description__c,
                                        Account__c,
                                        Account__r.Name
                                FROM    RCR_Invoice_Upload__c
                                WHERE   ID = :FileUploadId];
            }
            return FileUpload;
        }
        set;
    }

    public boolean disableButton
    {
    	get;
    	set;
    }

    public String FileUploadId
    {
        get;
        set;
    }

    public Date EndDate
    {
        get;
        set;
    }

    public Date StartDate
    {
        get;
        set;
    }

    public String OrgName
    {
        get;
        set;
    }

    public List<RCR_Invoice__c> Invoices
    {
        get
        {
            if(Invoices == null)
            {
                Invoices = new List<RCR_Invoice__c>();
            }
            return Invoices;
        }
        set;
    }

    public List<Application_Help_Item__c> HelpItems
    {
        get
        {
            return [SELECT  c.Name,
                            c.Help_Heading__c,
                            c.Help_Text__c
                    FROM    Application_Help_Item__c c
                    WHERE   c.Application__c = 'RCR'];
        }
    }

    public PageReference reconcileAll()
    {
        try
        {
        	Invoices = getInvoices();
			List<RCR_Invoice__c> invoicesToProcess = new List<RCR_Invoice__c>();
        	for(RCR_Invoice__c inv : Invoices)
        	{
        		if(inv.Status__c != APS_PicklistValues.RCRInvoice_Status_Approved &&
        				inv.Status__c != APS_PicklistValues.RCRInvoice_Status_PaymentProcessed &&
        				inv.Status__c != APS_PicklistValues.RCRInvoice_Status_ReconciliationInProgress)
				{
        			inv.Reconciliation_User__c = UserInfo.getUserId();
        			invoicesToProcess.add(inv);
				}
        	}
        	update invoicesToProcess;
			
			RCRInvoiceSchedule invoiceSchedule = new RCRInvoiceSchedule();
        	invoiceSchedule.scheduleReconciliation(invoicesToProcess.deepClone(true));
        	Invoices = getInvoices();

        	A2HCException.formatException(ApexPages.Severity.INFO, 'Your activity statement(s) has been scheduled for reconciliation.');

			return null;
        }
        catch(Exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }


    public PageReference cancel()
    {
        try
        {
            String retURL = getParameter('retURL');
			String searchCriteria = getParameter('srch');
			searchCriteria = searchCriteria == null ? '' : searchCriteria;
        	PageReference pg = null;
        	if(retURL != null)
        	{
        		pg = new PageReference(retURL + (retURL.contains('?') ? '&' : '?') + 'srch=' + searchCriteria);
        	}
        	else
        	{
        		pg = Page.RCRInvoiceSearch;
        		pg.getParameters().put('srch', searchCriteria);
        	}
        	return pg.setRedirect(true);
        }
        catch(Exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }

    private List<RCR_Invoice__c> getInvoices()
    {
    	disableButton = false;

    	List<RCR_Invoice__c> rcrInvoices = new List<RCR_Invoice__c>();
    	Set<String> scheduleIDs = new Set<String>();

        for(RCR_Invoice__c inv : [SELECT  	i.ID,
				                            i.Invoice_Number__c,
				                            i.Total__c,
				                            i.Status__c,
				                            i.Start_Date__c,
				                            i.End_Date__c,
				                            i.Account__c,
				                            i.Ready_For_Reconciliation__c,
				                            i.Reconciliation_User__c,
				                            i.Processing__c,
				                            i.Account__r.Name,
				                            i.Schedule__c,
				                            i.Case_safe_schedule_ID__c
				                    FROM    RCR_Invoice__c  i
				                    WHERE   i.RCR_Invoice_Upload__c = :FileUploadId
				                    ORDER BY i.Invoice_Number__c DESC])
        {
        	rcrInvoices.add(inv);
        	scheduleIDs.add(inv.Case_safe_schedule_ID__c);

        	if(inv.Processing__c > 0)
        	{
        		disableButton = true;
        	}
        }

        // If disableButton is still false we also need to check to see if any of the invoices have been scheduled for reconciliation.
        integer scheduleCount = [SELECT count()
        						 FROM 	CronTrigger
        						 WHERE	Id IN :scheduleIDs
        						 AND	State IN ('WAITING', 'ACQUIRED')];

		if(scheduleCount > 0)
		{
			disableButton = true;
		}

		return rcrInvoices;
    }

	
}