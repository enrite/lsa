public with sharing class RCRNewCBMSContractRedirect 
    extends A2HCPageBase
{
    public RCRNewCBMSContractRedirect(ApexPages.StandardController cont)
    {
    }
    
    public PageReference redir()
    {
        try
        {
            return null;
        }
        catch(Exception ex)
        {
            return A2HCException.formatException(ex); 
        }
    }
}