global class ScheduledSetCssGLCategory implements Schedulable 
{
    global void execute(SchedulableContext sc) 
    {
    	String s;
		List<RCR_Contract_Scheduled_Service__c> cssList = [SELECT 	ID,
																	(SELECT ID,
																			RCR_Program_Category_Service_Type__r.RCR_Program_Category__r.Name
																	FROM 	RCR_Contract_Service_Types__r
																	WHERE 	RCR_Program_Category_Service_Type__r.RCR_Program_Category__r.Name != null)
														FROM 		RCR_Contract_Scheduled_Service__c
														WHERE 		//RCR_Contract__r.RecordType.DeveloperName = 'Service_Order' AND
                                                                    GL_Category__c = null AND 
																	ID IN (SELECT 	RCR_Contract_Scheduled_Service__c 
																			FROM 	RCR_Contract_Service_Type__c)];
		for(RCR_Contract_Scheduled_Service__c css : cssList)   
		{
			s = '';
            Set<String> sTypes = new Set<String>();
            for(RCR_Contract_Service_Type__c cst : css.RCR_Contract_Service_Types__r)
            {
                String sType = cst.RCR_Program_Category_Service_Type__r.RCR_Program_Category__r.Name;
                if(sTypes.contains(sType))
                {
                    continue;
                }
                sTypes.add(sType);
                s += sType + '\n';
                // this will be displayed through a formula field
                // so limit to less than 255 characters
                if(s.length() >= 240)
                {
                    s = s.left(240) + '... and more';
                    break;
                }
			}
			css.GL_Category__c = String.isBlank(s) ? null : s;
		} 
		TriggerBypass.RCRServiceOrder = true;
		Database.update(cssList, false);	

		// now update scheduled services where there is no service types, just use tha approved service type for the service
		cssList = new List<RCR_Contract_Scheduled_Service__c>();
		List<RCR_Contract_Service_Type__c> serviceTypeList = new List<RCR_Contract_Service_Type__c>();
		for(RCR_Service__c svc : [SELECT 	ID,
											(SELECT ID
											FROM 	RCR_Contract_Scheduled_Services__r
											WHERE 	RCR_Contract__r.RecordType.DeveloperName = 'Service_Order' AND 
                                                    GL_Category__c = null),
											(SELECT ID,
													RCR_Approved_Service_Type__r.RCR_Program_Category_Service_Type__c,
													RCR_Approved_Service_Type__r.RCR_Program_Category_Service_Type__r.RCR_Program_Category__r.RCR_Program__c,
													RCR_Approved_Service_Type__r.RCR_Program_Category_Service_Type__r.RCR_Program_Category__r.Name
											FROM 	RCR_Approved_Service_Type_Services__r
											WHERE 	RCR_Approved_Service_Type__r.RCR_Program_Category_Service_Type__r.RCR_Program_Category__r.Name != null)
								FROM 		RCR_Service__c
								WHERE 		ID IN (SELECT 	RCR_Service__c 
													FROM 	RCR_Contract_Scheduled_Service__c
													WHERE 	RCR_Contract__r.RecordType.DeveloperName = 'Service_Order' AND
                                                            GL_Category__c = null) AND 
											ID IN (SELECT 	RCR_Service__c
													FROM 	RCR_Approved_Service_Type_Service__c
													WHERE 	RCR_Approved_Service_Type__r.RCR_Program_Category_Service_Type__r.RCR_Program_Category__r.Name != null)])
		{
			Set<String> serviceTypes = new Set<String>();
			Set<ID> progCatServiceTypes = new Set<ID>();
			for(RCR_Approved_Service_Type_Service__c asts : svc.RCR_Approved_Service_Type_Services__r)
			{
				serviceTypes.add(asts.RCR_Approved_Service_Type__r.RCR_Program_Category_Service_Type__r.RCR_Program_Category__r.Name);
				progCatServiceTypes.add(asts.RCR_Approved_Service_Type__r.RCR_Program_Category_Service_Type__c);
			}
			s = '';
			for(String st : serviceTypes)
			{
				s += st + '\n';
                // this will be displayed through a formula field
                // so limit to less than 255 characters
                if(s.length() >= 240)
                {
                    s = s.left(240) + '... and more';
                    break;
                }
			}
		
			for(RCR_Contract_Scheduled_Service__c css : svc.RCR_Contract_Scheduled_Services__r)   
			{
				for(RCR_Approved_Service_Type_Service__c asts : svc.RCR_Approved_Service_Type_Services__r)
				{
					// only add one contract servoice type for each service type
					if(progCatServiceTypes.contains(asts.RCR_Approved_Service_Type__r.RCR_Program_Category_Service_Type__c))
					{
						RCR_Contract_Service_Type__c cst = new RCR_Contract_Service_Type__c();
						cst.RCR_Contract_Scheduled_Service__c = css.ID;
						cst.RCR_Program__c = asts.RCR_Approved_Service_Type__r.RCR_Program_Category_Service_Type__r.RCR_Program_Category__r.RCR_Program__c;
						cst.RCR_Program_Category_Service_Type__c = asts.RCR_Approved_Service_Type__r.RCR_Program_Category_Service_Type__c;
						serviceTypeList.add(cst);
						progCatServiceTypes.remove(asts.RCR_Approved_Service_Type__r.RCR_Program_Category_Service_Type__c);
					}
				}
				css.GL_Category__c = String.isBlank(s) ? null : s;
			}
			cssList.addAll(svc.RCR_Contract_Scheduled_Services__r);
		}	
		Database.update(cssList, false);
		Database.insert(serviceTypeList, false);	

		BatchSetPlanActionGLCategory b = new BatchSetPlanActionGLCategory();
		Database.executeBatch(b, 50);			
    }

}