@isTest
public class TestScheduledStaffAvailability
{
    public static TestMethod void myTestMethod() 
    {                   
        Staff_Availability__c sa = new Staff_Availability__c();
        sa.RecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Staff_Availability__c' AND Name = 'Staff Absences'].Id;
        sa.Start_Date__c = Date.today().addDays(1);
        sa.End_Date__c = Date.today().addDays(2);
        sa.Staff_Member__c = [SELECT Id FROM User WHERE CompanyName = 'LSA' LIMIT 1].Id;
                 
        Test.startTest();
                                
        String sched = '0 0 18 * * ? 2099';
        
        // Schedule the test job
        String jobId = System.schedule('TestScheduledStaffAvailability', sched, new ScheduledStaffAvailability());
        
        // Get the information from the CronTrigger API object
        CronTrigger ct = [SELECT id, 
                                 CronExpression, 
                                 TimesTriggered, 
                                 NextFireTime 
                          FROM   CronTrigger 
                          WHERE  id = :jobId];
                          
        // Verify the expressions are the same
        System.assertEquals(sched, ct.CronExpression);
        
        // Verify the job has not run
        System.assertEquals(0, ct.TimesTriggered);
        
        // Verify the next time the job will run
        System.assertEquals('2099-01-01 18:00:00', String.valueOf(ct.NextFireTime));      
        
        Test.stopTest();  
    }
}