public with sharing class SubmitFormExtension
{
    public SubmitFormExtension(ApexPages.StandardController c)
    {
        record = (Form__c)c.getRecord();  
        
        // if the form already has a client then it can't be changed        
        CanChangeClient = (record.Client__c == null) && !(record.RecordType.DeveloperName == 'FIM_Score_Sheet')&& !(record.RecordType.DeveloperName == 'FIM_FAM_Score_Sheet');
        
        // get details from the client if appropriate
        CopyDetailsFromClient();
    }
    
    private Form__c record;
    public String ClientId { get; set; }    
    public Boolean CanChangeClient { get; set; }    
    public String SelectedParentGuardian { get; set; }
    
    public PageReference CheckSubmitOrProcess()
    {
        PageReference ref;
        
        // if this is a discharge rehabilitation form then check if a discharge plan exists for the client
        if (record.RecordType.DeveloperName == 'Discharge_Rehabilitation_Form')
        {
            Boolean planFound = false;
        
            for (Plan__c p : [SELECT Id
                              FROM Plan__c
                              WHERE Client__c = :record.Client__c
                              AND RecordType.DeveloperName = 'Discharge_Plan'
                              AND Plan_Status__c = 'Draft'
                              LIMIT 1])
            {
                planFound = true;
            }                    
            
            // if no draft discharge plan was found then redirect to the error page
            if (!planFound)
            {
                ref = Page.ErrorPage;    
                ref.getParameters().put('title', 'Submit Form');
                ref.getParameters().put('subTitle', record.Name);
                ref.getParameters().put('errorMsg', 'A Discharge Rehabilitation Form cannot be processed if there is no draft Discharge Plan for the Participant');
                ref.getParameters().put('retURL', '/' + record.Id);
                
                return ref;
            }                                                                
        }
                        
        if (!FormEditExtension.ValidateForm(record, record.RecordType.DeveloperName, record.Motor_Vehicles__r))
        {     
            /*     
            // check if all required fields have been supplied
            if (record.RecordType.DeveloperName == 'Severe_Injury_Notification')
            {                
                ref = Page.SevereInjuryNotificationEdit;                                
            }
            else*/ 
            
            if (record.RecordType.DeveloperName.startsWith('Application_Form'))
            {               
                ref = Page.ApplicationFormEdit;                                
            }    
            else if (record.RecordType.DeveloperName.startsWith('Function_on_Discharge'))
            {               
                ref = Page.FunctionOnDischargeEdit;                                
            }         
            else if (record.RecordType.DeveloperName == 'Home_Modification_Preliminary_Scoping')
            {               
                ref = Page.HomeModificationEdit;                                
            }
            else if (record.RecordType.DeveloperName == 'Discharge_Rehabilitation_Form')
            {           
                ref = Page.DischargeRehabilitationEdit;                                
            }
            else if (record.RecordType.DeveloperName == 'FIM_Score_Sheet')
            {               
                ref = Page.FIMScoreSheetEdit;                                
            }
            else if (record.RecordType.DeveloperName == 'FIM_FAM_Score_Sheet')
            {
                ref = Page.FIMFAMScoreSheetEdit;
            }
            else if (record.RecordType.DeveloperName == 'Medical_Certificate')
            {               
                ref = Page.MedicalCertificateEdit;                                
            }
        }
                                
        if (ref != null)
        {
            ref.getParameters().put('id', record.Id);    
            ref.getParameters().put('preSubmit', '1');    
            ref.getParameters().put('saveURL', ApexPages.CurrentPage().getUrl());    
            ref.getParameters().put('retURL', ApexPages.CurrentPage().getParameters().get('retURL'));    
            
            return ref;
        }
    
        // if this is a community user then change the status to submitted and return to the view page
        if (UserInfo.getUserType() == 'CspLitePortal')
        {
            record.Status__c = 'Submitted';
            update record;
            
            return new PageReference('/' + record.Id);
        }
                        
        return null;
    }
    
    public List<Referred_Client__c> PossibleMatches
    {
        get
        {
            if (PossibleMatches == null)
            {                
                PossibleMatches = [SELECT Id,
                                          Full_Name__c,
                                          Given_Name__c,
                                          Family_Name__c,
                                          Address__c,
                                          Suburb__c,
                                          State__c,
                                          Postcode__c,
                                          Phone__c,
                                          Mobile__c,
                                          Date_Of_Birth__c
                                   FROM Referred_Client__c
                                   WHERE (Given_Name__c != null AND Given_Name__c = :record.First_Name__c)
                                   OR (Family_Name__c != null AND Family_Name__c = :record.Surname__c)
                                   OR (Date_of_Birth__c != null AND Date_of_Birth__c = :record.Date_of_Birth__c)
                                   OR (Phone__c != null AND Phone__c = :record.Home_Phone__c)
                                   OR (Mobile__c != null AND Mobile__c = :record.Mobile_Phone__c)];                                                                               
            }
            return PossibleMatches;
        }
        set;
    }
    
    public void AssignClient()
    {
        try
        {  
            record.Client__c = ClientId;
            CopyDetailsFromClient();        
            
            ParentGuardians = null;
            SelectedParentGuardian = null;                       
        }
        catch(Exception e)
        {
            A2HCException.formatException(e);
        }    
    }
    
    private void CopyDetailsFromClient()
    {    
        for (Referred_Client__c c : [SELECT Id,
                                            Account__c,
                                            Allocated_to__c                  
                                     FROM Referred_Client__c
                                     WHERE Id = :record.Client__c])
        {                                                     
            record.Hospital_Facility__c = c.Account__c;
            record.Allocated_to__c = c.Allocated_to__c;
        }
    }            
    
    // only show the potential parents/guardians if any info has been supplied for them
    public Boolean ShowParentGuardians 
    { 
        get
        {                     
            return record.Contact_Title__c != null
                   || record.Contact_First_Name__c != null
                   || record.Contact_Surname__c != null
                   || record.Contact_Street__c != null
                   || record.Contact_Suburb__c != null
                   //|| record.Contact_State__c != null
                   || record.Contact_Postcode__c != null
                   || record.Relationship_to_Injured_Person__c != null
                   || record.Contact_Email__c != null
                   || record.Contact_Home_Phone__c != null
                   || record.Contact_Work_Phone__c != null
                   || record.Contact_Mobile_Phone__c != null
                   || record.Contact_Date_of_Birth__c != null
                   || record.Is_an_Interpreter_Required_for_Contact__c != null
                   || record.Contact_Interpreter_Language__c != null
                   || record.Contact_ATSI_origin__c != null
                   || record.Contact_preferred_written_communication__c != null
                   || record.Contact_preferred_phone_number__c != null                   
                   || record.Lawful_authority_act_on_behalf__c != null
                   || record.Birth_Certificate__c
                   || record.Guardianship_Order__c
                   || record.Custody_Order__c
                   || record.If_yes_details_of_lawful_authority__c != null
                   || record.Guardianship_Type__c != null
                   || record.Guardianship_Type_Details__c != null;                                   
        } 
    }
    
    public List<SelectOption> ParentGuardians
    {
        get
        {
            if (ParentGuardians == null)
            {
                ParentGuardians = new List<SelectOption>();
                ParentGuardians.add(new SelectOption('', 'New Parent or Guardian'));
                
                for (Participant_Contact__c pc : [SELECT Id,
                                                         Title__c,
                                                         First_Name__c,
                                                         Surname__c,
                                                         Suburb__c,
                                                         Relationship_to_Participant__c
                                                  FROM Participant_Contact__c
                                                  WHERE Participant__c != null
                                                  AND Participant__c = :record.Client__c
                                                  AND RecordType.DeveloperName = 'Parent_or_Guardian'
                                                  ORDER BY Surname__c])
                {
                    String display = '';
                    display += (pc.Title__c != null ? pc.Title__c + ' ' : '');
                    display += pc.First_Name__c + ' ' + pc.Surname__c;
                    display += (pc.Suburb__c != null ? ' of ' + pc.Suburb__c : '');
                    display += ' (' + pc.Relationship_to_Participant__c + ')';
                
                    ParentGuardians.add(new SelectOption(pc.Id, display));                                            
                }                                                  
            }                        
            return ParentGuardians;
        }
        set;
    }
            
    public PageReference Process()
    {
        Savepoint sp = Database.setSavepoint();
    
        try
        {
            CopyDetailsToClient();
            
            record.Status__c = 'Submitted';
                                    
            update record;
            
            PageReference ref = new PageReference('/' + record.Id);
            
            // if this is an application form then perform some tasks
            if (record.RecordType.DeveloperName.startsWith('Application_Form'))
            {
                // create the service planner application assessment
                Form_Detail__c aa = new Form_Detail__c();
                aa.Form_Application_Assessment__c = record.Id;
                aa.Completed_By__c = record.Allocated_to__c;   
                aa.RecordTypeId = [SELECT Id
                                   FROM RecordType
                                   WHERE sObjectType = 'Form_Detail__c'
                                   AND DeveloperName = 'Service_Planner_Application_Assessment'].Id;               
                                                 
                insert aa;                                
                                
                /*ref = Page.GenerateLetter;
                ref.getParameters().put('pg', 'LetterOfAcknowledgementPDF');      
                ref.getParameters().put('id', record.Client__c);
                ref.getParameters().put('parent', record.Id);      
                ref.getParameters().put('file', 'Letter of Acknowledgement.pdf');      
                ref.getParameters().put('desc', 'Letter of Acknowledgement');                     
                ref.getParameters().put('retURL', '/' + record.Id);                     
                
                String email = record.Email__c;
                String preferredMethod = record.Preferred_form_of_written_communication__c;
                String subject;
                String body;
                
                if (ShowParentGuardians)
                {
                    ref.getParameters().put('pg', 'LetterOfAcknowledgementParentGuardianPDF'); 
                    ref.getParameters().put('id', SelectedParentGuardian);
                                             
                    email = record.Contact_Email__c;
                    preferredMethod = record.Contact_preferred_written_communication__c;
                    
                    for (EmailTemplate et : [SELECT Id,
                                                    Subject,
                                                    HTMLValue
                                             FROM EmailTemplate
                                             WHERE Name = 'Application Acknowledgement Parent/Guardian'])
                    {
                        subject = et.Subject;
                        body = et.HTMLValue;
                        body = body.replace('{!Participant_Contact__c.First_Name__c}', record.Contact_First_Name__c);
                        
                        //String behalf = (record.Relationship_to_Injured_Person__c == 'Parent' ? 'your ' + (record.Sex__c == 'Male' ? 'son ' : 'daughter ') : ' ') + record.First_Name__c;                        
                        body = body.replace('{!Participant_Contact__c.Participant_Relationship_for_Email__c}', record.First_Name__c);
                    }                                             
                }
                else
                {
                    for (EmailTemplate et : [SELECT Id,
                                                    Subject,
                                                    HTMLValue
                                             FROM EmailTemplate
                                             WHERE Name = 'Application Acknowledgement'])
                    {
                        subject = et.Subject;
                        body = et.HTMLValue;
                        body = body.replace('{!Referred_Client__c.Given_Name__c}', record.First_Name__c);
                    } 
                }           
                
                if (preferredMethod == 'email')
                {
                    ref.getParameters().put('email', email);
                    ref.getParameters().put('subject', subject);
                    ref.getParameters().put('bdy', body);
                } */    
                
                return ref;                                                                                
            }              
            // if this is a discharge rehabilitation form then create some records
            else if (record.RecordType.DeveloperName == 'Discharge_Rehabilitation_Form')  
            {
                List<Plan_Action__c> newPlanActions = new List<Plan_Action__c>();
                                                
                // find through the draft discharge plans for the client
                Map<Id, Plan__c> plans = new Map<Id, Plan__c> ([SELECT Id
                                                                FROM Plan__c
                                                                WHERE Client__c = :record.Client__c
                                                                AND RecordType.DeveloperName = 'Discharge_Plan'
                                                                AND Plan_Status__c = 'Draft']);
                                                                
                // loop through the standard list
                for (Form_Detail__c fd : [SELECT Id,
                                                 RecordType.DeveloperName,
                                                 Service_Type__c,
                                                 Quantity__c,
                                                 Related_to_Vehicle_Accident_Injuries__c
                                          FROM Form_Detail__c
                                          WHERE Form_Discharge_Rehabilitation_Standard__c = :record.Id])
                {
                    for (Id pId : plans.keySet())
                    {
                        Plan_Action__c pa = new Plan_Action__c();
                        pa.Participant_Plan__c = pId;
                        pa.RecordTypeId = RecordTypes.get('Plan_Action__c:Discharge_Rehabilitation_Standard_Services').Id;
                        pa.Pre_Defined_Services__c = fd.Service_Type__c;
                        pa.Quantity__c = fd.Quantity__c;
                        pa.Related_to_Vehicle_Accident_Injuries__c = fd.Related_to_Vehicle_Accident_Injuries__c;
                        
                        newPlanActions.add(pa);
                    }                    
                }                                             
                
                // loop through the non-standard list
                for (Form_Detail__c fd : [SELECT Id,
                                                 RecordType.DeveloperName,
                                                 Service_Type__c,
                                                 Other_Required_Services__c,
                                                 Quantity__c,
                                                 Related_to_Vehicle_Accident_Injuries__c
                                          FROM Form_Detail__c
                                          WHERE Form_Discharge_Rehab_Non_Standard__c = :record.Id])
                {
                    for (Id pId : plans.keySet())
                    {
                        Plan_Action__c pa = new Plan_Action__c();
                        pa.Participant_Plan__c = pId;
                        pa.RecordTypeId = RecordTypes.get('Plan_Action__c:Discharge_Rehabilitation_Non_Standard_Services').Id;
                        pa.Pre_Defined_Services__c = fd.Service_Type__c;  
                        pa.Item_Description__c = fd.Other_Required_Services__c;                      
                        pa.Quantity__c = fd.Quantity__c;
                        pa.Related_to_Vehicle_Accident_Injuries__c = fd.Related_to_Vehicle_Accident_Injuries__c;
                        
                        newPlanActions.add(pa);                                                
                    }                    
                }
                
                insert newPlanActions;
            } 
                       
            return ref;
        }
        catch(Exception e)
        {
            Database.rollBack(sp);
            return A2HCException.formatException(e);    
        }
    }
    
    public Map<String, RecordType> RecordTypes
    {
        get
        {
            if (RecordTypes == null)
            {
                RecordTypes = new Map<String, RecordType>();
                
                for (RecordType rt : [SELECT Id,
                                             Name,
                                             DeveloperName,
                                             sObjectType
                                      FROM RecordType
                                      WHERE sObjectType = 'Plan_Action__c'])
                {
                    RecordTypes.put(rt.sObjectType + ':' + rt.DeveloperName, rt);
                }                                                                            
            }
            return RecordTypes;
        }
        set;
    }
    
    private void CopyDetailsToClient()
    {     
        // if this is a FIM score sheet then we don't create a client from it
        if ((record.RecordType.DeveloperName == 'FIM_Score_Sheet' || record.RecordType.DeveloperName == 'FIM_FAM_Score_Sheet' )&& record.Client__c == null)
        {
            return;
        }
           
        Referred_Client__c client = new Referred_Client__c();
        
        // see if we can find a client that matches the Id - we may not if there's previously been a rollback
        for (Referred_Client__c c : [SELECT Id,                              
                                            Full_Name__c,
                                            Title__c,
                                            Given_Name__c,
                                            Family_Name__c,
                                            Address__c,
                                            Suburb__c,
                                            State__c,
                                            Postcode__c,
                                            Email__c,
                                            Phone__c,
                                            Mobile__c,
                                            Date_Of_Birth__c,
                                            SAPOL_Vehicle_Collision_Report_Number__c,
                                            Date_of_Accident__c,                             
                                            Time_of_Accident_hours__c,
                                            Time_of_Accident_minutes__c,
                                            Location_of_Accident__c,
                                            Postal_Address__c,
                                            Postal_Suburb__c,
                                            Postal_State__c,
                                            Postal_Postcode__c,
                                            ATSI__c,
                                            Preferred_Communication_Method__c,
                                            Preferred_phone_number__c,
                                            Interpreter_Required__c,
                                            Interpreter_Language__c,
                                            Status__c
                                     FROM Referred_Client__c  
                                     WHERE Id = :record.Client__c])
        {
            client = c;
        }

        if (record.RecordType.DeveloperName == 'Severe_Injury_Notification')
        {
            client.Status__c = 'Severe Injury Notified';
            client.Status_Date__c = Date.today();
        
            client.Title__c = record.Title__c;
            client.Given_Name__c = record.First_Name__c;
            client.Family_Name__c = record.Surname__c;  
            client.Allocated_to__c = record.Allocated_to__c;
            client.Account__c = record.Hospital_Facility__c;
            client.Address__c = record.Street__c;                
            client.Suburb__c = record.Suburb__c;
            client.State__c = record.State__c;
            client.Postcode__c = record.Postcode__c;            
            client.Email__c = record.Email__c;
            client.Phone__c = record.Home_Phone__c;
            client.Mobile__c = record.Mobile_Phone__c;
            client.Date_Of_Birth__c = record.Date_of_birth__c;
            client.Interpreter_Required__c = record.Is_an_Interpreter_Required__c;
            client.Interpreter_Language__c = record.Interpreter_Language__c;
            client.SAPOL_Vehicle_Collision_Report_Number__c = record.SAPOL_Vehicle_Collision_Report_Number__c;
            client.Date_of_Accident__c = record.Date_of_accident__c;
            client.Time_of_Accident_hours__c = record.Time_of_accident_hours__c;  
            client.Time_of_Accident_minutes__c = record.Time_of_accident_minutes__c;                                      
        }
        else if (record.RecordType.DeveloperName == 'Medical_Certificate')
        {
            client.Title__c = record.Title__c;
            client.Given_Name__c = record.First_Name__c;
            client.Family_Name__c = record.Surname__c; 
            client.Injury__c = record.Injury__c;         
        } 
        else if (record.RecordType.DeveloperName.startsWith('Application_Form'))
        { 
            client.Status__c = 'Application Received';
            client.Status_Date__c = record.Date__c;
            client.Current_Status__c = 'Application Received';
            client.Current_Status_Date__c = record.Date__c;
            client.Application_Submitted_by__c = record.This_application_is_being_completed_by__c;
        
            client.Title__c = record.Title__c;
            client.Given_Name__c = record.First_Name__c;
            client.Family_Name__c = record.Surname__c;          
            client.Allocated_to__c = record.Allocated_to__c;
            client.Account__c = record.Hospital_Facility__c;
            client.Address__c = record.Street__c;                
            client.Suburb__c = record.Suburb__c;
            client.State__c = record.State__c;
            client.Postcode__c = record.Postcode__c;            
            client.Email__c = record.Email__c;
            client.Phone__c = record.Home_Phone__c;
            client.Mobile__c = record.Mobile_Phone__c;
            client.Date_Of_Birth__c = record.Date_of_birth__c;
            client.Interpreter_Required__c = record.Is_an_Interpreter_Required__c;
            client.Interpreter_Language__c = record.Interpreter_Language__c;
            /*client.SAPOL_Vehicle_Collision_Report_Number__c = record.SAPOL_Vehicle_Collision_Report_Number__c;
            client.Date_of_Accident__c = record.Date_of_accident__c;
            client.Time_of_Accident_hours__c = record.Time_of_accident_hours__c;  
            client.Time_of_Accident_minutes__c = record.Time_of_accident_minutes__c; 
            client.Location_of_Accident__c = record.Location_of_Accident__c;  
            client.Postcode_where_accident_occurred__c = record.Postcode_where_accident_occurred__c;*/
            client.Postal_Address__c = record.Postal_Street__c;                
            client.Postal_Suburb__c = record.Postal_Suburb__c;
            client.Postal_State__c = record.Postal_State__c;
            client.Postal_Postcode__c = record.Postal_Postcode__c;
            client.ATSI__c = record.ATSI_origin__c;
            client.Preferred_Communication_Method__c = record.Preferred_form_of_written_communication__c;
            client.Preferred_phone_number__c = record.Preferred_phone_number__c; 
            client.Pre_Existing_Medical_Conditions__c = record.Pre_Existing_Medical_Conditions__c;
        }
        else if (record.RecordType.DeveloperName == 'Discharge_Rehabilitation_Form')
        {
            client.Title__c = record.Title__c;
            client.Given_Name__c = record.First_Name__c;
            client.Family_Name__c = record.Surname__c;
            client.Address__c = record.Street__c;                
            client.Suburb__c = record.Suburb__c;
            client.State__c = record.State__c;
            client.Postcode__c = record.Postcode__c;  
            client.Email__c = record.Email__c;          
            client.Phone__c = record.Home_Phone__c;
            client.Mobile__c = record.Mobile_Phone__c;   
            client.Interpreter_Required__c = record.Is_an_Interpreter_Required__c;
            client.Interpreter_Language__c = record.Interpreter_Language__c;                
            client.Date_of_birth__c = record.Date_Of_Birth__c;
            client.Date_of_accident__c = record.Date_of_Accident__c;
        }    
        else if (record.RecordType.DeveloperName == 'Home_Modification_Preliminary_Scoping')
        {
            client.Title__c = record.Title__c;
            client.Given_Name__c = record.First_Name__c;
            client.Family_Name__c = record.Surname__c;
            client.Address__c = record.Street__c;                
            client.Suburb__c = record.Suburb__c;
            client.State__c = record.State__c;
            client.Postcode__c = record.Postcode__c;  
            client.Email__c = record.Email__c;          
            client.Phone__c = record.Home_Phone__c;
            client.Mobile__c = record.Mobile_Phone__c;   
            client.Interpreter_Required__c = record.Is_an_Interpreter_Required__c;
            client.Interpreter_Language__c = record.Interpreter_Language__c;                
            client.Date_of_birth__c = record.Date_Of_Birth__c;
            client.Date_of_accident__c = record.Date_of_Accident__c;
        }    
        else if (record.RecordType.DeveloperName.startsWith('Function_on_Discharge'))
        {
            client.Title__c = record.Title__c;
            client.Given_Name__c = record.First_Name__c;
            client.Family_Name__c = record.Surname__c;
            client.Address__c = record.Street__c;                
            client.Suburb__c = record.Suburb__c;
            client.State__c = record.State__c;
            client.Postcode__c = record.Postcode__c;  
            client.Email__c = record.Email__c;          
            client.Phone__c = record.Home_Phone__c;
            client.Mobile__c = record.Mobile_Phone__c;   
            client.Interpreter_Required__c = record.Is_an_Interpreter_Required__c;
            client.Interpreter_Language__c = record.Interpreter_Language__c;                
            client.Date_of_birth__c = record.Date_Of_Birth__c;
            client.Date_of_accident__c = record.Date_of_Accident__c;
        }               
        else if (record.RecordType.DeveloperName == 'FIM_Score_Sheet')
        {
            client.FIM_Score__c = record.FIM_Total_Score__c;
        }
        else if (record.RecordType.DeveloperName == 'FIM_FAM_Score_Sheet')
        {
            client.FIMFAM_Score__c= record.FIMFAM_Total_Score__c;
        }

        upsert client;
        record.Client__c = client.Id;    
    
        // see if we need to handle parent or guardian info
        if (ShowParentGuardians)
        {
            Participant_Contact__c pc = new Participant_Contact__c();
        
            // either update a contact create a new one
            if (SelectedParentGuardian != null)
            {
                pc = [SELECT Id,
                             Title__c,
                             First_Name__c,
                             Surname__c,
                             Street__c,
                             Suburb__c,
                             State__c,
                             Postcode__c,
                             Relationship_to_Participant__c,
                             Email__c,
                             Home_Phone__c,
                             Work_Phone__c,
                             Mobile_Phone__c,
                             Date_of_Birth__c,
                             Is_an_Interpreter_Required__c,
                             Interpreter_Language__c,
                             ATSI_Origin__c,
                             Preferred_form_of_written_communication__c,
                             Preferred_phone_number__c,                             
                             Lawful_authority_act_on_behalf__c,
                             Birth_Certificate__c,
                             Guardianship_Order__c,
                             Custody_Order__c,
                             If_yes_details_of_lawful_authority__c,
                             Guardianship_Type__c,
                             Guardianship_Type_Details__c              
                      FROM Participant_Contact__c
                      WHERE Id = :SelectedParentGuardian];
            }
            else
            {
                pc.Participant__c = record.Client__c;
                pc.RecordTypeId = [SELECT Id
                                   FROM RecordType
                                   WHERE sObjectType = 'Participant_Contact__c'
                                   AND DeveloperName = 'Parent_or_Guardian'].Id;
            }
            
            pc.Title__c = record.Contact_Title__c;         
            pc.First_Name__c = record.Contact_First_Name__c;
            pc.Surname__c = record.Contact_Surname__c;
            pc.Street__c = record.Contact_Street__c;
            pc.Suburb__c = record.Contact_Suburb__c;
            pc.State__c = record.Contact_State__c;
            pc.Postcode__c = record.Contact_Postcode__c;
            pc.Relationship_to_Participant__c = record.Relationship_to_Injured_Person__c;
            pc.Email__c = record.Contact_Email__c;
            pc.Home_Phone__c = record.Contact_Home_Phone__c;
            pc.Work_Phone__c = record.Contact_Work_Phone__c;
            pc.Mobile_Phone__c = record.Contact_Mobile_Phone__c;
            pc.Date_of_Birth__c = record.Contact_Date_of_Birth__c;
            pc.Is_an_Interpreter_Required__c = record.Is_an_Interpreter_Required_for_Contact__c;
            pc.Interpreter_Language__c = record.Contact_Interpreter_Language__c;
            pc.ATSI_Origin__c = record.Contact_ATSI_origin__c;
            pc.Preferred_form_of_written_communication__c = record.Contact_preferred_written_communication__c;
            pc.Preferred_phone_number__c = record.Contact_preferred_phone_number__c;            
            pc.Lawful_authority_act_on_behalf__c = record.Lawful_authority_act_on_behalf__c;
            pc.Birth_Certificate__c = record.Birth_Certificate__c;
            pc.Guardianship_Order__c = record.Guardianship_Order__c;
            pc.Custody_Order__c = record.Custody_Order__c;
            pc.If_yes_details_of_lawful_authority__c = record.If_yes_details_of_lawful_authority__c;
            pc.Guardianship_Type__c = record.Guardianship_Type__c;
            pc.Guardianship_Type_Details__c = record.Guardianship_Type_Details__c;

            upsert pc;
            
            SelectedParentGuardian = pc.Id;
        }
    }
}