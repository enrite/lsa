/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestRCRBatchUpdateScheduledService {

    
    @IsTest//(SeeAlldata=true)
    static void test1()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        RCR_Service_Rate__c rate = null;
        Integer batchsize = 0;
        for(RCR_Service__c service : [SELECT ID,
                                        (SELECT ID,
                                                RCR_Service__c
                                        FROM    RCR_Contract_Scheduled_Services__r),
                                        (SELECT ID,
                                                Rate__c,
                                                Public_Holiday_Rate__c,
                                                RCR_Service__c,
                                                Start_Date__c,
                                                End_Date__c
                                        FROM    RCR_Service_Rates__r)
                                    FROM   RCR_Service__c
                                    WHERE  ID IN (SELECT    RCR_Service__c
                                                    FROM    RCR_Contract_Scheduled_Service__c
                                                    /*WHERE   RCR_Contract__r.Name = 'TestSO'*/)])
        {       
            if(service.RCR_Contract_Scheduled_Services__r.size() <= 20)
            {
                if(service.RCR_Service_Rates__r.size() > 0)
                {
                    rate = service.RCR_Service_Rates__r[0];
                    batchsize = service.RCR_Contract_Scheduled_Services__r.size();
                    break;
                }
            }
        }   
            
        RCRBatchUpdateScheduledServiceTotalCost batch = new RCRBatchUpdateScheduledServiceTotalCost();
        batch.Rate = rate;
        batch.ProposedRateActivation = true;
        batch.RunMode = RCRBatchUpdateScheduledServiceTotalCost.Mode.ProposedRateUpdateByNightProcess;
        ID batchprocessid = Database.executeBatch(batch, batchsize);        
        
        batch.ScheduleID = '123456789012345667';
        system.assert(batch.ScheduleID.length() == 15);
    }
    
    @IsTest//(SeeAlldata=true)
    static void test2()
    {      
        TestLoadData.loadRCRData();
        
        Test.StartTest();
     
        RCR_Contract__c contract = getContract();
        contract.Schedule__c = 'test1234';
        update contract;
                
        RCRBatchUpdateScheduledServiceTotalCost batch = new RCRBatchUpdateScheduledServiceTotalCost();
        batch.ScheduleID = contract.Schedule__c;
        batch.RunMode = RCRBatchUpdateScheduledServiceTotalCost.Mode.ProposedRateUpdateByNightProcess;
        ID batchprocessid = Database.executeBatch(batch, contract.RCR_Contract_Scheduled_Services__r.size());
        
        system.debug(batch.Rate);
        system.assert(batch.RatesByServiceID != null);
        system.assert(batch.Errors != null);
        system.assert(batch.ProposedRateActivation != null);
        system.assert(batch.cssIDs != null);
        system.assert(batch.RateAuditRecordsByScheduledService != null);
        system.assert(batch.ScheduleID != null);
                
        Test.StopTest(); 
    }

    @IsTest//(SeeAlldata=true)
    static void test3()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
        
        RCR_Contract__c contract = getContract();
        contract.Schedule__c = 'test1234';
        update contract;
                
        RCRBatchUpdateScheduledServiceTotalCost batch = new RCRBatchUpdateScheduledServiceTotalCost();
        batch.ScheduleID = contract.Schedule__c;
        for(RCR_Service_Rate__c rt : [SELECT ID,
                                            Start_Date__c,
                                            End_Date__c,
                                            RCR_Service__c
                                    FROM    RCR_Service_Rate__c
                                    WHERE   RCR_Service__c = :contract.RCR_Contract_Scheduled_Services__r[0].RCR_Service__c])
        {
            if(A2HCUtilities.dateRangesOverlap(contract.Start_Date__c, contract.End_Date__c, rt.Start_Date__c, rt.End_Date__c))
            {
                batch.Rate = rt;
                break;
            }
        }                            
                
        batch.RunMode = RCRBatchUpdateScheduledServiceTotalCost.Mode.SingleRateUpdate;
        ID batchprocessid = Database.executeBatch(batch, contract.RCR_Contract_Scheduled_Services__r.size());

        Test.stopTest();
    }
    
    @IsTest//(SeeAlldata=true)
    static void test4()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
        
        RCR_Contract__c contract = getContract();
        contract.Schedule__c = 'test1234';
        update contract;    
                
        RCRBatchUpdateScheduledServiceTotalCost batch = new RCRBatchUpdateScheduledServiceTotalCost();
        batch.ScheduleID = contract.Schedule__c;
        for(RCR_Service_Rate__c rt : [SELECT ID,
                                            Start_Date__c,
                                            End_Date__c,
                                            RCR_Service__c
                                    FROM    RCR_Service_Rate__c
                                    WHERE   RCR_Service__c = :contract.RCR_Contract_Scheduled_Services__r[0].RCR_Service__c])
        {
            if(A2HCUtilities.dateRangesOverlap(contract.Start_Date__c, contract.End_Date__c, rt.Start_Date__c, rt.End_Date__c))
            {
                batch.Rates.add(rt);
            }
        }           
        batch.RunMode = RCRBatchUpdateScheduledServiceTotalCost.Mode.MultipleRateUpdate;
        ID batchprocessid = Database.executeBatch(batch, contract.RCR_Contract_Scheduled_Services__r.size());

        Test.stopTest();
    }
    
    @IsTest//(SeeAlldata=true)
    static void test5()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
        
        RCR_Contract__c contract = getContract();
        contract.Schedule__c = 'test1234';
        update contract;
                
        RCRBatchUpdateScheduledServiceTotalCost batch = new RCRBatchUpdateScheduledServiceTotalCost();
        batch.ScheduleID = contract.Schedule__c;
        batch.RunMode = RCRBatchUpdateScheduledServiceTotalCost.Mode.ServiceOrderRollover;
        ID batchprocessid = Database.executeBatch(batch, contract.RCR_Contract_Scheduled_Services__r.size());

        Test.stopTest();
    }
    
    @IsTest//(SeeAlldata=true)
    static void test6()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
        
        RCR_Contract__c contract = getContract();
        contract.Schedule__c = 'test1234';
        update contract;
                
        RCRBatchUpdateScheduledServiceTotalCost batch = new RCRBatchUpdateScheduledServiceTotalCost();
        batch.ScheduleID = contract.Schedule__c;
        batch.RunMode = RCRBatchUpdateScheduledServiceTotalCost.Mode.SpecificScheduledServices;
        ID batchprocessid = Database.executeBatch(batch, contract.RCR_Contract_Scheduled_Services__r.size());

        Test.stopTest();
    }
    
    @IsTest//(SeeAlldata=true)
    static void test7()
    {       
        TestLoadData.loadRCRData();
        
        Test.StartTest();
    
        RCR_Contract__c contract = getContract();
        
        RCRBatchUpdateScheduledServiceTotalCost batch = new RCRBatchUpdateScheduledServiceTotalCost();
        batch.ProposedRateActivation = true;
        batch.RunMode = RCRBatchUpdateScheduledServiceTotalCost.Mode.ProposedRateUpdateByNightProcess;
        for(RCR_Contract_Scheduled_Service__c css : contract.RCR_Contract_Scheduled_Services__r)
        {
            batch.setServiceOrderValueAtRateUpdate(css);
        }
        batch.RunMode = RCRBatchUpdateScheduledServiceTotalCost.Mode.SpecificScheduledServices;
        for(RCR_Contract_Scheduled_Service__c css : contract.RCR_Contract_Scheduled_Services__r)
        {
            batch.setServiceOrderValueAtRateUpdate(css);
        }
        
        Test.StopTest();
    }

    private static RCR_Contract__c getContract()
    {
        return  [SELECT ID,
                        Start_Date__c,
                        End_Date__c,
                    (SELECT ID,
                            RCR_Service__c,
                            RCR_Contract__c,
                            RCR_Contract__r.Total_Service_Order_Cost__c
//                            RCR_Contract__r.CBMS_Service_Order_Value_at_Rate_Update__c,
//                            RCR_Contract__r.RCR_CBMS_Contract__r.Contract_Total__c,
//                            RCR_Contract__r.RCR_CBMS_Contract__c
                    FROM    RCR_Contract_Scheduled_Services__r)
                FROM   RCR_Contract__c
                WHERE  ID IN (SELECT RCR_Contract__c
                                FROM    RCR_Contract_Scheduled_Service__c
                                /*WHERE   RCR_Contract__r.Name = 'TestSO'*/)];
    }
}