@isTest
public class TestSelectRecordTypeController
{                    
    static testMethod void myTestMethod()
    {
        TestLoadFormData data = new TestLoadFormData();
                             
        Test.setCurrentPage(Page.SelectRecordType);                             
        ApexPages.CurrentPage().getParameters().put('o', 'Form_Detail__c');
        ApexPages.CurrentPage().getParameters().put('rid', data.RecordTypes.get('Chief_Executive_Application_Assessment').Id);
        ApexPages.CurrentPage().getParameters().put('rn', 'Chief_Executive_Application_Assessment');
        ApexPages.CurrentPage().getParameters().put('test', 'test');
                                           
        SelectRecordTypeController ctr = new SelectRecordTypeController();
        system.debug(ctr.RecordTypes);
        ctr.ClickContinue();
        ctr.Cancel();
    }        
}