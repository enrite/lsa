public with sharing class ParticipantPortalMyRequestsList extends A2HCPageBase{
    // ApexPages.StandardSetController must be instantiated
    // for standard list controllers
    public ApexPages.StandardSetController setCon {
        get {
            if(setCon == null) {
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(
                    [SELECT Id,
                            Name,
                            CreatedDate,
                            Request_Details__c,
                            Status__c,
                            Client__r.Name                                                 
                     FROM Request__c
                     ORDER BY CreatedDate DESC]));
            }
            return setCon;
        }
        set;
    }

    // Initialize setCon and return a list of records
    public List<Request__c> getRequests() {
        return (List<Request__c>) setCon.getRecords();
    }

    public List<Adhoc_Payment__c> Payments
    {
        get
        {
            if(Payments == null)
            {
                Payments = ParticipantPortalNoSharing.getParticipantPayments(CurrentUser);
            }
            return Payments;
        }
        private set;
    }

    public Account Acc
    {
        get
        {
            if(Acc == null)
            {
                Acc = [SELECT ID,
                        Name
                FROM    Account
                ];
            }
            return Acc;
        }
        private set;
    }

    public Referred_Client__c Cli
    {
        get
        {
            if(Cli == null)
            {
                Cli = [SELECT ID,
                        Name
                FROM    Referred_Client__c
                ];
            }
            return Cli;
        }
        private set;
    }




}