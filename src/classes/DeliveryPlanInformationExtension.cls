public with sharing class DeliveryPlanInformationExtension
{
    public DeliveryPlanInformationExtension(ApexPages.StandardController c)
    {
        record = (Plan__c)c.getRecord();   
    }

    private Plan__c record;
        
    public Plan_Action__c PlanAction
    {
        get
        {
            if (PlanAction == null)
            {
                PlanAction = new Plan_Action__c();
                for (Plan_Action__c pa : [SELECT Id,
                                                 Service_Delivery_Matters__c,
                                                 About_Participant__c,
                                                 Objectives_and_Outcomes_of_the_Service__c,
                                                 Plan_Goal__r.Name,
                                                 Plan_Goal__r.Goal_Details__c
                                          FROM Plan_Action__c
                                          WHERE Participant_Plan__c = :record.Id
                                          AND RecordType.DeveloperName = 'Attendant_Care_and_Support_MyPlan'
                                          LIMIT 1])
                {
                    PlanAction = pa;
                }                                                      
            }
            return PlanAction;
        }
        set;    
    }

    public List<Plan_Action__c> MyPlanActions
    {
        get
        {
            if (MyPlanActions == null)
            {
                MyPlanActions = [SELECT Id,
                                        Name,
                                        Provider_Display__c,
                                        Item_Description__c,
                                        Recently_Updated__c,
                                        Pre_Defined_Services__c
                                FROM    Plan_Action__c
                                WHERE   Participant_Plan__c = :record.Id
                                ORDER BY Recently_Updated__c DESC,
                                        Plan_Start_Date__c DESC];
            }
            return MyPlanActions;
        }
        set;
    }
}