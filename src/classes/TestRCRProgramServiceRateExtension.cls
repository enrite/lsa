/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestRCRProgramServiceRateExtension {

    static testMethod void myUnitTest()
    {
/*    	
        Account acc = new Account(Name = 'test');
		insert acc;
		RCR_Program__c prog = new RCR_Program__c(Account__c = acc.ID, Name = 'test');
		insert prog;
		RCR_Program_Service_Type__c psType = new RCR_Program_Service_Type__c(RCR_Program__c = prog.Id);
		psType.Name = 'test';
		insert psType;
		RCR_Program_Service__c ps = new RCR_Program_Service__c(RCR_Program_Service_Type__c = psType.ID);
		ps.Name = 'test';
		insert ps;
		RCR_Program_Service_Rate__c rate = new RCR_Program_Service_Rate__c(Program_Service__c = ps.ID);
		rate.Start_Date__c = date.today().addYears(-1);
		rate.End_Date__c = date.today().addYears(1);
		rate.Rate__c = 1.00;
		insert rate;
		RCR_Contract__c contract = new RCR_Contract__c(RCR_Program__c = prog.ID);
		contract.Level__c = '1';
		insert contract;

		RCR_Invoice_Batch__c b = new RCR_Invoice_Batch__c();
		insert b;

		RCR_Invoice__c i = new RCR_Invoice__c(RCR_Contract__c = contract.ID, RCR_Invoice_Batch__c = b.ID);
		i.Invoice_Number__c = '1';
		insert i;

		RCR_Contract_Scheduled_Service__c css = new RCR_Contract_Scheduled_Service__c(RCR_Contract__c = contract.ID, RCR_Program_Service__c = ps.ID);
		css.End_Date__c	= date.today().addYears(1);
		css.Start_Date__c= date.today().addYears(-1);
		css.Quantity_Per_Day__c = 1.0;
		insert css;

		Test.setCurrentPage(new PageReference('/apex/RCRContract'));
		ApexPages.currentPage().getParameters().put('retURL', ps.ID);
		RCRProgramServiceRateExtension ext = new RCRProgramServiceRateExtension(new ApexPages.StandardController(rate));

    	ApexPages.currentPage().getParameters().put('id', rate.Id);
		ext = new RCRProgramServiceRateExtension(new ApexPages.StandardController(rate));

		ext.saveOverride();

		ext.RCRProgramServiceRate.Start_Date__c = ext.RCRProgramServiceRate.End_Date__c.addDays(1);
		ext.saveOverride();

		RCR_Program_Service_Rate__c rate2 = new RCR_Program_Service_Rate__c(Program_Service__c = ps.ID);
		rate2.Start_Date__c = rate.Start_Date__c;
		rate2.End_Date__c = rate.End_Date__c;
		rate2.Rate__c = 1.00;
		ext.RCRProgramServiceRate = rate2;
		ext.saveOverride();

		ext.RCRProgramServiceRate = null;
		system.debug(ext.RCRProgramServiceRate);

		ext.cancelOverride();
		
*/		
    }
}