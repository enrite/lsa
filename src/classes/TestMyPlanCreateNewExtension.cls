@isTest
public class TestMyPlanCreateNewExtension
{                   
    static testMethod void myTestMethod()
    {   
        TestLoadFormData data = new TestLoadFormData();
                 
        Test.StartTest();
                                                           
        Plan__c p = new Plan__c();
        p.Client__c = data.Client.Id;
        p.RecordTypeId = data.RecordTypes.get('MyPlan').Id;
        p.Plan_Status__c = 'Draft';
        insert p;
        
        Plan_Action__c pa = new Plan_Action__c();
        pa.Participant_Plan__c = p.Id;
        pa.RecordTypeId = data.RecordTypes.get('Attendant_Care_and_Support').Id;
        insert pa;
        
        Plan_Goal__c g = new Plan_Goal__c();
        g.Participant_Plan__c = p.Id;
        insert g;               
                                     
        MyPlanCreateNewExtension ext = new MyPlanCreateNewExtension(new ApexPages.StandardController(p));
        ext.Create();
        
        Test.StopTest();        
    }
}