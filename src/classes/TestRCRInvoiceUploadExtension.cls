/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestRCRInvoiceUploadExtension 
{
/*
    static testMethod void myUnitTest()
    {
    	Account acc = [SELECT ID,
    							Name
						FROM	Account
						WHERE	Name = 'UnitingCare Wesley Adelaide'];

    	User u = [SELECT ID,
						AccountID
				FROM	User
				WHERE	AccountID = :acc.ID  AND
						UserType = 'CSPLitePortal'
				LIMIT 1];

		RCR_Program__c prog = new RCR_Program__c(Name = 'test');
		insert prog;

		RCR_Account_Program_Category__c psType = new RCR_Account_Program_Category__c(Account__c = acc.Id);
		psType.Name = 'category 1';
		insert psType;

		RCR_Account_Program_Category__c psType2 = new RCR_Account_Program_Category__c(Account__c = acc.Id);
		psType2.Name = 'Respite';
		insert psType2;

		RCR_Service__c ps = new RCR_Service__c(Account__c = acc.ID);
		ps.Name = 'Respite';
		ps.Day_of_Week__c = 'Monday; Tuesday; Wednesday; Thursday; Friday; Saturday; Sunday';
		insert ps;

		RCR_Service__c ps2 = new RCR_Service__c(Account__c = acc.ID);
		ps2.Name = 'HL FT LEVEL 2';
		ps2.Day_of_Week__c = 'Monday; Tuesday; Wednesday; Thursday; Friday; Saturday; Sunday';
		insert ps2;

		RCR_Service_Rate__c rate = new RCR_Service_Rate__c(RCR_Service__c = ps.ID);
		rate.Start_Date__c = date.today().addYears(-1);
		rate.End_Date__c = date.today().addYears(1);
		rate.Rate__c = 1.00;
		insert rate;

		RCR_Service_Rate__c rate2 = new RCR_Service_Rate__c(RCR_Service__c = ps2.ID);
		rate2.Start_Date__c = date.today().addYears(-1);
		rate2.End_Date__c = date.today().addYears(1);
		rate2.Rate__c = 1.00;
		insert rate2;

		RCR_Contract__c contract = new RCR_Contract__c(Account__c = acc.ID);
		contract.Level__c = '2';
		contract.Name = '72515';
		insert contract;

		RCR_Invoice_Batch__c b = new RCR_Invoice_Batch__c();
		insert b;

		RCR_Invoice__c i = new RCR_Invoice__c(RCR_Contract__c = contract.ID, RCR_Invoice_Batch__c = b.ID);
		i.Invoice_Number__c = '1zz';
		i.Start_Date__c = Date.today().addDays(-30);
		i.End_Date__c = Date.today();
		i.Date__c = Date.today();
		insert i;

		RCR_Invoice_Item__c it = new RCR_Invoice_Item__c(RCR_Invoice__c = i.ID);
		it.Activity_Date__c = date.today();
		it.Code__c = ps.Name;
		it.GST__c = rate.Rate__c * 0.1;
		it.Quantity__c = 1.0;

		RCR_Invoice_Item__c it2 = new RCR_Invoice_Item__c(RCR_Invoice__c = i.ID);
		it2.Activity_Date__c = date.today();
		it2.Code__c = ps2.Name;
		it2.GST__c = rate.Rate__c * 0.1;
		it2.Quantity__c = 1.0;

		RCR_Contract_Scheduled_Service__c css = new RCR_Contract_Scheduled_Service__c(RCR_Contract__c = contract.ID, RCR_Service__c = ps.ID);
		css.End_Date__c	= date.today().addYears(1);
		css.Start_Date__c= date.today().addYears(-1);
		css.Quantity_Per_Day__c = 1.0;
		insert css;

		RCR_Contract_Adhoc_Service__c ass = new RCR_Contract_Adhoc_Service__c(RCR_Contract__c = contract.ID, RCR_Account_Program_Category__c = psType.ID);
		ass.End_Date__c	= date.today().addYears(1);
		ass.Start_Date__c= date.today().addYears(-1);
		ass.Amount__c = 100.0;
		insert ass;

		try
		{
			RCR_Invoice_Tolerance__c tol = new RCR_Invoice_Tolerance__c();
			tol.Lower__c = 60.0;
			tol.Upper__c = 40.0;
			insert tol;
		}
		catch(Exception ex)
		{}


        RCRInvoiceUploadExtension ext = new RCRInvoiceUploadExtension();

        RCR_Invoice_Upload__c upload = new RCR_Invoice_Upload__c();

        upload.File_Name__c = 'test';
        upload.OwnerId = A2HCUtilities.getAnyUserForAccount(acc.ID).ID;
		upload.Date__c = Date.today();
		insert upload;
		PageReference page = new PageReference('/apex/RCRInvoiceUpload');

		Test.setCurrentPage(page);
		page.getParameters().put('id', upload.id);

		ext = new RCRInvoiceUploadExtension();

		upload = new RCR_Invoice_Upload__c();
system.debug('This should work');
		String csv = 'Program,Contract Number,Client Name,Invoice Date,Invoice Start Date,Invoice End Date,Invoice Number,Service Date,Code,Qty,Rate,GST %,Total Ex Gst,Total inc GST\n' +
						'Home Link SA,72515,,01/08/2011,01/07/2011,31/07/2011,62222,28/07/2011,HL FT LEVEL 2,1,82.95,8.30,82.95,91.25\n' +
						'Home Link SA,72515,,01/08/2011,01/07/2011,31/07/2011,62222,29/07/2011,HL FT LEVEL 2,1,82.95,8.30,82.95,91.25';
		Attachment att = new Attachment();
		att.Description = 'test';
		att.Body = Blob.valueOf(csv);

		ext.file = null;
		ext.file = att;
		ext.file.Name = 'test';

		upload.OwnerId = u.ID;
		ext.InvoiceUpload = upload;
		ext.InvoiceUpload.Description__c = 'test';
		ext.InPortal = false;
		system.debug(ext.InvoiceWarnings.size());

		ext.SelectedAccount = acc.ID;
		ext.saveOverride();
		ext.confirmSave();
		ext.cancel();

system.debug('This should fail');
		csv = 'Program,Contract Number,Client Name,Invoice Date,Invoice Start Date,Invoice End Date,Invoice Number,Service Date,Code,Qty,Rate,GST %,Total Ex Gst,Total inc GST\n' +
						'ZZZZZZZ,ZZZZ,,01/08/2011,01/07/2011,31/07/2011,62222,28/07/2011,HL FT LEVEL 4,1,82.95,8.30,82.95,91.25\n' +
						'ZZZZZZZ,ZZZZ,,01/08/2011,01/07/2011,31/07/2011,62222,31/07/2011,RespiteZ,1,42.01,5.1,8.46,111.2';
		att = new Attachment();
		att.Description = 'test';
		att.Body = Blob.valueOf(csv);

		ext.file = null;
		ext.file = att;
		ext.file.Name = 'test';

		upload.OwnerId = u.ID;
		ext.InvoiceUpload = upload;
		ext.InvoiceUpload.Description__c = 'test';
		ext.InPortal = false;
		system.debug(ext.InvoiceWarnings.size());

		ext.SelectedAccount = acc.ID;
		ext.saveOverride();
		ext.confirmSave();
		ext.cancel();

		ext.InvoiceUpload = null;

		csv = 'Program,Contract Number,Client Name,Invoice Date,Invoice Start Date,Invoice End Date,Invoice Number,Service Date,Code,Qty,Rate,GST %,Total Ex Gst,Total inc GST\n';
		att = new Attachment();
		att.Description = 'test';
		att.Body = Blob.valueOf(csv);

		ext.file = null;
		ext.file = att;
		ext.file.Name = 'test';

		upload.OwnerId = A2HCUtilities.getAnyUserForAccount(acc.ID).ID;
		ext.InvoiceUpload = upload;
		ext.InvoiceUpload.Description__c = 'test';
		ext.InPortal = false;
		system.debug(ext.InvoiceWarnings.size());

		Account accz = new Account(Name = 'testzz');
		insert accz;
		ext.SelectedAccount = accz.ID;
		ext.saveOverride();
		ext.confirmSave();

		ext.RCRProviderList = null;
		system.debug(ext.RCRProviderList);

		ext.confirmSave();
		
		ext.file = null;
		system.debug(ext.file);
		
		ext.InvoiceUpload = null;
		system.debug(ext.InvoiceUpload);

    }
*/    
}