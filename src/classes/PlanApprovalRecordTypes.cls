public class PlanApprovalRecordTypes {
	private static PlanApprovalRecordTypes instance = null;
	public Map<Id, RecordType> recordTypes {
		get{
			if(recordTypes == null)
			{
				recordTypes = new Map<Id, RecordType>([SELECT Id,
					Name
					FROM RecordType
					WHERE sObjectType = 'Plan_Approval__c']);
			}
			return recordTypes;
		}private set;
	} 

	public static PlanApprovalRecordTypes getInstance(){
		if(instance == null) instance = new PlanApprovalRecordTypes();
		return instance;
	}
}