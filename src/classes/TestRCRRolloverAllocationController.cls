/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest//(SeeAllData=true)
private class TestRCRRolloverAllocationController {

//    static testMethod void myUnitTest()
//    {
//        TestLoadData.loadRCRData();
//
//        Test.StartTest();
//
//        RCRRolloverAllocationController c = new RCRRolloverAllocationController();
//
//        system.debug(c.FinancialYears);
//
//        Financial_Year__c finYear = [SELECT Id FROM Financial_Year__c WHERE Start_Date__c <= :Date.today() AND End_Date__c >= :Date.today() LIMIT 1];
//        c.SelectedFinancialYear = finYear.Id;
//        /*
//        string serviceID = [SELECT Service_Type__c
//                            FROM    IF_Personal_Budget__c
//                            WHERE Financial_Year__c = :finYear.Id
//                            LIMIT 1].Service_Type__c;
//        */
//        system.debug(c.FinancialYearsByStartDate);
//        system.debug(c.FinancialYear);
//        system.debug(c.NextFinancialYear);
//
//        c.SelectedServiceIDs = new string[]{ 'Advocacy and information' /*serviceID*/ };
//        system.debug(c.SelectedServiceIDs);
//        system.debug(c.UnSelectedServiceTypes);
//        system.debug(c.ServiceProviders);
//        system.debug(c.FinancialYears);
//        system.debug(c.ServiceOrders);
//        system.debug(c.ClientAllocations);
//        system.debug(c.Allocations);
//
//        c.StartDate = '2012-06-01';
//        c.EndDate = '2012-07-01';
//        c.NewEndDate = '01/07/2013';
//        c.SelectedServiceOrder = 'TestSO';
//
//        c.doSearch();
//        c.doSOSearch();
//        c.doSearch(true);
//
//        system.debug(c.Allocations);
//
//        c.NextYearAdjustment = '1';
//        c.adjustAllocations();
//
//        system.debug(c.Allocations);
//        c.createAllocations();
//        c.approveAllocations();
//
//        c.doSearchApprovals();
//
//        RCR_Contract__c so = [SELECT Id,
//                                     Client__c,
//                                     RecordTypeId,
//                                     Funding_Source__c,
//                                     Allocation_Approved_Date__c
//                              FROM RCR_Contract__c
//                              WHERE Client__c != null
//                              LIMIT 1];
//
//        c.ServiceOrders.add(new RCRRolloverAllocationController.ServiceOrderWrapper(so));
//        c.ServiceOrders[0].Selected = true;
//        c.createRollovers();
//
//        // Invalid NewEndDates
//        c.NewEndDate = null;
//        c.createRollovers();
//        c.NewEndDate = '1111111';
//        c.createRollovers();
//
//
//        c.toggleAdjustment();
//
//        c.CCMSID = '1';
//        system.debug(c.CCMSID);
//        c.ClientName = 'Test Client';
//        system.debug(c.ClientName);
//        c.SelectedServiceProvider = 'Test Account';
//        system.debug(c.SelectedServiceProvider);
//
//
//        Referred_Client__c client = [SELECT ID,
//                                            Family_Name__c,
//                                            Given_Name__c,
//                                            Name
////                                            CCMS_ID__c,
////                                            (SELECT ID,
////                                                    FYE_Amount__c,
////                                                    Allocation_Source__c,
////                                                    Service_Type__c
////                                            FROM    IF_Personal_Budget_Amounts__r
////                                            WHERE   Rollover_Not_Required__c = false AND
////                                                    Status__c = :APS_PicklistValues.IFAllocation_Status_Approved AND
////                                                    Allocation_Type__c = :APS_PicklistValues.IFAllocation_Type_Recurrent AND
////                                                    Roll_Over_Destination__c = null)
//                                    FROM    Referred_Client__c
//                                    /*WHERE   ID IN (SELECT   Client__c
//                                                            FROM    IF_Personal_Budget__c
//                                                            WHERE   Rollover_Not_Required__c = false AND
//                                                                    Status__c = :APS_PicklistValues.IFAllocation_Status_Approved AND
//                                                                    Allocation_Type__c = :APS_PicklistValues.IFAllocation_Type_Recurrent AND
//                                                                    Roll_Over_Destination__c = null)*/
//                                    LIMIT 1];
//
//        RCRRolloverAllocationController.RolloverWrapper wrap = new RCRRolloverAllocationController.RolloverWrapper(client);
//        system.debug(wrap.AllocationsByTypeAndSource);
//
////        IF_Personal_Budget__c pb = new IF_Personal_Budget__c();
////        pb.Client__c = so.Client__c;
////        pb.Financial_Year__c = finYear.Id;
////        pb.RCR_Service_Order__c = so.Id;
////        pb.Status__c = APS_PicklistValues.IFAllocation_Status_Approved;
////        pb.Allocation_Type__c = 'Once Off';
////        pb.Amount__c = 1;
////        pb.FYE_Amount__c = 1;
////        insert pb;
////
////        IF_Personal_Budget__c alloc = [SELECT   ID,
////                                                FYE_Amount__c,
////                                                Allocation_Source__c,
////                                                Service_Type__c,
////                                                Client__c,
////                                                Client__r.Name,
////                                                Client__r.CCMS_ID__c,
////                                                Financial_Year__r.Name,
////                                                (SELECT FYE_Amount__c
////                                                FROM    Roll_Over_Sources__r)
////                                        FROM    IF_Personal_Budget__c
////                                        WHERE   Client__c != null LIMIT 1];
//
//        RCRRolloverAllocationController.AllocationRolloverWrapper wrapper = new RCRRolloverAllocationController.AllocationRolloverWrapper(alloc);
//        system.debug(wrapper.Client);
//        system.debug(wrapper.AllocationForApproval);
//        system.debug(wrapper.SourceAllocationIDs);
//        system.debug(wrapper.Selected);
//        system.debug(wrapper.Rejected);
//        system.debug(wrapper.Source);
//        system.debug(wrapper.Type);
//        system.debug(wrapper.PropAllocation);
//        system.debug(wrapper.FYEAmount);
//        system.debug(wrapper.ProposedAllocation);
//        system.debug(wrapper.ProposedAllocationVariation);
//        system.debug(wrapper.CurrentFYEAmount);
//        system.debug(wrapper.CurFYEAmount);
//        system.debug(wrapper.CurrentApprovalFYEAmount);
//        system.debug(wrapper.ProposedApprovalFYEAmount);
//        system.debug(wrapper.ProposedApprovalVariation);
//        wrapper.addFYEAmount(alloc);
//        wrapper.setAdjustment(1.0);
//        wrapper.createRolloverAllocation(finYear);
//
//
//
//        Test.StopTest();
//    }
//
//    static testMethod void testSoSearch()
//    {
//        TestLoadData.loadRCRData();
//
//        Test.StartTest();
//
//        RCRRolloverAllocationController c = new RCRRolloverAllocationController();
//
//        Id finID = [SELECT  Id
//                    FROM    Financial_Year__c
//                    WHERE Name = '2013 - 2014'].Id;
//
//        c.StartDate = null;
//        c.doSOSearch();
//
//        c.StartDate = Date.today().format();
//        c.EndDate = Date.today().format();
//        c.CCMSID = null;
//        c.ClientName = null;
//        c.SelectedServiceProvider = null;
//        c.ClientName = null;
//        c.SelectedServiceOrder = null;
//        c.doSOSearch();
//
//
//        RCR_Contract__c so = [SELECT Id FROM RCR_Contract__c LIMIT 1 /* WHERE Name = 'TestSO' */];
//        RCRRolloverAllocationController.ServiceOrderWrapper wrap = new RCRRolloverAllocationController.ServiceOrderWrapper(so);
//        system.debug(wrap.ServiceOrder);
//        system.debug(wrap.Selected);
//
//        Test.StopTest();
//    }
//
//    static testMethod void testAdjustAllocations()
//    {
//        TestLoadData.loadRCRData();
//
//        Test.StartTest();
//
//        RCRRolloverAllocationController c = new RCRRolloverAllocationController();
//
//        Id finID = [SELECT  Id
//                    FROM    Financial_Year__c
//                    WHERE Name = '2013 - 2014'].Id;
//
//        c.NextYearAdjustment = null;
//        c.adjustAllocations();
//
//        c.NextYearAdjustment = 'NonNumeric';
//        c.adjustAllocations();
//
//        Test.StopTest();
//    }
//
//    static testMethod void testExceptions()
//    {
//        TestLoadData.loadRCRData();
//
//        Test.StartTest();
//
//        RCRRolloverAllocationController c = new RCRRolloverAllocationController();
//        c.forceTestError = true;
//
//        try
//        {
//            c.doSOSearch();
//        }
//        catch(Exception ex){}
//
//        try
//        {
//            c.doSearch();
//        }
//        catch(Exception ex){}
//
//        try
//        {
//            c.adjustAllocations();
//        }
//        catch(Exception ex){}
//
//        try
//        {
//            c.createAllocations();
//        }
//        catch(Exception ex){}
//
//        try
//        {
//            c.doSearchApprovals();
//        }
//        catch(Exception ex){}
//
//        try
//        {
//            c.createRollovers();
//        }
//        catch(Exception ex){}
//
//        try
//        {
//            c.approveAllocations();
//        }
//        catch(Exception ex){}
//
//        try
//        {
//            c.toggleAdjustment();
//        }
//        catch(Exception ex){}
//        try
//        {
//            c.displayMessages(new String[]{'HELLO WORLD'});
//            c.displayMessages(new String[]{'1','2','3','4','5','6','7','8','9','10','11'});
//        }
//        catch(Exception ex){}
//
//        Test.StopTest();
//    }
}