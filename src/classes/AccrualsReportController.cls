public with sharing class AccrualsReportController
{
    public AccrualsReportController()        
    {        
        // set up the date parameters
        Params = new Schedule__c();
        Date today = Date.today();
        
        Params.Start_Date__c = Date.newInstance(today.year(), 7, 1);
        if (today.month() <= 6)
        {
            Params.Start_Date__c = Params.Start_Date__c.addYears(-1);
        }
        
        Params.End_Date__c = Date.newInstance(today.year(), today.month(), 1).addDays(-1);
        
        if (Params.End_Date__c < Params.Start_Date__c)
        {
            Params.Start_Date__c = Params.Start_Date__c.addYears(-1);
        }
        
        // set up the report by parameter
        ReportBy = 'GL';
        
        // if parameter have been passed in then use those
        if (ApexPages.CurrentPage().getParameters().containsKey('start'))
        {
            Params.Start_Date__c = Date.valueOf(ApexPages.CurrentPage().getParameters().get('start'));
        }
        
        if (ApexPages.CurrentPage().getParameters().containsKey('end'))
        {
            Params.End_Date__c = Date.valueOf(ApexPages.CurrentPage().getParameters().get('end'));
        }
        
        if (ApexPages.CurrentPage().getParameters().containsKey('reportBy'))
        {
            ReportBy = ApexPages.CurrentPage().getParameters().get('reportBy');
        }
        
        FindData();
    }

    public Schedule__c Params { get; set; }
    public String ReportBy { get; set; }
    public List<String> Months { get; set; }
    public List<String> clientIds { get; set; }
    public Map<String, String> clientNames { get; set; }
    public Map<String, Integer> clientGlCodeSize { get; set; }
    public Map<String, List<ReportRow>> ReportRows { get; set; }  
    public Map<String, ReportRow> SubTotals { get; set; }  
    public ReportRow GrandTotals { get; set; }
                
    public void FindData()
    {
        try
        {
            // create the months list and map
            Months = new List<String>();            
            for (Date d = Params.Start_Date__c; d <= Params.End_Date__c; d = d.addMonths(1))
            {
                Months.add(Datetime.newInstance(d.year(), d.month(), 1, 20, 0, 0).format('MMM yyyy'));
            }
            //Months.add('Grand Total');
            
            Map<String, Integer> monthsMap = new Map<String, Integer>();
            for (Integer i = 0; i < Months.size(); i++)
            {
                monthsMap.put(Months[i], i);
            }
            
            clientIds = new List<String>(); 
            clientNames = new Map<String, String>();      
            Map<String, List<String>> glCodes = new Map<String, List<String>>();
            Map<String, Map<String, ReportRow>> rr = new Map<String, Map<String, ReportRow>>();   
            SubTotals = new Map<String, ReportRow>();       
            GrandTotals = new ReportRow('Grand Total', Months.size());
                                                
            for (RCR_Scheduled_Service_Breakdown__c ssb : [SELECT Id,
                                                                  RCR_Contract_Scheduled_Service__r.RCR_Contract__r.Client__c,
                                                                  RCR_Contract_Scheduled_Service__r.RCR_Contract__r.Client__r.Name,
                                                                  RCR_Contract_Scheduled_Service__r.Service_Types_Desc__c,                                                                                                                                  
                                                                  Start_Date__c,
                                                                  End_Date__c,
                                                                  Total_Cost__c                                                                                                                                 
                                                          FROM RCR_Scheduled_Service_Breakdown__c
                                                          WHERE RCR_Contract_Scheduled_Service__r.RCR_Contract__r.RecordType.DeveloperName = 'Service_Order'
                                                          AND End_Date__c >= :Params.Start_Date__c
                                                          AND End_Date__c <= :Params.End_Date__c])
            { 
                // default to generic client details
                String clientId = '';
                String clientName = '';
                
                // if GL by Participant is selected then use specific client details
                if (ReportBy == 'GL by Participant')
                {
                    clientId = ssb.RCR_Contract_Scheduled_Service__r.RCR_Contract__r.Client__c;
                    clientName = ssb.RCR_Contract_Scheduled_Service__r.RCR_Contract__r.Client__r.Name;
                }
                
                // check if the report rows contain an entry for the client
                if (!rr.containsKey(clientId))
                {
                    rr.put(clientId, new Map<String, ReportRow>());
                    clientIds.add(clientId);
                    clientNames.put(clientId, clientName);  
                    glCodes.put(clientId, new List<String>());  
                    
                    SubTotals.put(clientId, new ReportRow('Sub Total', Months.size()));
                }
                
                // check if the client entry contains a report row for the gl code                            
                if (!rr.get(clientId).containsKey(ssb.RCR_Contract_Scheduled_Service__r.Service_Types_Desc__c))
                {
                    rr.get(clientId).put(ssb.RCR_Contract_Scheduled_Service__r.Service_Types_Desc__c, new ReportRow(ssb.RCR_Contract_Scheduled_Service__r.Service_Types_Desc__c, Months.size()));
                    glCodes.get(clientId).add(ssb.RCR_Contract_Scheduled_Service__r.Service_Types_Desc__c);                    
                }           
                
                // find the amount per day of the breakdown - add the 1 because the dates are inclusive
                Integer days = ssb.Start_Date__c.daysBetween(ssb.End_Date__c) + 1;
                Decimal amountPerDay = ssb.Total_Cost__c / days;
                
                // iterate through the months                
                for (Date d = Date.newInstance(ssb.Start_Date__c.year(), ssb.Start_Date__c.month(), 1) ; d <= Date.newInstance(ssb.End_Date__c.year(), ssb.End_Date__c.month(), 1); d = d.addMonths(1))
                {
                    // if the date falls outside the report date then skip this month
                    if (d < Params.Start_Date__c
                        || d.addMonths(1).addDays(-1) > Params.End_Date__c)
                    {
                        continue;
                    }
                
                    Integer daysInMonth = date.daysInMonth(d.year(), d.month());

                    // see if the breakdown starts or ends partway through the month
                    if (ssb.End_Date__c < d.addMonths(1).addDays(-1))
                    {
                        daysInMonth = daysInMonth - (daysInMonth - ssb.End_Date__c.day());    
                    }

                    if (ssb.Start_Date__c > d)
                    {
                        daysInMonth = daysInMonth - ssb.Start_Date__c.day();
                    }

                    // find the amount for the month
                    Decimal monthAmount = amountPerDay * daysInMonth;
                
                    // sum up the total cost for the breakdowns                
                    String col = Datetime.newInstance(d.year(), d.month(), 1, 20, 0, 0).format('MMM yyyy');
                    Integer i = monthsMap.get(col);
                    
                    rr.get(clientId).get(ssb.RCR_Contract_Scheduled_Service__r.Service_Types_Desc__c).TotalCost[i] = rr.get(clientId).get(ssb.RCR_Contract_Scheduled_Service__r.Service_Types_Desc__c).TotalCost[i] + IsNull(monthAmount);
                    rr.get(clientId).get(ssb.RCR_Contract_Scheduled_Service__r.Service_Types_Desc__c).GrandTotalCost += IsNull(monthAmount);
                    
                    // add to the amount accrued
                    rr.get(clientId).get(ssb.RCR_Contract_Scheduled_Service__r.Service_Types_Desc__c).AmountAccrued[i] = rr.get(clientId).get(ssb.RCR_Contract_Scheduled_Service__r.Service_Types_Desc__c).AmountAccrued[i] + IsNull(monthAmount);
                    rr.get(clientId).get(ssb.RCR_Contract_Scheduled_Service__r.Service_Types_Desc__c).GrandAmountAccrued += IsNull(monthAmount);
                    
                    // add to the sub totals
                    SubTotals.get(clientId).TotalCost[i] = SubTotals.get(clientId).TotalCost[i] + IsNull(monthAmount);                    
                    SubTotals.get(clientId).AmountAccrued[i] = SubTotals.get(clientId).AmountAccrued[i] + IsNull(monthAmount);                    
                    
                    // add to the grand total
                    GrandTotals.TotalCost[i] = GrandTotals.TotalCost[i] + IsNull(monthAmount);                    
                    GrandTotals.AmountAccrued[i] = GrandTotals.AmountAccrued[i] + IsNull(monthAmount);                    
                }
                
                // add the breakdown total to the sub totals
                SubTotals.get(clientId).GrandTotalCost += IsNull(ssb.Total_Cost__c);
                SubTotals.get(clientId).GrandAmountAccrued += IsNull(ssb.Total_Cost__c);
                
                // add the breakdown total to the sub totals
                GrandTotals.GrandTotalCost += IsNull(ssb.Total_Cost__c);
                GrandTotals.GrandAmountAccrued += IsNull(ssb.Total_Cost__c); 
                
                 /*           
                // sum up the total cost for the breakdowns                
                String col = Datetime.newInstance(ssb.End_Date__c.year(), ssb.End_Date__c.month(), 1, 20, 0, 0).format('MMM yyyy');
                Integer i = monthsMap.get(col);
                
                rr.get(clientId).get(ssb.RCR_Contract_Scheduled_Service__r.Service_Types_Desc__c).TotalCost[i] = rr.get(clientId).get(ssb.RCR_Contract_Scheduled_Service__r.Service_Types_Desc__c).TotalCost[i] + IsNull(ssb.Total_Cost__c);
                rr.get(clientId).get(ssb.RCR_Contract_Scheduled_Service__r.Service_Types_Desc__c).GrandTotalCost += IsNull(ssb.Total_Cost__c);
                
                // add to the amount accrued
                rr.get(clientId).get(ssb.RCR_Contract_Scheduled_Service__r.Service_Types_Desc__c).AmountAccrued[i] = rr.get(clientId).get(ssb.RCR_Contract_Scheduled_Service__r.Service_Types_Desc__c).AmountAccrued[i] + IsNull(ssb.Total_Cost__c);
                rr.get(clientId).get(ssb.RCR_Contract_Scheduled_Service__r.Service_Types_Desc__c).GrandAmountAccrued += IsNull(ssb.Total_Cost__c);
                
                // add to the sub totals
                SubTotals.get(clientId).TotalCost[i] = SubTotals.get(clientId).TotalCost[i] + IsNull(ssb.Total_Cost__c);
                SubTotals.get(clientId).GrandTotalCost += IsNull(ssb.Total_Cost__c);
                SubTotals.get(clientId).AmountAccrued[i] = SubTotals.get(clientId).AmountAccrued[i] + IsNull(ssb.Total_Cost__c);
                SubTotals.get(clientId).GrandAmountAccrued += IsNull(ssb.Total_Cost__c);
                
                // add to the grand total
                GrandTotals.TotalCost[i] = GrandTotals.TotalCost[i] + IsNull(ssb.Total_Cost__c);
                GrandTotals.GrandTotalCost += IsNull(ssb.Total_Cost__c);
                GrandTotals.AmountAccrued[i] = GrandTotals.AmountAccrued[i] + IsNull(ssb.Total_Cost__c);
                GrandTotals.GrandAmountAccrued += IsNull(ssb.Total_Cost__c);                                                                               */
            }
                                   
            for (RCR_Contract__c c : [SELECT Id,
                                             Client__c,
                                             Client__r.Name,                                                                                                                                                                            
                                             (SELECT Id,
                                                     Date_of_Transaction__c,
                                                     GL_Code__r.Name,
                                                     Amount__c,
                                                     GST__c
                                              FROM Adhoc_Payments__r
                                              WHERE Date_of_Transaction__c >= :Params.Start_Date__c
                                              AND Date_of_Transaction__c <= :Params.End_Date__c),
                                             (SELECT Id,
                                                     Date_of_Transaction__c,
                                                     GL_Code__r.Name,
                                                     Amount__c,
                                                     GST__c
                                              FROM External_Expense_Line_Items__r
                                              WHERE Date_of_Transaction__c >= :Params.Start_Date__c
                                              AND Date_of_Transaction__c <= :Params.End_Date__c)
                                     FROM RCR_Contract__c
                                     WHERE RecordType.DeveloperName = 'Service_Order'
                                     AND End_Date__c >= :Params.Start_Date__c
                                     AND End_Date__c <= :Params.End_Date__c])
            {       
                // loop through the adhoc payments
                for (Adhoc_Payment__c a : c.Adhoc_Payments__r)
                {
                    // default to generic client details
                    String clientId = '';
                    String clientName = '';
                    
                    // if GL by Participant is selected then use specific client details
                    if (ReportBy == 'GL by Participant')
                    {
                        clientId = c.Client__c;
                        clientName = c.Client__r.Name;
                    }
                    
                    // check if the report rows contain an entry for the client
                    if (!rr.containsKey(clientId))
                    {
                        rr.put(clientId, new Map<String, ReportRow>());
                        clientIds.add(clientId);
                        clientNames.put(clientId, clientName);  
                        glCodes.put(clientId, new List<String>());  
                        
                        SubTotals.put(clientId, new ReportRow('Sub Total', Months.size()));
                    }
                    
                    // check if the client entry contains a report row for the gl code                            
                    if (!rr.get(clientId).containsKey(a.GL_Code__r.Name))
                    {
                        rr.get(clientId).put(a.GL_Code__r.Name, new ReportRow(a.GL_Code__r.Name, Months.size()));
                        glCodes.get(clientId).add(a.GL_Code__r.Name);                    
                    }           
                
                    // sum up the total cost for the breakdowns                
                    String col = Datetime.newInstance(a.Date_of_Transaction__c.year(), a.Date_of_Transaction__c.month(), 1, 20, 0, 0).format('MMM yyyy');
                    Integer i = monthsMap.get(col);

                    rr.get(clientId).get(a.GL_Code__r.Name).TotalCost[i] = rr.get(clientId).get(a.GL_Code__r.Name).TotalCost[i] + IsNull(a.Amount__c);
                    rr.get(clientId).get(a.GL_Code__r.Name).GrandTotalCost += IsNull(a.Amount__c);
                    
                    // add to the amount accrued
                    rr.get(clientId).get(a.GL_Code__r.Name).AmountAccrued[i] = rr.get(clientId).get(a.GL_Code__r.Name).AmountAccrued[i] + IsNull(a.Amount__c);
                    rr.get(clientId).get(a.GL_Code__r.Name).GrandAmountAccrued += IsNull(a.Amount__c);
                    
                    // add to the sub totals
                    SubTotals.get(clientId).TotalCost[i] = SubTotals.get(clientId).TotalCost[i] + IsNull(a.Amount__c);
                    SubTotals.get(clientId).GrandTotalCost += IsNull(a.Amount__c);
                    SubTotals.get(clientId).AmountAccrued[i] = SubTotals.get(clientId).AmountAccrued[i] + IsNull(a.Amount__c);
                    SubTotals.get(clientId).GrandAmountAccrued += IsNull(a.Amount__c);
                    
                    // add to the grand total
                    GrandTotals.TotalCost[i] = GrandTotals.TotalCost[i] + IsNull(a.Amount__c);
                    GrandTotals.GrandTotalCost += IsNull(a.Amount__c);
                    GrandTotals.AmountAccrued[i] = GrandTotals.AmountAccrued[i] + IsNull(a.Amount__c);
                    GrandTotals.GrandAmountAccrued += IsNull(a.Amount__c);
                }  
                
                // loop through the external expense line items
                for (External_Expense_Line_Item__c a : c.External_Expense_Line_Items__r)
                {
                    // default to generic client details
                    String clientId = '';
                    String clientName = '';
                    
                    // if GL by Participant is selected then use specific client details
                    if (ReportBy == 'GL by Participant')
                    {
                        clientId = c.Client__c;
                        clientName = c.Client__r.Name;
                    }
                    
                    // check if the report rows contain an entry for the client
                    if (!rr.containsKey(clientId))
                    {
                        rr.put(clientId, new Map<String, ReportRow>());
                        clientIds.add(clientId);
                        clientNames.put(clientId, clientName);  
                        glCodes.put(clientId, new List<String>());  
                        
                        SubTotals.put(clientId, new ReportRow('Sub Total', Months.size()));
                    }
                    
                    // check if the client entry contains a report row for the gl code                            
                    if (!rr.get(clientId).containsKey(a.GL_Code__r.Name))
                    {
                        rr.get(clientId).put(a.GL_Code__r.Name, new ReportRow(a.GL_Code__r.Name, Months.size()));
                        glCodes.get(clientId).add(a.GL_Code__r.Name);                    
                    }           
                
                    // sum up the total cost for the breakdowns                
                    String col = Datetime.newInstance(a.Date_of_Transaction__c.year(), a.Date_of_Transaction__c.month(), 1, 20, 0, 0).format('MMM yyyy');
                    Integer i = monthsMap.get(col);
                    
                    rr.get(clientId).get(a.GL_Code__r.Name).TotalCost[i] = rr.get(clientId).get(a.GL_Code__r.Name).TotalCost[i] + IsNull(a.Amount__c);
                    rr.get(clientId).get(a.GL_Code__r.Name).GrandTotalCost += IsNull(a.Amount__c);
                    
                    // add to the amount accrued
                    rr.get(clientId).get(a.GL_Code__r.Name).AmountAccrued[i] = rr.get(clientId).get(a.GL_Code__r.Name).AmountAccrued[i] + IsNull(a.Amount__c);
                    rr.get(clientId).get(a.GL_Code__r.Name).GrandAmountAccrued += IsNull(a.Amount__c);
                    
                    // add to the sub totals
                    SubTotals.get(clientId).TotalCost[i] = SubTotals.get(clientId).TotalCost[i] + IsNull(a.Amount__c);
                    SubTotals.get(clientId).GrandTotalCost += IsNull(a.Amount__c);
                    SubTotals.get(clientId).AmountAccrued[i] = SubTotals.get(clientId).AmountAccrued[i] + IsNull(a.Amount__c);
                    SubTotals.get(clientId).GrandAmountAccrued += IsNull(a.Amount__c);
                    
                    // add to the grand total
                    GrandTotals.TotalCost[i] = GrandTotals.TotalCost[i] + IsNull(a.Amount__c);
                    GrandTotals.GrandTotalCost += IsNull(a.Amount__c);
                    GrandTotals.AmountAccrued[i] = GrandTotals.AmountAccrued[i] + IsNull(a.Amount__c);
                    GrandTotals.GrandAmountAccrued += IsNull(a.Amount__c);
                }                                                                           
            }
                               
            for (RCR_Invoice_Item__c ii : [SELECT Id,
                                                  RCR_Contract_Scheduled_Service__r.RCR_Contract__r.Client__c,
                                                  RCR_Contract_Scheduled_Service__r.RCR_Contract__r.Client__r.Name,
                                                  RCR_Contract_Scheduled_Service__r.Service_Types_Desc__c,                                                                                                                                  
                                                  Activity_Date__c,
                                                  Total_ex_GST__c                                                               
                                           FROM RCR_Invoice_Item__c
                                           WHERE RCR_Contract_Scheduled_Service__r.RCR_Contract__r.RecordType.DeveloperName = 'Service_Order'
                                           AND Activity_Date__c >= :Params.Start_Date__c
                                           AND Activity_Date__c <= :Params.End_Date__c
                                           AND Status__c = 'Approved'])
            { 
                // default to generic client details
                String clientId = '';
                String clientName = '';
                
                // if GL by Participant is selected then use specific client details
                if (ReportBy == 'GL by Participant')
                {
                    clientId = ii.RCR_Contract_Scheduled_Service__r.RCR_Contract__r.Client__c;
                    clientName = ii.RCR_Contract_Scheduled_Service__r.RCR_Contract__r.Client__r.Name;
                }
                
                // check if the report rows contain an entry for the client
                if (!rr.containsKey(clientId))
                {
                    rr.put(clientId, new Map<String, ReportRow>());
                    clientIds.add(clientId);
                    clientNames.put(clientId, clientName);  
                    glCodes.put(clientId, new List<String>());  
                    
                    SubTotals.put(clientId, new ReportRow('Sub Total', Months.size()));
                }
                
                // check if the client entry contains a report row for the gl code                            
                if (!rr.get(clientId).containsKey(ii.RCR_Contract_Scheduled_Service__r.Service_Types_Desc__c))
                {
                    rr.get(clientId).put(ii.RCR_Contract_Scheduled_Service__r.Service_Types_Desc__c, new ReportRow(ii.RCR_Contract_Scheduled_Service__r.Service_Types_Desc__c, Months.size()));
                    glCodes.get(clientId).add(ii.RCR_Contract_Scheduled_Service__r.Service_Types_Desc__c);                    
                }           
                                            
                // sum up the total submitted for the activity statements                
                String col = Datetime.newInstance(ii.Activity_Date__c.year(), ii.Activity_Date__c.month(), 1, 20, 0, 0).format('MMM yyyy');
                Integer i = monthsMap.get(col);
                
                rr.get(clientId).get(ii.RCR_Contract_Scheduled_Service__r.Service_Types_Desc__c).TotalSubmitted[i] = rr.get(clientId).get(ii.RCR_Contract_Scheduled_Service__r.Service_Types_Desc__c).TotalSubmitted[i] + IsNull(ii.Total_ex_GST__c);
                rr.get(clientId).get(ii.RCR_Contract_Scheduled_Service__r.Service_Types_Desc__c).GrandTotalSubmitted += IsNull(ii.Total_ex_GST__c);
                
                // subtract from the amount accrued
                rr.get(clientId).get(ii.RCR_Contract_Scheduled_Service__r.Service_Types_Desc__c).AmountAccrued[i] = rr.get(clientId).get(ii.RCR_Contract_Scheduled_Service__r.Service_Types_Desc__c).AmountAccrued[i] - IsNull(ii.Total_ex_GST__c);
                rr.get(clientId).get(ii.RCR_Contract_Scheduled_Service__r.Service_Types_Desc__c).GrandAmountAccrued -= IsNull(ii.Total_ex_GST__c);
                
                // add to the sub totals
                SubTotals.get(clientId).TotalSubmitted[i] = SubTotals.get(clientId).TotalSubmitted[i] + IsNull(ii.Total_ex_GST__c);
                SubTotals.get(clientId).GrandTotalSubmitted += IsNull(ii.Total_ex_GST__c);                                        
                SubTotals.get(clientId).AmountAccrued[i] = SubTotals.get(clientId).AmountAccrued[i] - IsNull(ii.Total_ex_GST__c);
                SubTotals.get(clientId).GrandAmountAccrued -= IsNull(ii.Total_ex_GST__c);
                
                // add to the grand total
                GrandTotals.TotalSubmitted[i] = GrandTotals.TotalSubmitted[i] + IsNull(ii.Total_ex_GST__c);
                GrandTotals.GrandTotalSubmitted += IsNull(ii.Total_ex_GST__c);                                        
                GrandTotals.AmountAccrued[i] = GrandTotals.AmountAccrued[i] - IsNull(ii.Total_ex_GST__c);
                GrandTotals.GrandAmountAccrued -= IsNull(ii.Total_ex_GST__c);
                                            
            } 
            
            ReportRows = new Map<String, List<ReportRow>>();
            clientGlCodeSize = new Map<String, Integer>();
            for (String clientId : clientIds)
            {
                clientGlCodeSize.put(clientId, glCodes.get(clientId).size());
            
                glCodes.get(clientId).sort();
                ReportRows.put(clientId, new List<ReportRow>());
                
                for (String gl : glCodes.get(clientId))
                {
                    ReportRows.get(clientId).add(rr.get(clientId).get(gl));
                }                                
            }                                                                                                                       
        }
        catch(Exception e)
        {
            A2HCException.formatException(e);
        }
    }
    
    public String CSVString
    {
        get
        {
            String csv = '';           
            
            // headers
            if (ReportBy == 'GL by Participant')
            {
                csv += 'Client,';
            }
            csv += 'GL Code,,';
            
            for (String m : Months)
            {
                csv += m + ',';        
            }
            csv += 'Grand Total';
            
            // content
            for (String clientId : clientIds)
            {               
                for (ReportRow r : ReportRows.get(clientId))
                {    
                    // Sum of Total Cost
                    csv += '\n';
                    if (ReportBy == 'GL by Participant')
                    {
                        csv += clientNames.get(clientId) + ',';
                    }
                    
                    csv += r.GLCode + ',Sum of Total Cost,';
                    for (Decimal c : r.TotalCost)
                    {
                        csv += String.valueOf(c) + ',';
                    }
                    
                    csv += String.valueOf(r.GrandTotalCost);
                    
                    // Sum of Total Submitted
                    csv += '\n';
                    if (ReportBy == 'GL by Participant')
                    {
                        csv += clientNames.get(clientId) + ',';
                    }
                    
                    csv += r.GLCode + ',Sum of Total Submitted,';
                    for (Decimal c : r.TotalSubmitted)
                    {
                        csv += String.valueOf(c) + ',';
                    }
                    
                    csv += String.valueOf(r.GrandTotalSubmitted);
                    
                    // Amount Accrued
                    csv += '\n';
                    if (ReportBy == 'GL by Participant')
                    {
                        csv += clientNames.get(clientId) + ',';
                    }
                    
                    csv += r.GLCode + ',Amount Accrued,';
                    for (Decimal c : r.AmountAccrued)
                    {
                        csv += String.valueOf(c) + ',';
                    }
                    
                    csv += String.valueOf(r.GrandAmountAccrued);
                } 
                
                if (ReportBy == 'GL by Participant')
                {
                    // sub totals
                    csv += '\n';   
                    csv += clientNames.get(clientId) + ','; 
                    csv += 'Sub Total,Sum of Total Cost,';
                    for (Decimal c : SubTotals.get(clientId).TotalCost)
                    {
                        csv += String.valueOf(c) + ',';
                    }
                    csv += String.valueOf(SubTotals.get(clientId).GrandTotalCost);
                    
                    csv += '\n';   
                    csv += clientNames.get(clientId) + ','; 
                    csv += 'Sub Total,Sum of Total Submitted,';                    
                    for (Decimal c : SubTotals.get(clientId).TotalSubmitted)
                    {
                        csv += String.valueOf(c) + ',';
                    }
                    csv += String.valueOf(SubTotals.get(clientId).GrandTotalSubmitted);
                    
                    csv += '\n';   
                    csv += clientNames.get(clientId) + ','; 
                    csv += 'Sub Total,Amount Accrued,';                    
                    for (Decimal c : SubTotals.get(clientId).AmountAccrued)
                    {
                        csv += String.valueOf(c) + ',';
                    }
                    csv += String.valueOf(SubTotals.get(clientId).GrandAmountAccrued);
                }                               
            }
            
            // grand totals
            csv += '\n';  
            if (ReportBy == 'GL by Participant') 
            {
                csv += ','; 
            }
            csv += 'Grand Total,Sum of Total Cost,';
            for (Decimal c : GrandTotals.TotalCost)
            {
                csv += String.valueOf(c) + ',';
            }
            csv += String.valueOf(GrandTotals.GrandTotalCost);
            
            csv += '\n';   
            if (ReportBy == 'GL by Participant') 
            {
                csv += ','; 
            }
            csv += 'Grand Total,Sum of Total Submitted,';                    
            for (Decimal c : GrandTotals.TotalSubmitted)
            {
                csv += String.valueOf(c) + ',';
            }
            csv += String.valueOf(GrandTotals.GrandTotalSubmitted);
            
            csv += '\n';   
            if (ReportBy == 'GL by Participant') 
            {
                csv += ','; 
            }
            csv += 'Grand Total,Amount Accrued,';                    
            for (Decimal c : GrandTotals.AmountAccrued)
            {
                csv += String.valueOf(c) + ',';
            }
            csv += String.valueOf(GrandTotals.GrandAmountAccrued);
        
            return csv;
        }        
    }
    
    private Decimal IsNull(Decimal d)
    {
        if (d == null)
        {
            return 0;
        }
        return d;
    }
                          
    private class ReportRow
    {
        public ReportRow(String gc, Integer cols)
        {            
            GLCode = gc;
            TotalCost = new List<Decimal>();
            TotalSubmitted = new List<Decimal>();
            AmountAccrued = new List<Decimal>();
            
            for (Integer i = 0; i < cols; i++)
            {
                TotalCost.add(0);    
                TotalSubmitted.add(0);    
                AmountAccrued.add(0);    
            }
            
            GrandTotalCost = 0;
            GrandTotalSubmitted = 0;
            GrandAmountAccrued = 0;
        }
        
        public String Client { get; set; }        
        public String GLCode { get; set; }
        public List<Decimal> TotalCost { get; set; }
        public List<Decimal> TotalSubmitted { get; set; }
        public List<Decimal> AmountAccrued { get; set; }     
        public Decimal GrandTotalCost { get; set; }
        public Decimal GrandTotalSubmitted { get; set; }
        public Decimal GrandAmountAccrued { get; set; }                     
    }
}