@isTest
private class TestRCRScheduledServiceCostReportExt
{
    static testMethod void myUnitTest1() 
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
        
        TestLoadData.loadAdhocServices();
        
        RCR_Contract__c so;
        
        for (RCR_Contract__c c : [SELECT Id, 
                                         RecordTypeId,
                                         (SELECT Id
                                          FROM RCR_Contract_Scheduled_Services__r),
                                         (SELECT Id
                                          FROM RCR_Contract_Adhoc_Services__r)                                      
                                  FROM RCR_Contract__c LIMIT 1])
        {
            if (c.RCR_Contract_Scheduled_Services__r.size() > 0)
            {
                so = c;
                break;
            }                 
        }                                  
                              
        so.RecordTypeId = [SELECT Id
                           FROM RecordType
                           WHERE sObjectType = 'RCR_Contract__c'
                           AND Name = 'Service Order'].Id;
        update so;                           
        
        ApexPages.CurrentPage().getParameters().put('so', so.Id);
        ApexPages.CurrentPage().getParameters().put('SelectedServiceOrderID', so.Id);
        ApexPages.CurrentPage().getParameters().put('css', so.RCR_Contract_Scheduled_Services__r[0].Id);        
        RCRScheduledServiceCostReportExtension ext = new RCRScheduledServiceCostReportExtension(new ApexPages.StandardController(so));
        
        system.debug(ext.TotalDollarsApprovedByServiceID);
        system.debug(ext.RenderScheduledService);
                
        ext.loadServiceOrder();        
        
        Test.StopTest();
    }
    
    static testMethod void myUnitTest2() 
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
        
        TestLoadData.loadAdhocServices();
        
        RCR_Contract__c so;
        
        for (RCR_Contract__c c : [SELECT Id, 
                                         RecordTypeId,
                                         (SELECT Id
                                          FROM RCR_Contract_Scheduled_Services__r),
                                         (SELECT Id
                                          FROM RCR_Contract_Adhoc_Services__r)                                      
                                  FROM RCR_Contract__c LIMIT 1])
        {
            if (c.RCR_Contract_Adhoc_Services__r.size() > 0)
            {
                so = c;
                break;
            }                 
        }                                  
                              
        so.RecordTypeId = [SELECT Id
                           FROM RecordType
                           WHERE sObjectType = 'RCR_Contract__c'
                           AND Name = 'Service Order'].Id;
        update so;                           
        
        ApexPages.CurrentPage().getParameters().put('so', so.Id);
        ApexPages.CurrentPage().getParameters().put('SelectedServiceOrderID', so.Id);        
        ApexPages.CurrentPage().getParameters().put('ahs', so.RCR_Contract_Adhoc_Services__r[0].Id);
        RCRScheduledServiceCostReportExtension ext = new RCRScheduledServiceCostReportExtension(new ApexPages.StandardController(so));
                        
        system.debug(ext.RenderAdHocervice);
        system.debug(ext.AdHocServicesByID);
        
        ext.loadServiceOrder();
        //ext.loadAdHocServiceData();
        
        Test.StopTest();
    }
}