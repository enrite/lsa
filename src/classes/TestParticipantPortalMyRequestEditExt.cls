@isTest
public class TestParticipantPortalMyRequestEditExt
{                    
    static testMethod void myTestMethod()
    {            
        TestLoadFormData data = new TestLoadFormData();
                 
        Test.StartTest();
        
        Request__c r = new Request__c();
        r.Client__c = data.Client.Id;
        r.Request_Details__c = 'test';
                                
        ParticipantPortalMyRequestEditExtension ext = new ParticipantPortalMyRequestEditExtension(new ApexPages.StandardController(r));
        ext.SaveOverride();                                                                      
        
        Test.StopTest();
    }
}