public with sharing class RCRActivityStatementDeleteExtension
{
    public RCRActivityStatementDeleteExtension(ApexPages.StandardController ctrlr)
    {
        controller = ctrlr;
    }
    private ApexPages.StandardController controller;

    public PageReference redir()
    {
        for(RCR_Invoice__c inv : [SELECT ID
                                FROM    RCR_Invoice__c
                                WHERE   ID = :controller.getId() AND
                                        RCR_Invoice_Batch__r.Masterpiece_Paid__c != null])
        {
            return A2HCException.formatException('This invoice has been paid by Masterpiece and cannot be deleted');
        }
        PageReference pg = new PageReference(controller.delete().getUrl());
        pg.getParameters().put('nooverride', '1');
        return pg;
    }
}