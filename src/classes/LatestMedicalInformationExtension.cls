public with sharing class LatestMedicalInformationExtension
{
    public LatestMedicalInformationExtension(ApexPages.StandardController c)
    {
        record = (Referred_Client__c)c.getRecord();
    }

    private Referred_Client__c record;
    
    public Form__c LatestMedicalCertificate
    {
        get
        {
            if (LatestMedicalCertificate == null)
            {   
                LatestMedicalCertificate = new Form__c();
                         
                for (Form__c f : [SELECT Id,
                                         Brain_Injury__c,
                                         FIM_item__c,
                                         Less_than_3_Permanent_Impairment__c,
                                         Spinal_Cord_Injury__c,
                                         Neurological_SCI_level__c,
                                         ASIA_impairment_scale__c,
                                         ISAFSCI_scores__c,
                                         Brachial_plexus_avulsion_or_rupture__c,
                                         Single_forequarter_amputation__c,
                                         Single_amputation_of_the_lower_limb__c,
                                         Multiple_amputations_lower_limbs__c,
                                         Multiple_amputations_upper_limbs__c,
                                         Burns_Aged_16_and_under__c,
                                         Burns_Aged_Over_16__c,
                                         Burns_FIM_item__c,
                                         The_injured_person_is_legally_blind__c,
                                         Treating_Doctor_s_Name__c,
                                         Hospital_or_Rehabilitation_Unit__c,
                                         Signature_Date__c,
                                         Injury__c
                                  FROM Form__c
                                  WHERE Client__c = :record.Id
                                  AND RecordType.DeveloperName = 'Medical_Certificate'
                                  ORDER BY Signature_Date__c DESC NULLS LAST, CreatedDate DESC])
                {
                    LatestMedicalCertificate = f;   
                }                                                                          
            }
            return LatestMedicalCertificate;
        }
        set;
    }
}