public class RCRScheduledServiceOrderCloneBatch  
		extends RCRServiceOrderCloneBase 
		implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts
{
	public RCRScheduledServiceOrderCloneBatch() 
	{
	}
	
	public RCRScheduledServiceOrderCloneBatch(Date pNewEndDate)
	{
		super(pNewEndDate);
	}
	
	public RCRScheduledServiceOrderCloneBatch(RCRServiceOrderCloneBase previousBatch) 
	{
		super(previousBatch);
	}  

    public Database.QueryLocator start(Database.BatchableContext batch)
    {
    	reparentBatchLogs(batch); 
    	
    	if(ScheduleId != null)
    	{
    		ServiceOrderIds = null;
	        for(RCR_Contract__c c : [SELECT ID
	        						FROM	RCR_Contract__c
                                    WHERE   Schedule__c = :ScheduleId])
			{                                    
                ServiceOrderIds.add(c.ID);                
			}
    	}
    	return Database.getQueryLocator([SELECT Id,
												Name,
												Service_Description__c,
												Start_Date__c,
												End_Date__c,
												Original_End_Date__c,
												RCR_Service__c,
												RCR_Service__r.Name,
												Service_Code__c,
												Flexibility__c,
												Public_holidays_not_allowed__c,
												Total_Cost__c,
												Service_Types__c,
												Exception_Count__c,
												Use_Negotiated_Rates__c,
												Negotiated_Rate_Standard__c,
												Negotiated_Rate_Public_Holidays__c,
												Comment__c,
												Internal_Comments__c,
												Rollover_Comment__c,
												Funding_Commitment__c,
												Expenses__c,
												RCR_Contract__c,
												GL_Category__c,
                                                RCR_Contract__r.Name,
                                                RCR_Contract__r.CCMS_ID__c,
                                                RCR_Contract__r.Start_Date__c,
                                                RCR_Contract__r.End_Date__c,
                                                RCR_Contract__r.Original_End_Date__c,
                                                RCR_Contract__r.Cancellation_Date__c,
                                                RCR_Contract__r.Account__c,
                                                RCR_Contract__r.Account__r.Name,
                                                RCR_Contract__r.Level__c,
                                                RCR_Contract__r.Comments__c,
                                                RCR_Contract__r.Internal_Comments__c,
                                                RCR_Contract__r.Data_Migration_Status__c,
                                                RCR_Contract__r.Migration_User__c,
                                                RCR_Contract__r.Roll_Over_Service_Order__c,
                                                RCR_Contract__r.Compensable__c,
                                                RCR_Contract__r.Signed_By_All_Parties__c,
												RCR_Contract__r.Source_Record__c,
												RCR_Contract__r.RecordType.Name,
												RCR_Contract__r.RecordType.DeveloperName,
												Source_Record__r.Start_Date__c,
                                                (SELECT Id,
												        RCR_Program__c,
												        RCR_Contract_Adhoc_Service__c,
												        RCR_Contract_Scheduled_Service__c,
												        RCR_Program_Category_Service_Type__c,
												        Quantity__c,
														GL_Category__c
												 FROM   RCR_Contract_Service_Types__r),
												(SELECT Id,
												        Date__c,
												        Public_Holiday_Quantity__c,
												        Standard_Quantity__c,
												        Description__c,
												        RCR_Contract_Scheduled_Service__c
												FROM    RCR_Scheduled_Service_Exceptions__r
												ORDER BY Date__c)
                                       FROM     RCR_Contract_Scheduled_Service__c
                                       WHERE    RCR_Contract__c IN :ServiceOrderIds]);                                      
    }

    public void execute(Database.BatchableContext batch, List<RCR_Contract_Scheduled_Service__c> scheduledServices)
    {
        List<Batch_Log__c> batchLogs = new List<Batch_Log__c>(); 
        
        for(RCR_Contract_Scheduled_Service__c css : scheduledServices)
        {     
        	if(Mode == RunMode.Rollover &&
//        			(
//        				(css.RCR_Contract__r.RCR_CBMS_Contract__c == null &&
        				css.Funding_Commitment__c != APS_PicklistValues.RCRContract_FundingCommitment_Recurrent
//        				||
////        				(css.RCR_Contract__r.RCR_CBMS_Contract__c != null &&
//        				css.RCR_Contract__r.RCR_CBMS_Contract__r.Contract_Type__c != APS_PicklistValues.RCRCBMSContract_ContractType_Recurrent)
    				)
			{
    			continue;	
			} 
        	System.savePoint sp = Database.setSavepoint();  
	        RCR_Contract__c existingServiceOrder = getServiceOrder(css.RCR_Contract__c);
	        lastModifiedUserIds.add(existingServiceOrder.LastModifiedByID);	        
        	RCR_Contract__c newServiceOrder = processRecord(existingServiceOrder, sp, batchLogs, batch);  
        	if(newServiceOrder != null)
        	{                  		                                     
		        try
		        {
					if(!cloneScheduledService(existingServiceOrder, newServiceOrder, css, batchLogs, batch)) 
					{
						handleError(existingServiceOrder, 
		            				newServiceOrder,
		            				sp, 
		            				batchLogs, 
		            				batch, 
		            				null, 
		            				css.RCR_Service__r.Name);
					}													
		        }
		        catch(Exception ex)
		        {
		            handleError(existingServiceOrder, 
		            				newServiceOrder,
		            				sp, 
		            				batchLogs, 
		            				batch, 
		            				ex, 
		            				css.RCR_Service__r.Name);						
		        }	 
        	}       	            
        }
        upsert batchLogs;
    }

    public void finish(Database.BatchableContext info)   
    {    	
    	if(Mode == RunMode.RequestApproved)
    	{
    		String templateName = null;
    		//only send email if errors encountered
			for(Batch_Log__c bl : [SELECT 	Error__c
									FROM	Batch_Log__c
									WHERE	Name = :info.getJobId()])
			{		
				if(String.isNotBlank(bl.Error__c))	
				{		
					templateName = 'Request_Approval_Errors';
					break;
				}
			}
			// if templateName == null, no emails will be sent
    		finaliseBatch(info, templateName);
    	}
    	else
    	{
	    	RCRServiceOrderCloneSchedule scheduleClass = new RCRServiceOrderCloneSchedule(this);     
			String scheduleID = BatchUtilities.getNextScheduleID(scheduleClass, RCRAdhocServiceOrderCloneBatch.class, 'Service_Order_Adhoc'); 
			List<RCR_Contract__c> serviceOrders = [SELECT 	ID
													FROM	RCR_Contract__c
													WHERE	ID IN :ServiceOrderIds];
			for(RCR_Contract__c so : serviceOrders)
			{
	        	so.Schedule__c = scheduleID;
			}
			update serviceOrders;
    	}
    }       
       
    public RCR_Contract__c cloneServiceOrder(RCR_Contract__c serviceOrder)
    {
    	try
    	{
	    	if(NewEndDate == null)
	    	{
	    		throw new A2HCException('New End Date is not defined.');
	    	}
	    	RCR_Contract__c so = copyServiceOrder(serviceOrder); 
	    	return so;
    	}
    	catch(Exception ex)
    	{
    		throw new A2HCException(ex);
    	}
    }

    public Boolean cloneScheduledService(RCR_Contract__c oldServiceOrder,
    											RCR_Contract__c newServiceOrder, 
    											RCR_Contract_Scheduled_Service__c oldScheduledService,
    											List<Batch_Log__c> batchLogs,
    											Database.BatchableContext batch) 
    {    	
    	RCR_Service__c service = [SELECT r.ID,
				                        r.Name,
				                        r.Day_Of_Week__c,
				                        r.Description__c,
				                        (SELECT Name,
				                        		Start_Date__c,
				                                Rate__c,
				                                Public_Holiday_Rate__c,				                                
				                                End_Date__c,
				                                Provisional_Rate__c,
				                                RCR_Service__c
				                         FROM   RCR_Service_Rates__r)
				                 FROM   RCR_Service__c r
				                 WHERE  r.Id = :oldScheduledService.RCR_Service__c];
			                 			                 				
		RCRScheduledServiceExtension schedServiceExtension = new RCRScheduledServiceExtension(false);
        schedServiceExtension.RCRContract = newServiceOrder; 
        Integer rolloverIncrement = 0;
        if(Mode == RunMode.Rollover)
        {
        	rolloverIncrement = oldServiceOrder.Start_Date__c.daysBetween(newServiceOrder.Start_Date__c) + 1;
        }
        
        RCR_Contract_Scheduled_Service__c newScheduledService = createScheduledService(oldScheduledService, oldServiceOrder, newServiceOrder, rolloverIncrement); 
        
        List<RCR_Service_Rate__c> rates = new List<RCR_Service_Rate__c>();
        for(RCR_Service_Rate__c rate : service.RCR_Service_Rates__r)
        {
        	if(!rate.Provisional_Rate__c && 
        		(
        			(rate.Start_Date__c <= newScheduledService.End_Date__c && rate.End_Date__c >= newScheduledService.Start_Date__c) ||
        			(rate.Start_Date__c >= newScheduledService.Start_Date__c && rate.End_Date__c <= newScheduledService.End_Date__c)
        		)
    		)
    		{
    			rates.add(rate);
    		}
        }       
		String errMsg = RCRScheduledServiceExtension.isValid(newScheduledService, rates, true);
        if(errMsg != null)
        {
			addBatchLog(oldServiceOrder, batchLogs, batch, null, oldScheduledService.RCR_Service__r.Name + ': ' + errMsg);
			return false;
        }

		cloneSchedule(oldScheduledService, newScheduledService);
        cloneServiceTypes(oldScheduledService.RCR_Contract_Service_Types__r, newScheduledService.Id, 'Scheduled');
        rolloverIncrement = 0; 
        if(Mode == RunMode.Rollover)
        {
        	rolloverIncrement = oldScheduledService.Start_Date__c.daysBetween(newScheduledService.Start_Date__c);
        }        
		cloneScheduledServiceExceptions(oldScheduledService.RCR_Scheduled_Service_Exceptions__r, 
												newScheduledService, 
												rolloverIncrement);		
		if(Mode == RunMode.Rollover)
		{																						
			if(!oldScheduledService.RCR_Scheduled_Service_Exceptions__r.isEmpty())
			{
				setRolloverComment(newScheduledService, 'Scheduled Service has Service Exceptions.');
			}							
	        if(newScheduledService.Flexibility__c == APS_PicklistValues.RCRContractScheduledService_Flexibilty_Accumulative)
	        {
	        	setRolloverComment(newScheduledService, 'Scheduled Service has a Flexibilty of ' + newScheduledService.Flexibility__c + '.');
	        }	
	        if(oldScheduledService.RCR_Contract__r.Signed_By_All_Parties__c == null)
	        {
	        	setRolloverComment(newScheduledService, 'Service Order has not been signed by both parties.'); 
	        }	
		}	
		newScheduledService = getScheduledService(newScheduledService.ID);    	
        schedServiceExtension.setTotals(newScheduledService);														       
        update newScheduledService;
        schedServiceExtension.saveScheduledServiceBreakdown(newScheduledService, false);
        
        addBatchLog(oldServiceOrder, batchLogs, batch, 'Scheduled Service created: ' + oldScheduledService.RCR_Service__r.Name + '.', null); 
        return true;
    }

	private RCR_Contract_Scheduled_Service__c createScheduledService(RCR_Contract_Scheduled_Service__c oldScheduledService, 
																		RCR_Contract__c oldServiceOrder,
																		RCR_Contract__c newServiceOrder,
																		Integer rolloverIncrement)
	{		
		RCR_Contract_Scheduled_Service__c newScheduledService = oldScheduledService.clone(false, true);				
		newScheduledService.RCR_Contract__c = newServiceOrder.Id;
		newScheduledService.Source_Record__c = oldScheduledService.ID;
		newScheduledService.Rollover_Comment__c = null;
		if(Mode == RunMode.Rollover)
		{		
			setServiceDates(oldServiceOrder, newServiceOrder, oldScheduledService, newScheduledService);
		}
        insert newScheduledService;

        // Refresh the newScheduledService
        // Need to get the results of formula fields, especially End Date
        newScheduledService = getScheduledService(newScheduledService.ID);                          
        return newScheduledService;
	}

    private void cloneSchedule(RCR_Contract_Scheduled_Service__c oldScheduledService, RCR_Contract_Scheduled_Service__c newScheduledService)
    {
    	Schedule__c oldSchedule = [SELECT	Name ,
										s.Start_Date__c,
			                    		s.End_Date__c,
			                    		s.Schedule_Recurrence_Pattern__c,
			                    		s.Number_of_weeks_in_period__c,
			                    		s.Quantity_Per_Period__c,
			                    		(SELECT Schedule__c,
			                    				Week_Number__c,
			                    				Week_Of_Month__c,
			                    				Monday__c,
			                    				Tuesday__c,
			                    				Wednesday__c,
			                    				Thursday__c,
			                    				Friday__c,
			                    				Saturday__c,
			                    				Sunday__c,
			                    				Total_Quantity__c
			                    		 FROM	Schedule_Periods__r
			                    		 ORDER BY Week_Number__c)
			                    FROM	Schedule__c s
			                    WHERE	Name = :oldScheduledService.ID];
			                    
    	List<Schedule_Period__c> newSchedulePeriods = new List<Schedule_Period__c>();
        Schedule__c newSchedule = oldSchedule.clone(false, true);
        newSchedule.Start_Date__c = newScheduledService.Start_Date__c;
        newSchedule.End_Date__c = newScheduledService.End_Date__c;
        newSchedule.Name = newScheduledService.Id;
        insert newSchedule;

        for(Schedule_Period__c p : oldSchedule.Schedule_Periods__r)
        {
            Schedule_Period__c newScheduledPeriod = p.clone(false, true);
            newScheduledPeriod.Schedule__c = newSchedule.Id;
            newSchedulePeriods.add(newScheduledPeriod);
        }
		insert newSchedulePeriods;
    }

    private void cloneScheduledServiceExceptions(List<RCR_Scheduled_Service_Exception__c> oldExceptions, 
    													RCR_Contract_Scheduled_Service__c newSchedService, 
    													Integer daysAdded)
    {
    	if(Mode == RunMode.RolloverApproved)
		{	
			daysAdded = 0;
		}
    	List<RCR_Scheduled_Service_Exception__c> newExceptions = new List<RCR_Scheduled_Service_Exception__c>();
    	for(RCR_Scheduled_Service_Exception__c ex : oldExceptions)
        {
            RCR_Scheduled_Service_Exception__c newException = ex.clone(false, true);
            newException.RCR_Contract_Scheduled_Service__c = newSchedService.ID;
            newExceptions.add(newException);
        }
        insert newExceptions; 
    }
    
    private RCR_Contract_Scheduled_Service__c getScheduledService(ID cssID)
    {
    	return [SELECT  Id,
						Name,
						Service_Description__c,
						Start_Date__c,
						End_Date__c,
						Original_End_Date__c,
						RCR_Service__c,
						RCR_Service__r.Name,
						Service_Code__c,
						Flexibility__c,
						Public_holidays_not_allowed__c,
						Total_Cost__c,
						Service_Types__c,
						Exception_Count__c,
						Use_Negotiated_Rates__c,
						Negotiated_Rate_Standard__c,
						Negotiated_Rate_Public_Holidays__c,
						Rollover_Comment__c,
						Comment__c,
						Internal_Comments__c,
						Expenses__c,
						GL_Category__c,
						Source_Record__c,
						Source_Record__r.Start_Date__c,
						RCR_Contract__c,
                        RCR_Contract__r.Name,
                        RCR_Contract__r.CCMS_ID__c,
                        RCR_Contract__r.Start_Date__c,
                        RCR_Contract__r.End_Date__c,
                        RCR_Contract__r.Original_End_Date__c,
                        RCR_Contract__r.Cancellation_Date__c,
                        RCR_Contract__r.Account__c,
                        RCR_Contract__r.Account__r.Name,
                        RCR_Contract__r.Level__c,
                        RCR_Contract__r.Comments__c,
                        RCR_Contract__r.Internal_Comments__c,
                        RCR_Contract__r.Data_Migration_Status__c,
                        RCR_Contract__r.Migration_User__c,
                        RCR_Contract__r.Roll_Over_Service_Order__c,
                        RCR_Contract__r.Compensable__c,
                        RCR_Contract__r.Signed_By_All_Parties__c,
						RCR_Contract__r.Source_Record__c,
						RCR_Contract__r.RecordType.Name,
						RCR_Contract__r.RecordType.DeveloperName,
                        (SELECT Id,
						        RCR_Program__c,
						        RCR_Contract_Adhoc_Service__c,
						        RCR_Contract_Scheduled_Service__c,
						        RCR_Program_Category_Service_Type__c,
						        Quantity__c,
								GL_Category__c
						 FROM   RCR_Contract_Service_Types__r),
						(SELECT Id,
						        Date__c,
						        Public_Holiday_Quantity__c,
						        Standard_Quantity__c,
						        Description__c,
						        RCR_Contract_Scheduled_Service__c
						FROM    RCR_Scheduled_Service_Exceptions__r
						ORDER BY Date__c)
        FROM    RCR_Contract_Scheduled_Service__c
        WHERE   Id = :cssId];
    }

	
}