public with sharing class FormDetailEditRedirectExtension
{
    public FormDetailEditRedirectExtension(ApexPages.StandardController c)
    {
        record = (Form_Detail__c)c.getRecord();
    }

    private Form_Detail__c record;
    
    public PageReference Redir()
    {
        try
        {    
            // if this is an existing record then check if the current user has edit access
            if (record.Id != null)
            {
                Boolean hasEditAccess = false;
                
                // query for user edit access
                for (UserRecordAccess ura : [SELECT RecordId
                                             FROM UserRecordAccess
                                             WHERE UserId = :UserInfo.getUserId()
                                             AND RecordId = :record.Id 
                                             AND HasEditAccess = true])
                {                                             
                    hasEditAccess = true; 
                }
                
                // check access based on user role and record type                
                for (UserRole r : [SELECT Name
                                   FROM UserRole
                                   WHERE Id = :UserInfo.getUserRoleId()])
                {        
                    if (
                            (r.Name == 'Service Planners' && 
                                (record.RecordType.DeveloperName == 'Manager_ASD_Application_Assessment' || 
                                record.RecordType.DeveloperName == 'Chief_Executive_Application_Assessment' ||
                                record.RecordType.DeveloperName == 'Lifetime_Assessment_CE_Approval')
                            )
                            || 
                            (r.Name == 'Manager ASD' && 
                                (
                                //record.RecordType.DeveloperName == 'Service_Planner_Application_Assessment' || 
                                record.RecordType.DeveloperName == 'Chief_Executive_Application_Assessment'  ||
                                record.RecordType.DeveloperName == 'Lifetime_Assessment_CE_Approval')
                            )
                        /*|| (r.Name == 'CE' && (record.RecordType.DeveloperName == 'Service_Planner_Application_Assessment' || record.RecordType.DeveloperName == 'Manager_ASD_Application_Assessment'))*/
                        )
                    {
                        hasEditAccess = false; 
                    } 
                }
                
                // if no edit access then throw an exception
                if (!hasEditAccess)
                {
                    return A2HCException.formatException('You do not have permission to edit this record');
                }                                                   
            }
                                                                             
            Map<String, PageReference> pages = new Map<String, PageReference>();
            pages.put('Lifetime_Assessment_Start', Page.lasApplicationAssessmentEdit);  
            pages.put('Lifetime_Assessment_CE_Approval', Page.lasApplicationAssessmentEdit); 
            pages.put('Service_Planner_Application_Assessment', Page.SPApplicationAssessmentEdit); 
            pages.put('Manager_ASD_Application_Assessment', Page.MASDApplicationAssessmentEdit);            
            pages.put('Chief_Executive_Application_Assessment', Page.CEApplicationAssessmentEdit);            
            pages.put('Assessment_Complete', Page.CEApplicationAssessmentEdit);   

        
            RecordType rt = [SELECT Name,
                                    DeveloperName
                             FROM RecordType
                             WHERE Id = :record.RecordTypeId
                             OR Id = :(record.RecordTypeId == null ? ApexPages.CurrentPage().getParameters().get('RecordType') : '~')];
            
            // if the type of the new record if CE Application Assessment then we need to check if Manager has recommended form to CE
            // if this is a new record then check if it can be created
            if (record.Id == null)
            {
                if (rt.DeveloperName == 'Internal_Review' || rt.DeveloperName == 'Expert_Panel_Review')
                {
                    Boolean ceCompleted = false;
                    
                    for (Form_Detail__c fd : [SELECT Id
                                              FROM Form_Detail__c
                                              WHERE Form_Application_Assessment__c = :record.Form_Application_Assessment__c                                                                                         
                                              AND Status__c IN ('Not Accepted', 'Accepted as Interim Participant', 'Lifetime Participant')
                                              LIMIT 1])
                    {
                        ceCompleted = true;
                    }                      
                    
                    if (!ceCompleted)
                    {
                        return A2HCException.formatException('You cannot create an ' + rt.Name + ' until an Application Assessment has been completed');
                    }    
                } 
                else if (rt.DeveloperName == 'District_Court_Review')
                {
                    Boolean canCreate = false;
                    
                    for (Form_Detail__c fd : [SELECT Id
                                              FROM Form_Detail__c
                                              WHERE Form_Application_Assessment__c = :record.Form_Application_Assessment__c
                                              AND RecordType.DeveloperName IN ('Internal_Review', 'Expert_Panel_Review')
                                              AND Review_Decision_Date__c != null
                                              LIMIT 1])
                    {
                        canCreate = true;
                    }                      
                    
                    if (!canCreate)
                    {
                        return A2HCException.formatException('You cannot create a District Court Review until an Internal Review or Expert Panel Review has been completed');
                    }    
                }                                   
            }
            else
            {
                // if the record type is assessment complete then the record cannot be edited
                if (rt.DeveloperName == 'Assessment_Complete')
                {
                    // check if the current user has the CE profile
                    Boolean isCE = false;
                    
                    for (UserRole r : [SELECT Id
                                       FROM UserRole
                                       WHERE Id = :UserInfo.getUserRoleId()
                                       AND Name = 'CE'])
                    {
                        isCE = true;
                    }                                       
                    
                    // if the current user is not a CE then they can't edit a completed assessment
                    if (!isCE)
                    {
                        return A2HCException.formatException('You cannot edit a completed assessment');    
                    }
                }
            }
            
            // find the page reference to redirect to
            PageReference pg;       
            if (pages.containsKey(rt.DeveloperName))
            {                        
                pg = pages.get(rt.DeveloperName);
            }            
            else
            {
                if (record.Id == null)
                {
                    pg = new PageReference('/' + Form_Detail__c.sObjectType.getDescribe().getKeyPrefix() + '/e');    
                }
                else
                {
                    pg = new PageReference('/' + record.Id + '/e');
                }
                
                pg.getParameters().put('nooverride', '1');
            }
            
            // add the parameters for this page to the new page reference
            for (String k : ApexPages.CurrentPage().getParameters().keySet())
            {
                if (k != 'save_new' && k != '_CONFIRMATIONTOKEN')
                {
                    pg.getParameters().put(k, ApexPages.CurrentPage().getParameters().get(k));
                }
            }
            
            return pg;
        }
        catch(Exception e)
        {
            return A2HCException.formatException(e);
        }
    }
}