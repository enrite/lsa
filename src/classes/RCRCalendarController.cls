public class RCRCalendarController extends A2HCPageBase
{
    public RCRCalendarController(ApexPages.StandardController ctr)
    {
        initialise(ctr.getID());
    }

    public RCRCalendarController()
    {
        initialise(getParameter('retURL'));
    }

    public List<ScheduleDayItemWrapper> displayIDNamesList
    {
        get
        {
            if(displayIDNamesList == null)
            {
                displayIDNamesList = new List<ScheduleDayItemWrapper>();
            }
            return displayIDNamesList;
        }
        set;
    }


    public List<Id> adHocServiceIDsList
    {
        get
        {
            if(adHocServiceIDsList == null)
            {
                adHocServiceIDsList = new List<Id>();
            }
            return adHocServiceIDsList;
        }
        private set;
    }


    public String ContextName
    {
        get;
        set;
    }


    private void initialise(String contractID)
    {
        if(contractID != null)
        {
            contractID = contractID.replace('/', '');
        }
        for(RCR_Contract_Scheduled_Service__c s :[SELECT r.Service_Code__c,
                                                        r.Service_Description__c,
                                                        r.Id,
                                                        r.RCR_Contract__r.Name
                                                 FROM   RCR_Contract_Scheduled_Service__c r
                                                 WHERE  r.RCR_Contract__c = :contractID])
         {
            displayIDNamesList.add(new ScheduleDayItemWrapper(s.Id, s.Service_Code__c));
            ContextName = s.RCR_Contract__r.Name;
         }
         
         for(RCR_Contract_Adhoc_Service__c ahs : [SELECT  Id
                                                   FROM   RCR_Contract_Adhoc_Service__c
                                                   WHERE  RCR_Contract__c = :contractID])
        {
            adHocServiceIDsList.add(ahs.Id);
        }
    }
    
    @IsTest//(SeeAllData=true)
    static void test1()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();

        RCR_Contract_Scheduled_Service__c css = 
                        [SELECT     ID,
                                    RCR_Contract__c
                        FROM        RCR_Contract_Scheduled_Service__c
                        WHERE       Total_Cost__c > 0
                        LIMIT 1];

        ApexPages.currentPage().getParameters().put('contractID', css.RCR_Contract__c);
        RCRCalendarController ctr = new RCRCalendarController();
        ctr = new RCRCalendarController(new ApexPages.StandardController([SELECT ID FROM RCR_Contract__c WHERE ID = :css.RCR_Contract__c]));
        system.debug(ctr.displayIDNamesList);
        ctr.displayIDNamesList = null;
        
        Test.StopTest();
    }
}