public class LetterTemplatePDFController
{
    public LetterTemplatePDFController()
    {
        PagesAfterFirst = new List<Integer>();
        
        Integer pages = 0;
        if (ApexPages.CurrentPage().getParameters().containsKey('pages'))
        {
            pages = Integer.valueOf(ApexPages.CurrentPage().getParameters().get('pages'));
        }
                              
        for (Integer i = 2; i <= pages; i++)
        {
            PagesAfterFirst.add(i);
        }
    }

    public List<Integer> PagesAfterFirst { get; set; }
}