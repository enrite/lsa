/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestObjectHistoryController {

    static testMethod void myUnitTest()
    {
        RCR_Directorate__c ref = new RCR_Directorate__c();
        ref.Name = 'test';
        ref.Active__c = true;
        insert ref;
        
        ref = [SELECT Id,
                      Name,
                      Active__c,
                      CreatedDate,
                      LastModifiedDate
               FROM RCR_Directorate__c
               LIMIT 1];
                                               
        ObjectHistoryController ext = new ObjectHistoryController();
        
        ext.Record = ref;

        ext.RecordLimit = null;
        system.debug(ext.RecordLimit);

        system.debug(ext.ObjectLabel);

        system.debug(ext.ObjectHistory);    
        
        ext.getText(ref, 'CreatedDate');
        ext.getText(ref, 'LastModifiedDate');
        ext.getText(ref, 'Name');
                        
        Map<String, Schema.SObjectField> objectFieldMap = new Map<String, Schema.SObjectField>();
        ext.getFieldLabel('created', ref.getSObjectType().getDescribe().fields.getMap(), ref.getSObjectType().getDescribe());
        ext.getFieldLabel('Name', ref.getSObjectType().getDescribe().fields.getMap(), ref.getSObjectType().getDescribe());
        
        RCR_Program__c p = new RCR_Program__c(Name = 'test');
        insert p;
        
        RCR_Program_Category__c pc = new RCR_Program_Category__c(RCR_Program__c = p.Id, Name = 'test');
        insert pc;
        
        RCR_Program_Category_Service_Type__c pcst = new RCR_Program_Category_Service_Type__c(RCR_Program_Category__c = pc.Id);
        insert pcst;
                        
        ext.getFieldLabel('Allocation_Service_Type__c', new Map<String, Schema.SObjectField>(), pcst.getSObjectType().getDescribe());
        
        ObjectHistoryController.ObjectHistoryLine ohl = new ObjectHistoryController.ObjectHistoryLine();
        system.debug(ohl.theDate);
        system.debug(ohl.who);
        system.debug(ohl.userId);
        system.debug(ohl.action);
    }
}