public class CsvParser
{
    public CsvParser(String fileContent, Boolean ignoreFirstLine)
    {
        initialise(fileContent, ignoreFirstLine);
    }

    public CsvParser(String fileContent, Boolean ignoreFirstLine, Integer fieldsToParse)
    {
        lastFieldToParse = fieldsToParse;
        initialise(fileContent, ignoreFirstLine);
    }

    private String delim = ',';
    private String[] fields;
    private String buffer;
    private Integer lastFieldToParse = 0;
    private final String NEWLINE = '\n';

    //Read and parse next available line. Return null if end of stream.
    public String[] ReadLine()
    {
    	try
    	{
	        if(buffer.length() == 0)
	        {
	            return new String[]{};
	        }
            String line; 
            if(buffer.indexOf(NEWLINE) >= 0)
            {
                line = buffer.substring(0, buffer.indexOf(NEWLINE)); 
                buffer = buffer.removeStart(line + NEWLINE);
            }
            else
            {
                line = buffer;
                buffer = '';
            }                
	        String[] parts = new String[]{};
			parts = line.replace('\r', '').replace(NEWLINE, '').replace('\f', '').replace('"', '').split(',');
        	if(parts.size() < lastFieldToParse)
        	{
        		parts.add('');
        	}
        	for(String s : parts)
        	{
        		//s = s.unescapeCsv().removeStart('"').removeEnd('"');
        		s = s.replace('"', '');
        	}            
			return parts;
    	}
    	catch(Exception ex)
    	{
    		return null;
    	}
    }

    private void initialise(String fileContent, Boolean ignoreFirstLine)
    {
    	try
    	{
        	buffer = fileContent;
            if(ignoreFirstLine)
        {
        	ReadLine();
        }
    	}
    	catch(Exception ex)
    	{
    		buffer = 'String too large';
    	}
    }
}