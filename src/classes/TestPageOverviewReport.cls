@isTest
private class TestPageOverviewReport {
	static testMethod void testController()
    {

    	Test.StartTest();

    	Milestone1_Project__c project = new Milestone1_Project__c(Name='test');
    	insert project;

    	Milestone1_Milestone__c milestone = new Milestone1_Milestone__c(Name='milestone',Project__c=project.id,Team__c='Governance, Systems & Review');
    	insert milestone;

    	PageOverviewReportController controller = new PageOverviewReportController(new ApexPages.StandardController(project));
    	system.debug(controller.UpcomingTaskCompletion);
        system.debug(controller.Teams); 
        system.debug(controller.TasksComplete);                
        system.debug(controller.TimelineData);    

    	Test.StopTest();
    }
}