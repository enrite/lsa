public class RCRServiceOrderCloneController extends A2HCPageBase
{
	/*public RCRServiceOrderCloneController()
	{
		SearchDone = false;
	}
    private final Integer BATCHSIZE = 5;

	public Boolean SearchDone
	{
		get;
		private set;
	}
	
    public string SelectedAccount
    {
        get;
        set;
    }

    public string ClientName
    {
        get;
        set;
    }
    
    public string CCMSID
    {
        get;
        set;
    }
    
    public string ServiceOrderNumber
    {
        get;
        set;
    }

    public Date StartDate
    {
        get;
        set;
    }

    public Date EndDate
    {
        get;
        set;
    }
	
	@TestVisible
    private date NewEndDate 
    {
    	get
    	{
    		if(NewEndDate == null)
    		{
    			NewEndDate = A2HCUtilities.getEndOfFinancialYear(getDefaultDate().addYears(1));
    		}
    		return NewEndDate;
    	}
    	set;
    }   
    
    public String DefaultEndDateString   
    {
    	get
    	{
    		return A2HCUtilities.getEndOfFinancialYear(getDefaultDate()).format();
    	}
    } 
    
    public String NewEndDateString   
    {
    	get
    	{
    		return NewEndDate.format();
    	}
    	set
    	{
    		NewEndDate = A2HCUtilities.tryParseDate(value);
    		NewEndDateString = value;
    	}
    } 

    public List<ServiceOrderWrapper> ServiceOrders
    {
        get
        {
            if(ServiceOrders == null)
            {
                ServiceOrders = new List<ServiceOrderWrapper>();
            }
            return ServiceOrders;
        }
        private set;
    }

    public List<SelectOption> Accounts
    {
        get
        {
            if(Accounts == null)
            {
				Accounts = getSelectListFromObjectList([SELECT  ID,
					                                            Name
					                                    FROM    Account
					                                    WHERE   ID IN (SELECT  	Account__c
					                                    				FROM    RCR_Contract__c r
							                                           WHERE    (
							                                           				r.RecordType.Name = :APSRecordTypes.RCRServiceOrder_ServiceOrder OR
							                                           				(r.RecordType.Name = :APSRecordTypes.RCRServiceOrder_ServiceOrderCBMS AND
							                                           				RCR_CBMS_Contract__r.Contract_Type__c = :APS_PicklistValues.RCRContract_FundingCommitment_Recurrent)) AND
							                                           			r.Destination_Record__c = null AND
                                           										Rollover_Status__c = null)                           										
					                                    ORDER BY Name], false);
                Accounts.add(0, new SelectOption('', '--All--'));
            }
            return Accounts;
        }
        set;
    }

    public void doSearch()
    {
    	try
    	{	        
	        if(StartDate == null ||
	        		EndDate == null)
    		{
    			A2HCException.formatException('End Date range is required.');
    			return;
    		}
	        if(String.isBlank(CCMSID) &&
	        		String.isBlank(ClientName) &&
	        		String.isBlank(SelectedAccount) &&
	        		String.isBlank(ServiceORderNumber))
    		{
    			A2HCException.formatException('At least one of CCMS ID, Client/Group Name, Service Order or Service Provider must be entered.');
    			return;
    		}
    		ServiceOrders = null;
	        for(RCR_Contract__c contract : [SELECT  Id,
                                                    Name,
                                                    Start_Date__c,
                                                    End_Date__c,
                                                    Original_End_Date__c,
                                                    Total_Service_Order_Cost__c,
                                                    Group_Program__c,
                                                    Cancellation_Date__c,
                                                    Account__c,
                                                    Account__r.Name,
                                                    Client__r.Name,
                                                    Client__r.CCMS_ID__c,
                                                    RCR_CBMS_Contract__c,
                                                    RCR_CBMS_Contract__r.Name,
                                                    RCR_CBMS_Contract__r.Contract_Total__c,
                                                    RecordType.Name,
                                                    (SELECT	ID
                                                    FROM 	RCR_Contract_Scheduled_Services__r
                                                    WHERE	Funding_Commitment__c = :APS_PicklistValues.RCRContract_FundingCommitment_Recurrent OR
                                                    		RCR_Contract__r.RCR_CBMS_Contract__r.Contract_Type__c = :APS_PicklistValues.RCRContract_FundingCommitment_Recurrent)
                                           FROM     RCR_Contract__c 
                                           WHERE    (		                                            
		                                            	RecordType.Name = :APSRecordTypes.RCRServiceOrder_ServiceOrder 			                                            
	                                            		OR
			                                            (RecordType.Name = :APSRecordTypes.RCRServiceOrder_ServiceOrderCBMS AND
			                                            RCR_CBMS_Contract__r.Contract_Type__c = :APS_PicklistValues.RCRContract_FundingCommitment_Recurrent)
		                                            ) AND
                                            		Destination_Record__c = null AND
                                           			Rollover_Status__c = null AND   
                                           			Cancellation_Date__c = null AND
                                           			Requested_Cancellation_Date__c = null AND                                    
                                           			(Account__c = :SelectedAccount OR 
                                                 	Name LIKE :(String.isBlank(SelectedAccount) ? '%' : '~')) AND
                                                 	(Client__r.Name LIKE :('%' + ClientName + '%') OR 
                                                 	Name LIKE :(String.isBlank(ClientName) ? '%' : '~')) AND
                                                 	(Client__r.CCMS_ID__c = :CCMSID OR 
                                                 	Name LIKE :(String.isBlank(CCMSID) ? '%' : '~')) AND
                                                 	(Name = :ServiceOrderNumber OR 
                                                 	Name LIKE :(String.isBlank(ServiceOrderNumber) ? '%' : '~')) AND
                                           			End_Date__c >= :StartDate AND 
                                           			End_Date__c <= :EndDate 
                                           ORDER BY Client__r.Family_Name__c NULLS FIRST, 
                                           			Client__r.Given_Name__c, 
                                           			Group_Program__c, 
                                           			Name
                                           LIMIT :(BATCHSIZE + 1)])
            {
            	if((contract.RecordType.Name == APSRecordTypes.RCRServiceOrder_ServiceOrder &&
            			 contract.RCR_Contract_Scheduled_Services__r.size() > 0)||
            		(contract.RecordType.Name == APSRecordTypes.RCRServiceOrder_ServiceOrderCBMS &&	
                		contract.Total_Service_Order_Cost__c <= contract.RCR_CBMS_Contract__r.Contract_Total__c))
               	{
                	ServiceOrders.add(new ServiceOrderWrapper(contract));
               	}
            }

            if(ServiceOrders.size() > BATCHSIZE)
            {
            	while(ServiceOrders.size() > BATCHSIZE)
            	{
            		ServiceOrders.remove(ServiceOrders.size() - 1);
            	}            	
            	A2HCException.formatWarning('Your search returned more than the limit of ' + 
            			BATCHSIZE.format() + 
            			' records. You will need to run the search again to process the remainder of the Service Orders.');
            }
            SearchDone = true;
    	}
    	catch(Exception ex)
    	{
    		A2HCException.formatException(ex.getMessage());
    	}
    }


    public void rolloverServiceOrders()
    {
        try
        {
        	if(NewEndDate == null)
        	{
        		A2HCException.formatException('New End Date is required.');
        		return;
        	}
        	if(NewEndDate < EndDate)
        	{
        		A2HCException.formatException('New End Date must ne later than the End Date range.');
        		return;
        	}
        	
			List<RCR_Contract__c> serviceOrdersToUpdate = new List<RCR_Contract__c>(); 
            for(ServiceOrderWrapper w : ServiceOrders)
            {
                // Identify Service Orders to be rolled over.
                if(w.Selected)
                {      	
                    serviceOrdersToUpdate.add(w.ServiceOrder);
                }
            }            
            if(serviceOrdersToUpdate.size() == 0)
            {
            	A2HCException.formatMessage('No Service Orders were selected to roll over.');
            	return;
            }
            RCRServiceOrderCloneSchedule schedClass = new RCRServiceOrderCloneSchedule(NewEndDate, RCRServiceOrderCloneBase.RunMode.Rollover);  
	    	String scheduleId = BatchUtilities.getNextScheduleID(schedClass, RCRScheduledServiceOrderCloneBatch.class, 'Rollover_Scheduled');
	    	
            for(RCR_Contract__c so : serviceOrdersToUpdate)
            {
                so.Schedule__c = scheduleId;
                so.Rollover_Status__c = APS_PicklistValues.RCRContract_RolloverStatus_Queued;
            }
			update serviceOrdersToUpdate;
			doSearch();
            A2HCException.formatMessage(serviceOrdersToUpdate.size().format() + ' Service Orders have been submitted for processing. You will be notified by email when processing has been completed.');
        }
        catch(Exception ex)
        {
            A2HCException.formatException(ex);
        }
    }

	private Date getDefaultDate()
	{
		Date defaultDate = Date.today();
		if(defaultDate.month() > 6)
		{
			return defaultDate.addYears(-1);
		}
		return defaultDate;
	}

    public class ServiceOrderWrapper
    {
        public ServiceOrderWrapper(RCR_Contract__c pServiceOrder)
        {
            Selected = false;
            ServiceOrder = pServiceOrder;
        }

        public boolean Selected
        {
            get;
            set;
        }

        public RCR_Contract__c ServiceOrder
        {
            get;
            private set;
        }
    }*/
	
	
}