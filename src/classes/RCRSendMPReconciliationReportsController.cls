public class RCRSendMPReconciliationReportsController
{
    public PageReference Send()
    {
        String batchId = ApexPages.CurrentPage().getParameters().get('batchId');
        
        if (batchId != null)
        {
            EmailManager em = new EmailManager();
            em.sendMasterpieceReconcilliationReports(new RCR_Invoice_Batch__c(Id = batchId));
        }
        
        return new PageReference('/' + batchId);
    }
}