public with sharing class FormDetailViewRedirectExtension
{
    public FormDetailViewRedirectExtension(ApexPages.StandardController c)
    {
        record = (Form_Detail__c)c.getRecord();
    }

    private Form_Detail__c record;
    
    public PageReference Redir()
    {
        try
        {    
            // if this is an existing record then check if the current user has read access
            if (record.Id != null)
            {
                Boolean hasReadAccess = false;
                
                // query for user edit access
                for (UserRecordAccess ura : [SELECT RecordId
                                             FROM UserRecordAccess
                                             WHERE UserId = :UserInfo.getUserId()
                                             AND RecordId = :record.Id 
                                             AND HasReadAccess = true])
                {                                             
                    hasReadAccess = true;
                }
                
                // if no edit access then throw an exception
                if (!hasReadAccess)
                {
                    return A2HCException.formatException('You do not have permission to view this record');
                }
            }
                                                                                                 
            Map<String, PageReference> pages = new Map<String, PageReference>();
            pages.put('Lifetime_Assessment_Start', Page.LifetimeAssessmentView);      
            pages.put('Lifetime_Assessment_CE_Approval', Page.LifetimeAssessmentView);
            pages.put('Lifetime_Assessment_Complete', Page.LifetimeAssessmentView);
            pages.put('Service_Planner_Application_Assessment', Page.ApplicationAssessmentView);            
            pages.put('Manager_ASD_Application_Assessment', Page.ApplicationAssessmentView);            
            pages.put('Chief_Executive_Application_Assessment', Page.ApplicationAssessmentView);  
            pages.put('Assessment_Complete', Page.ApplicationAssessmentView);            
                    
            RecordType rt = [SELECT Name,
                                    DeveloperName
                             FROM RecordType
                             WHERE Id = :record.RecordTypeId
                             OR Id = :ApexPages.CurrentPage().getParameters().get('RecordType')];
                                    
            // find the page reference to redirect to
            PageReference pg;
            
            if (pages.containsKey(rt.DeveloperName))
            {                        
                pg = pages.get(rt.DeveloperName);
            }            
            else
            {                
                pg = new PageReference('/' + record.Id);                                
                pg.getParameters().put('nooverride', '1');
            }
            
            // add the parameters for this page to the new page reference
            for (String k : ApexPages.CurrentPage().getParameters().keySet())
            {
                if (k != 'save_new' && k != '_CONFIRMATIONTOKEN')
                {
                    pg.getParameters().put(k, ApexPages.CurrentPage().getParameters().get(k));
                }
            }
            
            return pg;
        }
        catch(Exception e)
        {
            return A2HCException.formatException(e);
        }
    }
}