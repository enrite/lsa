/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest//(SeeAllData=true)
public class TestA2HCPageBase {

    static testMethod void myUnitTest()
    {
        TestClass tc = new TestClass();

        system.debug(tc.CurrentUser);
        system.debug(tc.YesNoList);
        system.debug(tc.YesNoNoneList);
        system.debug(tc.YesNoUndefinedList);
        system.debug(tc.StateList);
        system.debug(tc.RandomString);
        Application_Help_Item__c ahi = new Application_Help_Item__c(
                                        Name = 'test',
                                        Help_Heading__c = 'test',
                                        Help_Text__c = 'test',
                                        Application__c = 'RCR');
        insert ahi;
        system.debug(tc.RCRHelpItems);
        system.debug(tc.ShowPageDebugging);
        system.debug(tc.ShowReportURL);
        system.debug(tc.isAdminProfile);
        system.debug(tc.InPortal);

        system.assert(tc.CurrentDate != null);
        system.assert(tc.CurrentDateTime != null);
        system.assert(tc.CurrentDateTimeSeconds != null);
        
        system.assert(tc.Is_IE != null);
        system.assert(tc.ShowPageDebugging != null);
        system.assert(tc.ShowReportURL != null);
        
        Test.setCurrentPage(Page.RCR);
        ApexPages.currentPage().getHeaders().put('Host', 'test');
        system.assert(tc.SalesforceURL != null);

        tc.dummy();
/*
        system.debug(A2HCPageBase.getAnyA2HCOfficer());

        Profile p = [SELECT ID
                    FROM    Profile
                    WHERE   Name = 'A2HC Officer'];
        User u;
        for(User usr : [SELECT  ID,
                                firstName,
                                lastName,
                                userName
                        FROM    User
                        WHERE   username='fred@dd.com'])
        {
            u = usr;
        }
        if(u == null)
        {
            u = new User(alias = 'fred',
                        email='fred@dd.com',
                        emailencodingkey='UTF-8',
                        lastname='fred@dd.com',
                        languagelocalekey='en_US',
                        localesidkey='en_AU',
                        profileid = p.Id,
                        timezonesidkey='Australia/Adelaide',
                        username='fred@dd.com',
                        A2HC_Site__c = 'Angaston',
                        CommunityNickname = 'fred@dd.com');

            insert u;
        }
        system.debug(A2HCPageBase.getA2HCOfficer(u.username, u.firstName, u.lastName));
        
        User a2hcUser = [SELECT ID,
                                Username
                        FROM    User
                        WHERE   Profile.Name = 'A2HC Officer' AND
                                IsActive = true
                        LIMIT 1];
        String nullString = null;                       
        A2HCPageBase.getA2HCOfficer(a2hcUser.Username, nullString, nullString);
        
        User rcrUser = [SELECT ID
                        FROM    User
                        WHERE   Profile.Name = 'RCR DCSI User' AND
                                IsActive = true
                        LIMIT 1];
        System.runAs(rcrUser)
        {
            tc = new TestClass();
            system.assert(tc.accessablePages != null);
            tc.throwTestError();
            
            A2HCPageBase.submitForApproval(null, true);
            List<ID> serviceOrderIds = new List<ID>();
            List<RCR_Contract__c> serviceOrders = new List<RCR_Contract__c>();
            for(RCR_Contract__c serviceOrder : [SELECT ID
                                                FROM    RCR_Contract__c
                                                WHERE   Name = 'TestSO'])
            {
                serviceOrderIds.add(serviceOrder.ID);
                serviceOrders.add(serviceOrder);
            }
            
            A2HCPageBase.submitForApproval(serviceOrders, true);
            
            A2HCPageBase.submitForApproval(serviceOrderIds, 'test', true);
            
            A2HCPageBase.submitForApprovalFuture(serviceOrderIds, 'test', true);
            
            
        }   */                    
    }
    
    static testMethod void test2()
    {
        TestClass tc = new TestClass();
        //tc.testProtectedMethods();        
    }
    
    static testMethod void test3()
    {
        TestClass tc = new TestClass();
        tc.testProtectedMethods2();     
    }

    public class TestClass extends A2HCPageBase
    {
        public TestClass()
        {
        }
/*
        public void testProtectedMethods()
        {
            Needs_Identification__c needsId = [SELECT ID,
                                                        Client__c
                                                FROM    Needs_Identification__c
                                                WHERE Client__c != null
                                                LIMIT 1];
                                                                                
            getNewSelectListWithOptgroup('test');
            getSelectListFromPicklist(Account.Type.getDescribe());
            getSelectListFromPicklist(Account.Type.getDescribe(), 'Test');
            getSelectListFromPicklist(Account.Type.getDescribe(), true, 'Other');
            getReturnPage(needsId.ID, needsId.Client__c);

            Test.setCurrentPage(Page.Needs_Identification);
            ApexPages.currentPage().getParameters().put('retURL', needsId.ID);           
            getReturnPage(needsId.ID, needsId.Client__c);
            getReturnPageFromURL();
            getParentIDFromURL();
            addReturnURL(new PageReference('/'), '/', 'test');
            isUserInGroup('test', '00590000000dn8r');
            isUserInRole('test', '00590000000dn8r', true);
            isNumericFieldValid(2, 'test', 1, 3, true);
            isNumericFieldValid(2, 'test', 3, 4, true);
            isNumericFieldValid(2, 'test', null, 1, true);
            isNumericFieldValid(2, 'test', 3, null, true);
            isNumericFieldValid('a', 'test', 1, 3, true);
            getGroup('test');
            
            Needs_Identification__c ni = [SELECT ID
                                        FROM    Needs_Identification__c
                                        LIMIT 1];
            ApexPages.currentPage().getParameters().put('retURL', '/apex/test?id=' + ni.ID);           
            getParentIDFromURL();

            Attachment att = new Attachment();
            att.Body = Blob.valueOf('ssaasdas');
            att.ParentId = needsId.ID;
            att.Name = 'test';
            insert att;

            deleteAttachments(needsId.ID, null);

            Client_Health_Condition__c chc = [SELECT    ID
                                                FROM    Client_Health_Condition__c
                                                WHERE   Needs_Identification__c != null
                                                LIMIT 1];
            deleteChildRecord(chc.ID);
            deleteChildRecord(null);

            getDefaultRecordTypeId(Contact.sObjectType.getDescribe());

            validateRichText('asdasdasda');
        }*/
        
        public void testProtectedMethods2()
        {    
            TestLoadData.loadRCRData();
            
            Test.StartTest();
        
            system.assert(getSelectListFromPicklist(Account.Type.getDescribe()) != null);
            system.assert(getSelectListFromPicklist(Account.Type.getDescribe(), true) != null);
            system.assert(getSelectListFromPicklist(Account.Type.getDescribe(), 'test') != null);
            system.assert(getSelectListFromPicklist(Account.Type.getDescribe(), false, 'Customer') != null);
            
            List<sObject> objects = [SELECT ID, Name FROM Account LIMIT 5];
            system.assert(getSelectListFromObjectList(objects, true) != null);
            system.assert(getSelectListFromObjectList(objects, 'ID', 'Name', true) != null);
            
            RCR_Contract__c serviceOrder = [SELECT ID
                                                FROM    RCR_Contract__c
                                                /*WHERE   Name = 'TestSO'*/
                                                LIMIT 1];
            serviceOrder = RCRContractExtension.getContract(serviceOrder.ID);                                               
            submitToApprovalProcess(serviceOrder.ID); 
            
            Map<String, Schema.FieldSet> soFieldsets = Schema.SObjectType.RCR_Contract__c.fieldSets.getMap();
            Integer i = 0; 
            for(Schema.FieldSet fs : soFieldsets.values())
            {
                isFieldSetCompleted(serviceOrder, fs, i == 0, i == 1, i == 2);
                i++;
            }
          
            Group grp = [SELECT DeveloperName
                        FROM Group 
                        LIMIT 1];
                        
            getGroup(grp.DeveloperName);
            isUserInGroup(grp.DeveloperName, UserInfo.getUserID());
            
            PermissionSet ps = [SELECT Name
                                FROM    PermissionSet
                                LIMIT 1];
            isUserInPermissionSet(ps.Name);  
            
            UserRole r = [SELECT    Name
                            FROM    UserRole
                            LIMIT 1];
            isUserInRole(r.Name, UserInfo.getUserID(), true);                       

            Test.StopTest();
        }
    }
}