@isTest
public class TestExternalExpenseEditExtension
{                   
    static testMethod void myTestMethod()
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
        
        Account acc = [SELECT Id,
                              Masterpiece_Payment_Required__c
                       FROM Account 
                       LIMIT 1];
        acc.Masterpiece_Payment_Required__c = false;
        update acc;                                              
                      
        External_Expense__c e = new External_Expense__c();
        e.Account__c = acc.Id;
        e.Month__c = Datetime.newInstance(Date.today().year(), Date.today().month(), 1, 18, 0, 0).format('MMMM');
        e.Year__c = String.valueOf(Date.today().year());        
        
        ExternalExpenseEditExtension ext = new ExternalExpenseEditExtension(new ApexPages.StandardController(e));
        ext.ItemKey = ext.Keys[0];
        ext.RemoveLineItem();
        system.debug(ext.Years);
        system.debug(ext.Totals);
                                
        External_Expense_Line_Item__c i = ext.LineItems.get(ext.Keys[0]);                                     
        i.Participant__c = [SELECT Id FROM Referred_Client__c LIMIT 1].Id;
        
        ext.SaveOverride();        
        
        i.Date_of_Transaction__c = Date.today();
        i.Expense_Description__c = 'test';
        i.GL_Code__c = [SELECT Id FROM RCR_Program_Category_Service_Type__c LIMIT 1].Id;
        i.Amount__c = 10;
        i.GST__c = 1;        
                       
        ext.SaveOverride();
        
        ext = new ExternalExpenseEditExtension(new ApexPages.StandardController(e));
        
        // test the triggers
        e.Approved__c = true;
        update e;
        
        try
        {
            External_Expense__c e2 = new External_Expense__c();
            e2.Account__c = acc.Id;
            e2.Month__c = Datetime.newInstance(Date.today().year(), Date.today().month(), 1, 18, 0, 0).format('MMMM');
            e2.Year__c = String.valueOf(Date.today().year());        
            insert e2;
        }
        catch(Exception ex)
        { }
        
        Test.StopTest();
    }
}