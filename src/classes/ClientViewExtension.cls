public with sharing class ClientViewExtension extends A2HCPageBase
{
    public ClientViewExtension(ApexPages.StandardController c)
    {
        record = (Referred_Client__c)c.getRecord();
        
        RefreshRows();
        GetCurrentPlans();
    }

    private Referred_Client__c record;
    private transient Map<String, List<Standard_Letter__c>> tClientDocumentsByCategory;
    private transient List<SelectOption> tDocumentStatusList;
    public transient List<Row> Rows { get; set; }
    public transient List<Participant_Contact__c> tDecisionMakingContacts;
    public transient List<Participant_Contact__c> tEmergencyContacts;
    public transient List<Participant_Contact__c> tAllContacts;
    public Integer RowsSize
    {
        get
        {
            if(RowsSize == null)
            {
                return 0;
            }
            return RowsSize;
        }
        set;
    }
    public Integer PlanCount { get; set; }
    public String PlanCountTitle  { get; set; }

    private void RefreshRows()
    {
        if (ApexPages.CurrentPage().getURL().contains('ClientView'))
        {
            FindRows(10);
        }
        else if (ApexPages.CurrentPage().getURL().contains('ClientCaseFile'))
        {
            FindRows(0);
        }    
    }

    private void GetCurrentPlans()
    {
        //List<Plan__c> Plans = [SELECT Id FROM Plan__c WHERE Client__c = :record.ID AND (Plan_Status__c = :APS_PicklistValues.ParticipantPlan_PlanStatus_Approved OR Plan_Status__c = :APS_PicklistValues.ParticipantPlan_PlanStatus_Amendment) AND Plan_End_Date__c >= TODAY];
        List<Plan__c> Plans = [SELECT Id, Plan_Status__c FROM Plan__c WHERE Client__c = :record.ID AND Plan_End_Date__c >= TODAY];
        PlanCount = 0;
        PlanCountTitle = 'No plan for participant';
        If(!Plans.isEmpty())
        {
            for(Plan__c p : Plans)
            {
                if(p.Plan_Status__c == APS_PicklistValues.ParticipantPlan_PlanStatus_Approved || p.Plan_Status__c == APS_PicklistValues.ParticipantPlan_PlanStatus_Amendment)
                {
                    PlanCount = 1; //Plans.size();
                    PlanCountTitle = 'Has a current approved plan or plan in amendment';
                    break;
                }
            }

        }

    }

    private List<Participant_Contact__c> AllContacts
    {
        get
        {
            if(tAllContacts == null)
            {
                tAllContacts = [SELECT ID,
                                        Name,
                                        Full_Name__c,
                                        Relationship_to_Participant__c,
                                        Preferred_Phone__c,
                                        Email__c,
                                        In_Case_Of_Emergency__c,
                                        Decision_Making_Contact__c,
                                        Do_not_contact__c
                                FROM    Participant_Contact__c
                                WHERE    Participant__c = :record.ID];
            }
            return tAllContacts;
        }
    }

    public List<Participant_Contact__c> DecisionMakingContacts
    {
        get
        {
            if(tDecisionMakingContacts == null)
            {
                tDecisionMakingContacts = new List<Participant_Contact__c>();
                for(Participant_Contact__c pc : AllContacts)
                {
                    if(pc.Decision_Making_Contact__c)
                    {
                        tDecisionMakingContacts.add(pc);
                    }
                }
            }
            return tDecisionMakingContacts;
        }
    }

    public List<Participant_Contact__c> EmergencyContacts
    {
        get
        {
            if(tEmergencyContacts == null)
            {
                tEmergencyContacts = new List<Participant_Contact__c>();
                for(Participant_Contact__c pc : AllContacts)
                {
                    if(pc.In_Case_Of_Emergency__c)
                    {
                        tEmergencyContacts.add(pc);
                    }
                }
            }
            return tEmergencyContacts;
        }
    }

    public Boolean FromFormDetail
    {
        get
        {
            return FormDetail.ID != null;
        }
    }

    private Form_Detail__c FormDetail
    {
        get
        {
            if(FormDetail == null)
            {
                FormDetail = new Form_Detail__c();
                for(Form_Detail__c fd : [SELECT ID,
                                                Letter_Recipient__c,
                                                Assessment_Type__c
                                        FROM Form_Detail__c
                                        WHERE ID = :getParameter('fdid')])
                {
                    FormDetail = fd;
                }
            }
            return FormDetail;
        }
        set;
    }

    public Map<String, List<Standard_Letter__c>> ClientDocumentsByCategory
    {
        get
        {
            if(tClientDocumentsByCategory == null)
            {
                tClientDocumentsByCategory = new Map<String, List<Standard_Letter__c>>();           
                for(Schema.Picklistentry ple : Standard_Letter__c.Category__c.getDescribe().getPicklistValues())
                {
                    tClientDocumentsByCategory.put(ple.getValue(), new List<Standard_Letter__c>());
                }
                for(Standard_Letter__c cd : [SELECT ID,
                                                    Name,
                                                    Description__c,
                                                    Base_URL__c,
                                                    Category__c,
                                                    Assessment_Type__c,
                                                    Recipient__c,
                                                    Participant_Current_Status__c
                                            FROM    Standard_Letter__c 
                                            WHERE   Participant_Current_Status__c INCLUDES (:record.Current_Status__c)])
                {
                    if(FormDetail.ID == null)
                    {
                        tClientDocumentsByCategory.get(cd.Category__c).add(cd);
                    }
                    else
                    {
                        if(cd.Assessment_Type__c == FormDetail.Assessment_Type__c &&
                            cd.Recipient__c == FormDetail.Letter_Recipient__c)
                        {
                            tClientDocumentsByCategory.get(cd.Category__c).add(cd);
                        }
                    }
                }
            }
            return tClientDocumentsByCategory;
        }
    }

    public void FindRows(Integer rowLimit)
    {      
        try
        {              
            Map<Id, Row> rowMap = new Map<Id, Row>();
            
            // find the case notes
            for (Case_Notes__c cn : [SELECT Id,
                                            Name,
                                            Note_Date__c,
                                            Category__c,
                                            Description__c,
                                            Resulting_From__c,
                                            CreatedBy.Name,
                                            LastModifiedDate,
                                            LastModifiedBy.Name,
                                            (SELECT ID 
                                            FROM Attachments)
                                     FROM Case_Notes__c
                                     WHERE Client__c = :record.Id])
            {
                rowMap.put(cn.Id, new Row(cn));                
            }  
            
            // find the emails
            for (Notification__c e : [SELECT Id,
                                             Name,
                                             Date_Received__c,
                                             Email_Date__c,
                                             Category__c,
                                             Description__c,
                                             CreatedBy.Name,
                                            LastModifiedDate,
                                            LastModifiedBy.Name
                                      FROM Notification__c 
                                      WHERE Client__c = :record.Id])
            {
                rowMap.put(e.Id, new Row(e));                
            }   
            
            // find the attachments
            for (Attachment a : [SELECT Id,
                                        Name,
                                        CreatedDate,                                            
                                        CreatedBy.Name,
                                        LastModifiedDate,
                                        LastModifiedBy.Name
                                 FROM Attachment 
                                 WHERE ParentId = :record.Id])
            {
                rowMap.put(a.Id, new Row(a));            
            } 
            
            /*
            Issue with more then 200 records
            for (UserRecordAccess ura : [SELECT RecordId,
                                                HasEditAccess,
                                                HasDeleteAccess,
                                                MaxAccessLevel
                                         FROM UserRecordAccess
                                         WHERE UserId = :UserInfo.getUserId()
                                         AND RecordId IN :rowMap.keySet()])
            { 
                rowMap.get(ura.RecordId).Actions.put('Edit', ura.HasEditAccess || ura.MaxAccessLevel == 'All');
                rowMap.get(ura.RecordId).Actions.put('Del', ura.HasDeleteAccess || ura.MaxAccessLevel == 'All');
            }        
            */                                     
        
            for(Id recordId : rowMap.keySet())
            {
                //PM 14 May: Set to true SF permissions will stop the user performing the action if they shouldn't.
                rowMap.get(recordId).Actions.put('Edit', true);
                rowMap.get(recordId).Actions.put('Del',true);  
            }
            
            Rows = rowMap.values();
            Rows.sort();
            RowsSize = Rows.size();
            // reduce the list if a limit has been passed
            if (rowLimit > 0)
            {
                while (Rows.size() > rowLimit)
                {
                    Rows.remove(Rows.size() - 1);
                }   
            }
        }
        catch(Exception e)
        {
            A2HCException.formatException(e);
        }      
    }
    
    public String DeleteId { get; set; }
    public void DeleteRecord()
    {
        try
        {
            delete [SELECT Id
                    FROM Case_Notes__c
                    WHERE Id = :DeleteId];
            
            delete [SELECT Id
                    FROM Notification__c
                    WHERE Id = :DeleteId];        
            
            delete [SELECT Id
                    FROM Attachment
                    WHERE Id = :DeleteId];   
                    
            RefreshRows();                                           
        }
        catch(Exception e)
        {
            A2HCException.formatException(e);        
        }
    }
    
    public class Row implements Comparable
    {
        public Row(Case_Notes__c cn)
        {
            CaseNote = cn;            
            Type = 'Case Note';
            RecordID = cn.Id;
            Name = cn.Name;
            RecordDate = cn.Note_Date__c;
            Category = cn.Category__c;
            Description = cn.Description__c;
            CreatedBy = cn.CreatedBy.Name;
            LastModifiedDate = cn.LastModifiedDate;
            LastModifiedUser = cn.LastModifiedBy.Name;
            ResultingFrom = cn.Resulting_From__c;
            if(cn.Attachments.size() > 0)
            {
                AttachmentCount = cn.Attachments.size();
            }
            DefaultActions();
        }
        
        public Row(Notification__c e)
        {
            Email = e;
            Type = 'Email';
            RecordID = e.Id;
            Name = e.Name;
            RecordDate = e.Email_Date__c;
            Category = e.Category__c;
            Description = e.Description__c;
            CreatedBy = e.CreatedBy.Name;
            LastModifiedDate = e.LastModifiedDate;
            LastModifiedUser = e.LastModifiedBy.Name;
            DefaultActions();
        }
        
        public Row(Attachment a)
        {
            Attachment = a;
            Type = 'Attachment';
            RecordID = a.Id;
            Name = 'Attachment';
            RecordDate = a.CreatedDate.Date();            
            Description = a.Name;
            CreatedBy = a.CreatedBy.Name;
            LastModifiedDate = a.LastModifiedDate;
            LastModifiedUser = a.LastModifiedBy.Name;
            DefaultActions();
        }
        
        private void DefaultActions()
        {
            Actions = new Map<String, Boolean>();
            Actions.put('Edit', false);
            Actions.put('Del', false);
        }
        
        public Case_Notes__c CaseNote { get; set; }
        public Notification__c Email { get; set; }
        public Attachment Attachment { get; set; }
        
        public String Type { get; set; }
        public Map<String, Boolean> Actions { get; set; }
        public String RecordID { get; set; }
        public String Name { get; set; }
        public Date RecordDate { get; set; }
        public String Category { get; set; }
        public String Description { get; set; }
        public String CreatedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public String LastModifiedUser { get; set; }
        public Integer AttachmentCount { get; set; }
        public String ResultingFrom { get; set; }
        
        // Compare opportunities based on the opportunity amount.
        public Integer compareTo(Object compareTo) 
        {
            Row compareToRow = (Row)compareTo;
            // The return value of 0 indicates that both elements are equal.
            if(RecordDate == compareToRow.RecordDate)
            {
                return Math.signum((compareToRow.LastModifiedDate.getTime() - LastModifiedDate.getTime()) * 1.0).intValue();
            }
            return RecordDate > compareToRow.RecordDate ? -1 : 1;
        }
    }
}