@isTest
private class TestRCRServiceOrderSubmitForCSABatch
{
    public static TestMethod void myTestMethod() 
    {   
        TestLoadData.loadRCRData();
        
        Test.StartTest();
        
        RCRServiceOrderCloneBase socb = new RCRServiceOrderCloneBase(Date.today());
                    
        RCRServiceOrderSubmitForCSABatch batch = new RCRServiceOrderSubmitForCSABatch(socb);           
        Database.executeBatch(batch, 10);
        
        Test.StopTest();
    }
}