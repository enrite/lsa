public without sharing class EmailManager// extends A2HCPageBase
{
    private static string EmailStyles
    {
        get
        {
            if(EmailStyles == null)
            {
                EmailStyles = '<style type="text/css">';
                EmailStyles += 'body {';
                EmailStyles += 'font-size:0.75em;';
                EmailStyles += 'font-family:verdana, helvetica, arial, sans-serif;';
                EmailStyles += '}';
                EmailStyles += '.hdg {';
                EmailStyles += 'margin-bottom:5px;';
                EmailStyles += '}';
                EmailStyles += '.hdg2 {';
                EmailStyles += 'margin-bottom:5px;font-weight:bold;';
                EmailStyles += '}';
                EmailStyles += 'div {';
                EmailStyles += ' margin-bottom:2px;';
                EmailStyles += '}';
                EmailStyles += '.left {';
                EmailStyles += 'text-align:left;';
                EmailStyles += '}';
                EmailStyles += '.right {';
                EmailStyles += 'text-align:right;';
                EmailStyles += '}';
                EmailStyles += '</style>';
            }
            return EmailStyles;
        }
        private set;
    }

    private final String EMAILFOOTER = 'Regards,' +
                                            '<p/><br/>' +
                                            'RCR Administration' +
                                            '<p/>' +
                                            'This is an automated message. Please do not reply to this message.' +
                                            '</div>';
    private final String EMAILGREETINGINTERNAL = '<div>' +
                                                    'Dear DCSI RCR user,' +
                                                    '<p/>';
    private final String EMAILGREETINGPORTAL = '<div>' +
                                                    'Dear  Service Provider,' +
                                                    '<p/>';

    public static void sendApprovalProcessComplete(String[] emailAddresses, String objectLabel, Integer recordCount, Integer approvedCount)
    {
        Messaging.Singleemailmessage msg = new Messaging.Singleemailmessage();
        msg.setToAddresses(emailAddresses);
        msg.setPlainTextBody(recordCount + ' ' + objectLabel + (recordCount > 1 ? '(s) were' : ' was') + ' submitted for approval and ' +
                                approvedCount + (approvedCount > 1 ? ' were' : ' was') +' automatically approved.');
        Messaging.sendEmail(new Messaging.Singleemailmessage[]{msg});
    }

    public static void sendAdminErrorMessage(String subject, Exception ex)
    {
        List<Messaging.SingleEmailMessage> msgs = new List<Messaging.SingleEmailMessage>();
        for(User u :
        [
                SELECT ID
                FROM User
                WHERE IsActive = true AND
                ID IN (SELECT UserOrGroupId
                        FROM GroupMember
                        WHERE Group.Name = 'SysAdmin')])
        {
            Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
            msg.setSubject(subject + ' ' + Datetime.now().format('dd/MM/yyyy HH:mm:ss'));
            msg.setPlainTextBody(ex.getMessage() + '\n\n' + ex.getStackTraceString());
            msg.setSaveAsActivity(false);
            msg.setTargetObjectId(u.ID);
            msg.setTreatTargetObjectAsRecipient(true);
            msgs.add(msg);
        }
        Messaging.sendEmail(msgs);
    }

    public static void sendHTMLMessage(String emailAddress, String subject, String body, String senderDisplayName, String replyTo)
    {
        sendMessage(new String[]{emailAddress}, null, subject, body, senderDisplayName, replyTo, true);
    }

    public static void sendHTMLMessage(String[] emailAddresses, String subject, String body, String senderDisplayName, String replyTo)
    {
        sendMessage(emailAddresses, null, subject, body, senderDisplayName, replyTo, true);
    }

    public static void sendHTMLMessage(ID targetObjectId, String subject, String body, String senderDisplayName, String replyTo)
    {
        sendMessage(null, targetObjectId, subject, body, senderDisplayName, replyTo, true);
    }

    public static void sendPlainTextMessage(ID targetObjectId, String subject, String body, String senderDisplayName, String replyTo)
    {
        sendMessage(null, targetObjectId, subject, body, senderDisplayName, replyTo, false);
    }

    public static void sendPlainTextMessage(String[] emailAddresses, String subject, String body, String senderDisplayName, String replyTo)
    {
        sendMessage(emailAddresses, null, subject, body, senderDisplayName, replyTo, false);
    }

    private static void sendMessage(String[] emailAddresses,
                                                    ID targetObjectId,
                                                    String subject,
                                                    String body,
                                                    String senderDisplayName,
                                                    String replyTo,
                                                    Boolean isHTML)
    {
        Messaging.Singleemailmessage message = new Messaging.Singleemailmessage();
        if(targetObjectId == null)
        {
            message.setToAddresses(emailAddresses);
        }
        else
        {
            message.setTargetObjectId(targetObjectId);
        }
        if(isHTML)
        {
            message.setHtmlBody(body);
        }
        else
        {
            message.setPlainTextBody(body);
        }
        message.setSaveAsActivity(false);
        message.setSenderDisplayName(senderDisplayName);
        message.setReplyTo(replyTo);
        message.setSubject(subject);
        Messaging.sendEmail(new Messaging.Singleemailmessage[]{message});
    }

    public void sendTemplateMessage(String templateName, sObject record, ID userID, String[] toAddresses)
    {
        addMessage(getTemplate(templateName), userID, record.ID, toAddresses, true);
    }

    public void sendUpdatedServiceQueryEmails(List<Service_Query__c> queries, Map<ID, User> internalUsersByID)
    {
        for(Service_Query__c sq : queries)
        {
            String[] toAddress = new String[]{};
            if(internalUsersByID.containsKey(sq.Assigned_To__c))
            {
                toAddress.add(internalUsersByID.get(sq.Assigned_To__c).Email);
            }
            addMessage(getTemplate('Service_Query_Updated'), sq.CreatedByID, sq.ID, toAddress, false);
        }
        sendMessages();
    }

    public void sendInternalServiceQueryEmails(List<Service_Query__c> queries)
    {
        for(Service_Query__c sq : queries)
        {
            addMessage(getTemplate('Service_Query_Updated_Internal'), sq.Assigned_To__c, sq.ID, null, false);
        }
        sendMessages();
    }

    public void sendParticipationPlanManualAcceptance(RCR_Contract__c serviceOrder)
    {
        addMessage(getTemplate('Manual_Acceptance_of_Participation_Plan'), UserInfo.getUserId(), serviceOrder.ID, null);
    }

    public void sendCSAAcceptanceNotifications(List<User> portalUsers, RCR_Contract__c serviceOrder)
    {
        sendCSAAcceptanceNotifications(portalUsers, serviceOrder, true);
    }

    public void sendCSAAcceptanceNotifications(List<User> portalUsers, RCR_Contract__c serviceOrder, Boolean sendImmediate)
    {
        string targetUserID;
        List<string> userEmailAddresses = new List<string>();
        if(portalUsers != null && portalUsers.size() > 0)
        {
            integer cnt = 0;
            for(User u : portalUsers)
            {
                if(cnt == 0)
                {
                    targetUserID = u.Id;
                }
                else
                {
                    userEmailAddresses.add(u.Email);
                }
                cnt++;
            }

            string msgBody = EmailStyles + EMAILGREETINGPORTAL;
            msgBody += 'Service Order ' + serviceOrder.Name + ' for ' + getSOClientName(serviceOrder);
            msgBody += ' is now available via the Service Orders Pending Acceptance section on your Home page via the RCR Portal:';
            msgBody += '<p/>';
            msgBody += '<a id="theLink" name="theLink" href="' + BaseURL__c.getInstance().Service_Provider_Portal__c + serviceOrder.Id + '">' +
                                    BaseURL__c.getInstance().Service_Provider_Portal__c + serviceOrder.Id + '</a>';
            msgBody += '<p/>';
            msgBody += 'Can you please connect to the RCR Portal to check whether all the terms and conditions are agreeable? ';
            msgBody += 'If the Client Service Agreement is acceptable then please press the Accept CSA button to electronically confirm your acceptance, ';
            msgBody += 'otherwise please submit an RCR Service Query raising any concerns.';
            msgBody += '<p/>';
            msgBody += EMAILFOOTER;

            addMessage(targetUserID, null, userEmailAddresses, 'New Customer Service Agreement Requiring Acceptance', msgBody, sendImmediate);
        }
    }

    public void sendCSAAcceptanceNotifications(List<User> portalUsers, List<RCR_Contract__c> serviceOrders)
    {
        string targetUserID;
        List<string> userEmailAddresses = new List<string>();
        if(portalUsers != null && portalUsers.size() > 0)
        {
            integer cnt = 0;
            for(User u : portalUsers)
            {
                if(cnt == 0)
                {
                    targetUserID = u.Id;
                }
                else
                {
                    userEmailAddresses.add(u.Email);
                }
                cnt++;
            }

            string msgBody = EmailStyles + EMAILGREETINGPORTAL;
            msgBody += 'The following Service Orders are now available via the Service Orders Pending Acceptance section on your Home page via the RCR Portal:';
            msgBody += '<p/>';

            for(RCR_Contract__c c : serviceOrders)
            {
                msgBody += c.Name + ' for ' + getSOClientName(c) + '<br/>';
            }

            msgBody += '<p/>';
            msgBody += '<a id="theLink" name="theLink" href="' + BaseURL__c.getInstance().Service_Provider_Portal__c + '">' +
                                    BaseURL__c.getInstance().Service_Provider_Portal__c + '</a>';
            msgBody += '<p/>';
            msgBody += 'Can you please connect to the RCR Portal to check whether all the terms and conditions are agreeable? ';
            msgBody += 'If the Client Service Agreement is acceptable then please press the Accept CSA button to electronically confirm your acceptance, ';
            msgBody += 'otherwise please submit an RCR Service Query raising any concerns.';
            msgBody += '<p/><br/>';
            msgBody += EMAILFOOTER;

            addMessage(targetUserID, null, userEmailAddresses, 'New Customer Service Agreements Requiring Acceptance', msgBody);
        }
    }

    public void sendSubmitterApprovalGrantedNotification(RCR_Contract__c serviceOrder)
    {
        sendSubmitterApprovalGrantedNotification(serviceOrder, true, false);
    }

    public void sendSubmitterApprovalGrantedNotification(RCR_Contract__c serviceOrder, Boolean sendImmediate, Boolean sendToGenericInbox)
    {
        string msgBody = EmailStyles + EMAILGREETINGINTERNAL;
        msgBody += 'The Service Order ' + serviceOrder.Name + ' for ' + getSOClientName(serviceOrder);
        msgBody += ' has been \'Approved\' by ' + serviceOrder.Approver_User_Name__c + '.';
        msgBody += '<p/>';
        msgBody += '<a id="theLink" name="theLink" href="' + BaseURL__c.getInstance().Internal__c + serviceOrder.Id + '">' + BaseURL__c.getInstance().Internal__c + serviceOrder.Id + '</a>';
        msgBody += '<p/>';
        msgBody += EMAILFOOTER;

        if(sendToGenericInbox)
        {
            string[] toAddresses = new string[]{Email_Addresses__c.getInstance('Brokerage').Name};
            addMessage(null, null, toAddresses, 'Service Order ' + serviceOrder.Name + ' Approved', msgBody, sendImmediate);
        }
        else if(serviceOrder.Submited_For_Approval_User_ID__c != null)
        {
            addMessage(serviceOrder.Submited_For_Approval_User_ID__c, null, null, 'Service Order ' + serviceOrder.Name + ' Approved', msgBody, sendImmediate);
        }
    }

    public void sendSubmitterApprovalRejectedNotification(RCR_Contract__c serviceOrder)
    {
        sendSubmitterApprovalRejectedNotification(serviceOrder, true, false);
    }

    public void sendSubmitterApprovalRejectedNotification(RCR_Contract__c serviceOrder, Boolean sendImmediate, Boolean sendToGenericInbox)
    {
        string msgBody = EmailStyles  + EMAILGREETINGINTERNAL;
        msgBody += 'The Service Order ' + serviceOrder.Name + ' for ' + getSOClientName(serviceOrder);
        msgBody += ' has been \'Rejected\' by ' + serviceOrder.Approver_User_Name__c + '. Please review their comments and action accordingly.';
        msgBody += '<p/>';
        msgBody += '<a id="theLink" name="theLink" href="' + BaseURL__c.getInstance().Internal__c + serviceOrder.Id + '">' + BaseURL__c.getInstance().Internal__c + serviceOrder.Id + '</a>';
        msgBody += '<p/>';
        msgBody += EMAILFOOTER;

        if(sendToGenericInbox)
        {
            string[] toAddresses = new string[]{Email_Addresses__c.getInstance('Brokerage').Name};
            addMessage(null, null, toAddresses, 'Service Order ' + serviceOrder.Name + ' Approved', msgBody, sendImmediate);
        }
        else
        {
            addMessage(serviceOrder.Submited_For_Approval_User_ID__c, null, null, 'Service Order ' + serviceOrder.Name + ' Rejected', msgBody, sendImmediate);
        }
    }

    public void sendNoCSAApproversForAccountNotification(RCR_Contract__c serviceOrder)
    {
        sendNoCSAApproversForAccountNotification(serviceOrder, true, false);
    }

    public void sendNoCSAApproversForAccountNotification(RCR_Contract__c serviceOrder, Boolean sendImmediate, Boolean sendToGenericInbox)
    {
        Account acc = [SELECT   Name
                        FROM    Account
                        WHERE   ID = :serviceOrder.Account__c];

        string msgBody = EmailStyles + EMAILGREETINGINTERNAL;
        msgBody += 'The Client Services Agreement for Service Order ' + serviceOrder.Name + ' for ' + getSOClientName(serviceOrder);
        msgBody += ' requires acceptance by ' + acc.Name + ', however none of their users have permission to';
        msgBody += ' accept the Client Service Agreement electronically. You will need to send them a copy of the CSA for signing.';
        msgBody += '<p/>';
        msgBody += '<a id="theLink" name="theLink" href="' + BaseURL__c.getInstance().Internal__c + serviceOrder.Id + '">' + BaseURL__c.getInstance().Internal__c + serviceOrder.Id + '</a>';
        msgBody += '<p/>';
        msgBody += EMAILFOOTER;

        if(sendToGenericInbox)
        {
            string[] toAddresses = new string[]{Email_Addresses__c.getInstance('Brokerage').Name};
            addMessage(null, null, toAddresses, 'Service Order ' + serviceOrder.Name + ' Approved', msgBody, sendImmediate);
        }
        else
        {
            addMessage(serviceOrder.Submited_For_Approval_User_ID__c, null, null, 'Manual CSA Acceptance Required', msgBody, sendImmediate);
        }
    }

    public void sendNoCSAApproversForAccountNotification(List<RCR_Contract__c> serviceOrders, Id submitterID)
    {
        if(serviceOrders.isEmpty())
        {
            return;
        }
        Account acc = [SELECT   Name
                        FROM    Account
                        WHERE   ID = :serviceOrders[0].Account__c];

        if(submitterID != null)
        {
            string msgBody = EmailStyles + EMAILGREETINGINTERNAL;
            msgBody += 'The Client Services Agreements for the following Service Orders';
            msgBody += ' require acceptance by ' + acc.Name + ', however none of their users have permission to';
            msgBody += ' accept the Client Service Agreements electronically. You will need to send them a copy of the CSA for signing  for the following Service Orders:';
            msgBody += '<p/>';

            for(RCR_Contract__c c : serviceOrders)
            {
                msgBody += c.Name + ' for ' + getSOClientName(c) + '<br/>';
            }

            msgBody += '<p/><br/>';
            msgBody += EMAILFOOTER;

            addMessage(submitterID, null, null, 'Manual CSA Acceptance Required', msgBody);
        }
    }

    public void sendSubmitterServiceProviderAcceptanceNotification(RCR_Contract__c serviceOrder, string action, User acceptanceUser)
    {
        if(serviceOrder.Submited_For_Approval_User_ID__c != null)
        {
            if(action == 'Accepted' && UserUtilities.getUser(serviceOrder.Submited_For_Approval_User_ID__c).UserRole.Name == 'RCR Director')
            {
                return;
            }
            string msgBody = EmailStyles + EMAILGREETINGINTERNAL;
            msgBody += 'The Client Services Agreement ' + serviceOrder.Name + ' for ' + getSOClientName(serviceOrder);
            msgBody += ' has been \'' + action + '\' by ' + acceptanceUser.Name + '.';
            msgBody += '<p/>';
            msgBody += 'For more information please click on this link:';
            msgBody += '<p/>';
            msgBody += '<a id="theLink" name="theLink" href="' + BaseURL__c.getInstance().Internal__c + serviceOrder.Id + '">' + BaseURL__c.getInstance().Internal__c + serviceOrder.Id + '</a>';
            msgBody += '<p/>';
            msgBody += EMAILFOOTER;

            addMessage(serviceOrder.Submited_For_Approval_User_ID__c, null, null, 'Service Order ' + serviceOrder.Name + ' ' + action, msgBody);
        }
    }

    public void sendSubmitInvoiceAsyncComplete(RCR_Invoice__c invoice)
    {
        addMessage(getTemplate('Invoice_Submit_Async_Completed'), UserInfo.getUserId(), invoice.ID, null);
    }

    public void sendSubmitInvoiceAsyncComplete(Id jobID)
    {
        addMessage(getTemplate('Activity_Statements_Submitted_Complete'), UserInfo.getUserId(), jobID, null);
    }

    public void sendServiceOrderRequestReviwed(RCR_Contract__c serviceOrder)
    {
        addMessage(getTemplate('RCR_Service_Order_Request_Reviewed'), UserInfo.getUserId(), serviceOrder.ID, null);
    }

    public void sendServiceOrderReviewStatusUpdated(Task t)
    {
        addMessage(getTemplate('Service_Order_Task_Status_Changed'), t.CreatedById, t.ID, null);
    }

    public void sendServiceOrderBrokerageCompleted(Task t)
    {
        addMessage(getTemplate('Service_Order_Brokerage_Completed'), t.CreatedById, t.ID, null);
    }

    public void sendRateVariationNotifications(List<User> portalUsers, RCR_Service_Rate_Variation__c rateVariation)
    {
        string targetUserID;
        List<string> userEmailAddresses = new List<string>();
        string msgBody = EmailStyles;
        if(portalUsers != null && portalUsers.size() > 0)
        {
            integer cnt = 0;
            for(User u : portalUsers)
            {
                if(cnt == 0)
                {
                    targetUserID = u.Id;
                }
                else
                {
                    userEmailAddresses.add(u.Email);
                }
                cnt++;
            }

            msgBody += EMAILGREETINGPORTAL;
            msgBody += 'A Service Rate Variations Agreement requires actioning. Could you please visit the RCR Portal';
            msgBody += ' and check whether the details in the agreement are acceptable?';
            msgBody += '<p/>';
            msgBody += '<a id="theLink" name="theLink" href="' + BaseURL__c.getInstance().Internal__c + rateVariation.Id + '">' + BaseURL__c.getInstance().Internal__c + rateVariation.Id + '</a>';
            msgBody += '<p/>';
            msgBody += 'Can you please connect to the RCR Portal to check whether all the terms and conditions are agreeable?';
            msgBody += 'If the Client Service Agreement is acceptable then please press the Accept CSA button to electronically confirm your acceptance,';
            msgBody += 'otherwise please submit an RCR Service Query raising any concerns.';
            msgBody += '<p/>';
            msgBody += EMAILFOOTER;

            addMessage(targetUserID, null, userEmailAddresses, 'Service Rate Variations Agreement Requires Actioning', msgBody);
        }
        else
        {
            string accountName = [SELECT Name
                                FROM    Account
                                WHERE   Id = :rateVariation.Account__c].Name;

            for(User usr : [SELECT  Email
                            FROM    User
                            WHERE   Id = :rateVariation.Submitted_By__c])
            {
                userEmailAddresses.add(usr.Email);
                break;
            }

            msgBody += EMAILGREETINGINTERNAL;
            msgBody += 'A Service Rate Variations Agreement needs to be accepted however ' + accountName + ' is not set up for online acceptance.';
            msgBody += ' Please download and send the Service Rate Variations Agreement document to the Service Provider for signing.';
            msgBody += ' You can use this link to navigate to the RCR Portal:';
            msgBody += '<p/>';
            msgBody += '<a id="theLink" name="theLink" href="' + BaseURL__c.getInstance().Internal__c + rateVariation.Id + '">' + BaseURL__c.getInstance().Internal__c + rateVariation.Id + '</a>';
            msgBody += '<p/>';
            msgBody += EMAILFOOTER;

            addMessage(null, rateVariation.Id, userEmailAddresses, 'Manual Acceptance of Service Rates Required', msgBody);
        }
    }

    public void sendMasterpieceReconcilliationReports(RCR_Invoice_Batch__c invBatch)
    {
        decimal totalExGST = 0.0;
        decimal totalGST = 0.0;
        decimal totalIncGST = 0.0;

        RCRMasterpieceBatchReport mpReport = new RCRMasterpieceBatchReport(invBatch.ID);

        String[] sharedServicesEmailAddr = new string[] {(Test.isRunningTest() ? '1@test.com' : Email_Addresses__c.getInstance('SharedServices').Email__c)};
        String[] lsaFinancialsEmailAddr = new string[] {(Test.isRunningTest() ? '2@test.com' : Email_Addresses__c.getInstance('LSAFinancials').Email__c)};

        String msg = EmailStyles;

        msg +=  '<body>';
        msg +=  '<div class="hdg2" style="color:blue">LSA Masterpiece Reconciliation as at: ' + Date.today().format() + '</div>';
        msg +=  '<table width="50%">';
        msg +=      '<tr>';
        msg +=          '<td class="right">Batch Number: </td>';
        msg +=          '<td class="left">' + mpReport.BatchName + '</td>';
        msg +=      '</tr>';
        msg +=      '<tr>';
        msg +=          '<td class="right">Total Inc. GST: </td>';
        msg +=          '<td class="left">$' + mpReport.Batch.Total_Inc_GST_MP__c + '</td>';
        msg +=      '</tr>';
        msg +=      '<tr>';
        msg +=          '<td class="right">No. of Invoice Records: </td>';
        msg +=          '<td class="left">' + mpReport.VoucherCount + '</td>';
        msg +=      '</tr>';
        msg +=      '<tr>';
        msg +=          '<td class="right">No. of Vendor Records: </td>';
        msg +=          '<td class="left">' + mpReport.VendorCount + '</td>';
        msg +=      '</tr>';
        msg += '</table>';
        msg += '<body>';

        addMessage(UserInfo.getUserID(), null, sharedServicesEmailAddr, lsaFinancialsEmailAddr, 'LSA Masterpiece Reconciliation Report: ' + Date.today().format(), msg);
    }

    public void sendMessages()
    {
        if(Messages.size() > 0 && !Test.isRunningTest())
        {
            Messaging.sendEmail(Messages);
            Messages = null;
        }
    }

    private List<Messaging.SingleEmailMessage> Messages
    {
        get
        {
            if(Messages == null)
            {
                Messages = new List<Messaging.SingleEmailMessage>();
            }
            return Messages;
        }
        set;
    }

    @TestVisible
    private String getSOClientName(RCR_Contract__c serviceOrder)
    {

        if(String.isNotBlank(serviceOrder.Group_Program__c))
        {
            return serviceOrder.Group_Program__c;
        }
        // Client__r.Name relationship field will be null when called from trigger for a single approval,
        // but populated when called from a bulk process
        if(serviceOrder.Client__r.Name == null)
        {
            for(Referred_Client__c client : [SELECT Name,
                                                    Title__c,
                                                    CCMS_ID__c
                                            FROM    Referred_Client__c
                                            WHERE   ID = :serviceOrder.Client__c])
            {
                return client.Title__c + ' ' + client.Name + ' (' + client.CCMS_ID__c + ')';
            }
        }
        else
        {
            return serviceOrder.Client__r.Title__c != null ? serviceOrder.Client__r.Title__c : '' + ' ' + serviceOrder.Client__r.Name + serviceOrder.Client__r.CCMS_ID__c != null ? ' (' + serviceOrder.Client__r.CCMS_ID__c + ')' : '';
        }
        return '';
    }

    private void addMessage(EmailTemplate template, ID targetId, ID whatID, String[] toAddresses)
    {
        addMessage(template, targetId, whatID, toAddresses, true);
    }

    private void addMessage(EmailTemplate template, ID targetId, ID whatID, String[] toAddresses, Boolean sendImmediate)
    {
        if(template == null || targetID == null && whatID == null)
        {
            return;
        }
        Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
        msg.setTemplateId(template.ID);
        msg.setTargetObjectId(targetId);
        msg.setWhatId(whatID);
        msg.setSaveAsActivity(false);
        if(toAddresses != null)
        {
            msg.setToAddresses(toAddresses);
        }
        Messages.add(msg);
        if(sendImmediate)
        {
            sendMessages();
        }
    }

    private void addMessage(ID targetID, ID whatID, String[] toAddresses, String subject, String htmlBody)
    {
        addMessage(targetID, whatID, toAddresses, subject, htmlBody, true);
    }

    private void addMessage(ID targetID, ID whatID, String[] toAddresses, String[] ccAddresses, String subject, String htmlBody)
    {
        addMessage(targetID, whatID, toAddresses, ccAddresses, subject, htmlBody, true);
    }

    private void addMessage(ID targetID, ID whatID, String[] toAddresses, String subject, String htmlBody, Boolean sendImmediate)
    {
        addMessage(targetID, whatID, toAddresses, null, subject, htmlBody, sendImmediate);
    }

    private void addMessage(ID targetID, ID whatID, String[] toAddresses, String[] ccAddresses, String subject, String htmlBody, Boolean sendImmediate)
    {
        if(targetID == null && whatID == null)
        {
            return;
        }
        Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
        msg.setSubject(subject);
        msg.setHtmlBody(htmlBody);
        msg.setSaveAsActivity(false);
        msg.setTargetObjectId(targetID);
        msg.setWhatId(whatID);
        if(toAddresses != null)
        {
            msg.setToAddresses(toAddresses);
        }
        if(ccAddresses != null)
        {
            msg.setCcAddresses(ccAddresses);
        }
        Messages.add(msg);
        if(sendImmediate)
        {
            sendMessages();
        }
    }

    private EmailTemplate getTemplate(String templateDeveloperName)
    {
        return [SELECT  ID
                FROM    EmailTemplate
                WHERE   DeveloperName = :templateDeveloperName];
    }
}