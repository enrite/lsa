global class RCRServiceOrderAllocationSchedule  implements Schedulable
{
    global RCRServiceOrderAllocationSchedule(decimal adjForNextYear, decimal adjForFollowingYear)
    {
    	adjustmentForNextYear = adjForNextYear;
    	adjustmentForFollowingYear = adjForFollowingYear;
    }
	private decimal adjustmentForNextYear;
	private decimal adjustmentForFollowingYear;
	
    global void execute(SchedulableContext SC) 
    {
//        RCRServiceOrderAllocationBatch allocBatch = new RCRServiceOrderAllocationBatch();
//        allocBatch.NextYearAdjustment = adjustmentForNextYear;
//        allocBatch.FollowingYearAdjustment = adjustmentForFollowingYear;
//        allocBatch.ScheduleId = SC.getTriggerId();
//        Database.executeBatch(allocBatch, Batch_Size__c.getInstance('RCR_Service_Order_Allocation').Records__c.intValue());
    }
}