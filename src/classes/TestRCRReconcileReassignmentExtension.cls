@isTest
private class TestRCRReconcileReassignmentExtension
{
    static testMethod void myUnitTest() 
    {
        TestLoadData.loadRCRData();
        
        Test.StartTest();
        
        TestLoadData.loadAdhocServices();
        
        RCR_Contract__c so;
        
        for (RCR_Contract__c c : [SELECT Id, 
                                         RecordTypeId,
                                         (SELECT Id
                                          FROM RCR_Contract_Scheduled_Services__r),
                                         (SELECT Id
                                          FROM RCR_Contract_Adhoc_Services__r)                                      
                                  FROM RCR_Contract__c LIMIT 1])
        {
            if (c.RCR_Contract_Scheduled_Services__r.size() > 0)
            {
                so = c;
                break;
            }                 
        }           
        
        RCRReconcileReassignmentExtension.ServiceWrapper sw = new RCRReconcileReassignmentExtension.ServiceWrapper(so.RCR_Contract_Scheduled_Services__r[0], so.RCR_Contract_Adhoc_Services__r[0]);
        system.debug(sw.ScheduledService);
        system.debug(sw.AdHocService);
        system.debug(sw.Selected);
                
        Test.StopTest();
    }
}