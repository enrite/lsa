public with sharing class PlanGoalsViewRedirectExtension {
    Plan_Goal__c record;

    public PlanGoalsViewRedirectExtension(ApexPages.StandardController con) {
        record = (Plan_Goal__c)con.getRecord();
    }

    public PageReference Redir()
    {
        try
        {  
            PageReference pg;

            if (ApexPages.currentPage().getHeaders().get('referer').toLowerCase().contains('participantportal')){
                pg = Page.ParticipantPortalPlanGoal;
            }            
            else
            {
                pg = new PageReference('/' + record.Id);
                
                pg.getParameters().put('nooverride', '1');
            }
            
            // add the parameters for this page to the new page reference
            for (String k : ApexPages.CurrentPage().getParameters().keySet())
            {
                if (k != 'save_new' && k != '_CONFIRMATIONTOKEN')
                {
                    pg.getParameters().put(k, ApexPages.CurrentPage().getParameters().get(k));
                }
            }
            
            pg.setRedirect(true);
            return pg;
        }
        catch(Exception e)
        {
            return A2HCException.formatException(e);
        }
    }
}