public class SMSCompositionController extends A2HCPageBase
{
    private transient List<SelectOption> tParticipantStatuses;
    public List<ResultWrapper> Clients
    {
        get 
        {
            if(Clients == null)
            {
                Clients = new List<ResultWrapper>();
            }
            return Clients;
        }
        private set;
    }

    public List<ResultWrapper> Contacts
    {
        get
        {
            if(Contacts == null)
            {
                Contacts = new List<ResultWrapper>();
            }
            return Contacts;
        }
        private set;
    }

    public Map<ID, String> MsgsByClientId
    {
        get 
        {
            if(MsgsByClientId == null)
            {
                MsgsByClientId = new Map<ID, String>();
            }
            return MsgsByClientId;
        }
        private set;
    }

    public Integer Balance 
    {
        get 
        {
            if(Balance == null)
            {
                Balance = SMSUtilities.getBalance();
            }
            return Balance;
        }
        private set;
    }

    public List<SelectOption> AccountTypes
    {
        get
        {
            if(AccountTypes== null)
            {
                AccountTypes = getSelectListFromPicklist(Account.Account_Type__c.getDescribe(), false);
            }
            return AccountTypes;
        }
        private set;
    }

    public List<SelectOption> ParticipantTypes
    {
        get
        {
            if(ParticipantTypes== null)
            {
                ParticipantTypes = getSelectListFromObjectList([SELECT ID, Name
                                                                FROM    RecordType
                                                                WHERE   sObjectType = 'Referred_Client__c'
                                                                ORDER BY Name], false);
                ParticipantTypes.add(0, new SelectOption('', '--All--'));
            }
            return ParticipantTypes;
        }
        private set;
    }

    private List<SelectOption> ParticipantStatuses
    {
        get
        {
            if(tParticipantStatuses == null)
            {
                tParticipantStatuses = getSelectListFromPicklist(Referred_Client__c.Current_Status__c.getDescribe(), false);
            }
            return tParticipantStatuses;
        }
    }

    public MultiSelectController MutliController
    {
        get
        {
            if(MutliController == null)
            {
                MutliController = new MultiSelectController();
                MutliController.LeftOptions = ParticipantStatuses;
                MutliController.RightOptions = new List<SelectOption>{};
            }
            return MutliController;
        }
        set;
    }

    public MultiSelectController AccountTypeMutliController
    {
        get
        {
            if(AccountTypeMutliController == null)
            {
                AccountTypeMutliController = new MultiSelectController();
                AccountTypeMutliController.LeftOptions = AccountTypes;
                AccountTypeMutliController.RightOptions = new List<SelectOption>{};
            }
            return AccountTypeMutliController;
        }
        set;
    }

    public MultiSelectController StateMutliController
    {
        get
        {
            if(StateMutliController == null)
            {
                StateMutliController = new MultiSelectController();
                StateMutliController.LeftOptions = StateList;
                StateMutliController.LeftOptions.remove(0);
                StateMutliController.RightOptions = new List<SelectOption>{};
            }
            return StateMutliController;
        }
        set;
    }

    public Referred_Client__c SearchClient
    {
        get
        {
            if(SearchClient == null)
            {
                SearchClient = new Referred_Client__c();
            }
            return SearchClient;
        }
        private set;
    }

    public String SearchType
    {
        get;
        set;
    }

    public String SearchParticipantName
    {
        get;
        set;
    }

    public String SearchCountry
    {
        get
        {
            if(SearchCountry == null)
            {
                SearchCountry = 'Australia';
            }
            return SearchCountry;
        }
        set;
    }


    public String ParticipantType
    {
        get;
        set;
    }

    public Boolean IncludeParticipantContacts
    {
        get
        {
            if(IncludeParticipantContacts == null)
            {
                return false;
            }
            return IncludeParticipantContacts;
        }
        set;
    }

    public String SearchContactName
    {
        get;
        set;
    }

    public String AllocatedTo
    {
        get;
        set;
    }

    public String SearchAccountName
    {
        get;
        set;
    }

    public String ServiceAccountType
    {
        get;
        set;
    }

    public String ServiceServiceType
    {
        get;
        set;
    }

    public String Message 
    {
        get;
        set;
    }

    public void changeSearchType()
    {
        Contacts = null;
        Clients = null;
    }

    public void doSearch()
    {
        try 
        {
            Contacts = null;
            Clients = null;
            MsgsByClientId = null;

            String clName = String.isBlank(SearchParticipantName) ? '%' : ('%' + SearchParticipantName + '%');
            String partType = String.isBlank(ParticipantType) ? '%' : ParticipantType;
            String allocTo = String.isBlank(AllocatedTo) ? null : ('%' + AllocatedTo + '%');
            List<String> statuses = MutliController.getSelected();

            for(Referred_Client__c cl : [SELECT   ID,
                                                    Name,
                                                    Title__c,
                                                    Given_Name__c,
                                                    Family_Name__c, 
                                                    Mobile__c,
                                                    Current_Status__c,
                                                    Client_managed_for__c,
                                                    RecordType.Name,
                                                    (SELECT   ID,
                                                            First_Name__c,
                                                            Surname__c,
                                                            Title__c,
                                                            Full_Name__c,
                                                            Mobile_Phone__c,
                                                            Relationship_to_Participant__c,
                                                            Participant__r.Full_Name__c
                                                    FROM    Participant_Contacts__r
                                                    WHERE   Mobile_Phone__c != null
                                                    ORDER BY Surname__c, First_Name__c)
                                            FROM    Referred_Client__c
                                            WHERE   Mobile__c != null AND
                                                    Name LIKE :clName AND
                                                    (
                                                            RecordTypeId = :ParticipantType OR
                                                            Name LIKE :(String.isBlank(ParticipantType) ? '%' : '~')
                                                    ) AND
                                                    (
                                                        Allocated_to__r.Name LIKE :(allocTo == null ? '%' : allocTo) OR
                                                        Allocated_to__c = :allocTo
                                                    ) AND
                                                    (
                                                            Current_Status__c IN :statuses OR
                                                            Name LIKE :(statuses.isEmpty() ? '%' : '~')
                                                    )
                                            ORDER BY Family_Name__c, Given_Name__c
                                            LIMIT 1001])
            {
                Clients.add(new ResultWrapper(cl));
                MsgsByClientId.put(cl.ID, '');
                if(IncludeParticipantContacts)
                {
                    for(Participant_Contact__c pc : cl.Participant_Contacts__r)
                    {
                        Clients.add(new ResultWrapper(pc));
                        MsgsByClientId.put(pc.ID, '');
                    }
                }
            }

            if(Clients.size() > 1000)
            {
                Clients = null;
                throw new A2HCException('Too many records found, try limiting the search') ;
            }
        }
        catch (Exception ex)
        {
            A2HCException.formatException(ex);
        }
    }

    public void doContactSearch()
    {
        try
        {
            Contacts = null;
            Clients = null;
            MsgsByClientId = null;

            String accName = (String.isBlank(SearchAccountName) ? '%' : SearchAccountName) + '%';
            String accServType = (String.isBlank(ServiceServiceType) ? '%' : ServiceServiceType) + '%';
            List<String> accTypes = AccountTypeMutliController.getSelected();
            List<String> states = StateMutliController.getSelected();
            for(Integer i = 0; i < states.size(); i++)
            {
                String s = states[i];
                if(s == 'SA')
                {
                    states.add('South Australia');
                }
                if(s == 'VIC')
                {
                    states.add('Victoria');
                }
                if(s == 'NSW')
                {
                    states.add('New South Wales');
                }
                if(s == 'QLD')
                {
                    states.add('Queensland');
                }
                if(s == 'TAS')
                {
                    states.add('Tasmania');
                }
                if(s == 'ACT')
                {
                    states.add('Australian Capital Territory');
                }
                if(s == 'NT')
                {
                    states.add('Northern Territory');
                }
                if(s == 'WA')
                {
                    states.add('Western Australia');
                }
            }
system.debug(accTypes);
system.debug(accServType);
system.debug(states);
            MsgsByClientId = null;
            for(Contact c : [SELECT     ID,
                                        Name,
                                        Title,
                                        FirstName,
                                        LastName,
                                        MobilePhone,
                                        AccountId,
                                        Account.Name
                                FROM    Contact
                                WHERE   MobilePhone != null AND
                                        Account.Name LIKE :accName AND
                                        (
                                            Account.Account_Type__c IN :accTypes OR
                                            Name LIKE :(accTypes.isEmpty() ? '%' : '~')
                                        ) AND
                                        (
                                            Account.Service_Type__c = null OR
                                            Account.Service_Type__c LIKE :accServType
                                        ) AND
                                        (
                                            Account.ShippingState IN :states OR
                                            Name LIKE :(states.isEmpty() ? '%' : '~')
                                        ) AND
//                                        Account.ShippingCountry = :SearchCountry AND
                                        ID NOT IN (SELECT   Participant_Portal_Contact__c
                                                    FROM    Referred_Client__c)
                                ORDER BY Account.Name, LastName, FirstName
                                LIMIT 1001])
            {
                Contacts.add(new ResultWrapper(c));
                MsgsByClientId.put(c.ID, '');
            }
            if(Contacts.size() > 1000)
            {
                Contacts = null;
                throw new A2HCException('Too many records found, try limiting the search') ;
            }
        }
        catch (Exception ex)
        {
            A2HCException.formatException(ex);
        }
    }

    public void sendSMS()
    {
        try 
        {
            //MsgsByClientId = null;
            final String REFERRED_CLIENT = Referred_Client__c.SObjectType.getDescribe().getName();
            final String PARTICIPANT_CONTACT = Participant_Contact__c.SObjectType.getDescribe().getName();
            List<Referred_Client__c> selectedClients = new List<Referred_Client__c>();
            List<Participant_Contact__c> selectedParticipantContacts = new List<Participant_Contact__c>();
            List<Contact> selectedContacts = new List<Contact>();
            for(ResultWrapper rw : Clients)
            {
                if(rw.Selected)
                {
                    if(rw.ObjectType == REFERRED_CLIENT)
                    {
                        selectedClients.add(rw.Participant);
                    }
                    else if(rw.ObjectType == PARTICIPANT_CONTACT)
                    {
                        selectedParticipantContacts.add(rw.ParticipantContact);
                    }
                }
            }
            for(ResultWrapper rw : Contacts)
            {
                if(rw.Selected)
                {
                    selectedContacts.add(rw.ProviderContact);
                }
            }
            if(selectedClients.isEmpty() && selectedParticipantContacts.isEmpty() && selectedContacts.isEmpty())
            {
                A2HCException.formatException('No recipients selected');
            }
            if(String.isBlank(Message))
            {
                A2HCException.formatException('Message is required');
            }
            if(ApexPages.hasMessages(ApexPages.Severity.Error))
            {
                return;
            }
            SMS_Message__c smsMessage = new SMS_Message__c(Message__c = Message);
            Map<ID, String> returnMsgs;
            Integer sent = 0;
            Integer err = 0;
            if(selectedClients.size() > 0)
            {
                returnMsgs = SMSUtilities.sendSMS(selectedClients, smsMessage, Referred_Client__c.Mobile__c, Message);
                sent += getMessageResult(returnMsgs, false);
                err += getMessageResult(returnMsgs, true);
                MsgsByClientId.putAll(returnMsgs);
            }
            if(selectedParticipantContacts.size() > 0)
            {
                returnMsgs = SMSUtilities.sendSMS(selectedParticipantContacts, smsMessage, Participant_Contact__c.Mobile_Phone__c, Message);
                sent += getMessageResult(returnMsgs, false);
                err += getMessageResult(returnMsgs, true);
                MsgsByClientId.putAll(returnMsgs);
            }
            if(selectedContacts.size() > 0)
            {
                returnMsgs = SMSUtilities.sendSMS(selectedContacts, smsMessage, Contact.MobilePhone, Message);
                sent += getMessageResult(returnMsgs, false);
                err += getMessageResult(returnMsgs, true);
                MsgsByClientId.putAll(returnMsgs);
            }

            A2HCException.formatMessage(String.valueOf(sent) + ' messages sent');
            if(err > 0)
            {
                A2HCException.formatException(String.valueOf(err) + ' messages were not sent, see errors');
            }
            Balance -= sent;
        }
        catch (Exception ex)
        {
            A2HCException.formatException(ex);
        }
    }

    private Integer getMessageResult(Map<ID, String> returnMsgs, Boolean countError)
    {
        Integer i = 0;
        for(String msg : returnMsgs.values())
        {
            if((!countError && String.isBlank(msg)) ||
                    (countError && String.isNotBlank(msg)))
            {
                i++;
            }
        }
        return i;
    }

    public class ResultWrapper
    {
        public ResultWrapper(Referred_Client__c client)
        {
            Participant = client;
            Selected = false;
            ObjectType = Referred_Client__c.SObjectType.getDescribe().getName();
            PersonId = client.ID;
            Name = client.Name;
            MobileNumber = client.Mobile__c;
            Type = client.RecordType.Name;
            Status = client.Current_Status__c;
        }

        public ResultWrapper(Contact cont)
        {
            ProviderContact = cont;
            Selected = false;
            ObjectType = Contact.SObjectType.getDescribe().getName();
            PersonId = cont.ID;
            Name = cont.Name;
            MobileNumber = cont.MobilePhone;

        }

        public ResultWrapper(Participant_Contact__c pc)
        {
            ParticipantContact = pc;
            Selected = false;
            ObjectType = Participant_Contact__c.SObjectType.getDescribe().getName();
            PersonId = pc.ID;
            Name = pc.Full_Name__c;
            MobileNumber = pc.Mobile_Phone__c;
            Type = 'Contact';
            Status = pc.Relationship_to_Participant__c + ' of ' + pc.Participant__r.Full_Name__c;
        }

        public Boolean Selected
        {
            get;
            set;
        }

        public String ObjectType
        {
            get;
            private set;
        }

        public ID PersonId
        {
            get;
            private set;
        }

        public String MobileNumber
        {
            get;
            private set;
        }

        public String Name
        {
            get;
            private set;
        }

        public String Type
        {
            get;
            private set;
        }

        public String Status
        {
            get;
            private set;
        }

        public Referred_Client__c Participant
        {
            get;
            set;
        }

        public Contact ProviderContact
        {
            get;
            set;
        }

        public Participant_Contact__c ParticipantContact
        {
            get;
            set;
        }

    }
}