public with sharing class RCRInvoiceSearchController extends GridPager2
{
    public RCRInvoiceSearchController(ApexPages.StandardSetController controller)
    {
        doSearch(getParameter('srch'));
    }

    public RCRInvoiceSearchController(ApexPages.StandardController controller)
    {
        doSearch(getParameter('srch'));
    }

    public RCRInvoiceSearchController()
    {
        doSearch(getParameter('srch'));
    }

    public List<RCR_Invoice__c> Invoices
    {
        get
        {
            return (List<RCR_Invoice__c>)Records;
        }
        set;
    }

    public List<SelectOption> Accounts
    {
        get
        {
            if(Accounts == null)
            {
                Accounts = new List<SelectOption>();
                if(!InPortal)
                {
                    Accounts.add(new SelectOption('', '<All>'));
                }
                for(Account acc : [SELECT   ID,
                                            Name
                                    FROM    Account
                                    ORDER BY Name])
                {
                    Accounts.add(new SelectOption(acc.ID, acc.Name));
                }
            }
            return Accounts;
        }
        set;
    }

    public List<SelectOption> Statuses
    {
        get
        {
            if(Statuses == null)
            {
                Statuses = new List<SelectOption>();
                Statuses.add(new SelectOption('', '<All>'));
                Statuses.add(new SelectOption(APS_PicklistValues.RCRInvoice_Status_Loaded, APS_PicklistValues.RCRInvoice_Status_Loaded));
                Statuses.add(new SelectOption(APS_PicklistValues.RCRInvoice_Status_ReconciliationInProgress, APS_PicklistValues.RCRInvoice_Status_ReconciliationInProgress));
                Statuses.add(new SelectOption(APS_PicklistValues.RCRInvoice_Status_ReconciledErrors, APS_PicklistValues.RCRInvoice_Status_ReconciledErrors));
                Statuses.add(new SelectOption(APS_PicklistValues.RCRInvoice_Status_ReconciledNoErrors, APS_PicklistValues.RCRInvoice_Status_ReconciledNoErrors));
                Statuses.add(new SelectOption(APS_PicklistValues.RCRInvoice_Status_PendingApproval, APS_PicklistValues.RCRInvoice_Status_PendingApproval));
                Statuses.add(new SelectOption(APS_PicklistValues.RCRInvoice_Status_Approved, APS_PicklistValues.RCRInvoice_Status_Approved));
                //Statuses.add(new SelectOption(APS_PicklistValues.RCRInvoice_Status_RCTIGenerated, APS_PicklistValues.RCRInvoice_Status_RCTIGenerated));
            }
            return Statuses;
        }
        set;
    }

    public String SearchCriteria
    {
        get
        {
            String s = '';
            s += (SelectedAccount == null ? '' : SelectedAccount) + '~';
            s += (SelectedProgram == null ? '' : SelectedProgram) + '~';
            s += (SelectedStatus == null ? '' : SelectedStatus) + '~';
            s += (InvoiceNumber == null ? '' : InvoiceNumber) + '~';
            s += (FromDate == null ? '' : FromDate) + '~';
            s += (ToDate == null ? '' : ToDate) + '~';
            s += (Filename == null ? '' : Filename) + '~';
            s += (Description == null ? '' : Description) + '~';
            s += (IncludeRecon ? 'Y' : 'N');

            return s;
        }
    }

    public String SelectedProgram
    {
        get;
        set;
    }

    public String SelectedAccount
    {
        get;
        set;
    }

    public String SelectedStatus
    {
        get;
        set;
    }
    public String InvoiceNumber
    {
        get;
        set;
    }

    public String FromDate
    {
        get;
        set;
    }

    public String ToDate
    {
        get;
        set;
    }

    public String Filename
    {
        get;
        set;
    }

    public String Description
    {
        get;
        set;
    }

    public Boolean IncludeRecon
    {
        get
        {
            if(IncludeRecon == null)
            {
                return false;
            }
            return IncludeRecon;
        }
        set;
    }

    public override void goPage()
    {
        try
        {
        	super.goPage();
            doSearch();
        }
        catch(Exception ex)
        {
            A2HCException.formatException(ex);
        }
    }

    public override void first()
    {
        super.first();
        doSearch();
    }

    public override void last()
    {
        super.last();
        doSearch();
    }

    public PageReference searchInvoices()
    {
        try
        {
            Invoices = null;
            PageNumber = 1;
            goPage();
            return null;
        }
        catch(Exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }

    public Pagereference newInvoice()
    {
        try
        {
            PageReference pg = Page.RCRInvoiceUpload;
            pg.getParameters().put('retURL', '/apex/RCRInvoiceSearch');
            pg.getParameters().put('srch', SearchCriteria);
            return pg.setRedirect(true);
        }
        catch(Exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }

    public Pagereference uploadInvoice()
    {
        try
        {
            PageReference pg = Page.RCRInvoiceUpload;
            pg.getParameters().put('retURL', '/apex/RCRInvoiceSearch');
            pg.getParameters().put('srch', SearchCriteria);
            return pg.setRedirect(true);
        }
        catch(Exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }


    private String getParameterFromURLParameter(String s)
    {
        return A2HCUtilities.isStringNullOrEmpty(s) ? null : s;
    }

    private void doSearch(String srchCriteria)
    {
        if(A2HCUtilities.isTrimmedStringNullOrEmpty(srchCriteria) || srchCriteria == 'null')
        {
            return;
        }
        String[] criteria = srchCriteria.split('~');
        if(criteria.size() < 8)
        {
            return;
        }
        SelectedAccount = getParameterFromURLParameter(criteria[0]);
        SelectedProgram = getParameterFromURLParameter(criteria[1]);
        SelectedStatus = getParameterFromURLParameter(criteria[2]);
        InvoiceNumber = getParameterFromURLParameter(criteria[3]);
        FromDate = getParameterFromURLParameter(criteria[4]);
        ToDate = getParameterFromURLParameter(criteria[5]);
        Filename = getParameterFromURLParameter(criteria[6]);
        Description = getParameterFromURLParameter(criteria[7]);
        IncludeRecon = getParameterFromURLParameter(criteria[8]) == 'Y';

        searchInvoices();
    }

    private void doSearch()
    {
        String[] searchStatus = new String[]{};
        if(SelectedStatus == null)
        {
            for(SelectOption opt : Statuses)
            {
                searchStatus.add(opt.getValue());
            }
        }
        else
        {
            searchStatus = new String[]{SelectedStatus};
        }
        if(IncludeRecon)
        {
            searchStatus.add(APS_PickListValues.RCRInvoice_Status_PaymentProcessed);
        }
        Date startDate = A2HCUtilities.tryParseDate(FromDate);
        Date endDate = A2HCUtilities.tryParseDate(ToDate);
        startDate = startDate == null ? Date.newInstance(1900, 1, 1) : startDate;
        endDate = endDate == null ? Date.newInstance(3000, 1, 1) : endDate;

        Filename = A2HCUtilities.escapeSearchString(Filename);
        InvoiceNumber = A2HCUtilities.escapeSearchString(InvoiceNumber);
		Description = A2HCUtilities.escapeSearchString(Description);
		
        TotalRecords = [SELECT  COUNT()
                       	FROM    RCR_Invoice__c  i
                        WHERE   (i.Date__c = null OR
                        		(i.Date__c >= :startDate AND
	                            i.Date__c <= :endDate)) AND
	                            (
	                                i.Account__c = :SelectedAccount OR
	                                i.Account__r.Name LIKE :(SelectedAccount == null ? '%' : '~')
	                            ) AND
                                (i.Invoice_Number__c LIKE :(InvoiceNumber == null ? '%' : InvoiceNumber.trim() + '%') OR
                                i.LP_Invoice_Number__c LIKE :(InvoiceNumber == null ? '%' : InvoiceNumber.trim() + '%')) AND
	                            i.Status__c IN :searchStatus AND
	                            (
	                                i.RCR_Invoice_Upload__r.File_Name__c = :Filename OR
	                                i.RCR_Invoice_Upload__r.File_Name__c LIKE :(Filename == null ? '%' : '~')
	                            ) AND
	                            (
	                                i.RCR_Invoice_Upload__r.Description__c = :Description OR
	                                i.RCR_Invoice_Upload__r.Description__c LIKE :('%' + Description + '%') OR
	                                i.RCR_Invoice_Upload__r.Description__c LIKE :(Description == null ? '%' : '~')
	                            )];

		// can't seem to able to use an inherited property for this
		Integer offst = QueryOffset;
        Records = [SELECT i.Id,
                            i.Name,
                            i.Invoice_Number__c,
                            LP_Invoice_Number__c,
                            i.Date__c,
                            i.Start_Date__c,
                            i.End_Date__c,
                            i.Total__c,
                            i.Total_ex_GST__c,
                            i.Total_GST__c,
                            i.Status__c,
                            Created_By_Portal_User__c,
                            i.RCR_Invoice_Batch__c,
                            i.RCR_Invoice_Batch__r.Name,
                            i.RCR_Invoice_Batch__r.Batch_Number__c,
                            i.RCR_Invoice_Upload__c,
                            i.RCR_Invoice_Upload__r.File_Name__c,
                            i.RCR_Invoice_Upload__r.Description__c,
                            i.Account__r.Name
                    FROM    RCR_Invoice__c  i
                    WHERE   (i.Date__c = null OR
                    		(i.Date__c >= :startDate AND
                            i.Date__c <= :endDate)) AND
                            (
                                i.Account__c = :SelectedAccount OR
                                i.Account__r.Name LIKE :(SelectedAccount == null ? '%' : '~')
                            ) AND
                            (i.Invoice_Number__c LIKE :(InvoiceNumber == null ? '%' : InvoiceNumber.trim() + '%') OR
                            i.LP_Invoice_Number__c LIKE :(InvoiceNumber == null ? '%' : InvoiceNumber.trim() + '%')) AND
                            i.Status__c IN :searchStatus AND
                            (
                                i.RCR_Invoice_Upload__r.File_Name__c = :Filename OR
                                i.RCR_Invoice_Upload__r.File_Name__c LIKE :(Filename == null ? '%' : '~')
                            ) AND
                            (
                                i.RCR_Invoice_Upload__r.Description__c = :Description OR
	                                i.RCR_Invoice_Upload__r.Description__c LIKE :('%' + Description + '%') OR
	                                i.RCR_Invoice_Upload__r.Description__c LIKE :(Description == null ? '%' : '~')
                            )
                    ORDER BY i.Date__c DESC
				   	LIMIT 	:PageSize
				   	OFFSET 	:offst];			   	
    }
}