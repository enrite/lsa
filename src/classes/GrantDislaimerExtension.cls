/**
 * Created by USer on 06/09/2016.
 */

public with sharing class GrantDislaimerExtension extends A2HCPageBase
{
    public GrantDislaimerExtension()
    {
    }

    public PageReference redir()
    {
        if(CurrentUser.Grant_Disclaimer__c == null)
        {
            return null;
        }
        PageReference pg = Page.GPMyGrants;
        return pg;
    }

    public PageReference accept()
    {
        try
        {
            CurrentUser.Grant_Disclaimer__c = DateTime.now();
            update CurrentUser;

            PageReference pg = Page.GPMyGrants;
            return pg;
        }
        catch(Exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }
}