public with sharing class RCRTopAllocationsApprovedRptController extends A2HCPageBase
{
//	public RCRTopAllocationsApprovedRptController()
//	{
//
//		loadData();
//	}
//
//
//	public string StartDt
//	{
//		get;
//		private set;
//	}
//
//
//	public string EndDt
//	{
//		get;
//		private set;
//	}
//
//
//	public boolean ByRequestType
//	{
//		get
//		{
//			if(ByRequestType == null)
//			{
//				ByRequestType = false;
//			}
//			return ByRequestType;
//		}
//		set;
//	}
//
//	public List<AllocationWrapper> Allocations
//	{
//		get
//		{
//			if(Allocations == null)
//			{
//				Allocations = new List<AllocationWrapper>();
//			}
//			return Allocations;
//		}
//		private set;
//	}
//
//
//	public List<AllocationWrapper> NewServicesAllocations
//	{
//		get
//		{
//			if(NewServicesAllocations == null)
//			{
//				NewServicesAllocations = new List<AllocationWrapper>();
//			}
//			return NewServicesAllocations;
//		}
//		private set;
//	}
//
//
//	public List<AllocationWrapper> AdditionalNewServiceAllocations
//	{
//		get
//		{
//			if(AdditionalNewServiceAllocations == null)
//			{
//				AdditionalNewServiceAllocations = new List<AllocationWrapper>();
//			}
//			return AdditionalNewServiceAllocations;
//		}
//		private set;
//	}
//
//
//	public List<AllocationWrapper> IncreaseExistingServiceAllocations
//	{
//		get
//		{
//			if(IncreaseExistingServiceAllocations == null)
//			{
//				IncreaseExistingServiceAllocations = new List<AllocationWrapper>();
//			}
//			return IncreaseExistingServiceAllocations;
//		}
//		private set;
//	}
//
//
//	public PageReference Close()
//	{
//		try
//		{
//			PageReference pg = Page.RCRPTICReports;
//			pg.setRedirect(true);
//			return pg;
//		}
//		catch(Exception ex)
//		{
//			return A2HCException.formatException(ex);
//		}
//	}
//
//
//	public void loadData()
//	{
//		try
//		{
//			date startDate = Date.parse(getParameter('startDate'));
//            date endDate = Date.parse(getParameter('endDate'));
//
//            StartDt = startDate.format();
//            EndDt = endDate.format();
//
//            Allocations = null;
//            NewServicesAllocations = null;
//            AdditionalNewServiceAllocations = null;
//            IncreaseExistingServiceAllocations = null;
//
//            Map<Id, List<RCR_Contract__c>> ServiceOrdersByClientID = new Map<Id, List<RCR_Contract__c>>();
//            Map<string, decimal> ServiceTypeCostByClientIDAndServiceType = new Map<string, decimal>();
//            Map<string, decimal> AllocationsByClientIDAndServiceType = new Map<string, decimal>();
//
//			Set<Id> ClientIDs = new Set<Id>();
//			for(IF_Personal_Budget__c pb : [SELECT	Client__c
//											FROM	IF_Personal_Budget__c
//											WHERE	RCR_Service_Order__r.Allocation_Approved_Date__c >= :startDate
//											AND		RCR_Service_Order__r.Allocation_Approved_Date__c <= :endDate
//											ORDER BY Amount__c desc])
//			{
//				ClientIDs.add(pb.Client__c);
//			}
//
//			// Get Total Service Order Costs column.
//			for(RCR_Contract_Scheduled_Service__c css : [SELECT	Id,
//																RCR_Contract__r.Client__c,
//																Total_Cost__c,
//																Total_Quantity__c,
//																(SELECT Allocation_Service_Type__c,
//																		Quantity__c
//																 FROM	RCR_Contract_Service_Types__r)
//														 FROM	RCR_Contract_Scheduled_Service__c
//														 WHERE	RCR_Contract__r.Client__c IN :ClientIDs
//														 AND	RCR_Contract__r.RecordType.Name IN (:APSRecordTypes.RCRServiceOrder_ServiceOrder,
//														 											:APSRecordTypes.RCRServiceOrder_ServiceOrderCBMS)])
//			{
//				calculateServiceTypeCost(css, ServiceTypeCostByClientIDAndServiceType);
//			}
//
//			// Get Total Service Order Costs column.
//			for(RCR_Contract_AdHoc_Service__c ahs : [SELECT	Id,
//															RCR_Contract__r.Client__c,
//															Amount__c,
//															Qty__c,
//															(SELECT Allocation_Service_Type__c,
//																	Quantity__c
//															 FROM	RCR_Contract_Service_Types__r)
//													 FROM	RCR_Contract_AdHoc_Service__c
//													 WHERE	RCR_Contract__r.Client__c IN :ClientIDs
//													 AND	RCR_Contract__r.RecordType.Name IN (:APSRecordTypes.RCRServiceOrder_ServiceOrder,
//													 											:APSRecordTypes.RCRServiceOrder_ServiceOrderCBMS)])
//			{
//				calculateServiceTypeCost(ahs, ServiceTypeCostByClientIDAndServiceType);
//			}
//
//
//			// Query to get Total Allocations Column.
//			for(AggregateResult ar : [SELECT	SUM(Amount__c) Amount,
//												Service_Type__c,
//												Client__c,
//												Financial_Year__r.Name FY
//									  FROM		If_Personal_Budget__c
//									  WHERE		Client__c IN :ClientIDs
//									  AND		Status__c = :APS_PicklistValues.IFAllocation_Status_Approved
//									  GROUP BY Service_Type__c,
//												Client__c,
//												Financial_Year__r.Name])
//			{
//				string key = (string)ar.get('Client__c') + (string)ar.get('Service_Type__c') + (string)ar.get('FY');
//				AllocationsByClientIDAndServiceType.put(key , (decimal)ar.get('Amount'));
//				system.debug(key + ': ' + (decimal)ar.get('Amount'));
//			}
//
//
//			integer allocationsCount = 0;
//			integer newServiceAllocationsCount = 0;
//			integer additionalNewServiceAllocationsCount = 0;
//			integer increaseExistingServiceAllocationsCount = 0;
//			for(IF_Personal_Budget__c pb : [SELECT	Id,
//													Name,
//													Service_Type__c,
//													Amount__c,
//													Client__c,
//													Client__r.CCMS_ID__c,
//													RCR_Service_Order__r.Name,
//													RCR_Service_Order__r.Client_Name__c,
//													RCR_Service_Order__r.Allocation_Approved_Date__c,
//													RCR_Service_Order__r.New_Request_Type__c,
//													Financial_Year__r.Name
//											FROM	IF_Personal_Budget__c
//											WHERE	RCR_Service_Order__r.Allocation_Approved_Date__c >= :startDate
//											AND		RCR_Service_Order__r.Allocation_Approved_Date__c <= :endDate
//											ORDER BY Amount__c desc
//											LIMIT 10])
//			{
//				if(allocationsCount <= 10)
//				{
//					Allocations.add(new AllocationWrapper(pb, ServiceTypeCostByClientIDAndServiceType.get(pb.Client__c + pb.Service_Type__c), AllocationsByClientIDAndServiceType.get(pb.Client__c + pb.Service_Type__c + pb.Financial_Year__r.Name)));
//					allocationsCount++;
//				}
//
//				if(ByRequestType)
//				{
//					if(newServiceAllocationsCount <= 10 && pb.RCR_Service_Order__r.New_Request_Type__c == 'New Service')
//					{
//						NewServicesAllocations.add(new AllocationWrapper(pb, ServiceTypeCostByClientIDAndServiceType.get(pb.Client__c + pb.Service_Type__c), AllocationsByClientIDAndServiceType.get(pb.Client__c + pb.Service_Type__c + pb.Financial_Year__r.Name)));
//						newServiceAllocationsCount++;
//					}
//					if(additionalNewServiceAllocationsCount <= 10 && pb.RCR_Service_Order__r.New_Request_Type__c == 'Additional New Service')
//					{
//						AdditionalNewServiceAllocations.add(new AllocationWrapper(pb, ServiceTypeCostByClientIDAndServiceType.get(pb.Client__c + pb.Service_Type__c), AllocationsByClientIDAndServiceType.get(pb.Client__c + pb.Service_Type__c + pb.Financial_Year__r.Name)));
//						additionalNewServiceAllocationsCount++;
//					}
//					if(increaseExistingServiceAllocationsCount <= 10 && pb.RCR_Service_Order__r.New_Request_Type__c == 'Increase to Existing Service')
//					{
//						IncreaseExistingServiceAllocations.add(new AllocationWrapper(pb, ServiceTypeCostByClientIDAndServiceType.get(pb.Client__c + pb.Service_Type__c), AllocationsByClientIDAndServiceType.get(pb.Client__c + pb.Service_Type__c + pb.Financial_Year__r.Name)));
//						increaseExistingServiceAllocationsCount++;
//					}
//				}
//			}
//		}
//		catch(exception ex)
//		{
//			A2HCException.formatException(ex);
//		}
//	}
//
//
//	private void calculateServiceTypeCost(sObject obj, Map<string, decimal> ServiceTypeCostByClientIDAndServiceType)
//	{
//		if(obj.getsObjectType() == RCR_Contract_Scheduled_Service__c.sObjectType)
//		{
//			RCR_Contract_Scheduled_Service__c css = (RCR_Contract_Scheduled_Service__c)obj;
//			for(RCR_Contract_Service_Type__c st : css.RCR_Contract_Service_Types__r)
//			{
//				if(!ServiceTypeCostByClientIDAndServiceType.containsKey(css.RCR_Contract__r.Client__c + st.Allocation_Service_Type__c))
//				{
//					ServiceTypeCostByClientIDAndServiceType.put(css.RCR_Contract__r.Client__c + st.Allocation_Service_Type__c, 0.0);
//				}
//				if(css.Total_Quantity__c != 0)
//				{
//					// Get the current value and add the
//					decimal total = ServiceTypeCostByClientIDAndServiceType.get(css.RCR_Contract__r.Client__c + st.Allocation_Service_Type__c) + css.Total_Cost__c * (st.Quantity__c / css.Total_Quantity__c);
//					// Overwrite new Total.
//					ServiceTypeCostByClientIDAndServiceType.put(css.RCR_Contract__r.Client__c + st.Allocation_Service_Type__c, total);
//				}
//			}
//		}
//		else if(obj.getsObjectType() == RCR_Contract_AdHoc_Service__c.sObjectType)
//		{
//			RCR_Contract_AdHoc_Service__c ahs = (RCR_Contract_AdHoc_Service__c)obj;
//			for(RCR_Contract_Service_Type__c st : ahs.RCR_Contract_Service_Types__r)
//			{
//				if(!ServiceTypeCostByClientIDAndServiceType.containsKey(ahs.RCR_Contract__r.Client__c + st.Allocation_Service_Type__c))
//				{
//					ServiceTypeCostByClientIDAndServiceType.put(ahs.RCR_Contract__r.Client__c + st.Allocation_Service_Type__c, 0.0);
//				}
//				decimal total = ServiceTypeCostByClientIDAndServiceType.get(ahs.RCR_Contract__r.Client__c + st.Allocation_Service_Type__c) + ahs.Amount__c / ahs.RCR_Contract_Service_Types__r.size();
//				// Overwrite new Total.
//				system.debug((ahs.RCR_Contract__r.Client__c + st.Allocation_Service_Type__c) + ' OLD TOTAL: ' + ServiceTypeCostByClientIDAndServiceType.get(ahs.RCR_Contract__r.Client__c + st.Allocation_Service_Type__c) + ' Overwrite NEW Total: ' + ' ' + total);
//				ServiceTypeCostByClientIDAndServiceType.put(ahs.RCR_Contract__r.Client__c + st.Allocation_Service_Type__c, total);
//			}
//		}
//	}
//
//
//	public class AllocationWrapper
//	{
//		public AllocationWrapper(IF_Personal_Budget__c pAllocation, decimal pTotalServiceOrderCosts, decimal pTotalAllocations)
//		{
//			Allocation = pAllocation;
//			TotalAllocations = pTotalAllocations;
//			TotalServiceOrderCosts = pTotalServiceOrderCosts != null ? pTotalServiceOrderCosts : 0.0;
//		}
//
//		public IF_Personal_Budget__c Allocation
//		{
//			get;
//			private set;
//		}
//
//		public decimal TotalAllocations
//		{
//			get;
//			private set;
//		}
//
//		public decimal TotalServiceOrderCosts
//		{
//			get;
//			private set;
//		}
//	}
}