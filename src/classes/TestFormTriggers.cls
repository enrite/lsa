@isTest
public class TestFormTriggers
{                    
    static testMethod void testTrgAttachmentBeforeUD()
    {
        TestLoadFormData data = new TestLoadFormData();
                                           
        System.runAs(data.PortalUser)
        {
            Form__c f = new Form__c();
            insert f;
        
            Attachment a = new Attachment();
            a.ParentId = f.Id;
            a.Name = 'test';
            a.Description = 'SIGNATURE:';
            a.Body = Blob.valueOf('test');
            
            insert a;
            
            try
            {
                update a;
            }
            catch (Exception e) {}
            
            try
            {
                delete a;
            }
            catch (Exception e) {}
        }             
    }
    
    static testMethod void testTrgFormDetailAfterIU()
    {
        TestLoadFormData data = new TestLoadFormData();
        
        Form__c f = new Form__c(Client__c = data.Client.Id,
                                Hospital_Facility__c = data.ServiceProvider.Id);
        insert f;                                   

        RCR_Office_Team__c ot = new RCR_Office_Team__c(Name = 'LSA');
        insert ot;
                        
        Form_Detail__c asd = new Form_Detail__c(Form_Application_Assessment__c = f.Id,
                                                RecordTypeId = data.RecordTypes.get('Manager_ASD_Application_Assessment').Id,                                                
                                                Application_Consistent_with_LSS_Rules__c = true,
                                                Legal_Tests__c = true,
                                                Medical_Tests__c = true);
        insert asd;
        asd.Recommended_to_Chief_Executive_CE__c = true;
        update asd;
        
        Form_Detail__c ce = new Form_Detail__c(Form_Application_Assessment__c = f.Id,
                                               RecordTypeId = data.RecordTypes.get('Chief_Executive_Application_Assessment').Id);
        insert ce;
               
        ce.Status__c = 'Referred back to Manager ASD';              
        update ce;        
        
        ce.Status__c = 'Referred back to Service Planner';             
        update ce;   
        
        ce.Status__c = 'Accepted';  
        ce.Application_Rules_Comm_Chair_Concurs__c = true;           
        update ce;   
    }
    
    static testMethod void testTrgFormDetailBeforeI()
    {
        TestLoadFormData data = new TestLoadFormData();
        
        Form__c f = new Form__c(Client__c = data.Client.Id);
        insert f;
        
        insert new Form_Detail__c(Form_Motor_Vehicle__c = f.Id, RecordTypeId = data.RecordTypes.get('Motor_Vehicle').Id);
        insert new Form_Detail__c(Form_Witness__c = f.Id, RecordTypeId = data.RecordTypes.get('Witness').Id);
        insert new Form_Detail__c(Form_Application_Assessment__c = f.Id, RecordTypeId = data.RecordTypes.get('Chief_Executive_Application_Assessment').Id);
    }
   
    static testMethod void testTrgCANSLevelAfterIU()
    {
        TestLoadFormData data = new TestLoadFormData();
    
        CANS_Level__c cl = new CANS_Level__c();
        cl.Client__c = data.Client.Id;
        insert cl;
    }
    
    static testMethod void testTrgParticipantContactBeforeIU()
    {
        TestLoadFormData data = new TestLoadFormData();
        
        Plan__c p = new Plan__c();
        p.Client__c = data.Client.Id;
        insert p;
        
        Participant_Contact__c pc = new Participant_Contact__c();
        pc.Participant_Plan__c = p.Id;
        pc.RecordTypeId = data.RecordTypes.get('Consulting_Practitioner').Id;
        pc.Health_Facility__c = data.ServiceProvider.Id;
        insert pc;
    }
    
    static testMethod void testTrgPlanActionBeforeIU()
    {
        TestLoadFormData data = new TestLoadFormData();
        
        Plan__c p = new Plan__c();
        p.Client__c = data.Client.Id;
        insert p;
        
        Plan_Action__c pa = new Plan_Action__c();
        pa.Participant_Plan__c = p.Id;  
        insert pa;      
    }
    
    static testMethod void testTrgPlanActionAfterUD()
    {
        TestLoadFormData data = new TestLoadFormData();
        
        Plan__c p = new Plan__c();
        p.Client__c = data.Client.Id;
        insert p;
        
        Plan_Action__c pa = new Plan_Action__c();
        pa.Participant_Plan__c = p.Id;   
        insert pa;
        update pa;     
    }
    
    static testMethod void testTrgParticipantPlanBeforeU()
    {
        TestLoadFormData data = new TestLoadFormData();
        
        Plan__c p = new Plan__c();
        p.Client__c = data.Client.Id;
        insert p;
        update p;
    }
    
    static testMethod void testTrgParticipantPlanAfterUD()
    {
        TestLoadFormData data = new TestLoadFormData();
        
        Plan__c p = new Plan__c();
        p.Client__c = data.Client.Id;
        insert p;
        update p;
    }

    static testMethod void testTrgPlanApprovalBeforeU_1()
    {
        TestLoadFormData data = new TestLoadFormData();

        Plan__c p = new Plan__c();
        p.Client__c = data.Client.Id;

        insert p;

        Plan_Approval__c pa = new Plan_Approval__c();
        pa.Participant_Plan__c = p.Id;
        pa.RecordTypeId = data.RecordTypes.get('Service_Planner_Recommendation').Id;
        pa.Plan_Update_Required__c = 'Yes';
        insert pa;

        Plan_Action__c act = new Plan_Action__c();
        act.Participant_Plan__c = p.Id;
        insert act;

        pa.Service_Planner_Complete__c = true;
        update pa;
        pa.Manager_ASD_Complete__c = 'Referred back to Service Planner';
        update pa;

        pa.Service_Planner_Complete__c = true;
        update pa;
        pa.Manager_ASD_Complete__c = 'Recommended to Chief Executive';
        update pa;
        pa.Chief_Executive_Decision__c = 'Referred back to Service Planner';
        update pa;

        /*pa.Service_Planner_Complete__c = true;
        update pa;
        pa.Manager_ASD_Complete__c = 'Recommended to Chief Executive';
        update pa;
        pa.Chief_Executive_Decision__c = 'Referred back to Manager ASD';
        update pa;

        pa.Manager_ASD_Complete__c = 'Recommended to Chief Executive';
        update pa;
        pa.Chief_Executive_Decision__c = 'Not Approved';
        update pa;
        pa.Chief_Executive_Decision__c = 'Approved';
        update pa;*/

    }

    static testMethod void testTrgPlanApprovalBeforeU_2()
    {
        TestLoadFormData data = new TestLoadFormData();

        Plan__c p = new Plan__c();
        p.Client__c = data.Client.Id;

        insert p;

        Plan_Approval__c pa = new Plan_Approval__c();
        pa.Participant_Plan__c = p.Id;
        pa.RecordTypeId = data.RecordTypes.get('Service_Planner_Recommendation').Id;
        pa.Plan_Update_Required__c = 'Yes';
        insert pa;

        Plan_Action__c act = new Plan_Action__c();
        act.Participant_Plan__c = p.Id;
        insert act;

        pa.Service_Planner_Complete__c = true;
        update pa;
        pa.Manager_ASD_Complete__c = 'Recommended to Chief Executive';
        update pa;
        pa.Chief_Executive_Decision__c = 'Referred back to Manager ASD';
        update pa;

        pa.Manager_ASD_Complete__c = 'Recommended to Chief Executive';
        update pa;
        pa.Chief_Executive_Decision__c = 'Not Approved';
        update pa;
        pa.Chief_Executive_Decision__c = 'Approved';
        update pa;
    }
    
    static testMethod void testTrgTaskAfterU ()
    {
        TestLoadFormData data = new TestLoadFormData();
        
        Task t = [SELECT Id,
                         Status
                  FROM Task
                  WHERE LSA_Task_Type__c = 'Request Vehicle Collision Report'
                  LIMIT 1];
                  
        t.Status = 'Completed';
        update t;                      
    }  
    
    static testMethod void testTrgLocationHistoryAfterIU()
    {
        TestLoadFormData data = new TestLoadFormData();
        
        Location_History__c lh = new Location_History__c();
        lh.Client__c = data.Client.Id;
        lh.Update_Current_Hospital_Facility__c = true;
        insert lh;
    }
}