public class ServiceOrderTrigger
    extends TriggerBase
{
    public ServiceOrderTrigger(Map<ID, RCR_Contract__c> pTriggerNewMap,
                            Map<ID, RCR_Contract__c> pTriggerOldMap)
    {
        super(pTriggerNewMap, pTriggerOldMap);
    }

    public ServiceOrderTrigger(Map<ID, RCR_Contract__c> pTriggerNewMap)
    {
        super(pTriggerNewMap);
    }

//    private AggregateResult[] adhocResults;
    private AggregateResult[] cssResults;
    private Set<Id> accountIDs = new Set<Id>();
    private Set<Id> approvedSoIDs = new Set<Id>();
    private EmailManager emailMgr = null;
    @TestVisible
    private Map<String, List<User>> serviceProviderUsersByAccountID = new Map<string, List<User>>();
    @TestVisible

    private Map<ID, RCR_Contract__c> TriggerNewMap
    {
        get
        {
            return Trigger.newMap == null ? null : (Map<ID, RCR_Contract__c>)Trigger.newMap;
        }
    }

    private Map<ID, RCR_Contract__c> TriggerOldMap
    {
        get
        {
            return Trigger.oldMap == null ? null : (Map<ID, RCR_Contract__c>)Trigger.oldMap;
        }
    }

    private List<GroupMember> ServicePlannerGroupMembers
    {
        get
        {
            if(ServicePlannerGroupMembers == null)
            {
                ServicePlannerGroupMembers = [SELECT ID,
                                                    UserOrGroupID
                                            FROM    GroupMember
                                            WHERE   Group.Developername = 'Service_Planner'];
            }
            return ServicePlannerGroupMembers;
        }
        set;
    }

    public void afterUpdate()
    {
        if(System.isBatch() || System.isFuture() || TriggerBypass.RCRServiceOrder)
        {
            // batch and future can handle there own validations
            return;
        }
        Map<Id, Plan_Action__c> planActionsToUpdate = new Map<Id, Plan_Action__c>();
        Set<RCR_Group_Agreement__c> agreementsToUpdate = new Set<RCR_Group_Agreement__c>();
        List<Task> contractReviewTasks = new List<Task>();
        for (RCR_Contract__c so : (List<RCR_Contract__c>)Trigger.new)
        {
            if(so.RCR_Group_Agreement__c != null)
            {
                agreementsToUpdate.add(new RCR_Group_Agreement__c(ID = so.RCR_Group_Agreement__c));
            }

            RCR_Contract__c oldValue = (RCR_Contract__c)Trigger.oldMap.get(so.ID);
            if(so.Plan_Action__c != null && !planActionsToUpdate.containsKey(so.Plan_Action__c) &&  
                (oldValue.Total_Service_Order_Cost__c != so.Total_Service_Order_Cost__c) || (oldValue.Plan_Action__c != so.Plan_Action__c))
            {
                if(oldValue.Plan_Action__c != so.Plan_Action__c && oldValue.Plan_Action__c != null)
                {
                    planActionsToUpdate.put(oldValue.Plan_Action__c, new Plan_Action__c(ID = oldValue.Plan_Action__c));
                }
                if(so.Plan_Action__c != null)
                {
                    planActionsToUpdate.put(so.Plan_Action__c, new Plan_Action__c(ID = so.Plan_Action__c));
                }
            }

            if(oldValue.Status__c != so.Status__c)
            {
                if(so.Status__c == APS_PicklistValues.RCRContract_Status_Approved)
                {
                    if(so.Plan_Action__c != null && !planActionsToUpdate.containsKey(so.Plan_Action__c))
                    {
                        planActionsToUpdate.put(so.Plan_Action__c, new Plan_Action__c(ID = so.Plan_Action__c));
                    }
                    if(so.RCR_Cancellation_Date__c == null)
                    {
                        createNewServiceOrder(so, oldValue);
                    }
                    else if(so.RecordTypeId == A2HCUtilities.getRecordType('RCR_Contract__c', APSRecordTypes.RCRServiceOrder_ServiceOrder).ID)
                    {
                        // recalculate service order cost
                        ServiceOrderFuture.recalculateServiceOrderCost(so.ID);                       
                    }
                }
                else if(so.Status__c == APS_PicklistValues.RCRContract_Status_AllocationApproved ||
                        so.Status__c == APS_PicklistValues.RCRContract_Status_AllocationRejected)
                {
                    approvedSoIDs.add(so.ID);
                }
            }
            createContractReviewTasks(so, TriggerOldMap.get(so.ID), contractReviewTasks);
        }
        // fire trigger to update service order totals on this object
        update new List<RCR_Group_Agreement__c>(agreementsToUpdate);

        update planActionsToUpdate.values();
    }

    public void beforeInsertUpdate()
    {
        if(System.isBatch() || System.isFuture() || TriggerBypass.RCRServiceOrder)
        {
            // batch and future can handle there own validations
            return;
        }

        Set<String> soNames = new Set<String>();
        Set<ID> clientIds = new Set<ID>();
        Set<ID> planActionIds = new Set<ID>();

        for(RCR_Contract__c so : (List<RCR_Contract__c>)Trigger.new)
        {
            if(so.RecordTypeId == A2HCUtilities.getRecordType('RCR_Contract__c', APSRecordTypes.RCRServiceOrder_Rollover).ID &&
                    so.Destination_Record__c != null)
            {
                so.addError('Rollover Requests cannot be edited after destination Service Order has been created.');
            }
            if(Trigger.isUpdate &&
                    so.CSA_Status__c == APS_PicklistValues.RCRContractCSAStatus_Pending_Acceptance &&
                    ((RCR_Contract__c)Trigger.oldMap.get(so.ID)).CSA_Status__c == APS_PicklistValues.RCRContractCSAStatus_Pending_Approval)
            {
                accountIDs.add(so.Account__c);
            }
            soNames.add(so.Name);
            if(so.Client__c != null)
            {
                clientIds.add(so.Client__c);
            }
            if(so.Plan_Action__c != null)
            {
                planActionIds.add(so.Plan_Action__c);
            }
        }

        Map<String, List<RCR_Contract__c>> otherServiceOrdersByName = new Map<String, List<RCR_Contract__c>>();
        system.debug(soNames);
        for(RCR_Contract__c so : [SELECT    ID,
                                            Name,
                                            Start_Date__c,
                                            End_Date__c,
                                            Account__c,
                                            (SELECT ID
                                            FROM RCR_Invoice_Items__r
                                            WHERE RecordType.NAme = 'Submitted')
                                    FROM    RCR_Contract__c
                                    WHERE   Name IN :soNames AND
                                            (RecordType.Name = :APSRecordTypes.RCRServiceOrder_ServiceOrderCBMS OR
                                            RecordType.Name = :APSRecordTypes.RCRServiceOrder_ServiceOrder)])
        {
            if(!otherServiceOrdersByName.containsKey(so.Name))
            {
                otherServiceOrdersByName.put(so.Name, new List<RCR_Contract__c>());
            }
            otherServiceOrdersByName.get(so.Name).add(so);
        }

//        Map<ID, Referred_Client__c> clientsById = new Map<ID, Referred_Client__c>(
//                        [SELECT     ID,
//                                    Allocated_to__c,
//                                    CCMS_ID__c,
//                                    Account__r.API_Name__c,
//                                    IF_AssignedFacilitator__r.Email
//                        FROM        Referred_Client__c
//                        WHERE       ID IN :clientIds]);

        if(Trigger.isUpdate)
        {
            if(accountIDs.size() > 0)
            {
                for(User u : [SELECT Name,
                                     Email,
                                     Contact.AccountId,
                                     Contact.RecordType.Name
                              FROM   User u
                              WHERE  Authorised_To_Accept_CSA__c = true
                              AND    isActive = true
                              AND    Contact.AccountId IN :accountIDs
                              AND    Contact.RecordType.Name = :APSRecordTypes.Contact_ServiceProvider])
                {
                    if(!serviceProviderUsersByAccountID.containsKey(u.Contact.AccountId))
                    {
                        serviceProviderUsersByAccountID.put(u.Contact.AccountId, new List<User>());
                    }
                    serviceProviderUsersByAccountID.get(u.Contact.AccountId).add(u);
                }
            }

//            adhocResults = [SELECT  MIN(Start_Date__c)  Min_Start_Date,
//                                    MAX(Original_End_Date__c)    Max_End_Date,
//                                    RCR_Contract__c
//                            FROM    RCR_Contract_Adhoc_Service__c
//                            WHERE   RCR_Contract__c IN :Trigger.new
//                            GROUP BY RCR_Contract__c];

            cssResults = [SELECT    MIN(Start_Date__c)  Min_Start_Date,
                                    MAX(Original_End_Date__c)    Max_End_Date,
                                    RCR_Contract__c
                            FROM    RCR_Contract_Scheduled_Service__c
                            WHERE   RCR_Contract__c IN :Trigger.new
                            GROUP BY RCR_Contract__c];
        }

        for (RCR_Contract__c so : (List<RCR_Contract__c>)Trigger.new)
        {
            RCR_Contract__c oldSo = null;
            if(Trigger.isUpdate)
            {
                oldSo = triggerOldMap.get(so.Id);
            }

            if(Trigger.isInsert ||
                (Trigger.isUpdate && so.Name != oldSo.Name))
            {
                //RecordType amendmentRecordType = A2HCUtilities.getRecordType('RCR_Contract__c', APSRecordTypes.RCRServiceOrder_Amendment);
                ID amendmentRecordTypeId = RCR_Contract__c.SObjectType.getDescribe().getRecordTypeInfosByDeveloperName().get('Amendment').getRecordTypeId();
                if(otherServiceOrdersByName.containsKey(so.Name))
                {
                    if(so.RecordTypeId == amendmentRecordTypeId)
                    {
                        for(RCR_Contract__c otherSo : otherServiceOrdersByName.get(so.Name))
                        {
                            if(otherSo.Account__c != so.Account__c)
                            {
                                so.addError('Service Provider cannot be changed for amendments.');
                                break;
                            }
                            if(!otherSo.RCR_Invoice_Items__r.isEmpty() &&
                                    otherSo.Start_Date__c < so.Start_Date__c)
                            {
                                so.addError('Start Date cannot be amended once activity statement items have been reconciled against the original service order.');
                                break;
                            }
                        }
                    }
                    else
                    {
                        for(RCR_Contract__c otherSo : otherServiceOrdersByName.get(so.Name))
                        {
                            if(otherSo.ID != so.ID &&
                                    A2HCUtilities.dateRangesOverlap(otherSo.Start_Date__c, otherSo.End_Date__c, so.Start_Date__c, so.End_Date__c))
                            {
                                so.addError('The date range overlaps another Service Order with the same Service Order Number.');
                                break;
                            }

                        }
                    }
                }
            }
            if(Trigger.isUpdate)
            {
                if(so.Status__c != oldSo.Status__c &&
                        (so.Status__c == APS_PicklistValues.RCRContract_Status_PendingApproval ||
                        so.Status__c == APS_PicklistValues.RCRContract_Status_AllocationPendingApproval))
                {
                    so.Status_Recall__c = oldSo.Status__c;
                }
                if(so.CSA_Status__c != oldSo.CSA_Status__c &&
                        so.CSA_Status__c == APS_PicklistValues.RCRContractCSAStatus_Pending_Approval)
                {
                    so.CSA_Status_Recall__c = oldSo.CSA_Status__c;
                }
                doUpdateValidation(so, oldSo, cssResults);
                if(so.RecordTypeId == A2HCUtilities.getRecordType('RCR_Contract__c', APSRecordTypes.RCRServiceOrder_ServiceOrder).ID ||
                            so.RecordTypeId == A2HCUtilities.getRecordType('RCR_Contract__c', APSRecordTypes.RCRServiceOrder_ServiceOrderCBMS).ID)
                {
                    doCSAApproval(so, oldSo);
                }

//                if(oldSo.Status__c != APS_PicklistValues.RCRContract_Status_New &&
//                    so.Status__c == APS_PicklistValues.RCRContract_Status_New
//                    && so.OwnerId != so.CreatedById)
//                {
//                    so.OwnerId = so.CreatedById;
//                }
            }
            //setClient(so, clientsById);

//            if(so.RecordTypeId == A2HCUtilities.getRecordType('RCR_Contract__c', APSRecordTypes.RCRServiceOrder_NewRequest).ID
//                && so.Is_IF__c == true)
//            {
//                if(clientsById.containsKey(so.Client__c))
//                {
//                    Referred_Client__c cl = clientsById.get(so.Client__c);
//                    so.IF_Facilitator_Email__c = cl.IF_AssignedFacilitator__r.Email;
//                }
//            }
        }

        if(emailMgr != null)
        {
            emailMgr.sendMessages();
        }
    }

    public void beforeDelete()
    {
//        ID amendmentRecordTypeID = RCR_Contract__c.SObjectType.getDescribe().getRecordTypeInfosByDeveloperName().get(APSRecordTypes.RCRServiceOrder_Amendment).getRecordTypeId();
//        Set<ID> serviceOrderIdsToDeleteContractTypes = Trigger.oldMap.keySet().clone();
//        List<RCR_Contract__c> soToUpdate = new List<RCR_Contract__c>();
//        for(RCR_Contract__c so : (List<RCR_Contract__c>)Trigger.old)
//        {
//            if(so.RecordTypeId == amendmentRecordTypeID &&
//                    so.Source_Record__c != null &&
//                    so.Plan_Action__c != null)
//            {
//                RCR_Contract__c serviceOrder = new RCR_Contract__c(ID = so.Source_Record__c);
//                serviceOrder.Plan_Action__c = so.Plan_Action__c;
//                soToUpdate.add(serviceOrder);
//            }
//        }
//
//        TriggerBypass.RCRServiceOrder = true;
//        update soToUpdate;
//        TriggerBypass.RCRServiceOrder = false;
//
//        delete[SELECT   ID
//                FROM    RCR_Contract_Service_Type__c
//                WHERE   RCR_Service_Order__c IN :TriggerOldMap.keySet() OR
//                        RCR_Contract_Scheduled_Service__r.RCR_Contract__c IN :serviceOrderIdsToDeleteContractTypes OR
//                        RCR_Contract_Adhoc_Service__r.RCR_Contract__c  IN :serviceOrderIdsToDeleteContractTypes];
    }

    public void afterDelete()
    {
        ID amendmentRecordTypeID = RCR_Contract__c.SObjectType.getDescribe().getRecordTypeInfosByDeveloperName().get(APSRecordTypes.RCRServiceOrder_Amendment).getRecordTypeId();
        Map<Id, Plan_Action__c> planActionsToUpdate = new Map<Id, Plan_Action__c>();
        List<RCR_Contract__c> soToUpdate = new List<RCR_Contract__c>();
        Set<ID> serviceOrderIdsToDeleteContractTypes = Trigger.oldMap.keySet().clone();

        for(RCR_Contract__c so : (List<RCR_Contract__c>)Trigger.old)
        {
            if(so.RecordTypeId == amendmentRecordTypeID)
            {
                if(so.Source_Record__c != null &&
                        so.Plan_Action__c != null)
                {
                    RCR_Contract__c serviceOrder = new RCR_Contract__c(ID = so.Source_Record__c);
                    serviceOrder.Plan_Action__c = so.Plan_Action__c;
                    soToUpdate.add(serviceOrder);
                }
            }
            else if(so.Plan_Action__c != null && !planActionsToUpdate.containsKey(so.Plan_Action__c))
            {
                planActionsToUpdate.put(so.Plan_Action__c, new Plan_Action__c(ID = so.Plan_Action__c));
            }
        }

        TriggerBypass.RCRServiceOrder = true;
        update soToUpdate;
        TriggerBypass.RCRServiceOrder = false;

        TriggerBypass.PlanAction = true;
        update planActionsToUpdate.values();
        TriggerBypass.PlanAction = false;

        delete[SELECT   ID
                FROM    RCR_Contract_Service_Type__c
                WHERE   RCR_Service_Order__c IN :TriggerOldMap.keySet() OR
                        RCR_Contract_Scheduled_Service__r.RCR_Contract__c IN :serviceOrderIdsToDeleteContractTypes OR
                        RCR_Contract_Adhoc_Service__r.RCR_Contract__c  IN :serviceOrderIdsToDeleteContractTypes];
    }

    public static void createNewServiceOrder(RCR_Contract__c so)
    {
        createNewServiceOrder(so, null);
    }

    public static void createNewServiceOrder(RCR_Contract__c so, RCR_Contract__c oldValue)
    {system.debug('debug1:' + so); system.debug('debug2:' + so.RecordTypeId); system.debug('debug3:' + A2HCUtilities.getRecordType('RCR_Contract__c', APSRecordTypes.RCRServiceOrder_NewRequest).ID);
        // following ServiceOrderFuture calls are future as we need to update the approved record which is still locked
        // as part of the approval process
        if(so.RecordTypeId == A2HCUtilities.getRecordType('RCR_Contract__c', APSRecordTypes.RCRServiceOrder_Amendment).ID)
        {
            ServiceOrderFuture.mergeAmendment(so.ID);
        }
        else if(so.RecordTypeId == A2HCUtilities.getRecordType('RCR_Contract__c', APSRecordTypes.RCRServiceOrder_NewRequest).ID)
        {
            ServiceOrderFuture.createServiceOrderFromRequest(so.ID);
        }
        else if(so.RecordTypeId == A2HCUtilities.getRecordType('RCR_Contract__c', APSRecordTypes.RCRServiceOrder_Rollover).ID)
        {
            ServiceOrderFuture.createServiceOrderFromRollover(so.ID);
        }
    }

    private void createContractReviewTasks(RCR_Contract__c so, RCR_Contract__c oldSo, List<Task> contractReviewTasks)
    {
        if(so.Original_End_Date__c != oldSo.Original_End_Date__c ||
                so.Cancellation_Date__c != oldSo.Cancellation_Date__c)
        {
            Date dueDate = (so.Cancellation_Date__c == null ? so.Original_End_Date__c : so.Cancellation_Date__c).addDays(-14);
            for(GroupMember gm : ServicePlannerGroupMembers)
            {
                Task t = new Task();
                t.Status = 'Not Started';
                t.Priority = 'Normal';
                t.OwnerId = gm.UserOrGroupID;
                t.WhatId = so.ID;
                t.ActivityDate = dueDate;
                t.Subject = 'Contract expiry review for ' + so.Name;
                contractReviewTasks.add(t);
            }
        }
    }

//    @TestVisible
//    private void setClient(RCR_Contract__c so, Map<ID, Referred_Client__c> clientsById)
//    {
//        if(clientsById.containsKey(so.Client__c))
//        {
//            Referred_Client__c client = clientsById.get(so.Client__c);
//            if(client.CCMS_ID__C == null && client.Account__r.API_Name__c == APS_PicklistValues.Account_DCSI_RCR_Programs)
//            {
//                return;
//            }
//        }
//    }

    @TestVisible
    private void doUpdateValidation(RCR_Contract__c so, RCR_Contract__c oldSo, List<AggregateResult> cssResults)
    {
//        for(AggregateResult result : adhocResults)
//        {
//            if(so.ID != (String)result.get('RCR_Contract__c'))
//            {
//                continue;
//            }
//            if(so.Start_Date__c > (Date)result.get('Min_Start_Date'))
//            {
//                so.addError('Cannot set the Start Date after the Start Date of any associated Scheduled or AdHoc Service.');
//            }
//            else if(so.Original_End_Date__c < (Date)result.get('Max_End_Date'))
//            {
//                so.addError('Cannot set the End Date before the end of any associated Scheduled Service or AdHoc Service End Date.');
//            }
//        }
        for(AggregateResult result : cssResults)
        {
            if(so.ID != (String)result.get('RCR_Contract__c'))
            {
                continue;
            }
            if(so.Start_Date__c > (Date)result.get('Min_Start_Date'))
            {
                so.addError('Cannot set the Start Date after the Start Date of any associated Scheduled or AdHoc Service.');
            }
            else if(so.Original_End_Date__c < (Date)result.get('Max_End_Date'))
            {
                so.addError('Cannot set the End Date before the end of any associated Scheduled Service or AdHoc Service End Date.');
            }
        }
    }

    @TestVisible
    private void doCSAApproval(RCR_Contract__c so , RCR_Contract__c oldValue)
    {
        if(emailMgr == null)
        {
            emailMgr = new EmailManager();
        }
        if(so.Service_Request_Approval_Recalled__c)
        {
            so.Status__c = so.Status_Recall__c;
            so.Service_Request_Approval_Recalled__c = false;
        }
        else if(so.Approval_Request_Recalled__c)
        {
            // If the Service Order was submitted for approval then recalled
            // return the values of the following fields back to what they were
            // before submission.
            so.CSA_Status__c = so.CSA_Status_Recall__c;
            so.Approver_User_ID__c = so.Approver_User_ID_Recall__c;
            so.Approver_User_Name__c = so.Approver_User_Name_Recall__c;
            so.CSA_Acceptance_Date__c = so.CSA_Accepted_On_Recall__c;
            so.Date_Approved__c = so.Date_Approved_Recall__c;
            so.OwnerId = so.Submited_For_Approval_User_ID__c;
            so.Approval_Request_Recalled__c = false;
        }
        else
        {
            // Notifiy Service Providers authorised persons that a Customer Service Agreement requires Acceptance, provided that
            // the user has permission to accept electronically. Alternatively notify the submitter and approver that they will need to contact
            // the Service Provider by phone.
            boolean sendToGenericInbox = so.Submited_For_Approval_User_ID__c == null || !UserUtilities.isUserInGroup('RCR_Notify_Submitter_CSA_Approval', so.Submited_For_Approval_User_ID__c);

            if(oldValue.CSA_Status__c == APS_PicklistValues.RCRContractCSAStatus_Pending_Approval &&
                    so.CSA_Status__c == APS_PicklistValues.RCRContractCSAStatus_Pending_Acceptance)
            {
                if(serviceProviderUsersByAccountID.containsKey(so.Account__c))
                {
                    emailMgr.sendCSAAcceptanceNotifications(serviceProviderUsersByAccountID.get(so.Account__c), so, false);
                    emailMgr.sendSubmitterApprovalGrantedNotification(so, false, sendToGenericInbox);
                }
                else
                {
                    // Notify the Submitter and Approver of the Service Order that there are no Service Provider users able to
                    // accept the CSA electronically.
                    emailMgr.sendNoCSAApproversForAccountNotification(so, false, sendToGenericInbox);
                }
            }
            else if(oldValue.CSA_Status__c == APS_PicklistValues.RCRContractCSAStatus_Pending_Approval &&
                            so.CSA_Status__c == APS_PicklistValues.RCRContractCSAStatus_Rejected)
            {
                emailMgr.sendSubmitterApprovalRejectedNotification(so, false, sendToGenericInbox);
            }
        }

        // If the Service Order has just been Accepted or Rejected by the Service Provider set the ownership of the record back to the Submitter.
        if((oldValue.CSA_Status__c == APS_PicklistValues.RCRContractCSAStatus_Pending_Acceptance ||
                        oldValue.CSA_Status__c == APS_PicklistValues.RCRContractCSAStatus_Pending_Approval) &&
                (so.CSA_Status__c == APS_PicklistValues.RCRContractCSAStatus_Accepted ||
                        so.CSA_Status__c == APS_PicklistValues.RCRContractCSAStatus_Rejected))
        {
            if(so.Submited_For_Approval_User_ID__c != null)
            {
                so.OwnerId = so.Submited_For_Approval_User_ID__c;
            }
        }
    }
}