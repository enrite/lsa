public class RCRProcessPaymentController
{
    public void ProcessPayment()
    {
        if(!Test.isRunningTest())
        {
            // do the actions that were previously done by DCSI's ASP.Net application
            RCRInvoiceWebService.updateBatch(ApexPages.CurrentPage().getParameters().get('batchId'), 1, ApexPages.CurrentPage().getParameters().get('mpFileName'), null);
        }
    }
}