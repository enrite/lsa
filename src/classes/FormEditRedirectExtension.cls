public class FormEditRedirectExtension
{
    public FormEditRedirectExtension(ApexPages.StandardController c)
    {
        record = (Form__c)c.getRecord();
    }

    private Form__c record;
    public Boolean CommunityUserSubmittedForm { get; set; }
    public Boolean SignedForm { get; set; }
    
    public Boolean InCommunity
    {
        get
        {
            return UserInfo.getUserType() == 'CspLitePortal';
        }
    }
    
    public PageReference Redir()
    {
        try
        {
            // community users cannot edit a submitted form
            if (UserInfo.getUserType() == 'CspLitePortal'
                && record.Status__c == 'Submitted')
            {
                CommunityUserSubmittedForm = true;
                return null;
            }
            
            // signed forms cannot be edited unless the user has appropriate permission
            if (record.Form_Signed__c)
            {
                Boolean canEditSignedForms = false;
            
                for (User u : [SELECT Id,
                                      Can_Edit_Signed_Forms__c
                               FROM User
                               WHERE Id = :UserInfo.getUserId()])
                {
                    canEditSignedForms = u.Can_Edit_Signed_Forms__c;
                }
            
                if (!canEditSignedForms)
                {
                    SignedForm = true;
                    return null;
                }
            }                
        
            Map<String, PageReference> pages = new Map<String, PageReference>();
            pages.put('Severe_Injury_Notification', Page.SevereInjuryNotificationEdit);
            pages.put('Medical_Certificate', Page.MedicalCertificateEdit);
            pages.put('Pre_Accident_Information', Page.PreAccidentInformationEdit);
            pages.put('FIM_Score_Sheet', Page.FIMScoreSheetEdit);
            pages.put('FIM_FAM_Score_Sheet', Page.FIMFAMScoreSheetEdit);
            pages.put('Application_Form_from_Other', Page.ApplicationFormEdit);
            pages.put('Application_Form_from_Insurer', Page.ApplicationFormEdit);
            pages.put('Discharge_Rehabilitation_Form', Page.DischargeRehabilitationEdit);
            pages.put('Home_Modification_Preliminary_Scoping', Page.HomeModificationEdit);   
            pages.put('Function_on_Discharge_Estimated', Page.FunctionOnDischargeEdit);     
            pages.put('Function_on_Discharge_Actual', Page.FunctionOnDischargeEdit);        
        
            RecordType rt = [SELECT Name,
                                    DeveloperName
                             FROM RecordType
                             WHERE Id = :record.RecordTypeId
                             OR Id = :ApexPages.CurrentPage().getParameters().get('RecordType')];
                                     
            PageReference pg = pages.get(rt.DeveloperName);
            
            for (String k : ApexPages.CurrentPage().getParameters().keySet())
            {
                if (k != 'save_new' && k != '_CONFIRMATIONTOKEN')
                {
                    pg.getParameters().put(k, ApexPages.CurrentPage().getParameters().get(k));
                }
            }
            
            return pg;
        }
        catch(Exception e)
        {
            return A2HCException.formatException(e);
        }
    }
}