public with sharing class InvoiceTaskManagementController
        extends A2HCPageBase
{
    public InvoiceTaskManagementController()
    {
        init();
    }

    public enum TaskIssue
    {
        ParticipantOnly,
        ServiceOrderNotFound,
        ServiceNotFoundOnAccount,
        GLCodeNotFoundOnAccount,
        NotEnoughFundsOnPlanAction,
        NotEnoughFundsOnServiceOrder,
        ServiceNotOnServiceOrder,
        ActivityOutsideDateRange,
        GLCodeNotFoundOnServiceOrder,
        ServiceQuantityExceeded,
        ActivityOutsideServiceDateRange,
        ScheduledServiceFlexibilty,
        RateNotMatched,
        ReconciliationFailed,
        Rejected,
        ReconciledNoFurtherAction,
        ReconciledPlanActionContingency,
        ReviewReconciliation,
        PlanActionRemaining,
        PlanActionAmendment,
        NoFundsAvailable,
        PreApprovedPlanAction
    }

    public static String getTaskSubject(InvoiceTaskManagementController.TaskIssue issueEnum)
    {
        switch on issueEnum
        {
            when ParticipantOnly
            {
                return Label.NoMatchingServiceOrders; //'Participant only';
            }
            when ServiceOrderNotFound
            {
                return 'Service Order not found';
            }
            when ServiceNotFoundOnAccount
            {
                return 'Service Order not found on account';
            }
            when GLCodeNotFoundOnAccount
            {
                return 'GL Code not found on account';
            }
            when NotEnoughFundsOnPlanAction
            {
                return 'Not enough funds on Plan Action';
            }
            when NotEnoughFundsOnServiceOrder
            {
                return 'Not enough funds on Service Order';
            }
            when ServiceNotOnServiceOrder
            {
                return 'Service not found on Service Order';
            }
            when ActivityOutsideDateRange
            {
                return 'Activity outside date range';
            }
            when GLCodeNotFoundOnServiceOrder
            {
                return 'GL Code not found on Service Order';
            }
            when ServiceQuantityExceeded
            {
                return 'Quantity exceeded for scheduled service';
            }
            when ActivityOutsideServiceDateRange
            {
                return 'Activity outside scheduled service date range';
            }
            when ScheduledServiceFlexibilty
            {
                return Label.ServiceDateRange; //'Scheduled service flexibility needs review';
            }
            when RateNotMatched
            {
                return 'Rate does not match contracted rate';
            }
            when ReconciliationFailed
            {
                return 'Unable to reconcile';
            }
            when ReconciledNoFurtherAction
            {
                return 'Reconciled - No Further Action';
            }
            when ReviewReconciliation
            {
                return 'Reconciled - Review';
            }
            when ReconciledPlanActionContingency
            {
                return 'Reconciled - Used Plan Action Contingency';
            }
            when PlanActionRemaining
            {
                return Label.PlanFundsAvailable; //'Service order requires amendment to use available plan action funds';
            }
            when PlanActionAmendment
            {
                return Label.OtherPlanAction; //'Plan Action funding requires review';
            }
            when NoFundsAvailable
            {
                return 'Not enough funds available on any service order or plan action';
            }
            when PreApprovedPlanAction
            {
                return 'Item belongs to a pre-approved service, plan action and service order need to be reviewed for this claim';
            }
            when else
            {
                return null;
            }
        }
    }

    public ID TaskId
    {
        get;
        set;
    }

    public String ServiceOrderNumber
    {
        get;
        set;
    }

//    public String GLCode
//    {
//        get;
//        set;
//    }

    public String ServiceCode
    {
        get;
        set;
    }

    public String TaskMessage
    {
        get;
        set;
    }

    public Boolean TaskLPMultiReject
    {
        get;
        set;
    }

    public Map<String, List<RCR_Invoice__c>> InvoicesByClient
    {
        get;
        set;
    }

    public Map<String, Map<ID, List<InvoiceItemWrapper>>> TasksByClientAndInvoiceID
    {
        get;
        set;
    }

    public Boolean RejectSuccess
    {
        get;
        set;
    }

    private void init()
    {
        Map<String, Set<RCR_Invoice__c>> m_InvoicesByClient = new Map<String, Set<RCR_Invoice__c>>();
        InvoicesByClient = new Map<String, List<RCR_Invoice__c>>();
        TasksByClientAndInvoiceID = new Map<String, Map<ID, List<InvoiceItemWrapper>>>();
        Set<ID> invoiceItemIds = new Set<ID>();
        Map<ID, List<Task>> tasksByItemID = new Map<ID, List<Task>>();
        Map<String, RCR_Service__c> servicesByCode = new Map<String, RCR_Service__c>();

        for(Task t : [SELECT  ID,
                                WhatId,
                                Adjust_Service_On_Approval__c,
                                Subject,
                                RCR_Service_Order__c,
                                RCR_Contract_Scheduled_Service__c,
                                Plan_Action__c
                        FROM    Task
                        WHERE   What.Type = :RCR_Invoice_Item__c.sObjectType.getDescribe().getName() AND
                                OwnerId = :UserInfo.getUserId() AND
                                IsClosed = false])
        {
            invoiceItemIds.add(t.WhatId);
            if(!tasksByItemID.containsKey(t.WhatId))
            {
                tasksByItemID.put(t.WhatId, new List<Task>());
            }
            tasksByItemID.get(t.WhatId).add(t);
        }
        List<RCR_Invoice__c> invoices =
        [
                SELECT ID,
                        Name,
                        Invoice_Number__c,
                        Account__c,
                        Account__r.Name,
                        Total__c,
                        Total_ex_GST__c,
                        Date__c,
                        LP_Invoice_Key__c,
                        LP_Invoice_Number__c,
                        (
                            SELECT ID,
                                    Name,
                                    Service_Order_Number__c,
                                    Total_ex_GST__c,
                                    Rate__c,
                                    Quantity__c,
                                    GL_Code__c,
                                    Activity_Date__c,
                                    Code__c,
                                    LP_Claim_Key__c,
                                    LP_Claim_Status__c,
                                    LP_State__c,
                                    Client__c,
                                    Client_Name_As_Loaded__c,
                                    Client__r.Name,
                                    RCR_Invoice__c,
                                    RCR_Invoice__r.All_Line_Count__c,
                                    RCR_Service_Order__c
                            FROM RCR_Invoice_Items__r
                            WHERE ID IN :invoiceItemIds
                        )
                FROM RCR_Invoice__c
                WHERE ID IN
                (
                        SELECT RCR_Invoice__c
                        FROM RCR_Invoice_Item__c
                        WHERE ID IN :invoiceItemIds
                )
                ORDER BY Account__r.Name,
                        Invoice_Number__c,
                        Date__c
        ];
        Set<String> svcCodes = new Set<String>();
        for(RCR_Invoice__c invoice : invoices)
        {
            for(RCR_Invoice_Item__c invItem : invoice.RCR_Invoice_Items__r)
            {
                if(invItem.Code__c != null)
                {
                    svcCodes.add(invItem.Code__c);
                }
            }
        }

        for(RCR_Service__c serv : [ SELECT ID,
                                            Name,
                                            Description__c,
                                            Account__c,
                                            Standard_Service_Code__r.Code__c
                                    FROM    RCR_Service__c
                                    WHERE   Standard_Service_Code__r.Code__c IN :svcCodes])
        {
            servicesByCode.put(serv.Standard_Service_Code__r.Code__c, serv);
        }

        for(RCR_Invoice__c invoice : invoices)
        {
            for(RCR_Invoice_Item__c invItem : invoice.RCR_Invoice_Items__r)
            {
                if(tasksByItemID.containsKey(invItem.ID))
                {
                    String clientName = invItem.Client__c == null ? invItem.Client_Name_As_Loaded__c : invItem.Client__r.Name;
                    clientName = String.isBlank(clientName) ? 'Client not found' : clientName;
                    if(!m_InvoicesByClient.containsKey(clientName))
                    {
                        m_InvoicesByClient.put(clientName, new Set<RCR_Invoice__c>{invoice});
                    }
                    else
                    {
                        if(!m_InvoicesByClient.get(clientName).contains(invoice))
                        {
                            m_InvoicesByClient.get(clientName).add(invoice);
                        }
                    }
                    if(!TasksByClientAndInvoiceID.containsKey(clientName))
                    {
                        TasksByClientAndInvoiceID.put(clientName, new Map<ID, List<InvoiceItemWrapper>>());
                    }
                    List<InvoiceItemWrapper> itemWrappers = new List<InvoiceItemWrapper>();
                    for(Task t : tasksByItemID.get(invItem.ID))
                    {
                        String svcDescription = '';
                        RCR_Service__c svc = servicesByCode.get(invItem.Code__c);
                        if(svc != null)
                        {
                            svcDescription = svc.Description__c;
                        }
                        itemWrappers.add(new InvoiceItemWrapper(invItem, t, svcDescription));
                    }
                    if(!TasksByClientAndInvoiceID.get(clientName).containsKey(invoice.ID))
                    {
                        TasksByClientAndInvoiceID.get(clientName).put(invoice.ID, new List<InvoiceItemWrapper>());
                    }
                    TasksByClientAndInvoiceID.get(clientName).get(invoice.ID).addAll(itemWrappers);
                }
            }
        }

        List<String> clientNames = new List<String>(m_InvoicesByClient.keySet());
        clientNames.sort();
        for(String cl : clientNames)
        {
            Set<RCR_Invoice__c> invoiceSet = m_InvoicesByClient.get(cl);
            if(!invoiceSet.isEmpty())
            {
                InvoicesByClient.put(cl, new List<RCR_Invoice__c>(invoiceSet));
            }
        }
    }

    public PageReference cancel()
    {
        String retURL = getParameter('retURL');
        PageReference pg;
        if(String.isBlank(retURL))
        {
            pg = Page.RCR;
        }
        else
        {
            pg = new PageReference(retURL);
        }
        return pg;
    }

    public void approveTask()
    {
        Savepoint sp = Database.setSavepoint();
        try
        {
            Task t = getTask();
            RCR_Invoice_Item__c item = completeTask(t);
            if(t.Adjust_Service_On_Approval__c)
            {

                Boolean soEndDatesAdjusted = false;
                for(RCR_Contract_Scheduled_Service__c css : [SELECT ID,
                                                                    RCR_Contract__c,
                                                                    RCR_Contract__r.Start_Date__c,
                                                                    RCR_Contract__r.End_Date__c
                                                            FROM    RCR_Contract_Scheduled_Service__c
                                                            WHERE   ID = :t.RCR_Contract_Scheduled_Service__c])
                {
                    if(!A2HCUtilities.isDateBetween(item.Activity_Date__c, css.RCR_Contract__r.Start_Date__c, css.RCR_Contract__r.End_Date__c))
                    {
                        RCR_Contract__c so = new RCR_Contract__c(ID = css.RCR_Contract__c);
                        if(item.Activity_Date__c < css.RCR_Contract__r.Start_Date__c)
                        {
                            so.Start_Date__c = item.Activity_Date__c;
                        }
                        else if(item.Activity_Date__c < css.RCR_Contract__r.End_Date__c)
                        {
                            so.Original_End_Date__c = item.Activity_Date__c;
                        }
                        TriggerBypass.RCRServiceOrder = true;
                        update so;
                        TriggerBypass.RCRServiceOrder = false;
                        soEndDatesAdjusted = true;
                        RCRReconcileInvoice.reconcileNew(item.RCR_Invoice__c);
                    }
                }
                if(!soEndDatesAdjusted)
                {
                    system.debug(item);
                    item.Error__c = null;
                    RCRReconcileBatchNew.adjustScheduledService(item, t);
                    if(item.Error__c != null)
                    {
                        A2HCException.formatExceptionAndRollback(sp, item.Error__c);
                    }
                }
            }
            else
            {
                RCRReconcileInvoice.reconcileNew(item.RCR_Invoice__c);
            }
        }
        catch(Exception ex)
        {
            A2HCException.formatExceptionAndRollback(sp, ex);
        }
    }

    public void reconcileTask()
    {
        Savepoint sp = Database.setSavepoint();
        try
        {
            RCR_Invoice_Item__c item = completeTask();
            RCRReconcileInvoice.reconcileNew(item.RCR_Invoice__c);
        }
        catch(Exception ex)
        {
            A2HCException.formatExceptionAndRollback(sp, ex);
        }
    }

    public PageReference rejectTask()
    {
        try
        {
            RejectSuccess = true;
            if(String.isBlank(TaskMessage))
            {
                A2HCException.formatException('Rejection reason is required');
                RejectSuccess = false;
                return null;
            }
//            RCR_Invoice_Item__c item = completeTask(TaskMessage);
            RCR_Invoice_Item__c item = getInvoiceItemFromTask(getTask());
            if(item != null)
            {
//                item.Error__c = 'Rejected by ' + CurrentUser.FullName__c;
//                item.Rejection_Reason__c = TaskMessage;
//                item.Exceeds_Tolerance__c = false;
//                update item;

                if(item.LP_Claim_Key__c != null)
                {
                    LPRESTInterface.rejectInvoice(item.ID);

                    item.Error__c = 'Rejected by ' + CurrentUser.FullName__c;
                    item.Rejection_Reason__c = TaskMessage;
                    item.Exceeds_Tolerance__c = false;
                    update item;

                    RCRReconcileInvoice.withdrawLPInvoice(item);
                    if(TaskLPMultiReject)
                    {
                        // force page refresh as multiple items will be rejected
                        PageReference pg = Page.InvoiceTaskManagement;
                        for(String param : ApexPages.currentPage().getParameters().keySet())
                        {
                            if(String.isNotBlank(param))
                            {
                                pg.getParameters().put(param, ApexPages.currentPage().getParameters().get(param));
                            }
                        }
                        return pg;
                    }
                }
            }
            return null;
        }
        catch(Exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }

    private RCR_Invoice_Item__c completeTask()
    {
        return completeTaskWithReason(null);
    }

    private RCR_Invoice_Item__c completeTask(String rejectionReason)
    {
        return completeTaskWithReason(rejectionReason);
    }

    private RCR_Invoice_Item__c completeTask(Task t)
    {
        return completeTaskWithReason(null);
    }

    private Task getTask()
    {
        return [SELECT ID,
                        WhatId,
                        Adjust_Service_On_Approval__c,
                        Subject,
                        RCR_Service_Order__c,
                        RCR_Contract_Scheduled_Service__c,
                        Plan_Action__c
                FROM Task
                WHERE ID = :TaskId];
    }
    private RCR_Invoice_Item__c completeTaskWithReason(String rejectionReason)
    {
        Task t = getTask();
        t.Status = 'Completed';
        if(rejectionReason != null)
        {
            t.Rejection_Reason__c = rejectionReason;
        }
        update t;

        return getInvoiceItemFromTask(t);
    }

    private RCR_Invoice_Item__c getInvoiceItemFromTask(Task t)
    {
        for(RCR_Invoice_Item__c item : [SELECT ID,
                                                Name,
                                                Activity_Date__c,
                                                Code__c,
                                                Comment__c,
                                                Error__c,
                                                Quantity__c,
                                                Rate__c,
                                                GST__c,
                                                Total__c,
                                                Total_GST__c,
                                                Total_ex_GST__c,
                                                Exceeds_Tolerance__c,
                                                Override_Tolerance__c,
                                                Service_Order_Number__c,
                                                Unique_Identifier__c,
                                                Period_Start_Date__c,
                                                Period_End_Date__c,
                                                Summary_AdHoc_Item__c,
                                                Write_To_Log__c,
                                                Reconcile_Log__c,
                                                GL_Code__c,
                                                Force_Reconcile_To__c,
                                                LP_Claim_Key__c,
                                                LP_Claim_Status__c,
                                                LP_State__c,
                                                Client__c,
                                                Client__r.Allocated_to__c,
                                                Client__r.Name,
                                                RCR_Contract_Scheduled_Service__c,
                                                RCR_Contract_Scheduled_Service__r.Name,
                                                RCR_Contract_Adhoc_Service__c,
                                                RCR_Invoice__r.Status__c,
                                                RCR_Invoice__r.Account__c,
                                                RCR_Invoice__c,
                                                RCR_Invoice__r.Name,
                                                RCR_Invoice__r.Created_By_Portal_User__c,
                                                RCR_Invoice__r.Override_validation__c,
                                                RCR_Invoice__r.Assigned_To__c,
                                                RCR_Invoice__r.Reconciliation_User__c,
                                                RCR_Service_Order__c
                                        FROM    RCR_Invoice_Item__c
                                        WHERE   ID = :t.WhatID])
        {
            return item;
        }
        return null;
    }

//    private List<RCR_Invoice_Item__c> getAllInvoiceItemsFromItem(RCR_Invoice_Item__c item)
//    {
//        return [SELECT ID,
//                        LP_Claim_Key__c,
//                        Service_Order_Number__c,
//                        Code__c,
//                        RCR_Invoice__c,
//                        RCR_Invoice__r.Account__c,
//                        RCR_Invoice__r.RecordType.DeveloperName
//                FROM    RCR_Invoice_Item__c
//                WHERE   RCR_Invoice__c = :item.RCR_Invoice__c AND
//                        ID != :item.ID];
//    }

    public class InvoiceItemWrapper
    {
        public InvoiceItemWrapper(RCR_Invoice_Item__c invItem, Task t, String svcDescription)
        {
            this.ItemTask = t;
            this.InvoiceItem = invItem;
            LPMulitipleInvoiceItems = invItem.LP_Claim_Key__c != null && invItem.RCR_Invoice__r.All_Line_Count__c > 1;
            ServiceDescription = svcDescription == null ? '' : svcDescription;

            ShowReconcile = t.Adjust_Service_On_Approval__c ||
                            (t.Subject == getTaskSubject(TaskIssue.PlanActionAmendment) && t.Plan_Action__c != null) ||
                            (t.Subject == getTaskSubject(TaskIssue.ScheduledServiceFlexibilty) && t.RCR_Contract_Scheduled_Service__c != null);
            ShowApprove = ShowReconcile ? false : t.Adjust_Service_On_Approval__c || (t.RCR_Service_Order__c != null && t.RCR_Contract_Scheduled_Service__c == null);
            ShowReject = invItem.LP_State__c != 'Authorized';
        }

        public Task ItemTask
        {
            get;
            set;
        }

        public RCR_Invoice_Item__c InvoiceItem
        {
            get;
            set;
        }

        public Plan_Action__c PlanAction
        {
            get;
            set;
        }

        public String ServiceDescription
        {
            get;
            set;
        }

        public Boolean ShowReconcile
        {
            get;
            set;
        }

        public Boolean ShowApprove
        {
            get;
            set;
        }

        public Boolean ShowReject
        {
            get;
            set;
        }

        public Boolean LPMulitipleInvoiceItems
        {
            get;
            set;
        }
    }
}