/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest//(SeeAllData=true)
private class TestRCRInvoiceSearchController 
{
    static testMethod void test1() 
    {
        TestLoadData.loadRCRData();                
    
        Test.startTest();
        RCRInvoiceSearchController ext = new RCRInvoiceSearchController();

        ext.Invoices = null;
        system.debug(ext.Invoices);

        ext.Accounts = null;
        system.debug(ext.Accounts);

        ext.Statuses = null;
        system.debug(ext.Statuses);

        system.debug(ext.SearchCriteria);

        ext.SelectedProgram = null;
        system.debug(ext.SelectedProgram);

        ext.SelectedAccount = null;
        system.debug(ext.SelectedAccount);
        ext.SelectedAccount = 'Adelaide Family Care';
        
        ext.SelectedStatus = null;
        system.debug(ext.SelectedStatus);

        ext.InvoiceNumber = null;
        system.debug(ext.InvoiceNumber);

        ext.FromDate = null;
        system.debug(ext.FromDate);

        ext.ToDate = null;
        system.debug(ext.ToDate);

        ext.FileName = null;
        system.debug(ext.FileName);

        ext.Description = null;
        system.debug(ext.Description);

        ext.IncludeRecon = null;
        system.debug(ext.IncludeRecon);
        ext.IncludeRecon = true;
        system.debug(ext.IncludeRecon);

        system.debug(ext.GridPagerController);

        ext.searchInvoices();

        ext.first();
        ext.last();
        ext.PageNumber = 1;
        ext.goPage();

        ext.newInvoice();

        ext.cancel();

        ext.uploadInvoice();

        Test.setCurrentPage(Page.RCRInvoiceSearch);
        RCR_Invoice__c inv2 = [SELECT ID,
                                        Invoice_Number__c
                                FROM RCR_Invoice__c 
                                LIMIT 1];
        ApexPages.currentPage().getParameters().put('srch', '~~~' + inv2.Invoice_Number__c + '~~~~~Y~');
        ext = new RCRInvoiceSearchController(new ApexPages.StandardSetController([SELECT ID FROM RCR_Invoice__c LIMIT 1]));

        ext.SelectedStatus = APS_PicklistValues.RCRInvoice_Status_Loaded;
        ext.searchInvoices();

        ApexPages.currentPage().getParameters().remove('srch');
        ApexPages.currentPage().getParameters().put('srch', '~~~' + inv2.Invoice_Number__c + '~~');
        ext = new RCRInvoiceSearchController(new ApexPages.StandardController(inv2));
        Test.stopTest();
    }
}