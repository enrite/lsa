public class RCRServiceRateVariationRequestController extends A2HCPageBase	
{
	public RCRServiceRateVariationRequestController(ApexPages.Standardcontroller controller)
	{
		VariationRequest = (RCR_Service_Rate_Variation__c)controller.getRecord();

		getServiceOrdersAndRates();
	}

	@testVisible
	private RCR_Service_Rate_Variation__c VariationRequest;
	
	public string RateVariationID
	{
		get;
		private set;
	} 
	
	public decimal TotalOldCost
	{
		get; 
		private set;
	}
	
	public decimal TotalNewCost
	{
		get;
		private set;
	}
	
	public boolean AuthorisedToApprove
	{
		get
		{
			return isUserInGroup('RCR_Service_Rate_Variations_Approver', UserInfo.getUserId());
		}
	}
	
	public boolean AcceptedByIsReadOnly
	{
		get
		{
			return VariationRequest.Status__c == 'Approved' ? false : true;
		}
	}
	
	public decimal TotalCostVariation
	{
		get
		{
			return TotalNewCost - TotalOldCost;
		}
		private set;
	}
	
	public string DateTimeGenerated
	{
		get
		{
			return Datetime.now().format();
		}
	}
	
	public string DepartmentName
	{
		get
		{
			return Disability_SA__c.getValues('DisabilitySA').Department_Name__c;
		}
	}
	
	public string Building
	{
		get
		{
			return Disability_SA__c.getValues('DisabilitySA').Building__c;
		}
	}
	
	public string StreetAddress
	{
		get
		{
			return Disability_SA__c.getValues('DisabilitySA').Street_Address__c;
		}
	}
	
	public string Suburb
	{
		get
		{
			return Disability_SA__c.getValues('DisabilitySA').Suburb__c;
		}
	}
	
	public string Postcode
	{
		get
		{
			return Disability_SA__c.getValues('DisabilitySA').Postcode__c;
		}
	}
	
	public string PostBox
	{
		get
		{
			return Disability_SA__c.getValues('DisabilitySA').Post_Box__c;
		}
	}
	
	public string Phone
	{
		get
		{
			return Disability_SA__c.getValues('DisabilitySA').Phone__c;
		}
	}
	
	public string Fax
	{
		get
		{
			return Disability_SA__c.getValues('DisabilitySA').Fax__c;
		}
	}
	
	public string ABN
	{
		get
		{
			return Disability_SA__c.getValues('DisabilitySA').ABN__c;
		}
	}
	
	public string DCSIDepartmentName
	{
		get
		{
			return Disability_SA__c.getValues('DCSI').Department_Name__c;
		}
	}
	
	public string DCSIPostbox
	{
		get
		{
			return Disability_SA__c.getValues('DCSI').Post_Box__c;
		}
	}
	
	public string DCSISuburb
	{
		get
		{
			return Disability_SA__c.getValues('DCSI').Suburb__c;
		}
	}
	
	public string DCSIPostBoxPostcode
	{
		get
		{
			return Disability_SA__c.getValues('DCSI').Postcode__c; 
		}
	}
	
	public String MinistersContractManagerName
    {
        get
        {
            return RCR_Client_Service_Agreement__c.getValues('CSA').Minister_s_Contract_Manger__c;
        }
    }
    
    public String MinistersContractManagerPosition
    {
        get
        {
            return RCR_Client_Service_Agreement__c.getValues('CSA').Ministers_Contract_Manager_Position__c;
        }
    }    

	public List<ServiceOrderWrapper> ServiceOrders
	{
		get
		{
			if(ServiceOrders == null)
			{
				ServiceOrders = new List<ServiceOrderWrapper>();
			}
			return ServiceOrders;
		}
		private set;
	}
	
	public List<RCR_Service_Rate__c> Rates
	{
		get
		{
			if(Rates == null) 
			{
				Rates = new List<RCR_Service_Rate__c>();
			}
			return Rates;
		}
		private set;
	}
	
	public Map<id, decimal> OriginalServiceOrderCosts
	{
		get
		{
			if(OriginalServiceOrderCosts == null)
			{
				OriginalServiceOrderCosts = new Map<id, decimal>();
			}
			return OriginalServiceOrderCosts;
		}
		private set;
	}
	
	
	public PageReference submitForApproval()
	{
		SavePoint sp = Database.setSavepoint();
		try
		{
			if(Rates.size() == 0)
			{
				return null;
			}
			
			List<RCR_Service_Rate_Audit__c> auditRecords = new List<RCR_Service_Rate_Audit__c>();
			
			VariationRequest.Status__c = 'Pending Approval';
			VariationRequest.Total_Original_Service_Order_Cost__c = TotalOldCost;
			VariationRequest.Total_New_Service_Order_Value__c = TotalNewCost;
			VariationRequest.Submitted_By__c = UserInfo.getUserId();
			VariationRequest.Date_Submitted__c = Datetime.now();
			upsert VariationRequest;
			
			for(ServiceOrderWrapper soWrap : ServiceOrders)
			{
				for(RCR_Service_Rate_Audit__c auditRec :soWrap.ServiceOrder.RCR_Service_Rate_Audits__r)
				{
					auditRec.RCR_Service_Rate_Variation__c = VariationRequest.Id;
					auditRecords.add(auditRec);
				}
			}
			update auditRecords;
			
			return null;
		}
		catch(Exception ex)
		{
			Database.rollback(sp);
			return A2HCException.formatException(ex);
		}
	}
	
	
	public PageReference approve()
	{
		try
		{
			if(Rates.size() == 0)
			{
				return null;
			}
			
			VariationRequest.Status__c = 'Approved'; 
			VariationRequest.Approved_By__c = UserInfo.getUserId();
			VariationRequest.Date_Approved__c = Datetime.now();
			update VariationRequest;

			return null;
		}
		catch(Exception ex)
		{
			return A2HCException.formatException(ex);
		}
	}
	
	
	public PageReference saveOverride()
	{
		try
		{
			if(Rates.size() == 0)
			{
				return null;
			}
			
			if(VariationRequest.Accepted_By_Text__c != null && VariationRequest.Date_Accepted__c != null)
			{
				VariationRequest.Status__c = 'Accepted';
			}
			
			upsert VariationRequest;
			
			return null;
		}
		catch(Exception ex)
		{
			return A2HCException.formatException(ex);
		}
	}
	
	
	public PageReference cancelOverride()
	{
		try
		{
			PageReference page = new PageReference('/' + VariationRequest.Account__c);
			page.setRedirect(true);
			return page;
		}
		catch(Exception ex)
		{
			return A2HCException.formatException(ex);
		}
	}
	
	@testVisible 
	private void getServiceOrdersAndRates()
	{
		TotalNewCost = 0.0;
		TotalOldCost = 0.0;
system.debug('VariationRequest.Id: ' + VariationRequest.Id);		
		if(VariationRequest.Id == null)
		{
			// New Rate Variation. Get Service Orders where New Service Rate Variation Required flag is set to true.
system.debug('New Rate Variation');			
			Set<id> serviceRateIDs = new Set<id>();
			decimal originalServiceOrderCost = 0.0;
			
			
			for(RCR_Contract__c serviceOrder : [SELECT	Id,
														Name,
														CCMS_ID__c,
														Start_Date__c,
														End_Date__c,
														Total_Service_Order_Cost__c,
														Client__r.Full_Name__c,
														(SELECT Id,
																Name,
																RCR_Service_Rate__c,
																Service_Order_Value_Before__c 
														 FROM	RCR_Service_Rate_Audits__r
														 WHERE	RCR_Service_Rate_Variation__c = null	
														 ORDER BY Name desc)
												 FROM	RCR_Contract__c
											 	 WHERE	(RecordType.Name = :APSRecordTypes.RCRServiceOrder_ServiceOrder OR
											 	 		RecordType.Name = :APSRecordTypes.RCRServiceOrder_ServiceOrderCBMS)
											 	 AND	New_Service_Rate_Variation_Required__c = true
											 	 AND	id in (SELECT RCR_Service_Order__c
														 			FROM   RCR_Service_Rate_Audit__c
														 			WHERE	RCR_Service_Rate_Variation__c = null)])
			{			
				for(RCR_Service_Rate_Audit__c a : serviceOrder.RCR_Service_Rate_Audits__r)
				{
					serviceRateIDs.add(a.RCR_Service_Rate__c);
					originalServiceOrderCost = a.Service_Order_Value_Before__c;
				}

				TotalNewCost += serviceOrder.Total_Service_Order_Cost__c;
				TotalOldCost += originalServiceOrderCost;
				
				ServiceOrders.add(new ServiceOrderWrapper(serviceOrder, TotalOldCost, TotalNewCost));
			}
			
			
			for(RCR_Service_Rate__c rate : [SELECT	id,
													Name,
													Rate__c,
													Public_Holiday_Rate__c,
													RCR_Service__r.Name,
													RCR_Service__r.Description__c
											 FROM	RCR_Service_Rate__c
											 WHERE	Id in :serviceRateIDs])
			{
				Rates.add(rate);
			}
		}
		else
		{
			// Existing Rate Variation.
system.debug('Existing Rate Variation');
		

			Map<Id, RCR_Service_Rate_Audit__c> auditRecordsByServiceOrderID = new Map<Id, RCR_Service_Rate_Audit__c>();	
			Set<Id> serviceRateIDs = new Set<Id>();	
			for(RCR_Service_Rate_Audit__c auditRecord : [SELECT	Id,
																Name,
																RCR_Service_Rate__c,
																RCR_Service_Order__c,
																Service_Order_Value_Before__c,
																RCR_Service_Order__r.Total_Service_Order_Cost__c
														 FROM	RCR_Service_Rate_Audit__c
														 WHERE	RCR_Service_Rate_Variation__c = :VariationRequest.Id	
														 ORDER BY Name desc])
			{
				auditRecordsByServiceOrderID.put(auditRecord.RCR_Service_Order__c, auditRecord);
				serviceRateIDs.add(auditRecord.RCR_Service_Rate__c);
			}
system.debug('Service Order IDs: ' + auditRecordsByServiceOrderID.keySet());			
			for(RCR_Contract__c so : [SELECT 	Id,
												Name,
												CCMS_ID__c,
												Start_Date__c,
												End_Date__c,
												Client__r.Full_Name__c,
												Total_Service_Order_Cost__c
									  FROM		RCR_Contract__c
								  	  WHERE		Id IN :auditRecordsByServiceOrderID.keySet()])
			{
				ServiceOrders.add(new ServiceOrderWrapper(so, auditRecordsByServiceOrderID.get(so.Id).Service_Order_Value_Before__c, auditRecordsByServiceOrderID.get(so.Id).RCR_Service_Order__r.Total_Service_Order_Cost__c));
				TotalNewCost += auditRecordsByServiceOrderID.get(so.Id).RCR_Service_Order__r.Total_Service_Order_Cost__c;
				TotalOldCost += auditRecordsByServiceOrderID.get(so.Id).Service_Order_Value_Before__c;
			}
			
			Set<Id> serviceOrderIDs = new Set<Id>();	

			
			for(RCR_Service_Rate__c rate : [SELECT	id,
													Name,
													Rate__c,
													Public_Holiday_Rate__c,
													RCR_Service__r.Name,
													RCR_Service__r.Description__c
											 FROM	RCR_Service_Rate__c
											 WHERE	Id in :serviceRateIDs])
			{
				Rates.add(rate);
			}
		}
	}
	
	public class ServiceOrderWrapper
	{
		public ServiceOrderWrapper(RCR_Contract__c pServiceOrder, decimal pOldServiceOrderCost, decimal pNewServiceOrderCost) 
		{
			ServiceOrder = pServiceOrder;
			OldServiceOrderCost = pOldServiceOrderCost;
			NewServiceOrderCost = pNewServiceOrderCost;
		}
		
		public RCR_Contract__c ServiceOrder
		{
			get;
			private set;
		} 
		
		public decimal OldServiceOrderCost
		{
			get;
			private set;
		}
		
		public decimal NewServiceOrderCost
		{
			get;
			private set;
		}
		
		public decimal CostVariation
		{
			get
			{
				return NewServiceOrderCost - OldServiceOrderCost;
			}
		}
	}

}