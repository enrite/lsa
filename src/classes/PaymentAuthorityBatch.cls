public class PaymentAuthorityBatch implements Database.Batchable<sObject>,Database.Stateful, Schedulable
{
    public PaymentAuthorityBatch()
    {}

    // allows for an adhoc job run for a service order that has failed in the scheduled job
    public PaymentAuthorityBatch(ID serviceOrderID, Date startDate, Date endDate)
    {
        onceOffServiceOrderID = serviceOrderID;
        periodStartDate = startDate;
        periodEndDate = endDate;
    }
    Date periodEndDate, periodStartDate;
    List<Exception> errorList = new List<Exception>();
    private ID onceOffServiceOrderID;

    public Database.QueryLocator start(Database.BatchableContext bc)
    {
        if(onceOffServiceOrderID == null)
        {
            //Current month
            Integer numDays = Date.daysInMonth(Date.today().year(), Date.today().month());
            periodStartDate = Date.newInstance(Date.today().year(), Date.today().month(), 1);
            periodEndDate = Date.newInstance(Date.today().year(), Date.today().month(), numDays);
        }
        return Database.getQueryLocator([SELECT Id,
                                                Name,
                                                Start_Date__c,
                                                End_Date__c,
                                                Client__r.Client_ID__c,
                                                Account__c
                                        FROM RCR_Contract__c
                                        WHERE Status__c = 'Approved'
                                        AND Plan_Action__r.Plan_Start_Date__c <= :periodStartDate
                                        AND Plan_Action__r.Plan_End_Date__c >= :periodEndDate
                                        AND Plan_Action__r.Approved__c = true
                                        AND Plan_Action__r.RecordType.Name = 'MyWay Service'
                                        AND (ID = :onceOffServiceOrderID OR
                                            Name LIKE :(onceOffServiceOrderID == null ? '%' : '~'))
                                        AND ID NOT IN (SELECT   Service_Order__c
                                                        FROM    RCR_Invoice__c
                                                        WHERE   SDS__c = true
                                                        AND Date__c >= :periodStartDate
                                                        AND Date__c <= :periodEndDate
                                                        AND Status__c IN (:APS_PicklistValues.RCRInvoice_Status_ReconciledNoErrors,
                                                                        :APS_PicklistValues.RCRInvoice_Status_Approved,
                                                                        :APS_PicklistValues.RCRInvoice_Status_PaymentProcessed))]);

    }

    public void execute(Database.BatchableContext bc,  List<RCR_Contract__c> scope)
    {
        Date dt = onceOffServiceOrderID == null ? Date.today() : getLastMondayInMonth(periodEndDate);
        String month = String.valueOf(dt.month()+100).right(2);
        String year = String.valueOf(dt.year()).right(2);
        Map<Id,RCR_Invoice__c> actStmtToInsertMap = new Map<Id,RCR_Invoice__c>();
        Set<Id> servOrderIds = new Set<Id>();
        for(RCR_Contract__c con : scope)
        {
            servOrderIds.add(con.Id);
        }

        RCR_Invoice_Item__c newItem;
        List<RCR_Invoice_Item__c> newItemsforSO = new List<RCR_Invoice_Item__c>();
        Map<Id,List<RCR_Invoice_Item__c>> newItemsBySOMap = new Map<Id, List<RCR_Invoice_Item__c>>();
        Id curSO;
        Decimal actStmtTotal=0;

        try
        {
            //Get Map of SOs with a List Of ActStmtItems per SO.
            for(RCR_Scheduled_Service_Breakdown__c ssb : [SELECT Name, Start_Date__c, End_Date__c ,
                                                                    RCR_Contract_Scheduled_Service__r.Name,
                                                                    RCR_Contract_Scheduled_Service__r.Service_Description__c,
                                                                    RCR_Contract_Scheduled_Service__r.Service_Code__c,
                                                                    RCR_Contract_Scheduled_Service__r.RCR_Contract__r.Name,
                                                                    RCR_Contract_Scheduled_Service__r.RCR_Contract__c,
                                                                    Total_Quantity__c,
                                                                    RCR_Contract_Scheduled_Service__r.Negotiated_Rate_Standard__c,
                                                                    RCR_Contract_Scheduled_Service__r.Total_Cost__c,
                                                                    RCR_Contract_Scheduled_Service__r.Total_Quantity__c
                                                            FROM RCR_Scheduled_Service_Breakdown__c
                                                            WHERE RCR_Contract_Scheduled_Service__r.RCR_Contract__c IN :servOrderIds
                                                                AND (RCR_Contract_Scheduled_Service__r.RCR_Contract__r.Cancellation_Date__c = null
                                                                OR RCR_Contract_Scheduled_Service__r.RCR_Contract__r.Cancellation_Date__c > :periodStartDate)
                                                                AND RCR_Contract_Scheduled_Service__r.RCR_Contract__r.Signed_By_All_Parties__c != null
                                                                AND Start_Date__c >= :periodStartDate
                                                                AND Start_Date__c <= :periodEndDate
                                                                AND End_Date__c <= :periodEndDate
                                                            ORDER BY RCR_Contract_Scheduled_Service__r.RCR_Contract__c])
            {
                newItem = new RCR_Invoice_Item__c();
                newItem.RecordTypeId = RCR_Invoice_Item__c.SObjectType.getDescribe().getRecordTypeInfosByDeveloperName().get('Created').getRecordTypeId();
                newItem.Service_Order_Number__c = ssb.RCR_Contract_Scheduled_Service__r.RCR_Contract__r.Name;
                newItem.Code__c = ssb.RCR_Contract_Scheduled_Service__r.Service_Code__c;
                newItem.Activity_Date__c = dt;
                newItem.GST__c = 0;
                newItem.Quantity__c = ssb.Total_Quantity__c;
                if(ssb.RCR_Contract_Scheduled_Service__r.Negotiated_Rate_Standard__c == null)
                {
                    newItem.Rate__c =  ssb.RCR_Contract_Scheduled_Service__r.Total_Cost__c / ssb.RCR_Contract_Scheduled_Service__r.Total_Quantity__c;
                }
                else
                {
                    newItem.Rate__c = ssb.RCR_Contract_Scheduled_Service__r.Negotiated_Rate_Standard__c;
                }
                newItem.Total__c = ssb.Total_Quantity__c * newItem.Rate__c;

                if(ssb.RCR_Contract_Scheduled_Service__r.RCR_Contract__c == curSO || curSO == null)
                {
                    newItemsforSO.add(newItem);
                    actStmtTotal += newItem.Total__c;
                    curSO = ssb.RCR_Contract_Scheduled_Service__r.RCR_Contract__c;
                }
                else
                {
                    if(actStmtTotal > 0)
                    {
                        newItemsBySOMap.put(curSO,newItemsforSO);
                    }
                    newItemsforSO = new List<RCR_Invoice_Item__c>();
                    newItemsforSO.add(newItem);
                    actStmtTotal = newItem.Total__c;
                    curSO = ssb.RCR_Contract_Scheduled_Service__r.RCR_Contract__c;
                }
            }
            //Catch the last one
            if(curSO != null && actStmtTotal > 0)
            {
                newItemsBySOMap.put(curSO,newItemsforSO);
            }
            //Do ActStmts
            for(RCR_Contract__c servOrder : scope)
            {
                String cleanClientId = dropLeadingZeroes(servOrder.Client__r.Client_ID__c.removeStart('CL'));
                String stmtNum = 'MY' + cleanClientId + year + month;

                RCR_Invoice__c actStmt = new RCR_Invoice__c();
                actStmt.RecordTypeId = RCR_Invoice__c.SObjectType.getDescribe().getRecordTypeInfosByDeveloperName().get('Created').getRecordTypeId();
                actStmt.Invoice_Number__c = stmtNum;
                actStmt.Date__c = dt;
                actStmt.Start_Date__c = periodStartDate;
                actStmt.End_Date__c = periodEndDate;
                actStmt.Invoice_Received_Date__c = dt;
                actStmt.Account__c = servOrder.Account__c;
                actStmt.Service_Order__c = servOrder.ID;
                actStmt.SDS__c = true;

                //Only add ActStmts that have items. We only have items if their total is > 0
                if(newItemsBySOMap.containsKey(servOrder.Id))
                {
                    actStmtToInsertMap.put(servOrder.Id, actStmt);
                }
            }
            //Insert records
            if(!actStmtToInsertMap.isEmpty())
            {
                INSERT actStmtToInsertMap.values();
                List<RCR_Invoice_Item__c> items = new List<RCR_Invoice_Item__c>();
                for(Id soId : actStmtToInsertMap.keySet())
                {
                    RCR_Invoice__c actStmt = actStmtToInsertMap.get(soId);
                    for(RCR_Invoice_Item__c item : newItemsBySOMap.get(soId))
                    {
                        item.RCR_Invoice__c = actStmt.ID;
                        items.add(item);
                    }
                }
                INSERT items;

                //Reconcile
                RCRInvoiceSchedule invSchedule = new RCRInvoiceSchedule();
                invSchedule.scheduleReconciliation(actStmtToInsertMap.values(), false);
            }
        }
        catch(Exception e)
        {
            if(errorList.size() < 100)
            {
                errorList.add(e);
            }
        }

    }

    public void finish(Database.BatchableContext bc)
    {
        if(!errorList.isEmpty())
        {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setSubject('Errors occurred during Payment Authority Batch process.');
            mail.setTargetObjectId(UserInfo.getUserId());
            mail.setSaveAsActivity(false);
            mail.setPlainTextBody(buildErrEmail());
            Messaging.sendEmail(new Messaging.Email[] { mail });
        }
    }

    public void execute(SchedulableContext sc)
    {
        Database.executeBatch(new PaymentAuthorityBatch(), 200);
    }

    private Date getLastMondayInMonth(Date dt)
    {
        Date lastDay = dt.toStartOfMonth().addMonths(1).addDays(-1);
        while(lastDay >= dt)
        {
            if(lastDay.toStartOfWeek().addDays(1) == lastDay)
            {
                return lastDay;
            }
            lastDay = lastDay.addDays(-1);
        }
        return lastDay;
    }

    private String dropLeadingZeroes(String passedValue)
    {
        String returnString = null;
        if (passedValue != null)
        {
            returnString = passedValue.trim();
            Pattern validCharacters = Pattern.compile('([0-9]+)');
            Matcher checkChars = validCharacters.matcher(returnString);

            if (checkChars.matches())
            {
                if (returnString.startsWith('0') && returnString.length() > 1)
                { //if the string begins with a 0 and the length is greater than 1

                    boolean keepChecking = true; //create a boolean variable
                    while (keepChecking)
                    {
                        if (returnString.startsWith('0') && returnString.length() > 1)
                        { //if the string begins with 0 and there is more than 1 character
                            returnString = returnString.substring(1);
                        }
                        else
                        { //either the string doesn't begin with 0 or the length is less than or equal to 1
                            keepChecking = false;
                        }
                    }
                }
                if (returnString == '0')
                { //if the resulting string is now a single '0'
                    returnString = null; //set the string to null
                }
            }
            else
            { //otherwise the value passed was not valid
                returnString = null;
            }
        }
        return returnString; //pass back a value
    }

    private String buildErrEmail()
    {
        String msgBody;

        msgBody = 'Payment Authority Batch errors: \n';
        for(Exception e : errorList)
        {
            msgBody += e + '\n';
        }
        return msgBody;
    }
}