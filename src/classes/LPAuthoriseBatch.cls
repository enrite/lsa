public without sharing class LPAuthoriseBatch extends LPAuthoriseRejectInvoiceBatchBase
{
    public LPAuthoriseBatch(Set<ID> lpItemsForApproval)
    {
        super(lpItemsForApproval, LPRESTInterface.LPINVOICE_APPROVAL_AUTHORIZE);
    }
}