global virtual class FinancialYearAllocation 
    extends GridPager2 
{       
    public FinancialYearAllocation()
    {}
    
    public FinancialYearAllocation(ApexPages.StandardController controller)
    {
        if(controller.getRecord().getSObjectType() == RCR_Contract__c.sObjectType && controller.getId() != null)
        {
            ServiceOrder = [SELECT  Id,
                                    Name,
                                    Client__c,
                                    Start_Date__c,
                                    End_Date__c,
                                    Level__c,
                                    Rollover_Status__c
                            FROM    RCR_Contract__c
                            WHERE   Id = :controller.getId()];
        }
    }

    public FinancialYearAllocation(ApexPages.StandardController controller, Integer pageSize)
    {
        super(pageSize);
        if(controller.getRecord().getSObjectType() == RCR_Contract__c.sObjectType && controller.getId() != null)
        {
            ServiceOrder = [SELECT  Id,
                                    Name,
                                    Client__c,
                                    Start_Date__c,
                                    End_Date__c,
                                    Level__c,
                                    Rollover_Status__c
                            FROM    RCR_Contract__c
                            WHERE   Id = :controller.getId()];
        }
    }
    
    public FinancialYearAllocation(Financial_Year__c pFinancialYear)
    {
        FinancialYear = pFinancialYear;
    }
    
    private final String NOTSTATED = 'Undefined';
    
    private ID clientID;
    private transient List<RCR_Contract__c> reqClientServiceOrders; 
    private transient Map<String, FinancialYearAllocation> allocsByFinancialYear;
    private transient  List<FinancialYearAllocation> allocsForServiceOrderPeriod;
    private transient  List<FinancialYearAllocation> allAllocsForServiceOrderPeriod;
    private transient  List<Financial_Year__c> soFinancialYears;
    private transient  Boolean serviceOrderPeriodOnly;
        
    public FinancialYearAllocation ControllerClass 
    {
        get
        {
            return this;  
        }
    }
    
    public Boolean ServiceOrderPeriod
    {
        get
        {
            return serviceOrderPeriodOnly == null ? false : serviceOrderPeriodOnly;  
        }
        set
        {
            serviceOrderPeriodOnly = value;
        }
    }
    
    public RCR_Contract__c ServiceOrder 
    {
        get
        {
            if(ServiceOrder == null)
            {
               ServiceOrder = new RCR_Contract__c(Status__c = APS_PicklistValues.RCRContract_Status_New);
               ServiceOrder.Level__c = '1';
            }
            return ServiceOrder;
        }
        set;
    }
    
    public Financial_Year__c FinancialYear
    {
        get;
        private set;
    }
    
    public List<RCR_Contract__c> RequestClientServiceOrders
    {
        get
        {       
            if(reqClientServiceOrders == null)
            {
                reqClientServiceOrders = new  List<RCR_Contract__c>();
                reqClientServiceOrders.addAll(getSubmittedServiceOrders());
            }
            return reqClientServiceOrders;
        }
        protected set
        {
            reqClientServiceOrders = value;
        }
    }
    
    
    public List<RCR_Contract__c> OtherClientRollovers
    {
        get
        {
            if(OtherClientRollovers == null)
            {
                List<Integer> fiscalYears = new List<Integer>();
                if(serviceOrder.ID != null)
                {
                    fiscalYears = A2HCUtilities.getSFFiscalYears(serviceOrder.Start_Date__c, serviceOrder.End_Date__c);
                }
                
                OtherClientRollovers = new List<RCR_Contract__c>();
                for(RCR_Contract__c c : [SELECT Id,
                                                Name,
                                                Status__c,
                                                Rollover_Status__c,
                                                Account__c,
                                                Account__r.Name,
                                                Funding_Source__c,
                                                Start_Date__c,
                                                End_Date__c,
                                                Total_Service_Order_Cost__c,
                                                Allocation_Total_Approved__c,
                                                Allocation_Total_Not_Approved__c
                                         FROM   RCR_Contract__c
                                         WHERE  Record_Type_Name__c = :APSRecordTypes.RCRServiceOrder_Rollover
                                         AND    Client__c = :ServiceOrder.Client__c
                                         AND    FISCAL_YEAR(Start_Date__c) IN :fiscalYears
                                         ORDER BY CreatedDate desc])
                {
                    OtherClientRollovers.add(c);
                }
            }
            return OtherClientRollovers;
        }
        private set;
    }

                
    public Map<String, FinancialYearAllocation> AllocationsByFinancialYear 
    {
        get
        {
            if(allocsByFinancialYear == null)
            {
                allocsByFinancialYear = new Map<String, FinancialYearAllocation>();  
                Set<String> soFinancialYears = null;
                if(ServiceOrderPeriod && serviceOrder.Start_Date__c != null && serviceOrder.End_Date__c != null)
                {
                    soFinancialYears = new Set<String>(A2HCUtilities.getFinancialYears(serviceOrder.Start_Date__c, serviceOrder.End_Date__c));
                }
                for(FinancialYearAllocation finYearAllocation : ServiceOrderFuture.getFinancialYearAllocations(ServiceOrder.Client__c, true).values())
                {           
                    // if displaying in context of a service order, only show allocations relevant to that service order
                    if(soFinancialYears != null && !soFinancialYears.contains(finYearAllocation.FinancialYear.Name))
                    {
                        continue;
                    }
                    allocsByFinancialYear.put(finYearAllocation.FinancialYear.Name, finYearAllocation);
                }
            }
            return allocsByFinancialYear; 
        }
        set
        {
            allocsByFinancialYear = value;
        }
    }

    public List<FinancialYearAllocation> AllocationsForServiceOrderPeriod
    {
        get
        {
            if(allocsForServiceOrderPeriod == null)
            {
                allocsForServiceOrderPeriod = new List<FinancialYearAllocation>();
                for(Financial_Year__c fy : ServiceOrderFinancialYears)
                {
                    if(AllocationsByFinancialYear.containsKey(fy.Name))
                    {
                        allocsForServiceOrderPeriod.add(AllocationsByFinancialYear.get(fy.Name));
                    }
                } 
            }
            return allocsForServiceOrderPeriod;
        }
        set
        {
            allocsForServiceOrderPeriod = value;
        }
    }

    public List<FinancialYearAllocation> AllAllocationsForServiceOrderPeriod
    {
        get
        {
            if(allAllocsForServiceOrderPeriod == null)
            {
                allAllocsForServiceOrderPeriod = new List<FinancialYearAllocation>();
                for(Financial_Year__c fy : ServiceOrderFinancialYears)
                {
                    if(AllocationsByFinancialYear.containsKey(fy.Name))
                    {
                        allAllocsForServiceOrderPeriod.add(AllocationsByFinancialYear.get(fy.Name));
                    }
                    else
                    {
                        allAllocsForServiceOrderPeriod.add(new FinancialYearAllocation(fy));
                    }
                } 
            }
            return allAllocsForServiceOrderPeriod;
        }
    }

    @TestVisible
    private List<Financial_Year__c> ServiceOrderFinancialYears
    {
        get
        {
            if(soFinancialYears == null)
            {
                soFinancialYears = new List<Financial_Year__c>();
                if(serviceOrder.ID != null && serviceOrder.Start_Date__c != null && serviceOrder.End_Date__c != null)
                {
                    Date soFyStartDate = A2HCUtilities.getStartOfFinancialYear(serviceOrder.Start_Date__c);
                    Date soFyEndDate = A2HCUtilities.getEndOfFinancialYear(serviceOrder.End_Date__c);
                    while(soFyStartDate < soFyEndDate)
                    {
                        soFinancialYears.add(FinancialYearsByStartDate.get(soFyStartDate));
                        soFyStartDate = soFyStartDate.addYears(1);
                    } 
                } 
            }
            return soFinancialYears;
        }
    }
    
    @TestVisible
    public List<String> FinancialYears
    {
        get
        {
            if(FinancialYears == null)
            {
                FinancialYears = new List<String>();
                List<String> years = new List<String>(AllocationsByFinancialYear.keySet());  
                years.sort();
                for(Integer i = years.size() - 1; i >= 0; i--)
                {
                    FinancialYears.add(years[i]);
                }
            }
            return FinancialYears;
        }
        protected set;
    }
    
    @TestVisible
    public decimal ServiceOrderCost
    {
        get
        {
            if(ServiceOrderCost == null)
            {
                getColumnTotals();
            }
            return ServiceOrderCost;
        }
        private set;
    }
    
    @TestVisible
    public decimal AllocationAmount
    {
        get
        {
            if(AllocationAmount == null)
            {
                getColumnTotals();
            }
            return AllocationAmount;
        }
        private set;
    }
    
    @TestVisible
    public decimal RecurrentAllocationAmount
    {
        get
        {
            if(RecurrentAllocationAmount == null)
            {
                getColumnTotals();
            }
            return RecurrentAllocationAmount;
        }
        private set;
    }

    public decimal AllocationAvailable
    {
        get
        {
            return AllocationAmount - ServiceOrderCost;
        }
    }
    
    public decimal RequestsNotYetApproved
    {
        get
        {
            if(RequestsNotYetApproved == null)
            {
                getColumnTotals();
            }
            return RequestsNotYetApproved;
        }
        private set;
    }
    
//    public Set<IF_Personal_Budget__c> Allocationss
//    {
//        get
//        {
//            if(Allocations == null)
//            {
//                Allocations = new Set<IF_Personal_Budget__c>();
//            }
//            return Allocations;
//        }
//        private set;
//    }
    
    public Set<RCR_Contract__c> ServiceOrders
    {
        get
        {
            if(ServiceOrders == null)
            {
                ServiceOrders = new Set<RCR_Contract__c>();
            }
            return ServiceOrders;
        }
        private set;
    }               
    
    public List<FinancialYearAllocationRow> Rows
    {
        get
        {
            if(Rows == null)
            {
                Rows = new List<FinancialYearAllocationRow>(); 
                Map<String, FinancialYearAllocationRow> rowsBySourceAndType = new Map<String, FinancialYearAllocationRow>();
                Map<ID, Set<RCR_Contract_Service_Type__c>> serviceTypesByServiceID = new Map<ID, Set<RCR_Contract_Service_Type__c>>();
                for(RCR_Contract_Service_Type__c cst : [SELECT  Allocation_Service_Type__c,
                                                                Quantity__c,
                                                                RCR_Contract_Scheduled_Service__c,
                                                                RCR_Contract_Adhoc_Service__c
                                                        FROM    RCR_Contract_Service_Type__c
                                                        WHERE   RCR_Service_Order__c = null AND
                                                                (RCR_Contract_Scheduled_Service__r.RCR_Contract__c IN :ServiceOrders OR
                                                                RCR_Contract_Adhoc_Service__r.RCR_Contract__c IN :ServiceOrders)])
                {
                    if(cst.RCR_Contract_Scheduled_Service__c != null)
                    {
                        if(!serviceTypesByServiceID.containsKey(cst.RCR_Contract_Scheduled_Service__c))
                        {
                            serviceTypesByServiceID.put(cst.RCR_Contract_Scheduled_Service__c, new Set<RCR_Contract_Service_Type__c>());
                        }   
                        serviceTypesByServiceID.get(cst.RCR_Contract_Scheduled_Service__c).add(cst);
                    }
                    else if(cst.RCR_Contract_Adhoc_Service__c != null)
                    {
                        if(!serviceTypesByServiceID.containsKey(cst.RCR_Contract_Adhoc_Service__c))
                        {
                            serviceTypesByServiceID.put(cst.RCR_Contract_Adhoc_Service__c, new Set<RCR_Contract_Service_Type__c>());
                        }   
                        serviceTypesByServiceID.get(cst.RCR_Contract_Adhoc_Service__c).add(cst);
                    }
                }
                for(RCR_Contract__c so : ServiceOrders)
                {
                    getRowsForScheduledServices(so, rowsBySourceAndType, serviceTypesByServiceID);
                    getRowsForAdhocServices(so, rowsBySourceAndType, serviceTypesByServiceID);
                }   
                getAllocationRows(rowsBySourceAndType);
                Rows = rowsBySourceAndType.values();
                Rows.sort();
                getColumnTotals();              
            }
            return Rows;
        }
        private set;
    } 
    
    @TestVisible
    public String TotalServiceOrderCost
    {
        get
        {
            if(TotalServiceOrderCost == null)
            {
                getColumnTotals();
            }
            return TotalServiceOrderCost;
        }
        private set;
    }
    
    @TestVisible
    public String TotalAllocationCost
    {
        get
        {
            if(TotalAllocationCost == null)
            {
                getColumnTotals();
            }
            return TotalAllocationCost;
        }
        private set;
    }
    
    @TestVisible
    public String TotalRequestCostNotApproved
    {
        get
        {
            if(TotalRequestCostNotApproved == null)
            {
                getColumnTotals();
            }
            return TotalRequestCostNotApproved;
        }
        private set;
    }
    
    @TestVisible
    public String TotalAllocationFYECost
    {
        get
        {
            if(TotalAllocationFYECost == null)
            {
                getColumnTotals();
            }
            return TotalAllocationFYECost;
        }
        private set;
    }
       
    @TestVisible 
    public String TotalAvailableAllocation
    {
        get
        {
            if(TotalAvailableAllocation == null)
            {
                getColumnTotals();
            }
            return TotalAvailableAllocation;
        }
        private set;
    }  
    
    @TestVisible 
    protected Map<Date, Financial_Year__c> FinancialYearsByStartDate
    {
        get
        {
            if(FinancialYearsByStartDate == null)
            {        
                FinancialYearsByStartDate = new Map<Date, Financial_Year__c>();
                for(Financial_Year__c fy : [SELECT ID,
                                                    Name,
                                                    Start_Date__c,
                                                    End_Date__c
                                            FROM    Financial_Year__c])
                {
                    FinancialYearsByStartDate.put(fy.Start_Date__c, fy);
                }               
            }
            return FinancialYearsByStartDate;
        }
        set;
    }    
    
    @TestVisible 
    protected Map<ID, Financial_Year__c> FinancialYearsByID
    {
        get
        {
            if(FinancialYearsByID == null)
            {        
                FinancialYearsByID = new Map<ID, Financial_Year__c>();
                for(Financial_Year__c fy : FinancialYearsByStartDate.values())
                {
                    FinancialYearsByID.put(fy.ID, fy);
                }               
            }
            return FinancialYearsByID;
        }
        set;
    } 
    
    private void getRowsForScheduledServices(RCR_Contract__c so, Map<String, FinancialYearAllocationRow> rowsBySourceAndType, Map<ID, Set<RCR_Contract_Service_Type__c>> serviceTypesByServiceID)
    {
        for(RCR_Contract_Scheduled_Service__c css : so.RCR_Contract_Scheduled_Services__r) 
        {
            if(!serviceTypesByServiceID.containsKey(css.ID))
            {
                serviceTypesByServiceID.put(css.ID, new Set<RCR_Contract_Service_Type__c>());
            }
            
            decimal cstTotalQuantity = getServiceTypeTotalQuantity(serviceTypesByServiceID.get(css.ID));
            for(RCR_Contract_Service_Type__c cst : serviceTypesByServiceID.get(css.ID))
            {
                decimal cstQuantityRatio = (cstTotalQuantity == 0.0 || cstTotalQuantity == null) ? 1.0 : (cst.Quantity__c / cstTotalQuantity);
                String svcTypeKey = String.isBlank(cst.Allocation_Service_Type__c) ? NOTSTATED : cst.Allocation_Service_Type__c;
//                String fundingType = css.RCR_Contract__r.RCR_CBMS_Contract__r.Contract_Type__c == null ?
//                                        (css.Funding_Commitment__c == null ? NOTSTATED : css.Funding_Commitment__c) : css.RCR_Contract__r.RCR_CBMS_Contract__r.Contract_Type__c;
                String fundingType = css.Funding_Commitment__c == null ? NOTSTATED : css.Funding_Commitment__c;
                String key = fundingType + '|' + svcTypeKey;                                                  
                if(!rowsBySourceAndType.containsKey(key))
                {
                    rowsBySourceAndType.put(key, new FinancialYearAllocationRow(svcTypeKey, fundingType));
                }
                decimal cssSoCost = 0.0;
                decimal requestCost = 0.0;                        
                decimal requestCostToCalc = 0.0;
                if(so.RecordType.Name == APSRecordTypes.RCRServiceOrder_ServiceOrder ||
                        so.RecordType.Name == APSRecordTypes.RCRServiceOrder_ServiceOrderCBMS)
                {                                                       
                     cssSoCost = ServiceOrderFuture.getApportionedAmountForFinancialYear(FinancialYear, 
                                                                                        css.Start_Date__c, 
                                                                                        css.End_Date__c, 
                                                                                        css.Total_Cost__c * cstQuantityRatio);  
                }                        
                else if(so.Status__c !=  APS_PicklistValues.RCRContract_Status_Approved &&
                        so.Status__c != APS_PicklistValues.RCRContract_Status_Rejected)
                {                       
                    if(so.RecordType.Name == APSRecordTypes.RCRServiceOrder_Amendment)
                    {
                        requestCostToCalc = css.Source_Record__r.Total_Cost__c == null ? 0.0 : css.Total_Cost__c - css.Source_Record__r.Total_Cost__c;  
                        requestCostToCalc = requestCostToCalc * cstQuantityRatio;   
                    }
                    else
                    {
                        if(so.Rollover_Status__c == APS_PicklistValues.RCRContract_RolloverStatus_RolloverNotRequired)
                        {
                            continue;
                        }
                        requestCostToCalc = css.Total_Cost__c * cstQuantityRatio;
                    }
                    if(requestCostToCalc != 0)
                    {
                        requestCost = ServiceOrderFuture.getApportionedAmountForFinancialYear(FinancialYear, 
                                                                                        css.Start_Date__c, 
                                                                                        css.End_Date__c, 
                                                                                        requestCostToCalc);
                    }
                }
                rowsBySourceAndType.get(key).ServiceOrderCost += cssSoCost;
                rowsBySourceAndType.get(key).RequestCostNotApproved += requestCost;
            }
        }
    }
    
    private void getRowsForAdhocServices(RCR_Contract__c so, Map<String, FinancialYearAllocationRow> rowsBySourceAndType, Map<ID, Set<RCR_Contract_Service_Type__c>> serviceTypesByServiceID)
    {
        for(RCR_Contract_Adhoc_Service__c ahs : so.RCR_Contract_Adhoc_Services__r)
        {
            if(!serviceTypesByServiceID.containsKey(ahs.ID))
            {
                serviceTypesByServiceID.put(ahs.ID, new Set<RCR_Contract_Service_Type__c>());
            }
            decimal cstCount = serviceTypesByServiceID.get(ahs.ID).size() * 1.0;
            for(RCR_Contract_Service_Type__c cst : serviceTypesByServiceID.get(ahs.ID))
            {
                String svcTypeKey = String.isBlank(cst.Allocation_Service_Type__c) ? NOTSTATED : cst.Allocation_Service_Type__c;
                String fundingType = ahs.Funding_Commitment__c == null ? NOTSTATED : ahs.Funding_Commitment__c;
                String key = fundingType + '|' + svcTypeKey;                                                  
                if(!rowsBySourceAndType.containsKey(key))
                {
                    rowsBySourceAndType.put(key, new FinancialYearAllocationRow(svcTypeKey, fundingType));
                } 
                decimal ahsCost = ServiceOrderFuture.getApportionedAmountForFinancialYear(FinancialYear,
                                                                                            ahs.Start_Date__c, 
                                                                                            ahs.End_Date__c, 
                                                                                            ahs.Amount__c / cstCount);   
                if(so.RecordType.Name == APSRecordTypes.RCRServiceOrder_ServiceOrder ||
                        so.RecordType.Name == APSRecordTypes.RCRServiceOrder_ServiceOrderCBMS)
                {                       
                    rowsBySourceAndType.get(key).ServiceOrderCost += ahsCost; 
                }
                else if((so.RecordType.Name == APSRecordTypes.RCRServiceOrder_NewRequestCBMS &&
                            so.Status__c != APS_PicklistValues.RCRContract_Status_CBMSApproved) ||
                        (so.RecordType.Name == APSRecordTypes.RCRServiceOrder_Rollover &&
                            so.Status__c != APS_PicklistValues.RCRContract_Status_Approved))
                {
                    rowsBySourceAndType.get(key).RequestCostNotApproved += ahsCost;
                }
            }
        }
    }
    
    private void getAllocationRows(Map<String, FinancialYearAllocationRow> rowsBySourceAndType)
    {
//        for(IF_Personal_Budget__c alloc : Allocations)
//        {
//            String source = alloc.Service_Type__c == null ? NOTSTATED : alloc.Service_Type__c;
//            String key = alloc.Allocation_Type__c + '|' + source;
//            if(!rowsBySourceAndType.containsKey(key))
//            {
//                rowsBySourceAndType.put(key, new FinancialYearAllocationRow(source, alloc.Allocation_Type__c));
//            }
//            rowsBySourceAndType.get(key).AllocationCost += alloc.Amount__c;
//            rowsBySourceAndType.get(key).AllocationFYECost += alloc.FYE_Amount__c == null ? 0.0 : alloc.FYE_Amount__c;
//        }
    }            
            
    private void getColumnTotals()
    {
        decimal svcOrderCost = 0.0;
        decimal allocCost = 0.0;
        decimal recurrentAllocCost = 0.0;
        decimal allocFYECost = 0.0; 
        decimal reqNotApproved = 0.0;    
        
        for(FinancialYearAllocationRow fyr : Rows)
        {           
            svcOrderCost += fyr.ServiceOrderCost == null ? 0.0 : fyr.ServiceOrderCost;
            allocCost += fyr.AllocationCost == null ? 0.0 : fyr.AllocationCost;
            allocFYECost += fyr.AllocationFYECost == null ? 0.0 : fyr.AllocationFYECost;        
            if(fyr.Type == APS_PicklistValues.RCRContract_FundingCommitment_Recurrent)
            {
                recurrentAllocCost += allocCost;
            }
            reqNotApproved += fyr.RequestCostNotApproved == null ? 0.0 : fyr.RequestCostNotApproved;
        }
        ServiceOrderCost = svcOrderCost;
        AllocationAmount = allocCost;
        RecurrentAllocationAmount = recurrentAllocCost;
        TotalServiceOrderCost = A2HCUtilities.decimalToCurrency(svcOrderCost, true);
        TotalAllocationCost = A2HCUtilities.decimalToCurrency(allocCost, true);
        TotalAllocationFYECost = A2HCUtilities.decimalToCurrency(allocFYECost, true);
        TotalAvailableAllocation = A2HCUtilities.decimalToCurrency(allocCost - svcOrderCost, true);
        TotalRequestCostNotApproved = A2HCUtilities.decimalToCurrency(reqNotApproved, true);
    }
    
    private decimal getServiceTypeTotalQuantity(Set<RCR_Contract_Service_Type__c> serviceTypes)
    {
        decimal cstTotalQuantity = 0.0;
        for(RCR_Contract_Service_Type__c cst : serviceTypes)
        {
            cstTotalQuantity += cst.Quantity__c == null ? 0.0 : cst.Quantity__c;
        }
        return cstTotalQuantity;
    }
    
    @TestVisible
    private List<RCR_Contract__c> getSubmittedServiceOrders()
    {     
        List<Integer> fiscalYears = new List<Integer>();
        if(serviceOrder.ID != null)
        {
            fiscalYears = A2HCUtilities.getSFFiscalYears(serviceOrder.Start_Date__c, serviceOrder.End_Date__c);
        }
        return [SELECT ID,
                        Name,
                        Client__c,
                        Status__c,
                        Request_Amount__c,
                        Start_Date__c,
                        End_Date__c,
                        Cancellation_Date__c,
                        CSA_Status__c,
                        Level__c,
                        Funding_Source__c,
                        Service_Order_Number__c,
                        Total_Service_Order_Cost__c,
                        Account__r.Name,
                        RecordType.Name,
                        Rollover_Status__c
                FROM    RCR_Contract__c
                WHERE   Client__c != null AND
                        Client__c = :ServiceOrder.Client__c AND
                        (
                            RecordType.Name = :APSRecordTypes.RCRServiceOrder_ServiceOrder 
                            OR
                            RecordType.Name = :APSRecordTypes.RCRServiceOrder_ServiceOrderCBMS                          
                        ) AND
                        (
                            FISCAL_YEAR(Start_Date__c) IN :fiscalYears OR 
                            Name LIKE :(fiscalYears.isEmpty() ? '%' : '~')
                        )
                ORDER BY Start_Date__c DESC];
    }     

    public class FinancialYearAllocationRow implements Comparable
    {
        public FinancialYearAllocationRow(String pSource, String pType)
        {
            Source = pSource;
            Type = pType;
            ServiceOrderCost = 0.0;
            AllocationCost = 0.0;
            AllocationFYECost = 0.0;
            RequestCostNotApproved = 0.0;
        }    
        
        public Integer compareTo(Object objToCompare) 
        {
            if (Source == null || Type == null)
            { 
                return -1; 
            }
            FinancialYearAllocationRow compareRow = (FinancialYearAllocationRow)objToCompare;
            Integer sourceCompare = Source.compareTo(compareRow.Source);
            // we want Type in reverse order
            return sourceCompare == 0 ? Type.compareTo(compareRow.Type) * -1 : sourceCompare;
        }           
        
        public String Source
        {
            get;
            private set;
        }
        
        public String Type
        {
            get;
            private set;
        }
        
        public decimal ServiceOrderCost
        {
            get;
            set;
        }
        
        public decimal AllocationCost
        {
            get;
            set;
        }
        
        public decimal RequestCostNotApproved
        {
            get;
            set;
        }
        
        public decimal AllocationAvailable
        {
            get
            {
                return (AllocationCost == null || ServiceOrderCost == null) ? 0.0 : (AllocationCost - ServiceOrderCost);
            }
        }
        
        public decimal AllocationFYECost
        {
            get;
            set;
        }
        
        public String ServiceOrderCostString
        {
            get
            {
                return A2HCUtilities.decimalToCurrency(ServiceOrderCost, true);
            }
        }
        
        public String AllocationCostString
        {
            get
            {
                return A2HCUtilities.decimalToCurrency(AllocationCost, true);
            }
        }
        
        public String AllocationAvailableString
        {
            get
            {
                return A2HCUtilities.decimalToCurrency(AllocationAvailable, true);
            }
        }
        
        public String AllocationFYECostString
        {
            get
            {
                return A2HCUtilities.decimalToCurrency(AllocationFYECost, true);
            }
        }
        
        public String RequestCostNotApprovedString
        {
            get
            {
                return A2HCUtilities.decimalToCurrency(RequestCostNotApproved, true);
            }
        }
    }    
}