public with sharing class FormViewExtension {

	private final Form__c record;

    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public FormViewExtension(ApexPages.StandardController stdController) {
        this.record = (Form__c)stdController.getRecord();
    }

    //This will not work if Lifetime Assessment – Start record type label is renamed 
    public Boolean LifetimeAssessmentAccess
    {
        get{
            List<RecordTypeInfo> infos = Form_Detail__c.SObjectType.getDescribe().getRecordTypeInfos();

            if (infos.size() > 1) {
                for (RecordTypeInfo i : infos) {
                   if (i.isAvailable() && i.getName() == 'Lifetime Assessment – Start')
                        return true;
                }
            } 
            return false;
        }   
    }
}