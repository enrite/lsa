global class PermissionSetAuditScheduled implements Schedulable 
{
    private ID permissionSetRtId = User_Audit__c.sObjectType.getDescribe().getRecordTypeInfosByName().get('Permission Set').getRecordTypeId();
	/**
	 * @description Executes the scheduled Apex job. 
	 * @param sc contains the job ID
	 */ 
	global void execute(SchedulableContext sc) 
    {
        DateTime lastRun = Datetime.newInstance(1990, 1, 1, 0, 0, 0);
        for(User_Audit__c ua : [SELECT ID,                                        
                                        Updated__c
                                FROM    User_Audit__c
                                WHERE   RecordTypeId = :permissionSetRtId 
                                ORDER BY Updated__c DESC 
                                LIMIT 1])
        {
            lastRun = ua.Updated__c;
        }
        List<User_Audit__c> newAudits = new List<User_Audit__c>(); 
		for(PermissionSetAssignment psa : [SELECT   ID,
                                                    AssigneeId,
                                                    SystemModStamp,
                                                    PermissionSetId,
                                                    PermissionSet.Name,
                                                    PermissionSet.ProfileId,
                                                    PermissionSet.Profile.Name
                                            FROM    PermissionSetAssignment
                                            WHERE   SystemModStamp > :lastRun])
        {
            User_Audit__c audit = new User_Audit__c(RecordTypeId = permissionSetRtId);
            audit.User__c = psa.AssigneeId;
            audit.Permission_Set_Assignment_ID__c = psa.ID;
            audit.Permission_Set_ID__c = psa.PermissionSetId;
            audit.Permission_Set_Name__c = psa.PermissionSet.ProfileId == null ? psa.PermissionSet.Name : psa.PermissionSet.Profile.Name;
            audit.Grant__c = 'Granted';
            audit.Updated__c = psa.SystemModStamp;
            audit.Updated_On__c = String.valueOf(psa.SystemModStamp);
            newAudits.add(audit);
        }
        insert newAudits;

        PermissionSetAuditBatch batch = new PermissionSetAuditBatch(lastRun);
        Database.executeBatch(batch, 100);
	}
}