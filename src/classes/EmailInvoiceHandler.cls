global class EmailInvoiceHandler implements Messaging.InboundEmailHandler
{
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) 
    {
        Savepoint sp = Database.setSavepoint();
        Email2Invoice_File_Types__c fileTypes = Email2Invoice_File_Types__c.getOrgDefaults();
        Set<String> allowedFileTypes = new Set<String>();
        if(fileTypes != null && fileTypes.File_Extensions__c != null)
        {
            for(String s : fileTypes.File_Extensions__c.split(';'))
            {
                allowedFileTypes.add(s);
            }
        }
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        try 
        {	        
            Email_To_Invoice__c e2I = new Email_To_Invoice__c();
            e2I.Email_Subject__c = email.Subject;
            e2I.Email_Body__c = email.plainTextBody == null ? email.htmlBody.stripHtmlTags() : email.plainTextBody;
            e2I.Received_From__c = email.fromName;
            e2I.Date__c = DAte.today();
            e2I.RecordTypeID = A2HCUtilities.getRecordType('Email_To_Invoice__c', 'Unprocessed').ID;
            insert e2I;

            List<Attachment> attachments = new List<Attachment>();
            if(email.BinaryAttachments != null)
            {
                for(Messaging.InboundEmail.BinaryAttachment att : email.BinaryAttachments) 
                {
                    if(isValidFileExtension(att.FileName, allowedFileTypes))
                    {
                        Attachment a = new Attachment(ParentID = e2I.ID);
                        a.Body = att.Body;
                        a.ContentType = att.mimeTypeSubType;
                        a.Name = att.FileName;
                        attachments.add(a);
                    }
                }
            }
            if(email.TextAttachments != null)
            {
                for(Messaging.InboundEmail.TextAttachment att : email.TextAttachments)
                {
                    if(isValidFileExtension(att.FileName, allowedFileTypes))
                    {
                        Attachment a = new Attachment(ParentID = e2I.ID);
                        a.Body = Blob.valueOf(att.Body);
                        a.ContentType = att.mimeTypeSubType;
                        a.Name = att.FileName;
                        attachments.add(a);
                    }
                }
            }
            insert attachments;
            result.message = email.Subject + '\nEmail received and processed successfully';
            result.success = true;
        }
        catch(Exception ex)
        {
            if(sp != null)
            {
                Database.rollback(sp);
            }
            result.message = email.Subject + ' could not be processed.\nError: ' + ex.getMessage();
            result.success = false;
        }
        return result;
    }

    private Boolean isValidFileExtension(String fileName, Set<String> allowedFileTypes)
    {
        return fileName != null && allowedFileTypes.contains(fileName.substringAfterLast('.'));
    }
}