public with sharing class RCRUploadRatesController extends A2HCPageBase
{
	public RCRUploadRatesController()
	{
		returnURL = getReturnPageFromURL();
	}
	
	private PageReference returnURL;
	
	public integer exceptionCount
	{
		get
		{
			if(exceptionCount == null)
			{
				exceptionCount = 0;
			}
			return exceptionCount;
		}
		private set;
	}


	public Attachment File
    {
        get
        {
            if(File == null)
            {
                File = new Attachment();
            }
            return File;
        }
        set;
    }
    
    
    public RCR_Rate_Upload__c RateUpload
    {
    	get
    	{
    		if(RateUpload == null)
    		{
    			RateUpload = new RCR_Rate_Upload__c();
    		}
    		return RateUpload;
    	}
    	set;
    }
    
    
    public List<RateWrapper> DisplayRates
    {
    	get
    	{
    		if(DisplayRates == null)
    		{
    			DisplayRates = new List<RateWrapper>();
    		}
    		return DisplayRates;
    	}
    	private set;
    }
    
    @testVisible
    private List<RCR_Service_Rate__c> Rates
    {
    	get
    	{
    		if(Rates == null)
    		{
    			Rates = new List<RCR_Service_Rate__c>();
    		}
    		return Rates;
    	}
    	private set;
    }
    
    @testVisible
    private List<RCR_Service_Rate__c> SplitRates
    {
    	get
    	{
    		if(SplitRates == null)
    		{
    			SplitRates = new List<RCR_Service_Rate__c>();
    		}
    		return SplitRates;
    	}
    	private set;
    }
    
    @testVisible
    private Map<String, String> ErrorMsgs
    {
        get
        {
            if(ErrorMsgs == null) 
            {
                ErrorMsgs = new Map<String, String>();
                for(RCR_Error_Message__c msg : [SELECT  Name,
                                                Message__c
                                        FROM    RCR_Error_Message__c])
                {
                    ErrorMsgs.put(msg.Name, msg.Message__c);
                }
            }
            return ErrorMsgs;
        }
        private set;
    }
    
    @testVisible
    private Map<String, String> ServiceCodeIDsByAccountNameCodeAndLevel
    {
    	get
    	{
    		if(ServiceCodeIDsByAccountNameCodeAndLevel == null)
    		{
    			ServiceCodeIDsByAccountNameCodeAndLevel = new Map<String, String>();
    			
    			for(RCR_Service__c service : [SELECT	Id,
    													Name,
    													Level__c,
    													Account__r.Name
    										  FROM		RCR_Service__c])
    			{
    				if(!ServiceCodeIDsByAccountNameCodeAndLevel.containsKey(service.Account__r.Name + service.Name + service.Level__c))
    				{
    					ServiceCodeIDsByAccountNameCodeAndLevel.put(service.Account__r.Name + service.Name + service.Level__c, service.Id);
    				}
    			}
    		}
    		return ServiceCodeIDsByAccountNameCodeAndLevel;
    	}
    	private set;
    }
    
    @testVisible
    private Map<string, List<RCR_Service_Rate__c>> existingRatesByAccountAndCode
    {
    	get
    	{
    		if(existingRatesByAccountAndCode == null)
    		{
    			existingRatesByAccountAndCode = new Map<string, List<RCR_Service_Rate__c>>();
    		}
    		return existingRatesByAccountAndCode;
    	}
    	private set;
    } 
    
    
    public void displayRates()
    {
        try
        {
        	exceptionCount = 0;
        	DisplayRates = null;
        	Rates = null;
        	SplitRates = null;
        	
        	if(File.Name == null)
            {
                A2HCException.formatException('File is required.');
                File.Name = null;
                return;
            }
            
        	RateUpload.File_Name__c = File.Name;
        	RateUpload.Date_Uploaded__c = datetime.now();
        	upsert RateUpload;
        	
        	File.ParentId = RateUpload.ID;
            insert File;
            
            if(!parseCSV())
	        {
	            RateUpload = RateUpload.clone(false);
	            File = new Attachment();
	        }
            
            File = null;
            RateUpload = null;
        } 
        catch(Exception ex)
        {
        	A2HCException.formatException(ex);
        }
    }
    
    
    public PageReference saveOverride()
    {
		Savepoint sp = Database.setSavepoint();
		try
		{
			PageReference pg = null;
			
			if(Rates.size() > 0 || SplitRates.size() > 0)
			{
				for(RCR_Service_Rate__c r : SplitRates)
				{
					r.Bulk_Update__c = true;
				}
				upsert SplitRates;
				upsert Rates;
				
				List<RCR_Service_Rate__c> allRates = new List<RCR_Service_Rate__c>();
				
				allRates.addAll(SplitRates);
				allRates.addAll(Rates);

				integer batchSize = Batch_Size__c.getInstance('Rate_Recalculation').Records__c.IntValue();	
				
				// Perform batch cost recalculation of all affected Scheduled Services.
				RCRBatchUpdateScheduledServiceTotalCost batch = new RCRBatchUpdateScheduledServiceTotalCost();
		    	batch.Rates = allRates;
		    	batch.runMode = RCRBatchUpdateScheduledServiceTotalCost.Mode.MultipleRateUpdate;
				ID batchprocessid = Database.executeBatch(batch, batchSize);
				
				pg = Page.RCRRateUploadCompleted;
                pg.setredirect(true);
			}
			return pg;
		}
		catch(Exception ex)
		{
			return A2HCException.formatExceptionAndRollback(sp, ex);
		}
    }
    
    
    public PageReference cancel()
    {
    	try
        {
            return returnURL;
        }
        catch(Exception ex)
        {
            return A2HCException.formatException(ex);
        }
    }
    
   
    private boolean parseCSV()
    {
    	// Indexes indicate field position not character number.
    	final integer RATE_ID_INDEX = 0;
    	final integer SERVICE_CODE_INDEX = 1;
    	final integer SERVICE_LEVEL_INDEX = 2;
    	final integer START_DATE_INDEX = 3;
    	final integer END_DATE_INDEX = 4;
    	final integer RATE_INDEX = 5;
    	final integer PUBLIC_HOLIDAY_RATE_INDEX = 6;
    	final integer PROPOSED_RATE_STANDARD = 7;
    	final integer PROPOSED_RATE_PUBLIC_HOLIDAYS = 8;
    	final integer SPLIT_RATE_DATE_INDEX = 9;
    	final integer ACCOUNT_NAME_INDEX = 10;
    	
    	
    	boolean valid = true;
    	Integer lineCount = 2;
    	string fileBody = '';
    	
    	DisplayRates = null;
    	Rates = null;
    	SplitRates = null;

    	try
        {
            fileBody = File.body.toString();
        }
        catch(Exception ex) 
        {
            // If unable to convert the blob to a string dont throw an unfriendly error.
            // The user will still get the File Unreadable error because the file could not be split into fields.
        }
        
        CSVParser parser = new CSVParser(fileBody, true, ACCOUNT_NAME_INDEX + 1);
        
        // Read line into string array.
        String[] rateFields = parser.readLine();
        
        if(rateFields == null || rateFields.size() == 0 || rateFields.size() != ACCOUNT_NAME_INDEX + 1)
        {
            // CSV Parser could not split the line into the field string array, or there was no line to read.
            return A2HCException.formatValidationException(ErrorMsgs.get('File_Unreadable'));
        }

        // Initialise min and max dates.
        date minStartDate = Date.newInstance(2500, 1, 1);
        date maxEndDate = Date.newInstance(1900, 1, 1);
      
        while (rateFields != null && String.isNotBlank(rateFields[ACCOUNT_NAME_INDEX]))
        {        	
        	string serviceID = ServiceCodeIDsByAccountNameCodeAndLevel.get(rateFields[ACCOUNT_NAME_INDEX] + rateFields[SERVICE_CODE_INDEX] + rateFields[SERVICE_LEVEL_INDEX]);
        	decimal tempRate = null;
   	
        	if(serviceID == null)
        	{
        		A2HCException.formatException('Service Code ' + rateFields[SERVICE_CODE_INDEX] + ' does not exist.');
        		return false;
        	}

        	RCR_Service_Rate__c rate = new RCR_Service_Rate__c(Id = rateFields[RATE_ID_INDEX] == '' ? null : rateFields[RATE_ID_INDEX],
        													   RCR_Service__c = ServiceCodeIDsByAccountNameCodeAndLevel.get(rateFields[ACCOUNT_NAME_INDEX] 
        													   																+ rateFields[SERVICE_CODE_INDEX] 
        													   																+ rateFields[SERVICE_LEVEL_INDEX]));
        													   
			RateWrapper rateWrap = new RateWrapper(lineCount, rateFields[ACCOUNT_NAME_INDEX], rateFields[SERVICE_CODE_INDEX], rateFields[SERVICE_LEVEL_INDEX], rate);
													   
        	rateWrap.rate.Start_Date__c = parseDate(rateFields[START_DATE_INDEX], 'Start Date', false, rateWrap);
        	rateWrap.rate.End_Date__c = parseDate(rateFields[END_DATE_INDEX], 'End Date', false, rateWrap);
        	rateWrap.rate.Rate__c = parseDecimal(rateFields[RATE_INDEX], 'Rate', rateWrap).setScale(2);
        	rateWrap.rate.Public_Holiday_Rate__c = parseDecimal(rateFields[PUBLIC_HOLIDAY_RATE_INDEX], 'Public Holiday Rate', rateWrap).setScale(2);
        	
        	tempRate = parseDecimal(rateFields[PROPOSED_RATE_STANDARD], 'Proposed Rate (Standard)', rateWrap);
        	if(tempRate != null)
        	{
        		tempRate = tempRate.setScale(2);
        	}
        	rateWrap.rate.Proposed_Rate_Standard__c = tempRate;
        	
        	tempRate = parseDecimal(rateFields[PROPOSED_RATE_PUBLIC_HOLIDAYS], 'Proposed Rate (Public Holidays)', rateWrap);
        	if(tempRate != null)
        	{
        		tempRate = tempRate.setScale(2);
        	}
        	rateWrap.rate.Proposed_Rate_Public_Holidays__c = tempRate;
        	
        	rateWrap.SplitDate = parseDate(rateFields[SPLIT_RATE_DATE_INDEX], 'Split Date', true, rateWrap);
        	
        	//Force recalculation of Service Orders for existing Rates being updated or split.
        	rateWrap.rate.Recalculate_Service_Order_Value__c = (rateFields[RATE_ID_INDEX] != '' || (rateFields[RATE_ID_INDEX] != '' && rateFields[SPLIT_RATE_DATE_INDEX] != '')) ? true : false; 
    	 	
        	DisplayRates.add(rateWrap);      	
        	
        	if(rateWrap.SplitDate != null && rateWrap.Rate.Id != null)
        	{
        		splitRate(rateWrap);
        	}
        	else
        	{
        		Rates.add(rate);
        	}
        	
        	// Identify the earliest Start Date and latest End Date.
        	if(minStartDate > rate.Start_Date__c)
        	{
        		minStartDate = rate.Start_Date__c;
        	}
        	if(maxEndDate < rate.End_Date__c)
        	{
        		maxEndDate = rate.End_Date__c;
        	}
        	rateFields = parser.readLine();
        	lineCount++;
        }
        
        // Get rates that overlap the minimum start date and maximum end date from the file.
        getExistingRates(minStartDate, maxEndDate);
        
        for(RateWrapper rateWrpr : DisplayRates)
        {
       		rateWrpr.Error += getValidationError(rateWrpr);
        }
    	return valid;
    }
    
    
    private Date parseDate(String str, String fieldName, Boolean allowNull, RateWrapper rateWrap)
    {
        if(allowNull && String.isBlank(str))
        {
            return null;
        }
        Date dateVal = A2HCUtilities.tryParseDate(str);
        if(dateVal != null)
        {
            return dateVal;
        }
        else
        {
            exceptionCount++;           
            rateWrap.Error += fieldName + ': ' + str + ' is not a valid date. <br/>';           
            return null;
        }
    }
    
    
    private decimal parseDecimal(String str, String fieldName, RateWrapper rateWrap)
    {
        try
        {     	
        	if(str == null || str == '')
        	{
        		return null;
        	}
        	else
        	{
        		return decimal.valueOf(str);
        	}
        }
        catch(System.TypeException ex)
        {
        	exceptionCount++;
        	rateWrap.Error += fieldName + ': \"' + str + '\" is not a valid rate. <br/>';
        	return 0;
        }
    }
    
    
    private void getExistingRates(date minStartDate, date maxEndDate)
    {
    	for(RCR_Service_Rate__c rate : [SELECT  r.Id,
                                                r.Name,
                                                r.Start_Date__c,
                                                r.End_Date__c,
                                                r.RCR_Service__r.Name,
                                                r.RCR_Service__r.Account__r.Name,
                                                r.Bulk_Update__c,
                                                r.RCR_Service__r.Level__c
                                         FROM   RCR_Service_Rate__c r
                                         WHERE  ((r.Start_Date__c <= :maxEndDate AND r.End_Date__c >= :minStartDate)
        								 OR 	(r.Start_Date__c >= :minStartDate AND r.End_Date__c <= :maxEndDate))]) 
        {
        	if(!existingRatesByAccountAndCode.containsKey(rate.RCR_Service__r.Account__r.Name + rate.RCR_Service__r.Name))
        	{
        		existingRatesByAccountAndCode.put(rate.RCR_Service__r.Account__r.Name + rate.RCR_Service__r.Name, new List<RCR_Service_Rate__c>());
        	}
        	existingRatesByAccountAndCode.get(rate.RCR_Service__r.Account__r.Name + rate.RCR_Service__r.Name).add(rate);
        }
    }
    
    @testVisible
    private void splitRate(RateWrapper originalRateWrap)
    {
    	RCR_Service_Rate__c newRate = originalRateWrap.Rate.clone();
    	
    	newRate.Start_Date__c = originalRateWrap.SplitDate;
    	newRate.End_Date__c = originalRateWrap.Rate.End_Date__c;
    	newRate.Proposed_Rate_Standard__c = originalRateWrap.rate.Proposed_Rate_Standard__c;
    	newRate.Proposed_Rate_Public_Holidays__c = originalRateWrap.rate.Proposed_Rate_Public_Holidays__c;
    	
    	RateWrapper newRateWrap = new RateWrapper(originalRateWrap.LineNumber, originalRateWrap.ServiceProvider, originalRateWrap.ServiceCode, originalRateWrap.ServiceLevel, newRate);
	
    	RCR_Service_Rate__c origRate = originalRateWrap.Rate.clone(true, false, true, true);
    	origRate.Proposed_Rate_Standard__c = null;
    	origRate.Proposed_Rate_Public_Holidays__c = null;
    	origRate.End_Date__c = originalRateWrap.SplitDate.addDays(-1);
		SplitRates.add(origRate);
		Rates.add(newRate);
    }
    
    
    private string getValidationError(RateWrapper RCRServiceRate)
    {
        string validationError = '';
        boolean isValid = true;
		
        if(RCRServiceRate.Rate.End_Date__c <= RCRServiceRate.Rate.Start_Date__c)
        {
            validationError = 'End date must be later than start date.<br/>';
            exceptionCount++;
            isValid = false;
        }

		if(isValid)
		{			
			List<RCR_Service_Rate__c> rates = existingRatesByAccountAndCode.get(RCRServiceRate.ServiceProvider + RCRServiceRate.ServiceCode);
	
			if(rates != null)
			{
		        for(RCR_Service_Rate__c rate : rates)
		        {
		        	// Rate effective periods must not overlap each other.
		        	if(RCRServiceRate.Rate.Id <> rate.Id && RCRServiceRate.ServiceLevel == rate.RCR_Service__r.Level__c && A2HCUtilities.dateRangesOverlap(rate.Start_Date__c, rate.End_Date__c, RCRServiceRate.Rate.Start_Date__c, RCRServiceRate.Rate.End_Date__c))
					{	
						validationError = 'The date range must not overlap with an existing Service Rate for this Service.<br/>';
						exceptionCount++;
						isValid = false;
		                break;
					}
		        }
			}
		}
		
		if(isValid)
		{
			if(RCRServiceRate.SplitDate != null)
			{
				if(A2HCUtilities.isDateBetween(RCRServiceRate.SplitDate, RCRServiceRate.Rate.Start_Date__c, RCRServiceRate.Rate.End_Date__c))
    			{
					validationError = 'This rate will be split into two rates at the split date.';
    			}
    			else
    			{
    				validationError = 'The Split Date must be between the Start and End Dates of the Rate being split.<br/>';
		    		exceptionCount++;
    			}
			}
		}
        return validationError;
    }
    
    
    public class RateWrapper
    {
    	public RateWrapper(integer plineNumber, string pServiceProvider, string pServiceCode, string pServiceLevel, RCR_Service_Rate__c pRate)
    	{
    		LineNumber = pLineNumber;
    		ServiceProvider = pServiceProvider;
    		ServiceCode = pServiceCode;
    		ServiceLevel = pServiceLevel;
    		Error = '';
    		Rate = pRate;
    	}
    	
    	public integer LineNumber
    	{
    		get;
    		set;
    	}
    	
    	public string ServiceProvider
    	{
    		get;
    		set;
    	}
    	
    	public string ServiceCode
    	{
    		get;
    		set;
    	}
    	
    	public string ServiceLevel
    	{
    		get;
    		set;
    	}
    	
    	public date SplitDate
    	{
    		get;
    		set;
    	}
    	
    	public string Error
    	{
    		get;
    		set;
    	}
    	
    	public RCR_Service_Rate__c Rate
    	{
    		get;
    		set;
    	}
    }
}