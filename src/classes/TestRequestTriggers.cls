@isTest
public class TestRequestTriggers
{                    
    static testMethod void myTestMethod()
    {            
        TestLoadFormData data = new TestLoadFormData();
                 
        Test.StartTest();
                
        Request__c request = new Request__c();
        request.Request_Details__c = 'test';
        request.Client__c = data.client.Id;
        insert request;
        
        request.Status__c = 'In Progress';
        update request;
        
        for (Task t : [SELECT Id,
                              Status
                       FROM Task
                       WHERE WhatId = :Request.Id])
        {
            t.Status = 'Completed';
            update t;
        }                                                                                     
        
        Test.StopTest();
    }
}