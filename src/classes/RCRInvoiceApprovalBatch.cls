global class RCRInvoiceApprovalBatch
    implements Database.Batchable<sObject>
{
    private Set<ID> serviceOrderIds; // = new Set<ID> ();
    
    public RCRInvoiceApprovalBatch()
    {
        serviceOrderIds = new Set<ID>();    
    }
    
    public String InvoiceID
    {
        get;
        set
        {
            InvoiceIDs.add(value);
            InvoiceID = value;
        }
    }
    
    public Set<Id> InvoiceIDs
    {
        get
        {
            if(InvoiceIDs == null)
            {
                InvoiceIDs = new Set<Id>();
            }
            return InvoiceIDs;
        }
        set;
    }

    global Database.QueryLocator start(Database.BatchableContext BC)
    {            
        List<Batch_Log__c> logs = new List<Batch_Log__c>();
        for(RCR_Invoice__c inv : [SELECT    ID, 
                                            Invoice_Number__c 
                                    FROM    RCR_Invoice__c 
                                    WHERE   ID IN :InvoiceIDs])
        {
            Batch_Log__c log = new Batch_Log__c(Name = BC.getJobId(), Record_Id__c = inv.Id, Record_Name__c = inv.Invoice_Number__c);
            logs.add(log);
        }
        insert logs;
        
        return Database.getQueryLocator([SELECT  ID,
                                                    Name,
                                                    Code__c,
                                                    Error__c,
                                                    Quantity__c,
                                                    Rate__c,
                                                    Total__c,
                                                    Status__c,
                                                    RCR_Service_Order__c,
                                                    RecordTypeId
                                        FROM    RCR_Invoice_Item__c
                                        WHERE   RCR_Invoice__c IN :InvoiceIDs
                                        ORDER BY RCR_Invoice__c]);
    }

    global void execute(Database.BatchableContext batch, List<RCR_Invoice_Item__c> invoiceItems)
    {
        for(RCR_Invoice_Item__c item : invoiceItems)
        {
            serviceOrderIds.add(item.RCR_Service_Order__c); 
            item.RecordTypeId = A2HCUtilities.getRecordType('RCR_Invoice_Item__c', 'Submitted').ID;
        }
        update invoiceItems;
    }

    global void finish(Database.BatchableContext info)
    {
        List<RCR_Invoice__c> invoices = [SELECT Id 
                                        FROM    RCR_Invoice__c
                                        WHERE   Id IN :InvoiceIDs];                             
        for(RCR_Invoice__c invoice : invoices)
        {
            invoice.RecordTypeId = A2HCUtilities.getRecordType('RCR_Invoice__c', 'Submitted').Id;
        }
        update invoices;

        // create service query records for each rejected activity statement in the invoice TURNED OFF 29/7/2013
        // RCRReconcileInvoice.createServiceQueries(InvoiceIDs);
        // update scheduled and adhoc services submitted totals       
        serviceOrderIds = new Set<Id>();
        for (RCR_Invoice_Item__c i : [SELECT RCR_Service_Order__c
                                      FROM    RCR_Invoice_Item__c
                                      WHERE   RCR_Invoice__c IN :InvoiceIDs
                                      ORDER BY RCR_Invoice__c])
        {
            serviceOrderIds.add(i.RCR_Service_Order__c);
        }                                                              
        RCRReconcileInvoice.setServiceTotals(serviceOrderIds);

        EmailManager em = new EmailManager();
        if(invoices.size() == 1)
        {
            em.sendSubmitInvoiceAsyncComplete(invoices[0]);
        }
        else
        {
            em.sendSubmitInvoiceAsyncComplete(info.getJobId());
        }
        
        delete [SELECT  Id 
                FROM    Batch_Log__c 
                WHERE   Name = :info.getJobId()];
    }
}