public with sharing class RCRReportController extends A2HCPageBase
{
	public RCRReportController()
	{
		
	}
	
	public string SelectedReport
	{
		get;
		set;
	}
	
	public string StartDate
	{
		get;
		set;
	}
	
	public string EndDate
	{
		get;
		set;
	}
	
	public List<SelectOption> Reports
	{
		get
		{
			if(Reports == null)
			{
				Reports = new List<SelectOption>();
				Reports.add(new SelectOption('', '--Select Report--'));

				for(RCR_Report__c r : [SELECT	Name,
												API_Name__c 
									   FROM		RCR_Report__c 
								   		WHERE   API_Name__c IN :UserUtilities.accessablePages.keySet()
									   ORDER BY Name])
				{
					Reports.add(new SelectOption(r.API_Name__c, r.Name));
				}
			}
			return Reports;
		}
		private set;
	} 

	public PageReference runReport()
	{
		try
		{
			if(SelectedReport == null)
			{
				return A2HCException.formatException('You must select a report.');
			}
			
			PageReference page = new PageReference('/apex/' + SelectedReport);
			page.getParameters().put('startDate', StartDate);
			if(EndDate != null)
			{
				page.getParameters().put('endDate', EndDate);
			}
			return page.setRedirect(true);
		}
		catch(Exception ex)
		{
			return A2HCException.formatException(ex);
		}
	}

}