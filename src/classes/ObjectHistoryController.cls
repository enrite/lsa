public with sharing class ObjectHistoryController
{
    public SObject Record
    {
        get;
        set;
    }

    public Integer RecordLimit
    {
        get
        {
            return RecordLimit == null ? 100 : RecordLimit;
        }
        set;
    }

    public String ObjectLabel
    {
        get;
        private set;
    }

    public List<ObjectHistoryLine> ObjectHistory
    {
        get
        {
            if(ObjectHistory == null)
            {
                ObjectHistory = new List<ObjectHistoryLine>();
                getObjectHistory();
            }
            return ObjectHistory;
        }
        private set;
    }

    private void getObjectHistory()
    {
        if(Record == null)
        {
            return;
        }
        ID recordId = String.valueOf(Record.get('Id'));
        if(recordId == null)
        {
            return;
        }

        Map<String, Schema.SObjectType> objectTypeMap = Schema.getGlobalDescribe();
        Schema.DescribeSObjectResult objectDescription = Record.getSObjectType().getDescribe();
        Map<String, Schema.SObjectField> objectFieldMap = objectDescription.fields.getMap();
        String recordTypeIdPrefix = '012';
        ObjectLabel = String.valueOf(objectDescription.getLabel());

        //Get the name of the history table
        String tableName = objectDescription.getName();


        //if we have a custom object we need to drop the 'c' off the end before adding 'History' to get the history tables name
        if (objectDescription.isCustom())
        {
            tableName = tableName.substring(0, tableName.length()-1);
        }
        tableName = tableName + 'History';

        Schema.DescribeFieldResult objectHistoryField = objectTypeMap.get(tableName).getDescribe().fields.getMap().get('Field').getDescribe();
        String prevDate = '';

        List<sObject> historyList = Database.query( 'SELECT CreatedDate,'+
                                                    'CreatedById,'+
                                                    'Field,'+
                                                    'NewValue,'+
                                                    'OldValue ' +
                                                    'FROM ' + tableName + ' ' +
                                                    'WHERE ParentId =\'' + recordId + '\' ' +
                                                    'ORDER BY CreatedDate DESC '+
                                                    'LIMIT ' + String.valueOf(RecordLimit));

        for(Integer i = 0; i < historyList.size(); i++)
        {
            sObject historyLine = historyList.get(i);
            ObjectHistoryLine tempHistory = new ObjectHistoryLine();
            // Set the Date and who performed the action
            tempHistory.userId = getText(historyLine, 'CreatedById');
            String dtText = getText(historyLine, 'CreatedDate');
            // need to group by date so only show the first of similar dates
            if (dtText != prevDate)
            {
                tempHistory.theDate = dtText;
                tempHistory.who = tempHistory.userId;
            }
            else
            {
                tempHistory.theDate = '';
                tempHistory.who = '';   
            }
            prevDate = dtText;

            // Get the field label
            String fieldLabel = getFieldLabel(getText(historyLine, 'Field'), objectFieldMap, objectDescription);
            // Set the Action value
            if(getText(historyLine, 'Field') == 'created')
            {    // on Creation
                   tempHistory.action = 'Created.';
            }
            // record type audit returns two rows, one with name and one with ID, ignore the ID row
            else if(getText(historyLine, 'Field') == 'RecordType' && getText(historyLine, 'newValue').startsWith(recordTypeIdPrefix))
            {
                continue;
            }
            else if (historyLine.get('oldValue') != null && historyLine.get('newValue') == null)
            {
                tempHistory.action = 'Deleted ' + getText(historyLine, 'oldValue') + ' in <b>' + fieldLabel + '</b>.';
            }
            
            else
            {
                // all other scenarios
                String fromText = '';
                if (historyLine.get('oldValue') != null)
                {
                    fromText = ' from ' + getText(historyLine, 'oldValue');
                }

                String toText = null;
                if (historyLine.get('newValue') != null)
                {
                    toText = getText(historyLine, 'newValue');
                }
                if(fieldLabel == 'Created' || fieldLabel == 'Locked' || fieldLabel == 'Unlocked')
                {
                    tempHistory.action = fieldLabel;
                }
                else
                {
                    tempHistory.action = 'Changed <b>' + fieldLabel + (toText == null ? '' : ('</b>' + fromText + ' to <b>' + toText + '</b>.'));
                }
            }
               // Add to the list
               ObjectHistory.add(tempHistory);
        }

        List<Id> userIdList = new List<Id>();
        for (ObjectHistoryLine myHistory : ObjectHistory)
        {
            userIdList.add(myHistory.userId);
        }
        Map<Id, User> userIdMap = new Map<ID, User>([SELECT ID,
                                                    Name
                                            FROM    User
                                            WHERE   Id IN : userIdList]);

        for (ObjectHistoryLine myHistory : ObjectHistory)
        {
            if (userIdMap.containsKey(myHistory.userId) & (myHistory.who != '') )
            {
                myHistory.who = userIdMap.get(myHistory.who).Name;
            }
        }
    }

    @TestVisible
    private String getText(sObject historyLine, String fieldName)
    {
        if(fieldName == 'CreatedDate')
        {
            DateTime dtTime = DateTime.valueOf(historyLine.get(fieldName));
            return dtTime.format();
        }
        String val = String.valueOf(historyLine.get(fieldName));
        Date dt = A2HCUtilities.tryParseDate(val);
        if(dt != null)
        {
            return dt.format();
        }
        return val;
    }

    @TestVisible
    private String getFieldLabel(String fieldName, Map<String, Schema.SObjectField> objectFieldMap, Schema.DescribeSObjectResult objectDescription)
    {
        if(fieldName == 'created' || fieldName == 'locked' || fieldName == 'unlocked' || fieldName == 'RecordType')
        {
            return fieldName.capitalize();
        }
        if(objectFieldMap.containsKey(fieldName))
        {
            return objectFieldMap.get(fieldName).getDescribe().getLabel();
        }
        else
        {
            for(Schema.PicklistEntry pickList : objectDescription.fields.getMap().get(fieldName).getDescribe().getPickListValues())
            {
                if (pickList.getValue() == fieldName)
                {
                    if (pickList.getLabel() != null)
                    {
                        return pickList.getLabel();
                    }
                    else
                    {
                        return pickList.getValue();
                    }
                }
            }
        }
        return '';
    }


    // Inner Class to store the detail of the object history lines
    public class ObjectHistoryLine
    {
        public String theDate {get; set;}
        public String who {get; set;}
        public String userId {get; set;}
        public String action {get; set;}
    }
}