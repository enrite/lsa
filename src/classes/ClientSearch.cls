public abstract class ClientSearch extends GridPager2
{
    public ClientSearch()
    {
        super(20);
    }
    
    public ClientSearch(Integer pageSize)
    {
        super(pageSize);
    }
    public ClientSearch ClientSearchController
    {
        get
        {
            return this;
        }
    }
    public List<Referred_Client__c> Clients
    {
        get
        {
            if(Clients == null)
            {
                Clients = new List<Referred_Client__c>();
            }
            return Clients;
        }
        set;
    } 
    
    public String SearchCCMSID
    {
        get;
        set;
    }

    public String SearchGivenName
    {
        get;
        set;
    }

    public String SearchFamilyName
    {
        get;
        set;
    }

    public String SearchDOB
    {
        get;
        set;
    }

    public String SearchAddress
    {
        get;
        set;
    }

    public String SearchLocality
    {
        get;
        set;
    }

    public String SearchSex
    {
        get;
        set;
    }
    
    public boolean IFOnly
    {
        get;
        set;
    }

    public String SearchStatus
    {
        get;
        set;
    }

    public String SortImageURL
    {
        get
        {
            if(SortDirection == null)
            {
                return '';
            }
            return SortDirection == 'ASC' ? '/img/colTitle_uparrow.gif' : '/img/colTitle_downarrow.gif';
        }
    }

    public String SortDirection
    {
        get;
        set;
    }

    public Boolean SearchDone
    {
        get
        {
            if(SearchDone == null)
            {
                SearchDone = false;
            }
            return SearchDone;
        }
        set;
    }

    public List<SelectOption> SexList
    {
        get
        {
            if(SexList == null)
            {
                SexList = addPicklistOptions('Sex__c');
            }
            return SexList;
        }
        set;
    }

    public List<SelectOption> StatusList
    {
        get
        {
            if(StatusList == null)
            {
                StatusList = addPicklistOptions('Status__c');
            }
            return StatusList;
        }
        set;
    }

    protected String FieldName
    {
        get;
        set;
    }

    @TestVisible
    protected Map<String, Schema.SObjectField> ObjectFieldMap
    {
        get
        {
            if(ObjectFieldMap == null)
            {
                ObjectFieldMap = Referred_Client__c.sObjectType.getDescribe().fields.getMap();
            }
            return ObjectFieldMap;
        }
        set;
    }


    protected void setSearchFields()
    {
        SearchGivenName = setSearchString(SearchGivenName, true);
        SearchFamilyName = setSearchString(SearchFamilyName, true);
        SearchAddress = setSearchString(SearchAddress, true);
        SearchLocality = setSearchString(SearchLocality, true);
        SearchCCMSID = setSearchString(SearchCCMSID, false);
        SearchSex = setSearchString(SearchSex, false);
        SearchStatus = setSearchString(SearchStatus, false);
    }    
    
    @TestVisible        
    protected List<SelectOption> addPicklistOptions(String fieldName)
    {
        List<SelectOption> opts = new List<SelectOption>();
        opts.add(new SelectOption('', '<All>'));

        for(Schema.PicklistEntry p : ObjectFieldMap.get(fieldName).getDescribe().getPicklistValues())
        {
            if(p.isActive())
            {
                opts.add(new SelectOption(p.getValue(), p.getLabel()));
            }
        }
        return opts;
    }   
}