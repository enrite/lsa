/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestRCRScheduledServiceExtension 
{
    @IsTest
    static void test1()
    {
        TestLoadData.loadRCRData();
    
        Test.startTest(); 

        for(RCR_Contract_Scheduled_Service__c schServ : [SELECT r.Start_Date__c,
                                                                r.RCR_Service__c,
                                                                r.RCR_Service__r.Account__c,
                                                                r.RCR_Sub_Program__c,
                                                                r.RCR_Contract__c,
                                                                r.RCR_Contract__r.Account__c,
                                                                r.RCR_Contract__r.Level__c,
                                                                r.RCR_Contract__r.Name,
                                                                r.Name,
                                                                r.Id,
                                                                r.End_Date__c,
                                                                r.Original_End_Date__c,
                                                                r.Flexibility__c,
                                                                r.Public_holidays_not_allowed__c,
                                                                r.Comment__c,
                                                                Exception_Count__c,
                                                                //r.RCR_Contract__r.RCR_CBMS_Contract__r.Contract_Total__c,
                                                                (SELECT Date__c,
                                                                        Public_Holiday_Quantity__c,
                                                                        Standard_Quantity__c,
                                                                        RCR_Contract_Scheduled_Service__c
                                                                 FROM   RCR_Scheduled_Service_Exceptions__r
                                                                 ORDER BY Date__c)
                                                         FROM   RCR_Contract_Scheduled_Service__c r
                                                        LIMIT 1])
        {
            Test.setCurrentPage(Page.RCR_Scheduled_Service);
            ApexPages.currentPage().getParameters().put('retURL', '/' + schServ.RCR_Contract__c);
            RCRScheduledServiceExtension ext = new RCRScheduledServiceExtension(schServ.Id);

            ext.PageMode = true;
            ext.saveOverride();
            
            ApexPages.currentPage().getParameters().put('saveandnew', '1');
            ext.saveOverride();
            
            system.debug(ext.StartDate);
            system.debug(ext.EndDate);
            system.debug(ext.PageController); 
            

            ext.ScheduledService = null;
            system.debug(ext.ScheduledService);

            ext.Service = null;
            system.debug(ext.Service);

            system.debug(ext.ServiceTypes);

            system.assert(ext.Programs != null);

            ext.SubPrograms = null;
            system.debug(ext.SubPrograms);

            system.assert(ext.ProgramCategories != null);

            ext.ScheduledServiceExceptions = null;
            system.debug(ext.ScheduledServiceExceptions);
                    
        }

        Test.stopTest();
    }
    
    @IsTest
    static void test2()
    {
        TestLoadData.loadRCRData();
    
        Test.startTest();

        for(RCR_Contract_Scheduled_Service__c schServ : [SELECT r.Start_Date__c,
                                                                r.RCR_Service__c,
                                                                r.RCR_Service__r.Account__c,
                                                                r.RCR_Sub_Program__c,
                                                                r.RCR_Contract__c,
                                                                r.RCR_Contract__r.Account__c,
                                                                r.RCR_Contract__r.Level__c,
                                                                r.RCR_Contract__r.Name,
                                                                r.Name,
                                                                r.Id,
                                                                r.End_Date__c,
                                                                r.Original_End_Date__c,
                                                                r.Flexibility__c,
                                                                r.Public_holidays_not_allowed__c,
                                                                r.Comment__c,
                                                                Exception_Count__c,
                                                                //r.RCR_Contract__r.RCR_CBMS_Contract__r.Contract_Total__c,
                                                                (SELECT Date__c,
                                                                        Public_Holiday_Quantity__c,
                                                                        Standard_Quantity__c,
                                                                        RCR_Contract_Scheduled_Service__c
                                                                 FROM   RCR_Scheduled_Service_Exceptions__r
                                                                 ORDER BY Date__c)
                                                         FROM   RCR_Contract_Scheduled_Service__c r
                                                        LIMIT 1])
        {
            Test.setCurrentPage(Page.RCR_Scheduled_Service);
            ApexPages.currentPage().getParameters().put('retURL', schServ.RCR_Contract__c);
            RCRScheduledServiceExtension ext = new RCRScheduledServiceExtension();
            ext = new RCRScheduledServiceExtension(new ApexPages.StandardController(schServ));
            system.debug(ext.IsParticipationPlan);

            //ext.setComponentController(new ScheduleController());
            ////ext.setComponentController2(new RCRContractServiceTypeController());
            
            ext.RCRContract = [SELECT ID,
                                        Account__c,
                                        Level__c
                                FROM    RCR_Contract__c
                                WHERE   ID = :schServ.RCR_Contract__c];
            
            ext.ServiceList = null;
            system.debug(ext.ServiceList);

            update schServ;

            ApexPages.currentPage().getParameters().put('id', schServ.ID);
            ext = new RCRScheduledServiceExtension(new ApexPages.StandardController(schServ));

            ext.calculateCost();
            ext.saveOverride();
            ext.getSSClient();

            ext.addScheduledServiceException();
            ApexPages.currentPage().getParameters().put('rowIndex', '0');
            ext.deleteScheduledServiceException();
            ext.refreshPublicHolidayOptions();
            system.debug(ext.PeriodQuantity);
            system.debug(ext.ApplicationHelp);
            system.debug(ext.displayExceptions);
            
            ext.saveScheduledServiceExceptions();
            ext.validateServiceTypeQty();
            
            ext.ScheduledService = null;
            ext.cancelOverride();
        }

        Test.stopTest();
    }
    
    @IsTest
    static void test3()
    {
        TestLoadData.loadRCRData();
    
        Test.startTest();

        for(RCR_Contract_Scheduled_Service__c schServ : [SELECT r.Start_Date__c,
                                                                r.RCR_Sub_Program__c,
                                                                r.Name,
                                                                r.Id,
                                                                r.End_Date__c,
                                                                r.Original_End_Date__c,
                                                                Cancellation_Date__c,
                                                                r.Flexibility__c,
                                                                r.Public_holidays_not_allowed__c,
                                                                r.Comment__c,        
                                                                Exception_Count__c,               
                                                                r.Internal_Comments__c,
                                                                //r.RCR_Contract__r.RCR_CBMS_Contract__r.Contract_Total__c,
                                                                r.Total_Cost__c,
                                                                r.Total_Quantity__c,
                                                                r.Use_Negotiated_Rates__c,
                                                                r.Negotiated_Rate_Standard__c,
                                                                r.Negotiated_Rate_Public_Holidays__c,
                                                                Client__c,
                                                                Client__r.Name,
                                                                r.RCR_Service__c,
                                                                r.RCR_Service__r.Name,
                                                                r.RCR_Service__r.Account__c,
                                                                r.RCR_Contract__c,
                                                                r.RCR_Contract__r.Account__c,
                                                                r.RCR_Contract__r.Level__c,
                                                                r.RCR_Contract__r.Name,
                                                                r.RCR_Contract__r.Client__c,
                                                                r.RCR_Contract__r.Client__r.Name,
                                                               // RCR_Contract__r.DO_Day_Option__c, 
                                                                RCR_Contract__r.RecordType.Name,
                                                                (SELECT Date__c,
                                                                        Public_Holiday_Quantity__c,
                                                                        Standard_Quantity__c,
                                                                        RCR_Contract_Scheduled_Service__c
                                                                 FROM   RCR_Scheduled_Service_Exceptions__r
                                                                 ORDER BY Date__c),
                                                                 (SELECT Id,
                                                                        RCR_Program__c,
                                                                        RCR_Contract_Adhoc_Service__c,
                                                                        RCR_Contract_Scheduled_Service__c,
                                                                        RCR_Program_Category_Service_Type__c,
                                                                        Quantity__c
                                                                 FROM   RCR_Contract_Service_Types__r)
                                                         FROM   RCR_Contract_Scheduled_Service__c r
                                                        LIMIT 1])
        {
            Test.setCurrentPage(Page.RCR_Scheduled_Service);
            ApexPages.currentPage().getParameters().put('id', schServ.ID);
            ApexPages.currentPage().getParameters().put('retURL', schServ.RCR_Contract__c);
            RCRScheduledServiceExtension ext = new RCRScheduledServiceExtension(new ApexPages.StandardController(schServ));
            ext = new RCRScheduledServiceExtension();
            ext = new RCRScheduledServiceExtension(true);

            //ext.setComponentController(new ScheduleController());
            //ext.setComponentController2(new RCRContractServiceTypeController());
            
            ext.FlexibilityOptions = null;
            system.debug(ext.FlexibilityOptions);
                       
            system.assert(ext.PublicHolidayOptions != null);         
            
            ext.refreshPublicHolidayOptions();
            
            ext.refreshSchedule();
            
            ext.saveAndClose();
            //ext.copy();
            
            ext.saveScheduledService(schServ);
            
            ext.setTotals(schServ);
            
            system.debug(ext.Client);           
            system.debug(ext.ReadOnly);
            system.debug(ext.SubPrograms);
        }

        Test.stopTest();
    }
}