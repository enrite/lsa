public with sharing class RCRAuditController 
	extends A2HCPageBase
{

    public String EndDate 
    { 
    	get; 
    	set; 
	}

    public String StartDate
	{ 
    	get; 
    	set; 
	}
	
	public List<RCR_Invoice_Item__c> Items
	{ 
    	get
    	{
    		if(Items == null)
    		{
    			Items = new List<RCR_Invoice_Item__c>();
    		}
    		return Items;
    	}
    	private set; 
	}
	
	public void search()
	{
		try
		{
			Items = null;
			if(String.isBlank(StartDate) || String.isBlank(EndDate))
			{
				A2HCException.formatException('Start Date and End Date are required.');
				return;
			}
			
			Date startDt = A2HCUtilities.tryParseDate(StartDate);
			Date endDt = A2HCUtilities.tryParseDate(EndDate);
			if(startDt == null || endDt == null)
			{
				A2HCException.formatException('Start Date and End Date are not valid dates.');
				return;
			}
			
			List<RCR_Invoice_Item__c> allItems = [SELECT	ID,
															Name,
															RCR_Service_Order__c,
															RCR_Service_Order__r.Name,
															RCR_Invoice__r.Name,
															RCR_Invoice__r.Invoice_Number__c
													FROM	RCR_Invoice_Item__c
													WHERE	RecordTypeID = :(A2HCUtilities.getRecordType('RCR_Invoice_Item__c', 'Submitted').ID) AND
															Status__c = 'Approved' AND
															RCR_Service_Order__c != null AND
															Activity_Date__c >= :startDt AND
															Activity_Date__c <= :endDt
													LIMIT 10000];
			if(allItems.size() <= 10)
			{
				Items = allItems;
				A2HCException.formatWarning((allItems.size() == 0 ? 'No' : 'Only ' + allItems.size().format()) + ' items were found.');
				return;
			}													
			Set<Integer> itemNumbers = new Set<Integer>();			
			while(itemNumbers.size() <= 10)
			{
				Integer val = (Math.random() * allItems.size()).intValue();				
				if(val <= allItems.size())
				{
					itemNumbers.add(val);
				}
			}			
			for(Integer itemIndex : itemNumbers)	
			{
				Items.add(allItems[itemIndex]);
				if(Items.size() >= 10)
				{
					break;
				}									
			}												
		}
		catch(Exception ex)
		{
			A2HCException.formatException(ex);
		}
	}
	
	@isTest(SeeAllData=true)
	static void test1()
	{
		RCRAuditController ext = new RCRAuditController();
		ext.StartDate = '01/01/2000';
		ext.EndDate = '01/01/2030';
		ext.search();
		system.debug(ext.Items);
	}
	
	@isTest(SeeAllData=true)
	static void test2()
	{
		RCRAuditController ext = new RCRAuditController();
		ext.StartDate = '';
		ext.EndDate = '';
		ext.search();
		
		ext.StartDate = 'blah';
		ext.EndDate = 'blah';
		ext.search();
		system.debug(ext.Items);
	}
	
	@isTest(SeeAllData=true)
	static void test3()
	{
		RCRAuditController ext = new RCRAuditController();
		ext.StartDate = '01/01/2090';
		ext.EndDate = '01/01/2090';
		ext.search();
		system.debug(ext.Items);
	}
}