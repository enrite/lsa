public with sharing class RCRCreateServiceOrderFromRollover extends A2HCPageBase
{
	public RCRCreateServiceOrderFromRollover(ApexPages.StandardController cntrlr)
	{
		
	}
	
	public PageReference CreateServiceOrder()
	{
		try
		{
			throwTestError();
			
			List<Id> recordIDs = new List<Id>();
			recordIDs.add(getParameter('id'));
			ServiceOrderFuture.submitRolloversForApproval(recordIDs);
			
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'A new Service Order has been scheduled to be created from this rollover and will be ready shortly.'));
			
			return null;
		}
		catch(Exception ex)
		{
			return A2HCException.formatException(ex);
		}
	}
}