@isTest
public class TestProcessNotificationEmail
{                
    static testMethod void myTestMethod() 
    {    
        // create a new email and envelope object
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        
        // setup the data for the email
        email.subject = 'Test';
        email.fromname = 'Test';
        env.fromAddress = 'tes@test.com';
        
        // add an attachment
        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();                
        attachment.body = blob.valueOf('test');
        attachment.fileName = 'test.csv';
        attachment.mimeTypeSubType = 'text/csv';
        
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
        
        // call the email service class and test it with the data in the testMethod
        ProcessNotificationEmail emailProcess = new ProcessNotificationEmail();
        emailProcess.handleInboundEmail(email, env);                  
    }
}