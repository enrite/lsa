public with sharing class RCRServiceOrderSubmitForCSABatch 
		extends RCRServiceOrderCloneBase 
		implements Database.Batchable<sObject>, Database.Stateful 
{
	public RCRServiceOrderSubmitForCSABatch(RCRServiceOrderCloneBase previousBatch) 
	{
		super(previousBatch);
	}
	
    public Database.QueryLocator start(Database.BatchableContext batch)  
    {
    	reparentBatchLogs(batch); 
    	return Database.getQueryLocator([SELECT Id,
    											Name
						    			FROM	RCR_Contract__c
						    			WHERE	Source_Record__c IN :ServiceOrderIds]);
    }
    
    public void execute(Database.BatchableContext batch, List<RCR_Contract__c> serviceOrders) 
    {
    	List<Id> soIDs = new List<Id>();
    	Map<string, RCR_Contract__c> serviceOrdersById = new Map<String, RCR_Contract__c>();
    	List<Batch_Log__c> batchLogs = new List<Batch_Log__c>();
    	
    	for(RCR_Contract__c so : serviceOrders)
    	{
    		soIDs.add(so.Id);
    		serviceOrdersById.put(so.Id, so);
    	}
    	Approval.Processresult[] results =  ServiceOrderFuture.submitForApprovalImmediate(soIDs,  false);
    	
    	for(Approval.Processresult result : results)
    	{
    		if(!result.isSuccess())
    		{
    			addBatchLog(serviceOrdersByID.get(result.getEntityId()), 
		        						batchLogs, 
		        						batch, 
		        						'Record could not be submitted for approval', 
		        						result.getErrors()[0].getMessage()); 
    		}
    	}
    	upsert batchLogs;
    }

	public void finish(Database.BatchableContext info)
    {
    	finaliseBatch(info, null); 	
    }
}