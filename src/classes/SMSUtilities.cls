public class SMSUtilities
{
    private static SMS_Settings__c Settings
    {
        get
        {
            SMS_Settings__c s = SMS_Settings__c.getInstance();
            if(s == null || s.URL__c == null || s.Username__c == null || s.Password__c == null)
            {
                throw new A2HCException('SMS settings have not been configured, contact your system administrator.');   
            }
            return s;
        }
    }

    private static String BaseURL
    {
        get
        {
            return Settings.URL__c + '?username=' + Settings.Username__c + '&password=' + Settings.Password__c;
        }
    }

    public static SMS_Message_Recipient__c sendSMS(sObject record, String phNumber, String message)
    {
        SMS_Message__c smsMessage = new SMS_Message__c();
        smsMessage.Message__c = message;
        SMS_Message_Recipient__c msgRecipient = sendSMS(record, message, phNumber, null);
        insert smsMessage;
        msgRecipient.SMS_Message__c = smsMessage.ID;
        insert msgRecipient;
        return msgRecipient;
    }


    public static Map<ID, String> sendSMS(List<sObject> records, SMS_Message__c smsMessage, SObjectField phNumberField, String message)
    {
        List<SMS_Message_Recipient__c> smsMessageRecipients = new List<SMS_Message_Recipient__c>();
        Map<ID, String> msgsByClientId = new Map<ID, String>();
        Integer lastMessageId = getLatestMessageId();
        for(sObject record : records)
        {
            String phNumber = (String)record.get(phNumberField);
            if(String.isBlank(phNumber))
            {
                continue;
            }
            try
            {
                smsMessageRecipients.add(sendSMS(record, message, phNumber, ++lastMessageId));
                msgsByClientId.put(record.ID, '');
            }
            catch(Exception ex)
            {
                msgsByClientId.put(record.ID, ex.getMessage());
            }
        }
        insert smsMessage;
        for(SMS_Message_Recipient__c mr : smsMessageRecipients)
        {
            mr.SMS_Message__c = smsMessage.ID;
        }
        insert smsMessageRecipients;
        return msgsByClientId;
    }
    
    private static SMS_Message_Recipient__c sendSMS(SObject record, String message, String phNumber, Integer messageId)
    {
        if(messageId == null)
        {
            messageId = getLatestMessageId() + 1;
        }
        message = getFormttedMessage(record, message);
    	String[] response = sendSMSToGateway(message, phNumber, messageId, true);
        if(response == null || response.size() == 0)
        {
            throw new A2HCException('SMS gateway did not respond.');   
        }
        else
        {
            if(response[0] == 'BAD')
	        {
	            throw new A2HCException('SMS gateway rejected the message due to an invalid number.');
	        }
            else if(response[0] == 'ERROR')
            {
                throw new A2HCException(response[1]);
            }
            SMS_Message_Recipient__c smsMessageRecipient = new SMS_Message_Recipient__c();
            if(record instanceof Referred_Client__c)
            {
                smsMessageRecipient.Participant__c = record.ID;
                smsMessageRecipient.Account__c = (ID)record.get(Referred_Client__c.Client_managed_for__c);
            }
            else if(record instanceof Participant_Contact__c)
            {
                smsMessageRecipient.Participant_Contact__c = record.ID;
            }
            else if(record instanceof Contact)
            {
                smsMessageRecipient.Account_Contact__c = record.ID;
                smsMessageRecipient.Account__c = (ID)record.get(Contact.AccountId);
            }
            smsMessageRecipient.Message__c = message;
            smsMessageRecipient.Sent_To__c = phNumber;
            smsMessageRecipient.Gateway_Reference__c = response[2];
            smsMessageRecipient.Message_Id__c = messageId;
            smsMessageRecipient.Message_Sent__c = Datetime.now();
            return smsMessageRecipient;
        }
    }

    private static String getFormttedMessage(sObject record, String message)
    {
        // message can be formatted using String.format()
        // {0} = Recipient's title
        // {1} = Recipient's first name
        // {2} = Recipient's last name
        if(!message.contains('{'))
        {
            return message;
        }
        List<String> substitutes = new List<String>();
        if(record instanceof Referred_Client__c)
        {
            Referred_Client__c client = (Referred_Client__c)record;
            substitutes = new List<String>{client.Title__c, client.Given_Name__c, client.Family_Name__c};
        }
        else if(record instanceof Participant_Contact__c)
        {
            Participant_Contact__c pc = (Participant_Contact__c)record;
            substitutes = new List<String>{pc.Title__c, pc.First_Name__c, pc.Surname__c};
        }
        else if(record instanceof Contact)
        {
            Contact cont = (Contact)record;
            substitutes = new List<String>{cont.Title, cont.FirstName, cont.LastName};
        }
        for(String s : substitutes)
        {
            s = s == null ? '' : s;
        }
        return String.format(message, substitutes);
    }

    private static String[] sendSMSToGateway(String message, String phNumber, Integer msgId, Boolean checkBalance)
    {
        phNumber = cleanPhoneNumber(phNumber, true);   
        if(String.isBlank(phNumber))
        {
            throw new A2HCException('Mobile phone number is required to send an SMS.');
        }
          
        if(checkBalance)
        {
            Integer balance = getBalance();
            if(balance < Settings.Minimum_Balance__c)
            {
                if(balance == 0 || Settings.Low_Balance_Phone_Number__c == null)
                {
                    return null;
                }
                sendSMSToGateway('Credit low (' + balance.format() + ')', Settings.Low_Balance_Phone_Number__c, null, false);
            }
        }

        message = EncodingUtil.urlEncode(message, 'UTF-8');
        HttpRequest request = new HttpRequest();
        request.setMethod('POST');
        String url = BaseURL + 
                        '&to=' + phNumber + 
                        '&from=' + Settings.From__c + 
                        '&message=' + message + 
                        (msgId == null ? '' : ('&ref=' + msgId.format().replace(',', ''))) + 
                        '&maxsplit=5';
        request.setEndPoint(url);
        String[] response;
        if(Test.isRunningTest())
        {
           response = new String[]{'test', 'test', 'test'};
        }
        else
        {
            Http http = new Http();
            HTTPResponse res = http.send(request);
            response = res.getBody().split(':');
        }
        return response;           
    }

    public static String cleanPhoneNumber(String phNumber, Boolean throwException)
    {
        phNumber = phNumber.replace('(', '').replace(')', '').deleteWhitespace();
        Pattern mobilePattern = Pattern.compile('04[0-9]{8}');
        if(!(mobilePattern.matcher(phNumber).matches()))
        {
            if(throwException)
            {
                throw new A2HCException('Mobile phone number must be in the format 0499999999.');
            }
            return null;
        }
        return phNumber;
    }

    public static Integer getBalance()
    {
        Integer minBalance = Test.isRunningTest() ? 20 : Settings.Minimum_Balance__c.intValue();
    	try
    	{
	        HttpRequest request = new HttpRequest();
	        request.setMethod('POST');
	        request.setEndPoint(BaseUrl + '&action=balance');
	        Http http = new Http();
            HTTPResponse res = Test.isRunningTest() ? null : http.send(request);
            String[] response = Test.isRunningTest() ? new String[]{'test', '1'} : res.getBody().split(':');
	        if(response.size() != 2 || response[0] == 'ERROR')
	        {
	            return minBalance;
	        }   
        	return Integer.valueOf(response[1]); 
    	}
    	catch(Exception ex)
    	{
    		return minBalance;
    	}
    }

    private static Integer getLatestMessageId()
    {
        Integer msgId = 0;
        for(SMS_Message_Recipient__c s : [SELECT    Message_ID__c
                                            FROM    SMS_Message_Recipient__c
                                            WHERE   Message_ID__c != null
                                            ORDER BY Message_ID__c DESC
                                            LIMIT 1])
        {
            msgId = s.Message_ID__c.intValue();
        }
        return msgId;
    }
}