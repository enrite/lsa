@RestResource(UrlMapping='/LPMemberResponse/*')
global without sharing class LPMemberResponse
{
    @HttpPost
    global static void setStatus()
    {
        String requestBody = RestContext.request.requestBody.toString();
        LPMemberActivationResponseJSON memberActivation = LPMemberActivationResponseJSON.parse(requestBody);
        String clientNumber = memberActivation.data.membershipNumber;
        system.debug(memberActivation);
        system.debug(clientNumber);
        for(Referred_Client__c client : [SELECT ID,
                                                LP_Member_Status__c
                                        FROM    Referred_Client__c
                                        WHERE   Client_ID__c = :clientNumber AND
                                                Client_ID__c != null
                                        LIMIT 1])
        {
            String newMemberStatus = null;
            if(memberActivation.type_Z == 'planmanagement.member.activated')
            {
                newMemberStatus = 'Activated';
            }
            else if(memberActivation.type_Z == 'planmanagement.member.addfailed')
            {
                newMemberStatus = 'Failed';
            }
            system.debug(newMemberStatus);
            if(newMemberStatus == 'Failed' || newMemberStatus == 'Activated')
            {
                String key = LPRESTInterface.getMemberKey(memberActivation);
                system.debug(key);
                if(key != null)
                {
                    client.LP_Member_Date__c = Datetime.now();
                    client.LP_Member_Key__c = key;
                    client.LP_Member_Status__c = newMemberStatus;

                    TriggerBypass.Client = true;
                    update client;
                }
            }
        }
    }
}