@isTest//(SeeAllData=true)
private class TestA2HCExceptions
{
    static testMethod void myUnitTest() 
    {
        A2HCException ex = new A2HCException('test');
        
        system.assert(A2HCException.formatException(ex) == null);
        system.assert(A2HCException.formatWarning('test') == null);
        system.assert(A2HCException.formatMessage('test') == null);
        system.assert(A2HCException.formatWarning(ApexPages.Severity.ERROR, 'test') == null);
        system.assert(A2HCException.formatException(ApexPages.Severity.ERROR, ex) == null);
        system.assert(A2HCException.formatException('test') == null);
        system.assert(A2HCException.formatException(ApexPages.Severity.ERROR, 'test') == null);     
        system.assert(A2HCException.formatValidationException('test') != null);
        
        SavePoint sp = Database.setSavepoint();
        system.assert(A2HCException.formatExceptionAndRollback(sp, 'test') == null);
        system.assert(A2HCException.formatExceptionAndRollback(sp, ex) == null);
        system.assert(A2HCException.getExceptionMessage(ex) != null);
        system.assert(A2HCException.getExceptionMessage(ex, 1) != null);
        system.assert(A2HCException.getExceptionMessage(ex, 100) != null);
        
        /*
        User portalUser = [SELECT ID,
                                    Contact.AccountID,
                                    Contact.Account.Name
                            FROM    User
                            WHERE   Profile.Name = 'RCR Portal' AND
                                    IsActive = true 
                            LIMIT 1];
       
        for(RCR_Contract__c serviceOrder : [SELECT ID
                                            FROM    RCR_Contract__c
                                            WHERE   Account__c != :portalUser.Contact.AccountID
                                            LIMIT 1])
        {
            System.runAs(portalUser)
            {
                try
                {
                    update serviceOrder;
                }
                catch(Exception e)
                {
                    if(e instanceof DMLException)
                    {
                        system.assert(A2HCException.getExceptionMessage(e) != null);
                    }
                }
            }   
        }  */                 
    }
}